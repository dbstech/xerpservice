FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY src/MagicLamp.PMS.Application/*.csproj ./src/MagicLamp.PMS.Application/
COPY src/MagicLamp.PMS.Core/*.csproj ./src/MagicLamp.PMS.Core/
COPY src/MagicLamp.PMS.EntityFrameworkCore/*.csproj ./src/MagicLamp.PMS.EntityFrameworkCore/
COPY src/MagicLamp.PMS.Web/*.csproj ./src/MagicLamp.PMS.Web/
WORKDIR /app/src/MagicLamp.PMS.Web
RUN dotnet restore

# copy and publish app and libraries
WORKDIR /app/
COPY src/. ./src/
WORKDIR /app/src/MagicLamp.PMS.Web
RUN dotnet publish -c Release -o out


# test application -- see: dotnet-docker-unit-testing.md
# FROM build AS testrunner
# WORKDIR /app/tests
# COPY tests/. .
# ENTRYPOINT ["dotnet", "test", "--logger:trx"]


FROM mcr.microsoft.com/dotnet/core/runtime:2.2 AS runtime

# Update stretch repositories
RUN sed -i s/deb.debian.org/archive.debian.org/g /etc/apt/sources.list
RUN sed -i 's|security.debian.org|archive.debian.org/|g' /etc/apt/sources.list
RUN sed -i '/stretch-updates/d' /etc/apt/sources.list

# runtime dependencies
RUN apt-get update && apt-get install -y \
    libc6-dev \
    libgdiplus \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app
COPY --from=build /app/src/MagicLamp.PMS.Web/out ./
ENTRYPOINT ["dotnet", "MagicLamp.PMS.Web.dll"]
