using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MagicLamp.PMS.Web.Startup;
namespace MagicLamp.PMS.Web.Tests
{
    [DependsOn(
        typeof(PMSWebModule),
        typeof(AbpAspNetCoreTestBaseModule)
        )]
    public class PMSWebTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PMSWebTestModule).GetAssembly());
        }
    }
}