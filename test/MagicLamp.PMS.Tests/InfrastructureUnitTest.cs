﻿using MagicLamp.PMS.Proxy;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Xunit;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.DTOs;
using Abp.AutoMapper;
using MagicLamp.XERP.BackgroundJob;
using MagicLamp.PMS.DTOs.ThirdPart;
using Newtonsoft.Json;
using System.Net.Http;
using MagicLamp.PMS.DTO;
using MagicLamp.PMS.Domain;
using NPOI.SS.Formula;
using System.Drawing;

namespace MagicLamp.PMS.Tests
{
    public class InfrastructureUnitTest
    {

        [Fact]
        public void TestCLSLogger()
        {
            try
            {
                CLSLogger.Info("netcoretest", "cccc", null);
            }
            catch
            {

                throw;
            }

        }

        [Fact]
        public void TestReflection()
        {
            try
            {
                List<object> parameters = new List<object>();
                IDCardDTO input = new IDCardDTO
                {
                    Id = "id11111",
                    Number = "id11111",
                    Name = "lee",
                    OrderID = "orderid",
                    CourierNo = "courierno"
                };
                OrderIDCardEntity inputEntity = new OrderIDCardEntity
                {
                    Id = "id11111",
                    IDCardNo = "id11111",
                    IDCardName = "lee",
                    OrderID = "orderid",
                    CarrierID = "courierno"
                };

            }
            catch
            {

                throw;
            }

        }

        [Fact]
        public void TestOrderIDJob()
        {
            new OrderIDCardJob().MatchData();
        }

        [Fact]
        public void TestUploadIDCard()
        {
            dynamic result1 = GetCountryFromIP("203.211.75.147", "b062936e9f3063");
            string city = result1.city;


            MagicLamp.PMS.Proxy.UserLoginLogProxy.UserLoginInfo userlog = new UserLoginLogProxy.UserLoginInfo();
            userlog.UserName = "Spark111";
            userlog.IpAddress = "1111";
            userlog.UserAgent = "2222222";
            userlog.LoginResult = string.Format("{0} Opened IDCard at {1} ", "Lumpy", System.DateTime.Now);
            userlog.City = "222";
            userlog.Region = "222";
            userlog.Country = "222";
            userlog.Org = "222";
            userlog.Loc = "222";
            userlog.Timezone = "222";
            MagicLamp.PMS.Proxy.UserLoginLogProxy.AddUserLoginInfo(userlog);

        }


        public static dynamic GetCountryFromIP(string ip, string apiKey)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = "GetCountryFromIP"
            };

            try
            {
                var url = $"http://ipinfo.io/{ip}?token={apiKey}";

                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(url).Result;
                    var json = response.Content.ReadAsStringAsync().Result;
                    dynamic result = JsonConvert.DeserializeObject(json);

                    //CLSLogger.Error("GetCountryFromIPSuccess", JsonConvert.SerializeObject(result), clogTags);
                    return result;
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("GetCountryFromIP失败", ex, clogTags);
                return null;

            }

        }

        [Fact]
        public void TestsycnFluxInventory()
        {
            List<string> sku = new List<string>();
            sku.Add("ZNZATDD552265");

            SyncFluxStockJob testsycninventory = new SyncFluxStockJob();
            testsycninventory.SyncStock(sku);

        }
    }
}
