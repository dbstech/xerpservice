﻿using System;
using System.Threading.Tasks;
using Abp.TestBase;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Tests.TestDatas;

namespace MagicLamp.PMS.Tests
{
    public class PMSTestBase : AbpIntegratedTestBase<PMSTestModule>
    {
        public PMSTestBase()
        {
            UsingDbContext(context => new TestDataBuilder(context).Build());
        }

        protected virtual void UsingDbContext(Action<PMSDbContext> action)
        {
            using (var context = LocalIocManager.Resolve<PMSDbContext>())
            {
                action(context);
                context.SaveChanges();
            }
        }

        protected virtual T UsingDbContext<T>(Func<PMSDbContext, T> func)
        {
            T result;

            using (var context = LocalIocManager.Resolve<PMSDbContext>())
            {
                result = func(context);
                context.SaveChanges();
            }

            return result;
        }

        protected virtual async Task UsingDbContextAsync(Func<PMSDbContext, Task> action)
        {
            using (var context = LocalIocManager.Resolve<PMSDbContext>())
            {
                await action(context);
                await context.SaveChangesAsync(true);
            }
        }

        protected virtual async Task<T> UsingDbContextAsync<T>(Func<PMSDbContext, Task<T>> func)
        {
            T result;

            using (var context = LocalIocManager.Resolve<PMSDbContext>())
            {
                result = await func(context);
                context.SaveChanges();
            }

            return result;
        }
    }
}
