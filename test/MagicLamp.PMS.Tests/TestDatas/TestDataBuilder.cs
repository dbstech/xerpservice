using MagicLamp.PMS.EntityFrameworkCore;

namespace MagicLamp.PMS.Tests.TestDatas
{
    public class TestDataBuilder
    {
        private readonly PMSDbContext _context;

        public TestDataBuilder(PMSDbContext context)
        {
            _context = context;
        }

        public void Build()
        {
            //create test data here...
        }
    }
}