﻿using Abp.AutoMapper;
using Abp.Json;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;

namespace MagicLamp.XERP.BackgroundJob
{
    public class SaveOrderPackDetailProductCostJob : CommonBackgroundJob<List<OrderPackDetailProductCostDTO>>
    {
        private SendNotificationToDingDingJob _dingdingNotificationJob = new SendNotificationToDingDingJob();

        public override void Process(List<OrderPackDetailProductCostDTO> inputDto)
        {

            if (inputDto == null || !inputDto.Any())
            {
                CLSLogger.Warn("参数为空", inputDto.ToJsonString(), LogTags);
                return;
            }


            LogTags["orderid"] = inputDto.FirstOrDefault().OrderID;

            try
            {
                CLSLogger.Info("记录订单内商品批次成本input", inputDto.ToJsonString(), LogTags);

                PMSDbContext pmsDbContext = new PMSDbContextFactory().CreateDbContext();
                // RetryHelper
                bool success = false;
                int retryCount = 0;

                while (!success && retryCount < 3)
                {
                    try
                    {
                        SaveProductCost(inputDto, pmsDbContext);
                        success = true;
                    }
                    catch (SqlException ex) when (ex.Number == 1205) // SQL Server Deadlock Error Code
                    {
                        retryCount++;
                        Thread.Sleep(10000); // 延迟10秒再试
                    }
                }
                CLSLogger.Info("记录订单内商品批次成本完成", inputDto.ToJsonString(), LogTags);
            }
            catch (Exception ex)
            {
                CLSLogger.Error("记录订单内商品批次成本出错", ex.ToString(), LogTags);
                throw ex;
            }
        }

        private void SaveProductCost(List<OrderPackDetailProductCostDTO> inputDto, PMSDbContext pmsDbContext)
        {
            Dictionary<string, string> LogTags = new Dictionary<string, string>
            {
                ["jobid"] = Guid.NewGuid().ToString(),
                ["method"] = "SaveProductCost"
            };

            string MessageContent = "";

            try
            {
                if (inputDto == null || !inputDto.Any())
                {
                    return;
                }

                LogTags["orderid"] = inputDto.FirstOrDefault()?.OrderID;

                var listSKU = inputDto.Select(x => x.SKU.ToLower()).Distinct().ToList();

                var listProduct = pmsDbContext.Products
                                              .Where(x => listSKU.Contains(x.SKU.ToLower()))
                                              .ToList();

                var listPurchaseOrderLine = pmsDbContext.PurchaseOrderLines
                                                        .Where(x => listSKU.Contains(x.Sku.ToLower()))
                                                        .OrderByDescending(x => x.CreateDate)
                                                        .ToList();

                var entities = new List<OrderPackDetailProductCostEntity>();

                foreach (var item in inputDto)
                {
                    var entity = item.MapTo<OrderPackDetailProductCostEntity>();

                    var productInfo = listProduct.FirstOrDefault(x => x.SKU.StandardizeSKU() == item.SKU.StandardizeSKU());

                    if (productInfo == null || productInfo.Cost == null)
                    {
                        // 记录日志
                        CLSLogger.Info("Productinfoorcostisnull", productInfo?.ToJsonString(), LogTags);

                        var orderPoLine = listPurchaseOrderLine.FirstOrDefault(x => x.Sku.StandardizeSKU() == item.SKU.StandardizeSKU());

                        if (orderPoLine != null)
                        {
                            entity.Cost = orderPoLine.Cost;
                            entity.Currency = orderPoLine.Currency;
                        }
                    }
                    else
                    {
                        entity.Cost = productInfo.Cost;
                        entity.Currency = productInfo.Currency;
                    }

                    entities.Add(entity);
                }

                pmsDbContext.OrderPackDetailProductCosts.AddRange(entities);
                pmsDbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageContent = MessageContent + $" ##### 记录订单内商品批次成本出错原因:  {ex.ToJsonString()} \n\n";
                _dingdingNotificationJob.SendEcinxERPAlert("记录订单内商品批次成本出错", MessageContent);
                CLSLogger.Error("记录订单内商品批次成本出错", ex.ToString(), LogTags);
                //throw;
            }
            
        }

        public static class RetryHelper
        {
            /// <summary>
            /// Retries the specified action on exception.
            /// </summary>
            /// <param name="retryCount">Number of retry attempts.</param>
            /// <param name="delay">Delay between retries.</param>
            /// <param name="action">The action to execute.</param>
            public static void RetryOnException(int retryCount, TimeSpan delay, Action action)
            {
                if (retryCount < 1) throw new ArgumentException("Retry count must be greater than 0.", nameof(retryCount));
                if (action == null) throw new ArgumentNullException(nameof(action));

                for (int attempt = 1; attempt <= retryCount; attempt++)
                {
                    try
                    {
                        action();
                        return; // Exit if the action succeeds
                    }
                    catch (Exception ex)
                    {
                        if (attempt == retryCount)
                        {
                            // Log or rethrow the exception if all retries failed
                            throw;
                        }

                        // Optional: Log the exception for debugging
                        Console.WriteLine($"Attempt {attempt} failed: {ex.Message}");
                    }

                    // Wait before the next attempt
                    Thread.Sleep(delay);
                }
            }
        }

    }
}
