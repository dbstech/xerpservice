﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using MagicLamp.PMS.DTOs;
using MagicLamp.XERP.BackgroundJob.Common;
using MagicLamp.PMS.Entities;
using Abp.Domain.Repositories;
using System.Linq;
using MagicLamp.PMS.DTO;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.EntityFrameworkCore;
using Abp.UI;
using MagicLamp.PMS.Infrastructure.Extensions;

namespace MagicLamp.XERP.BackgroundJob
{
    public class NSFBackgroundJob : CommonBackgroundJob<NSFOrderDTO>
    {
        private const string ClogTagType = "NSFBackgroundJob";

        public NSFBackgroundJob()
        {

        }

        public override void Process(NSFOrderDTO inputDto)
        {
            switch (inputDto.Action)
            {
                case NSFBackgroundJobActionEnum.CreateOrder:
                    CreateOrder(inputDto);
                    break;
                default:
                    throw new Exception("Unkonw action");
            }
        }


        private void CreateOrder(NSFOrderDTO inputDto)
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = ClogTagType;
            tags["method"] = "CreateOrder";
            tags["orderid"] = inputDto.orderid.ToString();

            HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();

            var orderPackInfo = heERPContext.OrderPacks.FirstOrDefault(x => x.carrierId == inputDto.carrierid);

            if (orderPackInfo == null)
            {
                CLSLogger.Error("NSFBackgroundJob根据包裹下单失败", $"orderpackno={inputDto.orderpackno}找不到订单！freightid={inputDto.freightid} carrierid={inputDto.carrierid}", tags);
            }

            NSFOrderInfoDTO orderInfo = new NSFOrderInfoDTO
            {
                sender_name = "", //y.sender_name
                sender_addr = "", //y.sender_addr
                sender_phone = "", //y.sender_phone
                rec_name = orderPackInfo.rec_name,
                rec_addr = orderPackInfo.rec_addr,
                idno = orderPackInfo.idNo,
                rec_phone = orderPackInfo.rec_phone
            };

            if (orderInfo == null)
            {
                throw new UserFriendlyException("Can't find any order infos");
            }

            //通过 oid 取 orderdetail
            List<NSFOrderDetailDTO> orderDetails = heERPContext.OrderPackDetails.Where(x => x.opid == inputDto.opid).Select(y => new NSFOrderDetailDTO
            {
                product_name = y.sku,
                Number = y.qty,
                Weight = y.weight.HasValue ? y.weight.Value : 0,
                flag = y.flag
            }).ToList();

            if (orderDetails.IsNullOrEmpty())
            {
                throw new UserFriendlyException("Can't find any order details");
            }

            inputDto.NSFOrderInfo = orderInfo;
            inputDto.listOrderDetail = orderDetails;
            NSFCarrierResultDTO result = NSFProxy.CreateOrder(inputDto);

            if (result.IsSuccessful)
            {
                orderPackInfo.sycNSFFlag = 1;
                CLSLogger.Info("NSFBackgroundJob根据包裹下单成功", $"orderpackno={inputDto.orderpackno} freightid={inputDto.freightid} carrierid={inputDto.carrierid}", tags);
            }
            //订单Id已存在，无需再推
            else if (result.Code == -13)
            {
                orderPackInfo.sycNSFFlag = 1;
                CLSLogger.Info("NSFBackgroundJob根据包裹下单无效，订单Id已存在，无需再推", $"orderpackno={inputDto.orderpackno} freightid={inputDto.freightid} carrierid={inputDto.carrierid}", tags);
            }
            heERPContext.SaveChangesAsync();
        }

    }
}
