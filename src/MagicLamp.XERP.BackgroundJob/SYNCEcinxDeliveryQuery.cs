﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Runtime.Caching;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Proxy.ECinx;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using MagicLamp.PMS.Proxy.ECinx.Models;
using MagicLamp.PMS.Entities;
using TimeZoneConverter;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Infrastructure.Extensions;



namespace MagicLamp.XERP.BackgroundJob
{
    public class SYNCEcinxDeliveryQuery
    {
        private const string ClogTagType = "SYNCEcinxDeliveryQuery";
        Dictionary<string, string> clogTags = new Dictionary<string, string>
        {
            ["type"] = ClogTagType
        };
        private PMSDbContext _pmsContext;
        private HEERPDbContext _heerpContext;
        private TimeZoneInfo timeZoneNZ = TZConvert.GetTimeZoneInfo("Pacific/Auckland");
        private SendNotificationToDingDingJob _dingdingNotificationJob;

        EcinxConfigerData config = EcinxConfigerData.Configuration;
        public SYNCEcinxDeliveryQuery(
           IBackgroundJobManager backgroundJobManager
       )
        {
            _pmsContext = new PMSDbContextFactory().CreateDbContext();
            _heerpContext = new HEERPDbContextFactory().CreateDbContext();
            ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
            _dingdingNotificationJob = new SendNotificationToDingDingJob();
        }

        public async Task Execute()
        {
            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();

            config.MFD.ExportMapping.Values.Select(e => e.ExportFlag).ToList();

            var mergExportFlagsedList = config.MFD.ExportMapping.Values
                .Select(e => e.ExportFlag)
                .Concat(config.WHYC.ExportMapping.Values.Select(e => e.ExportFlag))
                .ToList();
           
            // 0: Created Ecinx Success
            // 1: Ordered Ecinx Success
            // 2: Ordered Ecinx Failed
            // 3: order out
            // 4： Ecinx 已配货
            // -1：已删除 （Ecinx可以恢复）
            // -2: 已取消 （Ecinx不可恢复）
            var vaildstatus = new List<int> { 1, 2, 4 };
          

            try
            {
                List<EcinxPackNo>  _ecinxorderpack = _pmsContext.EcinxOrdersPackTrades.Where(x => vaildstatus.Contains(x.Status) 
                && x.CreateTime>=DateTime.Now.AddYears(-1)
                && mergExportFlagsedList.Contains((int)x.ExportFlag)
                ).Select(x => new EcinxPackNo
                {
                    ExportFlag = x.ExportFlag.GetValueOrDefault(),
                    OrderPackNo = x.OrderPackNo
                }).ToList();


                if (_ecinxorderpack.Count() == 0)
                {
                    return;
                }


                int pageSize = 100; // 每页的记录数
               

                int totalpage = (int)Math.Ceiling((double)_ecinxorderpack.Count / pageSize);

                for (int i = 0; i < totalpage; i++)
                {
                    List<SyncEcinxStockDTO> dtos = new List<SyncEcinxStockDTO>();

                    var currentPageItems = _ecinxorderpack.Skip(i * pageSize).Take(pageSize).ToList();

                    if (currentPageItems.Count == 0)
                    {
                        // 如果当前页没有记录，说明已经遍历完所有记录，可以退出循环
                        break;
                    }
                    CLSLogger.Info("Ecinx全量同步信息", $"i：{i} / {totalpage} detail: {currentPageItems.ToJsonString()} ", clogTags);

                    await SyncEcinxDelivery(currentPageItems);

                }

            }
            catch (Exception ex)
            {
                string MessageContent = $"Ecinx出库状态查询失败: 返回值:{ex.ToJsonString()}";
                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx出库状态查询失败(定时查询任务)", MessageContent);
                CLSLogger.Info("Ecinx出库查询", $" 错误：{ex.ToJsonString()} ", clogTags);
                _pmsContext.Dispose();
                //throw;
            }
            finally
            {
                _pmsContext.Dispose();
            }

               
            }


        public async Task SyncEcinxDelivery(List<EcinxPackNo> ecinxorderpack)
        {
            EcinxDeliveryResponse queryResponse = new EcinxDeliveryResponse();

            foreach (var op in ecinxorderpack)
            {


                EcinxClientV2 ecinxv2 = new EcinxClientV2();
                queryResponse = ecinxv2.QueryDeliveryV2(op.ExportFlag, new EcinxDeliveryInput { declareOrderCode = op.OrderPackNo }).Result;


                if (queryResponse == null)
                {
                    continue;
                }

                CLSLogger.Info("Ecinx出库查询包裹Result", op.OrderPackNo + " " + queryResponse.ToJsonString(), clogTags);

                if (queryResponse.result.total > 0)
                {
                    //try
                    //{
                    int delivered = queryResponse.result.rows[0].delivered; //是否已发货(0:已发货 1:未发货)

                    if (delivered == 1)
                    {
                        continue;
                    }

                    var orderpackno = queryResponse.result.rows[0].declareOrderCode;
                    var logisticCode = queryResponse.result.rows[0].logisticCode;
                    var expressCode = queryResponse.result.rows[0].expressCode;


                    OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x => x.orderPackNo == orderpackno);
                    if (orderPack == null)
                    {
                        continue;
                    }

                    if (string.IsNullOrWhiteSpace(logisticCode))
                    {
                        continue;
                    }

                    var _ecinxorder = _pmsContext.EcinxOrdersPackTrades.Where(x => x.Opid == orderPack.opID).Select(x => new
                    {
                        x.CustomsPaymentData,
                        x.CreateTime,
                        x.OriginalInfo,
                        x.Status,
                        x.EcinxResponse,
                        x.PayTime
                    }).ToList();

                    EcinxOrdersPackTradeEntity EcinxorderPackTrade = new EcinxOrdersPackTradeEntity();

                    EcinxorderPackTrade.Opid = orderPack.opID;
                    EcinxorderPackTrade.OrderID = orderPack.orderID;
                    EcinxorderPackTrade.OrderPackNo = orderPack.orderPackNo;
                    EcinxorderPackTrade.CustomsPaymentData = _ecinxorder.Count == 0 ? "" : _ecinxorder.FirstOrDefault().CustomsPaymentData;
                    EcinxorderPackTrade.CreateTime = _ecinxorder.Count == 0 ? DateTime.UtcNow.ConvertUTCTimeToNZTime() : _ecinxorder.FirstOrDefault().CreateTime;
                    EcinxorderPackTrade.ExportFlag = orderPack.exportFlag;
                    EcinxorderPackTrade.OriginalInfo = _ecinxorder.Count == 0 ? "" : _ecinxorder.FirstOrDefault().OriginalInfo;
                    // 0: Created Ecinx Success 1: Ordered  Ecinx Success 2: Ordered  Ecinx Failed; 3 order out
                    EcinxorderPackTrade.Status = delivered == 0 ? 3 : _ecinxorder.FirstOrDefault().Status;
                    EcinxorderPackTrade.EcinxResponse = _ecinxorder.Count == 0 ? "" : queryResponse.ToJsonString();
                    EcinxorderPackTrade.DeliveryCode = logisticCode; // 查询发货单号，保存发货单号;
                    EcinxorderPackTrade.PayTime = _ecinxorder.FirstOrDefault().PayTime;
                    _pmsContext.EcinxOrdersPackTrades.Update(EcinxorderPackTrade);
                    _pmsContext.SaveChanges();


                    //var orderpackstatus = new List<int> {0,3 };
                    // 已包包裹变-出库状态
                    if ((orderPack.status == 3 || orderPack.status == 1 || orderPack.status == 0) && delivered == 0)
                    {
                        orderPack.status = 4;
                        //orderPack.carrierId = logisticCode;
                        //orderPack.freightId = expressCode;
                        orderPack.bonded_warehouse_carrier_id = logisticCode;
                        orderPack.bonded_warehouse_carrier_company = expressCode;

                        //如果是真直邮，没有一程面单，用第三方物流覆盖为空的一程面单
                        //如果是假直邮，保留一程面单，不要覆盖原面单
                        if (orderPack.carrierId.IsNullOrEmpty())
                        {
                            orderPack.carrierId = logisticCode;
                            orderPack.freightId = expressCode;
                        }
                        orderPack.out_time = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                        _heerpContext.OrderPacks.Update(orderPack);
                        _heerpContext.SaveChanges();
                    }
                }

                // wait for 2 seconds due to yiwu api throttling
                await Task.Delay(2000);
            }

        }

        public class EcinxPackNo
        { 
            public int ExportFlag { set; get; }
            public string OrderPackNo { set; get; }
        }


    }


    }
