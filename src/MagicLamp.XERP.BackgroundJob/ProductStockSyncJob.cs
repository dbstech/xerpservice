﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Cmq_SDK;
using Hangfire;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Newtonsoft.Json.Linq;
using MagicLamp.PMS.Entities;
using Abp.UI;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.DTOs.Flux;
using MagicLamp.PMS.DTOs.ThirdPart;

namespace MagicLamp.XERP.BackgroundJob
{
    public class ProductStockSyncJob : CommonBackgroundJob<ProductStockMqDto>
    {

        public ProductStockSyncJob()
        {


        }

        public override void Process(ProductStockMqDto dto)
        {

            switch (dto.Type)
            {
                case "SyncStockToSalesChannel":
                    SyncStockToSalesChannel(dto);
                    break;
                default:
                    break;
            }

        }


        /// <summary>
        /// 同步虚拟库存更新更各销售渠道
        /// </summary>
        /// <param name="dto"></param>
        public void SyncStockToSalesChannel(ProductStockMqDto dto)
        {
            if (dto == null || dto.Data.IsNullOrEmpty())
            {
                return;
            }

            List<string> skus = dto.Data.Split(",", StringSplitOptions.RemoveEmptyEntries).ToList();

            IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
            backgroundJobManager.Enqueue<SyncAladdinStockJob, List<string>>(skus, BackgroundJobPriority.High);
        }




    }
}
