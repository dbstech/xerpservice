using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Proxy.Odoo;

namespace MagicLamp.XERP.BackgroundJob
{
    public class UpdateOdooProductCost : AsyncBackgroundJob<string>, ITransientDependency
    {
        private IRepository<ExchangeRate, string> _exchangeRateRepository;
        private IRepository<ProductEntity, string> _productRepository;
        private IUnitOfWorkManager _unitOfWorkManager;
        public UpdateOdooProductCost(
            IRepository<ExchangeRate, string> exchangeRateRepository,
            IRepository<ProductEntity, string> productRepository,
            IUnitOfWorkManager unitOfWorkManager
        )
        {
            _exchangeRateRepository = exchangeRateRepository;
            _productRepository = productRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }
        protected override async Task ExecuteAsync(string args)
        {
            OdooProxy odooProxy = new OdooProxy();

            try
            {
                await odooProxy.LoginToOdoo();
            }
            catch (Exception ex)
            {
                CLSLogger.ErrorWithSentry("同步Odoo产品价格,方法:UpdateOdooProductCost录发生异常", ex);
                throw ex;
            }

            await odooProxy.LoginToOdoo();
            var odooCategories = await odooProxy.GetAllCategories();
            List<ProductEntity> productsToUpdate = new List<ProductEntity> { };
            Dictionary<string, decimal> exchangeRates = new Dictionary<string, decimal> { };

            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                exchangeRates = _exchangeRateRepository
                    .GetAll()
                    .Where(x => x.To == "NZD")
                    .ToDictionary(x => x.From, x => x.Rate);

                productsToUpdate = _productRepository
                    .GetAll()
                    .Where(x => x.ActiveFlag == true)
                    .Where(x => x.SyncToOdoo == true)
                    .Where(x => x.PurchasePeriodCode == "virtualStock")
                    .Where(x => x.Currency != "NZD")
                    .Where(x => x.Cost.HasValue && x.Cost > 0)
                    .Where(x => x.OdooSyncStatus == OdooSyncStatusEnum.Success.ToString())
                    .ToList();

                unitOfWork.Complete();
            }

            if (productsToUpdate.Count == 0)
            {
                return;
            }

            //productsToUpdate = productsToUpdate.Where(product =>
            //{
            //    var odooCategory = odooCategories.FirstOrDefault(category => category["id"].ToObject<long>() == product.OdooCategoryID);
            //    string categoryCompleteName = odooCategory["complete_name"].ToString();
            //    return categoryCompleteName.Contains("DF") || categoryCompleteName.Contains("FO");
            //}).ToList();

            productsToUpdate = productsToUpdate.Where(product =>
            {
                var odooCategory = odooCategories.FirstOrDefault(category => category["id"].ToObject<long>() == product.OdooCategoryID);
                if (odooCategory == null)
                {
                    return false;
                }
                string categoryCompleteName = odooCategory["complete_name"].ToString();
                return categoryCompleteName.Contains("DF") || categoryCompleteName.Contains("FO");
            }).ToList();


            long odooProductId;
            foreach (var product in productsToUpdate)
            {
                odooProductId = product.OdooProductID ?? 0;
                decimal standardPrice = (product.Cost ?? 0) * exchangeRates[product.Currency];
                if (odooProductId == 0 || standardPrice == 0)
                {
                    continue;
                }
                var odooProduct = new Product
                {
                    standard_price = standardPrice,
                    taxes_id = new object[1] { new object[3] { 6, 0, new int[] { 3, 12 } } },
                    supplier_taxes_id = new object[1] { new object[3] { 6, 0, new int[] { 6, 15 } } }
                };

                await odooProxy.UpdateProduct(
                    odooProductId,
                    odooProduct,
                    new OptionalParameters
                    {
                        force_company = 1
                    }
                );
                await odooProxy.UpdateProduct(
                    odooProductId,
                    odooProduct,
                    new OptionalParameters
                    {
                        force_company = 2
                    }
                );
            }
        }

    }
}