﻿using MagicLamp.PMS.DTOs.ThirdPart;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.XERP.BackgroundJob
{
    public class SyncCreateOrderResultFromHH : CommonBackgroundJob<HHCreateOrderResultDTO>
    {
        public override void Process(HHCreateOrderResultDTO input)
        {
            PushOrderToFluxJob pushJob = new PushOrderToFluxJob();
            pushJob.ProcessCreateOrderAsyncResultFromHH(input);
        }
    }
}
