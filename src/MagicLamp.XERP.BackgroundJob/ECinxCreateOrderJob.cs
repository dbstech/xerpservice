﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Runtime.Caching;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Proxy.ECinx.Models;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Text;
using TimeZoneConverter;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Entities;
using System.Linq;
using MagicLamp.PMS.Proxy.ECinx;
using MagicLamp.PMS.DTOs.Enums;
using System.Threading.Tasks;
using MagicLamp.PMS.Domain;
using Abp.AutoMapper;
using MagicLamp.PMS.Infrastructure;
using static Org.BouncyCastle.Math.EC.ECCurve;

namespace MagicLamp.XERP.BackgroundJob
{
    public  class ECinxCreateOrderJob : CommonBackgroundJob<EcinxOrderInput>
    {
        private PMSDbContext _pmsContext;
        private HEERPDbContext _heerpContext;
        private SendNotificationToDingDingJob _dingdingNotificationJob;
        private TimeZoneInfo timeZoneNZ = TZConvert.GetTimeZoneInfo("Pacific/Auckland");
        private TimeZoneInfo timeZoneCN = TZConvert.GetTimeZoneInfo("Asia/Shanghai");
        
        private List<EcinxConfig> _config;

        public ECinxCreateOrderJob(
         IBackgroundJobManager backgroundJobManager
         )
        {
            _pmsContext = new PMSDbContextFactory().CreateDbContext();
            _heerpContext = new HEERPDbContextFactory().CreateDbContext();
            ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
            _config = Ultities.GetConfig<List<EcinxConfig>>();
            _dingdingNotificationJob = new SendNotificationToDingDingJob();
        }

        public override void Process(EcinxOrderInput inputDto)
        {
            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();

            OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x =>
                    x.orderPackNo == inputDto.platformOrderCode || x.orderID == inputDto.platformOrderCode);
            if (orderPack == null)
            {
                //string message = $"Ecinx仓创建订单失败, 未找到此包裹：{inputDto.platformOrderCode} ";
                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx 创建订单失败", message);
                //CLSLogger.Info("Ecinx仓创建订单失败", $"未找到此包裹：{inputDto.platformOrderCode} ", LogTags);
                //hangfire retry
                throw new Exception($"义乌保税仓下单时无orderpack,将重试 订单号：{inputDto.platformOrderCode} ");
            }
            int differenceInDays = 0;
            DateTime oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
            if (!string.IsNullOrWhiteSpace(orderPack.opt_time.ToString()))
            {
                oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(orderPack.opt_time.ToString()), timeZoneNZ);
                DateTime newDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                TimeSpan ts = newDate - oldDate;
                differenceInDays = ts.Days;
            }


            LogTags["method"] = "ECinxCreateOrder";
            LogTags["orderpackno"] = inputDto.platformOrderCode; //orderpacno
            var originalInputString = inputDto.ToJsonString();

            string MessageContent = "**消息类型**: 即时推送Ecinx订单 \n\n";

            //对象没有产品，直接退出
            if (inputDto.details.Count == 0)
            {
                 MessageContent = MessageContent + $"Ecinx创建订单失败, 包裹：{inputDto.platformOrderCode}, 数据：{originalInputString} ";
                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx 创建订单失败", MessageContent);
                CLSLogger.Info("Ecinx仓创建订单失败", $"包裹号：{inputDto.platformOrderCode} ", LogTags);

                ecinpushorderLog.OrderPackNo = inputDto.platformOrderCode;
                ecinpushorderLog.WarningLevel = 10;  //WarningLevel = 0: 普通告警; WarningLevel =10: 需要客服处理;
                ecinpushorderLog.LackStockDays = differenceInDays;
                ecinpushorderLog.MessageType = "即时推送Ecinx订单";
                ecinpushorderLog.FaildReason = "订单未包含产品信息";
                ecinpushorderLog.MessageDetail = "";
                _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                _pmsContext.SaveChanges();

                return;
            }

            try
            {
                if (orderPack.freezeFlag > 0)
                {
                    string message = $"ECinx下单时发现订单已取消, 包裹已冻结：{ orderPack.orderPackNo} freezeflag: { orderPack.freezeFlag} { orderPack.freezeComment} ";
                    _dingdingNotificationJob.SendEcinxERPAlert("Ecinx 创建订单失败", message);
                    CLSLogger.Info("ECinx下单时发现订单已取消", $"包裹已冻结：{orderPack.orderPackNo}  freezeflag: {orderPack.freezeFlag}  {orderPack.freezeComment}", LogTags);

                    ecinpushorderLog.OrderPackNo = inputDto.platformOrderCode;
                    ecinpushorderLog.WarningLevel = 10;  //WarningLevel = 0: 普通告警; WarningLevel =10: 需要客服处理;
                    ecinpushorderLog.LackStockDays = differenceInDays;
                    ecinpushorderLog.MessageType = "即时推送Ecinx订单";
                    ecinpushorderLog.FaildReason = "包裹已冻结";
                    ecinpushorderLog.MessageDetail = orderPack.freezeComment;
                    _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                    _pmsContext.SaveChanges();

                    return;
                }

                if (orderPack.status !=0 )
                {
                    MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {orderPack.orderPackNo}  \n\n";
                    MessageContent = MessageContent + $" ##### **失败原因**:  ECinx下单时发现订单已包 \n\n";
                    //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx 创建订单失败，", MessageContent);
                    CLSLogger.Info("ECinx下单时发现订单已包", $"包裹已包：{orderPack.orderPackNo}", LogTags);
                    //ecinpushorderLog.OrderPackNo = inputDto.platformOrderCode;
                    //ecinpushorderLog.WarningLevel = 10;  //WarningLevel = 0: 普通告警; WarningLevel =10: 需要客服处理;
                    //ecinpushorderLog.LackStockDays = differenceInDays;
                    //ecinpushorderLog.MessageType = "即时推送Ecinx订单";
                    //ecinpushorderLog.FaildReason = "包裹已包";
                    //ecinpushorderLog.MessageDetail = "";
                    //_pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                    //_pmsContext.SaveChanges();
                    return;
                }
               


                bool EcinxorderPackTradeExist = _pmsContext.EcinxOrdersPackTrades.Any(x => x.Opid == orderPack.opID);

                EcinxOrdersPackTradeEntity EcinxorderPackTrade = new EcinxOrdersPackTradeEntity();
                if (EcinxorderPackTradeExist)
                {
                    EcinxorderPackTrade.Opid = orderPack.opID;
                    EcinxorderPackTrade.OrderID = orderPack.orderID;
                    EcinxorderPackTrade.OrderPackNo = !string.IsNullOrWhiteSpace(orderPack.orderPackNo) ? orderPack.orderPackNo: orderPack.orderID; // orderpackno is nul then use orderid
                    EcinxorderPackTrade.CustomsPaymentData = inputDto.customsPaymentData == null ? "" :  inputDto.customsPaymentData.ToJsonString();
                    EcinxorderPackTrade.CreateTime = DateTime.UtcNow;
                    EcinxorderPackTrade.Status = 0; 
                                                    // 0: Created Ecinx Success 
                                                    // 1: Ordered Ecinx Success
                                                    // 2: Ordered Ecinx Failed
                                                    // 3: order out
                                                    // 4: 已配货
                                                    // -1: 已删除
                    EcinxorderPackTrade.ExportFlag = orderPack.exportFlag;
                    EcinxorderPackTrade.OriginalInfo = originalInputString;
                    EcinxorderPackTrade.PayTime = DateTime.Parse(inputDto.orderTime);// TimeZoneInfo.ConvertTimeFromUtc(, timeZoneCN);
                    //!string.IsNullOrWhiteSpace(inputDto.payments[0].payTime) ? DateTime.Parse(inputDto.payments[0].payTime) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneCN); 
                    _pmsContext.EcinxOrdersPackTrades.Update(EcinxorderPackTrade);
                    _pmsContext.SaveChanges();
                }
                else
                {
                    EcinxorderPackTrade = new EcinxOrdersPackTradeEntity
                    {
                        Opid = orderPack.opID,
                        OrderID = orderPack.orderID,
                        OrderPackNo = !string.IsNullOrWhiteSpace(orderPack.orderPackNo) ? orderPack.orderPackNo : orderPack.orderID, // orderpackno is nul then use orderid
                        CustomsPaymentData = inputDto.customsPaymentData ==  null ? "" : inputDto.customsPaymentData.ToJsonString(),
                        CreateTime = DateTime.UtcNow,
                        OriginalInfo = originalInputString,
                        Status = 0,  // 0: Created Ecinx Success 1: Ordered  Ecinx Success 2: Ordered  Ecinx Failed 3: order out ; 4: packaged; -1: deleted
                        ExportFlag = orderPack.exportFlag,
                        PayTime =TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneCN)
                    };
                    _pmsContext.EcinxOrdersPackTrades.Add(EcinxorderPackTrade);
                    _pmsContext.SaveChanges();
                }

               
                MessageContent = MessageContent + $" ##### **包裹号**:  **{orderPack.orderPackNo}** \n\n";

                /***
                    84	YW奶粉极速仓
                    85	YW奶粉普通线
                    88	YW保健品专线
                
                    1. 目的地  上海
                    2. erp包裹重量大于 1.5kg
                    3. hold单时间 早于11月15日的
                    都hold住不处理  
                    直到 11月15日后 统一推进ecinx

                    Austin 25/10/2024
                 * **/
                //if (orderPack.rec_addr.StartsWith("上海") && orderPack.weightVal > 1.50m
                //    && new[] { 84, 85, 88 }.Contains((int)orderPack.exportFlag))
                //{

                //    // 停止推送
                //    DateTime start = Convert.ToDateTime("2024-10-21 16:00:00"); //UTC时间 -> 北京时间: 2022-07-08 18:00:00
                //    DateTime end = Convert.ToDateTime("2024-11-14 16:00:00"); //UTC时间 -> 北京时间: 2022-02-07 00:00:00
                //    if (DateTime.UtcNow >= start && DateTime.UtcNow <= end)
                //    {
                //        MessageContent = "**消息类型**: **_<font face=华云彩绘 color=#FF0000>延时服务</font>_** \n\n";
                //        MessageContent = MessageContent + "##### **原因**: 上海世博会要求,停止推送Ecinx系统,所有包裹暂缓推送,(服务停止时间:北京时间 10月22日 00:00 ~  11月15日 00:00) \n\n \n\n";
                //        MessageContent = MessageContent + $" ##### **包裹号**:  **{orderPack.orderPackNo}** 包裹收件地址: {orderPack.rec_addr } \n\n";
                //        MessageContent = MessageContent + $" ##### **包裹总权重**:  **{orderPack.weightVal}**  \n\n";
                //        MessageContent = MessageContent + $" ##### **渠道代码**:  **{orderPack.exportFlag}**  \n\n";
                //        _dingdingNotificationJob.SendEcinxERPAlert("魔法灯-停止推送Ecinx系统", MessageContent);
                //        //CLSLogger.Info("义乌保税仓(暂停服务 9月3日 周五中午12点-9月6日 周五中午12),返回值为空:", $"包裹号：{orderPack.orderPackNo}  inputDto: {inputDto.ToJsonString()} listProductsFromOrder: {listProductsFromOrder.ToJsonString()} listProductInfo: {listProductInfo.ToJsonString()}", LogTags);
                //        return;
                //    }
                //}

                string warehousecode = "";

                //判断是否使用供销功能 2025.02.19
                //渠道ID
                int exportFlag = (int)orderPack.exportFlag;
                EcinxConfigerData config = EcinxConfigerData.Configuration;
                //是否使用供销功能
                bool supplySales = config.supplySalesExportFlag.Contains(exportFlag);
                ExportResponse _Ecinxconfig = config.GetExportByFlag(exportFlag);
                warehousecode = _Ecinxconfig.Export.WarehouseCode;
                // 生成成功通过回调函数获取真实状态
                EcinxResponse result = null;

                EcinxOrder eorder = new EcinxOrder();

                eorder.deduction = inputDto.deduction;
                eorder.discount = inputDto.discount;
                eorder.freight = inputDto.freight;
                eorder.orderTime = inputDto.orderTime;
                eorder.platformOrderCode = inputDto.platformOrderCode;
                eorder.declareOrderCode = inputDto.declareOrderCode;


                //// 阿狸： CQ奶粉普通线 82,106 渠道，指定Ecinx仓库，其它不指定Ecinx 仓库  2024.11.22
                List<int> mfdyc_export = new List<int> { 82, 106 };//82 MFD //106 YC
                eorder.warehouseCode = mfdyc_export.Contains(orderPack.exportFlag ?? 0) ? warehousecode : "";

                eorder.receiver = inputDto.receiver;
                eorder.receiverTel = inputDto.receiverTel;
                eorder.receiverProvince = inputDto.receiverProvince;
                eorder.receiverCity = inputDto.receiverCity;
                eorder.receiverArea = inputDto.receiverArea;
                eorder.receiverAddress = inputDto.receiverAddress;
                eorder.tax = inputDto.tax;
                eorder.type = inputDto.type;


                eorder.details = inputDto.details;
                eorder.customsPlatformCode = inputDto.customsPlatformCode;
                eorder.customsPlatformName = inputDto.customsPlatformName;

                eorder.purchaser = inputDto.purchaser;
                eorder.purchaserCardNo = inputDto.purchaserCardNo;
                eorder.purchaserCardType = inputDto.purchaserCardType;
                eorder.purchaserTel = inputDto.purchaserTel;
                //提交供销代码
                if (supplySales)
                {
                    eorder.providerCode = _Ecinxconfig.Export.providerCode;
                    eorder.type = 2;
                }
                
                eorder.payments = inputDto.payments;
                eorder.expressSheetInfo = inputDto.expressSheetInfo;
                eorder.presell = inputDto.presell;
                eorder.planDeliveryTime = inputDto.planDeliveryTime;

                eorder.purchaserCardFrontPic = inputDto.GetType().GetProperty("purchaserCardFrontPic") != null ? inputDto.purchaserCardFrontPic : "";
                eorder.purchaserCardBackPic = inputDto.GetType().GetProperty("purchaserCardBackPic") != null ? inputDto.purchaserCardBackPic : "";

                eorder.ExportFlag = orderPack.exportFlag;


                EcinxClientV2 ecinxv2 = new EcinxClientV2();
                result =  ecinxv2.CreateOrderV2(orderPack.exportFlag ?? 0, eorder).Result;
               

                if (!result.success)
                {
                    MessageContent = MessageContent + $" ##### **创建订单失败原因**:  **_<font face=华云彩绘 color=#FF0000> {result.errorMsg} </font>_** \n\n";
                    _dingdingNotificationJob.SendEcinxERPAlert("Ecinx 创建订单失败", MessageContent);
                    CLSLogger.Info("ECinx下单失败", $"包裹号：{orderPack.orderPackNo}  inputDto: {originalInputString} response: {result.ToJsonString()} ", LogTags);

                    ecinpushorderLog.OrderPackNo = inputDto.platformOrderCode;
                    ecinpushorderLog.WarningLevel = 10;  //WarningLevel = 0: 普通告警; WarningLevel =10: 需要客服处理;
                    ecinpushorderLog.LackStockDays = differenceInDays;
                    ecinpushorderLog.MessageType = "即时推送Ecinx订单";
                    ecinpushorderLog.FaildReason = result.errorMsg;
                    ecinpushorderLog.MessageDetail = "";
                    _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                    _pmsContext.SaveChanges();

                    return;
                }


            }
            catch (Exception ex)
            {
                CLSLogger.Error("Ecinx下单发生异常 Exception ", "参数："+ originalInputString +"异常："+ex, LogTags);

                ecinpushorderLog.OrderPackNo = inputDto.platformOrderCode;
                ecinpushorderLog.WarningLevel = 10;  //WarningLevel = 0: 普通告警; WarningLevel =10: 需要客服处理;
                ecinpushorderLog.LackStockDays = differenceInDays;
                ecinpushorderLog.MessageType = "即时推送Ecinx订单";
                ecinpushorderLog.FaildReason = "Exception: " + ex.ToJsonString();
                ecinpushorderLog.MessageDetail = "";
                _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                _pmsContext.SaveChanges();

                throw new Exception($"Ecinx查询接口失败，订单号：{ex.ToJsonString()}");
            }
        }
      
    }
}
