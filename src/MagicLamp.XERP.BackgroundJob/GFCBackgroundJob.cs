﻿using System;
using System.Collections.Generic;
using System.Text;
using MagicLamp.XERP.BackgroundJob.Common;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Infrastructure.Extensions;

namespace MagicLamp.XERP.BackgroundJob
{
    public class GFCBackgroundJob : CommonBackgroundJob<GFCDataMqDto>
    {
        public override void Process(GFCDataMqDto inputDto)
        {
            switch (inputDto.Action)
            {
                case GFCBackgroundJobActionEnum.Post:
                    Post(inputDto);
                    break;
                default:
                    throw new Exception("Unkonw action");
            }
            throw new NotImplementedException();
        }

        public void Post(GFCDataMqDto inputDto)
        {
            string data = inputDto.ToJsonString();
            string response = GFCProxy.Post(data);
        }
    }
}
