﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abp.BackgroundJobs;
using Abp.Dependency;
using MagicLamp.PMS;
using MagicLamp.PMS.Domain;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Common;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using Newtonsoft.Json.Linq;
using TimeZoneConverter;

namespace MagicLamp.XERP.BackgroundJob
{
    public class RefreshProductInfoCacheJob : CommonBackgroundJob<List<string>>
    {


        [SkipConcurrentExecution(60 * 120)]
        public override void Process(List<string> skus)
        {
            StringBuilder log = new StringBuilder();

            try
            {
                PMSDbContext pMSDbContext = new PMSDbContextFactory().CreateDbContext();
                ProductDomainService productDomainService = new ProductDomainService();

                if (skus.IsNullOrEmpty())
                {
                    int totalCount = pMSDbContext.Products.Count();
                    int pageSize = 1000;
                    int totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;

                    log.Append($"totalCount: {totalCount}, totalPage: {totalPage}");

                    for (int i = 0; i < totalPage; i++)
                    {
                        var pageSkus = pMSDbContext.Products.Skip(i * pageSize).Take(pageSize).Select(x => x.SKU).ToList();
                        productDomainService.UpdateProductsCache(pageSkus);

                        log.Append($"completed page: {i+1}, {DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")};");
                    }

                }
                else
                {
                    LogTags["sku"] = string.Join(";", skus);
                    productDomainService.UpdateProductsCache(skus);
                }

            }
            catch (Exception ex)
            {
                CLSLogger.Error("RefreshProductInfoCacheJobException", ex, LogTags);
            }
            finally
            {
                CLSLogger.Info("RefreshProductInfoCacheJobExcuted", log.ToString(), LogTags);
            }

        }





    }
}
