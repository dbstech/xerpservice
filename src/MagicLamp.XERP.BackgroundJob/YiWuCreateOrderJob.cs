﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using Cmq_SDK;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy;
using System;
using System.Collections.Generic;
using MagicLamp.PMS.Infrastructure.Extensions;
using System.Linq;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using MagicLamp.XERP.BackgroundJob;
using Microsoft.AspNetCore.Http;
using MagicLamp.PMS.Infrastructure;
using System.IO;
using System.Text;
using MagicLamp.Flux.API.Models;
using Abp.Runtime.Validation;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Linq;
using Abp.Domain.Uow;
using MagicLamp.PMS.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MagicLamp.PMS.Enums;
using MagicLamp.PMS.Entities.DBQueryTypes;
using OdooRpc.CoreCLR.Client.Models;
using OdooRpc.CoreCLR.Client.Models.Parameters;
using System.Data.SqlClient;
using MagicLamp.PMS.EntityFrameworkCore;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.Proxy.Odoo;
using MagicLamp.PMS.ExchangeRates;
using Abp.Linq.Extensions;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using Abp.Extensions;
using MagicLamp.PMS.YiWuBondedWarehouse;
using TimeZoneConverter;
using Abp.Runtime.Caching;
using Abp.Dependency;
using MagicLamp.XERP.BackgroundJob.Common;
using System.Text.RegularExpressions;
using Cmq_SDK.Cmq;

namespace MagicLamp.XERP.BackgroundJob
{
    public class YiWuCreateOrderJob : CommonBackgroundJob<CreateSupplierOrderInput>
    {
        private IBackgroundJobManager _backgroundJobManager;
        private const string ClogTagType = "YiWuNewPushOrderbyCreateTimeDesc";

        private List<string> messageTags = new List<string> { "YiWuCreateOrderJob" };
        private string WarehouseName = "义乌保税仓";
        private TimeZoneInfo timeZoneNZ = TZConvert.GetTimeZoneInfo("Pacific/Auckland");
        private IRepository<OrderPackTradeEntity, int> _orderPackTradeRepository;

        private PMSDbContext _pmsContext;
        private HEERPDbContext _heerpContext;
        private SendNotificationToDingDingJob _dingdingNotificationJob;
        private YiWuBondedWarehouseClient _yiwuClient;
        private YiWuBondedWarehouseClient _yiDaTongClient;

       

        public YiWuCreateOrderJob(
            IBackgroundJobManager backgroundJobManager,
            IRepository<OrderPackTradeEntity, int> orderPackTradeRepository
            )
        {
            _backgroundJobManager = backgroundJobManager;

            _pmsContext = new PMSDbContextFactory().CreateDbContext();
            _heerpContext = new HEERPDbContextFactory().CreateDbContext();
            _dingdingNotificationJob = new SendNotificationToDingDingJob();

            ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
            _yiwuClient = new YiWuBondedWarehouseClient(cacheManager, YiWuAccountEnum.Default);
            _yiDaTongClient = new YiWuBondedWarehouseClient(cacheManager, YiWuAccountEnum.YiDaTong);

            _orderPackTradeRepository = orderPackTradeRepository;

        }

        public override void Process(CreateSupplierOrderInput inputDto)
        {

            //var yiwuExportFlags = new List<int> { 79, 84, 85, 88 }; // 义乌保税仓

            var inputOrder = inputDto.Orders.FirstOrDefault();
            var originalOrderCode = inputOrder.orderPackNo.IsNullOrWhiteSpace() ? inputOrder.orderCode : inputOrder.orderPackNo;

            LogTags["method"] = "CreateOrder";
            LogTags["orderpackno"] = originalOrderCode;
            var originalInputString = inputDto.ToJsonString();

            if (originalInputString.Length <= 120)
            {
                _dingdingNotificationJob.SendBondedWarehouseYiWuAlert("系统级错误", $"缺少推送 义乌保税仓 参数 {originalInputString} ");
                return;
            }

            try
            {
                CLSLogger.Info("义乌保税仓下单请求报文", inputDto.ToJsonString(), LogTags);

                //义乌仓保税订单
                OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x =>
                    inputOrder.orderPackNo.IsNullOrWhiteSpace()
                    ? x.orderID == inputOrder.orderCode
                    : x.orderPackNo == inputOrder.orderPackNo
                );


                bool cacheOrder = _heerpContext.CacheOrders.Any(x => x.orderid == inputOrder.orderCode);
                OrderEntity order = _heerpContext.Orders.FirstOrDefault(x => x.orderid == inputOrder.orderCode);

                if (orderPack == null)
                {

                    if (cacheOrder == false && order == null)
                    {
                        CLSLogger.Info("义乌保税仓推单时发现订单已取消", "cache order,orders and orderpacks all not found", LogTags);
                        return;
                    }

                    if (cacheOrder == true || order != null)
                    {
                        throw new Exception($"义乌保税仓下单时无orderpack，将重试。订单号：{inputOrder.orderCode}");
                    }
                }
                else
                {
                    if (orderPack.freezeFlag > 0)
                    {
                        CLSLogger.Info("保税仓推单时发现订单已取消", $"包裹已冻结：{orderPack.orderPackNo}  freezeflag: {orderPack.freezeFlag}  {orderPack.freezeComment}", LogTags);
                        return;
                    }

                    if (orderPack.status == 4)
                    {
                        CLSLogger.Info("保税仓推单时发现订单已出库", $"包裹号：{orderPack.orderPackNo}  ", LogTags);
                        return;
                    }

                    if (orderPack.status == 3)
                    {
                        CLSLogger.Info("义乌保税_强制推送已包订单", $"包裹号：{orderPack.orderPackNo}  ", LogTags);
                        //_dingdingNotificationJob.SendBondedWarehouseYiWuAlert("义乌保税_强制推送已包订单", $"订单为:{originalOrderCode}。");
                        //return;
                    }

                }

                bool orderPackTradeExists = _pmsContext.OrderPackTrades.Any(x => x.Opid == orderPack.opID);
                OrderPackTradeEntity orderPackTradeEntity;
                if (orderPackTradeExists)
                {
                    orderPackTradeEntity = _pmsContext.OrderPackTrades.FirstOrDefault(x => x.Opid == orderPack.opID);
                    orderPackTradeEntity.Opid = orderPack.opID;
                    orderPackTradeEntity.OrderID = orderPack.orderID;
                    orderPackTradeEntity.OrderPackNo = orderPack.orderPackNo;
                    orderPackTradeEntity.CustomsPaymentData = inputOrder.customsPaymentData.ToJsonString();
                    orderPackTradeEntity.CreateTime = DateTime.UtcNow;
                    orderPackTradeEntity.OriginalInfo = originalInputString;
                    _pmsContext.OrderPackTrades.Update(orderPackTradeEntity);

                }
                else
                {
                    orderPackTradeEntity = new OrderPackTradeEntity
                    {
                        Opid = orderPack.opID,
                        OrderID = orderPack.orderID,
                        OrderPackNo = orderPack.orderPackNo,
                        CustomsPaymentData = inputOrder.customsPaymentData.ToJsonString(),
                        CreateTime = DateTime.UtcNow,
                        OriginalInfo = originalInputString,
                        HasBeenOrdered = false
                    };
                    _pmsContext.OrderPackTrades.Add(orderPackTradeEntity);
                }
                _pmsContext.SaveChanges();

                //erp sku from this order
                var listProductsFromOrder = inputOrder.OrderDtls.Select(x => x.commodityCode).ToList();

                //get related product info
                List<ProductEntity> listProductInfo = _pmsContext.Products
                    .Where(x => listProductsFromOrder.Contains(x.BondedWarehouseItemCode) || listProductsFromOrder.Contains(x.SKU))
                    .ToList();

                //filter sku can't push order
                //var filtersku = new list<string> { "mkywstfaf1a", "mkywstfas1a", "mkywstfa8f1a", "mkywstfa8s1a", "mkywstfabf1a", "mkywstfabs1a"};
                //var forbiddenorderpack = listproductinfo.where(x => filtersku.contains(x.sku)).tolist();
                //if (forbiddenorderpack.count>0)
                //{
                //    _backgroundjobmanager.enqueue<yiwucreateorderjob, createsupplierorderinput>(inputdto, backgroundjobpriority.normal, timespan.fromdays(1));
                //    _dingdingnotificationjob.sendbondedwarehouseyiwualert("暂停推送服务", $"不推送此包裹，原因: {orderpack.orderpackno} 此包裹包含有问题的 安佳产品:{forbiddenorderpack.firstordefault().sku} ");
                //    //clslogger.info("义乌保税仓(暂停服务 9月3日 周五中午12点-9月6日 周五中午12),返回值为空:", $"包裹号：{orderpack.orderpackno}  inputdto: {inputdto.tojsonstring()} listproductsfromorder: {listproductsfromorder.tojsonstring()} listproductinfo: {listproductinfo.tojsonstring()}", logtags);
                //    return;
                //}


                string carrierId = string.Empty;

                string standwebsite = "kuaidi.nz";
                string fwswebsite = "fws.mofa.ht";


                if (order.store.Trim().IndexOf(standwebsite)>=0 || order.store.Trim().IndexOf(fwswebsite) >= 0)
                {
                    carrierId = orderPack.carrierId;
                }
                else
                {
                    //use the original carrierid as orderCode for YiWu
                    //yidatong orders can skip the fake order logic and use the carrier id in orderpack.
                    if (inputOrder.orderPackNo.IsNullOrWhiteSpace() && orderPack.exportFlag != 86 && order != null)
                    {
                        //var orderInfo = _heerpContext.Orders.FirstOrDefault(x => x.orderid == inputOrder.orderCode);
                        string orderPackNoToMatch = order.notes == null ? null : order.notes.Trim();
                        carrierId = GetCarrierIDFromLinkedOrderPack(orderPackNoToMatch, inputOrder.orderCode);
                    }
                    else
                    {
                        carrierId = orderPack.carrierId;
                    }
                }

                //before create order, swap erp sku to barcode in erp
                if (!SwapSKUForItemCode(inputDto, listProductInfo))
                {
                    return;
                }

                inputOrder.outOrderCode = carrierId;
                ResultList result = null;




                //// 停止推送
                //DateTime start = Convert.ToDateTime("2022-04-28 03:00:00"); //UTC时间 -> 北京时间: 2022-01-25 23:00:00
                //DateTime end = Convert.ToDateTime("2022-02-30 03:00:00"); //UTC时间 -> 北京时间: 2022-02-07 00:00:00

                //if (DateTime.UtcNow >= start && DateTime.UtcNow <= end)
                //{
                //    _backgroundJobManager.Enqueue<YiWuCreateOrderJob, CreateSupplierOrderInput>(inputDto, BackgroundJobPriority.Normal, TimeSpan.FromDays(1));
                //    _dingdingNotificationJob.SendBondedWarehouseYiWuAlert("魔法灯（停止推送保税仓），需要切换到新的Ecinx系统", $"北京时间 4月28日11:00 停推, 包裹 {orderPack.orderPackNo} 会缓存！");
                //    //CLSLogger.Info("义乌保税仓(暂停服务 9月3日 周五中午12点-9月6日 周五中午12),返回值为空:", $"包裹号：{orderPack.orderPackNo}  inputDto: {inputDto.ToJsonString()} listProductsFromOrder: {listProductsFromOrder.ToJsonString()} listProductInfo: {listProductInfo.ToJsonString()}", LogTags);
                //    return;
                //}

                //暂停推送
                //if (result == null)
                //{
                //    _backgroundJobManager.Enqueue<YiWuCreateOrderJob, CreateSupplierOrderInput>(inputDto, BackgroundJobPriority.Normal, TimeSpan.FromDays(1));
                //    _dingdingNotificationJob.SendBondedWarehouseYiWuAlert("暂停服务消息", $"义乌保税暂停推送:此包裹 {orderPack.orderPackNo} 会缓存！");
                //    //CLSLogger.Info("义乌保税仓(暂停服务 9月3日 周五中午12点-9月6日 周五中午12),返回值为空:", $"包裹号：{orderPack.orderPackNo}  inputDto: {inputDto.ToJsonString()} listProductsFromOrder: {listProductsFromOrder.ToJsonString()} listProductInfo: {listProductInfo.ToJsonString()}", LogTags);
                //    return;
                //}


                // exportFlag YDTYW保税
                if (orderPack.exportFlag == 86)
                {
                    result = _yiDaTongClient.CreateSupplierOrder(inputDto).Result;
                }
                else
                {
                    result = _yiwuClient.CreateSupplierOrder(inputDto).Result;
                }

                if (result == null)
                {
                    CLSLogger.Info("义乌保税仓下单完成,返回值为空:", $"包裹号：{orderPack.orderPackNo}  inputDto: {inputDto.ToJsonString()} listProductsFromOrder: {listProductsFromOrder.ToJsonString()} listProductInfo: {listProductInfo.ToJsonString()}", LogTags);
                    return;
                }

                bool isCreateOrderSuccessfully = false;
                bool orderExists = false;

                foreach (var r in result.resultList)
                {
                    if (r.resultCode != true)
                    {
                        if (r.resultMsg.Contains("订单已存在"))
                        {
                            orderExists = true;
                        }
                        else
                        {
                            string BondedWarehouseItemCode = "", sku = "", MFDSKU = "";

                            if (r.resultMsg.Contains("库存不足") || r.resultMsg.Contains("商品不存在"))
                            {
                                BondedWarehouseItemCode = Regex.Replace(r.resultMsg, @"[^A-Za-z0-9]+", "");

                                List<string> erpSkus = listProductInfo
                                                        .Where(s => s.BondedWarehouseItemCode == BondedWarehouseItemCode)
                                                        .Select(x => x.SKU.Trim()).ToList();

                                var orderPackDetail = _heerpContext.OrderPackDetails
                                                       .Where(x => erpSkus.Contains(x.sku) && x.opid == orderPack.opID)
                                                       .FirstOrDefault();

                                MFDSKU = orderPackDetail.sku;
                            }
                            sku = MFDSKU != "" ? " 魔法灯SKU: " + MFDSKU : "";
                            // 计算时间差
                            int differenceInDays = 0;

                            //订单库存不足;+1天重推送订单;
                            if (r.resultMsg.Contains("库存不足"))
                            {
                                DateTime oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                                if (!string.IsNullOrWhiteSpace(inputDto.Orders.FirstOrDefault().orderDate))
                                {
                                    oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(inputDto.Orders.FirstOrDefault().orderDate), timeZoneNZ);
                                    DateTime newDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                                    TimeSpan ts = newDate - oldDate;
                                    differenceInDays = ts.Days;
                                }
                            }

                            // Push CMQ to Teambition_task Topic
                            // TeambitionMessage teambitionmesage = new TeambitionMessage
                            // {
                            //     OrderNo = originalOrderCode, //订单号
                            //     Warehouse = WarehouseName,
                            //     Title = r.resultMsg,
                            //     TriggerTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"),
                            //     OutOfStockSKU = MFDSKU != "" ? MFDSKU : "",
                            //     MessageContent = $"义乌保税仓下单失败。订单为:{originalOrderCode}。失败原因：{r.resultMsg} {sku} "
                            // };
                            // TencentCMQProxy.PublishTopicMessage(TCConstants.CMQTopics.TOPIC_TEAMBITIONNOTIFICATION, teambitionmesage, messageTags, LogTags);

                            // Push DingDing Message
                            string MessageContent = $"<font color=#FF0000  face=黑体>告警内容: 缺货 </font> \n\n";
                            MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {orderPack.orderPackNo}  \n\n";
                            MessageContent = MessageContent + $" ##### **保税仓 SKU**:  {BondedWarehouseItemCode}  \n\n";
                            MessageContent = MessageContent + $" ##### **魔法灯 SKU**:  {MFDSKU}  \n\n";
                            if (differenceInDays >= 5)
                            {
                                MessageContent = MessageContent + $" ##### **缺货天数**:  **_<font face=华云彩绘 color=#FF0000> {differenceInDays} 天</font>_** \n\n";
                            }
                            else
                            {
                                MessageContent = MessageContent + $" ##### **缺货天数**:  **_<font face=华云彩绘 color=#228b22> {differenceInDays} 天</font>_** \n\n";
                            }
                            MessageContent = MessageContent + $" ##### **订单创建时间**:  { inputDto.Orders.FirstOrDefault().orderDate }  \n\n";
                            MessageContent = MessageContent + $" ##### **API返回消息**:  {r.resultMsg}  ";

                            _dingdingNotificationJob.SendBondedWarehouseYiWuAlert("义乌保税仓推单结果", MessageContent);
                        }
                    }
                    else
                    {
                        isCreateOrderSuccessfully = true;
                    }
                }


                if ((orderPack.status == 0) && (orderExists || isCreateOrderSuccessfully))
                {
                    orderPack.status = 3;
                    orderPack.pack_time = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                    _heerpContext.SaveChanges();
                }

                if (isCreateOrderSuccessfully == true || orderExists)
                {
                    orderPackTradeEntity.HasBeenOrdered = true;
                    _pmsContext.SaveChanges();
                }
            }

            catch (Exception ex)
            {
                CLSLogger.Error("义乌保税仓下单发生异常", ex, LogTags);
                throw ex;
            }
        }
        private bool SwapSKUForItemCode(CreateSupplierOrderInput inputDto, List<ProductEntity> listProductInfo)
        {
            //if not found, return false
            foreach (var item in inputDto.Orders.FirstOrDefault().OrderDtls)
            {
                var productInfo = listProductInfo.FirstOrDefault(x => x.BondedWarehouseItemCode.StandardizeSKU() == item.commodityCode.StandardizeSKU() || x.SKU.StandardizeSKU() == item.commodityCode.StandardizeSKU());

                if (productInfo == null)
                {
                    var ordercode = inputDto.Orders.FirstOrDefault().orderPackNo.IsNullOrWhiteSpace() ? inputDto.Orders.FirstOrDefault().orderCode : inputDto.Orders.FirstOrDefault().orderPackNo;
                    // Push CMQ to Teambition_task Topic
                    // TeambitionMessage teambitionmesage = new TeambitionMessage
                    // {
                    //     OrderNo = ordercode, //订单号
                    //     Warehouse = WarehouseName,
                    //     Title = $"产品表无对应义乌保税仓商品。订单内SKU：{item.commodityCode}",
                    //     OutOfStockSKU = item.commodityCode,
                    //     TriggerTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"),
                    //     MessageContent = $"义乌保税仓下单失败。订单为：{ordercode}。失败原因：产品表无对应义乌保税仓商品。订单内SKU：{item.commodityCode}, 产品到货后，请尽快维护产品信息; 该订单计划在：" + DateTime.UtcNow.AddHours(24 + 8).ToString("yyyy-MM-dd HH:mm:ss") + "【北京时间】再次尝试向【义乌保税仓】推送此订单!"
                    // };
                    // TencentCMQProxy.PublishTopicMessage(TCConstants.CMQTopics.TOPIC_TEAMBITIONNOTIFICATION, teambitionmesage, messageTags, LogTags);

                    //push message to dingding
                    // 计算时间差
                    int differenceInDays = 0;
                    string MessageContent = $"<font color=#FF0000 face=黑体>告警内容: 产品表无对应义乌保税仓商品 </font> \n\n";
                    MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {ordercode}  \n\n";
                    MessageContent = MessageContent + $" ##### **保税仓 SKU**:  {item.commodityCode}  \n\n";
                    MessageContent = MessageContent + $" ##### **魔法灯 SKU**:  N/A  \n\n";

                    if (!string.IsNullOrWhiteSpace(inputDto.Orders.FirstOrDefault().orderDate))
                    {
                        DateTime oldDate = DateTime.Parse(inputDto.Orders.FirstOrDefault().orderDate);
                        DateTime newDate = DateTime.Now;
                        TimeSpan ts = newDate - oldDate;
                        differenceInDays = ts.Days;
                    }

                    if (differenceInDays >= 5)
                    {
                        MessageContent = MessageContent + $" ##### **缺货天数**:  **_<font face=华云彩绘 color=#FF0000> {differenceInDays} 天</font>_** \n\n";
                    }
                    else
                    {
                        MessageContent = MessageContent + $" ##### **缺货天数**:  **_<font face=华云彩绘 color=#228b22> {differenceInDays} 天</font>_** \n\n";
                    }
                    MessageContent = MessageContent + $" ##### **订单创建时间**:  { inputDto.Orders.FirstOrDefault().orderDate } ";

                    _dingdingNotificationJob.SendBondedWarehouseYiWuAlert("义乌保税仓推单结果", MessageContent);

                    return false;
                }
                else
                {
                    item.commodityCode = productInfo.BondedWarehouseItemCode;
                    item.commodityName = productInfo.ChineseName;
                }
            }

            return true;
        }
        private string GetCarrierIDFromLinkedOrderPack(string orderPackNoToMatch, string orderID)
        {
            if (orderPackNoToMatch.IsNullOrEmpty())
            {
                return orderID;
            }

            var orderPackToMatch = _heerpContext.OrderPacks.FirstOrDefault(x => x.orderPackNo == orderPackNoToMatch);

            if (orderPackToMatch == null)
            {

                // Push CMQ to Teambition_task Topic
                // TeambitionMessage teambitionmesage = new TeambitionMessage
                // {
                //     OrderNo = orderPackNoToMatch, //订单号
                //     Warehouse = WarehouseName,
                //     Title = "订单notes中包裹号找不到对应用户所下订单的包裹",
                //     OutOfStockSKU = "",
                //     TriggerTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"),
                //     MessageContent = $"义乌保税仓下单失败，订单notes中包裹号找不到对应用户所下订单的包裹, 订单为：{orderPackNoToMatch} "
                // };
                // TencentCMQProxy.PublishTopicMessage(TCConstants.CMQTopics.TOPIC_TEAMBITIONNOTIFICATION, teambitionmesage, messageTags, LogTags);


                // Push DingDing Message
                string MessageContent = $"<font color=#FF0000 face=黑体>告警内容: 订单notes中包裹号找不到对应用户所下订单的包裹 </font> \n\n";
                MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {orderPackNoToMatch}";

                _dingdingNotificationJob.SendBondedWarehouseYiWuAlert("义乌保税仓推单结果", MessageContent);

                throw new Exception($"义乌保税仓下单失败，订单号：{orderPackNoToMatch}");
            }

            return orderPackToMatch.carrierId;
        }



        public void PushyiwuNewOrderbyOid(int pageSize)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = "YiwuPushOrder",
                ["method"] = "YiwuNewPushOrderbyOID"
            };


            List<SqlParameter> parameters = new List<SqlParameter>();
            DateTime CurrentTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
            int CurrentHour = CurrentTime.Hour;


            string orderpackSQL =
             $@" with cte as (
                  select distinct ta.orderid,
				  --dateadd(hour,(select isnull(delayhour,0) from orders_pack_exportflag where flag=tb.exportflag),ta.ordercreate_time) as ordercreate_time,
                  ta.ordercreate_time as ordercreate_time,
				  ta.oid,tb.orderpackno,tb.exportflag
                  from orders ta  inner join orders_pack tb
                  on ta.orderid = tb.orderid
                  where tb.exportflag in (79,84,85) and tb.status = 0 and tb.freezeflag=0
                  union 
                  select distinct ta.orderid,
				  --dateadd(hour,(select isnull(delayhour,0) from orders_pack_exportflag where flag=tb.exportflag),ta.ordercreate_time) as ordercreate_time,
                  ta.ordercreate_time as ordercreate_time,
				  ta.oid,tb.orderpackno,tb.exportflag
                  from orders ta  inner join orders_pack tb
                  on ta.orderid = tb.orderid
                  where ta.store in ('qdbs-store.mofa.ht', 'kuaidi.nz','naifen.kuaidi.nz')
                  and tb.status = 0 and tb.freezeflag=0 and tb.exportflag not in (73)
                )
                , cteorder as (select orderid,ordercreate_time,oid,orderpackno,row_number() over(order by ordercreate_time) as ID
                from cte
				)
                , cteresult as (select orderid,ordercreate_time,ltrim(oid) as oid ,orderpackno,NTILE(24) over(order by id )-1 as rowid from cteorder)
                select orderid,ordercreate_time,oid,orderpackno,ltrim(rowid) as rowid  
				from cteresult
               
                where orderpackno not in (
			                SELECT 
			                OrderPackNo
			                FROM openrowset('sqloledb', '150.109.119.209,8433';'sa';'Mofadeng@1234', ' 
			                select OrderPackNo from ERP.dbo.EcinxOrdersPackTrade
			                  ') 
                  ) and  rowid={CurrentHour}
            ";
            // 过滤已经推送过Ecinx的订单；

            var hEERPDbContext = new HEERPDbContextFactory().CreateDbContext();
            var orderPackLocks = hEERPDbContext.OrderPackYiwu.FromSql(orderpackSQL)
                                .MapTo<List<yiwuorderpack>>()
                                .ToList();
            try
            {
                var totalCount = orderPackLocks.Count();
                int totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;

                for (int i = 0; i < totalPage; i++)
                {
                    var orderpack = orderPackLocks.OrderBy(x => x.oid).Skip(i * pageSize).Take(pageSize).ToList();

                    foreach (var order in orderpack)
                    {
                        RepushListOfOrder(order.orderpackno);
                    }
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("义乌仓重新推送失败(新推送)", ex, clogTags);
            }
        }



        public void RepushListOfOrder(string orderpackno)
        {
            //List<OrderPackTradeEntity> listOrderPackTrade = _orderPackTradeRepository.GetAll().Where(x => x.OrderPackNo == orderpackno).ToList();
            bool orderPackTradeExists = _pmsContext.OrderPackTrades.Any(x => x.OrderPackNo == orderpackno);
            OrderPackTradeEntity orderPackTradeEntity;

            if (orderPackTradeExists)
            {
                orderPackTradeEntity = _pmsContext.OrderPackTrades.FirstOrDefault(x => x.OrderPackNo == orderpackno);

                try
                {
                    var input = orderPackTradeEntity.OriginalInfo.ConvertFromJsonString<CreateSupplierOrderInput>();
                    if (input is null)
                    {
                        Dictionary<string, string> clogTags = new Dictionary<string, string>
                        {
                            ["type"] = ClogTagType,
                            ["orderpackno"] = orderPackTradeEntity.OrderPackNo
                        };
                        _dingdingNotificationJob.SendBondedWarehouseYiWuAlert("义乌仓重新推送失败(新算法推送):", $"input is null 包裹号:{orderPackTradeEntity}");
                        CLSLogger.Error("重推义乌保税仓失败(inputisnull)", $"input is null 包裹号:{orderPackTradeEntity}", clogTags);
                    }
                    else
                    {
                        _backgroundJobManager.Enqueue<YiWuCreateOrderJob, CreateSupplierOrderInput>(input, BackgroundJobPriority.Normal);
                    }
                  
                }
                catch (Exception ex)
                {
                    Dictionary<string, string> clogTags = new Dictionary<string, string>
                    {
                        ["type"] = ClogTagType,
                        ["orderpackno"] = orderPackTradeEntity.OrderPackNo
                    };
                    CLSLogger.Error("重推义乌保税仓失败(新推送)", ex, clogTags);
                }

            }
            else
            {
                string MessageContent = $"包裹号:{ orderpackno } 未找到188推送的支付记录，需要188重推此包裹";
                _dingdingNotificationJob.SendBondedWarehouseYiWuAlert("义乌仓重新推送失败(新算法推送):", MessageContent);

            }

        }


        public class yiwuorderpack
        {
            public string orderid { get; set; }
           
            public DateTime? ordercreate_time
            {
                get
                {
                    return _ordercreate_time;
                }
                set
                {
                    _ordercreate_time = value?.ToUniversalTime();
                }
            }

            public string oid { get; set; }
            public string orderpackno { get; set; }
            public string rowid { get; set; }

            private DateTime? _ordercreate_time;
        }


    }
}