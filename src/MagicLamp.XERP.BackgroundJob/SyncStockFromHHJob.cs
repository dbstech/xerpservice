﻿using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Dependency;
using MagicLamp.PMS.Domain;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.XERP.BackgroundJob
{
    public class SyncStockFromHHJob : CommonBackgroundJob<List<SyncHHStockDTO>>
    {
        private const int WarehouseIdNZ = 6;

        public override void Process(List<SyncHHStockDTO> dtos)
        {


            int pageSize = 300;

            int totalCountForUnsuccessful = 0;
            int totalCountForSynced = 0;


            long duration = 0;
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();



            bool fullSync = false;
            List<string> skuSynced = new List<string>();

            try
            {
                if (!ValidateInputIsNotEmpty(dtos))
                {
                    return;
                }

                HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
                PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();

                var totalCount = dtos.Count();
                int totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
                
                ProductDomainService productDomainService = new ProductDomainService();

                if (totalCount > 1000)
                {
                    fullSync = true;
                }


                for (int i = 0; i < totalPage; i++)
                {
                    StringBuilder logForFailed = new StringBuilder();
                    StringBuilder logForSucceed = new StringBuilder();
                

                    int iterationCountForSynced = 0;
                    int iterationCountForUnsuccessful = 0;


                    List<SyncHHStockDTO> currentIterationHHSKU = dtos.OrderBy(x => x.SKU).Skip(i * pageSize).Take(pageSize).ToList();
                    
                    


                    //get a list of sku from HH
                    List<string> listHHSKU = currentIterationHHSKU.Select(x => x.SKU.Trim()).ToList();

                    skuSynced.AddRange(listHHSKU);

                    //only query product that is from nz warehouse
                    List<ProductEntity> listProductInERP = pmsContext.Products.Where(x => listHHSKU.Contains(x.HHSKU.Trim()) && x.WarehouseID == WarehouseIdNZ && x.ActiveFlag == true 
                    && x.IsHHPurchase == true ).ToList();

                    if (listProductInERP.IsNullOrEmpty())
                    {
                        CLSLogger.Error("同步HH库存Job失败一组", $"失败原因：SKU无匹配商品。来自HH的SKU：{listHHSKU.ToString()}。", LogTags);
                        continue;
                    }

                    var listERPSKUFromProducts = listProductInERP.Select(x => x.SKU.Trim()).ToList();

                    //get api stock based on sku
                    List<APIStockEntity> listAPIStocks = heERPContext.APIStocks.Where(x => listERPSKUFromProducts.Contains(x.SKU.ToLower().Trim(), StringComparer.OrdinalIgnoreCase)).ToList();

                    //update product cache
                    productDomainService.UpdateProductStockCache(listAPIStocks);

                    //update qty_hh for api stock
                    UpdateAPIStockForCurrentIteration(heERPContext, listAPIStocks, currentIterationHHSKU, listProductInERP, iterationCountForUnsuccessful, iterationCountForSynced, totalCountForUnsuccessful, totalCountForSynced, logForFailed, logForSucceed);

                    //trigger the job of sync stock to 188                    
                    IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
                    backgroundJobManager.Enqueue<SyncAladdinStockJob, List<string>>(listERPSKUFromProducts, BackgroundJobPriority.High);


                  


                }

                //if full sync, set api stock where qty_hh is not null to 0
                if (fullSync)
                {
                    List<ProductEntity> listProductHHInERP = pmsContext.Products.Where(x => !skuSynced.Contains(x.HHSKU.Trim()) && x.WarehouseID == WarehouseIdNZ 
                    && x.ActiveFlag == true && x.IsHHPurchase == true).ToList();

                    var listERPSKUFromHHProducts = listProductHHInERP.Select(x => x.SKU.Trim()).ToList();

                    List<APIStockEntity> listAPIHHStockNotSync = heERPContext.APIStocks.Where(x => x.Qty_hh.HasValue && listERPSKUFromHHProducts.Contains(x.SKU, StringComparer.OrdinalIgnoreCase) 
                    && x.Qty_hh >0 ).ToList();

                    UpdateAPIStockHHNotShared(heERPContext, listAPIHHStockNotSync);
                }


              


            }
            catch (Exception ex)
            {
                CLSLogger.Error($"同步HH库存发生异常", ex, LogTags);
                throw ex;
            }
            finally
            {
                CLSLogger.Info("同步HH库存结束", $"成功同步{totalCountForSynced}个，失败{totalCountForUnsuccessful}个。", LogTags);
            }


            sw.Stop();
            duration = sw.ElapsedMilliseconds;
            CLSLogger.Info("本次同步", $"总耗时: {duration / 1000.0} 秒", LogTags);


        }

        private bool ValidateInputIsNotEmpty(List<SyncHHStockDTO> dtos)
        {
            List<string> allInputSKUs = dtos.Select(x => x.SKU).ToList();
            if (allInputSKUs.IsNullOrEmpty())
            {
                CLSLogger.Error("同步HH库存Job失败", $"失败原因：SKU不能为空。input：{dtos.ToJsonString()}。", LogTags);
                return false;
            }
            return true;
        }

        private string GetERPSKUBasedOnHHSKU(string HHSKU, List<ProductEntity> listProductInERP)
        {
            var productInfo = listProductInERP.FirstOrDefault(x => x.HHSKU.StandardizeSKU() == HHSKU.StandardizeSKU());

            return productInfo?.SKU;
        }

        private void UpdateAPIStockForCurrentIteration(HEERPDbContext heERPContext, List<APIStockEntity> listAPIStocks, List<SyncHHStockDTO> currentIterationHHSKU, List<ProductEntity> listProductInERP, int iterationCountForUnsuccessful, int iterationCountForSynced, int totalCountForUnsuccessful, int totalCountForSynced, StringBuilder logForFailed, StringBuilder logForSucceed)
        {
           

            foreach (var item in currentIterationHHSKU)
            {
                var HHSKU = item.SKU;
                var ERPSKU = GetERPSKUBasedOnHHSKU(HHSKU, listProductInERP);

                //cannot find sku in erp
                //this comparison operation is case sensitive
                if (ERPSKU == null)
                {
                    string hhDetail = $"SKUFromHH: {HHSKU}, Barcode: {item.Barcode}, " +
                                $"StockQty: {item.StockQty}, ExpiryDate: {item.ExpiryDate}, Pseq: {item.Pseq}";

                    logForFailed.Append($"失败一个。失败原因：HH提供SKU匹配不上BAS.Product表内数据，请检查product的仓库是否为nz仓及sku是否填写正确。HH提供数据：{hhDetail}。");

                    totalCountForUnsuccessful++;
                    iterationCountForUnsuccessful++;
                }
                else
                {
                    var APIStockInfo = listAPIStocks.FirstOrDefault(x => x.SKU.StandardizeSKU() == ERPSKU.StandardizeSKU());
                    //cannot find entry in apistocks
                    if (APIStockInfo == null)
                    {
                        string hhDetail = $"SKUFromHH: {HHSKU}, SKUInERP: {ERPSKU}, Barcode: {item.Barcode}, " +
                                $"StockQty: {item.StockQty}, ExpiryDate: {item.ExpiryDate}, Pseq: {item.Pseq}";

                        logForFailed.Append($"失败一个。失败原因：dbo.api_stocks无相关数据可同步，HH提供数据及匹配的ERPSKU：{hhDetail}。");

                        totalCountForUnsuccessful++;
                        iterationCountForUnsuccessful++;
                    }
                    else
                    {


                        if (APIStockInfo.Qty_hh.HasValue && APIStockInfo.Qty_hh.Value == item.StockQty
                            && !APIStockInfo.Expiry_date_hh.IsNullOrEmpty() && APIStockInfo.Expiry_date_hh == item.ExpiryDate) //item.ExpiryDate.Value.ToString("MM/yyyy")
                        {
                            logForSucceed.Append($"跳过更新，SKUFromERP：{APIStockInfo.SKU}，SKUFromHH：{item.SKU}，Barcode：{item.Barcode}，" +
                                $"StockQty：{item.StockQty}，ExpiryDate：{item.ExpiryDate}，Pseq：{item.Pseq}。");
                        }
                        else
                        {
                            APIStockInfo.Qty_hh = item.StockQty;
                            APIStockInfo.Expiry_date_hh = item.ExpiryDate; //item.ExpiryDate.Value.ToString("MM/yyyy");
                            APIStockInfo.Updated_time = DateTime.Now;
                            totalCountForSynced++;
                            iterationCountForSynced++;
                            heERPContext.SaveChanges();
                            logForSucceed.Append($"更新成功一个，SKUFromERP：{APIStockInfo.SKU}，SKUFromHH：{item.SKU}，Barcode：{item.Barcode}，" +
                                    $"StockQty：{item.StockQty}，ExpiryDate：{item.ExpiryDate}，Pseq：{item.Pseq}。");
                        }
                    }
                }
            }

            var processedSKU = currentIterationHHSKU.Select(x => x.SKU).ToList();
            LogTags["sku"] = string.Join(";", processedSKU);
            CLSLogger.Info("同步HH库存完成一组", $"成功同步{iterationCountForSynced}个，失败{iterationCountForUnsuccessful}个。成功明细：{ logForSucceed.ToString()} | 失败明细：{ logForFailed.ToString()}", LogTags);
        }

        private void UpdateAPIStockHHNotShared(HEERPDbContext heERPContext, List<APIStockEntity> listAPIStockHHNotSync)
        {

        


            foreach (var item in listAPIStockHHNotSync)
            {
                //var previousQtyHH = item.Qty_hh;
                //var previousExpiryDateHH = item.Expiry_date_hh;

                item.Qty_hh = 0;
                heERPContext.SaveChanges();
                //CLSLogger.Info("HH库存全量同步无此sku，更新其HH库存数为0", $"SKU: {item.SKU}, 原qty_hh: {previousQtyHH}, hh保质期: {previousExpiryDateHH}", LogTags);
            }

          
        }
    }
}
