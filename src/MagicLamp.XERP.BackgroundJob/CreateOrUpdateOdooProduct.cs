using System.Collections.Generic;
using Abp.Dependency;
using MagicLamp.PMS.Proxy;
using System.Threading.Tasks;
using System;
using System.Linq;
using MagicLamp.PMS.Entities;
using Abp;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.BackgroundJobs;
using OdooRpc.CoreCLR.Client.Models;
using System.Text;
using MagicLamp.PMS.DTOs.Enums;
using OdooRpc.CoreCLR.Client.Models.Parameters;
using Abp.ObjectMapping;
using Microsoft.EntityFrameworkCore;
using MagicLamp.PMS.Proxy.Odoo;

namespace MagicLamp.XERP.BackgroundJob
{
    public class CreateOrUpdateOdooProductJob : AsyncBackgroundJob<List<string>>, ITransientDependency
    {
        private IRepository<ProductEntity, string> _productRepository;
        private IRepository<ExchangeRate, string> _exchangeRateRepository;
        private IUnitOfWorkManager _unitOfWorkManager;
        private IObjectMapper _objectMapper;
        public CreateOrUpdateOdooProductJob(
            IRepository<ProductEntity, string> productRepository,
            IRepository<ExchangeRate, string> exchangeRateRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IObjectMapper objectMapper
        )
        {
            _productRepository = productRepository;
            _exchangeRateRepository = exchangeRateRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _objectMapper = objectMapper;
        }
        [UnitOfWork]
        protected override async Task ExecuteAsync(List<string> skus)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                IDictionary<string, string> exceptions = new Dictionary<string, string>();
                StringBuilder exMsg = new StringBuilder();
                StringBuilder successMsg = new StringBuilder();

                OdooProxy proxy = new OdooProxy();

                var sku = "";
                ProductEntity entity = null;
                try
                {
                    var productEntities = _productRepository.GetAll()
                    .Where(x =>
                        skus.Contains(x.SKU)
                        && (x.ActiveFlag == true)
                        && (x.SyncToOdoo == true)
                        && (x.HHSKU != null)
                    )
                    .Include(x => x.Country)
                    .Include(x => x.Brand)
                    .ToList();
                    var hhSkus = productEntities.Select(x => x.HHSKU);

                    await proxy.LoginToOdoo();

                    var odooProducts = await proxy.GetProducts(
                        new OdooDomainFilter().Filter("default_code", "in", hhSkus),
                        new OdooFieldParameters()
                        {
                        "id",
                        "default_code",
                        "barcode",
                        "responsible_id"
                        }
                    );
                    Dictionary<string, decimal> exchangeRates = _exchangeRateRepository
                        .GetAll()
                        .Where(x => x.To == "NZD")
                        .ToDictionary(x => x.From, x => x.Rate);
                    var odooCategories = await proxy.GetAllCategories();

                    foreach (var productEntity in productEntities)
                    {
                        try
                        {
                            sku = productEntity.SKU;
                            entity = productEntity;
                            if (entity.OdooSyncStatus == OdooSyncStatusEnum.Success.ToString())
                            {
                                continue;
                            }

                            var odooProduct = odooProducts.FirstOrDefault(x => x["default_code"].ToString() == entity.HHSKU);
                            var product = _objectMapper.Map<Product>(productEntity);

                            bool isVirtualStock = productEntity.PurchasePeriodCode == "virtualStock";
                            var odooCategory = odooCategories.FirstOrDefault(x => x["id"].ToObject<long>() == productEntity.OdooCategoryID);
                            string categoryCompleteName = odooCategory["complete_name"].ToString();
                            bool syncCostToOdoo = isVirtualStock && (categoryCompleteName.Contains("DF") || categoryCompleteName.Contains("FO"));
                            long odooProductId = 0;
                            decimal standardPrice = (productEntity.Cost ?? 0) * exchangeRates[productEntity.Currency];
                            if (syncCostToOdoo)
                            {
                                product.standard_price = standardPrice;
                                product.taxes_id = new object[1] { new object[3] { 6, 0, new int[] { 3, 12 } } };
                                product.supplier_taxes_id = new object[1] { new object[3] { 6, 0, new int[] { 6, 15 } } };
                            }

                            if (odooProduct == null)
                            {
                                odooProductId = await proxy.CreateProduct(product);
                                entity.OdooProductID = odooProductId;
                            }
                            else
                            {
                                odooProductId = odooProduct["id"].ToObject<long>();
                                product.responsible_id = odooProduct["responsible_id"][0].ToObject<long>();
                                entity.OdooProductID = odooProductId;
                                await proxy.UpdateProduct(odooProductId, product);
                            }

                            if (syncCostToOdoo)
                            {
                                await proxy.UpdateProduct(
                                    odooProductId,
                                    new Product
                                    {
                                        default_code = entity.HHSKU,
                                        standard_price = standardPrice,
                                        taxes_id = null,
                                        supplier_taxes_id = null
                                    },
                                    new OptionalParameters
                                    {
                                        force_company = 1
                                    }
                                );
                            }

                            successMsg.Append($"{sku} \n\n");
                            entity.OdooSyncStatus = OdooSyncStatusEnum.Success.ToString();
                        }
                        catch (RpcCallException ex)
                        {
                            exceptions.Add(sku, ex.RpcErrorData.ToString());
                            exMsg.Append($"{sku} \n\n");
                            exMsg.Append($"失败原因 \n\n");
                            exMsg.Append($"{ex.RpcErrorData.ToString()} \n\n");
                            if (entity != null)
                            {
                                entity.OdooSyncStatus = OdooSyncStatusEnum.Failed.ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                            exceptions.Add(sku, ex.Message);
                            exMsg.Append($"{sku} \n\n");
                            exMsg.Append($"失败原因 \n\n");
                            exMsg.Append($"{ex.Message} \n\n");
                            if (entity != null)
                            {
                                entity.OdooSyncStatus = OdooSyncStatusEnum.Failed.ToString();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    CLSLogger.ErrorWithSentry("批量创建或更新odoo产品登录发生异常", ex);
                    throw ex;
                }

                _unitOfWorkManager.Current.SaveChanges();
                unitOfWork.Complete();


                // Odoo 同步群
                string ddToken = "7ae174384da0978a10ded1e234f8004759eaade08f7ba3b1b4e6d4169595041e";
                string ddSignature = "SECaab5cd16c36283e1c6c67a3fa738e5728a99e20deb2839eda33708492382d10b";

                // New 
                //string ddToken = "ee4ac8ef810d0b01ab594bd49cd47b48b3bd3990238bdd15b590d6eb47e13085";
                //string ddSignature = "SEC174674d3ee1f2a25d536b57f81a3325cf02cd5dfa61ba767ca81e0e56c8d2326";

                if (successMsg.Length > 0)
                {
                    string title = "# 以下SKU同步odoo成功 \n\n";
                    successMsg.Insert(0, title);
                    DDNotificationProxy.MarkdownNotification(title, successMsg.ToString(), ddToken, ddSignature);
                }

                if (exceptions.Count > 0)
                {
                    CLSLogger.Info("批量創建或更新odoo产品发生异常", exMsg.ToString());
                    string title = "# 以下SKU同步odoo失败，请重新上传 \n\n";
                    exMsg.Insert(0, title);
                    DDNotificationProxy.MarkdownNotification(title, exMsg.ToString(), ddToken, ddSignature);
                    throw new Exception(exMsg.ToString());
                }
            }

        }
    }
}
