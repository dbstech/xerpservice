﻿using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Flux;
using MagicLamp.PMS.DTOs.ThirdPart;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.XERP.BackgroundJob
{
    public class AutoAddPurchasingInfoForHHJob : CommonBackgroundJob<HHBindWaveAndOrderInputDTO>
    {
        const string ClogTagType = "AutoAddPurchasingInfoForHHJob";
        private SendNotificationToDingDingJob _dingdingNotificationJob;
        private static List<FluxPutDBASNDataDTO> MakeFluxPutDBASNData(PurchaseOrderHeaderEntity inputHeader, List<PurchaseOrderLineEntity> inputLines)
        {
            List<FluxPutDBASNDataDTO> result = new List<FluxPutDBASNDataDTO>();

            FluxPutDBASNDataDTO dataToBePushed = new FluxPutDBASNDataDTO
            {
                ASNCreationTime = inputHeader.CreateDate.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                ASNReference2 = inputHeader.Reference2,
                ASNReference3 = "",
                ASNReference4 = inputHeader.Reference4,
                ASNReference5 = inputHeader.OrderType,
                CustomerID = inputHeader.OrgId,
                detailsItem = new List<FluxPoLineDTO>(),
                ExpectedArriveTime1 = inputHeader.ExpectedArriveTime1 == null ? null : ((DateTime)inputHeader.ExpectedArriveTime1).ToString("yyyy-MM-dd hh:mm:ss"),
                ExpectedArriveTime2 = inputHeader.ExpectedArriveTime2 == null ? null : ((DateTime)inputHeader.ExpectedArriveTime2).ToString("yyyy-MM-dd hh:mm:ss"),
                Notes = inputHeader.Notes,
                OrderNo = inputHeader.PurchasingOrderNo,
                //暂时为固定值(according to Views: [dbo].[V_SendFluxPOHeader])
                OrderType = "01",
                SupplierID = inputHeader.VendorId,
                UserDefine1 = inputHeader.UDF1,
                UserDefine2 = inputHeader.UDF2,
                UserDefine3 = inputHeader.UDF3,
                UserDefine4 = inputHeader.UDF4,
                UserDefine5 = inputHeader.ProductHolder,
                //CASE WHEN OrgId='AU' THEN 'WH02' ELSE 'WH01'(according to Views: [dbo].[V_SendFluxPOHeader])
                WarehouseID = "WH01"
            };

            foreach (var item in inputLines)
            {
                dataToBePushed.detailsItem.Add(new FluxPoLineDTO
                {
                    CustomerID = item.OrgId,
                    ExpectedQty = item.QTY,
                    Notes = "",
                    SKU = item.Sku,
                    UserDefine1 = "",
                    UserDefine2 = "",
                    UserDefine3 = "",
                    UserDefine4 = "",
                    UserDefine5 = "",
                    LotAtt11 = item.LotHolder,
                    LotAtt12 = item.LotNo,
                    LotAtt07 = inputHeader.PurchasingOrderNo
                });
            }

            result.Add(dataToBePushed);
            return result;
        }

        public override void Process(HHBindWaveAndOrderInputDTO input)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>();
            clogTags["type"] = ClogTagType;
            clogTags["jobid"] = Guid.NewGuid().ToString();
            clogTags["wavePickingNo"] = input.wave_picking_no;

            PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();

            StringBuilder logMessage = new StringBuilder();

            try
            {
                //find reservedHHSKU
                var listOrderPackNo = input.orders.Select(x => x).ToList();

                var listReservedHHSKUInfo = pmsContext.OrderPackReservedHHSKUs.Where(x => listOrderPackNo.Contains(x.OrderPackNo)).ToList();

                //HH推送需配货批次订单完成后，自动新建采购单

                //header data
                string user = "System";
                string orgId = "ALADDIN";
                string orderType = "Product";
                string currency = "NZD";
                string vendorId = "HNH";
                string orderStatus = "Create";
                decimal totalPrice = 0;

                //line data
                string orderLineType = "2";

                foreach (var order in listReservedHHSKUInfo)
                {
                    totalPrice += order.Qty.Value * order.Price.Value;
                }

                PurchaseOrderHeaderEntity orderHeader = new PurchaseOrderHeaderEntity
                {
                    PurchasingOrderNo = input.wave_picking_no,
                    OrgId = orgId,
                    OrderType = orderType,
                    Currency = currency,
                    VendorId = vendorId,
                    OrderStatus = orderStatus,
                    TotalAmount = totalPrice,
                    ExpectedArriveTime2 = DateTime.Now.AddDays(1),
                    CreateBy = user,
                    LastUpdateBy = user,
                    CreateDate = DateTime.UtcNow,
                    LastUpdateDate = DateTime.UtcNow
                };

                //add header
                pmsContext.PurchaseOrderHeaders.Add(orderHeader);

                EditLogEntity logHeader = new EditLogEntity
                {
                    System = "pms",
                    Type = "Add_Order",
                    Uesr = user,
                    DateTime = DateTime.UtcNow,
                    Content = $"新增订单成功; Content: {orderHeader.ToJsonString()}"
                };

                //add log
                pmsContext.EditLogs.Add(logHeader);

                List<PurchaseOrderLineEntity> orderLines = new List<PurchaseOrderLineEntity>();

                //group by same sku and same price
                var groupedReservedInfo = listReservedHHSKUInfo.GroupBy(x => new
                {
                    x.SKU,
                    x.Price
                }).Select(x => new OrderPackDetailDTO
                {
                    sku = x.Key.SKU,
                    qty = x.Sum(item => item.Qty.Value),
                    price = x.Key.Price
                }).ToList();

                foreach (var entity in groupedReservedInfo)
                {
                    PurchaseOrderLineEntity orderLine = new PurchaseOrderLineEntity
                    {
                        PoHeaderId = input.wave_picking_no,
                        OrderLineType = orderLineType,
                        Sku = entity.sku,
                        QTY = (decimal)entity.qty,
                        UOM = null,
                        Cost = Math.Round((entity.price.Value / 1.15M), 2),
                        TaxedCost = entity.price.Value,
                        TaxRate = 0.15M,
                        OrgId = orgId,
                        Currency = currency,
                        CreateBy = user,
                        LastUpdateBy = user,
                        CreateDate = DateTime.UtcNow,
                        LastUpdateDate = DateTime.UtcNow
                    };
                    //add line
                    pmsContext.PurchaseOrderLines.Add(orderLine);

                    orderLines.Add(orderLine);


                    EditLogEntity log = new EditLogEntity
                    {
                        System = "pms",
                        Type = "Add_OrderLine",
                        Uesr = user,
                        DateTime = DateTime.UtcNow,
                        Content = $"新增订单行成功; Content: {orderLine.ToJsonString()}"
                    };

                    //add log
                    pmsContext.EditLogs.Add(log);

                }

                pmsContext.SaveChanges();

                //push to flux

                //保存批次属性

                foreach (var line in orderLines)
                {
                    line.LotNo = $"{orderHeader.PurchasingOrderNo}-{line.PoLineId}";
                }



                var dataHeader = MakeFluxPutDBASNData(orderHeader, orderLines);


                var fluxResponse = FluxWMSProxy.PutDBASNData(dataHeader);

                CLSLogger.Info("富勒调拨入库单响应报文", fluxResponse.ToJsonString(), clogTags);

                //int.Parse(fluxResponse.Response.@return.returnFlag) != 1
                //发送富勒成功，SendFlag = 1, orderstatus = waiting
                //发送是失败，sendflag = 90，failreason = response

                //推失败
                if (fluxResponse == null || int.Parse(fluxResponse.Response.@return.returnFlag) != 1)
                {
                    var reason = fluxResponse.Response.@return.returnDesc;

                    orderHeader.SendFlag = "90";

                    clogTags["method"] = "PutSOData";
                    clogTags["trackid"] = orderHeader.PurchasingOrderNo;
                    CLSLogger.Error("调拨入库单推富勒发生异常", reason, clogTags);
                }
                //成功
                else
                {
                    var reason = fluxResponse.Response.@return.returnDesc;

                    orderHeader.SendFlag = "1";
                    orderHeader.OrderStatus = "Waiting";

                    clogTags["method"] = "PutSOData";
                    clogTags["trackid"] = orderHeader.PurchasingOrderNo;
                }

                pmsContext.SaveChanges();
            }
            catch (Exception ex)
            {
                string error = ex.InnerException == null ? ex.Message : ex.InnerException.ToString();
                logMessage.Insert(0, error);
                CLSLogger.Error("HH推送需配货批次订单完成后，自动新建采购单发生异常", ex, clogTags);
                _dingdingNotificationJob = new SendNotificationToDingDingJob();
                _dingdingNotificationJob.SendPurchaseOrderAlert("自动新建采购单发生异常", "HH推送需配货批次订单完成后，自动新建采购单发生异常，波次号:" + input.wave_picking_no);
            }
            finally
            {
                CLSLogger.Info("HH推送需配货批次订单完成后，自动新建采购单完成", $"处理的波次及订单：{logMessage}", clogTags);
            }
        }
    }
}
