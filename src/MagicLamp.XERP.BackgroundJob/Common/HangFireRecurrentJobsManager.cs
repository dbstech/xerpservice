﻿using Hangfire;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Text;
using TimeZoneConverter;

namespace MagicLamp.XERP.BackgroundJob
{
    public class HangFireRecurrentJobsManager
    {
        public static void PerformRecurrentJobs()
        {
            CmqQueueConfig cmqQueueConfig = Ultities.GetConfig<CmqQueueConfig>();

            BusinessConfig businessConfig = Ultities.GetConfig<BusinessConfig>();

            //全量同步Ecinx库存
            //RecurringJob.AddOrUpdate<SYNCEcinxAllInventory>(job => job.Execute(), "0 17 * * *", TZConvert.GetTimeZoneInfo("Pacific/Auckland"));
            if (!businessConfig.PushDDNotification)
            {
                return;
            }

            //循环执行订单匹配身份证信息
            RecurringJob.AddOrUpdate(() => new OrderIDCardJob().MatchData(), "45 */1 * * *"); //每小时0分钟
            //循环执行订单成本记录
            //RecurringJob.AddOrUpdate(() => new HEOrderToXJob().SynchronizeOrderCost(), Cron.MinuteInterval(12));
            //循环执行UGG产品库存更新
            RecurringJob.AddOrUpdate(() => new UpdateStockForUGGJob().UpdateStock(), "*/23 * * * *");
            //循环执行包裹出库补退新顺丰
            RecurringJob.AddOrUpdate(() => new RepushOrderPackToNSFJob().RepushOrderPack(), "0 */12 * * *");

            //循环执行推送订单到富勒  仓库转移
            RecurringJob.AddOrUpdate(() => new PushOrderToFluxJob().ScanQueue(100), "*/7 * * * *");

            //循环执行推送需配货批次订单给HH  仓库转移
            RecurringJob.AddOrUpdate(() => new PushOrderBindedToWaveToHHJob().ScanFluxWave(), "*/30 * * * *");


            //循环执行推送库存 (推送全量 NZ,AU库存给188或者洋小范每1.5小时）
            RecurringJob.AddOrUpdate(() => new SyncAladdinStockJob().SyncStock(new StringBuilder(), null), "30 */1 * * *");
            
            //循环同步保税仓SKU库存(重庆保税仓库存同步) 停止
            //RecurringJob.AddOrUpdate<UpdateStockForBondedWarehouse>(job => job.Execute(), "*/30 * * * *");

            //循环同步富勒已出库订单 仓库转移
            RecurringJob.AddOrUpdate(() => new SyncFluxOrderStatusJob().Process(null), "0 */2 * * *");

            //循环增量同步富勒库存 仓库转移
            RecurringJob.AddOrUpdate(() => new SyncFluxStockJob().Process(null), "0 9,14 * * *", TZConvert.GetTimeZoneInfo("Pacific/Auckland"));
            //循环全量同步富勒库存 仓库转移9-11-13-15-17-19
            RecurringJob.AddOrUpdate(() => new FullSyncFluxStockJob().Process(null), "0 9,11,13,15,17,19 * * *", TZConvert.GetTimeZoneInfo("Pacific/Auckland"));

            //定时检查HH相关状态并发送钉钉群通知  仓库转移
            RecurringJob.AddOrUpdate(() => new SendNotificationToDingDingJob().CheckHHRelatedStatus(), "0 9,17 * * *", TZConvert.GetTimeZoneInfo("Pacific/Auckland"));

            //循环执行刷新产品基本信息缓存
            RecurringJob.AddOrUpdate(() => new RefreshProductInfoCacheJob().Process(null), "0 */12 * * *");
            //循环执行刷新SKU锁定库存数量
            RecurringJob.AddOrUpdate(() => new RefreshSKULockQtyJob().Process(null), "*/10 * * * *");
            // One off job be triggered manually in hangfire dashboard, should be removed or changed to Cron.Never() or similar cron expression
            // RecurringJob.AddOrUpdate(() => new SyncOdooProductBrandJob().Process(), "0 0 1 1 1");
            // No long using qingdao bonded warehouse
            // RecurringJob.AddOrUpdate(() => new UpdateStockForQingDaoBondedWarehouseJob().UpdateStock(), "0 2 * * *", TZConvert.GetTimeZoneInfo("Asia/Shanghai"));

            //异常订单预警  仓库转移
            RecurringJob.AddOrUpdate(() => new SendNotificationToDingDingJob().MonitorAbnormalFluxOrders(), "0 0 * * *");

            RecurringJob.AddOrUpdate(() => new SendNotificationToDingDingJob().MonitorOrderPackWithoutCarrierID(), "0 1 * * *");
            RecurringJob.AddOrUpdate<XERP.BackgroundJob.FindActiveProductsWithoutCost>(job => job.Execute(), "0 1 * * *");
            // RecurringJob.AddOrUpdate<XERP.BackgroundJob.SyncExchangeRates>(job => job.Execute(cmqQueueConfig.YishanCurrencyRate), "*/10 * * * *");

            //八喜同步库存并推送给188；
            RecurringJob.AddOrUpdate<XERP.BackgroundJob.UpdateBaXiProductStock>(job => job.Execute(), "*/30 * * * *");
            //八禧订单处理
            RecurringJob.AddOrUpdate<XERP.BackgroundJob.BaXiOrderProcessJob>(job => job.Execute(null), "*/30 * * * *");
            //八禧订单状态同步
            RecurringJob.AddOrUpdate<BaXiOrderProcessJob>(job => job.SyncOrderDetailAsync(10), "*/17 * * * *");

            //RecurringJob.AddOrUpdate<SyncYiWuOrderStatus>(job => job.Execute(), "0 */3 * * *");
            //RecurringJob.AddOrUpdate<SyncYiDaTongYiWuOrderStatus>(job => job.Execute(), "30 */3 * * *");

            //每小时推送一次 (每小时的第15分钟推送一次) 暂停
            //RecurringJob.AddOrUpdate<YiWuCreateOrderJob>(job => job.PushyiwuNewOrderbyOid(10), "15 */1 * * *");

            RecurringJob.AddOrUpdate<SYNCCreateEcinxOrderJob>(job => job.Execute(), "18 22 * * *", TZConvert.GetTimeZoneInfo("Pacific/Auckland"));
            //NZ Time: 4:40 12:40; 22:40 
            RecurringJob.AddOrUpdate<SYNCEcinxDeliveryQuery>(job => job.Execute(), "40 4,12,22 * * *", TZConvert.GetTimeZoneInfo("Pacific/Auckland"));
            //BJ Time: 07:25; 17:25
            RecurringJob.AddOrUpdate<SYNCEcinxAllInventory>(job => job.Execute(), "25 23,03 * * *", TZConvert.GetTimeZoneInfo("Pacific/Auckland"));


        }
    }
}