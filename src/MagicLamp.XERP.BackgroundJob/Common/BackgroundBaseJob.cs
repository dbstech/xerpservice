﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Hangfire;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.XERP.BackgroundJob.Common
{
    public abstract class CommonBackgroundJob<T> : BackgroundJob<T>, ITransientDependency
    {
        public Dictionary<string, string> LogTags = new Dictionary<string, string>();

        public CommonBackgroundJob()
        {
            LogTags["jobid"] = Guid.NewGuid().ToString();
            LogTags["type"] = this.GetType().Name;
        }

        [AutomaticRetry(Attempts = 10)]
        [UnitOfWork]
        public override void Execute(T inputDto)
        {
            try
            {
                var type = this.GetType();
                CLSLogger.Info($"{type.Name}--开始运行", string.Empty, LogTags);
                //fix 188 pass {\"Orders\":null}
                if (this.GetType().Name == "YiWuCreateOrderJob"  &&  inputDto.ToJsonString().ToString().Length <= 30)
                {
                    CLSLogger.Info($"{type.Name}--运行发生异常--CommonBackgroundJobOrdersnull", inputDto.ToJsonString()+"->length: -> " + inputDto.ToJsonString().ToString().Length.ToString(), LogTags);
                    return; 
                }
                else
                {
                    Process(inputDto);
                    CLSLogger.Info($"{type.Name}--结束运行", string.Empty, LogTags);
                }

            }
            catch (Exception ex)
            {
                CLSLogger.Error($"{this.GetType().Name}运行发生异常", ex, LogTags);
                throw ex;
            }
        }

        public abstract void Process(T inputDto);

    }
}
