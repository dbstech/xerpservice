﻿using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.MqDTO;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.DTOs.ThirdPart;
using Abp.Domain.Uow;

namespace MagicLamp.XERP.BackgroundJob
{
    public class CancelHHOrderJob : CommonBackgroundJob<HHPushOrderInputDTO>
    {

        public override void Process(HHPushOrderInputDTO inputDto)
        {
            LogTags["orderpackno"] = inputDto.order_no;

            try
            {
                PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();
                StringBuilder log = new StringBuilder();

                //1：校验订单是否已经绑定过波次，Binded不允许取消
                if (pmsContext.OrderPackReservedHHSKUs.Any(x => x.OrderPackNo == inputDto.order_no
                && x.Status == OrderPackReservedStatusEnum.Binded.ToString()))
                {
                    CLSLogger.Info("cancel HH order failed", $"包裹：{inputDto.order_no}已绑定过波次，不允许取消", LogTags);
                    return;
                }

                //2：是否已经reserved，是的话，调用HH取消订单接口，失败需重试
                var listReserved = pmsContext.OrderPackReservedHHSKUs.Where(x => x.OrderPackNo == inputDto.order_no).ToList();
                //查询订单是否存在
                var resultFromGetOrderInHH = HHProxy.GetOrder(inputDto);

                if (!listReserved.IsNullOrEmpty() || resultFromGetOrderInHH.result == 0)
                {
                    var result = HHProxy.CancelOrderInHH(inputDto);

                    if (result == null || result.result != 0)
                    {
                        CLSLogger.Error("cancel HH order failed", result.ToJsonString(), LogTags);
                        throw new Exception("cancel HH order failed");
                    }
                    else
                    {
                        log.Append("取消推送给HH的订单信息成功。");
                    }
                }
                else
                {
                    log.Append("OrderPacks_ReservedHHSKU表中无相关包裹且HH查询订单不成功，无需取消HH订单。");
                }


                //3：将OrderPacks_ReservedHHSKU中对应订单修改为 Refund 状态
                if (listReserved.IsNullOrEmpty())
                {
                    CLSLogger.Info("修改OrderPacks_ReservedHHSKU中对应包裹终止", $"OrderPacks_ReservedHHSKU表无对应orderpackno", LogTags);
                }
                else
                {
                    foreach (var item in listReserved)
                    {
                        item.Status = OrderPackReservedStatusEnum.Refund.ToString();
                    }
                    log.Append("修改OrderPacks_ReservedHHSKU中对应包裹的状态为Refund成功。");
                }

                //4：将OrderPacks_WMSQueue中对应订单修改为 Refund 状态
                var listQueuedOrderPack = pmsContext.OrderPackWMSQueues.Where(x => x.OrderPackNo == inputDto.order_no).ToList();

                if (listQueuedOrderPack.IsNullOrEmpty())
                {
                    CLSLogger.Info("修改OrderPacks_WMSQueue中对应包裹终止", $"OrderPacks_WMSQueue表无对应orderpackno", LogTags);
                }
                else
                {
                    foreach (var item in listQueuedOrderPack)
                    {
                        item.Status = QueueStatusEnum.Refund.ToString();
                    }

                    log.Append("修改OrderPacks_WMSQueue中对应包裹的状态为Refund成功。");
                }

                //5：若OrderPacks_OutOfStock有相关缺货数据，则删除所有
                var listOutOfStock = pmsContext.OrderPackOutOfStocks.Where(x => x.OrderPackNo == inputDto.order_no).ToList();

                if (!listOutOfStock.IsNullOrEmpty())
                {
                    StringBuilder outOfStockInfo = new StringBuilder();
                    foreach (var item in listOutOfStock)
                    {
                        outOfStockInfo.Append($"缺货SKU：{item.SKU}，QTY：{item.LackQty}，缺货解决状态：{item.Status}。");
                        pmsContext.OrderPackOutOfStocks.Remove(item);
                    }

                    log.Append("此单有缺货信息，删除OrderPacks_OutOfStock中对应包裹的缺货信息成功。");
                    log.Append(outOfStockInfo.ToString());
                }

                pmsContext.SaveChanges();
                CLSLogger.Info("取消包裹流程结束", log.ToString(), LogTags);
            }
            catch (Exception ex)
            {
                CLSLogger.Error("cancel HH order encountered an exception", ex, LogTags);
                throw ex;
            }
        }
    }
}
