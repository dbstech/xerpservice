﻿using Abp.AutoMapper;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.XERP.BackgroundJob
{
    public class SyncProductCostPriceFromHHJob : CommonBackgroundJob<List<SyncProductCostPriceDTO>>
    {
        public override void Process(List<SyncProductCostPriceDTO> inputDto)
        {
            int pageSize = 100;
            int countForSuccess = 0;

            var reqJson = inputDto.ToJsonString();
            CLSLogger.Info("记录共享库存SKU价格变化Job请求报文", $"共{inputDto.Count()}条等待处理。为：{reqJson}", LogTags);

            if (inputDto.IsNullOrEmpty())
            {
                CLSLogger.Info("记录共享库存SKU价格变化Job失败", $"失败原因：input为空。input：{inputDto.ToJsonString()}", LogTags);
                return;
            }

            try
            {
                PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();

                var totalCount = inputDto.Count();
                int totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;

                for (int i = 0; i < totalPage; i++)
                {
                    StringBuilder logBuilder = new StringBuilder();

                    var currentIteration = inputDto.OrderBy(x => x.SKU).Skip(i * pageSize).Take(pageSize).ToList();

                    List<string> listSuccessSKU = new List<string>();

                    foreach (var item in currentIteration)
                    {
                        var elementToBeAdded = new SyncProductCostPriceEntity
                        {
                            SKU = item.SKU,
                            BeforePrice = item.BeforePrice,
                            AfterPrice = item.AfterPrice,
                            PriceGap = item.AfterPrice - item.BeforePrice,
                            CreationTime = DateTime.UtcNow
                        };
                        elementToBeAdded.CreationTime = DateTime.UtcNow;

                        pmsContext.SyncProductCostPriceEntities.Add(elementToBeAdded);
                        pmsContext.SaveChanges();
                        logBuilder.Append($"成功新增一个，{item.ToJsonString()}。");
                        listSuccessSKU.Add(item.SKU);
                        countForSuccess++;
                    }

                    LogTags["sku"] = string.Join(";", listSuccessSKU);

                    CLSLogger.Info("记录共享库存SKU价格变化完成一组", $"成功同步{listSuccessSKU.Count()}个。成功明细：{logBuilder.ToString()}", LogTags);
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("记录共享库存SKU价格变化Job发生异常", ex, LogTags);
                throw ex;
            }
            finally
            {
                CLSLogger.Info("记录共享库存SKU价格变化Job结束", $"成功记录{countForSuccess}个", LogTags);
            }
        }
    }
}
