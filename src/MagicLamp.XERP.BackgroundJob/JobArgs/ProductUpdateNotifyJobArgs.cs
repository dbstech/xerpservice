using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Security.Principal;
using MagicLamp.PMS.DTOs;

[Serializable]
public class ProductUpdateNotifyJobArgs
{
    public List<ProductInfoMqDto> Products { get; set; }
    public string UserId { get; set; }
    public string UserName { get; set; }

    public DateTime CreatedAt { get; set; }
}