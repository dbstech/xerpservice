using System.Collections.Generic;
using Abp.Dependency;
using MagicLamp.XERP.BackgroundJob.Common;
using MagicLamp.PMS.Proxy;
using System.Threading.Tasks;
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Entities;
using Abp.Extensions;

namespace MagicLamp.XERP.BackgroundJob
{
    public class SyncOdooProductBrandJob : ITransientDependency
    {


        public SyncOdooProductBrandJob()
        {
        }

        public async Task Process()
        {
            PMSDbContext pmsDbContext = new PMSDbContextFactory().CreateDbContext();
            OdooProxy odooProxy = new OdooProxy();

            try
            {
                await odooProxy.LoginToOdoo();
            }
            catch (Exception ex)
            {
                CLSLogger.ErrorWithSentry("同步odoo品牌,方法:SyncOdooProductBrandJob 登录发生异常", ex);
                throw ex;
            }
            

            var odooBrands = await odooProxy.GetAllProductBrands();
            var aladdinBrands = pmsDbContext.Brands.ToList();
            var matchingBrands = new List<BrandEntity> { };
            var aladdinBrandsNotInOdoo = new List<BrandEntity> { };

            var brandLevels = odooBrands.Select(x => x["brand_level"].ToString()).Distinct();

            foreach (var brandLevel in brandLevels)
            {
                if (brandLevel == "False")
                {
                    continue;
                }
                pmsDbContext.Add(new BrandCategoryEntity
                {
                    Name = brandLevel
                });
            }

            pmsDbContext.SaveChanges();

            var brandCategories = pmsDbContext.BrandCategory.ToList();

            foreach (var brand in aladdinBrands)
            {
                var matchingBrand = odooBrands.FirstOrDefault(x => x["name"].ToString().ToUpper() == brand.EnglishName.ToUpper());
                if (matchingBrand != null)
                {
                    brand.OdooBrandId = matchingBrand["id"].ToObject<long>();
                    var brandLevel = matchingBrand["brand_level"].ToString();
                    if (!brandLevel.IsNullOrWhiteSpace() && brandLevel != "False")
                    {
                        brand.BrandCategoryId = brandCategories.FirstOrDefault(x => x.Name == brandLevel)?.Id;
                    }
                    matchingBrands.Add(brand);
                }
                else
                {
                    var odooBrandId = await odooProxy.CreateProductBrand(brand.EnglishName, brand.ChineseName);
                    brand.OdooBrandId = odooBrandId;
                    aladdinBrandsNotInOdoo.Add(brand);
                }
            }

            pmsDbContext.SaveChanges();

            CLSLogger.Info("AladdinBrandsNotInOdoo", JsonConvert.SerializeObject(aladdinBrandsNotInOdoo));
        }
    }
}