﻿using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Caching;
using MagicLamp.PMS.Domain;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Proxy.ECinx;
using MagicLamp.XERP.BackgroundJob.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.XERP.BackgroundJob
{
    public class SyncStockFromEcinxJob : CommonBackgroundJob<List<SyncEcinxStockDTO>>
    {

        private SendNotificationToDingDingJob _dingdingNotificationJob = new SendNotificationToDingDingJob();
        private ECinxClient _EcinxClient; //连接Ecinx同步产品
        private ECinxClient _WHYCEcinxClient; //连接WHYC Ecinx同步产品


        public SyncStockFromEcinxJob(
        IBackgroundJobManager backgroundJobManager
    )
        {
            ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
            _EcinxClient = new ECinxClient(cacheManager, ECinxClient.EcinxAccountEnum.CQBSC); //连接Ecinx同步产品 任选一个数据源即可）
            _WHYCEcinxClient = new ECinxClient(cacheManager, ECinxClient.EcinxAccountEnum.YIWUYINGCHI); //连接WHYC Ecinx同步产品 任选一个数据源即可）
        }

        public override void Process(List<SyncEcinxStockDTO> dtos)
        {

            int totalCountForUnsuccessful = 0;
            int totalCountForSynced = 0;

            try
            {
                if (dtos.Count ==0 )
                {
                    return;
                }


                HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
                PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();
                ProductDomainService productDomainService = new ProductDomainService();

                StringBuilder logForFailed = new StringBuilder();
                StringBuilder logForSucceed = new StringBuilder();

                int iterationCountForSynced = 0;
                int iterationCountForUnsuccessful = 0;

                List<string> listEcinxSKU = dtos.Select(x => x.SKU.Trim()).ToList();
                //only query product that is from nz warehouse
                List<ProductEntity> listProductInERP = pmsContext.Products.Where(x => listEcinxSKU.Contains(x.SKU) && x.ActiveFlag == true).ToList();
                //get api stock based on sku
                List<APIStockEntity> listAPIStocks = heERPContext.APIStocks.Where(x => listEcinxSKU.Contains(x.SKU)).ToList();

                var listERPSKUFromProducts = listProductInERP.Select(x => x.SKU.Trim()).ToList();
                //update product cache
                productDomainService.UpdateProductStockCache(listAPIStocks);
                //update qty_hh for api stock
                UpdateAPIStockForCurrentIteration(heERPContext, listAPIStocks, dtos, listProductInERP, iterationCountForUnsuccessful, iterationCountForSynced, totalCountForUnsuccessful, totalCountForSynced, logForFailed, logForSucceed);

                //update cost_pric for bas.product
                UpdateCostPricekForCurrentIteration(dtos, listProductInERP, pmsContext);

                //trigger the job of sync stock to 188                    
                IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
                backgroundJobManager.Enqueue<SyncAladdinStockJob, List<string>>(listERPSKUFromProducts, BackgroundJobPriority.High);

            
            }
            catch (Exception ex)
            {
                //此处发现数据库异常，需要处理
                CLSLogger.Error($"同步Ecinx库存发生异常", ex, LogTags);
                //throw ex;
            }
            finally
            {
                CLSLogger.Info("同步Ecinx库存结束", $"成功同步{totalCountForSynced}个，失败{totalCountForUnsuccessful}个。", LogTags);
            }
        }

        private void UpdateAPIStockForCurrentIteration(HEERPDbContext heERPContext, List<APIStockEntity> listAPIStocks, List<SyncEcinxStockDTO> currentIterationHHSKU, List<ProductEntity> listProductInERP, int iterationCountForUnsuccessful, int iterationCountForSynced, int totalCountForUnsuccessful, int totalCountForSynced, StringBuilder logForFailed, StringBuilder logForSucceed)
        {
            foreach (var item in currentIterationHHSKU)
            {
                var ERPSKU = item.SKU;

                //cannot find sku in erp
                //this comparison operation is case sensitive
                var APIStockInfo = listAPIStocks.FirstOrDefault(x => x.SKU.StandardizeSKU() == ERPSKU.StandardizeSKU());

                if (APIStockInfo == null)
                {
                    string MessageContent = $"全量同步Ecinx, ERP未找到此SKU：{ ERPSKU.StandardizeSKU()}";
                    _dingdingNotificationJob.SendEcinxERPAlert("全量同步Ecinx库存异常", MessageContent);
                    continue;
                }

               
                //cannot find entry in apistocks
                if (APIStockInfo.Qty_hh.HasValue && APIStockInfo.Qty_hh.Value == item.StockQty
                         && !APIStockInfo.Expiry_date_hh.IsNullOrEmpty() && APIStockInfo.Expiry_date_hh == item.ExpiryDate) //item.ExpiryDate.Value.ToString("MM/yyyy")
                {
                    logForSucceed.Append($"跳过更新，SKUFromERP：{APIStockInfo.SKU}，SKUFromEcinx：{item.SKU}，Barcode：{item.Barcode}，" +
                        $"StockQty：{item.StockQty}，ExpiryDate：{item.ExpiryDate}。");
                }
                else
                {
                    DateTime temp_Expiry_date_hh = DateTime.MinValue;
                    string Expiry_date_hh = string.IsNullOrWhiteSpace(item.ExpiryDate) ? "" :
                                                        DateTime.TryParse(item.ExpiryDate, out temp_Expiry_date_hh) ? DateTime.Parse(item.ExpiryDate).ToString("MM/yyyy") : DateTime.Now.AddMonths(6).ToString("MM/yyyy");
                    
                    APIStockInfo.Qty_hh = item.StockQty;
                    APIStockInfo.Expiry_date_hh = Expiry_date_hh;
                    //APIStockInfo.Expired_date = item.ExpiryDate;
                    APIStockInfo.Updated_time = DateTime.UtcNow.ConvertUTCTimeToNZTime();
                    totalCountForSynced++;
                    iterationCountForSynced++;
                    heERPContext.SaveChanges();
                    logForSucceed.Append($"更新成功一个，SKUFromERP：{APIStockInfo.SKU}，SKUFromEcinx：{item.SKU}，Barcode：{item.Barcode}，" +
                            $"StockQty：{item.StockQty}，ExpiryDate：{Expiry_date_hh}。");
                }
            }

            var processedSKU = currentIterationHHSKU.Select(x => x.SKU).ToList();
            LogTags["sku"] = string.Join(";", processedSKU);
            CLSLogger.Info("同步Ecinx库存完成一组", $"成功同步{iterationCountForSynced}个，失败{iterationCountForUnsuccessful}个。成功明细：{ logForSucceed.ToString()} | 失败明细：{ logForFailed.ToString()}", LogTags);
        }


        private async void UpdateCostPricekForCurrentIteration(List<SyncEcinxStockDTO> currentIterationHHSKU, List<ProductEntity> listProductInERP, PMSDbContext pmsContext)
        {
            foreach (var item in currentIterationHHSKU)
            {
                var ERPSKU = item.SKU;

                //this comparison operation is case sensitive
                var BASProuctInfo = listProductInERP.FirstOrDefault(x => x.SKU.StandardizeSKU() == ERPSKU.StandardizeSKU());
                if (BASProuctInfo == null)
                {
                    //string MessageContent = $"同步Ecinx, ERP未找到此SKU：{ERPSKU.StandardizeSKU()}";
                    //_dingdingNotificationJob.SendEcinxERPAlert("同步Ecinx库存异常", MessageContent);
                    continue;
                }

                GoodsQueryResponse queryResponse;
                // Some SKU Don't need Sync Cost Price
                EcinxConfigerData config = EcinxConfigerData.Configuration;
                //WHYC
                if (config.PushWHYCMFDWarehouseID.Contains((int)BASProuctInfo.WarehouseID))
                {
                    queryResponse = await _WHYCEcinxClient.GoodsQuery(new GoodsQueryInput { code = ERPSKU });
                }
                //MFD
                else
                {
                    queryResponse = await _EcinxClient.GoodsQuery(new GoodsQueryInput { code = ERPSKU });
                }

                if (queryResponse.result == null)
                {
                    continue;
                }

                if (queryResponse.result.total == 0 )
                {
                    string MessageContent = $" Ecinx未找到此SKU：{ERPSKU.StandardizeSKU()} 不能更新ERP的成本价格,系统跳过更新成本";
                    _dingdingNotificationJob.SendEcinxERPAlert("读取Ecinx的SKU异常", MessageContent);
                    continue;
                }


                foreach (var productitem in queryResponse.result.rows)
                {
                    string remak = "";
                    if (productitem.remark == null)
                    {
                        remak = "";
                    }
                    else
                    {
                        remak = productitem.remark.ToString();
                    }


                    
                    if (remak == "" )
                    {
                        Double ProductCost = string.IsNullOrWhiteSpace(BASProuctInfo.Cost.ToString()) ? 0.00 : Double.Parse(BASProuctInfo.Cost.ToString());
                        //Diff more than 0.1 yuan 
                        if (Math.Abs(ProductCost - Double.Parse(item.CostPrice.ToString())) > 0.1)
                        {
                            EditLogEntity logHeader = new EditLogEntity
                            {
                                System = "pms",
                                Type = "Cost",
                                Content = $"{ERPSKU}:{BASProuctInfo.Cost}=>{item.CostPrice}",
                                Uesr = "Ecinx_SYNC",
                                DateTime = DateTime.UtcNow,
                            };
                            //add log
                            pmsContext.EditLogs.Add(logHeader);

                            BASProuctInfo.Currency = "CNY";
                            BASProuctInfo.Cost = Convert.ToDecimal(item.CostPrice);
                            pmsContext.SaveChanges();

                            string CostPriceChangedMessageContent = $" ##### **成本变化通知**:  Ecinx 采购价格变化 \n\n";
                            CostPriceChangedMessageContent = CostPriceChangedMessageContent + $" ##### **SKU**: {ERPSKU.StandardizeSKU()}  \n\n";
                            CostPriceChangedMessageContent = CostPriceChangedMessageContent + $" ##### **SKU NAME**:   {BASProuctInfo.EnglishName} \n\n";

                            if (ProductCost - item.CostPrice > 0)
                            {
                                CostPriceChangedMessageContent = CostPriceChangedMessageContent + $" ##### **变化情况**: before:¥{ProductCost} After: ¥{item.CostPrice} **_<font color=#FF0000 face=黑体>⤵</font>_** \n\n";
                            }
                            else
                            {
                                CostPriceChangedMessageContent = CostPriceChangedMessageContent + $" ##### **变化情况**: before:¥{ProductCost} After: ¥{item.CostPrice} **_<font color=#184f26 face=黑体>⤴</font>_** \n\n";
                            }
                            _dingdingNotificationJob.SendEcinxCostPriceChangeAlert("Ecinx 成本变化告警", CostPriceChangedMessageContent);
                            continue;
                        }
                    }
                    else
                    {
                        if (productitem.remark.ToString() == "禁用自动同步成本")
                        {
                            string MessageContent = $" {ERPSKU.StandardizeSKU()} 禁用自动同步成本,系统跳过更新成本";
                            //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx 禁用自动同步成本", MessageContent);
                            continue;
                        }

                    }
                }

            }
        }



    }


    public class SyncEcinxStockDTO
    {
        /// <summary>
        /// SKU编号
        /// </summary>
        public string SKU { get; set; }
        /// <summary>
        /// 库存数量
        /// </summary>
        public int StockQty { get; set; }
        /// <summary>
        /// Barcode
        /// </summary>
        public string Barcode { get; set; }
        /// <summary>
        /// 保质期
        /// </summary>
        public string ExpiryDate { get; set; }

        /// <summary>
        /// CostPrice
        /// </summary>
        public double CostPrice { get; set; }

    }
}
