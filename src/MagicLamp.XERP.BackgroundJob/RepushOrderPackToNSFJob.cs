﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.XERP.BackgroundJob
{
    public class RepushOrderPackToNSFJob : CommonBackgroundJob<string>
    {

        public override void Process(string param)
        {
            RepushOrderPack();
        }

        [SkipConcurrentExecution(60 * 120)]
        public void RepushOrderPack()
        {
            List<string> listNSF = new List<string> { "nsf", "sfnsf", "nsfyd", "nsfau", "nsfaunoqrcode" };

            try
            {
                HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
                var listOrderPacks = heERPContext.OrderPacks.Where(x => x.sycNSFFlag == null && x.status == 4 && listNSF.Contains(x.freightId) && x.pushOrderTime > DateTime.UtcNow.AddMonths(-2)).ToList();

                for (int i = 0; i < listOrderPacks.Count; i++)
                {
                    NSFOrderDTO NSFOrder = new NSFOrderDTO
                    {
                        opid = listOrderPacks[i].opID,
                        orderid = listOrderPacks[i].orderID,
                        orderpackno = listOrderPacks[i].orderPackNo,
                        carrierid = listOrderPacks[i].carrierId,
                        freightid = listOrderPacks[i].freightId,
                        exportflag = listOrderPacks[i].exportFlag.Value,
                        status = listOrderPacks[i].status.Value,
                        packweight = listOrderPacks[i].weightVal
                    };

                    NSFOrder.Action = NSFBackgroundJobActionEnum.CreateOrder;
                    IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
                    backgroundJobManager.Enqueue<NSFBackgroundJob, NSFOrderDTO>(NSFOrder);
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("RepushOrderPackToNSFJob失败", ex, LogTags);
            }
            finally
            {
                CLSLogger.Info("RepushOrderPackToNSFJob完成", $"Job finished.", LogTags);
            }
        }
    }
}
