﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Cmq_SDK;
using Hangfire;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MagicLamp.PMS.DTOs.ThirdPart;
using Newtonsoft.Json.Linq;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.DTOs.MqDTO;
using System.Net;
using System.IO;
using System.Threading.Tasks;

namespace MagicLamp.XERP.BackgroundJob
{
    public class IDCardUploadJob : CommonBackgroundJob<IDCardUploadMqDto>
    {
        public IDCardUploadJob()
        {

        }

        public override void Process(IDCardUploadMqDto dto)
        {
            switch (dto.FreightCode)
            {
                case PMS.DTOs.Enums.FreightCodeEnum.Unknow:
                    CLSLogger.Error("Unknow freight code", "", LogTags);
                    break;
                case PMS.DTOs.Enums.FreightCodeEnum.AUExpress:
                    UploadAUExpress(dto);
                    break;
                case PMS.DTOs.Enums.FreightCodeEnum.ChangJiangExpress:
                    UploadChangJiangExpress(dto);
                    break;

                case PMS.DTOs.Enums.FreightCodeEnum.FTD:
                    UploaFTDExpress(dto);
                    break;
                case PMS.DTOs.Enums.FreightCodeEnum.FTDEXPRESSML:
                    UploaFTDExpress(dto);
                    break;
                case PMS.DTOs.Enums.FreightCodeEnum.FTDSF:
                    UploaFTDExpress(dto);
                    break;
                case PMS.DTOs.Enums.FreightCodeEnum.HYFTDMILK:
                    UploaFTDExpress(dto);
                    break;

                case PMS.DTOs.Enums.FreightCodeEnum.FENGTIAN:
                    UploaFENGTIANExpress(dto);
                    break;

                default:
                    break;
            }
        }
        protected void UploadAUExpress(IDCardUploadMqDto dto)
        {
            HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
            var config = heERPContext.APIConfigs.FirstOrDefault(x => x.ConfigKey == "AUExpressAccount");

            var jObject = JObject.Parse(config.ConfigValue);

            if (jObject == null || !jObject.HasValues)
            {
                throw new Exception("请检查澳邮账号配置");
            }

            string username = jObject["UserName"].Value<string>();
            string password = jObject["Password"].Value<string>();

            AUExpressProxy aUExpressProxy = new AUExpressProxy(username, password);
            AliyunOSSProxy aliyunOSSProxy = AliyunOSSProxy.Instance;

            var frontImage = aliyunOSSProxy.DownloadImage(dto.PhotoFront);
            var rearImage = aliyunOSSProxy.DownloadImage(dto.PhotoRear);

            AUExpressUploadIDCardDTO auDTO = new AUExpressUploadIDCardDTO
            {
                OrderIDs = dto.TrackNos,
                PhotoFront = Ultities.ConvertImageToBase64(frontImage),
                PhotoRear = Ultities.ConvertImageToBase64(rearImage),
                PhotoID = dto.IDCardNo,
                ReceiverName = dto.ReceiverName,
                ReceiverPhone = dto.ReceiverPhone,
                OrderPackNo = dto.OrderPackNo
            };

            string error = string.Empty;
            bool success = aUExpressProxy.UploadIDCard(auDTO, ref error);

            if (!success && error.Contains("上传失败"))
            {
                Retry.Do(() =>
                {
                    aUExpressProxy.UploadIDCard(auDTO, ref error);
                }, TimeSpan.FromMilliseconds(500));
            }

        }
        protected void UploadChangJiangExpress(IDCardUploadMqDto dto)
        {
            AliyunOSSProxy aliyunOSSProxy = AliyunOSSProxy.Instance;

            var frontImage = aliyunOSSProxy.DownloadImage(dto.PhotoFront);
            var rearImage = aliyunOSSProxy.DownloadImage(dto.PhotoRear);

            CJIDCardInfoDTO changJiangFrontDto = new CJIDCardInfoDTO
            {
                data = Ultities.ConvertImageToBase64(frontImage),
                type = "a",
                idcard = dto.IDCardNo,
                name = dto.ReceiverName,
                mobile = dto.ReceiverPhone
            };
            var result = ChangJiangExpressProxy.UploadIDCard(changJiangFrontDto);


            //正面上传正常后，上传反面
            if (result != null && result.Success)
            {
                CJIDCardInfoDTO changJiangRearDto = new CJIDCardInfoDTO
                {
                    data = Ultities.ConvertImageToBase64(rearImage),
                    type = "b",
                    idcard = dto.IDCardNo,
                    name = dto.ReceiverName,
                    mobile = dto.ReceiverPhone
                };
                ChangJiangExpressProxy.UploadIDCard(changJiangRearDto);
            }

        }
        protected async void UploaFTDExpress(IDCardUploadMqDto dto)
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "FTDloadIDCardLog";
            tags["IDCard"] = dto.IDCardNo;

            WebClient myWebClient = new WebClient();

            AliyunOSSProxy aliyunOSSProxy = AliyunOSSProxy.Instance;

            var frontImage = ImageToByteArray(aliyunOSSProxy.DownloadImage(dto.PhotoFront));
            var rearImage = ImageToByteArray(aliyunOSSProxy.DownloadImage(dto.PhotoRear));

            FTDIDCardInfoDTO FTDFrontDto = new FTDIDCardInfoDTO
            {
                name = dto.ReceiverName,
                id = dto.IDCardNo,
                card_full = null,
                card_front = frontImage,
                card_back = rearImage,
                card_frontname = dto.PhotoFront,
                card_backtname = dto.PhotoRear

            };
            var result = await FTDExpressProxy.UploadIDCard(FTDFrontDto);
            //Test;
            CLSLogger.Info("FTDExpressProxy", $"重试:{!result.success} 结果:{result} ", tags);

            //重试 失败并且不是重复传，才重试!
            if (!result.success)
            {
                Retry.Do(() =>
                {
                    FTDExpressProxy.UploadIDCard(FTDFrontDto);
                }, TimeSpan.FromMilliseconds(500));
            }
        }


        protected void UploaFENGTIANExpress(IDCardUploadMqDto dto)
        {
            FENGTIANExpressProxy ft = new FENGTIANExpressProxy();

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "FENGTIANloadIDCardLog";
            tags["IDCard"] = dto.IDCardNo;

            WebClient myWebClient = new WebClient();

            AliyunOSSProxy aliyunOSSProxy = AliyunOSSProxy.Instance;

            var frontImage = ImageToByteArray(aliyunOSSProxy.DownloadImage(dto.PhotoFront));
            var rearImage = ImageToByteArray(aliyunOSSProxy.DownloadImage(dto.PhotoRear));

            // get file extion;
            string frontImageExtion = System.IO.Path.GetExtension(dto.PhotoFront).Substring(1, (System.IO.Path.GetExtension(dto.PhotoFront)).Length - 1);
            string rearImageExtion = System.IO.Path.GetExtension(dto.PhotoRear).Substring(1, (System.IO.Path.GetExtension(dto.PhotoRear)).Length - 1);

            string frontImageType = string.Format("data:image/{0};base64,", frontImageExtion);
            string rearImageType = string.Format("data:image/{0};base64,", rearImageExtion);


            FENGTIANIDCardInfoDTO FTFrontDto = new FENGTIANIDCardInfoDTO
            {
                name = dto.ReceiverName,
                inum = dto.IDCardNo,
                phone = dto.ReceiverPhone,
                b_img = rearImageType + Convert.ToBase64String(rearImage, 0, rearImage.Length),
                f_img = frontImageType +  Convert.ToBase64String(frontImage, 0, frontImage.Length)
            };

            var result =  ft.UploadIDCard(FTFrontDto);
            //Test;
            CLSLogger.Info("fengtianExpressProxy 结果:", $"结果:{result} ", tags);

            //重试 失败并且不是重复传，才重试!
            if (!result.Contains("100"))
            {
                Retry.Do(() =>
                {
                    var retry = ft.UploadIDCard(FTFrontDto);
                }, TimeSpan.FromMilliseconds(500));
            }
        }


        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }


       

    }
}
