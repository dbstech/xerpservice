using System.Collections.Generic;
using Abp.Dependency;
using MagicLamp.XERP.BackgroundJob.Common;
using MagicLamp.PMS.Proxy;
using System.Threading.Tasks;
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.DTOs;
using Abp.AutoMapper;
using MagicLamp.PMS.Proxy;
using Abp.ObjectMapping;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.BackgroundJobs;
using OdooRpc.CoreCLR.Client.Models;
using System.Text;
using MagicLamp.PMS.DTOs.Enums;
using OdooRpc.CoreCLR.Client.Models.Parameters;
using Microsoft.EntityFrameworkCore;

namespace MagicLamp.XERP.BackgroundJob
{
    public class CreateOrUpdateOdooBrand : AsyncBackgroundJob<List<int>>, ITransientDependency
    {

        public CreateOrUpdateOdooBrand()
        {
        }
        protected override async Task ExecuteAsync(List<int> brandIds)
        {
            PMSDbContext pmsDbContext = new PMSDbContextFactory().CreateDbContext();
            List<BrandEntity> brands;
            if (brandIds.Count == 0)
            {
                brands = pmsDbContext.Brands.Include(x => x.BrandCategory).Where(x =>
                x.ActiveFlag &&
                (x.OdooSyncStatus == OdooSyncStatusEnum.Pending.ToString() || x.OdooSyncStatus == OdooSyncStatusEnum.Failed.ToString())
                ).ToList();
            }
            else
            {
                brands = pmsDbContext.Brands.Include(x => x.BrandCategory).Where(x => brandIds.Contains(x.Id)).ToList();
            }

            var brandEngNames = brands.Select(x => x.EnglishName);

            IDictionary<string, string> exceptions = new Dictionary<string, string>();
            StringBuilder exMsg = new StringBuilder();
            StringBuilder successMsg = new StringBuilder();

            OdooProxy proxy = new OdooProxy();
            try
            {
                await proxy.LoginToOdoo();
            }
            catch (Exception ex)
            {
                CLSLogger.ErrorWithSentry("批量创建或更新odoo品牌登录发生异常", ex);
                throw ex;
            }

            var brandEngName = "";
            BrandEntity entity = null;
            try
            {
                var odooBrands = await proxy.GetProductBrands(
                    new OdooDomainFilter().Filter("name", "in", brandEngNames),
                    new OdooFieldParameters()
                    {
                        "id",
                        "name"
                    }
                );

                foreach (var brand in brands)
                {
                    brandEngName = brand.EnglishName;
                    entity = brand;
                    if (entity.OdooSyncStatus == OdooSyncStatusEnum.Success.ToString())
                    {
                        continue;
                    }
                    var odooBrand = odooBrands.FirstOrDefault(x => x["name"].ToString().ToUpper() == entity.EnglishName.ToUpper());

                    if (entity.OdooBrandId.HasValue)
                    {
                        long odooBrandId = entity.OdooBrandId ?? throw new Exception($"品牌id: {entity.Id}并未关联odoo");
                        await proxy.UpdateProductBrand(odooBrandId, brand.EnglishName, brand.ChineseName, brand?.BrandCategory.Name);
                    }
                    else
                    {
                        if (odooBrand == null)
                        {
                            long odooBrandId = await proxy.CreateProductBrand(brand.EnglishName, brand.ChineseName, brand?.BrandCategory?.Name);
                            entity.OdooBrandId = odooBrandId;
                        }
                        else
                        {
                            long odooBrandId = odooBrand["id"].ToObject<long>();
                            entity.OdooBrandId = odooBrandId;
                            await proxy.UpdateProductBrand(odooBrandId, brand.EnglishName, brand.ChineseName, brand?.BrandCategory?.Name);
                        }
                    }

                    successMsg.Append($"{brandEngName} \n\n");
                    entity.OdooSyncStatus = OdooSyncStatusEnum.Success.ToString();
                    pmsDbContext.SaveChanges();
                }
            }
            catch (RpcCallException ex)
            {
                exceptions.Add(brandEngName, ex.RpcErrorData.ToString());
                exMsg.Append($"{brandEngName} \n\n");
                exMsg.Append($"失败原因 \n\n");
                exMsg.Append($"{ex.RpcErrorData.ToString()} \n\n");
                if (entity != null)
                {
                    entity.OdooSyncStatus = OdooSyncStatusEnum.Failed.ToString();
                }
            }
            catch (Exception ex)
            {
                exceptions.Add(brandEngName, ex.Message);
                exMsg.Append($"{brandEngName} \n\n");
                exMsg.Append($"失败原因 \n\n");
                exMsg.Append($"{ex.Message} \n\n");
                if (entity != null)
                {
                    entity.OdooSyncStatus = OdooSyncStatusEnum.Failed.ToString();
                }
            }

            pmsDbContext.SaveChanges();

            string ddToken = "7ae174384da0978a10ded1e234f8004759eaade08f7ba3b1b4e6d4169595041e";
            string ddSignature = "SECaab5cd16c36283e1c6c67a3fa738e5728a99e20deb2839eda33708492382d10b";

            if (successMsg.Length > 0)
            {
                string title = "# 以下品牌同步odoo成功 \n\n";
                successMsg.Insert(0, title);
                DDNotificationProxy.MarkdownNotification(title, successMsg.ToString(), ddToken, ddSignature);
            }

            if (exceptions.Count > 0)
            {
                CLSLogger.Info("批量創建或更新odoo品牌发生异常", exMsg.ToString());
                string title = "# 以下品牌同步odoo失败，请重新上传 \n\n";
                exMsg.Insert(0, title);
                DDNotificationProxy.MarkdownNotification(title, exMsg.ToString(), ddToken, ddSignature);
                throw new Exception(exMsg.ToString());
            }
        }
    }
}