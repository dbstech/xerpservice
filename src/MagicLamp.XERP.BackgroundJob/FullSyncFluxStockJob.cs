﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abp.BackgroundJobs;
using Abp.Dependency;
using MagicLamp.PMS;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Common;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using Newtonsoft.Json.Linq;
using TimeZoneConverter;

namespace MagicLamp.XERP.BackgroundJob
{
    public class FullSyncFluxStockJob : CommonBackgroundJob<string>
    {
        [SkipConcurrentExecution(60 * 120)]
        public override void Process(string param)
        {
            StringBuilder log = new StringBuilder();

            try
            {

                FluxWMSDbContext fluxWmsContext = new FluxWMSDbContextFactory().CreateDbContext();
                HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
                PMSDbContext pMSDbContext = new PMSDbContextFactory().CreateDbContext();

                List<string> customerIDs = new List<string> { "ALADDIN", "AU", "HE" };

                IEnumerable<string> allSkus = null;

                var config = heERPContext.APIConfigs.FirstOrDefault(x => x.ConfigKey == "FluxStockSyncConfig");
                if (config != null)
                {
                    allSkus = config.ConfigValue.ConvertFromJsonString<List<string>>();
                }

                if (allSkus.IsNullOrEmpty())
                {
                    allSkus = fluxWmsContext.FluxBasSkus.Where(x => customerIDs.Contains(x.CustomerID) && x.Active_Flag == "Y")
                    .Select(x => x.SKU).AsEnumerable();
                }
                List<string> skus = pMSDbContext.Products.Where(x => allSkus.Contains(x.SKU) && x.PurchasePeriodCode != "virtualStock").Select(x => x.SKU).ToList();

                log.Append($"Total sku count：{skus.Count}；");

                int pageSize = 500;
                int totalPage = skus.Count % pageSize == 0 ? skus.Count / pageSize : skus.Count / pageSize + 1;

                log.Append($"Total page count：{totalPage}；");

                IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
                for (int i = 0; i < totalPage; i++)
                {
                    var currentPageSkus = skus.OrderByDescending(x => x).Skip(i * pageSize).Take(pageSize).ToList();
                    backgroundJobManager.Enqueue<SyncFluxStockJob, List<string>>(currentPageSkus, BackgroundJobPriority.High);
                }

                fluxWmsContext.Dispose();
                heERPContext.Dispose();
            }
            catch (Exception ex)
            {
                CLSLogger.Error("FullSyncFluxStockJobException", ex, LogTags);
            }
            finally
            {
                CLSLogger.Info("FullSyncFluxStockJobExcuted", log.ToString(), LogTags);
            }

        }





    }
}
