﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Extensions;
using Abp.Runtime.Caching;
using MagicLamp.Flux.API.Models;
using MagicLamp.PMS;
using MagicLamp.PMS.Domain;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Common;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using Microsoft.EntityFrameworkCore.Storage;
using NPOI.OpenXmlFormats.Dml;
using TimeZoneConverter;

namespace MagicLamp.XERP.BackgroundJob
{
    public class SyncFluxStockJob : CommonBackgroundJob<List<string>>
    {

        public ICacheManager Cache { get; set; }
        private bool AddOrUpdateApiStock(HEERPDbContext heERPContext, string sku, int qty, string expiryDate, APIStockEntity existed = null)
        {
            bool updated = false;
            if (existed == null)
            {
                APIStockEntity newEntry = new APIStockEntity
                {
                    SKU = sku.Trim().ToUpper(),
                    Flag = 1,
                    //ProductNameCN = product.CN,
                    //ProductNameEN = product.EN1,
                    Qty_baseline = 0,
                    Qty_locked = 0,
                    Qty_aladdin = qty,
                    Qty_available = qty,
                    Expired_date = expiryDate,
                    Syc_flag = false,
                    Qty = qty,
                    Qty_heweb = qty,
                    Qty_cache = 0,
                    Updated_time = DateTime.Now
                };
                heERPContext.APIStocks.Add(newEntry);
                updated = true;
            }
            //only change if the stock is not the same or expiry date is not the same
            else if (qty != existed.Qty
                || qty != existed.Qty_aladdin
                || (qty - existed.Qty_locked) != existed.Qty_available
                || (!expiryDate.IsNullOrEmpty() && expiryDate != existed.Expired_date)
                || (qty == 0 && expiryDate.IsNullOrEmpty() && !existed.Expired_date.IsNullOrEmpty()))
            {
                existed.Qty_aladdin = qty;
                existed.Qty_available = qty - existed.Qty_locked;
                existed.Expired_date = expiryDate;
                existed.Syc_flag = false;
                existed.Qty = qty;
                existed.Qty_heweb = existed.Qty_available;
                existed.Updated_time = DateTime.Now;
                updated = true;
            }
            return updated;
        }
        public override void Process(List<string> listSku)
        {
            SyncStock(listSku);
        }

        [SkipConcurrentExecution(60 * 120)]
        public void SyncStock(List<string> inputList = null)
        {
            LogTags["method"] = "SyncStock";

            int pageSize = 100;
            StringBuilder pageLogBuilder = new StringBuilder();

            try
            {
                FluxWMSDbContext fluxWmsContext = new FluxWMSDbContextFactory().CreateDbContext();
                HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
                PMSDbContext pMSDbContext = new PMSDbContextFactory().CreateDbContext();
                IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
                ProductDomainService productDomainService = new ProductDomainService();

                var listUnqualifiedLocation = PMSConsts.UnqualifiedLocationId;
                var excluded = PMSConsts.excludedLocation;
                var excludedLotatt07 = PMSConsts.ExcludedLotAtt07;

                List<string> listSku;
                int totalCount = 0;

                string maxTransactionID = string.Empty;
                string maxAllocationDetailID = string.Empty;
                AppConfigurationEntity lastTransactionID = null;
                AppConfigurationEntity lastAllocationDetailID = null;

                Cache = IocManager.Instance.Resolve<ICacheManager>();
                var cacheManager = Cache.GetCache(CacheKeys.JOBCACHEKEYNAME);
                var val = cacheManager.GetOrDefault(CacheKeys.FLUXEXPIRYDESCRIPTION);

                if (val == null)
                {
                    //expiry date description
                    var listFluxBasCodeExpiry = fluxWmsContext.FluxBasCodes.Where(x => x.CodeID == "UDF_EXPIRY").ToList();

                    cacheManager.Set(CacheKeys.FLUXEXPIRYDESCRIPTION, listFluxBasCodeExpiry, TimeSpan.FromDays(1));
                }

                var listFluxExpiryName = (List<FluxBasCodeEntity>)cacheManager.GetOrDefault(CacheKeys.FLUXEXPIRYDESCRIPTION);

                if (inputList.IsNullOrEmpty())
                {
                    lastTransactionID = pMSDbContext.AppConfigurationEntities.FirstOrDefault(x => x.ConfigKey == PMSConsts.FluxStockLastSyncConfigKey);

                    lastAllocationDetailID = pMSDbContext.AppConfigurationEntities.FirstOrDefault(x => x.ConfigKey == PMSConsts.FluxAllocationDetailLastSyncConfigKey);

                    if (lastTransactionID == null || lastTransactionID.ConfigValue.IsNullOrWhiteSpace())
                    {
                        throw new Exception("Can not found config of FluxLastSyncTransactionID in AppConfiguration");
                    }

                    if (lastAllocationDetailID == null || lastAllocationDetailID.ConfigValue.IsNullOrWhiteSpace())
                    {
                        throw new Exception("Can not found config of FluxLastSyncAllocationDetailID in AppConfiguration");
                    }

                    var changedSkus = fluxWmsContext.FluxACTTransactionLogs
                        .Where(x => string.Compare(x.TransactionID, lastTransactionID.ConfigValue) > 0)
                          .Select(x => new { x.FMSKU, x.ToSku, x.TransactionID }).ToList();

                    var allocatedSKUs = fluxWmsContext.FluxACTAllocationDetails
                        .Where(x => string.Compare(x.AllocationDetailsID, lastAllocationDetailID.ConfigValue) > 0)
                        .Select(x => new { x.SKU, x.AllocationDetailsID }).ToList();

                    if (changedSkus.IsNullOrEmpty() && allocatedSKUs.IsNullOrEmpty())
                    {
                        return;
                    }

                    maxTransactionID = changedSkus.Max(x => x.TransactionID);
                    maxAllocationDetailID = allocatedSKUs.Max(x => x.AllocationDetailsID);

                    //group and distinct sku
                    List<string> excludedSku = new List<string>() { "*" };
                    List<string> editedSku = new List<string>();
                    changedSkus.ForEach(x =>
                    {
                        editedSku.Add(x.FMSKU);
                        editedSku.Add(x.ToSku);
                    });

                    allocatedSKUs.ForEach(x =>
                    {
                        editedSku.Add(x.SKU);
                    });

                    editedSku = editedSku.Where(x => !string.IsNullOrWhiteSpace(x) && !excludedSku.Contains(x)).Distinct().ToList();

                    CLSLogger.Info("库存事务变化日志及富勒分配明细新增",
                        $"库存事务变化条数：{changedSkus.Count}, 富勒分配明细新增数：{allocatedSKUs.Count}, Sku Count: {editedSku.Count} , 库存事务最大transactionID：{maxTransactionID}, 分配明细最大AllocationDetailsID：{maxAllocationDetailID}",
                        LogTags);

                    listSku = editedSku;
                }
                else
                {
                    listSku = inputList;
                }

                //filter all virtual stock sku                
                listSku = pMSDbContext.Products.Where(x => listSku.Contains(x.SKU) && x.PurchasePeriodCode != "virtualStock").Select(x => x.SKU).ToList();

                if (listSku.IsNullOrEmpty())
                {
                    return;
                }

                //query
                var queryableResult =
                    from lotLoc in fluxWmsContext.FluxInvLotLocIds
                    join lotAtt in fluxWmsContext.FluxInvLotAtts on new { lotLoc.LotNum, lotLoc.SKU } equals new { lotAtt.LotNum, lotAtt.SKU }
                    where !listUnqualifiedLocation.Contains(lotLoc.LocationID)
                          && !lotLoc.LocationID.Contains(excluded)
                          && lotAtt.LotAtt07 == "" //exclude special stocks may are allocated to specific orders
                          && listSku.Contains(lotLoc.SKU)
                    group new { lotLoc, lotAtt } by new { lotLoc.SKU, lotLoc.LotNum, lotAtt.LotAtt02, lotAtt.LotAtt04 }
                    into grp
                    select new
                    {
                        Sku = grp.Key.SKU,
                        grp.Key.LotNum,
                        Qty = grp.Sum(x => x.lotLoc.Qty - x.lotLoc.QtyAllocated - x.lotLoc.QtyOnHold),
                        ExpiryDateSpecific = grp.Key.LotAtt02,
                        ExpiryDateAfterOpen = grp.Key.LotAtt04
                    };

                var listSkuStock = queryableResult.ToList();

                var listToBeProcessed = listSkuStock.Where(x => x.Qty > 0).GroupBy(x => x.Sku)
                    .Select(x => new
                    {
                        Sku = x.Key,
                        Qty = x.Sum(y => y.Qty),
                        ExpiryDateSpecific = x.Min(y => { DateTime.TryParse(y.ExpiryDateSpecific, out DateTime resultDate); return resultDate; }),
                        ExpiryDateAfterOpen = x.Select(y => y.ExpiryDateAfterOpen).FirstOrDefault(z => !z.IsNullOrEmpty())
                    })
                    .ToList();

                totalCount = listToBeProcessed.Count;
                pageLogBuilder.Append($"同步富勒库存开始，共需同步{totalCount}个sku。");


                //计算预售采购单
                var listPoOrderHeader = pMSDbContext.PurchaseOrderHeaders.Where(x => x.ExpectedArriveTime1.HasValue && DateTime.Today >= x.ExpectedArriveTime1.Value.Date && x.ExpectedArriveTime1.Value.Date >= DateTime.Parse("03-03-2021") && x.OrderStatus == "Create").ToList();
                
                var listOfReference1 = listPoOrderHeader.Select(x => x.PurchasingOrderNo).ToList();

                if (!listOfReference1.IsNullOrEmpty())
                {
                    CLSLogger.Info("同步富勒库存存在预售采购单", listOfReference1.ToJsonString(), LogTags);
                }

                var listPoOrderLine = pMSDbContext.PurchaseOrderLines.Where(x => listOfReference1.Contains(x.PoHeaderId)).ToList();


                //pagination
                int totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;

                pageLogBuilder.Append($"待处理总数：{totalCount}，分页总页数：{totalPage}。");

                //for input sku not in the returned query result
                var listSkuFound = listToBeProcessed.Select(x => x.Sku.Trim().ToLower()).ToList();
                //find input sku that are not in the returned query result
                var listSkuNotFound = listSku.Where(x => !listSkuFound.Contains(x.Trim().ToLower())).Select(x => x.Trim().ToLower()).ToList();

                //Refresh lock qty at first
                //new RefreshSKULockQtyJob().Process(listToBeProcessed.Select(x => x.Sku).ToList());

                if (!listSkuNotFound.IsNullOrEmpty())
                {
                    pageLogBuilder.Append($"此外存在无富勒有效库存的sku，数量：{listSkuNotFound.Count}。");

                    //set stock of sku not found to 0
                    var listApiStockZero = heERPContext.APIStocks.Where(x => listSkuNotFound.Contains(x.SKU.Trim().ToLower())).ToList();

                    //update product stock cache
                    productDomainService.UpdateProductStockCache(listApiStockZero);

                    pageLogBuilder.Append($"处理apistocks中无富勒有效库存的sku，数量：{listApiStockZero.Count}。");

                    StringBuilder logZeroBuilder = new StringBuilder();
                    int count = 0;
                    foreach (var item in listApiStockZero)
                    {
                        int presaleQTY = 0;
                        var listPresaleLine = listPoOrderLine.Where(x => x.Sku.StandardizeSKU() == item.SKU.StandardizeSKU()).ToList();

                        if (!listPresaleLine.IsNullOrEmpty())
                        {
                            presaleQTY = Convert.ToInt32(listPresaleLine.Sum(y => y.QTY));
                            logZeroBuilder.Append($"{item.SKU}存在预售数量：{presaleQTY}。");

                            CLSLogger.Info("同步富勒库存sku存在预售数量", $"SKU：{item.SKU}, 数量：{presaleQTY}", LogTags);
                        }

                        item.Qty = 0 + presaleQTY;
                        item.Expired_date = null;
                        item.Qty_aladdin = 0 + presaleQTY;
                        item.Qty_available = 0 + presaleQTY - item.Qty_locked;
                        item.Syc_flag = false;
                        item.Qty_heweb = item.Qty_available;
                        item.Expired_date = listPresaleLine.IsNullOrEmpty() ? null : item.Expiry_date_hh;
                        item.Updated_time = DateTime.Now;
                        logZeroBuilder.Append($"{item.SKU}，");
                        count++;
                    }
                    logZeroBuilder.Insert(0, $"共 {count} 个SKU：");

                    
                    CLSLogger.Info("处理apistocks中无富勒有效库存的sku结束", logZeroBuilder.ToString(), LogTags);

                    pageLogBuilder.Append($"成功处理无富勒有效库存的sku{count}个。");

                    heERPContext.SaveChanges();
                }

                for (int i = 0; i < totalPage; i++)
                {
                    StringBuilder logBuilder = new StringBuilder();
                    pageLogBuilder.Append($"开始处理第{i}页。");

                    var listThisIteration = listToBeProcessed.Skip(i * pageSize).Take(pageSize).ToList();
                    var listThisIterationSku = listThisIteration.Select(x => x.Sku.Trim().ToLower()).ToList();


                    var listApiStock = heERPContext.APIStocks.Where(x => listThisIterationSku.Contains(x.SKU.Trim().ToLower())).ToList();



                    int updatedCount = 0;
                    foreach (var item in listThisIteration)
                    {
                        var sku = item.Sku;

                        var totalQty = Convert.ToInt32(item.Qty);
                        var expiryDateSpecific = item.ExpiryDateSpecific;
                        var expiryCode = listFluxExpiryName.FirstOrDefault(x => x.Code == item.ExpiryDateAfterOpen);
                        var expiryDateAfterOpen = expiryCode == null ? null : expiryCode.CodeName_C;
                        logBuilder.Append($"SKU: {sku}, Qty: {totalQty}, ExpiryDateSpecific: {expiryDateSpecific}, ExpiryDateAfterOpen: {expiryDateAfterOpen}。");

                        //预售功能
                        var listPresaleLine = listPoOrderLine.Where(x => x.Sku.StandardizeSKU() == sku.StandardizeSKU()).ToList();

                        if (!listPresaleLine.IsNullOrEmpty())
                        {
                            var presaleQTY = Convert.ToInt32(listPresaleLine.Sum(y => y.QTY));
                            logBuilder.Append($"存在预售数量：{presaleQTY}。");
                            totalQty += presaleQTY;
                            CLSLogger.Info("同步富勒库存sku存在预售数量", $"SKU：{sku}, 数量：{presaleQTY}", LogTags);
                        }

                        //if no specific expiry date, then use after open
                        var expiryDateToUse = expiryDateSpecific == DateTime.MinValue ? expiryDateAfterOpen : expiryDateSpecific.ToString("yyyy/MM/dd");

                        var existedApiStock = listApiStock.FirstOrDefault(x => x.SKU.Trim().ToLower() == sku.Trim().ToLower());

                        bool updated = AddOrUpdateApiStock(heERPContext, sku, totalQty, expiryDateToUse, existedApiStock);
                        if (updated)
                        {
                            updatedCount++;
                        }
                    }
                    heERPContext.SaveChanges();

                    //update product stock cache
                    productDomainService.UpdateProductStockCache(listApiStock);

                    //notify stock changed to 188 and yangxiaofan
                    var currentPageSkus = listThisIteration.Select(x => x.Sku).ToList();
                    backgroundJobManager.Enqueue<SyncAladdinStockJob, List<string>>(currentPageSkus, BackgroundJobPriority.High);

                    pageLogBuilder.Append($"第{i}页处理完成。");

                    LogTags["sku"] = string.Join(";", listThisIterationSku);
                    CLSLogger.Info($"同步富勒库存成功一批--{i}页", logBuilder.ToString(), LogTags);
                }

                //update last transactionID
                if (lastTransactionID != null && !string.IsNullOrWhiteSpace(maxTransactionID))
                {
                    lastTransactionID.ConfigValue = maxTransactionID;
                    lastTransactionID.UpdateTime = DateTime.UtcNow;
                    pMSDbContext.SaveChanges();
                }

                if (lastAllocationDetailID != null && !string.IsNullOrWhiteSpace(maxAllocationDetailID))
                {
                    lastAllocationDetailID.ConfigValue = maxAllocationDetailID;
                    lastAllocationDetailID.UpdateTime = DateTime.UtcNow;
                    pMSDbContext.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                CLSLogger.Error("SyncFluxStockJob.SyncStock失败", ex, LogTags);
                throw ex;
            }
            finally
            {
                LogTags["sku"] = null;
                CLSLogger.Info("SyncFluxStockJob.SyncStock完成", pageLogBuilder.ToString(), LogTags);
            }
        }
    }
}
