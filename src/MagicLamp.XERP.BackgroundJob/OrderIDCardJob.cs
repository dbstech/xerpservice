﻿using Abp.Domain.Repositories;
using MagicLamp.PMS.Entities;
using System.Linq;
using System;

using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using Hangfire;
using Abp.Runtime.Caching;
using MagicLamp.PMS.Infrastructure.Common;
using System.Collections.Generic;
using Abp.Dependency;
using System.Diagnostics;
using MagicLamp.PMS.DTOs.ThirdPart;
using Abp.BackgroundJobs;
using MagicLamp.PMS.DTOs.MqDTO;
using MagicLamp.PMS.DTOs.Enums;
using Abp.Extensions;

namespace MagicLamp.XERP.BackgroundJob
{
    public class OrderIDCardJob
    {
        public ICacheManager Cache { get; set; }
        IRepository<OrderPackEntity> orderRepository;

        public OrderIDCardJob(IRepository<OrderPackEntity> _orderRepository)
        {
            orderRepository = _orderRepository;
        }
        public OrderIDCardJob()
        {

        }

        [SkipConcurrentExecution(60 * 120)]
        public void MatchData()
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["jobid"] = Guid.NewGuid().ToString();
            tags["type"] = "OrderIDJob开始运行";

            Cache = IocManager.Instance.Resolve<ICacheManager>();

            var cacheManager = Cache.GetCache(CacheKeys.JOBCACHEKEYNAME);
            var val = cacheManager.GetOrDefault(CacheKeys.ORDERIDCARDJOBKEY);
            CLSLogger.Info("OrderIDJob开始运行", $"是否已有job正在运行：{val != null}", tags);
            if (val == null)
            {
                cacheManager.Set(CacheKeys.ORDERIDCARDJOBKEY, true);
            }
            else
            {
                return;
            }

            System.Text.StringBuilder log = new System.Text.StringBuilder();
            Stopwatch stopwatch = new Stopwatch();
            Stopwatch queryWatch = new Stopwatch();
            stopwatch.Start();
            try
            {
                HEERPDbContext hEERPDbContext = new HEERPDbContextFactory().CreateDbContext(null);
                PMSDbContext pMSDbContext = new PMSDbContextFactory().CreateDbContext(null);
                DateTime dateTime = DateTime.Now.AddDays(-10);

                queryWatch.Start();

                var query = from ops in hEERPDbContext.OrderPacks
                            where ops.opt_time > dateTime &&
                            !string.IsNullOrWhiteSpace(ops.idNo)
                            select ops;

                int count = query.Count();

                queryWatch.Stop();
                log.Append($"查询count耗时：{queryWatch.ElapsedMilliseconds} ms，总条数：{count}；");

                int pagesize = 1000;
                int pageCount = count % pagesize == 0 ? count / pagesize : count / pagesize + 1;

                for (int i = 0; i < pageCount; i++)
                {
                    queryWatch.Restart();

                    var orderPacks = query.OrderByDescending(x => x.opID).PagedQuery(i, pagesize).ToList();

                    queryWatch.Stop();
                    log.Append($"执行包裹第{i}页查询耗时：{queryWatch.ElapsedMilliseconds} ms；");


                    queryWatch.Restart();

                    var idNos = orderPacks.Select(x => x.idNo).Distinct().ToList();
                    var idcards = pMSDbContext.IDCards.Where(x => idNos.Contains(x.Number)).ToList();

                    queryWatch.Stop();
                    log.Append($"执行身份证查询耗时：{queryWatch.ElapsedMilliseconds} ms；");

                    if (idcards.IsNullOrEmpty())
                    {
                        continue;
                    }

                    int updateCount = 0;
                    int insertCount = 0;
                    //筛选身份证
                    queryWatch.Restart();

                    //澳邮身份证集合
                    Dictionary<string, IDCardUploadMqDto> idCardMqDtos = new Dictionary<string, IDCardUploadMqDto>();

                    foreach (var pack in orderPacks)
                    {
                        var idCard = idcards.FirstOrDefault(id => id.Number == pack.idNo);
                        if (idCard != null)
                        {
                            var exsitOrderIdCard = pMSDbContext.OrderIDCards.FirstOrDefault(o => o.OrderPackNo == pack.orderPackNo);
                            if (exsitOrderIdCard == null)
                            {
                                var orderIdCard = new OrderIDCardEntity()
                                {
                                    CarrierID = pack.carrierId,
                                    ExportFlag = pack.exportFlag.Value,
                                    FreightID = pack.freightId,
                                    OrderID = pack.orderID,
                                    OrderPackNo = pack.orderPackNo,
                                    RecevierName = pack.rec_name,
                                    IDCardName = idCard.Name,
                                    IDCardNo = idCard.Number,
                                    IDCardUpdateTime = idCard.UpdateTime,
                                    URLBack = idCard.URLBack,
                                    URLFont = idCard.URLFont,
                                    Verification = idCard.Verification,
                                    CreationTime = DateTime.Now,
                                    ReceiverPhone = pack.rec_phone,
                                    DateOfBirth =idCard.date_of_birth,
                                    ExpiredDate = idCard.expired_date
                                };
                                pMSDbContext.OrderIDCards.Add(orderIdCard);
                                insertCount++;
                                AddOrderIDCard(orderIdCard, idCardMqDtos);
                            }
                            else if (exsitOrderIdCard.IDCardNo != idCard.Number || exsitOrderIdCard.IDCardName != idCard.Name
                                || exsitOrderIdCard.URLBack != idCard.URLBack || exsitOrderIdCard.URLFont != idCard.URLFont
                                || exsitOrderIdCard.FreightID != pack.freightId || exsitOrderIdCard.CarrierID != pack.carrierId
                                || exsitOrderIdCard.ExportFlag != pack.exportFlag || exsitOrderIdCard.ReceiverPhone != pack.rec_phone
                                || exsitOrderIdCard.DateOfBirth != idCard.date_of_birth || exsitOrderIdCard.ExpiredDate != idCard.expired_date
                                )
                            {
                                exsitOrderIdCard.UpdateTime = DateTime.Now;
                                exsitOrderIdCard.IDCardUpdateTime = idCard.UpdateTime;
                                exsitOrderIdCard.IDCardName = idCard.Name;
                                exsitOrderIdCard.IDCardNo = idCard.Number;
                                exsitOrderIdCard.URLBack = idCard.URLBack;
                                exsitOrderIdCard.URLFont = idCard.URLFont;
                                exsitOrderIdCard.Verification = idCard.Verification;
                                exsitOrderIdCard.FreightID = pack.freightId;
                                exsitOrderIdCard.CarrierID = pack.carrierId;
                                exsitOrderIdCard.ExportFlag = pack.exportFlag.Value;
                                exsitOrderIdCard.ReceiverPhone = pack.rec_phone;
                                exsitOrderIdCard.DateOfBirth = idCard.date_of_birth;
                                exsitOrderIdCard.ExpiredDate = idCard.expired_date;

                                updateCount++;
                                AddOrderIDCard(exsitOrderIdCard, idCardMqDtos);
                            }

                        }
                    }

                    if (updateCount > 0 || insertCount > 0)
                    {
                        pMSDbContext.SaveChanges();
                    }

                    //上传澳邮身份证
                    IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
                    foreach (var item in idCardMqDtos)
                    {
                        backgroundJobManager.Enqueue<IDCardUploadJob, IDCardUploadMqDto>(item.Value);
                    }

                    queryWatch.Stop();
                    log.Append($"更新身份证耗时：{queryWatch.ElapsedMilliseconds} ms，新增条数：{insertCount}，更新条数：{updateCount}；");
                    CLSLogger.Info($"执行第{i}页查询日志详情", log.ToString(), tags);
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("OrderIDCardJob发生异常", ex);
                throw ex;
            }
            finally
            {
                cacheManager.Remove(CacheKeys.ORDERIDCARDJOBKEY);
                stopwatch.Stop();
                CLSLogger.Info("OrderIDJob结束运行", $"总耗时：{stopwatch.ElapsedMilliseconds} ms", tags);
            }

        }


        private void AddOrderIDCard(OrderIDCardEntity idCardEntity, Dictionary<string, IDCardUploadMqDto> idCardDtos)
        {

            if (idCardEntity.CarrierID.IsNullOrWhiteSpace()
                || idCardEntity.FreightID.IsNullOrWhiteSpace()
                || idCardEntity.URLFont.IsNullOrWhiteSpace()
                || idCardEntity.URLBack.IsNullOrWhiteSpace())
            {
                return;
            }

            FreightCodeEnum freightCode = FreightCodeEnum.Unknow;

            if (!Enum.TryParse<FreightCodeEnum>(idCardEntity.FreightID, true, out freightCode))
            {
                return;
            }

            string key = $"{idCardEntity.IDCardNo}-{idCardEntity.FreightID}";

            if (idCardDtos.ContainsKey(key))
            {
                if (!idCardDtos[key].TrackNos.Contains(idCardEntity.CarrierID))
                {
                    idCardDtos[key].TrackNos.Add(idCardEntity.CarrierID);
                }
            }
            else
            {
                idCardDtos[key] = new IDCardUploadMqDto
                {
                    TrackNos = new List<string> { idCardEntity.CarrierID },
                    OrderPackNo = idCardEntity.OrderPackNo,
                    IDCardNo = idCardEntity.IDCardNo,
                    ReceiverName = idCardEntity.RecevierName,
                    ReceiverPhone = idCardEntity.ReceiverPhone,
                    PhotoFront = idCardEntity.URLFont,
                    PhotoRear = idCardEntity.URLBack,
                    FreightCode = freightCode
                };
            }

        }

    }
}
