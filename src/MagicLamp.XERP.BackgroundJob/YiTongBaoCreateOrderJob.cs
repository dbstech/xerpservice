﻿using Abp.Json;
using MagicLamp.PMS.DTOs.BondedWarehouseQingDao;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.XERP.BackgroundJob
{
    /// <summary>
    /// 青岛保税仓推单任务
    /// </summary>
    public class YiTongBaoCreateOrderJob : CommonBackgroundJob<QingDaoBondedOrderInputDTO>
    {
        private PMSDbContext pmsContext;
        private HEERPDbContext heerpContext;

        public YiTongBaoCreateOrderJob()
        {
            pmsContext = new PMSDbContextFactory().CreateDbContext();
            heerpContext = new HEERPDbContextFactory().CreateDbContext();
        }

        public override void Process(QingDaoBondedOrderInputDTO inputDTO)
        {
            LogTags["method"] = "CreateOrder";
            LogTags["orderid"] = inputDTO.orderNo;


            var originalInput = inputDTO;

            try
            {
                // log input
                CLSLogger.Info("青岛保税仓下单请求报文", inputDTO.ToJsonString(), LogTags);

                List<OrderPackEntity> orderPacks = heerpContext.OrderPacks.Where(x => x.orderID == inputDTO.orderNo).ToList();
                

                if (orderPacks.IsNullOrEmpty())
                {
                    bool cacheOrder = heerpContext.CacheOrders.Any(x => x.orderid == inputDTO.orderNo);
                    var order = heerpContext.Orders.Any(x => x.orderid == inputDTO.orderNo);
                    if (cacheOrder == false && order == false)
                    {
                        CLSLogger.Info("保税仓推单时发现订单已取消", "cache order,orders and orderpacks all not found", LogTags);
                        return;
                    }

                    if (cacheOrder != false || order != false)
                    {
                        throw new Exception($"青岛保税仓下单时无orderpack，将重试。订单号：{inputDTO.orderNo}");
                    }
                }
                else
                {
                    var freezeOrderPack = orderPacks.FirstOrDefault(x => x.freezeFlag > 0);
                    if (freezeOrderPack != null)
                    {
                        CLSLogger.Info("保税仓推单时发现订单已取消", $"包裹已冻结：{freezeOrderPack.orderPackNo}  freezeflag: {freezeOrderPack.freezeFlag}  {freezeOrderPack.freezeComment}", LogTags);
                        return;
                    }
                }

                List<ProductEntity> listQingDaoProduct = pmsContext.Products.Where(x => x.BondedWarehouseItemCode != null).ToList();

                YiTongBaoOMSProxy ytbProxy = new YiTongBaoOMSProxy();

                //swap sku
                var isSKUSwapSuccessful = SwapSKUForYiTongBaoItemCode(listQingDaoProduct, inputDTO);
                if (!isSKUSwapSuccessful)
                {
                    CLSLogger.Info("保税仓推单时发现产品表无对应青岛保税仓商品编码", $"订单内包含产品为：{inputDTO.orderDetail.ToJsonString()}", LogTags);
                    return;
                }

                //verify address
                OrderEntity orderInfo = heerpContext.Orders.FirstOrDefault(x => x.orderid == inputDTO.orderNo);
                string orderPackNoToMatch = orderInfo.notes.Trim();


                int exportFlag = orderPacks.FirstOrDefault().exportFlag.Value;
                string carrierID = GetCarrierIDFromLinkedOrderPack(orderPackNoToMatch, inputDTO.orderNo, exportFlag);

                if (exportFlag == 73 && carrierID.IsNullOrEmpty())
                {
                    throw new Exception($"青岛保税仓下单时为QD奶粉渠道，但匹配的用户所下包裹无物流号，将重试。订单号：{inputDTO.orderNo}");
                }

                VerifyAndCorrectAddress(orderInfo, inputDTO);

                YiTongBaoDeliveryOrderDTO orderInput = ytbProxy.AssembleYiTongBaoCreateOrderInput(inputDTO, exportFlag, carrierID);

                var response = ytbProxy.CreateOrder(orderInput);

                string content = JObject.Parse(response.Content).ToJsonString();

                CLSLogger.Info("青岛保税仓下单响应报文", content, LogTags);
                                
                //save orderpacktrade

                if (response.IsSuccessful)
                {
                    var result = content.ConvertFromJsonString<YiTongBaoDeliveryOrderOutputDTO>();

                    if (result.code != "0")
                    {
                        //failed
                        SendNotificationToDingDingJob ddJob = new SendNotificationToDingDingJob();
                        ddJob.SendBondedWarehouseQingDaoAlert($"易通宝返回失败，请检查。订单为：{originalInput.orderNo}。易通宝返回失败原因：{result.msg}");
                    }
                    else
                    {
                        //success
                    }
                }
                else
                {
                    throw new Exception($"青岛保税仓下单接口失败，订单号：{originalInput.orderNo}");
                }


                //order pack trade
                if (orderPacks.IsNullOrEmpty())
                {
                    CLSLogger.Info("保税仓下单请求报文&业务端信息，无相关包裹",
                        $"{originalInput.ToJsonString()}，customsPaymentData: {originalInput.customsPaymentData}", LogTags);
                }
                else
                {
                    foreach (var item in orderPacks)
                    {
                        if (item.status != (int)OrderPackStatusEnum.Outbound)
                        {
                            item.status = (int)OrderPackStatusEnum.Packed;
                            pmsContext.SaveChanges();
                        }

                        OrderPackTradeEntity orderPackTrade = new OrderPackTradeEntity
                        {
                            Opid = item.opID,
                            OrderID = item.orderID,
                            OrderPackNo = item.orderPackNo,
                            CustomsPaymentData = originalInput.customsPaymentData.ToJsonString(),
                            CreateTime = DateTime.UtcNow,
                            OriginalInfo = originalInput.ToJsonString()
                        };
                        if (content.ConvertFromJsonString<YiTongBaoDeliveryOrderOutputDTO>().code == "0")
                        {
                            orderPackTrade.HasBeenOrdered = true;
                        }
                        else
                        {

                        }

                        //check if opid is already existed
                        var entry = pmsContext.OrderPackTrades.FirstOrDefault(x => x.Opid == item.opID);
                        if (entry == null)
                        {
                            pmsContext.OrderPackTrades.AddAsync(orderPackTrade);
                            pmsContext.SaveChangesAsync();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                CLSLogger.Error("青岛保税仓下单发生异常", ex, LogTags);
                throw ex;
            }
        }

        private string GetCarrierIDFromLinkedOrderPack(string orderPackNoToMatch, string orderID, int exportFlag)
        {
            if (exportFlag != 73)
            {
                return null;
            }

            SendNotificationToDingDingJob ddJob = new SendNotificationToDingDingJob();

            if (orderPackNoToMatch.IsNullOrEmpty())
            {
                ddJob.SendBondedWarehouseQingDaoAlert($"青岛保税仓下单失败，order中未找到notes，无法定位用户所下订单的包裹，订单为：{orderID}");
                throw new Exception($"青岛保税仓下单失败，order中未找到notes，无法定位用户所下订单的包裹，订单为：{orderID}");
            }

            OrderPackEntity orderPackToMatch = heerpContext.OrderPacks.FirstOrDefault(x => x.orderPackNo == orderPackNoToMatch);

            if (orderPackToMatch == null)
            {
                ddJob.SendBondedWarehouseQingDaoAlert($"青岛保税仓下单失败，订单notes中包裹号找不到对应用户所下订单的包裹，订单为：{orderID}");
                throw new Exception($"青岛保税仓下单失败，订单notes中包裹号找不到对应用户所下订单的包裹，订单为：{orderID}");
            }

            var carrierID = orderPackToMatch.carrierId;

            return carrierID;
        }

        private bool SwapSKUForYiTongBaoItemCode(List<ProductEntity> listQingDaoProduct, QingDaoBondedOrderInputDTO inputDTO)
        {
            foreach (var item in inputDTO.orderDetail)
            {
                var productInfo = listQingDaoProduct.FirstOrDefault(x => x.SKU.StandardizeSKU() == item.itemNo.StandardizeSKU() || x.BondedWarehouseItemCode.StandardizeSKU() == item.itemNo.StandardizeSKU());
                
                //if no match in product
                if (productInfo == null)
                {
                    SendNotificationToDingDingJob ddJob = new SendNotificationToDingDingJob();
                    ddJob.SendBondedWarehouseQingDaoAlert($"易通宝下单失败，请检查。订单为：{inputDTO.orderNo}。失败原因：产品表无对应青岛保税仓商品编码。订单内SKU：{item.itemNo}");
                    return false;
                }
                else
                {
                    item.itemNo = productInfo.BondedWarehouseItemCode;
                }
            }

            return true;
        }

        private void VerifyAndCorrectAddress(OrderEntity orderInfo, QingDaoBondedOrderInputDTO inputDTO)
        {
            if (orderInfo != null)
            {
                if (inputDTO.consignee != orderInfo.rec_name)
                {
                    inputDTO.consignee = orderInfo.rec_name;
                }

                if (inputDTO.consigneeTelephone != orderInfo.rec_phone)
                {
                    inputDTO.consigneeTelephone = orderInfo.rec_phone;
                }

                if (inputDTO.consigneeAddress != orderInfo.rec_addr)
                {
                    inputDTO.consigneeAddress = orderInfo.rec_addr;
                }

                if (inputDTO.province != orderInfo.province)
                {
                    inputDTO.province = orderInfo.province;
                }

                if (inputDTO.city != orderInfo.city)
                {
                    inputDTO.city = orderInfo.city;
                }

                if (inputDTO.area != orderInfo.district)
                {
                    inputDTO.area = orderInfo.district;
                }
            }
        }
    }
}