﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using MagicLamp.PMS.Domain;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.DTOs.ThirdPart;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.XERP.BackgroundJob
{
    public class PushOrderBindedToWaveToHHJob
    {
        private const string ClogTagType = "PushNewAndAllocatedOrderToHHJob";
        /// <summary>
        /// 选择状态为下列的波次：
        /// 00（创建订单）
        /// 30（部分分配）
        /// 40（分配完成）
        /// 50（部分拣货）
        /// </summary>
        private List<string> qualifiedWaveStatus = new List<string> { "00", "30", "40", "50" };


        private void RecordBindResult(HHBindedWaveNoEntity existedBindedWaveNo, string currentWaveNo, string resultMessage, string status, PMSDbContext pmsContext, string ordersIncluded)
        {
            //add new entry
            if (existedBindedWaveNo == null)
            {
                HHBindedWaveNoEntity entry = new HHBindedWaveNoEntity
                {
                    WaveNo = currentWaveNo,
                    Status = status,
                    Message = resultMessage,
                    CreateTime = DateTime.UtcNow,
                    OrdersIncluded = ordersIncluded
                };
                pmsContext.HHBindedWaveNoEntities.Add(entry);
            }
            //update status
            else
            {
                existedBindedWaveNo.Status = status;
                existedBindedWaveNo.Message = resultMessage;
                existedBindedWaveNo.UpdateTime = DateTime.UtcNow;
                existedBindedWaveNo.OrdersIncluded = ordersIncluded;
            }
        }

        /// <summary>
        /// Scan Flux Wave and bind WaveNo and orderpackno to HH.
        /// </summary>
        [SkipConcurrentExecution(60 * 120)]
        public void ScanFluxWave()
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>();
            clogTags["jobid"] = Guid.NewGuid().ToString();
            clogTags["type"] = ClogTagType;

            List<HHBindWaveAndOrderInputDTO> orderToBePushed = new List<HHBindWaveAndOrderInputDTO>();

            try
            {
                FluxWMSDbContext fluxWMSContext = new FluxWMSDbContextFactory().CreateDbContext();
                HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
                PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();


                StockShareDomainService stockShareDomainService = new StockShareDomainService();
                StockShareConfigVO configStockShare = stockShareDomainService.GetStockShareConfig();
                var enabled = configStockShare.EnableStockShare;

                List<String> listTestSKU = configStockShare.TestSKU;

                
                List<String> listWaveRule = configStockShare.WaveRule;

                if (!enabled)
                {
                    clogTags["method"] = "ScanFluxWave";
                    CLSLogger.Info("推送需配货批次订单给HH Job终止", "StockShareConfig中EnableStockShare关闭", clogTags);
                    return;
                }

                //get a list of wave header
                //x.WaveNo == "W1806110004"
                var listFluxWaveHeader = fluxWMSContext.FluxDocWaveHeaders.Where(x => qualifiedWaveStatus.Contains(x.WaveStatus) && listWaveRule.Contains(x.WaveRule) && x.AddTime.Year>2020).ToList(); // filter >2020 wave

                if (listFluxWaveHeader.IsNullOrEmpty())
                {
                    clogTags["method"] = "ScanFluxWave";
                    CLSLogger.Info("推送需配货批次订单给HH Job结束", "无符合条件的Flux波次计划表头", clogTags);
                    return;
                }

                //get a list of wave number
                var listWaveNo = listFluxWaveHeader.Select(x => x.WaveNo).ToList();
                //get a list of wave detail
                var listFluxWaveDetail = fluxWMSContext.FluxDocWaveDetails.Where(x => listWaveNo.Contains(x.WaveNo)).ToList();
                if (listFluxWaveDetail.Count == 0)
                {
                    clogTags["method"] = "ScanFluxWave";
                    CLSLogger.Info("推送需配货批次订单给HH Job结束", "无符合条件的Flux波次计划明细", clogTags);
                    return;
                }
                //get a list of flux order no from wave detail
                var listFluxOrderNo = listFluxWaveDetail.Select(x => x.OrderNo).ToList();
                //get a list of flux order header
                var listFluxOrderHeader = fluxWMSContext.FluxDocOrderHeaders.Where(x => listFluxOrderNo.Contains(x.FluxOrderNo)).ToList();
                //get a list of flux order details
                var listFluxOrderDetail = fluxWMSContext.FluxDocOrderDetails.Where(x => listFluxOrderNo.Contains(x.OrderNo)).ToList();
                //get a list of orderpackno
                var listOrderPackNo = listFluxOrderHeader.Select(x => x.OrderPackNo).ToList();
                //get a list of order pack
                var listOrderPack = heERPContext.OrderPacks.Where(x => listOrderPackNo.Contains(x.orderPackNo)).ToList();
                //get a list of binded wave no
                var listBindedWaveNo = pmsContext.HHBindedWaveNoEntities.Where(x => listWaveNo.Contains(x.WaveNo)).ToList();

                //
                var listOrderPackReservedHHSKUs = pmsContext.OrderPackReservedHHSKUs.Where(x => listOrderPackNo.Contains(x.OrderPackNo)).ToList();
                var listOrderToBePushedToHH = listOrderPackReservedHHSKUs.Select(x => x.OrderPackNo).ToList();
                var reservedSKU = listOrderPackReservedHHSKUs.Select(x => x.SKU).Distinct().ToList();

                //loop through wave no 
                foreach (var item in listFluxWaveHeader)
                {
                    string currentWaveNo = item.WaveNo;
                    clogTags["trackid"] = currentWaveNo;

                    //if no entry in HHBindedWaveNO, or entry status is BindFailed, then try bind
                    var existedBindedWaveNo = listBindedWaveNo.FirstOrDefault(x => x.WaveNo == currentWaveNo);
                    if (existedBindedWaveNo == null || existedBindedWaveNo.Status == QueueStatusEnum.BindFailed.ToString())
                    {
                        //find all the orderpack related to wave no
                        var fluxWaveDetails = listFluxWaveDetail.Where(x => x.WaveNo.Equals(currentWaveNo)).ToList();
                        var fluxOrderNos = fluxWaveDetails.Select(x => x.OrderNo).ToList();

                        var fluxOrderHeaders = listFluxOrderHeader.Where(x => fluxOrderNos.Contains(x.FluxOrderNo)).ToList();
                        var fluxOrderDetails = listFluxOrderDetail.Where(x => fluxOrderNos.Contains(x.OrderNo)).ToList();
                        var orderPackNos = fluxOrderHeaders.Select(x => x.OrderPackNo).Distinct().ToList();

                        List<string> ordersForHH = new List<string>();

                        //推送需配货批次订单给HH
                        foreach (var op in orderPackNos)
                        {
                            //批次下的订单只有需要HH方配货的订单才推送（通过OrderPacks_ReservedHHSKU 表判断），可通过记录推
                            if (listOrderToBePushedToHH.Contains(op))
                            {
                                ordersForHH.Add(op);
                            }
                        }

                        HHBindWaveAndOrderInputDTO input = new HHBindWaveAndOrderInputDTO
                        {
                            wave_picking_no = currentWaveNo,
                            create_time = item.AddTime.ToUniversalTime().SpecifyKindInUTC().ToString("yyyy-MM-dd HH:mm:ss"),
                            orders = ordersForHH
                        };

                        HHCreateOrderResultDTO bindResult = HHProxy.BindWaveAndOrder(input);

                        if (bindResult.result == 0 && !bindResult.message.IsNullOrEmpty() && bindResult.message.Contains("成功"))
                        {
                            var ordersIncluded = ordersForHH.ToJsonString();

                            clogTags["method"] = "BindWaveAndOrder";
                            CLSLogger.Info("推送需配货批次订单给HH完成一个波次", $"结果: {bindResult.message}, 波次: {currentWaveNo}, 订单: {ordersIncluded}", clogTags);

                            //change related OrderPacks_ReservedHHSKU entry status to binded
                            foreach (var order in ordersForHH)
                            {
                                listOrderPackReservedHHSKUs.Where(x => x.OrderPackNo == order).ToList()
                                    .ForEach(x => x.Status = OrderPackReservedStatusEnum.Binded.ToString());
                            }

                            //record result to HHBindedWaveNo
                            var done = QueueStatusEnum.Done.ToString();

                            RecordBindResult(existedBindedWaveNo, currentWaveNo, bindResult.message, done, pmsContext, ordersIncluded);

                            //change lotatt007 in fluxorderdetails to waveno from the waveheader
                            foreach (var detail in fluxOrderDetails)
                            {
                                var fluxHeader = listFluxOrderHeader.FirstOrDefault(x => x.FluxOrderNo == detail.OrderNo);

                                if (fluxHeader == null)
                                {
                                    CLSLogger.Info("更新富勒order detail lotatt07失败，无相关富勒order header", $"波次: {currentWaveNo}, 富勒orderno: {detail.OrderNo}", clogTags);
                                }
                                else
                                {
                                    if (listOrderPackReservedHHSKUs.Any(x => x.OrderPackNo == fluxHeader.OrderPackNo && x.SKU.ToLower().Trim() == detail.SKU.ToLower().Trim()))
                                    {
                                        detail.LotAtt07 = currentWaveNo;
                                        CLSLogger.Info("更新富勒order detail lotatt07成功", $"波次: {currentWaveNo}, 包裹号: {fluxHeader.OrderPackNo}", clogTags);
                                    }
                                }
                            }

                            fluxWMSContext.SaveChanges();
                            pmsContext.SaveChanges();

                            //Add purchasing order, ALADDIN, Product, HNH, System, 到货时间+1
                            IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
                            backgroundJobManager.Enqueue<AutoAddPurchasingInfoForHHJob, HHBindWaveAndOrderInputDTO>(input, BackgroundJobPriority.Normal);

                        }
                        else
                        {
                            var ordersIncluded = ordersForHH.ToJsonString();
                            var bindFailed = QueueStatusEnum.BindFailed.ToString();
                            RecordBindResult(existedBindedWaveNo, currentWaveNo, bindResult.message, bindFailed, pmsContext, ordersIncluded);
                            pmsContext.SaveChanges();

                            clogTags["method"] = "BindWaveAndOrder";
                            CLSLogger.Info("推送需配货批次订单给HH失败一个波次", $"结果: {bindResult.message}, 波次: {currentWaveNo}, 订单: {ordersIncluded}", clogTags);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                clogTags["method"] = "ScanFluxWave";
                CLSLogger.Error("PushNewAndAllocatedOrderToHHJob失败", ex, clogTags);
                throw ex;
            }
        }
    }
}
