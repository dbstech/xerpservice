﻿using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.MqDTO;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace MagicLamp.XERP.BackgroundJob
{
    public class FluxStockAllocationAutoConfigJob : CommonBackgroundJob<string>
    {

        public override void Process(string args)
        {
            FluxWMSDbContext fluxWMSDbContext = new FluxWMSDbContextFactory().CreateDbContext();
            var rules = fluxWMSDbContext.FluxStockAllocationRules.Where(x => x.LotAttName == "LOTATT11").ToList();
            rules.ForEach(x=> {
                x.MatchValue = args;
                x.EditTime = DateTime.UtcNow;
            });
            fluxWMSDbContext.SaveChanges();
        }


    }


}
