﻿using System;
using System.Collections.Generic;
using System.Text;
using MagicLamp.PMS.DTOs;
using MagicLamp.XERP.BackgroundJob.Common;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.EntityFrameworkCore;
using System.Linq;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.DTOs.BondedWarehouse;
using MagicLamp.PMS.Infrastructure;
using Abp.UI;
using Abp.Extensions;
using Abp.BackgroundJobs;
using Abp.Dependency;
using MagicLamp.PMS.DTO.BondedWarehouse;

namespace MagicLamp.XERP.BackgroundJob
{
    public class GMCreateOrderJob : CommonBackgroundJob<GMCreateOrderInputDTO>
    {
        const string ClogTagType = "GMCreateOrderJob";
        public override void Process(GMCreateOrderInputDTO input)
        {
            var orderNo = input.OrderPackNo.IsNullOrWhiteSpace() ? input.OrderID : input.OrderPackNo;
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = ClogTagType,
                ["orderid"] = orderNo
            };

            HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
            PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();
            IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();

            try
            {

                string reqJson = input.ToJsonString();
                CLSLogger.Info("重庆保税仓下单请求报文", reqJson, clogTags);

                var orderPack = heERPContext.OrderPacks.FirstOrDefault(x =>
                    input.OrderPackNo.IsNullOrWhiteSpace()
                    ? x.orderID == input.OrderID
                    : x.orderPackNo == input.OrderPackNo
                );

                if (orderPack == null)
                {
                    bool cacheOrder = heERPContext.CacheOrders.Any(x => x.orderid == input.OrderID);
                    var order = heERPContext.Orders.Any(x => x.orderid == input.OrderID);
                    if (cacheOrder == false && order == false)
                    {
                        CLSLogger.Info("重庆保税仓推单时发现订单已取消", "cache order,orders and orderpacks all not found", clogTags);
                        return;
                    }

                    var orderEntity = heERPContext.Orders.FirstOrDefault(x => x.orderid == input.OrderID);
                    //无效单
                    if (orderEntity.valflag == 0)
                    {
                        CLSLogger.Info("重庆保税仓推单时发现订单为无效单", "订单的valflag为0，取消此单的重庆保税仓推单进程", clogTags);
                        return;
                    }

                    throw new Exception("Order has not been split yet.");
                }
                else
                {
                    if (orderPack.freezeFlag > 0)
                    {
                        CLSLogger.Info("重庆保税仓推单时发现订单已取消", $"包裹已冻结：{orderPack.orderPackNo}  freezeflag: {orderPack.freezeFlag}  {orderPack.freezeComment}", clogTags);
                        return;
                    }

                    OrderPackTradeEntity orderPackTrade = new OrderPackTradeEntity
                    {
                        Opid = orderPack.opID,
                        OrderID = orderPack.orderID,
                        OrderPackNo = orderPack.orderPackNo,
                        CustomsPaymentData = input.customsPaymentData.ToJsonString(),
                        CreateTime = DateTime.UtcNow,
                        OriginalInfo = input.ToJsonString(),
                        HasBeenOrdered = true
                    };
                
                    CLSLogger.Info("GMCreateOrderJobOrderPackTrade", orderPackTrade.ToJsonString(), clogTags);

                    //check if opid is already existed
                    var entry = pmsContext.OrderPackTrades.FirstOrDefault(x => x.Opid == orderPack.opID);
                    if (entry == null)
                    {
                        pmsContext.OrderPackTrades.Add(orderPackTrade);
                        pmsContext.SaveChanges();
                    }
                }

                bool isReceiverInfoAltered = false;
                var orderInfo = heERPContext.Orders.FirstOrDefault(x => x.orderid == input.OrderID);

                string orderInfoRecName = string.Empty;
                string orderInfoRecPhone = string.Empty;
                string orderInfoIdno = string.Empty;
                string orderInfoProvince = string.Empty;
                string orderInfoCity = string.Empty;
                string orderInfoDistrict = string.Empty;
                string orderInfoTown = string.Empty;

                if (orderInfo != null && (orderInfo.rec_name != input.ReceiverName
                                          || orderInfo.rec_phone != input.ReceiverPhone
                                          || orderInfo.idno != input.ReceiverIdNo
                                          || orderInfo.province != input.ReceiverProvince
                                          || orderInfo.city != input.ReceiverCity
                                          || orderInfo.district != input.ReceiverDistrict
                                          || orderInfo.town != input.ReceiverAddress))
                {

                    orderInfoRecName = orderInfo.rec_name;
                    orderInfoRecPhone = orderInfo.rec_phone;
                    orderInfoIdno = orderInfo.idno;
                    orderInfoProvince = orderInfo.province;
                    orderInfoCity = orderInfo.city;
                    orderInfoDistrict = orderInfo.district;
                    orderInfoTown = orderInfo.town;
                    isReceiverInfoAltered = true;

                    CLSLogger.Info("重庆保税仓推单时发现订单收件人信息有修改，使用orders的数据并更新input地址信息。"
                        , $"orders数据：{orderInfoRecName}，{orderInfoRecPhone}，{orderInfoIdno}，{orderInfoProvince}，{orderInfoCity}，{orderInfoDistrict}，{orderInfoTown}。" +
                          $"input数据：{input.ReceiverName}，{input.ReceiverPhone}，{input.ReceiverIdNo}，{input.ReceiverProvince}，{input.ReceiverCity}，{input.ReceiverDistrict}，{input.ReceiverAddress}。"
                        , clogTags);

                    //change input in case the order has to be repushed
                    input.ReceiverName = orderInfoRecName;
                    input.ReceiverPhone = orderInfoRecPhone;
                    input.ReceiverIdNo = orderInfoIdno;
                    input.ReceiverProvince = orderInfoProvince;
                    input.ReceiverCity = orderInfoCity;
                    input.ReceiverDistrict = orderInfoDistrict;
                    input.ReceiverAddress = orderInfoTown;
                }

                List<string> erpSkus = input.GoodsItems.Select(x => x.SKU).ToList();
                var bondedProducts = pmsContext.CQBondedProductEntities.Where(x => erpSkus.Contains(x.ERPSKU)).ToList();
                var erpProducts = pmsContext.Products.Where(x => erpSkus.Contains(x.SKU)).ToList();

                string customCode = "4403960G4J";
                string eShopEnterpriseCode = "4403960G4J";
                string eShopEnterpriseName = "迈集客网络技术（深圳）有限公司";
                string uniEshopEnterpriseCode = "500696082C";
                string uniEshopEnterpriseName = "重庆欧东快递服务有限公司";

                DTC_Message dTC_Message = new DTC_Message
                {
                    MessageHead = new MessageHead
                    {
                        MessageType = "ORDER_INFO",
                        ActionType = "1",
                        MessageId = $"{input.OrderID}-{DateTime.Now.ToUnixTimeInMilliSeconds()}",
                        MessageTime = DateTime.UtcNow.ToString(),
                        SenderId = customCode,
                        ReceiverId = "CQITC",
                        UserNo = customCode,
                        Password = Ultities.GetMd5Hash(customCode),
                    },
                    MessageBody = new MessageBody
                    {
                        DTCFlow = new DTCFlow
                        {
                            ORDER_HEAD = new ORDER_HEAD
                            {
                                CUSTOMS_CODE = "8013",
                                BIZ_TYPE_CODE = "I20",
                                SORTLINE_ID = "SORTLINE03",
                                ORIGINAL_ORDER_NO = orderNo,
                                ESHOP_ENT_CODE = eShopEnterpriseCode,
                                ESHOP_ENT_NAME = eShopEnterpriseName,
                                DESP_ARRI_COUNTRY_CODE = "142",
                                SHIP_TOOL_CODE = "Y",
                                //if receiver info has been edited
                                RECEIVER_ID_NO = isReceiverInfoAltered ? orderInfoIdno : input.ReceiverIdNo,
                                RECEIVER_NAME = isReceiverInfoAltered ? orderInfoRecName : input.ReceiverName,
                                RECEIVER_ADDRESS = isReceiverInfoAltered ?
                                    $"{orderInfoProvince}_{orderInfoCity}_{orderInfoDistrict}_{orderInfoTown}"
                                    : $"{input.ReceiverProvince}_{input.ReceiverCity}_{input.ReceiverDistrict}_{input.ReceiverAddress}",
                                RECEIVER_TEL = isReceiverInfoAltered ? orderInfoRecPhone : input.ReceiverPhone,
                                BUYER_TELEPHONE = isReceiverInfoAltered ? orderInfoRecPhone : input.ReceiverPhone,
                                //
                                GOODS_FEE = input.OrderAmount.ToString(),
                                TRANSPORT_FEE = input.ShippingFee.ToString(),
                                CC_TRADE_CODE = "500666001T",
                                CC_TRADE_NAME = "重庆丝路广序国际物流有限公司",
                                UNI_ESHOP_ENT_CODE = uniEshopEnterpriseCode,
                                UNI_ESHOP_ENT_NAME = uniEshopEnterpriseName,
                                SEND_ENT_CODE = eShopEnterpriseCode,
                                BUYER_REG_NO = input.MembershipNo,
                                BUYER_NAME = input.BuyerName,
                                BUYER_ID_TYPE = "1",
                                BUYER_ID = input.BuyerIdNo,
                                // value pending confirmation
                                DISCOUNT = input.Discount.HasValue ? input.Discount.Value.ToString() : "0",
                                INSURED_FEE = "0",
                                EBP_CODE = eShopEnterpriseCode,
                                EBP_NAME = eShopEnterpriseName,
                                ASSURE_CODE = eShopEnterpriseCode,
                                ORDER_DETAIL = new List<ORDER_DETAIL>()
                            }
                        }
                    }
                };

                //商品信息index
                int goodsIndex = 0;
                //总税费
                decimal totalTaxFee = input.OrderTax.HasValue ? input.OrderTax.Value : 0;
                decimal totalGrossWeight = 0;
                decimal totalNetWeight = 0;
                foreach (var goodsItem in input.GoodsItems)
                {
                    var bondedProduct = bondedProducts.FirstOrDefault(x => x.ERPSKU == goodsItem.SKU);
                    var erpProduct = erpProducts.FirstOrDefault(x => x.SKU == goodsItem.SKU);

                    if (bondedProduct == null)
                    {
                        var notificationMessage = $"请注意！订单号：{input.OrderID} 下单失败，原因：未找到SKU：{goodsItem.SKU}  的对应保税商品信息，请维护基础数据";
                        SendNotificationToDingDingJob ddJob = new SendNotificationToDingDingJob();
                        ddJob.SendBondedWarehouseChongQingAlert(notificationMessage);

                        throw new UserFriendlyException($"未找到SKU：{goodsItem.SKU}  的对应保税商品信息");
                    }

                    if (erpProduct == null)
                    {
                        throw new UserFriendlyException($"未找到SKU：{goodsItem.SKU}  的对应ERP商品信息");
                    }

                    decimal productTaxFee = (goodsItem.Price - goodsItem.Price / 1.091M);
                    goodsIndex++;

                    ORDER_DETAIL orderDetail = new ORDER_DETAIL
                    {
                        //序号
                        G_NUM = goodsIndex.ToString(),
                        //商品货号
                        SKU = bondedProduct.GMSKU,
                        //规格型号
                        GOODS_SPEC = bondedProduct.GOODS_SPEC,
                        //品名
                        GOODS_NAME = bondedProduct.GOODS_NAME,
                        //币制代码
                        CURRENCY_CODE = bondedProduct.CURRENCY_CODE,
                        BAR_CODE = bondedProduct.BAR_CODE,
                        //原产国
                        COUNTRY = bondedProduct.COUNTRY,
                        GOODS_FEE = (goodsItem.Price * goodsItem.Qty).ToString(),
                        TAX_FEE = "0",
                        //TAX_FEE = Math.Round(productTaxFee, 3, MidpointRounding.AwayFromZero).ToString(),//todo  确认税金如何计算
                        HS_CODE = bondedProduct.HS_CODE,
                        UNIT_CODE = bondedProduct.UNIT_CODE,
                        QTY = goodsItem.Qty.ToString(),
                        PRICE = goodsItem.Price.ToString()
                    };
                    //若有第二计量单位，则第二数量必填
                    if (!bondedProduct.UNIT2.IsNullOrEmpty())
                    {
                        //第二计量单位
                        orderDetail.UNIT2 = bondedProduct.UNIT2;
                        orderDetail.QTY2 = bondedProduct.QTY2;
                    }

                    //totalTaxFee += Convert.ToDecimal(orderDetail.TAX_FEE);
                    totalGrossWeight += erpProduct.GrossWeight.HasValue ? (erpProduct.GrossWeight.Value / 1000) * goodsItem.Qty : 0;
                    totalNetWeight += erpProduct.NetWeight.HasValue ? (erpProduct.NetWeight.Value / 1000) * goodsItem.Qty : 0;

                    dTC_Message.MessageBody.DTCFlow.ORDER_HEAD.ORDER_DETAIL.Add(orderDetail);
                }

                dTC_Message.MessageBody.DTCFlow.ORDER_HEAD.GROSS_WEIGHT = totalGrossWeight.ToString();
                dTC_Message.MessageBody.DTCFlow.ORDER_HEAD.NET_WEIGHT = totalNetWeight.ToString();
                dTC_Message.MessageBody.DTCFlow.ORDER_HEAD.TAX_FEE = totalTaxFee.ToString();
                //actual_paid = 货款+运费+税款+保费-优惠金额 (orderAmout = 货款+运费)
                decimal actualPaid = totalTaxFee + input.OrderAmount + input.ShippingFee - (input.Discount.HasValue ? input.Discount.Value : 0);
                dTC_Message.MessageBody.DTCFlow.ORDER_HEAD.ACTUAL_PAID = actualPaid.ToString();

                GuanMaoProxy guanMaoProxy = new GuanMaoProxy();
                var result = guanMaoProxy.CreateOrder(dTC_Message);

                if (result == false)
                {
                    throw new Exception($"重庆保税仓下单接口失败，订单号：{input.OrderID}");
                }

                //OrderPackTrade               
                if (orderPack == null)
                {
                    CLSLogger.Info("重庆保税仓下单请求报文&业务端信息，无相关包裹",
                        $"{input.ToJsonString()}，customsPaymentData: {input.customsPaymentData}", clogTags);
                }
                else
                {

                }
            }
            catch (UserFriendlyException ex)
            {
                CLSLogger.Error("重庆保税仓下单失败", ex, clogTags);
            }
            catch (Exception ex)
            {
                CLSLogger.Error("重庆保税仓下单发生异常", ex, clogTags);
                throw ex;
            }
            finally
            {
                CLSLogger.Info("重庆保税仓下单响应报文", "Finished", clogTags);
            }

        }
    }
}
