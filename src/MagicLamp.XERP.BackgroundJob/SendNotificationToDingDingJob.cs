﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Extensions;
using Abp.Runtime.Caching;
using MagicLamp.PMS;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Common;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using TimeZoneConverter;

namespace MagicLamp.XERP.BackgroundJob
{
    public class SendNotificationToDingDingJob
    {
        private const string ClogTagType = "SendNotificationToDingDingJob";

        private const string StockShare = "StockShare";
        private const string HH = "HH";

        private TimeZoneInfo timeZoneNZ = TZConvert.GetTimeZoneInfo("Pacific/Auckland");

        public ICacheManager Cache { get; set; }

        //群名称：E店，HH共享库存对接群
        private string ddToken = "c0871da06bd657f026f4807580e1130ffa85c809dd1ad6b62413fa118453b9e4";
        //群名称：HH匹配问题解决群（临时）
        private string ddToken2 = "3ffc5394562f379ad7ebb942e8f357eb6e1f59e0cd0549e36d8da7c644bd68cb";
        //群名称：青岛保税仓运维
        private string ddTokenQingDao = "7456c7f4c5ccda9c249cb72e85de502de3d6a5123b671ef093f62b677872c1a1";
        //群名称：重庆保税仓项目
        private string ddTokenChongQing = "f45c34c95708f5ee135a22d5d722cb9a064a46fca6383ecf99fcdb234e534d53";
        //群名称：异常订单预警群
        private string ddTokenOrderAlert = "b212b1711b1b117b7a259d3e1bc8e0879b63457ead81d8176970d03be97668e7";
        //群名称：八禧仓运维
        private string ddTokenBaXi = "34adddda7c173e972431bbd7a0f0f3ffbebb8746dbfdc66c4191f2e168e098ef";
        //群名称：义乌保税仓运维
        private string ddTokenYiWu = "449249c5ed7c9b62143f6a90c7f0a7781f6667110705f8c937fd1c07aac65777";

        //群名称：Ecinx订单运维
        private string ddTokenEcinx = "15fde86b498c10ce95daac72cee6f0878f3146d59525cd570a01c7d42cd88b26";
        //群名称：Ecinx 采购成本变动通知 
        private string ddTokenEcinxCostPrice = "ee4ac8ef810d0b01ab594bd49cd47b48b3bd3990238bdd15b590d6eb47e13085";

        //群名称：重庆+义乌保税仓库 库存告警群
        private string ddTokenEcinxStock = "4a412567119e79c018801bcdcd09a8790c561d44b1c3ddbc868650430674cfe7";
      


        /// <summary>
        /// format message to send to dingding
        /// </summary>
        /// <param name="message"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        private string FormatMessage(string message, int count)
        {
            string result = $"* {message} \n\n > 数量：{count} \n\n";

            return result;
        }

        [SkipConcurrentExecution(60 * 120)]
        public void CheckHHRelatedStatus()
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["jobid"] = Guid.NewGuid().ToString(),
                ["type"] = ClogTagType,
                ["method"] = "CheckHHRelatedStatus"
            };


            StringBuilder logBuilder = new StringBuilder();

            try
            {
                PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();
                FluxWMSDbContext fluxWmsContext = new FluxWMSDbContextFactory().CreateDbContext();

                //检查在HH绑定失败波次
                CheckBindedFailedWaveNoInHH(logBuilder, pmsContext);
                //HH配货监控
                CheckWaveNoWaitingToBeDeliveredByHH(logBuilder, fluxWmsContext);

                //检查HH相关波次，订单状态
                CheckHHRelatedWaveAndOrderStatus(logBuilder, pmsContext, fluxWmsContext);
            }
            catch (Exception ex)
            {
                CLSLogger.Error("SendNotificationToDingDingJob.CheckHHRelatedStatus失败", ex, clogTags);
            }
            finally
            {
                CLSLogger.Info("SendNotificationToDingDingJob.CheckHHRelatedStatus完成", logBuilder.ToString(), clogTags);
            }

        }

        public void SendBondedWarehouseQingDaoAlert(string message)
        {
            string signature = "SECd0eefc05fc79e0de57157844d214094b0c493460f7b9531e3b9121bdb20bfef7";
            DDNotificationProxy.MarkdownNotification($"青岛保税仓推单结果", message, ddTokenQingDao, signature);
        }


        public void SendEcinxYWCQStockAlert(string message)
        {
            string signature = "SECe6a99f97ad345d920105144d287b17b92938075cd56f4be32cbfc14413fc4d04";
            DDNotificationProxy.MarkdownNotification($"Ecinx保税仓库存告警", message, ddTokenEcinxStock, signature);
        }



        public void SendBondedWarehouseChongQingAlert(string message)
        {
            string signature = "SECda018b7d7b708ac34d9ad56360b0e7c7f257f7121bde2efcd78f566186424043";
            DDNotificationProxy.MarkdownNotification($"重庆保税仓推单结果", message, ddTokenChongQing, signature);
        }

        public void SendBondedWarehouseYiWuAlert(string title, string message)
        {
            string signature = "SEC0aa5b8b43d30298d14c92b2cd043f8de73b69c6febedaad940df5a14fdaed430";
            DDNotificationProxy.MarkdownNotification(title, message, ddTokenYiWu, signature);
        }

        public void SendEcinxERPAlert(string title, string message)
        {
            string signature = "SEC8346baaaf59077e44953cf3dec6549e19f3ad4f9a2c20694b621b6e439c7019b";
            DDNotificationProxy.MarkdownNotification(title, message, ddTokenEcinx, signature);
        }

        public void SendEcinxCostPriceChangeAlert(string title, string message)
        {
            string signature = "SEC174674d3ee1f2a25d536b57f81a3325cf02cd5dfa61ba767ca81e0e56c8d2326";
            DDNotificationProxy.MarkdownNotification(title, message, ddTokenEcinxCostPrice, signature);
        }


        // HH匹配问题解决群（临时）
        public void SendProductMappingAlert(string title, string message)
        {
            //Kimi
            List<string> listMobileNumber = new List<string> { "+64-0210587454" };
            DDNotificationProxy.MarkdownNotification(title, message, ddToken2, null, listMobileNumber);
        }

        // HH匹配问题解决群（临时）
        public void SendPurchaseOrderAlert(string title, string message)
        {
            //Kimi
            List<string> listMobileNumber = new List<string> { "+64-210587454" };
            DDNotificationProxy.MarkdownNotification(title, message, ddToken2, null, listMobileNumber);
        }

        public void SendBaXiAlert(string message, string key)
        {
            //cache
            Cache = IocManager.Instance.Resolve<ICacheManager>();
            string cacheKey = string.Format(CacheKeys.BAXIORDERNOTIFICATIONKEY, key);

            var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);
            var val = cacheManager.GetOrDefault(cacheKey);

            if (val != null)
            {
                return;
            }
            else
            {
                string signature = "SECe480bad4be94d0ce5b516f826141d46a6130a54be731558507d68a583f675fb0";
                DDNotificationProxy.MarkdownNotification($"八禧仓监控报警", message, ddTokenBaXi, signature);

                cacheManager.Set(cacheKey, 1, TimeSpan.FromHours(4));
            }

        }

        public void MonitorAbnormalFluxOrders()
        {
            string signature = "SECdb6aba13841a234b9024cc527e0d8f6d8932894aec06e04d2caddf9c867072fd";

            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["jobid"] = Guid.NewGuid().ToString(),
                ["type"] = ClogTagType,
                ["method"] = "MonitorAbnormalFluxOrders"
            };

            StringBuilder ddMessage = new StringBuilder();

            try
            {
                //steven
                List<string> listMobileForFlux = new List<string> { "+64-2108134363" };
                foreach (var item in listMobileForFlux)
                {
                    ddMessage.Append($"@{item} \n\n");
                }

                FluxWMSDbContext fluxWmsContext = new FluxWMSDbContextFactory().CreateDbContext();

                //1. 包裹超时进入波次(完全分配包裹 超过24小时 没有相关波次号)
                var listOrderWithoutWaveNo = fluxWmsContext.FluxDocOrderHeaders.Where(
                    x => x.SOStatus == "40"
                    && x.WAVENO == "*"
                    && TimeZoneInfo.ConvertTimeToUtc(x.EditTime, timeZoneNZ).AddDays(1) < DateTime.UtcNow);

                int countOrderWithoutWaveNo = listOrderWithoutWaveNo.Count();
                if (countOrderWithoutWaveNo > 0)
                {
                    ddMessage.Append(FormatMessage("检查出富勒有包裹超24小时未进入波次", countOrderWithoutWaveNo));

                    var listFluxOrderWithoutWaveNo = listOrderWithoutWaveNo.ToList();
                    foreach (var item in listFluxOrderWithoutWaveNo)
                    {
                        ddMessage.Append($"> {item.OrderPackNo} \n\n");
                    }
                }

                //2. 配货超时(配货完成包裹 超过24小时 没有包装完成时间) 拣货完成40 => 完全装箱63
                var listOrderAllocatedOverTime = fluxWmsContext.FluxDocOrderHeaders.Where(
                    x => x.SOStatus == "40"
                    && TimeZoneInfo.ConvertTimeToUtc(x.EditTime, timeZoneNZ).AddDays(1) < DateTime.UtcNow);

                int countOrderAllocatedOverTime = listOrderAllocatedOverTime.Count();
                if (countOrderAllocatedOverTime > 0)
                {
                    ddMessage.Append(FormatMessage("检查出富勒有包裹超24小时没有包装完成", countOrderAllocatedOverTime));
                }

                //3. 出库超时 （完全装箱63 => 订单完成99）
                var listOrderOutboundOverTime = fluxWmsContext.FluxDocOrderHeaders.Where(
                    x => x.SOStatus == "63"
                    && TimeZoneInfo.ConvertTimeToUtc(x.EditTime, timeZoneNZ).AddMinutes(30) < DateTime.UtcNow);

                int countOrderOutboundOverTime = listOrderOutboundOverTime.Count();
                if (countOrderOutboundOverTime > 0)
                {
                    ddMessage.Append(FormatMessage("检查出富勒有包裹超30分钟没有转变为出库状态", countOrderOutboundOverTime));
                }


                //4. 完全分配超时(进入flux包裹  超过48小时 状态没有变为完全分配)
                var listOrderNotAllocatedInTime = fluxWmsContext.FluxDocOrderHeaders.Where(
                    x => int.Parse(x.SOStatus) < 40
                    && TimeZoneInfo.ConvertTimeToUtc(x.AddTime, timeZoneNZ).AddDays(2) < DateTime.UtcNow);

                int countOrderNotAllocatedInTime = listOrderNotAllocatedInTime.Count();
                if (countOrderNotAllocatedInTime > 0)
                {
                    ddMessage.Append(FormatMessage("检查出富勒有包裹超48小时未完全分配", countOrderNotAllocatedInTime));

                    var listFluxOrderNotAllocatedInTime = listOrderNotAllocatedInTime.ToList();
                    foreach (var item in listFluxOrderNotAllocatedInTime)
                    {
                        ddMessage.Append($"> {item.OrderPackNo} \n\n");
                    }
                }


                //finally send message to dingding
                DDNotificationProxy.MarkdownNotification($"监控报警", ddMessage.ToString(), ddTokenOrderAlert, signature, listMobileForFlux);

            }
            catch (Exception ex)
            {
                CLSLogger.Error("MonitorAbnormalFluxOrders失败", ex, clogTags);
            }
        }

        public void MonitorOrderPackWithoutCarrierID()
        {
            string signature = "SECdb6aba13841a234b9024cc527e0d8f6d8932894aec06e04d2caddf9c867072fd";

            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["jobid"] = Guid.NewGuid().ToString(),
                ["type"] = ClogTagType,
                ["method"] = "MonitorOrderPackWithoutCarrierID"
            };

            StringBuilder ddMessage = new StringBuilder();

            try
            {
                //简儿，stark， page
                List<string> listMobileNumber = new List<string> { "+86-15953212877", "+86-18562596726", "+86-15762265605" };
                foreach (var item in listMobileNumber)
                {
                    ddMessage.Append($"@{item} \n\n");
                }

                HEERPDbContext heerpDbContext = new HEERPDbContextFactory().CreateDbContext();


                //订单物流单号回传超时（订单包裹 超过48小时没有物流单号）

                var listOrderPackWithoutCarrierID = heerpDbContext.OrderPacks.Where(
                    x => x.exportFlag.Value == 2
                    && x.freezeFlag.Value == 0
                    && x.status.Value != 4
                    && x.carrierId == null
                    && TimeZoneInfo.ConvertTimeToUtc(x.opt_time.Value, timeZoneNZ).AddDays(4) < DateTime.UtcNow);

                int countOrderPackWithoutCarrierID = listOrderPackWithoutCarrierID.Count();
                if (countOrderPackWithoutCarrierID > 0)
                {
                    ddMessage.Append(FormatMessage("检查出代发包裹超96小时没有物流单号", countOrderPackWithoutCarrierID));

                    var listOrderPack = listOrderPackWithoutCarrierID.OrderBy(x => x.opt_time).Take(10).ToList();

                    ddMessage.Append($"> 最早一单可追溯到 {listOrderPack.FirstOrDefault().opt_time}。前十单为：\n\n");
                    foreach (var item in listOrderPack)
                    {
                        ddMessage.Append($"> {item.orderPackNo} \n\n");
                    }
                }

                //finally send message to dingding
                DDNotificationProxy.MarkdownNotification($"监控报警", ddMessage.ToString(), ddTokenOrderAlert, signature, listMobileNumber);

            }
            catch (Exception ex)
            {
                CLSLogger.Error("MonitorOrderPackWithoutCarrierID失败", ex, clogTags);
            }
        }

        private void CheckBindedFailedWaveNoInHH(StringBuilder logBuilder, PMSDbContext pmsContext)
        {
            StringBuilder ddMessage = new StringBuilder();

            //尹小飞，Austin
            List<string> listMobileToNotify = new List<string> { "+64-212620316", "+64-272269999" };
            foreach (var item in listMobileToNotify)
            {
                ddMessage.Append($"@{item} \n\n");
            }

            logBuilder.Append("开始HH检查绑定失败的波次。");

            var listBindedFailed =
                pmsContext.HHBindedWaveNoEntities.Where(x => x.Status == QueueStatusEnum.BindFailed.ToString())
                    .ToList();

            if (!listBindedFailed.IsNullOrEmpty())
            {
                
                ddMessage.Append("##### 下列波次绑定失败，请检查失败原因。 \n\n");
                ddMessage.Append("------------------------------ \n\n");

                foreach (var item in listBindedFailed)
                {
                    ddMessage.Append($"* 波次：{item.WaveNo} \n\n > 原因：{(item.Message == null ? "HH无返回结果" : item.Message)} \n\n");
                    logBuilder.Append($"波次：{item.WaveNo}，原因：{(item.Message == null ? "HH无返回结果" : item.Message)}。");
                }

                DDNotificationProxy.MarkdownNotification($"检查出在HH绑定失败或无返回结果的波次", ddMessage.ToString(), ddToken, null, listMobileToNotify);
                DDNotificationProxy.MarkdownNotification($"检查出在HH绑定失败或无返回结果的波次", ddMessage.ToString(), ddToken2);
            }

            logBuilder.Append("检查HH绑定失败的波次结束。");
        }

        private void CheckWaveNoWaitingToBeDeliveredByHH(StringBuilder logBuilder, FluxWMSDbContext fluxWmsContext)
        {
            logBuilder.Append("开始检查等待HH配货到仓波次。");

            var listPendingASNHeader =
                fluxWmsContext.FluxDocASNHeaders.Where(x => x.ASNReference1.StartsWith("W2") && x.ASNStatus == "00")
                .ToList();

            if (!listPendingASNHeader.IsNullOrEmpty())
            {
                StringBuilder ddMessage = new StringBuilder();
                ddMessage.Append($"##### 下列 {listPendingASNHeader.Count()} 个波次等待HH配货到仓。 \n\n");
                ddMessage.Append("------------------------------ \n\n");

                foreach (var item in listPendingASNHeader)
                {
                    ddMessage.Append($"* 波次：{item.ASNReference1} \n\n > 通知配货时间（NZ）：{TimeZoneInfo.ConvertTimeFromUtc(item.ASNCreationTime, timeZoneNZ).ToString("yyyy-MM-dd HH:mm:ss")} \n\n");
                    logBuilder.Append($"波次：{item.ASNReference1}，通知配货时间（NZ）：{TimeZoneInfo.ConvertTimeFromUtc(item.ASNCreationTime, timeZoneNZ).ToString("yyyy-MM-dd HH:mm:ss")}。");
                }

                DDNotificationProxy.MarkdownNotification($"检查出等待HH配货到仓波次", ddMessage.ToString(), ddToken);
                DDNotificationProxy.MarkdownNotification($"检查出等待HH配货到仓波次", ddMessage.ToString(), ddToken2);
            }

            logBuilder.Append("检查等待HH配货到仓波次结束。");
        }

        private void CheckHHRelatedWaveAndOrderStatus(StringBuilder logBuilder, PMSDbContext pmsContext, FluxWMSDbContext fluxWmsContext)
        {
            //grouped message
            StringBuilder groupedMessage = new StringBuilder();
            //共享库存，富勒未进波次订单数
            logBuilder.Append("开始检查等待生成波次订单。");

            var listFluxOrderHeaderWithoutWaveNo =
                fluxWmsContext.FluxDocOrderHeaders.Where(x => x.UserDefine6 == StockShare && x.WAVENO == "*" && !PMSConsts.FluxSoStatusDone.Contains(x.SOStatus));
            var countForHeaderWithoutWaveNo = listFluxOrderHeaderWithoutWaveNo.Count();

            if (countForHeaderWithoutWaveNo > 0)
            {
                groupedMessage.Append(FormatMessage("检查出等待生成波次订单", countForHeaderWithoutWaveNo));
                logBuilder.Append($"检查出等待生成波次订单，数量：{countForHeaderWithoutWaveNo}。");
            }

            logBuilder.Append("检查等待生成波次订单完成。");

            //分配完成但未处理总订单数
            logBuilder.Append("开始检查分配完成但未处理订单。");

            var listFluxOrderHeader40 = fluxWmsContext.FluxDocOrderHeaders.Where(x => x.SOStatus == "40" && x.Warehouseid == "WH01");
            var coutForHeader40 = listFluxOrderHeader40.Count();

            if (coutForHeader40 > 0)
            {
                groupedMessage.Append(FormatMessage("检查出分配完成但未处理订单", coutForHeader40));
                logBuilder.Append($"检查出分配完成但未处理订单，数量：{coutForHeader40}。");
            }

            logBuilder.Append("检查分配完成但未处理订单完成。");

            //缺货总订单数，分为魔法灯，HH
            logBuilder.Append("开始检查缺货订单。");

            var listFluxOrderHeader0030 = fluxWmsContext.FluxDocOrderHeaders.Where(x => (x.SOStatus == "00" || x.SOStatus == "30")
            && x.Warehouseid == "WH01").ToList();

            //魔法灯
            var listMFD0030 = listFluxOrderHeader0030.Where(x => x.UserDefine5 == null).ToList();

            int countMFD0030 = listMFD0030.Count();
            //等待hh配货（userdefine6为stockshare，有waveno）
            var countMFDWaiting = listMFD0030.Where(x => x.UserDefine6 == StockShare && x.WAVENO != "*").Count();
            //等待生成波次（没waveno）
            var countMFDWaitingWave = listMFD0030.Where(x => x.WAVENO == "*").Count();


            //HH
            var listHH0030 = listFluxOrderHeader0030.Where(x => x.UserDefine5 == HH).ToList();
            var countHH0030 = listHH0030.Count();
            //等待hh配货（userdefine6为stockshare，有waveno）
            var countHHWaiting = listHH0030.Where(x => x.UserDefine6 == StockShare && x.WAVENO != "*").Count();
            //等待生成波次（没waveno）
            var countHHWaitingWave = listHH0030.Where(x => x.WAVENO == "*").Count();



            var listOrderPackOutOfStock = pmsContext.OrderPackOutOfStocks.Where(x => x.Status == QueueStatusEnum.OutOfStock.ToString())
                .GroupBy(x => x.OrderPackNo).ToList();

            var listPackNoOutOfStock = listOrderPackOutOfStock.Select(x => x.Key).ToList();

            //目前partner为null是魔法灯订单
            var listMFD = pmsContext.OrderPackWMSQueues.Where(x => x.Partner == null && listPackNoOutOfStock.Contains(x.OrderPackNo)).ToList();
            var countMFD = listMFD.Count();

            int countOutOfStockMFD = countMFD0030 + countMFD;

            if (countOutOfStockMFD > 0)
            {
                groupedMessage.Append(FormatMessage("检查出魔法灯订单缺货订单", countOutOfStockMFD));
                groupedMessage.Append($"> 其中：等待HH配货：{countMFDWaiting}，等待生成波次：{countMFDWaitingWave} \n\n");
                logBuilder.Append($"检查出魔法灯订单缺货订单，数量：{countOutOfStockMFD}。");
            }

            //partner为HH是HH订单
            var listHH = pmsContext.OrderPackWMSQueues.Where(x => x.Partner == HH && listPackNoOutOfStock.Contains(x.OrderPackNo)).ToList();
            var countHH = listHH.Count();

            int countOutOfStockHH = countHH0030 + countHH;

            if (countOutOfStockHH > 0)
            {
                groupedMessage.Append(FormatMessage("检查出HH订单缺货订单", countOutOfStockHH));
                groupedMessage.Append($"> 其中：等待HH配货：{countHHWaiting}，等待生成波次：{countHHWaitingWave} \n\n");
                logBuilder.Append($"检查出HH订单缺货订单，数量：{countOutOfStockHH}。");
            }

            logBuilder.Append("检查缺货订单完成。");

            //预定HH库存（创建订单）失败数
            logBuilder.Append("开始检查预定HH库存失败订单。");

            var listOrderInQueueOrderFailed = pmsContext.OrderPackWMSQueues.Where(x => x.Status == QueueStatusEnum.OrderFailed.ToString());
            var countForOrderFailed = listOrderInQueueOrderFailed.Count();
            if (countForOrderFailed > 0)
            {
                groupedMessage.Append(FormatMessage("检查出预定HH库存失败订单", countForOrderFailed));
                logBuilder.Append($"检查出预定HH库存失败订单，数量：{countForOrderFailed}。");
            }

            logBuilder.Append("检查预定HH库存失败订单完成。");

            DDNotificationProxy.MarkdownNotification($"监控报警", groupedMessage.ToString(), ddToken);
            DDNotificationProxy.MarkdownNotification($"监控报警", groupedMessage.ToString(), ddToken2);
        }
    }
}
