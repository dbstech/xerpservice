﻿using System.Collections.Generic;
using Abp.Dependency;
using System.Threading.Tasks;
using MagicLamp.PMS.Entities;
using Abp;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.BackgroundJobs;
using MagicLamp.PMS.Proxy.ECinx;
using Abp.Runtime.Caching;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.XERP.BackgroundJob.Common;
using System.Linq;

namespace MagicLamp.XERP.BackgroundJob
{
    public class CreateOrUpdateEcinxProduct : CommonBackgroundJob<List<string>>
    {
        private IRepository<ProductEntity, string> _productRepository;

        private ECinxClient _EcinxClient; //连接Ecinx同步产品
        private ECinxClient _WHYCEcinxClient; //连接EHYC Ecinx同步产品


        private const string ClogTagType = "syncproduct";
        Dictionary<string, string> clogTags = new Dictionary<string, string>
        {
            ["type"] = ClogTagType
        };

        public CreateOrUpdateEcinxProduct(
            IRepository<ProductEntity, string> productRepository
        )
        {
            _productRepository = productRepository;

            ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
            _EcinxClient = new ECinxClient(cacheManager, ECinxClient.EcinxAccountEnum.CQBSC); //连接Ecinx同步产品 任选一个数据源即可）
            _WHYCEcinxClient = new ECinxClient(cacheManager, ECinxClient.EcinxAccountEnum.YIWUYINGCHI); //连接WHYC Ecinx同步产品 任选一个数据源即可）
        }


        [UnitOfWork]
        public override void Process(List<string> skus)
        {

            CLSLogger.Info("同步产品列表", skus.ToJsonString(), clogTags);

            foreach (var _sku in skus)
            {
                var entity = _productRepository.FirstOrDefault(x => x.SKU.Trim().ToUpper() == _sku.Trim().ToUpper());
                EcinxConfigerData config = EcinxConfigerData.Configuration;
                //WHYC
                if (config.PushWHYCMFDWarehouseID.Contains((int)entity.WarehouseID))
                {
                    var queryResponse = _WHYCEcinxClient.GoodsQuery(new GoodsQueryInput
                    {
                        code = _sku
                    });


                    if (queryResponse.Result.result.total > 0)
                    {
                        // 存在 更新
                        var EcinxResponse = _WHYCEcinxClient.GoodsUpdate(new GoodsAddInput
                        {
                            code = entity.SKU.Trim(),
                            name = entity.ChineseName.Trim(),
                            barCode = entity.Barcode1.Trim(),
                            catalogCode = "KJED"
                            //height = input.High,
                            //length = input.Length
                        });
                    }
                    else
                    {
                        //不存在写入
                        var EcinxResponse = _WHYCEcinxClient.GoodsAdd(new GoodsAddInput
                        {
                            code = entity.SKU.Trim(),
                            name = entity.ChineseName.Trim(),
                            barCode = entity.Barcode1.Trim(),
                            catalogCode = "KJED"
                            //height = input.High,
                            //length = input.Length
                        });
                    }
                }
                //MFD
                else
                {
                    var queryResponse = _EcinxClient.GoodsQuery(new GoodsQueryInput
                    {
                        code = _sku
                    });

                    if (queryResponse.Result.result.total > 0)
                    {
                        // 存在 更新
                        var EcinxResponse = _EcinxClient.GoodsUpdate(new GoodsAddInput
                        {
                            code = entity.SKU.Trim(),
                            name = entity.ChineseName.Trim(),
                            barCode = entity.Barcode1.Trim(),
                            //catalogCode = "MFD"
                            //height = input.High,
                            //length = input.Length
                        });
                    }
                    else
                    {
                        //不存在写入
                        var EcinxResponse = _EcinxClient.GoodsAdd(new GoodsAddInput
                        {
                            code = entity.SKU.Trim(),
                            name = entity.ChineseName.Trim(),
                            barCode = entity.Barcode1.Trim(),
                            //catalogCode = "MFD"
                            //height = input.High,
                            //length = input.Length
                        });
                    }

                }

              

            }
           



        }
    }
}
