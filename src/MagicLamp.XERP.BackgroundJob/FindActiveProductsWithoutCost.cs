
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy;

namespace MagicLamp.XERP.BackgroundJob
{
    public class FindActiveProductsWithoutCost : ITransientDependency
    {
        private IRepository<ProductEntity, string> _productRepository;
        private IUnitOfWorkManager _unitOfWorkManager;

        public FindActiveProductsWithoutCost(
            IRepository<ProductEntity, string> productRepository,
            IUnitOfWorkManager unitOfWorkManager
        )
        {
            _productRepository = productRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        public void Execute()
        {
            List<ProductEntity> products = null;
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                products = _productRepository.GetAll().Where(x => x.ActiveFlag == true && !x.Cost.HasValue).ToList();
                unitOfWork.Complete();
            }

            if (products.Count == 0)
            {
                return;
            }

            string ddSignature = "SEC9c666eb72d24ffa421cc71e1fccc894c2d639213b6e61fd384db35930a67cf34";
            string ddToken = "0e35506f316c134c535ff9ccba5387111c1bc33c136958b9b47977b1984f7b44";
            string title = "# 以下产品缺少成本数据 \n\n";
            var skus = products.Select(product => product.SKU);
            string text = string.Join(", ", skus);

            try
            {
                DDNotificationProxy.MarkdownNotification(title, title + text, ddToken, ddSignature, null, 100000);
            }
            catch (Exception ex)
            {
                CLSLogger.ErrorWithSentry("FindActiveProductsWithoutCost job error", ex);
                throw ex;
            }
        }
    }
}
