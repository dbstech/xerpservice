﻿using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Dependency;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Data.SqlClient;
using MagicLamp.PMS.Entities.DBQueryTypes;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.Domain;

namespace MagicLamp.XERP.BackgroundJob
{
    public class RefreshSKULockQtyJob : CommonBackgroundJob<List<string>>
    {

        HEERPDbContext hEERPDbContext = null;

        public RefreshSKULockQtyJob()
        {
            hEERPDbContext = new HEERPDbContextFactory().CreateDbContext();
        }

        public override void Process(List<string> skus)
        {
            ProductDomainService productDomainService = new ProductDomainService();
            StringBuilder log = new StringBuilder();

            if (skus.IsNullOrEmpty())
            {
                FullRefreshLockQty();
            }
            else
            {
                int pageSize = 50;
                int totalPage = skus.Count % pageSize == 0 ? skus.Count / pageSize : skus.Count / pageSize + 1;
                log.Append($" Sku total count: {skus.Count}, totalPage: {totalPage}, ");

                for (int i = 0; i < totalPage; i++)
                {
                    var currentSkus = skus.Skip(i * pageSize).Take(pageSize).ToList();
                    productDomainService.RecalculateSkuLockQty(currentSkus);
                    log.Append($" page: {i + 1} processing completed ");
                }

                LogTags["method"] = "RefreshSKULockQtyJobBySku";
                CLSLogger.Info("RefreshSkuLockQtySuccessed", log.ToString(), LogTags);
            }

        }


        protected void FullRefreshLockQty()
        {
            ProductDomainService productDomainService = new ProductDomainService();
            StringBuilder log = new StringBuilder();
            Stopwatch sw = new Stopwatch();

            sw.Start();
            //query all skus which lockqty not equal zero in order to refresh lock qty
            var skus = hEERPDbContext.APIStocks.Where(x => x.Qty_locked != 0).Select(x => x.SKU).ToList();


            //cache order lock
            DateTime cacheOrderStartDate = DateTime.Now.AddDays(-3);
            var cacheOrderLock = hEERPDbContext.CacheOrderDetails
                .Where(x => x.CTime > cacheOrderStartDate)
                .GroupBy(x => x.SKU)
                .Select(x =>
              new SimpleSkuQueryType
              {
                  SKU = x.Key,
                  Qty = x.Sum(item => item.Qty)
              }).ToList();

            string orderpackSQL = $@" SELECT detail.sku AS 'SKU',detail.qty AS 'QTY', pack.orderpackno as 'orderPackNo' FROM dbo.orders_pack AS pack WITH (NOLOCK)
                     INNER JOIN dbo.orders_pack_detail AS detail WITH(NOLOCK)
                     ON pack.opid = detail.opid
                     WHERE
                     detail.flag = 1
                     AND pack.[freezeflag]  in (0,7,8)
                     AND pack.assign_time IS NULL AND pack.status= 0 AND pack.out_time IS NULL
                     AND pack.opt_time>=dateadd(day,-60, getdate())";

            var orderPackLocks = hEERPDbContext.SimpleSkuDBQuery.FromSql(orderpackSQL).ToList();

            //unpaid order lock
            var unpaidOrderLock = hEERPDbContext.OrderSkuLocks.GroupBy(x => x.SKU)
                .Select(x =>
                new SimpleSkuQueryType
                {
                    SKU = x.Key,
                    Qty = x.Sum(item => item.Qty)
                }).ToList();

            //unsplit order lock
            var orderStartDate = DateTime.Now.AddDays(-3);
            var unSplitOrderLock = (from order in hEERPDbContext.Orders
                                    join orderDetail in hEERPDbContext.OrderDetails
                                    on order.oid equals orderDetail.oid
                                    where order.status == 0 && order.valflag == 1 && order.orderCreateTime > orderStartDate
                                    group orderDetail by orderDetail.sku into result
                                    select new SimpleSkuQueryType
                                    {
                                        SKU = result.Key,
                                        Qty = result.Sum(item => item.qty)
                                    }).ToList();

            sw.Stop();

            log.Append($"Query lock qty total elapsed time: {sw.ElapsedMilliseconds} ms, cacheOrderLock: {cacheOrderLock.Count}, orderPackLocks: {orderPackLocks.Count}, unpaidOrderLock: {unpaidOrderLock.Count}, unSplitOrderLock: {unSplitOrderLock.Count}");

            LogTags["method"] = "FullRefreshLockQty";
            CLSLogger.Info("RefreshSkuLockQtySuccessed", log.ToString(), LogTags);

            productDomainService.RecalculateSkuLockQty(skus, cacheOrderLock, orderPackLocks, unSplitOrderLock, unpaidOrderLock);
        }


    }
}
