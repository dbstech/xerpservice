using System.Collections.Generic;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.EntityFrameworkCore;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.EntityFrameworkCore;
using EFCore.BulkExtensions;
using System.Linq;
using Abp.Extensions;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy;
using Cmq_SDK;
using Abp.Domain.Uow;
using System;
using MagicLamp.PMS.Infrastructure.Extensions;

namespace MagicLamp.XERP.BackgroundJob
{
    public class SyncHHPriceTo188 : BackgroundJob<List<SyncHHStockDTO>>, ITransientDependency
    {
        private const int WarehouseIdNZ = 6;
        private IDbContextProvider<PMSDbContext> _pmsDbContextProvider;
        private IUnitOfWorkManager _unitOfWorkManager;
        public Dictionary<string, string> LogTags = new Dictionary<string, string>();
      

        public SyncHHPriceTo188(IDbContextProvider<PMSDbContext> pmsDbContextProvider, IUnitOfWorkManager unitOfWorkManager)
        {
            _pmsDbContextProvider = pmsDbContextProvider;
            _unitOfWorkManager = unitOfWorkManager;
        }
        public override void Execute(List<SyncHHStockDTO> syncHHStockDtos)
        {
            LogTags["method"] = "syncHHStockDtos";
            LogTags["BulkInsertOrUpdate"] = "syncHHStockDtos";

            //CLSLogger.Info("SyncHHPriceTo188", syncHHStockDtos.ToJsonString(), LogTags);


            List<HHProductPrice> hHProductPrices = new List<HHProductPrice> { };
            List<ProductPriceMqDto> productPriceMqDtos = new List<ProductPriceMqDto> { };
            List<string> hhSkus = syncHHStockDtos.Select(x => x.SKU.ToUpper()).ToList();

            //Distinct
            List<string> hhSkusNew = new List<string>();
            hhSkusNew.AddRange(hhSkus.Distinct());


            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                PMSDbContext pMSDbContext = _pmsDbContextProvider.GetDbContext();
                Dictionary<string, string> hhSkuDict = pMSDbContext.Products.Where(x => hhSkusNew.Contains(x.HHSKU) && x.WarehouseID == WarehouseIdNZ &&  x.ActiveFlag == true ).ToDictionary(x => x.HHSKU, x => x.SKU.ToUpper());

                syncHHStockDtos.ForEach(x =>
                    {
                        string sValue = "";
                        // Key Exists 
                        if (hhSkuDict.TryGetValue(x.SKU, out sValue))
                        {
                            string sku = hhSkuDict[x.SKU];
                            hHProductPrices.Add(new HHProductPrice
                            {
                                HHSKU = x.SKU,
                                Price = x.Price,
                                WholeSalePrice = x.WholesalePrice,
                                LastModified = DateTime.UtcNow
                            });
                            if (!sku.IsNullOrWhiteSpace())
                            {
                                productPriceMqDtos.Add(new ProductPriceMqDto
                                {
                                    SKU = sku,
                                    ProductPrice = x.WholesalePrice,
                                    Type = "HHWholesalePrice",
                                    BusinessCode = "yishan",
                                    Currency = "NZD"
                                });
                            }
                        }
                    }

                );

                if (hHProductPrices.Count() == 0)
                {
                    return;
                }

                //LogTags["method"] = "SyncHHPriceTo188";
                //LogTags["BulkInsertOrUpdate"] = "hHProductPrices";

                //CLSLogger.Info("SyncHHPriceTo188", hHProductPrices.ToJsonString(), LogTags);


                hHProductPrices = hHProductPrices.Distinct().ToList();

                pMSDbContext.BulkInsertOrUpdate(hHProductPrices, new BulkConfig
                {
                    PropertiesToExclude = new List<string> { nameof(HHProductPrice.CreationTime) },
                });
                unitOfWork.Complete();
            }

            // BaseMqDTO<List<ProductPriceMqDto>> skuInfoMqDto = new BaseMqDTO<List<ProductPriceMqDto>>();
            // skuInfoMqDto.MessageBody = productPriceMqDtos;
            // List<string> tags = new List<string> { "yishan" };
            // TencentCMQProxy.PublishTopicMessage(TCConstants.CMQTopics.TOPIC_SKUPRICEINFO, skuInfoMqDto, tags);
        }
    }
}