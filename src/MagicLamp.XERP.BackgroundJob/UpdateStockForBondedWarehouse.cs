using System;
using System.Linq;
using System.Collections.Generic;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;

namespace MagicLamp.XERP.BackgroundJob
{
    public class UpdateStockForBondedWarehouse
    {
        private IRepository<ProductEntity, string> _productRepository;
        private IRepository<APIStockEntity, string> _apiStockRepository;
        private IRepository<WareHouseEntity, int> _warehouseRepository;
        private IUnitOfWorkManager _unitOfWorkManager;
        private IBackgroundJobManager _backgroundJobManager;

        public UpdateStockForBondedWarehouse(
            IRepository<ProductEntity, string> productRepository,
            IRepository<APIStockEntity, string> apiStockRepository,
            IRepository<WareHouseEntity, int> warehouseRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IBackgroundJobManager backgroundJobManager
        )
        {
            _productRepository = productRepository;
            _apiStockRepository = apiStockRepository;
            _warehouseRepository = warehouseRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _backgroundJobManager = backgroundJobManager;
        }
        private void SyncProducts(bool useIdCode2 = false)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                Dictionary<string, string> clogTags = new Dictionary<string, string>();
                clogTags["jobid"] = Guid.NewGuid().ToString();
                clogTags["type"] = "UpdateStockForBondedWarehouseJob";

                List<BondedWarehouseProductDTO> bondedWarehouseProducts = new BondedWarehouseProxy().GetAllProducts(useIdCode2);
                // T3 新库存
                List<BondedWarehouseProductDTO> T3bondedWarehouseProductsinvQuery = new BondedWarehouseProxy().T3GetAllProductsQty(useIdCode2);

                var T3bondedWarehouseProductinvQueryDict = T3bondedWarehouseProductsinvQuery.GroupBy(
                    x => x.Barcode
                )
                .Select(
                    x => new
                    {
                        Barcode = x.Key,
                        Quantity = x.Sum(y => y.Quantity)
                    }
                )
                .ToDictionary(x => x.Barcode, x => x);

                if (bondedWarehouseProducts.IsNullOrEmpty())
                {
                    CLSLogger.Error("UpdateStockForBondedWarehouseJob失败", "更新BondedWarehouse库存失败,未找到相关产品", clogTags);
                    return;
                }

                var bondedWarehouseProductDict = bondedWarehouseProducts.GroupBy(
                    x => x.Barcode
                )
                .Select(
                    x => new
                    {
                        Barcode = x.Key,
                        Quantity = x.Sum(y => y.Quantity),
                        ExpiryDate = x.Min(y => y.ExpiryDate)
                    }
                )
                .ToDictionary(x => x.Barcode, x => x);

                IEnumerable<string> barcodes = bondedWarehouseProductDict.Select(x => x.Key);
                var cqWarehouseIds = new List<int> { 15 };
                if (useIdCode2)
                {
                    cqWarehouseIds = new List<int> { 31, 32, 33 };
                }

                var products = _productRepository.GetAll()
                .Where(
                    x =>
                    cqWarehouseIds.Contains(x.WarehouseID.GetValueOrDefault())
                    && barcodes.Contains(x.Barcode1)
                )
                .Select(
                    x => new
                    {
                        Barcode = x.Barcode1,
                        Sku = x.SKU
                    }
                )
                .ToList();



                var warehouseproducts = _productRepository.GetAll()
                .Where(
                    x =>
                    cqWarehouseIds.Contains(x.WarehouseID.GetValueOrDefault())
                )
                .Select(
                    x => new
                    {
                        Barcode = x.Barcode1,
                        Sku = x.SKU
                    }
                )
                .ToList();



                var barcodesNotFound = barcodes.Except(products.Select(x => x.Barcode));
                if (barcodesNotFound.Count() > 0)
                {
                    CLSLogger.Info("UpdateStockForBondedWarehouseJob", $"更新BondedWarehouse未找到相关产品barcode: {string.Join(",", barcodesNotFound)}", clogTags);
                }

                IEnumerable<string> skus = products.Select(x => x.Sku);
                Dictionary<string, string> skuBarcodeDict = products.ToDictionary(x => x.Sku, x => x.Barcode);

                List<APIStockEntity> apiStocks = _apiStockRepository.GetAll().Where(x => skus.Contains(x.SKU)).ToList();


                // AllAPIStore(BondedWarehouse) - Found = LeftSKU
                IEnumerable<string> warehouseproductskus = warehouseproducts.Select(x => x.Sku);
                IEnumerable<string> skusFound = apiStocks.Select(x => x.SKU);
            

                IEnumerable<string> skusNotFound = warehouseproductskus.Except(skusFound);

                if (skusNotFound.Count() > 0)
                {
                    CLSLogger.Info("UpdateStockForBondedWarehouseJob", $"更新BondedWarehouse未找到相关产品sku: {string.Join(",", skusNotFound)}", clogTags);
                    _backgroundJobManager.EnqueueAsync<ProductRelatedInfoValidationJob, List<string>>(skusNotFound.ToList());
                    //_backgroundJobManager.Enqueue<ProductRelatedInfoValidationJob, List<string>>(skusNotFound.ToList(), BackgroundJobPriority.Normal, TimeSpan.FromSeconds(30));
                }

                foreach (APIStockEntity apiStock in apiStocks)
                {
                    string barcode = skuBarcodeDict[apiStock.SKU];
                    // new qty
                    int InvQTY = T3bondedWarehouseProductinvQueryDict[barcode].Quantity;

                    apiStock.Qty_available = InvQTY- apiStock.Qty_locked; //保税仓同步的库存数量，需要减去已经锁定数量；
                    apiStock.Qty = InvQTY;
                    apiStock.Qty_aladdin = InvQTY;
                    apiStock.Expired_date = bondedWarehouseProductDict[barcode].ExpiryDate?.ToString("MM/yyyy");
                    apiStock.Updated_time = DateTime.Now;
                }

                unitOfWork.Complete(); 

                CLSLogger.Info("UpdateStockForBondedWarehouseJob", $"更新了BondedWarehouse以下sku: {string.Join(",", apiStocks.Select(x => x.SKU))},仓属性:{string.Join(",", cqWarehouseIds)}", clogTags);
            }
        }

        public void Execute()
        {
            SyncProducts();
            SyncProducts(true);
        }
    }
}