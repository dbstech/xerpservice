﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Cmq_SDK;
using Hangfire;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace MagicLamp.XERP.BackgroundJob
{
    public class ProductPriceUpdateNotifyJob : CommonBackgroundJob<List<ProductPriceMqDto>>
    {

        public ProductPriceUpdateNotifyJob()
        {


        }

        [Queue("critical")]
        public override void Process(List<ProductPriceMqDto> inputDtos)
        {
            // var groups = inputDtos.GroupBy(x => x.BusinessCode).ToList();

            // foreach (var item in groups)
            // {
            //     BaseMqDTO<List<ProductPriceMqDto>> skuInfoMqDto = new BaseMqDTO<List<ProductPriceMqDto>>();
            //     skuInfoMqDto.MessageBody = item.ToList();
            //     List<string> tags = new List<string> { item.Key };
            //     TencentCMQProxy.PublishTopicMessage(TCConstants.CMQTopics.TOPIC_SKUPRICEINFO, skuInfoMqDto, tags);
            // }
            
        }


    }
}
