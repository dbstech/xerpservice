﻿using System;
using System.Collections.Generic;
using System.Text;
using MagicLamp.PMS.DTOs;
using MagicLamp.XERP.BackgroundJob.Common;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.EntityFrameworkCore;
using System.Linq;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.DTOs.BondedWarehouse;
using MagicLamp.PMS.Infrastructure;
using Abp.UI;
using MagicLamp.PMS;
using MagicLamp.PMS.DTOs.Enums;
using Abp.BackgroundJobs;
using Abp.Dependency;

namespace MagicLamp.XERP.BackgroundJob
{
    /// <summary>
    /// Re-sync order status for last month orders from flux
    /// </summary>
    public class SyncFluxOrderStatusJob : CommonBackgroundJob<string>
    {
        public override void Process(string input)
        {

            HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
            FluxWMSDbContext fluxDbContext = new FluxWMSDbContextFactory().CreateDbContext();
            StringBuilder log = new StringBuilder();

            try
            {
                List<int> status = new List<int> { (int)OrderPackStatusEnum.Assigned, (int)OrderPackStatusEnum.Packed };
                var beginTime = DateTime.UtcNow.AddMonths(-1);
                List<int> exportFlags = PMSConsts.NeedPushFluxExportFlag;
                int pagesize = 30;

                int totalCount = heERPContext.OrderPacks.Count(x => status.Contains(x.status.Value) && x.pushOrderTime > beginTime
                 && exportFlags.Contains(x.exportFlag.Value));

                int totalPage = totalCount % pagesize == 0 ? totalCount / pagesize : totalCount / pagesize + 1;

                log.Append($"TotalCount: {totalCount}, TotalPage: {totalPage}; ");

                for (int i = 0; i < totalPage; i++)
                {
                    var orderPacks = heERPContext.OrderPacks.Where(x => status.Contains(x.status.Value) && x.pushOrderTime > beginTime
                    && exportFlags.Contains(x.exportFlag.Value))
                        .OrderBy(x => x.opID).Skip(i * pagesize).Take(pagesize).ToList();

                    log.Append($"Processing page {i + 1}, orderPackCount: {orderPacks.Count}; ");

                    if (orderPacks.IsNullOrEmpty())
                    {
                        continue;
                    }

                    var orderpacknos = orderPacks.Select(x => x.orderPackNo).ToList();

                    //get flux order status
                    var fluxOrders = fluxDbContext.FluxDocOrderHeaders.Where(x => orderpacknos.Contains(x.OrderPackNo) && x.SOStatus == "99")
                        .Select(x => new
                        {
                            x.OrderPackNo,
                            x.SOStatus,
                            x.LastShipmentTime
                        }).ToList();

                    int updatedCount = 0;
                    foreach (var orderPack in orderPacks)
                    {
                        var currentFluxOrder = fluxOrders.FirstOrDefault(x => x.OrderPackNo == orderPack.orderPackNo);
                        if (currentFluxOrder == null)
                        {
                            continue;
                        }

                        log.Append($"{orderPack.orderPackNo}, ");

                        orderPack.status = (int)OrderPackStatusEnum.Outbound;
                        orderPack.out_time = currentFluxOrder.LastShipmentTime ?? DateTime.Now;

                        OrderLogEntity orderLog = new OrderLogEntity
                        {
                            OID = orderPack.oID,
                            OPID = orderPack.opID,
                            OptDate = DateTime.UtcNow,
                            Comments = $"检测到订单状态未与富勒同步，系统自动从富勒同步出库状态",
                            OptName = "System"
                        };
                        heERPContext.OrderLogs.Add(orderLog);
                        updatedCount++;
                    }

                    int affectedCount = heERPContext.SaveChanges();

                    log.Append($"Complete page {i + 1}, updatedCount: {updatedCount}; ");
                }

                //trigger the job of re-push order to NSF
                IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
                backgroundJobManager.Enqueue<RepushOrderPackToNSFJob, string>(null, BackgroundJobPriority.High);

            }
            catch (Exception ex)
            {
                CLSLogger.Error("Sync order status from flux occured exception", ex, LogTags);
            }
            finally
            {
                CLSLogger.Info("Sync order status from flux success", log.ToString(), LogTags);
            }

        }


    }
}
