﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Cmq_SDK;
using Hangfire;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MagicLamp.PMS.Entities;

namespace MagicLamp.XERP.BackgroundJob
{
    /// <summary>
    /// The purpose of this job is to check and correct the completeness of product, such as stock etc.
    /// </summary>
    public class ProductRelatedInfoValidationJob : CommonBackgroundJob<List<string>>
    {

        public ProductRelatedInfoValidationJob()
        {


        }

        public override void Process(List<string> inputSkus)
        {

            HEERPDbContext hEERPDbContext = new HEERPDbContextFactory().CreateDbContext();
            PMSDbContext pMSDbContext = new PMSDbContextFactory().CreateDbContext();

            int pageSize = 1000;
            int totalPage = inputSkus.Count % pageSize == 0 ? inputSkus.Count / pageSize : inputSkus.Count / pageSize + 1;

            for (int i = 0; i < totalPage; i++)
            {
                var skus = inputSkus.OrderBy(x => x).Skip(i * pageSize).Take(pageSize).ToList();
                LogTags["trackid"] = string.Join(";", skus);

                var skuInfos = pMSDbContext.Products.Where(x => skus.Contains(x.SKU)).Select(x =>
                                        new
                                        {
                                            x.SKU,
                                            x.EnglishName,
                                            x.ChineseName
                                        }).ToList();

                if (skuInfos.IsNullOrEmpty())
                {
                    CLSLogger.Error("can not found any sku info", string.Empty, LogTags);
                    continue;
                }

                var apiStocks = hEERPDbContext.APIStocks.Where(x => skus.Contains(x.SKU)).ToList();

                foreach (var item in skuInfos)
                {
                    //check if has api_stock
                    var apiStock = apiStocks.FirstOrDefault(x => x.SKU.ToUpper().Trim() == item.SKU.ToUpper().Trim());
                    if (apiStock == null)
                    {
                        APIStockEntity stock = new APIStockEntity
                        {
                            SKU = item.SKU,
                            Flag = 1,
                            ProductNameCN = item.ChineseName,
                            ProductNameEN = item.EnglishName,
                            Qty_available = 0,
                            Qty_locked = 0,
                            Qty = 0,
                            Qty_hh = null,
                            Expiry_date_hh = null,
                            Expired_date = null,
                            Qty_aladdin = 0,
                            Syc_flag = true,
                            Qty_baseline = 0,
                            Qty_cache = 0,
                            Qty_heweb = 0,
                            Updated_time = DateTime.UtcNow
                        };

                        hEERPDbContext.APIStocks.Add(stock);
                    }
                    else
                    {
                        apiStock.ProductNameEN = item.EnglishName;
                        apiStock.ProductNameCN = item.ChineseName;
                    }
                }

                hEERPDbContext.SaveChanges();
            }

            hEERPDbContext.Dispose();
            pMSDbContext.Dispose();
        }


    }
}
