﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Cmq_SDK;
using Hangfire;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MagicLamp.PMS.DTOs.MqDTO;

namespace MagicLamp.XERP.BackgroundJob
{
    public class ProductBrandUpdateNotifyJob : CommonBackgroundJob<BaseUpdateMqDTO<BrandDTO>>
    {

        public ProductBrandUpdateNotifyJob()
        {


        }

        public override void Process(BaseUpdateMqDTO<BrandDTO> inputDto)
        {

            // TencentCMQProxy.PublishTopicMessage(TCConstants.CMQTopics.TOPIC_BRANDINFO, inputDto);

        }


    }
}
