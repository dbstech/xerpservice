using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MagicLamp.PMS.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace MagicLamp.XERP.BackgroundJob
{

    public class XerpBackgroundJobModule : AbpModule
    {
        public override void PreInitialize()
        {
        }

        public override void Initialize()
        {
        }
    }
}