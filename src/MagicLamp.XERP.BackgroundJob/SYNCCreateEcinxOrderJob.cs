﻿using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using MagicLamp.PMS.BaXi;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy;
using System.Linq;
using MagicLamp.PMS.YiWuBondedWarehouse;
using Abp.Runtime.Caching;
using System;
using System.Collections.Generic;
using TimeZoneConverter;
using MagicLamp.PMS.Infrastructure.Extensions;
using Abp.Extensions;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Proxy.ECinx.Models;
using MagicLamp.PMS.Proxy.ECinx;
using Abp.Dependency;
using MagicLamp.PMS.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Abp.AutoMapper;
using System.Collections.Generic;
using Castle.Core.Internal;

namespace MagicLamp.XERP.BackgroundJob
{
    public class SYNCCreateEcinxOrderJob
    {
        private IRepository<OrderPackEntity, int> _orderPackRepository;
        private IRepository<ProductEntity, string> _productRepository;
        private IRepository<OrderPackDetailEntity, int> _orderdetailRepository;
        private IRepository<OrderEntity, int> _orderRepository;
       // private IRepository<EcinxOrdersPackTradeEntity, int> _ecinxordersRepository;
        private SendNotificationToDingDingJob _dingdingNotificationJob;
        private PMSDbContext _pmsContext;
        private HEERPDbContext _heerpContext;

        private TimeZoneInfo timeZoneNZ = TZConvert.GetTimeZoneInfo("Pacific/Auckland");
        Dictionary<string, string> LogTags = new Dictionary<string, string>
        {
            ["jobid"] = Guid.NewGuid().ToString(),
            ["method"] = "SyncCreateEcinxOder"
        };

        public SYNCCreateEcinxOrderJob(
            IRepository<OrderPackEntity, int> orderPackRepository,
            IRepository<ProductEntity, string> productRepository,
            IRepository<OrderPackDetailEntity, int> orderdetailRepository,
            IRepository<OrderEntity, int> orderRepository
            //IRepository<EcinxOrdersPackTradeEntity, int> ecinxorderRepository,
        )
        {
            _orderPackRepository = orderPackRepository;
            _productRepository = productRepository;
            _orderdetailRepository = orderdetailRepository;
            _orderRepository = orderRepository;
            _heerpContext = new HEERPDbContextFactory().CreateDbContext();
            //_ecinxordersRepository = ecinxorderRepository;
            _dingdingNotificationJob = new SendNotificationToDingDingJob();
            _pmsContext = new PMSDbContextFactory().CreateDbContext();
            ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
        }

        public void Execute()
        {
            // 下订单
            // 0: Created Ecinx Success 1: Ordered  Ecinx Success 2: Ordered  Ecinx Failed
            //List<string> orderpacnolist = _pmsContext.EcinxOrdersPackTrades.Where(x => x.Status == 0).Select(x => x.OrderPackNo).ToList();
            string orderpackSQL =
                 $@" 
                    
                with cte as (
                      select orderid,Opid,OrderPackNo,PayTime,customsPaymentData,ta.createtime,
                         datediff(day,PayTime,getdate()) as OutOfStockDays,
                         goodsCode,qty,Status,DeliveryCode,ExportFlag,EcinxResponse,OriginalInfo,EcinxOrderCode
                      from EcinxOrdersPackTrade ta
                        CROSS APPLY OPENJSON (ta.originalinfo, N'$.details')
                        WITH (
                        goodsCode VARCHAR(200) N'$.goodsCode',
                        qty VARCHAR(200) N'$.qty'
                        ) AS packdetails 
                      where  ta.status in (0,2)
                    ) 
                    select distinct ta.OrderID,ta.Opid,ta.OrderPackNo,ta.PayTime,customsPaymentData,
                    Status,DeliveryCode,ExportFlag,EcinxResponse,OriginalInfo,ta.createtime,EcinxOrderCode
                    from cte ta
                    where isnull(EcinxResponse,'') not like N'%此订单已存在%'

                ";

            try
            {
                using (var PMSDbContext = new PMSDbContextFactory().CreateDbContext())
                {
                    PMSDbContext.Database.SetCommandTimeout(120);
                    //List<Ecinxorderpack> orderpacklist = PMSDbContext.EcinxOrdersPackTrades
                    //    .FromSql(orderpackSQL)
                    //    .MapTo<List<Ecinxorderpack>>()
                    //    .ToList();
                    List<Ecinxorderpack> orderpacklist = PMSDbContext.EcinxOrdersPackTrades
                                                                    .FromSql(orderpackSQL)
                                                                    .Select(x => new Ecinxorderpack
                                                                    {
                                                                        OrderPackNo = x.OrderPackNo,
                                                                        Opid = x.Opid,
                                                                        CustomsPaymentData = x.CustomsPaymentData,
                                                                        CreateTime = x.CreateTime,
                                                                        OrderID = x.OrderID,
                                                                        Status = x.Status,
                                                                        DeliveryCode = x.DeliveryCode,
                                                                        ExportFlag = x.ExportFlag,
                                                                        EcinxResponse = x.EcinxResponse,
                                                                        OriginalInfo = x.OriginalInfo,
                                                                        PayTime = x.PayTime,
                                                                        EcinxOrderCode = x.EcinxOrderCode,
                                                                    })
                                                                    .ToList();

                    if (orderpacklist != null && orderpacklist.Count > 0)
                    {
                        BondWarehousePackaged(orderpacklist);
                    }
                    else
                    {
                        CLSLogger.Error("Ecinx下单发生异常 Exception ", "No data found for orderpacklist.", LogTags);
                    }
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("EcinxException ", $"Error: {ex.Message}, StackTrace: {ex.StackTrace}", LogTags);
                throw;
            }
           
        }

        private void BondWarehousePackaged(List<Ecinxorderpack> orderpacnolist)
        {
            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();

            foreach (var orderpacknoObject in orderpacnolist)
            {
                OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x =>
                   x.orderPackNo == orderpacknoObject.OrderPackNo);

                if (orderPack.freezeFlag > 0)
                {
                    string message = $"定时补推, 包裹已冻结：{orderPack.orderPackNo} freezeflag: {orderPack.freezeFlag} {orderPack.freezeComment} ";
                    _dingdingNotificationJob.SendEcinxERPAlert("Ecinx 创建订单失败", message);
                    CLSLogger.Info("定时补推", $"包裹已冻结：{orderPack.orderPackNo}  freezeflag: {orderPack.freezeFlag}  {orderPack.freezeComment}", LogTags);

                    continue;
                }

                try
                {

                    EcinxOrdersPackTradeEntity orderPackTradeEntity;
                    orderPackTradeEntity = _pmsContext.EcinxOrdersPackTrades.FirstOrDefault(x => x.OrderPackNo == orderpacknoObject.OrderPackNo);

                    try
                    {
                        var input = orderPackTradeEntity.OriginalInfo.ConvertFromJsonString<EcinxOrderInput>();


                        string warehousecode = "", warehouseName = "";

                        EcinxConfigerData config = EcinxConfigerData.Configuration;
                        ExportResponse _ecinxconfig = config.GetExportByFlag(orderPackTradeEntity.ExportFlag ?? 0);
                        warehousecode = _ecinxconfig.Export.WarehouseCode;
                        warehouseName = _ecinxconfig.Export.WarehouseName;
                        //是否使用供销功能 19/02/2025
                        bool supplySales = config.supplySalesExportFlag.Contains(orderPackTradeEntity.ExportFlag ?? 0);


                        int differenceInDays = 0;
                        DateTime oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                        if (!string.IsNullOrWhiteSpace(orderpacknoObject.PayTime.ToString()))
                        {
                            oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(orderpacknoObject.PayTime.ToString()), timeZoneNZ);
                            DateTime newDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                            TimeSpan ts = newDate - oldDate;
                            differenceInDays = ts.Days;
                        }


                        //string MessageContent = $"<font color=#FF0000  face=黑体>推送方式: 定时或手动触发推送 </font> \n\n";
                        //       MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {orderpacknoObject.OrderPackNo}  \n\n";
                        //       MessageContent = MessageContent + $" ##### **付款时间**:  {orderpacknoObject.PayTime.ToString("yyyy-MM-dd HH:mm:ss")}  \n\n";
                        //       MessageContent = MessageContent + $" ##### **归属仓**:  {warehouseName}  \n\n";
                        //       MessageContent = MessageContent + $"**预估延迟推送天数**: **_<font face=华云彩绘 color=#FF0000> {differenceInDays} </font>_**  \n\n";

                        string MessageContent = $"推送方式: 定时或手动触发推送\n\n";
                        MessageContent = MessageContent + $" 订单号或包裹号:  {orderpacknoObject.OrderPackNo}  \n\n";
                        MessageContent = MessageContent + $" 付款时间:  {orderpacknoObject.PayTime.ToString("yyyy-MM-dd HH:mm:ss")}  \n\n";
                        MessageContent = MessageContent + $" 归属仓:  {warehouseName}  \n\n";
                        MessageContent = MessageContent + $" 预估延迟推送天数:{differenceInDays} \n\n";


                        EcinxOrder eorder = new EcinxOrder();

                        eorder.deduction = input.deduction;
                        eorder.discount = input.discount;
                        eorder.freight = input.freight;
                        eorder.orderTime = input.orderTime;
                        eorder.platformOrderCode = input.platformOrderCode;
                        eorder.declareOrderCode = input.declareOrderCode;
                        //// 阿狸： CQ奶粉普通线 82,106 渠道，指定Ecinx仓库，其它不指定Ecinx 仓库  2024.11.22
                        List<int> mfdyc_export = new List<int> { 82, 106 };//82 MFD //106 YC
                        eorder.warehouseCode = mfdyc_export.Contains(orderPack.exportFlag ?? 0) ? warehousecode : "";

                        eorder.receiver = input.receiver;
                        eorder.receiverTel = input.receiverTel;
                        eorder.receiverProvince = input.receiverProvince;
                        eorder.receiverCity = input.receiverCity;
                        eorder.receiverArea = input.receiverArea;
                        eorder.receiverAddress = input.receiverAddress;
                        eorder.tax = input.tax;
                        eorder.type = input.type;
                        //提交供销代码
                        if (supplySales)
                        {
                            eorder.providerCode = _ecinxconfig.Export.providerCode;
                            eorder.type = 2;
                        }

                        eorder.details = input.details;
                        eorder.customsPlatformCode = input.customsPlatformCode;
                        eorder.customsPlatformName = input.customsPlatformName;

                        eorder.purchaser = input.purchaser;
                        eorder.purchaserCardNo = input.purchaserCardNo;
                        eorder.purchaserCardType = input.purchaserCardType;
                        eorder.purchaserTel = input.purchaserTel;

                        eorder.payments = input.payments;
                        eorder.expressSheetInfo = input.expressSheetInfo;
                        eorder.presell = input.presell;
                        eorder.planDeliveryTime = input.planDeliveryTime;

                        eorder.purchaserCardFrontPic = input.GetType().GetProperty("purchaserCardFrontPic") != null ? input.purchaserCardFrontPic : "";
                        eorder.purchaserCardBackPic = input.GetType().GetProperty("purchaserCardBackPic") != null ? input.purchaserCardBackPic : "";

                        eorder.ExportFlag = orderPackTradeEntity.ExportFlag;


                        EcinxResponse result = null;


                        EcinxClientV2 ecinxv2 = new EcinxClientV2();
                        result = ecinxv2.CreateOrderV2(orderPackTradeEntity.ExportFlag ?? 0, eorder).Result;


                        if (!result.success)
                        {
                            //MessageContent = MessageContent + $" ##### **创建订单失败原因**:  **_<font face=华云彩绘 color=#FF0000> {result.errorMsg} </font>_** \n\n";
                            MessageContent = MessageContent + $"创建订单失败原因: {result.errorMsg} \n\n";

                            //禁用 24.10.2024
                            _dingdingNotificationJob.SendEcinxERPAlert("Ecinx 创建订单失败", MessageContent);
                            CLSLogger.Info("Ecinx创建订单失败", $"包裹号：{orderpacknoObject.OrderPackNo}", LogTags);
                            ecinpushorderLog.OrderPackNo = orderpacknoObject.OrderPackNo;
                            ecinpushorderLog.LackStockDays = -1;
                            ecinpushorderLog.MessageType = "Ecinx创建订单(异步重试)";
                            ecinpushorderLog.FaildReason = result.errorMsg;
                            ecinpushorderLog.MessageDetail = "";
                            _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                            _pmsContext.SaveChanges();

                            continue;
                        }

                    }
                    catch (Exception ex)
                    {
                        CLSLogger.Error("EcinxException ", $"JSON Parsing Error: {ex.Message}", LogTags);
                        throw;
                    }
                }
                catch (Exception ex)
                {
                    CLSLogger.Error("Ecinx下单发生异常 Exception ", "参数：" + orderpacknoObject.OriginalInfo.ToJsonString() + "异常：" + ex, LogTags);

                    ecinpushorderLog.OrderPackNo = orderpacknoObject.OrderPackNo;
                    ecinpushorderLog.LackStockDays = -1;
                    ecinpushorderLog.MessageType = "Ecinx创建订单(异步重试)";
                    ecinpushorderLog.FaildReason = ex.ToJsonString();
                    ecinpushorderLog.MessageDetail = "";
                    _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                    _pmsContext.SaveChanges();

                    throw new Exception($"Ecinx查询接口失败，订单号：{ex.ToJsonString()}");
                }
                
            }


        }


        public class Ecinxorderpack
        {
            public int Opid { get; set; }
            public string OrderPackNo { get; set; }
            public string CustomsPaymentData { get; set; }
            public DateTime CreateTime { get; set; }
            public string OrderID { get; set; }
            public int Status { get; set; }
            public string DeliveryCode { get; set; }
            public int? ExportFlag { get; set; }
            public string EcinxResponse { get; set; }
            public string OriginalInfo { get; set; }
            public DateTime PayTime { get; set; }
            public string EcinxOrderCode { get; set; }
        }

    }
}
