﻿using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Runtime.Caching;
using Hangfire;
using MagicLamp.Flux.API.Models;
using MagicLamp.PMS;
using MagicLamp.PMS.Domain;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.DTOs.ThirdPart;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Infrastructure.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abp.BackgroundJobs;
using MagicLamp.PMS.Domain.ValueObjects;
using Abp.Extensions;

namespace MagicLamp.XERP.BackgroundJob
{
    public class PushOrderToFluxJob
    {
        private const string ClogTagType = "PushOrderToFluxJob";
        private const string MFD = "MFD";
        private const string HH = "HH";

        private const string UserDefine6 = "StockShare";

        private List<string> ToBeProcessed = new List<string>
        {
            QueueStatusEnum.Created.ToString(),
            QueueStatusEnum.OutOfStock.ToString(),
            QueueStatusEnum.OrderFailed.ToString()
        };

        private List<string> NoMiniumStockLimitSkus = new List<string>();

        private Dictionary<string, int> jobFluxStock = new Dictionary<string, int>();

        public ICacheManager Cache { get; set; }

        private HEERPDbContext heERPContext = null;
        private PMSDbContext pmsContext = null;
        private FluxWMSDbContext fluxWMSContext = null;

        public PushOrderToFluxJob()
        {
            heERPContext = new HEERPDbContextFactory().CreateDbContext();
            pmsContext = new PMSDbContextFactory().CreateDbContext();
            fluxWMSContext = new FluxWMSDbContextFactory().CreateDbContext();
        }


        public void PushOrderToHH(List<HHOrderLineDTO> hhorder, string orderpakno)
        {

            HHPushOrderInputDTO input = new HHPushOrderInputDTO
            {
                order_no = orderpakno,
                origin = "HH",
                order_lines = hhorder
            };

            var result = HHProxy.PushOrderToHH(input);
        }

        //如该SKU没有同步过HH的库存，则查询魔法灯库存, 如该SKU同步过HH库存，则使用HH的库存
        private IsStockFulfilledOutputDTO IsStockFulfilled(OrderPackEntity orderPackInfo, List<OrderPackDetailEntity> listOrderPackDetail, List<ProductEntity> listProduct,
            List<FluxInvLotLocIdEntity> listFluxInvLotLocId, List<APIStockEntity> listApiStock, string partner, bool ignoreOutOfStock, string status,
            HHCreateOrderResultDTO resultFromCachedProcessingInHH = null)
        {

            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["trackid"] = orderPackInfo.opID.ToString(),
                ["orderpackno"] = orderPackInfo.orderPackNo,
                ["orderid"] = orderPackInfo.orderID,
                ["type"] = ClogTagType,
                ["method"] = "IsStockFulfilled"
            };

            StringBuilder log = new StringBuilder();
            log.Append($"检查库存开始，查询包裹：{orderPackInfo.orderPackNo} 所需SKU库存情况：");

            IsStockFulfilledOutputDTO result = new IsStockFulfilledOutputDTO
            {
                ListOrderPackDetailForHH = new List<OrderPackDetailDTO>(),
                ListOutOfStockSKU = new List<OrderPackDetailDTO>(),
                ListOrderedFluxSKU = new List<OrderPackDetailDTO>()
            };

            result.IsFulfilled = true;

            var listOrderPackDetailInfo = listOrderPackDetail.Where(x => x.opid == orderPackInfo.opID).ToList();

            List<HHOrderLineDTO> hhorder = new List<HHOrderLineDTO>();

            List<HHSKUWithQty> listSKUInfoFromHHGetOrder = new List<HHSKUWithQty>();
            if (resultFromCachedProcessingInHH != null)
            {
                HHPushOrderInputDTO input = new HHPushOrderInputDTO
                {
                    order_no = orderPackInfo.orderPackNo
                };

                var orderInHH = HHProxy.GetOrder(input);

                if (orderInHH.result == 0)
                {
                    listSKUInfoFromHHGetOrder = orderInHH.data;
                }
            }

            foreach (var detailItem in listOrderPackDetailInfo)
            {

                HHOrderLineDTO line = new HHOrderLineDTO();
                line.sku = detailItem.sku;
                line.qty = detailItem.qty;
               

                log.Append($"开始查询SKU：{detailItem.sku} 库存情况，订单所需Qty：{detailItem.qty}，");

                try
                {
                    //如果是已经取消的detail
                    if (detailItem.flag == 0)
                    {
                        log.Append($"已在orderpackdetails中取消，");
                        continue;
                    }

                    var itemAlreadyOrdered = listSKUInfoFromHHGetOrder.FirstOrDefault(x => x.sku.StandardizeSKU() == detailItem.sku.StandardizeSKU());

                    if (itemAlreadyOrdered != null && itemAlreadyOrdered.qty == detailItem.qty)
                    {
                        log.Append($"该品已在HH预定成功，不参与库存判断逻辑，");
                        OrderPackDetailDTO detailForHH = new OrderPackDetailDTO
                        {
                            sku = detailItem.sku,
                            qty = detailItem.qty
                        };
                        result.ListOrderPackDetailForHH.Add(detailForHH);
                        continue;
                    }

                    //find out HH stock qty
                    var skuInfo = listApiStock.FirstOrDefault(x => x.SKU.StandardizeSKU() == detailItem.sku.StandardizeSKU());

                    if (skuInfo == null)
                    {
                        log.Append($"查询库存失败，api_stocks无匹配SKU，");
                        //组装outofstocksku
                        MakeOutOfStockSKU(result, detailItem);
                        continue;
                    }

                    //查询魔法灯库存
                    //查富勒InvLot，查不到的话视为MFD没库存（为0）
                    int stockQtyFlux = 0;

                    if (!listFluxInvLotLocId.Any(x => x.SKU.StandardizeSKU() == detailItem.sku.StandardizeSKU()))
                    {
                        log.Append($"富勒无库存，富勒INV_LOT_LOC_ID无匹配SKU，");
                    }
                    else
                    {
                        var listUnqualifiedLocation = PMSConsts.UnqualifiedLocationId;
                        var excludedLocation = PMSConsts.excludedLocation;

                        //排除lost库位，排除O1-01-01库位（有可能退给HH）
                        var listFluxStock = listFluxInvLotLocId.Where(x =>
                            x.SKU.ToLower().Trim() == detailItem.sku.ToLower().Trim()
                            && !listUnqualifiedLocation.Contains(x.LocationID)
                            && !x.LocationID.Contains(excludedLocation)).ToList();

                        foreach (var stock in listFluxStock)
                        {
                            log.Append($"富勒明细: LocationId: {stock.LocationID}, LotNum: {stock.LotNum}, ValidQty: {stock.Qty - stock.QtyAllocated - stock.QtyOnHold}; ");
                        }

                        stockQtyFlux = listFluxStock.Sum(x => Convert.ToInt32(x.Qty - x.QtyAllocated - x.QtyOnHold));

                    }

                    log.Append($"富勒当前库存数：{stockQtyFlux}，");


                    //store stock qty for this job to use if not in dictionary
                    if (!jobFluxStock.ContainsKey(detailItem.sku))
                    {
                        jobFluxStock.Add(detailItem.sku, stockQtyFlux);
                    }
                    else
                    {
                        jobFluxStock.TryGetValue(detailItem.sku, out stockQtyFlux);
                        log.Append($"使用本次Job本地储存库存数量：{stockQtyFlux}。");
                    }


                    //如该SKU没有同步过HH的库存
                    if (!skuInfo.Qty_hh.HasValue)
                    {
                        if (stockQtyFlux >= detailItem.qty)
                        {

                            UpdateJobStoredQty(result, jobFluxStock, detailItem.sku, detailItem.qty, stockQtyFlux);
                        }
                        else
                        {
                            //组装outofstocksku
                            MakeOutOfStockSKU(result, detailItem);
                        }
                    }
                    //该SKU同步过HH库存，HH不共享MFD库存，MFD优先使用自家库存，当MFD库存不足且SKU为A类品时可共享HH库存
                    else
                    {
                        ProductDomainService productDomainService = new ProductDomainService();

                        var stockHH = skuInfo.Qty_hh.Value;
                        log.Append($"HH当前库存数：{stockHH}，");

                        //根据order.partner优先使用自家库存
                        if (!partner.IsNullOrEmpty() && partner.Equals(HH))
                        {

                            //锁HH库存
                            if (status != String.Empty)
                            {
                                productDomainService.AddHHStockLockByOpid(orderPackInfo, detailItem, status);
                            }

                            //HH库存满足
                            if (stockHH >= detailItem.qty)
                            {
                                log.Append($"HH库存满足，订单属于HH，");
                                OrderPackDetailDTO detailForHH = new OrderPackDetailDTO
                                {
                                    sku = detailItem.sku,
                                    qty = detailItem.qty
                                };
                                result.ListOrderPackDetailForHH.Add(detailForHH);
                            }
                            //HH库存不满足
                            else
                            {
                                log.Append($"HH库存不满足，订单属于HH，");
                                //组装outofstocksku
                                MakeOutOfStockSKU(result, detailItem);
                            }
                        }
                        //使用魔法灯库存
                        else
                        {
                            if (stockQtyFlux >= detailItem.qty)
                            {
                                //富勒库存满足
                                log.Append($"富勒库存满足，订单不属于HH，");

                                UpdateJobStoredQty(result, jobFluxStock, detailItem.sku, detailItem.qty, stockQtyFlux);
                            }
                            else
                            {
                                //富勒库存不满足
                                //查A类品
                                var productInfo = listProduct.FirstOrDefault(x => x.SKU.StandardizeSKU() == detailItem.sku.StandardizeSKU());

                                if (productInfo != null && productInfo.IsHHPurchase.HasValue && productInfo.IsHHPurchase.Value == true)
                                {

                                    //锁HH库存
                                    if (status != String.Empty)
                                    {
                                        productDomainService.AddHHStockLockByOpid(orderPackInfo, detailItem, status);
                                    }


                                    if (stockHH >= detailItem.qty)
                                    {
                                        log.Append($"HH库存满足，只使用HH库存，为A类品，订单不属于HH，");

                                        OrderPackDetailDTO detailForHH = new OrderPackDetailDTO
                                        {
                                            sku = detailItem.sku,
                                            qty = detailItem.qty
                                        };
                                        result.ListOrderPackDetailForHH.Add(detailForHH);

                                    }
                                    else
                                    {
                                        log.Append($"富勒库存加HH库存不满足，为A类品，订单不属于HH，");
                                        //组装outofstocksku
                                        MakeOutOfStockSKU(result, detailItem);
                                    }
                                }
                                else
                                {
                                    log.Append($"富勒库存不满足，且不为A类品，不可共享HH库存，订单不属于HH，");
                                    //组装outofstocksku
                                    MakeOutOfStockSKU(result, detailItem);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Append($"发生异常：{ex.Message}，");
                }
                finally
                {
                    log.Append($"结束查询SKU：{detailItem.sku}；；");
                }

                hhorder.Add(line);
            }

            // 强制推送HH Spark
            if (orderPackInfo.orderPackNo == "2040800211322-4-1")
            {
                PushOrderToHH(hhorder, orderPackInfo.orderPackNo);
            }


            if (!result.ListOutOfStockSKU.IsNullOrEmpty())
            {
                log.Append("全部查询完毕，包裹缺货。");
            }
            else if (result.ListOrderPackDetailForHH.IsNullOrEmpty())
            {
                log.Append("全部查询完毕，包裹只使用富勒库存。");
            }
            else if (!result.ListOrderPackDetailForHH.IsNullOrEmpty())
            {
                log.Append("全部查询完毕，包裹需向HH预定库存。");
            }

            CLSLogger.Info("查询库存结束", $"{log.ToString()}缺货明细：{result.ListOutOfStockSKU.ToJsonString()}。", clogTags);


            if (!ignoreOutOfStock)
            {
                //if not fulfilled, add qty back to jobFluxStock
                if (!result.IsFulfilled && !result.ListOrderedFluxSKU.IsNullOrEmpty())
                {
                    RestoreJobStoredQty(result, jobFluxStock);
                }
            }

            return result;
        }

        private void UpdateJobStoredQty(IsStockFulfilledOutputDTO dto, Dictionary<string, int> jobFluxStock, string sku, int orderedQty, int stockQty)
        {
            stockQty -= orderedQty;
            jobFluxStock[sku] = stockQty;

            OrderPackDetailDTO detailFlux = new OrderPackDetailDTO
            {
                sku = sku,
                qty = orderedQty
            };

            dto.ListOrderedFluxSKU.Add(detailFlux);
        }

        /// <summary>
        /// assemble sku that are out of stock
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="detailItem"></param>
        private void MakeOutOfStockSKU(IsStockFulfilledOutputDTO dto, OrderPackDetailEntity detailItem)
        {
            OrderPackDetailDTO outOfStockSKU = new OrderPackDetailDTO
            {
                sku = detailItem.sku,
                qty = detailItem.qty
            };
            dto.ListOutOfStockSKU.Add(outOfStockSKU);
            dto.IsFulfilled = false;
        }

        /// <summary>
        /// restore qty orderd in Flux back to jobFluxStock
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="jobFluxStock"></param>
        private void RestoreJobStoredQty(IsStockFulfilledOutputDTO dto, Dictionary<string, int> jobFluxStock)
        {
            foreach (var item in dto.ListOrderedFluxSKU)
            {
                string sku = item.sku;
                int orderedQty = item.qty;

                jobFluxStock[sku] += orderedQty;
            }
        }

        private bool PushToFlux(OrderPackWMSQueueEntity item, OrderPackEntity orderPackInfo, OrderEntity orderInfo, List<OrderPackDetailEntity> listOrderPackDetail,
            SeqEntity seqInfo, BusinessEntity businessInfo, FreightsEntity freightsInfo, List<OrderPackOutOfStockEntity> listOrderPackOutOfStock, List<OrderPackDetailDTO> orderPackDetailsForHH,
            string userDefine6 = null)
        {
            bool result = false;
            Dictionary<string, string> clogTags = new Dictionary<string, string>();

            clogTags["type"] = ClogTagType;
            clogTags["method"] = "PushToFlux";
            clogTags["orderpackno"] = item.OrderPackNo;

            StringBuilder log = new StringBuilder();

            //推flux
            FluxDomainService fluxDomainService = new FluxDomainService();


            FluxResponse fluxResponse = fluxDomainService.PushOrderToFlux(orderPackInfo, orderInfo, listOrderPackDetail, seqInfo, businessInfo, freightsInfo, orderPackDetailsForHH, userDefine6);


            //推失败
            if (fluxResponse == null || int.Parse(fluxResponse.Response.@return.returnFlag) != 1 && int.Parse(fluxResponse.Response.@return.returnFlag) != 2)
            {
                if (fluxResponse != null)
                {
                    var reason = fluxResponse.Response.@return.returnDesc;

                    item.Status = QueueStatusEnum.OrderFailed.ToString();
                    item.ReturnMessageFromFlux = reason;
                    item.UpdateTime = DateTime.UtcNow;


                    pmsContext.SaveChanges();
                }
            }
            //1成功，或2部分成功
            else if (int.Parse(fluxResponse.Response.@return.returnFlag) == 1 || (int.Parse(fluxResponse.Response.@return.returnFlag) == 2 && fluxResponse.Response.@return.returnDesc == "成功"
                && (fluxResponse.Response.@return.resultInfo[0].errordescr.Contains("记录已存在") || fluxResponse.Response.@return.resultInfo[0].errordescr.Contains("订单已存在"))))
            {
                result = true;
                var reason = fluxResponse.Response.@return.returnDesc;
                log.Append($"订单推富勒成功。结果：{reason}。");

                item.Status = QueueStatusEnum.Done.ToString();
                item.ReturnMessageFromFlux = reason;
                item.UpdateTime = DateTime.UtcNow;

                //释放hh锁定
                ProductDomainService productDomainService = new ProductDomainService();
                productDomainService.FreeHHStockLockByOpid(orderPackInfo);

                //update OrderPacks_OutOfStock对应包裹的缺货明细
                var existedList = listOrderPackOutOfStock.Where(x => x.OrderPackNo == item.OrderPackNo);

                if (!existedList.IsNullOrEmpty())
                {
                    foreach (var entry in existedList)
                    {
                        entry.Status = QueueStatusEnum.Done.ToString();
                        entry.UpdateTime = DateTime.UtcNow;
                    }

                    log.Append($"更新OrderPakcs_OutOfStock，包裹号：{item.OrderPackNo}，其相关数据所有状态修改为：{QueueStatusEnum.Done.ToString()}。");
                }

                orderPackInfo.pushOrderTime = DateTime.Now;

                pmsContext.SaveChanges();
                heERPContext.SaveChanges();
            }

            CLSLogger.Info("包裹推富勒结果", log.ToString(), clogTags);
            return result;
        }

        private HHCreateOrderResultDTO AssembleInputAndPushToHH(OrderPackWMSQueueEntity item, OrderEntity orderInfo, IsStockFulfilledOutputDTO stockFulfilled)
        {
            var shipmentInfo = orderInfo.shipment.Split(" ");

            HHPushOrderInputDTO input = new HHPushOrderInputDTO
            {
                order_no = item.OrderPackNo,
                //partner is null or empty then use MFD
                origin = orderInfo.partner.IsNullOrEmpty() ? shipmentInfo[shipmentInfo.Length - 1] : orderInfo.partner,
                order_lines = new List<HHOrderLineDTO>()
            };

            foreach (var detail in stockFulfilled.ListOrderPackDetailForHH)
            {
                var orderLine = new HHOrderLineDTO
                {
                    sku = detail.sku,
                    qty = detail.qty
                };
                input.order_lines.Add(orderLine);
            }

            var result = HHProxy.PushOrderToHH(input);
            return result;
        }

        private HHGetOrderOutputDTO GetOrderInHH(OrderPackWMSQueueEntity item)
        {
            HHPushOrderInputDTO input = new HHPushOrderInputDTO
            {
                order_no = item.OrderPackNo
            };

            var result = HHProxy.GetOrder(input);
            return result;
        }

        private bool IsOrderPackInQueueNeedSkip(OrderPackEntity orderPackInfo, OrderPackWMSQueueEntity item, List<OrderPackOutOfStockEntity> listOrderPackOutOfStock,
            ICacheManager Cache, Dictionary<string, string> clogTags)
        {
            bool hasCachedCancellingRecord = false;

            var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);
            var val = cacheManager.GetOrDefault(string.Format(CacheKeys.CANCELLEDORDERID, orderPackInfo.orderID));

            if (val != null)
            {
                hasCachedCancellingRecord = true;
            }

            //订单存在缓存取消记录 || 订单已冻结 || 订单已出库
            if (hasCachedCancellingRecord || orderPackInfo.freezeFlag != (int)OrderPackFreezeFlagEnum.未冻结 || orderPackInfo.status == (int)OrderPackStatusEnum.Outbound)
            {
                var previousStatus = item.Status;
                item.Status = orderPackInfo.status == (int)OrderPackStatusEnum.Outbound ? QueueStatusEnum.Done.ToString() : QueueStatusEnum.Refund.ToString();
                item.UpdateTime = DateTime.UtcNow;

                string titleMessage = orderPackInfo.status == (int)OrderPackStatusEnum.Outbound ? "订单已出库" : "订单已冻结";

                //尝试删除OrderPacks_OutOfStock中包裹相关数据
                var existedOutOfStock = listOrderPackOutOfStock.Where(x => x.OrderPackNo == item.OrderPackNo).ToList();

                if (existedOutOfStock == null)
                {
                    CLSLogger.Info(titleMessage, $"Queue中不进一步处理，由于目前状态仍会被Job处理，修改状态为{item.Status}。原状态：{previousStatus}。", clogTags);
                }
                else
                {
                    foreach (var entry in existedOutOfStock)
                    {
                        pmsContext.OrderPackOutOfStocks.Remove(entry);
                    }
                    CLSLogger.Info(titleMessage, $"Queue中不进一步处理，由于目前状态仍会被Job处理，修改状态为{item.Status}。原状态：{previousStatus}。" +
                    $"同时删除OrderPacks_OutOfStock中相关数据", clogTags);
                }
                pmsContext.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsSKUOrderedToHHSame(IsStockFulfilledOutputDTO stockFulfilled, HHGetOrderOutputDTO resultFromGetOrderInHH)
        {
            bool result = true;

            if (!stockFulfilled.IsFulfilled)
            {
                result = false;
                return result;
            }

            List<HHSKUWithQty> listSKUInfoFromHHGetOrder = resultFromGetOrderInHH.data;
            List<OrderPackDetailDTO> listSKUOrderedInThisIteration = stockFulfilled.ListOrderPackDetailForHH;

            if (listSKUInfoFromHHGetOrder.Count != listSKUOrderedInThisIteration.Count)
            {
                result = false;
                return result;
            }

            foreach (var skuOrderedThisTime in listSKUOrderedInThisIteration)
            {
                var skuThisIteration = skuOrderedThisTime.sku;
                var qtyThisIteration = skuOrderedThisTime.qty;

                var match = listSKUInfoFromHHGetOrder.FirstOrDefault(x => x.sku.StandardizeSKU() == skuThisIteration.StandardizeSKU() && x.qty == qtyThisIteration);
                if (match == null)
                {
                    result = false;
                    return result;
                }
            }

            return result;
        }

        private string CancelERPModifiedOrderInHH(OrderPackWMSQueueEntity item, Dictionary<string, string> clogTags)
        {
            HHPushOrderInputDTO input = new HHPushOrderInputDTO
            {
                order_no = item.OrderPackNo
            };

            HHCreateOrderResultDTO resultFromHH = HHProxy.CancelOrderInHH(input);

            if (resultFromHH == null || resultFromHH.result != 0)
            {
                if (resultFromHH.message.Equals("不能取消已经同步波次的订单"))
                {
                    CLSLogger.Info("订单在HH已经存在，但不能取消已经同步波次的订单", resultFromHH.ToJsonString(), clogTags);
                    return "不能取消已经同步波次的订单";
                }

                CLSLogger.Error("订单在HH已经存在，尝试在HH取消订单失败", resultFromHH.ToJsonString(), clogTags);

                return "失败";
            }
            else
            {
                CLSLogger.Error("订单在HH已经存在，尝试在HH取消订单成功", resultFromHH.ToJsonString(), clogTags);
                return "成功";
            }

        }

        private void AddReservedHHSKU(OrderPackWMSQueueEntity item, HHSKUInfo detail, OrderEntity orderInfo,
            OrderPackEntity orderPackInfo, int qty, StringBuilder logForFulfilled)
        {
            OrderPackReservedHHSKUEntity entry = new OrderPackReservedHHSKUEntity
            {
                OrderPackNo = item.OrderPackNo,
                SKU = detail.sku,
                Price = detail.price,
                CreationTime = DateTime.UtcNow,
                Partner = orderInfo.partner,
                OrderId = orderInfo.orderid,
                Status = OrderPackReservedStatusEnum.Created.ToString(),
                OutboundTime = orderPackInfo.out_time,
                Qty = qty
            };
            pmsContext.OrderPackReservedHHSKUs.Add(entry);
            logForFulfilled.Append($"新增一个Reserved SKU，SKU：{detail.sku}, QTY：{qty}, PRICE：{detail.price}。");
        }

        private bool ProcessOrderAlreadyExistedInHH(List<OrderPackReservedHHSKUEntity> existedReservedRecord, HHCreateOrderResultDTO result,
            IsStockFulfilledOutputDTO stockFulfilled, OrderPackWMSQueueEntity item,
            OrderEntity orderInfo, OrderPackEntity orderPackInfo,
            StringBuilder logForFulfilled, Dictionary<string, string> clogTags)
        {
            bool resultProcessed = false;

            HHGetOrderOutputDTO resultFromGetOrderInHH = GetOrderInHH(item);

            if (resultFromGetOrderInHH.result == 0)
            {
                if (IsSKUOrderedToHHSame(stockFulfilled, resultFromGetOrderInHH))
                {
                    //same
                    //不用再次reserve sku，除非没有reserve记录（首次创建超时）
                    if (existedReservedRecord.IsNullOrEmpty())
                    {
                        foreach (HHSKUInfo detail in result.data)
                        {
                            int qty = stockFulfilled.ListOrderPackDetailForHH.Where(x => x.sku.ToLower() == detail.sku.ToLower()).Select(x => x.qty).Sum();

                            AddReservedHHSKU(item, detail, orderInfo, orderPackInfo, qty, logForFulfilled);
                        }

                        pmsContext.SaveChanges();
                        CLSLogger.Info("推送订单给HH成功后，更新OrderPacks_ReservedHHSKU完成", logForFulfilled.ToString(), clogTags);

                        resultProcessed = true;
                        return resultProcessed;
                    }
                    else if (existedReservedRecord.Any(x => x.Status == QueueStatusEnum.Refund.ToString()))
                    {
                        foreach (var reserved in existedReservedRecord)
                        {
                            pmsContext.OrderPackReservedHHSKUs.Remove(reserved);
                        }

                        pmsContext.SaveChanges();

                        CLSLogger.Info("推送订单给HH成功后，存在已reserve但状态为refund的记录，删除所有reserved记录", $"包裹号: {item.OrderPackNo}", clogTags);


                        foreach (HHSKUInfo detail in result.data)
                        {
                            int qty = stockFulfilled.ListOrderPackDetailForHH.Where(x => x.sku.ToLower() == detail.sku.ToLower()).Select(x => x.qty).Sum();

                            AddReservedHHSKU(item, detail, orderInfo, orderPackInfo, qty, logForFulfilled);
                        }

                        pmsContext.SaveChanges();
                        CLSLogger.Info("推送订单给HH成功后，更新OrderPacks_ReservedHHSKU完成", logForFulfilled.ToString(), clogTags);

                        resultProcessed = true;
                        return resultProcessed;
                    }
                    else
                    {
                        resultProcessed = true;
                        return resultProcessed;
                    }
                }
                else
                {
                    CLSLogger.Info("订单在HH已经存在，但目前订单内商品和首次创建时不一致，尝试在HH取消订单", logForFulfilled.ToString(), clogTags);

                    string resultFromOrderCancelledInHH = CancelERPModifiedOrderInHH(item, clogTags);

                    if (resultFromOrderCancelledInHH.Equals("成功"))
                    {
                        item.Status = QueueStatusEnum.Created.ToString();
                        item.ReturnMessageFromHH = "取消订单成功";
                        item.UpdateTime = DateTime.UtcNow;

                        foreach (var reserved in existedReservedRecord)
                        {
                            pmsContext.OrderPackReservedHHSKUs.Remove(reserved);
                        }

                        pmsContext.SaveChanges();

                        CLSLogger.Info("订单在HH已经存在，但目前订单内商品和首次创建时不一致，在HH取消订单成功，同时删除reserved记录", $"包裹号: {item.OrderPackNo}，其在Queue中修改状态为{item.Status}", clogTags);
                    }
                    else if (resultFromOrderCancelledInHH.Equals("不能取消已经同步波次的订单"))
                    {
                        if (existedReservedRecord.IsNullOrEmpty())
                        {
                            foreach (HHSKUInfo detail in result.data)
                            {
                                int qty = stockFulfilled.ListOrderPackDetailForHH.Where(x => x.sku.ToLower() == detail.sku.ToLower()).Select(x => x.qty).Sum();

                                AddReservedHHSKU(item, detail, orderInfo, orderPackInfo, qty, logForFulfilled);
                            }

                            pmsContext.SaveChanges();
                            CLSLogger.Info("推送订单给HH成功后，更新OrderPacks_ReservedHHSKU完成", logForFulfilled.ToString(), clogTags);

                            resultProcessed = true;
                            return resultProcessed;
                        }
                        else
                        {
                            resultProcessed = true;
                            return resultProcessed;
                        }
                    }
                    else
                    {
                        //1. cannot cancel

                        CLSLogger.Info("HH返回此订单已经存在，但HH取消订单失败", logForFulfilled.ToString(), clogTags);

                        throw new Exception($"HH返回此订单已经存在，但HH取消订单失败。原因：{resultFromOrderCancelledInHH}");
                    }
                }
            }
            else
            {
                CLSLogger.Info("HH返回此订单已经存在，但HH查询订单失败", logForFulfilled.ToJsonString() + "参数:" + item.ToJsonString(), clogTags);
                throw new Exception($"HH返回此订单已经存在，但HH查询订单失败，{clogTags.ToJsonString()}");
            }

            return resultProcessed;
        }

        private void ProcessResultFromPushingToHH(HHCreateOrderResultDTO result, IsStockFulfilledOutputDTO stockFulfilled,
            OrderPackWMSQueueEntity item, List<OrderPackReservedHHSKUEntity> listReservedHHSKU,
            OrderEntity orderInfo, OrderPackEntity orderPackInfo,
            SeqEntity seqInfo, BusinessEntity businessInfo, FreightsEntity freightsInfo,
            List<OrderPackDetailEntity> listOrderPackDetail, List<OrderPackOutOfStockEntity> listOrderPackOutOfStock,
            StringBuilder logForFulfilled, Dictionary<string, string> clogTags)
        {
            //HH缓存成功，等待HH异步返回结果
            if (result.result == 0 && result.message.Equals("缓存成功"))
            {
                item.Status = QueueStatusEnum.CachedInHH.ToString();
                item.ReturnMessageFromHH = result.message;
                item.UpdateTime = DateTime.UtcNow;
                pmsContext.SaveChanges();
                CLSLogger.Info("推送订单给HH结束，HH存入缓存", $"结果: {result.message}, 包裹号: {item.OrderPackNo}，其在Queue中修改状态为{item.Status}", clogTags);
            }
            //HH创建订单成功 or 已经推过HH
            else if ((result.result == 0 && result.message.Equals("create success")) || (result.result == 1 && result.message.Equals("此订单已经存在")))
            {
                //拿到之前reserved中目前orderpackno所有的entry
                List<OrderPackReservedHHSKUEntity> existedReservedRecord = listReservedHHSKU.Where(x => x.OrderPackNo == item.OrderPackNo).ToList();
                item.ReturnMessageFromHH = result.message;

                //已经推过HH
                if (result.result == 1 && result.message.Equals("此订单已经存在"))
                {
                    bool resultProcessed = ProcessOrderAlreadyExistedInHH(existedReservedRecord, result, stockFulfilled, item, orderInfo, orderPackInfo, logForFulfilled, clogTags);
                    if (resultProcessed == true)
                    {
                        //推flux
                        var isPushToFluxSuccessful = PushToFlux(item, orderPackInfo, orderInfo, listOrderPackDetail, seqInfo, businessInfo, freightsInfo, listOrderPackOutOfStock, stockFulfilled.ListOrderPackDetailForHH, UserDefine6);

                        //推富勒不成功
                        if (!isPushToFluxSuccessful)
                        {
                            throw new Exception("订单推送富勒失败，请检查云端日志 " + $"包裹号：{item.OrderPackNo}" );
                        }
                        else
                        {
                            CLSLogger.Info("推送订单给HH成功后，推送给富勒成功", $"包裹号：{item.OrderPackNo}。", clogTags);
                        }
                    }
                }
                else
                {
                    //本次推送给HH的sku
                    var pushedToHHSKU = result.data.Select(x => x.sku).ToList();

                    //删除在reserved表中，但不在本次推送给HH包裹下的sku
                    foreach (var reserved in existedReservedRecord)
                    {
                        //reserved的sku不在此次推给hh的订单中，已不需要，删除
                        if (!pushedToHHSKU.Contains(reserved.SKU) && reserved.Status != OrderPackReservedStatusEnum.Binded.ToString())
                        {
                            logForFulfilled.Append($"删除一个Reserved SKU，SKU：{reserved.SKU}, QTY：{reserved.Qty}, PRICE：{reserved.Price}。");
                            pmsContext.OrderPackReservedHHSKUs.Remove(reserved);
                        }
                        else if (reserved.Status == OrderPackReservedStatusEnum.Binded.ToString())
                        {
                            logForFulfilled.Append($"尝试删除一个Reserved SKU被终止，其状态为{reserved.Status}，请检查CancelHHOrderJob" +
                                $"SKU：{reserved.SKU}, QTY：{reserved.Qty}, PRICE：{reserved.Price}。");
                        }
                    }

                    foreach (var detail in result.data)
                    {
                        var sku = detail.sku;
                        var qty = stockFulfilled.ListOrderPackDetailForHH.Where(x => x.sku.ToLower() == detail.sku.ToLower()).Select(x => x.qty).Sum();

                        //包裹下sku没reserve记录
                        if (existedReservedRecord.IsNullOrEmpty())
                        {
                            AddReservedHHSKU(item, detail, orderInfo, orderPackInfo, qty, logForFulfilled);
                        }
                        //包裹下sku有reserve过
                        else
                        {
                            var reservedSKU = existedReservedRecord.Select(x => x.SKU).ToList();
                            //当前推给hh的sku，没reserve过，新增
                            if (!reservedSKU.Contains(detail.sku))
                            {
                                AddReservedHHSKU(item, detail, orderInfo, orderPackInfo, qty, logForFulfilled);
                            }
                            //reserve过，更新
                            else
                            {
                                var existedReservedInfo = existedReservedRecord.FirstOrDefault(x => x.SKU == detail.sku);
                                existedReservedInfo.Price = detail.price;
                                existedReservedInfo.CreationTime = DateTime.UtcNow;
                                existedReservedInfo.Partner = orderInfo.partner;
                                existedReservedInfo.OrderId = orderInfo.orderid;
                                existedReservedInfo.Status = OrderPackReservedStatusEnum.Created.ToString();
                                existedReservedInfo.OutboundTime = orderPackInfo.out_time;
                                existedReservedInfo.Qty = qty;
                                logForFulfilled.Append($"更新一个Reserved SKU，SKU：{detail.sku}, QTY：{qty}, PRICE：{detail.price}。");
                            }
                        }
                    }
                    pmsContext.SaveChanges();
                    CLSLogger.Info("推送订单给HH成功后，更新OrderPacks_ReservedHHSKU完成", logForFulfilled.ToString(), clogTags);

                    //推flux
                    var isPushToFluxSuccessful = PushToFlux(item, orderPackInfo, orderInfo, listOrderPackDetail, seqInfo, businessInfo, freightsInfo, listOrderPackOutOfStock, stockFulfilled.ListOrderPackDetailForHH, UserDefine6);

                    //推富勒不成功
                    if (!isPushToFluxSuccessful)
                    {
                        //throw new Exception("订单推送富勒失败");
                    }
                    else
                    {
                        CLSLogger.Info("推送订单给HH成功后，推送给富勒成功", $"包裹号：{item.OrderPackNo}。", clogTags);
                    }
                }
            }
            else
            {
                //状态改为OrderFailed
                item.Status = QueueStatusEnum.OrderFailed.ToString();
                item.ReturnMessageFromHH = result.message;
                item.UpdateTime = DateTime.UtcNow;
                pmsContext.SaveChanges();
                CLSLogger.Error("推送订单给HH失败", $"结果: {result.message}, 包裹号: {item.OrderPackNo}，其在Queue中修改状态为{item.Status}", clogTags);
            }
        }

        private void ProcessOrderPack(bool ignoreOutOfStock, IsStockFulfilledOutputDTO stockFulfilled, List<SeqEntity> listSeq,
            List<BusinessEntity> listBusiness, List<FreightsEntity> listFreights,
            OrderEntity orderInfo, OrderPackEntity orderPackInfo,
            OrderPackWMSQueueEntity item, List<OrderPackDetailEntity> listOrderPackDetail,
            List<OrderPackOutOfStockEntity> listOrderPackOutOfStock, List<OrderPackReservedHHSKUEntity> listReservedHHSKU,
            Dictionary<string, string> clogTags, HHCreateOrderResultDTO resultFromCachedProcessingInHH = null)
        {
            StringBuilder logForFulfilled = new StringBuilder();
            SeqEntity seqInfo = listSeq.FirstOrDefault(x => x.seq == orderInfo.seq);
            BusinessEntity businessInfo = listBusiness.FirstOrDefault(x => x.bizID == seqInfo.bizID);
            FreightsEntity freightsInfo = listFreights.FirstOrDefault(x => x.id.Equals(orderPackInfo.freightId));

            if (stockFulfilled.IsFulfilled || resultFromCachedProcessingInHH != null)
            {
                //如果没有需HH配货部分
                if (stockFulfilled.ListOrderPackDetailForHH.IsNullOrEmpty() && resultFromCachedProcessingInHH == null)
                {
                    //推flux
                    PushToFlux(item, orderPackInfo, orderInfo, listOrderPackDetail, seqInfo, businessInfo, freightsInfo, listOrderPackOutOfStock, stockFulfilled.ListOrderPackDetailForHH);
                }
                else
                {
                    HHCreateOrderResultDTO resultFromHH = new HHCreateOrderResultDTO();
                    if (resultFromCachedProcessingInHH != null)
                    {
                        resultFromHH = resultFromCachedProcessingInHH;
                    }
                    else
                    {
                        resultFromHH = AssembleInputAndPushToHH(item, orderInfo, stockFulfilled);
                    }

                    ProcessResultFromPushingToHH(resultFromHH, stockFulfilled, item, listReservedHHSKU, orderInfo, orderPackInfo, seqInfo, businessInfo, freightsInfo, listOrderPackDetail, listOrderPackOutOfStock, logForFulfilled, clogTags);
                }
            }
            //库存不满足
            else
            {
                //忽略缺货情况，不缺的找hh预定或使用富勒库存，缺的忽略，最后推富勒
                if (ignoreOutOfStock)
                {
                    //如果没有需HH配货部分
                    if (stockFulfilled.ListOrderPackDetailForHH.IsNullOrEmpty())
                    {
                        //推flux
                        PushToFlux(item, orderPackInfo, orderInfo, listOrderPackDetail, seqInfo, businessInfo, freightsInfo, listOrderPackOutOfStock, stockFulfilled.ListOrderPackDetailForHH);
                    }
                    else
                    {
                        HHCreateOrderResultDTO resultFromHH = new HHCreateOrderResultDTO();
                        if (resultFromCachedProcessingInHH != null)
                        {
                            resultFromHH = resultFromCachedProcessingInHH;
                        }
                        else
                        {
                            resultFromHH = AssembleInputAndPushToHH(item, orderInfo, stockFulfilled);
                        }

                        ProcessResultFromPushingToHH(resultFromHH, stockFulfilled, item, listReservedHHSKU, orderInfo, orderPackInfo, seqInfo, businessInfo, freightsInfo, listOrderPackDetail, listOrderPackOutOfStock, logForFulfilled, clogTags);
                    }
                }
                //修改包裹为OutOfStock状态，并记录缺货明细到表：OrderPacks_OutOfStock，如缺货明细已经存在有变化则做更新(定义状态位来标识)
                else
                {
                    StringBuilder logForNotFulfilled = new StringBuilder();
                    item.Status = QueueStatusEnum.OutOfStock.ToString();
                    item.UpdateTime = DateTime.UtcNow;

                    logForNotFulfilled.Append($"更新OrderPacks_WMSQueue，包裹号：{item.OrderPackNo}，状态修改为：{item.Status}。");


                    //get a list of orderpackdetail
                    var listOrderPackDetailInfo = listOrderPackDetail.Where(x => x.opid == orderPackInfo.opID).ToList();
                    var listOutOfStock = stockFulfilled.ListOutOfStockSKU;

                    foreach (var entry in listOrderPackDetailInfo)
                    {
                        //缺货表若有，listsOutOfStock若没有，说明已不缺货，删除缺货表内数据
                        //缺货表若有，listsOutOfStock若也有，说明还缺货，更新缺货表内数据
                        //缺货表若没有，listsOutOfStock若有，加入缺货表
                        //缺货表若没有，listsOutOfStock若也没有，不缺
                        var existed = listOrderPackOutOfStock.FirstOrDefault(x => x.OrderPackNo == item.OrderPackNo && x.SKU == entry.sku);
                        var currentOutOfStock = listOutOfStock.FirstOrDefault(x => x.sku == entry.sku);

                        if (existed != null)
                        {
                            if (currentOutOfStock != null)
                            {
                                existed.Status = QueueStatusEnum.OutOfStock.ToString();
                                existed.UpdateTime = DateTime.UtcNow;
                                logForNotFulfilled.Append($"更新OrderPakcs_OutOfStock，包裹号：{existed.OrderPackNo}，状态修改为：{existed.Status}。");
                            }
                            else
                            {
                                pmsContext.OrderPackOutOfStocks.Remove(existed);
                            }
                        }
                        else
                        {
                            if (currentOutOfStock != null)
                            {
                                OrderPackOutOfStockEntity orderPackOutOfStock = new OrderPackOutOfStockEntity
                                {
                                    OrderPackNo = item.OrderPackNo,
                                    SKU = entry.sku,
                                    LackQty = entry.qty,
                                    Status = QueueStatusEnum.OutOfStock.ToString(),
                                    CreationTime = DateTime.UtcNow,
                                    Oversale = entry.oversale
                                };
                                pmsContext.OrderPackOutOfStocks.Add(orderPackOutOfStock);
                                logForNotFulfilled.Append($"新增一个至OrderPakcs_OutOfStock，包裹号：{orderPackOutOfStock.OrderPackNo}，SKU：{orderPackOutOfStock.SKU}，缺少数量：{orderPackOutOfStock.LackQty}。");
                            }
                            else
                            {
                                //不缺
                            }
                        }
                    }
                    CLSLogger.Info("库存不满足，修改包裹为OutOfStock状态，并记录缺货明细到表：OrderPacks_OutOfStock", logForNotFulfilled.ToString(), clogTags);

                    pmsContext.SaveChanges();
                }
            }
        }

        private void FreeHHStockLock(string orderid)
        {
            ProductDomainService productDomainService = new ProductDomainService();

            productDomainService.FreeHHStockLockByOrderID(orderid);
        }

        /// <summary>
        /// Scan OrderPacks_WMSQueue and push related order pack to HH and Flux.
        /// </summary>
        /// <param name="pageSize"></param>
        [SkipConcurrentExecution(60 * 120)]
        [AutomaticRetry(Attempts = 0)]
        public void ScanQueue(int pageSize)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["jobid"] = Guid.NewGuid().ToString(),
                ["type"] = ClogTagType,
                ["method"] = "ScaneQueue"
            };





            try
            {
                //detect whether there is already a job running
                Cache = IocManager.Instance.Resolve<ICacheManager>();

                var cacheManager = Cache.GetCache(CacheKeys.JOBCACHEKEYNAME);
                var val = cacheManager.GetOrDefault(CacheKeys.PUSHORDERTOFLUXJOBKEY);
                CLSLogger.Info("PushOrderToFluxJobScanQueue开始运行", $"是否已有job正在运行：{val != null}", clogTags);
                if (val == null)
                {
                    cacheManager.Set(CacheKeys.PUSHORDERTOFLUXJOBKEY, true);
                }
                else
                {
                    return;
                }


                // continue:

                StringBuilder pageLog = new StringBuilder();

                try
                {
                    //查出包裹下的SKU明细

                    //get count
                    var totalCount = pmsContext.OrderPackWMSQueues.Count(x => ToBeProcessed.Contains(x.Status));
                    int totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;

                    pageLog.Append($"待处理总数：{totalCount}，分页总页数：{totalPage}。");

                    //get the skus without minium stock limitation
                    var stockConfig = pmsContext.AppConfigurationEntities.FirstOrDefault(x => x.ConfigKey == PMSConsts.FluxMiniumStockConfigKey);

                    if (stockConfig != null && !stockConfig.ConfigValue.IsNullOrWhiteSpace())
                    {
                        NoMiniumStockLimitSkus = stockConfig.ConfigValue.ConvertFromJsonString<List<string>>();
                        pageLog.Append($"没有富勒最低库存限制SKU：{string.Join(", ", NoMiniumStockLimitSkus)}。");
                    }

                    for (int i = 0; i < totalPage; i++)
                    {
                        pageLog.Append($"开始处理第{i}页。");

                        StockShareDomainService stockShareDomainService = new StockShareDomainService();
                        StockShareConfigVO configStockShare = stockShareDomainService.GetStockShareConfig();
                        var enabled = configStockShare.EnableStockShare;
                        var ignoreOutOfStock = configStockShare.IgnoreOutOfStock;
                        List<String> listTestSKU = configStockShare.TestSKU;

                        //get a list of orderpackno from queue where status is waiting and outofstock
                        List<OrderPackWMSQueueEntity> currentlistOrderPackWMSQueue = pmsContext.OrderPackWMSQueues.Where(x => ToBeProcessed.Contains(x.Status))
                            .OrderBy(x => x.CreationTime).Skip(i * pageSize).Take(pageSize).ToList();

                        List<string> listOrderPackno = currentlistOrderPackWMSQueue.Select(x => x.OrderPackNo).ToList();

                        //get a list of orderpackoutofstock
                        List<OrderPackOutOfStockEntity> listOrderPackOutOfStock = pmsContext.OrderPackOutOfStocks.Where(x => listOrderPackno.Contains(x.OrderPackNo)).ToList();

                        //get a list of reserved HH sku
                        List<OrderPackReservedHHSKUEntity> listReservedHHSKU = pmsContext.OrderPackReservedHHSKUs.Where(x => listOrderPackno.Contains(x.OrderPackNo)).ToList();

                        //get a list of opid to query orders_pack_detail
                        List<OrderPackEntity> listOrderPack = heERPContext.OrderPacks.Where(x => listOrderPackno.Contains(x.orderPackNo)).ToList();
                        List<int> listOpId = listOrderPack.Select(x => x.opID).ToList();

                        //get a list of oid
                        var listOId = listOrderPack.Select(x => x.oID).ToList();

                        //get a list of order
                        List<OrderEntity> listOrder = heERPContext.Orders.Where(x => listOId.Contains(x.oid)).ToList();

                        //get a list of sku to query flux
                        List<OrderPackDetailEntity> listOrderPackDetail = heERPContext.OrderPackDetails.Where(x => listOpId.Contains(x.opid.Value)).ToList();
                        var listSKU = listOrderPackDetail.Select(x => x.sku).Distinct().ToList();

                        //get a list of product
                        List<ProductEntity> listProduct = pmsContext.Products.Where(x => listSKU.Contains(x.SKU)).ToList();

                        //get fluxinvlot
                        //var listFluxInvLot = fluxWMSContext.FluxInvLots.Where(x => listSKU.Contains(x.SKU)).ToList();

                        //get Flux INV_LOT_LOC_ID
                        List<FluxInvLotLocIdEntity> listFluxInvLotLocId = fluxWMSContext.FluxInvLotLocIds.Where(x => listSKU.Contains(x.SKU)).ToList();

                        //get api_stocks
                        List<APIStockEntity> listApiStock = heERPContext.APIStocks.Where(x => listSKU.Contains(x.SKU)).ToList();

                        //get seq
                        var listSeqNumber = listOrder.Select(x => x.seq).ToList();
                        List<SeqEntity> listSeq = heERPContext.Seqs.Where(x => listSeqNumber.Contains(x.seq)).ToList();

                        //get business
                        var listBizId = listSeq.Select(x => x.bizID).ToList();
                        List<BusinessEntity> listBusiness = heERPContext.Businesses.Where(x => listBizId.Contains(x.bizID)).ToList();

                        //get freights
                        var listFreightId = listOrderPack.Select(x => x.freightId).ToList();
                        List<FreightsEntity> listFreights = heERPContext.Freights.Where(x => listFreightId.Contains(x.id)).ToList();

                        foreach (OrderPackWMSQueueEntity item in currentlistOrderPackWMSQueue)
                        {
                            clogTags["orderpackno"] = item.OrderPackNo;

                            OrderPackEntity orderPackInfo = listOrderPack.FirstOrDefault(x => x.orderPackNo == item.OrderPackNo);
                            if (orderPackInfo == null)
                            {
                                CLSLogger.Info("包裹号在orderpack表不存在，请检查", "请检查包裹是否有变更记录", clogTags);
                                continue;
                            }

                            if (IsOrderPackInQueueNeedSkip(orderPackInfo, item, listOrderPackOutOfStock, Cache, clogTags))
                            {
                                continue;
                            }

                            var oid = orderPackInfo.oID;

                            OrderEntity orderInfo = listOrder.FirstOrDefault(x => x.oid == oid);
                            if (orderInfo == null)
                            {
                                continue;
                            }

                            if (item.Status == QueueStatusEnum.Created.ToString())
                            {
                                FreeHHStockLock(orderInfo.orderid);
                            }

                            var partner = orderInfo.partner;

                            //every sku in orderpack is in stock
                            //库存满足，先推送给HH（只推送HH需配货的部分），成功后再推送富勒（全部SKU）。修改包裹为Done状态，删除OrderPacks_OutOfStock对应包裹的缺货明细

                            //判断config开关，开：检查库存， 不开：直接推富勒
                            if (!enabled)
                            {
                                var seqInfo = listSeq.FirstOrDefault(x => x.seq == orderInfo.seq);
                                var businessInfo = listBusiness.FirstOrDefault(x => x.bizID == seqInfo.bizID);
                                var freightsInfo = listFreights.FirstOrDefault(x => x.id.Equals(orderPackInfo.freightId));

                                FluxDomainService fluxDomainService = new FluxDomainService();
                                FluxResponse fluxResponse = fluxDomainService.PushOrderToFlux(orderPackInfo, orderInfo, listOrderPackDetail, seqInfo, businessInfo, freightsInfo, null);

                                //推失败
                                if (fluxResponse == null || int.Parse(fluxResponse.Response.@return.returnFlag) != 1)
                                {
                                    var reason = fluxResponse.Response.@return.returnDesc;

                                    clogTags["method"] = "PutSOData";
                                    CLSLogger.Error("订单推富勒发生异常", reason, clogTags);
                                }
                                //成功
                                else
                                {
                                    var reason = fluxResponse.Response.@return.returnDesc;
                                    clogTags["method"] = "PutSOData";
                                    CLSLogger.Info("订单推富勒成功", reason, clogTags);
                                }
                            }
                            else
                            {

                                IsStockFulfilledOutputDTO stockFulfilled = IsStockFulfilled(orderPackInfo, listOrderPackDetail, listProduct, listFluxInvLotLocId, listApiStock, partner, ignoreOutOfStock, item.Status);
                                ProcessOrderPack(ignoreOutOfStock, stockFulfilled, listSeq, listBusiness, listFreights, orderInfo, orderPackInfo, item, listOrderPackDetail, listOrderPackOutOfStock, listReservedHHSKU, clogTags);
                            }
                        }
                        pageLog.Append($"第{i}页处理完成。");
                    }
                }
                catch (Exception ex)
                {
                    clogTags["method"] = "ScanQueue";
                    cacheManager.Remove(CacheKeys.PUSHORDERTOFLUXJOBKEY);
                    string error = ex.InnerException == null ? ex.Message : ex.InnerException.ToString();
                    CLSLogger.Error("PushOrderToFluxJob.ScanQueue失败", ex, clogTags);
                }
                finally
                {
                    clogTags["method"] = "ScanQueue";
                    cacheManager.Remove(CacheKeys.PUSHORDERTOFLUXJOBKEY);
                    CLSLogger.Info("PushOrderToFluxJob.ScanQueue结束运行", pageLog.ToString(), clogTags);
                }


            }
            catch (Exception)
            {

                return;
            }

           

           
        }

        public void ProcessCreateOrderAsyncResultFromHH(HHCreateOrderResultDTO input)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["jobid"] = Guid.NewGuid().ToString(),
                ["type"] = ClogTagType,
                ["method"] = "ProcessCreateOrderAsyncResultFromHH",
                ["orderpackno"] = input.OrderPackNo
            };

            Cache = IocManager.Instance.Resolve<ICacheManager>();

            StringBuilder methodLog = new StringBuilder();

            try
            {
                //查出包裹下的SKU明细

                //get api stock early from input
                List<APIStockEntity> listApiStock = new List<APIStockEntity>();
                var listSKUFromInput = input.data.Select(x => x.sku).ToList();
                var listAPIStockFromInputSKU = heERPContext.APIStocks.Where(x => listSKUFromInput.Contains(x.SKU)).ToList();
                listApiStock.AddRange(listAPIStockFromInputSKU);


                //get the skus without minium stock limitation
                var stockConfig = pmsContext.AppConfigurationEntities.FirstOrDefault(x => x.ConfigKey == PMSConsts.FluxMiniumStockConfigKey);

                if (stockConfig != null && !stockConfig.ConfigValue.IsNullOrWhiteSpace())
                {
                    NoMiniumStockLimitSkus = stockConfig.ConfigValue.ConvertFromJsonString<List<string>>();
                    methodLog.Append($"没有富勒最低库存限制SKU：{string.Join(", ", NoMiniumStockLimitSkus)}。");
                }

                //get stockshare config
                StockShareDomainService stockShareDomainService = new StockShareDomainService();
                StockShareConfigVO configStockShare = stockShareDomainService.GetStockShareConfig();
                var enabled = configStockShare.EnableStockShare;
                var ignoreOutOfStock = configStockShare.IgnoreOutOfStock;
                List<String> listTestSKU = configStockShare.TestSKU;

                var orderPackInQueue = pmsContext.OrderPackWMSQueues.FirstOrDefault(x => x.OrderPackNo == input.OrderPackNo);

                if (orderPackInQueue == null)
                {
                    throw new Exception("失败，包裹在Queue中不存在，请检查。");
                }
                else if (input.message.Equals("此订单已经存在") && orderPackInQueue.Status == QueueStatusEnum.Refund.ToString())
                {
                    string resultFromOrderCancelledInHH = CancelERPModifiedOrderInHH(orderPackInQueue, clogTags);

                    CLSLogger.Info("处理HH订单回传终止，订单在queue中已Refund，尝试在HH取消订单", $"HH取消订单结果：{resultFromOrderCancelledInHH}", clogTags);

                    if (resultFromOrderCancelledInHH.Equals("失败"))
                    {
                        throw new Exception("处理HH订单回传失败，订单在queue中已Refund，尝试在HH取消订单失败，请检查。");
                    }

                    return;
                }
                else if (orderPackInQueue.Status != QueueStatusEnum.CachedInHH.ToString())
                {
                    CLSLogger.Info("处理HH订单回传终止，包裹在Queue中状态不为CachedInHH", $"包裹状态为：{orderPackInQueue.Status}", clogTags);
                    return;
                }

                //get a list of opid to query orders_pack_detail
                List<OrderPackEntity> listOrderPack = heERPContext.OrderPacks.Where(x => input.OrderPackNo == x.orderPackNo).ToList();
                List<int> listOpId = listOrderPack.Select(x => x.opID).ToList();

                //get a list of sku to query flux
                List<OrderPackDetailEntity> listOrderPackDetail = heERPContext.OrderPackDetails.Where(x => listOpId.Contains(x.opid.Value)).ToList();
                var listSKU = listOrderPackDetail.Select(x => x.sku).Distinct().ToList();

                //get api_stocks
                List<APIStockEntity> listApiStockFromOrderPackDeatilSKU = heERPContext.APIStocks.Where(x => (listSKU.Contains(x.SKU) && !listSKUFromInput.Contains(x.SKU))).ToList();
                listApiStock.AddRange(listApiStockFromOrderPackDeatilSKU);

                //get a list of oid
                var listOId = listOrderPack.Select(x => x.oID).ToList();

                //get a list of order
                List<OrderEntity> listOrder = heERPContext.Orders.Where(x => listOId.Contains(x.oid)).ToList();

                //get a list of product
                List<ProductEntity> listProduct = pmsContext.Products.Where(x => listSKU.Contains(x.SKU)).ToList();

                //get Flux INV_LOT_LOC_ID
                List<FluxInvLotLocIdEntity> listFluxInvLotLocId = fluxWMSContext.FluxInvLotLocIds.Where(x => listSKU.Contains(x.SKU)).ToList();

                //get a list of orderpackoutofstock
                List<OrderPackOutOfStockEntity> listOrderPackOutOfStock = pmsContext.OrderPackOutOfStocks.Where(x => input.OrderPackNo == x.OrderPackNo).ToList();

                //get a list of reserved HH sku
                List<OrderPackReservedHHSKUEntity> listReservedHHSKU = pmsContext.OrderPackReservedHHSKUs.Where(x => input.OrderPackNo == x.OrderPackNo).ToList();

                //get seq
                var listSeqNumber = listOrder.Select(x => x.seq).ToList();
                List<SeqEntity> listSeq = heERPContext.Seqs.Where(x => listSeqNumber.Contains(x.seq)).ToList();

                //get business
                var listBizId = listSeq.Select(x => x.bizID).ToList();
                List<BusinessEntity> listBusiness = heERPContext.Businesses.Where(x => listBizId.Contains(x.bizID)).ToList();

                //get freights
                var listFreightId = listOrderPack.Select(x => x.freightId).ToList();
                List<FreightsEntity> listFreights = heERPContext.Freights.Where(x => listFreightId.Contains(x.id)).ToList();


                OrderPackEntity orderPackInfo = listOrderPack.FirstOrDefault(x => x.orderPackNo == orderPackInQueue.OrderPackNo);
                if (orderPackInfo == null)
                {
                    CLSLogger.Info("包裹号在orderpack表不存在，请检查", "请检查包裹是否有变更记录", clogTags);
                    return;
                }

                if (IsOrderPackInQueueNeedSkip(orderPackInfo, orderPackInQueue, listOrderPackOutOfStock, Cache, clogTags))
                {
                    return;
                }

                var oid = orderPackInfo.oID;

                OrderEntity orderInfo = listOrder.FirstOrDefault(x => x.oid == oid);
                if (orderInfo == null)
                {
                    return;
                }

                var partner = orderInfo.partner;

                if (!enabled)
                {
                    var seqInfo = listSeq.FirstOrDefault(x => x.seq == orderInfo.seq);
                    var businessInfo = listBusiness.FirstOrDefault(x => x.bizID == seqInfo.bizID);
                    var freightsInfo = listFreights.FirstOrDefault(x => x.id.Equals(orderPackInfo.freightId));

                    FluxDomainService fluxDomainService = new FluxDomainService();
                    FluxResponse fluxResponse = fluxDomainService.PushOrderToFlux(orderPackInfo, orderInfo, listOrderPackDetail, seqInfo, businessInfo, freightsInfo, null);

                    //推失败
                    if (fluxResponse == null || int.Parse(fluxResponse.Response.@return.returnFlag) != 1)
                    {
                        var reason = fluxResponse.Response.@return.returnDesc;

                        clogTags["method"] = "PutSOData";
                        CLSLogger.Error("订单推富勒发生异常", reason, clogTags);
                    }
                    //成功
                    else
                    {
                        var reason = fluxResponse.Response.@return.returnDesc;
                        clogTags["method"] = "PutSOData";
                        CLSLogger.Info("订单推富勒成功", reason, clogTags);
                    }
                }
                else
                {
                    IsStockFulfilledOutputDTO stockFulfilled = IsStockFulfilled(orderPackInfo, listOrderPackDetail, listProduct, listFluxInvLotLocId, listApiStock, partner, ignoreOutOfStock, String.Empty, input);
                    ProcessOrderPack(ignoreOutOfStock, stockFulfilled, listSeq, listBusiness, listFreights, orderInfo, orderPackInfo, orderPackInQueue, listOrderPackDetail, listOrderPackOutOfStock, listReservedHHSKU, clogTags, input);
                }

            }
            catch (Exception ex)
            {
                CLSLogger.Error("ProcessCreateOrderAsyncResultFromHH失败", ex, clogTags);
                throw ex;
            }
            finally
            {
                CLSLogger.Info("ProcessCreateOrderAsyncResultFromHH结束运行", methodLog.ToString(), clogTags);
            }
        }


      
    }
}
