using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using MagicLamp.PMS.BaXi;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy;
using System.Linq;
using MagicLamp.PMS.YiWuBondedWarehouse;
using Abp.Runtime.Caching;
using System;
using System.Collections.Generic;
using TimeZoneConverter;
using MagicLamp.PMS.Infrastructure.Extensions;
using Abp.Extensions;
using MagicLamp.PMS.EntityFrameworkCore;

namespace MagicLamp.XERP.BackgroundJob
{
    public class SyncYiWuOrderStatus
    {
        private HEERPDbContext _heerpContext;
        private ICacheManager _cacheManager;
        private SendNotificationToDingDingJob _dingdingNotificationJob;



        Dictionary<string, string> LogTags = new Dictionary<string, string>
        {
            ["jobid"] = Guid.NewGuid().ToString(),
            ["method"] = "SyncYiWuOrderStatus"
        };
        public SyncYiWuOrderStatus(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<OrderEntity, int> orderRepository,
            IRepository<OrderPackEntity, int> orderPackRepository,

            ICacheManager cacheManager
        )
        {
            _heerpContext = new HEERPDbContextFactory().CreateDbContext();
            _cacheManager = cacheManager;
            _dingdingNotificationJob = new SendNotificationToDingDingJob();
        }

        public async Task Execute()
        {
            var client = new YiWuBondedWarehouseClient(_cacheManager, YiWuAccountEnum.Default);


            var yiwuExportFlags = new List<int> { 79, 84, 85, 88 };
            var useOrderPackNoFlags = new List<int> { 84, 85 };
            var storename = new List<string> { "qdbs-store.mofa.ht", "kuaidi.nz", "naifen.kuaidi.nz" }; // 二刷订单需要通过订单号取包裹状态; , "fws.mofa.ht" ??这个不知道为啥要包裹号取订单状态

            var orderPacks = _heerpContext.OrderPacks.Where(x => yiwuExportFlags.Contains(x.exportFlag.GetValueOrDefault()) && x.status == 3 && x.freezeFlag ==0 ).ToList(); //!= 4 && x.freezeFlag == 0
            if (orderPacks.Count() == 0)
            {
                return;
            }

            foreach (var orderPack in orderPacks)
            {
                var orders = _heerpContext.Orders.Where(x => storename.Contains(x.store.Trim().ToString()) && x.oid == orderPack.oID).ToList();

                try
                {
                    TradeOrderQueryResponse queryResponse = await client.TradeOrderQuery(new TradeOrderQueryData
                    {
                        //Second order use orerid else use orderPackNo
                        orderCode = orders.Count > 0 ? orderPack.orderID : orderPack.orderPackNo
                    });

                    if (queryResponse == null)
                    {
                        continue;
                    }

                    var order = queryResponse.ResultData.Orders.FirstOrDefault();

                    if (order == null)
                    {
                        continue;
                        // _dingdingNotificationJob.SendBondedWarehouseYiWuAlert("义乌保税仓订单信息同步失败", $"义乌保税仓订单信息同步失败, 失败原因: 无法找到对应的订单号: {orderPack.orderID}");
                        // throw new Exception($"Unable to find order using order id: {orderPack.orderID}");
                    }


                    if (new List<string> { "-99" }.Contains(order.StateCode.ToString()))
                    {
                        orderPack.status = 0;
                        orderPack.pack_time = (DateTime?)null;
                    }


                    // 900 已发货
                    // 1000 客户确认收货
                    // 9999 完成
                    if (new List<string> { "900", "1000", "9999" }.Contains(order.StateCode.ToString()))
                    {
                        orderPack.status = 4;
                        orderPack.out_time = DateTime.UtcNow.ConvertUTCTimeToNZTime();
                    }

                    // only fill carrierId and freight id if carrierId is null/empty
                    // record bondedwarehouse carrier id and carrier company into table
                    if (!order.ShipNo.IsNullOrWhiteSpace())
                    {
                        orderPack.bonded_warehouse_carrier_id = order.ShipNo;
                        orderPack.bonded_warehouse_carrier_company = order.ShipName;
                        if (orderPack.carrierId.IsNullOrWhiteSpace())
                        {
                            orderPack.carrierId = order.ShipNo;

                            if (order.ShipName == "申通快递")
                            {
                                orderPack.freightId = "ShengTong";
                            }
                            else if (order.ShipName.Contains("邮政")) //order.ShipName == "邮政速递" || order.ShipName == "邮政快递"
                            {
                                orderPack.freightId = "ems";
                            }
                            else if (order.ShipName == "百世汇通")
                            {
                                orderPack.freightId = "huitong";
                            }
                            else if (order.ShipName == "顺丰快递")
                            {
                                orderPack.freightId = "sfexpress";
                            }
                            else if (order.ShipName.Contains("韵达快递"))
                            {
                                orderPack.freightId = "yunda";
                            }
                            else
                            {
                                _dingdingNotificationJob.SendBondedWarehouseYiWuAlert("义乌Unable to find matching freighId", $"Exception: " + order.ShipName + "订单号:"  + orderPack.orderID );
                                // throw new Exception($"Unable to find order using order id: {orderPack.orderID}");
                                CLSLogger.Info("义乌Unable to find matching freighId for", "Exception:" + order.ShipName, LogTags);
                                throw new Exception($"Unable to find matching freighId for: {order.ShipName}");
                            }
                        }
                    }

                    _heerpContext.OrderPacks.Update(orderPack);
                    _heerpContext.SaveChanges();
                    // wait for 2 seconds due to yiwu api throttling
                    await Task.Delay(2000);

                }
                catch (Exception ex)
                {
                    CLSLogger.Info("义乌TradeOrderQueryResponse:", "Exception:" + ex.Message, LogTags);
                    //throw new Exception($"义乌TradeOrderQueryResponse: {orders.FirstOrDefault().orderid}");
                    continue;
                }


            }


            //using (var unitOfWork = _unitOfWorkManager.Begin())
            //{

            //    unitOfWork.Complete();
            //}
        }



    }
}