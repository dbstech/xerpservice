﻿using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.MqDTO;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.DTOs.ThirdPart;
using Abp.Domain.Uow;

namespace MagicLamp.XERP.BackgroundJob
{
    public class OrderPackOutboundNotifyHHJob : CommonBackgroundJob<List<OutboundNotifyHHInputDTO>>
    {

        [UnitOfWork(IsDisabled = false)]
        public override void Process(List<OutboundNotifyHHInputDTO> orderPacks)
        {
            PMSDbContext pMSDbContext = new PMSDbContextFactory().CreateDbContext();
            HEERPDbContext hEERPDbContext = new HEERPDbContextFactory().CreateDbContext();

            var orderPackNos = orderPacks.Select(x => x.OrderNo).ToList();
            LogTags["orderpackno"] = string.Join(";", orderPackNos);

            string outBoundStatus = OrderPackReservedStatusEnum.Outbound.ToString();
            var orderPackInfos = pMSDbContext.OrderPackReservedHHSKUs.Where(x => orderPackNos.Contains(x.OrderPackNo)
            && x.Status != outBoundStatus).ToList();

            if (orderPackInfos.IsNullOrEmpty())
            {
                CLSLogger.Info("Can not found any order pack need to be synced to HH", string.Empty, LogTags);
                return;
            }

            var erpOrderPackInfos = hEERPDbContext.OrderPacks.Where(x => orderPackNos.Contains(x.orderPackNo)).Select(x => new
            {
                x.orderPackNo,
                x.out_time
            }).ToList();

            StringBuilder error = new StringBuilder();

            foreach (var item in orderPackInfos)
            {
                var currentOutboundOrderPack = orderPacks.FirstOrDefault(x => x.OrderNo == item.OrderPackNo);
                var currentErpOrderPack = erpOrderPackInfos.FirstOrDefault(x => x.orderPackNo == item.OrderPackNo);

                if (currentErpOrderPack == null)
                {
                    error.Append($"can not find order pack：{item.OrderPackNo} in table: orderpacks;");
                    continue;
                }

                DateTime outboundTime = currentErpOrderPack.out_time.HasValue ? currentErpOrderPack.out_time.Value : currentOutboundOrderPack.OutBoundTime;

                HHSyncOutboundDateInputDTO hHSyncOutboundDateInput = new HHSyncOutboundDateInputDTO
                {
                    delivery_date = outboundTime.ToUniversalTime().SpecifyKindInUTC().ToString("yyyy-MM-dd HH:mm:ss"),
                    order_no = item.OrderPackNo,
                    package_weight = currentOutboundOrderPack.Weight,
                    express_company = currentOutboundOrderPack.CarrierName,
                    express_no = currentOutboundOrderPack.CarrierId
                };
                var result = HHProxy.SyncOutboundDateToHH(hHSyncOutboundDateInput);

                if (result != null && result.result == 0)
                {
                    item.Status = outBoundStatus;
                    item.OutboundTime = currentOutboundOrderPack.OutBoundTime;
                }
                else
                {
                    error.Append($"order pack: {item.OrderPackNo} sync outbound info to HH failed, the response from HH: {result.ToJsonString()} ;");
                }
            }

            pMSDbContext.SaveChanges();

            if (error.Length > 0)
            {
                CLSLogger.Error($"Some order packs have been synced to HH failed", error.ToString(), LogTags);
                throw new Exception("Some order packs sync to HH failed");
            }
        }
    }
}
