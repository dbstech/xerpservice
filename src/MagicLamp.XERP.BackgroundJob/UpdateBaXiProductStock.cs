using System;
using System.Collections.Generic;
using MagicLamp.PMS.EntityFrameworkCore;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using MagicLamp.PMS.BaXi;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.ThirdPart;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Infrastructure.Extensions;
using Abp.AutoMapper;
using Abp.UI;
using Abp.Dependency;
using Abp.BackgroundJobs;
using MagicLamp.XERP.BackgroundJob;
using MagicLamp.PMS.DTOs.Enums;
using Microsoft.AspNetCore.Mvc;
using MagicLamp.PMS.Domain;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.Enums;
using Microsoft.EntityFrameworkCore;




namespace MagicLamp.XERP.BackgroundJob
{
    public class UpdateBaXiProductStock
    {
        private PMSDbContext _pmsContext;
        private HEERPDbContext _heerpContext;

        //private IRepository<ProductEntity, string> _productRepository;
        //private IRepository<APIStockEntity, string> _apiStockRepository;
        //private IRepository<WareHouseEntity> _warehouseRepository;
        //private IRepository<BaXiProductInfo> _baXiProductInfoRepository;
        private IUnitOfWorkManager _unitOfWorkManager;
        public Dictionary<string, string> LogTags = new Dictionary<string, string>();
        public UpdateBaXiProductStock(
            //IRepository<ProductEntity, string> productRepository,
            //IRepository<APIStockEntity, string> apiStockRepository,
            //IRepository<WareHouseEntity> warehouseRepository,
            //IRepository<BaXiProductInfo> baXiProductInfoRepository,
            IUnitOfWorkManager unitOfWorkManager
        )
        {
            _pmsContext = new PMSDbContextFactory().CreateDbContext();
            _heerpContext = new HEERPDbContextFactory().CreateDbContext();

            // _productRepository = productRepository;
            _unitOfWorkManager = unitOfWorkManager;
            //_apiStockRepository = apiStockRepository;
            //_baXiProductInfoRepository = baXiProductInfoRepository;
            //_warehouseRepository = warehouseRepository;
        }



        public async Task Execute()
        {
            StringBuilder priceUpdateMsg = new StringBuilder();
            int PageSize = 100;
            List<SyncAladdinStockDTO> listStockForSync = new List<SyncAladdinStockDTO>();

            int warehouseId = _pmsContext.WareHouses.FirstOrDefault(x => x.Location == "八禧仓").Id;
            List<ProductEntity> products = _pmsContext.Products.Where(x => x.WarehouseID == warehouseId && x.ActiveFlag == true).ToList();
            if (products.Count() == 0)
            {
                return;
            }

            List<string> skus = products.Select(x => x.SKU).ToList();
            List<string> upcs = products.Select(x => x.Barcode1).Distinct().ToList();

            BaXiClient client = new BaXiClient();


            for (int i = 0; i < upcs.Count; i += PageSize)
            {
                var batchUpcs = upcs.Skip(i).Take(PageSize).ToList();


                //// data process 
                //List<BaXiProduct> baXiProducts = await client.ProductsQuery(batchUpcs);
                ////下线的八喜产品
                //List<BaXiProduct> baXiOffProducts = await client.ProductsOffQuery(batchUpcs);


                var result = await client.BAXIProductsQuery(upcs);
                // Access the off and true products
                List<BaXiProduct> baXiOffProducts = result.offProducts;
                List<BaXiProduct> baXiProducts = result.trueProducts;

                List<BaXiProductInfo> baXiProductInfos = _pmsContext.BaXiProductInfos.ToList();
                _heerpContext.Database.SetCommandTimeout(120);
                List<APIStockEntity> apiStocks = _heerpContext.APIStocks.Where(x => skus.Contains(x.SKU) && x.Status.Value == 1).ToList();


                CLSLogger.Info("baXiProducts", "baXiProducts:" + baXiProducts.ToJsonString(), LogTags);
                CLSLogger.Info("baXiOffProducts", "baXiOffProducts:" + baXiOffProducts.ToJsonString(), LogTags);

                foreach (var _baxiproducts in baXiProducts)
                {
                    string Upc = _baxiproducts.Upc;
                    int stock = (int)_baxiproducts.Stock;
                    string expired_date = _baxiproducts.Deadline?.ToString("yyyy/MM/dd");
                    decimal newPrice = Convert.ToDecimal(_baxiproducts.Price);

                    List<ProductEntity> productsToUpdate = products.Where(x => x.Barcode1 == Upc).ToList();

                    foreach (var product in productsToUpdate)
                    {
                        APIStockEntity apiStock = apiStocks.FirstOrDefault(x => x.SKU == product.SKU);

                        if (apiStock.Qty != stock)
                        {
                            SyncAladdinStockDTO syncdata = new SyncAladdinStockDTO();
                            syncdata.best_before = _baxiproducts.Deadline?.ToString("yyyy/MM/dd");
                            syncdata.sku = product.SKU;
                            syncdata.stock = stock;
                            listStockForSync.Add(syncdata);

                            // 更新库存
                            //APIStockEntity apistock = apiStocks.FirstOrDefault(x => x.SKU == product.SKU);
                            apiStock.Qty = stock;
                            apiStock.Qty_available = stock;
                            apiStock.Qty_aladdin = stock;
                            apiStock.Expired_date = expired_date;
                            apiStock.Updated_time = DateTime.Now;
                            _heerpContext.APIStocks.Update(apiStock);
                            _heerpContext.SaveChanges();
                        }


                        ProductEntity productinfo = product;
                        productinfo.Cost = newPrice;
                        _pmsContext.Products.Update(productinfo);
                        _pmsContext.SaveChanges();
                    }


                    //更新成本价格
                    BaXiProductInfo baXiProductInfo = baXiProductInfos.FirstOrDefault(x => x.Upc == Upc);
                    if (baXiProductInfo == null)
                    {
                        BaXiProductInfo baxiinfo = new BaXiProductInfo();
                        baxiinfo.Upc = Upc;
                        baxiinfo.Price = newPrice;
                        baxiinfo.Stock = stock;
                        _pmsContext.BaXiProductInfos.Add(baxiinfo);
                        _pmsContext.SaveChanges();

                    }
                    else
                    {
                        if (newPrice != baXiProductInfo.Price)
                        {
                            productsToUpdate.ForEach(product =>
                            {
                                priceUpdateMsg.Append($"{product.SKU} {Upc}\n\n{product.ChineseName}\n\n${baXiProductInfo.Price}->${newPrice}\n\n");
                            });
                            baXiProductInfo.PreviousPrice = baXiProductInfo.Price;
                            baXiProductInfo.Price = newPrice;
                            baXiProductInfo.LastModified = DateTime.UtcNow;
                            _pmsContext.BaXiProductInfos.Update(baXiProductInfo);
                            _pmsContext.SaveChanges();
                        }
                    }





                }

                foreach (var _baxiproducts in baXiOffProducts)
                {
                    string Upc = _baxiproducts.Upc;
                    int stock = 0;
                    List<ProductEntity> productsToUpdate = products.Where(x => x.Barcode1 == Upc).ToList();

                    foreach (var product in productsToUpdate)
                    {
                        APIStockEntity apiStock = apiStocks.FirstOrDefault(x => x.SKU == product.SKU);

                        if (apiStock.Qty != stock)
                        {
                            SyncAladdinStockDTO syncdata = new SyncAladdinStockDTO();
                            syncdata.sku = product.SKU;
                            syncdata.stock = stock;
                            listStockForSync.Add(syncdata);

                            // 更新库存0 已下线；
                            //APIStockEntity apistock = apiStocks.FirstOrDefault(x => x.SKU == product.SKU);
                            apiStock.Qty = stock;
                            apiStock.Qty_available = stock;
                            apiStock.Qty_aladdin = stock;
                            apiStock.Updated_time = DateTime.Now;
                            apiStock.ProductNameCN = apiStock.ProductNameCN + " Baxi Flag =Off(产品已下线)";
                            _heerpContext.APIStocks.Update(apiStock);
                            _heerpContext.SaveChanges();

                            ProductEntity productinfo = product;
                            productinfo.ActiveFlag = false;
                            productinfo.ChineseName = product.ChineseName + "[八喜仓已下线]";
                            _pmsContext.Products.Update(productinfo);
                            _pmsContext.SaveChanges();

                        }
                    }
                }

            }


            if (priceUpdateMsg.Length > 0)
            {
                priceUpdateMsg.Insert(0, "##### 以下八禧仓产品价格发生更改： \n\n");

                List<string> staffToNotify = new List<string> { "+86-18766212252" };

                string ddTokenBaXi = "34adddda7c173e972431bbd7a0f0f3ffbebb8746dbfdc66c4191f2e168e098ef";
                string signature = "SECe480bad4be94d0ce5b516f826141d46a6130a54be731558507d68a583f675fb0";
                DDNotificationProxy.MarkdownNotification($"八禧仓监控报警", priceUpdateMsg.ToString(), ddTokenBaXi, signature, staffToNotify, 100000);
            }

            if (listStockForSync.Count > 0)
            {
                LogTags["method"] = "IncrementBAXISKU_Stock";

                //同步给188
                EShopProxy eShop = new EShopProxy();
                eShop.SyncStockTo188(listStockForSync);

                //int currentRecord = i + 1;
                //int totalRecords = upcs.Count;
                //double progress = (double)currentRecord / totalRecords * 100;
                //CLSLogger.Info("增量库存_同步188完成", $"处理进度：{progress:F2}% ({currentRecord}/{totalRecords})", LogTags);
            }


        }
    }
}