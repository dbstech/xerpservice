﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Extensions;
using MagicLamp.PMS;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace MagicLamp.XERP.BackgroundJob
{
    public class UpdateStockForUGGJob
    {
        [SkipConcurrentExecution(60 * 120)]
        public async Task UpdateStock()
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>();
            clogTags["jobid"] = Guid.NewGuid().ToString();
            clogTags["type"] = "UpdateStockForUGGJob";
            int count = 0;

            List<string> productsNeedSkipping = new List<string>();

            try
            {
                PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();
                List<string> uggBrands = new List<string> { "UGG", "Everugg", "Tarramarra", "AS UGG", "Ever Australia" };
                List<int> wharehousids = new List<int> { 5, 7 };

                var stockConfig = pmsContext.AppConfigurationEntities.FirstOrDefault(x => x.ConfigKey == PMSConsts.UGGProductsSkipUpdateStockKey);

                if (stockConfig != null && !stockConfig.ConfigValue.IsNullOrWhiteSpace())
                {
                    productsNeedSkipping = stockConfig.ConfigValue.ConvertFromJsonString<List<string>>();
                    CLSLogger.Info("UpdateStockForUGGJob存在需要忽略的sku", productsNeedSkipping.ToJsonString(), clogTags);
                }

                List<ProductEntity> products = pmsContext.Products.Include(x => x.Brand)
                    .Where(x => uggBrands.Contains(x.Brand.EnglishName)
                    && !productsNeedSkipping.Contains(x.SKU, StringComparer.OrdinalIgnoreCase)
                    && wharehousids.Contains(x.WarehouseID ?? 0)
                    && (x.ActiveFlag == true)
                    )
                    .ToList();

                if (products.Count == 0)
                {
                    CLSLogger.Error("UpdateStockForUGGJob失败", "更新UGG库存失败,未找到UGG相关产品", clogTags);
                    return;
                }

                //get a list of sku
                List<string> listSkus = products.Select(x => x.SKU).ToList();

                //warehouse: 5 -> 澳洲仓, 7 -> 现货仓
                int warehouseAu = 5;
                int warehouseCn = 7;
                //调全部澳洲库，全部中国库
                //澳洲
                List<UGGStockResultDTO> auStocks = UGGProxy.GetAllAuStock();
                CLSLogger.Info("GetAllAuStock", auStocks.ToJsonString(), clogTags);
                if (auStocks.IsNullOrEmpty())
                {
                    CLSLogger.Info("UpdateStockForUGGJob部分失败", "UGGProxy GetAllAuStock无结果，查询Ever Australia的AU UGG库存失败", clogTags);
                }
                else
                {
                    count += await UpdateDBAsync(listSkus, products, auStocks, warehouseAu);
                }

                //中国 镇江仓  我们现在都在镇江仓 Nina 2024.10.29 21:27PM
                List<UGGStockResultDTO> cnStocks = UGGProxy.GetAllCnStock();
                CLSLogger.Info("GetAllZGZJStock", cnStocks.ToJsonString(), clogTags);

                if (cnStocks.IsNullOrEmpty())
                {
                    CLSLogger.Info("UpdateStockForUGGJob部分失败", "UGGProxy GetAllCnStock无结果，查询Ever Australia的CN UGG库存失败", clogTags);
                }
                else
                {
                    count += await UpdateDBAsync(listSkus, products, cnStocks, warehouseCn);
                }

                IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
                backgroundJobManager.Enqueue<SyncAladdinStockJob, List<string>>(listSkus, BackgroundJobPriority.Low);
            }
            catch (Exception ex)
            {
                CLSLogger.Error("UpdateStockForUGGJob失败", ex, clogTags);
            }
            finally
            {
                CLSLogger.Info("UpdateStockForUGGJob完成", $"Job finished. {count} UGG product(s) updated.", clogTags);
            }
        }

        private async Task<int> UpdateDBAsync(List<string> listSkus, List<ProductEntity> products, List<UGGStockResultDTO> stocks, int warehouseId)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>();
            clogTags["type"] = "UpdateStockForUGGJob";

            int count = 0;
           
            HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
            heERPContext.Database.SetCommandTimeout(120);

            // 查询所有需要的 APIStocks 一次性加载到内存 x.status == 1 有效的SKU加载到内存
            List<APIStockEntity> apiStocks = heERPContext.APIStocks
                .Where(x => listSkus.Contains(x.SKU) && x.Status == 1)
                .ToList();
            CLSLogger.Info("UGGapiStocks1", apiStocks.ToJsonString(), clogTags);
            var updatedStocks = new List<APIStockEntity>();

            // 遍历 apiStocks 并批量更新
            foreach (var apiStock in apiStocks)
            {
                clogTags["sku"] = apiStock.SKU;

                var product = products.FirstOrDefault(x => x.SKU.Equals(apiStock.SKU) && x.WarehouseID == warehouseId);
                if (product != null)
                {
                    var UGGProduct = stocks.FirstOrDefault(x => x.Barcode == product.Barcode1);
                    
                    if (UGGProduct != null)
                    {
                        if (UGGProduct.AvaiStockQty <= 5)
                        {
                            apiStock.Qty_available = 0;
                            apiStock.Qty = 0;
                            apiStock.Qty_aladdin = 0;
                        }
                        else
                        {
                            apiStock.Qty_available = UGGProduct.AvaiStockQty;
                            apiStock.Qty = UGGProduct.AvaiStockQty;
                            apiStock.Qty_aladdin = UGGProduct.AvaiStockQty;
                        }

                        apiStock.Updated_time = DateTime.UtcNow;
                        updatedStocks.Add(apiStock);

                        CLSLogger.Info(
                            UGGProduct.AvaiStockQty <= 5
                                ? "UpdateStockForUGGJob完成一个，其ERP库存数设定为0，因为UGG实际库存数小于等于5"
                                : "UpdateStockForUGGJob完成一个",
                            $"SKU: {apiStock.SKU}, barcode: {UGGProduct.Barcode}, qty: {UGGProduct.AvaiStockQty}",
                            clogTags
                        );

                        count++;
                    }
                }
            }

            // 批量更新并保存
            heERPContext.APIStocks.UpdateRange(updatedStocks);
            await heERPContext.SaveChangesAsync();
            return count;
        }
    }
}
