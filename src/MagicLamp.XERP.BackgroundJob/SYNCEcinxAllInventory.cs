﻿using Abp.Application.Services;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy.ECinx.Models;
using MagicLamp.XERP.BackgroundJob;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.EntityFrameworkCore;
using TimeZoneConverter;
using Abp.Web.Models;
using MagicLamp.PMS.Proxy.ECinx;
using MagicLamp.PMS.Infrastructure;
using Abp.Runtime.Caching;
using Abp.Dependency;
using MagicLamp.PMS.DTO;
using System.Globalization;
using static Org.BouncyCastle.Math.EC.ECCurve;

namespace MagicLamp.XERP.BackgroundJob
{
    public class SYNCEcinxAllInventory
    {

        Dictionary<string, string> clogTags = new Dictionary<string, string>
        {
            ["type"] = "EcinxAllInventory"
        };
        private PMSDbContext _pmsContext;
        private HEERPDbContext _heerpContext;
        private ECinxClient _CQBSCClient;
        private ECinxClient _WHYCCClient;

        private IBackgroundJobManager _backgroundJobManager;

        EcinxConfigerData config = EcinxConfigerData.Configuration;
        public SYNCEcinxAllInventory(
           IBackgroundJobManager backgroundJobManager
       )
        {
            _backgroundJobManager = backgroundJobManager;
            _pmsContext = new PMSDbContextFactory().CreateDbContext();
            _heerpContext = new HEERPDbContextFactory().CreateDbContext();
            ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
            _CQBSCClient = new ECinxClient(cacheManager, ECinxClient.EcinxAccountEnum.CQBSC);
            _WHYCCClient = new ECinxClient(cacheManager, ECinxClient.EcinxAccountEnum.YIWUYINGCHI);
        }

        public void Execute()
        {
            SyncMFDEcinxStock();
            SyncWHYCEcinxStock();
        }

        public void SyncMFDEcinxStock()
        {
            //MFD
            List<string> warehousecode = config.MFD.ExportMapping.Values.Select(e => e.WarehouseCode).ToList();

            PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();
            EcinxInventoryQueryResponse EcinxInventoryresult = new EcinxInventoryQueryResponse();
            // Get ALLSKU From Ecinx
            List<ProductEntity> ProductList = pmsContext.Products
                                .Where(x => x.SyncToEcinx == true
                                            && !string.IsNullOrWhiteSpace(x.ECinxSKU)
                                            && x.ActiveFlag == true
                                            && x.PurchasePeriodCode == "3rdParty"
                                            && !config.PushWHYCMFDWarehouseID.Contains((int)x.WarehouseID))
                                .ToList();

            int pageSize = 100;

            var totalCount = ProductList.Count();
            int totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;

            try
            {

                for (int i = 0; i < totalPage; i++)
                {
                    List<SyncEcinxStockDTO> dtos = new List<SyncEcinxStockDTO>();

                    var ProductListToProcess = ProductList.OrderBy(x => x.CreateTime).Skip(i * pageSize).Take(pageSize).ToList();

                    CLSLogger.Info("Ecinx全量同步信息", $"i：{i} detail: {ProductListToProcess.ToJsonString()} ", clogTags);

                    foreach (var item in ProductListToProcess)
                    {
                        EcinxInventoryQueryInput input = new EcinxInventoryQueryInput();
                        input.goodsCode = item.SKU;

                        CLSLogger.Info("Ecinx_Sync_stock0", $"SKU: {input.goodsCode} Inpput：{input.ToJsonString()}  ", clogTags);
                        EcinxInventoryresult = _CQBSCClient.QueryInventory(input).Result;

                        CLSLogger.Info("Ecinx_Sync_stock1", $"SKU: {input.goodsCode} InventoryResult：{EcinxInventoryresult.ToJsonString()}  ", clogTags);

                        // 如果 result 或 result.rows 为空，则跳过本次循环
                        if (EcinxInventoryresult?.result?.rows == null || !EcinxInventoryresult.result.rows.Any())
                        {
                            continue;
                        }

                        int totalinventory = 0;
                        string expiredate = "";
                        double CostPrice = 0;

                        List<double> CostPriceList = new List<double>();
                        List<string> expiredateList = new List<string>();


                        foreach (var Ecinxitem in EcinxInventoryresult.result.rows)
                        {
                            //Filter WarehouseCode
                            if (warehousecode.Contains(Ecinxitem.warehouseCode))
                            {
                                //All canSalesNum
                                totalinventory = totalinventory + Ecinxitem.canSalesNum;
                                if (Ecinxitem.costPrice > 0)
                                {
                                    CostPriceList.Add(Ecinxitem.costPrice);
                                }

                                if (Ecinxitem.batches != null) // && Ecinxitem.canSalesNum > 0
                                {
                                    if (Ecinxitem.batches.Where(t => t.canPickingNum > 0).Select(t => t.expiryDate) != null)
                                    {
                                        expiredateList.Add(Ecinxitem.batches.Where(t => t.canPickingNum > 0).Min(t => t.expiryDate));
                                    }

                                }
                            }
                            //No Data;
                            //else
                            //{
                            //    totalinventory = 0;
                            //}
                        }


                        expiredate = expiredateList.Count > 0 ? expiredateList.Min() : expiredate; //Muti Warehouse get Min
                        CostPrice = CostPriceList.Count > 0 ? CostPriceList.Max() : CostPrice; //Muti Warehosue get Max

                        CLSLogger.Info("Ecinx_Sync_stock2", $"SKU: {input.goodsCode} dtos：{expiredateList.ToJsonString()} {CostPriceList.ToJsonString()} {totalinventory.ToJsonString()} ", clogTags);

                        if (totalinventory >= 0 || expiredateList.Count > 0 || CostPriceList.Count > 0)
                        {
                            SyncEcinxStockDTO _ecinxsku = new SyncEcinxStockDTO();
                            _ecinxsku.SKU = item.SKU;
                            //_ecinxsku.Barcode = Barcode;
                            _ecinxsku.ExpiryDate = expiredate; // string.IsNullOrWhiteSpace(expiredate) ? "" : DateTime.ParseExact(expiredate, "MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/yyyy");
                                                               //运营还有一个需求 就是  由于188 并发导致的 超卖 需要ecinx同步库存的时候  跟odoo一样 mfderp 需要把 ecinx 可销售库存总数 * 0.9 （向下取整）  
                                                               //如果可销售总数 = 1 则同步 1
                                                               //这种方式来避免并发超卖
                            _ecinxsku.StockQty = totalinventory == 1 ? 1 : (int)Math.Floor(totalinventory * 0.9);
                            _ecinxsku.CostPrice = CostPrice;
                            dtos.Add(_ecinxsku);
                        }

                        // The Result is Correct.
                        CLSLogger.Info("Ecinx_expiredate3", $" dtos：{dtos.ToJsonString()} ", clogTags);
                    }

                    if (dtos.Count > 0)
                    {
                        _backgroundJobManager.EnqueueAsync<SyncStockFromEcinxJob, List<SyncEcinxStockDTO>>(dtos, BackgroundJobPriority.High, TimeSpan.FromSeconds(3));
                    }
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Info("Ecinx_expiredateexception", $"error：{ex.ToJsonString()} ", clogTags);
                //throw;
            }
        }

        public void SyncWHYCEcinxStock()
        {
            //WHYC
            List<string> whyc_warehousecode = config.WHYC.ExportMapping.Values.Select(e => e.WarehouseCode).ToList();

            PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();
            EcinxInventoryQueryResponse EcinxInventoryresult = new EcinxInventoryQueryResponse();
            // Get ALLSKU From Ecinx
            // select * from BAS.Product  where 
            List<ProductEntity> ProductList = pmsContext.Products
                .Where(x => x.SyncToEcinx == true
                            && !string.IsNullOrWhiteSpace(x.ECinxSKU)
                            && x.ActiveFlag == true
                            && x.PurchasePeriodCode == "3rdParty"
                            && config.PushWHYCMFDWarehouseID.Contains((int)x.WarehouseID))
                .ToList();

            int pageSize = 100;

            var totalCount = ProductList.Count();
            int totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;

            CLSLogger.Info("Ecinx全量同步信息1", $"detail: {ProductList.ToJsonString()} ", clogTags);

            try
            {

                for (int i = 0; i < totalPage; i++)
                {
                    List<SyncEcinxStockDTO> dtos = new List<SyncEcinxStockDTO>();

                    var ProductListToProcess = ProductList.OrderBy(x => x.CreateTime).Skip(i * pageSize).Take(pageSize).ToList();

                    CLSLogger.Info("Ecinx全量同步信息2", $"i：{i} detail: {ProductListToProcess.ToJsonString()} ", clogTags);


                    foreach (var item in ProductListToProcess)
                    {
                        EcinxInventoryQueryInput input = new EcinxInventoryQueryInput();
                        input.goodsCode = item.SKU;
                        EcinxInventoryresult = _WHYCCClient.QueryInventory(input).Result;

                        CLSLogger.Info("盈驰-Ecinx全量同步信息", $"item: {item.SKU.ToJsonString()} EcinxInventoryresult: {EcinxInventoryresult.ToJsonString()} ", clogTags);

                        // 如果 result 或 result.rows 为空，则跳过本次循环
                        if (EcinxInventoryresult?.result?.rows == null || !EcinxInventoryresult.result.rows.Any())
                        {
                            continue;
                        }

                        int totalinventory = 0;
                        string expiredate = "";
                        double CostPrice = 0;

                        List<double> CostPriceList = new List<double>();
                        List<string> expiredateList = new List<string>();


                        foreach (var Ecinxitem in EcinxInventoryresult.result.rows)
                        {
                            //Filter whyc_warehousecode
                            if (whyc_warehousecode.Contains(Ecinxitem.warehouseCode))
                            {
                                //All canSalesNum
                                totalinventory = totalinventory + Ecinxitem.canSalesNum;
                                if (Ecinxitem.costPrice > 0)
                                {
                                    CostPriceList.Add(Ecinxitem.costPrice);
                                }

                                if (Ecinxitem.batches != null) // && Ecinxitem.canSalesNum > 0
                                {
                                    if (Ecinxitem.batches.Where(t => t.canPickingNum > 0).Select(t => t.expiryDate) != null)
                                    {
                                        expiredateList.Add(Ecinxitem.batches.Where(t => t.canPickingNum > 0).Min(t => t.expiryDate));
                                    }

                                }
                            }
                            //No Data;
                            //else
                            //{
                            //    totalinventory = 0;
                            //}
                        }

                        expiredate = expiredateList.Count > 0 ? expiredateList.Min() : expiredate; //Muti Warehouse get Min
                        CostPrice = CostPriceList.Count > 0 ? CostPriceList.Max() : CostPrice; //Muti Warehosue get Max

                        if (totalinventory >= 0 || expiredateList.Count > 0 || CostPriceList.Count > 0)
                        {
                            SyncEcinxStockDTO _ecinxsku = new SyncEcinxStockDTO();
                            _ecinxsku.SKU = item.SKU;
                            //_ecinxsku.Barcode = Barcode;
                            _ecinxsku.ExpiryDate = expiredate; // string.IsNullOrWhiteSpace(expiredate) ? "" : DateTime.ParseExact(expiredate, "MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/yyyy");
                                                               //运营还有一个需求 就是  由于188 并发导致的 超卖 需要ecinx同步库存的时候  跟odoo一样 mfderp 需要把 ecinx 可销售库存总数 * 0.9 （向下取整）  
                                                               //如果可销售总数 = 1 则同步 1
                                                               //这种方式来避免并发超卖
                            _ecinxsku.StockQty = totalinventory == 1 ? 1 : (int)Math.Floor(totalinventory * 0.9);
                            _ecinxsku.CostPrice = CostPrice;
                            dtos.Add(_ecinxsku);
                        }

                        // The Result is Correct.
                        CLSLogger.Info("盈驰-Ecinx_expiredate3", $"dtos：{dtos.ToJsonString()} ", clogTags);
                    }

                    if (dtos.Count > 0)
                    {
                        _backgroundJobManager.EnqueueAsync<SyncStockFromEcinxJob, List<SyncEcinxStockDTO>>(dtos, BackgroundJobPriority.High, TimeSpan.FromSeconds(3));
                    }
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Info("盈驰-Ecinx_expiredateexception", $"error：{ex.ToJsonString()} ", clogTags);
                //throw;
            }
        }



    }

}
