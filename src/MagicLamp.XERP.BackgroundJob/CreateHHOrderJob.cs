﻿using MagicLamp.PMS.DTOs.ThirdPart;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.XERP.BackgroundJob
{
    public class CreateHHOrderJob : CommonBackgroundJob<HHPushOrderInputDTO>
    {
        public override void Process(HHPushOrderInputDTO inputDto)
        {
            LogTags["orderpackno"] = inputDto.order_no;

            StringBuilder logBuilder = new StringBuilder();

            try
            {
                var result = HHProxy.PushOrderToHH(inputDto);

                if (result == null || result.result != 0)
                {
                    CLSLogger.Error("create HH order failed", result.ToJsonString(), LogTags);
                    throw new Exception($"create HH order failed, response: {result.ToJsonString()}");
                }
                else
                {
                    logBuilder.Append("推送给HH的订单信息成功。");
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("create HH order encountered an exception", ex, LogTags);
                throw ex;
            }
            finally
            {
                CLSLogger.Info("创建HH订单流程结束", logBuilder.ToString(), LogTags);
            }
        }
    }
}
