﻿using MagicLamp.PMS.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Abp.Runtime.Caching;
using MagicLamp.PMS.Entities;
using System.Linq;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Infrastructure.Common;
using Abp.Dependency;
using System.Diagnostics;

namespace MagicLamp.XERP.BackgroundJob
{
    public class HEOrderToXJob
    {
        public ICacheManager Cache { get; set; }
        [SkipConcurrentExecution(60 * 120)]
        public void SynchronizeOrderCost()
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["jobid"] = Guid.NewGuid().ToString();
            tags["type"] = "HEOrderToXJob";

            Cache = IocManager.Instance.Resolve<ICacheManager>();

            var cacheManager = Cache.GetCache(CacheKeys.JOBCACHEKEYNAME);
            var val = cacheManager.GetOrDefault(CacheKeys.HEORDERJOBKEY);
            CLSLogger.Info("HEOrderToXJob开始运行", $"是否已有job正在运行：{val != null}", tags);
            if (val == null)
            {
                cacheManager.Set(CacheKeys.HEORDERJOBKEY, true);
            }
            else
            {
                return;
            }

            System.Text.StringBuilder log = new System.Text.StringBuilder();
            Stopwatch stopwatch = new Stopwatch();
            Stopwatch queryWatch = new Stopwatch();
            stopwatch.Start();
            try
            {
                HEERPDbContext hEERPDbContext = new HEERPDbContextFactory().CreateDbContext(null);
                PMSDbContext pMSDbContext = new PMSDbContextFactory().CreateDbContext(null);
                //查询没有获取成本的订单
                DateTime dateTime = DateTime.Now.AddDays(-1);
                queryWatch.Start();
                var orders = from order in hEERPDbContext.Orders
                             join seq in hEERPDbContext.Seqs on order.seq equals seq.seq
                             where order.update_time > dateTime
                             && (order.ISGetCost == null ? true : !Convert.ToBoolean(order.ISGetCost))
                             && (seq.bizID == 1 || seq.bizID == 99 || seq.bizID == 888 || seq.bizID == 999)//云商 HE 魔法海淘 圣诞比特
                             select order;
                List<OrderEntity> HEOrders = orders.ToList();
                List<int> oids = HEOrders.Select(x => x.oid).ToList();
                if (HEOrders.Count() > 0)
                {
                    var orderDetails = from orderDetail in hEERPDbContext.OrderDetails
                                       join order in hEERPDbContext.Orders on orderDetail.oid equals order.oid
                                       join seq in hEERPDbContext.Seqs on order.seq equals seq.seq
                                       join business in hEERPDbContext.Businesses on seq.bizID equals business.bizID
                                       where oids.Contains(orderDetail.oid)
                                       select new OrderLineCostAndPriceEntity()
                                       {
                                           OrderLineID = orderDetail.detailid,
                                           OrderID = order.orderid,
                                           Business = business.bizName,
                                           SKU = orderDetail.sku,
                                           Price = orderDetail.price,
                                           Qty = orderDetail.qty
                                       };
                    int count = orderDetails.Count();
                    queryWatch.Stop();
                    log.Append($"查询count耗时：{queryWatch.ElapsedMilliseconds} ms，总条数：{count}；");
                    queryWatch.Restart();
                    if (count > 0)
                    {
                        var orderDetailsCostAndPrice = orderDetails.ToList();
                        //查询成本                      
                        foreach (var lineCostAndPrice in orderDetailsCostAndPrice)
                        {
                            var exsitLine = pMSDbContext.OrderLineCostAndPrice.FirstOrDefault(o => o.OrderLineID == lineCostAndPrice.OrderLineID);
                            if (exsitLine == null)
                            {
                                var lineCost = (from cost in pMSDbContext.ProductBusinessCost
                                                where cost.Business.Equals(lineCostAndPrice.Business) && cost.SKU.Equals(lineCostAndPrice.SKU)
                                                select cost).ToList();
                                if (lineCost.Count > 0)
                                {
                                    var cost = lineCost.FirstOrDefault();
                                    lineCostAndPrice.ChineseName = cost.ChineseName;
                                    lineCostAndPrice.EnglishName = cost.EnglishName;
                                    lineCostAndPrice.Formula1 = cost.Formula1;
                                    lineCostAndPrice.Formula2 = cost.Formula2;
                                    lineCostAndPrice.BusinessCost = cost.BusinessCost;
                                    lineCostAndPrice.Cost = cost.Cost;
                                    lineCostAndPrice.CostType = cost.CostType;
                                    lineCostAndPrice.Currency = cost.Currency;
                                    lineCostAndPrice.CustomerID = cost.CustomerID;
                                    lineCostAndPrice.DefaultCost = cost.DefaultCost;
                                    lineCostAndPrice.DropShippingCost = cost.DropShippingCost;
                                    lineCostAndPrice.LaborCost = cost.LaborCost;
                                    lineCostAndPrice.MaterialCost = cost.MaterialCost;
                                    lineCostAndPrice.Purchasecost = cost.Purchasecost;
                                    lineCostAndPrice.TaxRate = cost.TaxRate;
                                    lineCostAndPrice.TransportCost = cost.TransportCost;
                                }
                                pMSDbContext.OrderLineCostAndPrice.Add(lineCostAndPrice);
                            }
                        }
                        pMSDbContext.SaveChanges();
                        queryWatch.Stop();
                        log.Append($"执行成本查询耗时：{queryWatch.ElapsedMilliseconds} ms；");
                        //修改HE ERP Order标志位
                        HEOrders.All(o => { o.ISGetCost = true; return true; });
                        hEERPDbContext.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("HEOrderToXJob发生异常", ex);
                throw ex;
            }
            finally
            {
                cacheManager.Remove(CacheKeys.HEORDERJOBKEY);
                stopwatch.Stop();
                CLSLogger.Info("HEOrderToXJob结束运行", $"总耗时：{stopwatch.ElapsedMilliseconds} ms", tags);
            }
        }
    }
}
