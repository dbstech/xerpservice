using System;
using System.Collections.Generic;
using System.Linq;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Caching;
using Cmq_SDK.Cmq;
using MagicLamp.PMS.Configuration;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure.Common;
using MagicLamp.PMS.Proxy;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json.Linq;

namespace MagicLamp.XERP.BackgroundJob
{
    public class SyncExchangeRates : BackgroundJob<string>, ITransientDependency
    {
        private IRepository<ExchangeRate, string> _exchangeRateRepository;
        private IUnitOfWorkManager _unitOfWorkManager;
        private IBackgroundJobManager _backgroundJobManager;
        private readonly ICacheManager _cacheManager;
        public SyncExchangeRates(
            IRepository<ExchangeRate, string> exchangeRateRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IBackgroundJobManager backgroundJobManager,
            ICacheManager cacheManager
        )
        {
            _exchangeRateRepository = exchangeRateRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _backgroundJobManager = backgroundJobManager;
            _cacheManager = cacheManager;
        }
        private void InsertOrUpdate(List<ExchangeRate> exchangeRates, string from, string to, decimal rate)
        {
            var exchangeRate = exchangeRates.FirstOrDefault(x => x.From == from && x.To == to);
            if (exchangeRate == null)
            {
                _exchangeRateRepository.Insert(new ExchangeRate {
                    From = from,
                    To = to,
                    Rate = rate
                });
            }
            else 
            {
                exchangeRate.Rate = rate;
                _exchangeRateRepository.Update(exchangeRate);
            }
        }
        public override void Execute(string queueName)
        {
            // List<Message> messages = TencentCMQProxy.BatchReceiveAndDeleteMessages(queueName, 16, 0);

            // if (messages.Count < 1)
            // {
            //     return;
            // }

            // List<JObject> exchangeRates188 = messages.Select(x => JObject.Parse(x.msgBody)).ToList();

            // var cache = _cacheManager.GetCache(CacheKeys.EXCHANGERATE188);

            // using (var unitOfWork = _unitOfWorkManager.Begin())
            // {
            //     var exchangeRates = _exchangeRateRepository.GetAll().ToList();
            //     exchangeRates188.ForEach(x =>
            //     {
            //         string from = x["from"].ToString();
            //         string to = x["to"].ToString();
            //         decimal rate = x["rate"].ToObject<decimal>();
            //         InsertOrUpdate(exchangeRates, from, to, rate);
            //         InsertOrUpdate(exchangeRates, to, from, 1 / rate);

            //         cache.Set(string.Format(CacheKeys.EXCHANGERATEFORMAT, from, to), rate, TimeSpan.FromHours(24));
            //         cache.Set(string.Format(CacheKeys.EXCHANGERATEFORMAT, to, from), (1 / rate), TimeSpan.FromHours(24));
            //     });
            //     _backgroundJobManager.Enqueue<UpdateOdooProductCost, string>(null);

            //     unitOfWork.Complete();
            // }

            // TODO: only update products if it's currency has been updated
        }
    }
}