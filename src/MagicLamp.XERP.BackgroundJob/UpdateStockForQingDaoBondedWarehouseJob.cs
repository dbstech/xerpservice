using System;
using System.Linq;
using System.Collections.Generic;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Proxy;
using Newtonsoft.Json;
using MagicLamp.PMS.Proxy.YiTongBaoOMSAPI;

namespace MagicLamp.XERP.BackgroundJob
{
    public class UpdateStockForQingDaoBondedWarehouseJob
    {
        public void UpdateStock()
        {
            PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();
            HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
            int warehouseId = pmsContext.WareHouses.FirstOrDefault(x => x.Location == "青岛保税仓").Id;
            YiTongBaoOMSProxy ytbProxy = new YiTongBaoOMSProxy();

            var itemCodeSkuDict = pmsContext.Products.Where(x => x.WarehouseID == warehouseId && x.BondedWarehouseItemCode != null).ToDictionary(x => x.BondedWarehouseItemCode, x => x.SKU.Trim());

            IEnumerable<string> skus = itemCodeSkuDict.Values;
            IEnumerable<string> itemCodes = itemCodeSkuDict.Keys;

            IEnumerable<APIStockEntity> apiStocks = heERPContext.APIStocks.Where(x => skus.Contains(x.SKU.Trim()));

            for (int i = 0; i < itemCodes.Count(); i = i + 10)
            {
                IEnumerable<InventoryQueryCriteria> criteriaList = itemCodes.Select(x => new InventoryQueryCriteria
                {
                    itemCode = x
                });
                try
                {
                    var bondedWarehouseProducts = ytbProxy.InventoryQuery(criteriaList.Skip(i).Take(10)).GroupBy(x => x.itemCode).Select(
                         x => new
                         {
                             ItemCode = x.Key,
                             // TODO: use availableQty instead of Qty around 15/12/2020 when yitongbao fix their wms and send back availableQty
                             Quantity = x.Sum(y => y.quantity),
                             ExpireDate = x.Min(y => y.expireDate)
                         });
                    foreach (var product in bondedWarehouseProducts)
                    {
                        var sku = itemCodeSkuDict[product.ItemCode];
                        var apiStock = apiStocks.FirstOrDefault(x => x.SKU == sku);
                        apiStock.Qty = product.Quantity;
                        apiStock.Qty_available = product.Quantity;
                        apiStock.Qty_aladdin = product.Quantity;
                        apiStock.Expired_date = product.ExpireDate;
                        apiStock.Updated_time = DateTime.Now;
                    }
                    heERPContext.SaveChanges();
                }
                catch (YiTongBaoServerException ex)
                {
                    SendNotificationToDingDingJob ddJob = new SendNotificationToDingDingJob();
                    ddJob.SendBondedWarehouseQingDaoAlert($"获取青岛保税仓库存失败，请检查。产品参数为：{JsonConvert.SerializeObject(criteriaList)}。易通宝返回失败原因：{ex.Message}");
                }
                catch (Exception ex)
                {
                    CLSLogger.Error("青岛保税仓库存同步发生异常", ex);
                    throw ex;
                }
            }
        }
    }
}