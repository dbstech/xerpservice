using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using MagicLamp.PMS.BaXi;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy;
using System.Linq;
using MagicLamp.PMS.YiWuBondedWarehouse;
using Abp.Runtime.Caching;
using System;
using System.Collections.Generic;
using TimeZoneConverter;
using MagicLamp.PMS.Infrastructure.Extensions;
using Abp.Extensions;
using MagicLamp.PMS.EntityFrameworkCore;

namespace MagicLamp.XERP.BackgroundJob
{
    public class SyncYiDaTongYiWuOrderStatus
    {
        private IUnitOfWorkManager _unitOfWorkManager;
        private IRepository<OrderEntity, int> _orderRepository;
        private IRepository<OrderPackEntity, int> _orderPackRepository;
        private ICacheManager _cacheManager;
        private SendNotificationToDingDingJob _dingdingNotificationJob;
        private PMSDbContext _pmsContext;

        public SyncYiDaTongYiWuOrderStatus(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<OrderEntity, int> orderRepository,
            IRepository<OrderPackEntity, int> orderPackRepository,
            ICacheManager cacheManager
        )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _orderRepository = orderRepository;
            _orderPackRepository = orderPackRepository;
            _cacheManager = cacheManager;
            _pmsContext = new PMSDbContextFactory().CreateDbContext();
            _dingdingNotificationJob = new SendNotificationToDingDingJob();
        }

        public async Task Execute()
        {
            var client = new YiWuBondedWarehouseClient(_cacheManager, YiWuAccountEnum.YiDaTong);

            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                // yidatong yiwu orders only
                var yiwuExportFlags = new List<int> { 86 };
                var orderPacks = _orderPackRepository.GetAll().Where(x => yiwuExportFlags.Contains(x.exportFlag.GetValueOrDefault()) && x.status == 3 ).ToList(); //x.status != 4 && x.freezeFlag == 0
                if (orderPacks.Count() == 0)
                {
                    return;
                }

                foreach (var orderPack in orderPacks)
                {
                    TradeOrderQueryResponse queryResponse = await client.TradeOrderQuery(new TradeOrderQueryData
                    {
                        orderCode = orderPack.orderPackNo
                    });

                    if (queryResponse == null)
                    {
                        continue;
                    }

                    var order = queryResponse.ResultData.Orders.FirstOrDefault();

                    if (order == null)
                    {
                        continue;
                    }

                    // 900 已发货
                    // 1000 客户确认收货
                    // 9999 完成
                    if (new List<string> { "900", "1000", "9999" }.Contains(order.StateCode.ToString()))
                    {
                        orderPack.status = 4;
                        orderPack.out_time = DateTime.UtcNow.ConvertUTCTimeToNZTime();

                        // 保税仓出库，通知Exinc发货单发货;
                        //EcinxDeliverySendOut(orderPack);
                    }

                    // only fill carrierId and freight id if carrierId is null/empty
                    // record bondedwarehouse carrier id and carrier company into table
                    if (!order.ShipNo.IsNullOrWhiteSpace())
                    {
                        orderPack.bonded_warehouse_carrier_id = order.ShipNo;
                        orderPack.bonded_warehouse_carrier_company = order.ShipName;
                        if (orderPack.carrierId.IsNullOrWhiteSpace())
                        {

                            orderPack.carrierId = order.ShipNo;

                            if (order.ShipName == "申通快递")
                            {
                                orderPack.freightId = "ShengTong";
                            }
                            else if (order.ShipName == "邮政速递")
                            {
                                orderPack.freightId = "ems";
                            }
                            else if (order.ShipName == "百世汇通")
                            {
                                orderPack.freightId = "huitong";
                            }
                            else if (order.ShipName == "顺丰快递")
                            {
                                orderPack.freightId = "sfexpress";
                            }
                            else if (order.ShipName.Contains("韵达快递"))
                            {
                                orderPack.freightId = "yunda";
                            }
                            else
                            {
                                throw new Exception($"Unable to find matching freighId for: {order.ShipName}");
                            }
                        }
                    }

                    // wait for 2 seconds due to yiwu api throttling
                    await Task.Delay(2000);
                }
                unitOfWork.Complete();
            }
        }


        //// 保税仓出库,Ecinx 发货单发货
        //public void EcinxDeliverySendOut(OrderPackEntity OrderPack)
        //{
        //    bool EcinxorderPackTradeExists = _pmsContext.EcinxOrdersPackTrades.Any(x => x.Opid == OrderPack.opID);
        //    if (!EcinxorderPackTradeExists)
        //    {
        //        return;
        //    }
        //    else
        //    {
        //        EcinxOrdersPackTradeEntity EcinxorderPackTradeEntity;
        //        if (EcinxorderPackTradeExists)
        //        {
        //            EcinxorderPackTradeEntity = _pmsContext.EcinxOrdersPackTrades.FirstOrDefault(x => x.Opid == OrderPack.opID);
        //            EcinxorderPackTradeEntity.DeliveryStatus = 2; //0: 创建Ecinx订单；1: 创建Ecinx出货单; 2: Ecinx出货单出货
        //            _pmsContext.EcinxOrdersPackTrades.Update(EcinxorderPackTradeEntity);
        //        }
        //        _pmsContext.SaveChanges();
        //    }
        //}


    }
}