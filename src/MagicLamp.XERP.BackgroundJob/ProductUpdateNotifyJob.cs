﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Cmq_SDK;
using Hangfire;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.Domain;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.Enums;

namespace MagicLamp.XERP.BackgroundJob
{
    public class ProductUpdateNotifyJob : CommonBackgroundJob<ProductUpdateNotifyJobArgs>
    {
        private IAccountManager _accountManager;
        public ProductUpdateNotifyJob(IAccountManager accountManager)
        {
            _accountManager = accountManager;
        }

        [Queue("critical")]
        public override void Process(ProductUpdateNotifyJobArgs args)
        {
            var inputDtos = args.Products;
            PMSDbContext pMSDbContext = new PMSDbContextFactory().CreateDbContext();
            Dictionary<string, string> clogTags = new Dictionary<string, string>();

            foreach (var inputDto in inputDtos)
            {
                if (inputDto.Before != null && inputDto.Before.WarehouseID.HasValue)
                {
                    var wareHouseEntity = pMSDbContext.WareHouses.FirstOrDefault(x => x.WareHouseId == inputDto.Before.WarehouseID);
                    if (wareHouseEntity != null)
                    {
                        inputDto.Before.Warehouse = wareHouseEntity.code;
                    }
                }
                if (inputDto.After != null && inputDto.After.WarehouseID.HasValue)
                {
                    var wareHouseEntity = pMSDbContext.WareHouses.FirstOrDefault(x => x.WareHouseId == inputDto.After.WarehouseID);
                    if (wareHouseEntity != null)
                    {
                        inputDto.After.Warehouse = wareHouseEntity.code;
                    }
                }

                if (inputDto.Before != null)
                {
                    inputDto.Before.MainImages = inputDto.Before.MainImages.OrderBy(x => x.Sort).ToList();
                    inputDto.Before.DetailImages = inputDto.Before.DetailImages.OrderBy(x => x.Sort).ToList();
                }

                if (inputDto.After != null)
                {
                    inputDto.After.MainImages = inputDto.After.MainImages.OrderBy(x => x.Sort).ToList();
                    inputDto.After.DetailImages = inputDto.After.DetailImages.OrderBy(x => x.Sort).ToList();
                    clogTags["trackid"] = inputDto.After.SKU;
                }

                BaseMqDTO<ProductInfoMqDto> skuInfoMqDto = new BaseMqDTO<ProductInfoMqDto>();
                skuInfoMqDto.MessageBody = inputDto;

                //clslog;
                CLSLogger.Info("ProductUpdateNotifyJobCMQ", $"MessageBody:{skuInfoMqDto.MessageBody.ToJsonString()}", LogTags);
                
                TencentCMQProxy.PublishTopicMessage(TCConstants.CMQTopics.TOPIC_SKUINFO, skuInfoMqDto, null, clogTags);

                string actionString = "";
                switch(inputDto.Action)
                {
                    case MqActionEnum.Create:
                        actionString = "创建";
                        break;
                    case MqActionEnum.Update:
                        actionString = "修改";
                        break;
                    case MqActionEnum.Delete:
                        actionString = "删除";
                        break;                    
                }

                LogUserActivityVO logUserActivityVO = new LogUserActivityVO
                {
                    UserId = args.UserId,
                    UserName = args.UserName,
                    ObjectType = UserActivityObjectType.Product,
                    ObjectId = inputDto.After.SKU,
                    Action = inputDto.Action.ToString(),
                    CreatedAt = args.CreatedAt,
                    Content = $"{actionString}了产品'{(inputDto.After.SKU)}'",
                    Before = inputDto.Before.ToJsonString(),
                    After = inputDto.After.ToJsonString()
                };

                _accountManager.LogUserActivity(logUserActivityVO);
            }

        }


    }
}
