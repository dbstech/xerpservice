﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using MagicLamp.XERP.BackgroundJob.Common;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.EntityFrameworkCore;
using System.Linq;
using Abp.UI;
using MagicLamp.PMS.Proxy;
using Cmq_SDK;
using Abp.BackgroundJobs;
using MagicLamp.PMS.DTOs.Enums;
using Abp.Extensions;

namespace MagicLamp.XERP.BackgroundJob
{
    public class FluxOrderPackOutboundGenerateJob: CommonBackgroundJob<FluxOrderInfoMqDto>
    {
        //list for NSF and NSFAU names
        private List<string> freightIdNSF = new List<string> { "nsf", "sfnsf", "nsfyd", "nsfau", "nsfaunoqrcode", "qdbsmilk" };
        //business id
        private List<int> bizId = new List<int> { 2, 3, 6, 14, 21, 22, 23, 32, 33, 36, 37, 38 };
        private IBackgroundJobManager backgroundJobManager;
        private List<string> messageTags = new List<string> { "delivery" };

        public FluxOrderPackOutboundGenerateJob(IBackgroundJobManager _backgroundJobManager)
        {
            backgroundJobManager = _backgroundJobManager;
        }

        public override void Process(FluxOrderInfoMqDto dto)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>();

            if (dto == null || dto.OrderNo.IsNullOrEmpty())
            {
                return;
            }

            HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
            var orderPacks = heERPContext.OrderPacks.Where(x => x.orderPackNo == dto.OrderNo).ToList();
            var listOrders = orderPacks.Select(x => x.orderID).ToList();
            var orders = heERPContext.Orders.Where(x => listOrders.Contains(x.orderid)).ToList();
            var listSeqs = orders.Select(x => x.seq).ToList();
            var seqs = heERPContext.Seqs.Where(x => listSeqs.Contains(x.seq)).ToList();


            if (orderPacks.IsNullOrEmpty())
            {
                throw new UserFriendlyException("Can't find any order packs"); //--http status code 500
            }
            //在ERP里carrierid是快递单号的意思，在Flux里carrierid是快递承运商ID==ERP中frieghtid,i.carrierid == confirm.DeliveryNo&&
            foreach (var oPack in orderPacks)
            {
                if (dto.Udf1 != null)
                {
                    oPack.syc_flag = Convert.ToBoolean(0);
                    oPack.attchment_name = dto.Udf1;
                }

                //保存状态
                bool flag = false;
                if (oPack.freightId != "fastway" && oPack.freightId != null)
                {

                }
                if (oPack.status != 4)
                {
                    oPack.status = 4;
                }
                else
                {
                    flag = true;
                }

                if (!oPack.out_time.HasValue)
                {
                    oPack.out_time = DateTime.UtcNow;
                }
                if (!dto.Weight.IsNullOrWhiteSpace())
                {
                    oPack.packWeight = Convert.ToDecimal(dto.Weight);
                }
                if (flag == false)
                {
                    clogTags["type"] = "backgroundjob_FluxOrderPackOutboundGenerateJob";
                    clogTags["orderpackno"] = oPack.orderPackNo;
                    NSFOrderDTO NSFOrder = new NSFOrderDTO
                    {
                        opid = oPack.opID,
                        orderid = oPack.orderID,
                        orderpackno = oPack.orderPackNo,
                        carrierid = oPack.carrierId,
                        freightid = oPack.freightId,
                        exportflag = oPack.exportFlag.Value,
                        status = oPack.status.Value,
                    };

                    var orderInfo = orders.FirstOrDefault(x => x.orderid == oPack.orderID);
                    var seqInfo = seqs.FirstOrDefault(x => x.seq == orderInfo.seq);

                    OrderPackStatusMqDto orderPackStatus = new OrderPackStatusMqDto
                    {
                        OrderID = oPack.orderID,
                        OrderPackNo = oPack.orderPackNo,
                        ParcelTrackingNo = oPack.carrierId,
                        ParcelPickDate = oPack.pick_time,
                        ParcelPackDate = oPack.pack_time,
                        CourierCode = oPack.freightId,
                        ParcelStatus = oPack.status.ToString(),
                        bizId = seqInfo.bizID
                    };

                    // 1：188 yishan
                    // 888：魔法海淘/洋小范 motan

                    var name = (BusinessEnum)orderPackStatus.bizId;
                    messageTags.Add(name.ToString());

                    TencentCMQProxy.PublishTopicMessage(TCConstants.CMQTopics.TOPIC_ORDERNOTIFICATION, orderPackStatus, messageTags, clogTags);

                    //TODO: TMall, Taobao推送出库信息
                    if (oPack.freezeFlag == 0 && bizId.Contains(seqInfo.bizID))
                    {
                        //add Background job
                    }

                    //出库API下单（NSF 和 NSFAU) 推送出库信息给新顺丰 nsf & sfnsf & nsfau，澳洲新顺丰"nsfau"
                    if (oPack.freezeFlag == 0 && oPack.status == 4 && freightIdNSF.Contains(oPack.freightId.ToLower()))
                    {
                        NSFOrder.Action = NSFBackgroundJobActionEnum.CreateOrder;
                        backgroundJobManager.Enqueue<NSFBackgroundJob, NSFOrderDTO>(NSFOrder);
                    }
                }
                //TODO:修改包裹锁定状态  update stock

            }
        }
    }
}
