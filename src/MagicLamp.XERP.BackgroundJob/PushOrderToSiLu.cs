using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Cmq_SDK;
using Cmq_SDK.Cmq;
using Hangfire;
using Hangfire.Server;
using MagicLamp.PMS.DTO.BondedWarehouse;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.BondedWarehouse;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Proxy.ECinx;
using MagicLamp.PMS.Proxy.ECinx.Models;
using MagicLamp.XERP.BackgroundJob.Common;
using TimeZoneConverter;

namespace MagicLamp.XERP.BackgroundJob
{
    [AutomaticRetry(Attempts = 0)]
    public class PushOrderToSiLu : BackgroundJob<ORDER_XT2WMS>, ITransientDependency
    {
        private IUnitOfWorkManager _unitOfWorkManager;
        private IBackgroundJobManager _backgroundJobManager;
        private IRepository<OrderPackEntity, int> _orderPackRepository;
        private IRepository<OrderEntity, int> _orderRepository;
        private IRepository<OrderPackTradeEntity, int> _orderPackTradeRepository;
        private IRepository<OrderPackDetailEntity, int> _orderPackDetailRepository;

        private PMSDbContext _pmsContext;
        private HEERPDbContext _heerpContext;


        private string WarehouseName = "重庆保税仓";
        Dictionary<string, string> LogTags = new Dictionary<string, string>
        {
            ["jobid"] = Guid.NewGuid().ToString(),
            ["method"] = "CQBondedWarehouseCreateOrder"
        };
        private List<string> messageTags = new List<string> { "CQBondedWarehouseCreateOrder" };
        private TimeZoneInfo timeZoneNZ = TZConvert.GetTimeZoneInfo("Pacific/Auckland");



        private IRepository<CQBondedProductEntity, string> _cqBondedProductRepository;
        public PushOrderToSiLu(
            IUnitOfWorkManager unitOfWorkManager,
            IBackgroundJobManager backgroundJobManager,
            IRepository<OrderPackEntity, int> orderPackRepository,
            IRepository<OrderEntity, int> orderRepository,
            IRepository<OrderPackTradeEntity, int> orderPackTradeRepository,
            IRepository<CQBondedProductEntity, string> cqBondedProductRepository,
            IRepository<OrderPackDetailEntity, int> orderpackdetailRepository

        )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _backgroundJobManager = backgroundJobManager;
            _orderPackRepository = orderPackRepository;
            _orderRepository = orderRepository;
            _orderPackTradeRepository = orderPackTradeRepository;
            _cqBondedProductRepository = cqBondedProductRepository;
            _orderPackDetailRepository = orderpackdetailRepository;

            _pmsContext = new PMSDbContextFactory().CreateDbContext();
            _heerpContext = new HEERPDbContextFactory().CreateDbContext();

        }

        private void UpdateOrderInfoAddress(GMCreateOrderInputDTO GMInput, OrderEntity orderInfo)
        {
            if (orderInfo != null && (orderInfo.rec_name != GMInput.ReceiverName
                                      || orderInfo.rec_phone != GMInput.ReceiverPhone
                                      || orderInfo.idno != GMInput.ReceiverIdNo
                                      || orderInfo.province != GMInput.ReceiverProvince
                                      || orderInfo.city != GMInput.ReceiverCity
                                      || orderInfo.district != GMInput.ReceiverDistrict
                                      || orderInfo.town != GMInput.ReceiverAddress))
            {
                GMInput.ReceiverName = orderInfo.rec_name;
                GMInput.ReceiverPhone = orderInfo.rec_phone;
                GMInput.ReceiverIdNo = orderInfo.idno;
                GMInput.ReceiverProvince = orderInfo.province;
                GMInput.ReceiverCity = orderInfo.city;
                GMInput.ReceiverDistrict = orderInfo.district;
                GMInput.ReceiverAddress = orderInfo.town;
            }
        }
        public override void Execute(ORDER_XT2WMS inputDto)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var orderNum = inputDto.ORDER_HEAD.ORIGINAL_ORDER_NO;
                bool isOrderPackNo = Regex.Match(orderNum, "^[0-9]+-[0-9]+-[0-9]+", RegexOptions.IgnoreCase).Success;
                var orderPack = _orderPackRepository.FirstOrDefault(x => isOrderPackNo ? x.orderPackNo == orderNum : x.orderID == orderNum);
                string carrierId = orderPack.carrierId;
                var orderInfo = _orderRepository.FirstOrDefault(x => x.orderid == orderPack.orderID);
                var orderPackTrade = _orderPackTradeRepository.FirstOrDefault(x => x.Opid == orderPack.opID);

                string[] realorder = orderNum.Split('-');


                OrderPackEntity _orderPack = _heerpContext.OrderPacks.FirstOrDefault(x => x.orderID == realorder[0] );
                if (_orderPack == null)
                {
                    bool cacheOrder = _heerpContext.CacheOrders.Any(x => x.orderid == realorder[0]);
                    var order = _heerpContext.Orders.Any(x => x.orderid == realorder[0]);
                    if (cacheOrder == false && order == false)
                    {
                        CLSLogger.Info("重庆保税仓_推单时发现订单已取消", $"cache order,orders and orderpacks all not found {realorder[0]}", LogTags);
                        return;
                    }

                    if (cacheOrder == true || order == true)
                    {
                        throw new Exception($"重庆保税仓_下单时无orderpack，将重试。订单号：{realorder[0]}");
                    }
                }
                else
                {
                    if (orderPack.freezeFlag > 0)
                    {
                        CLSLogger.Info("重庆保税仓_推单时发现订单已取消", $"包裹已冻结：{orderPack.orderPackNo}  freezeflag: {orderPack.freezeFlag}  {orderPack.freezeComment}", LogTags);
                        return;
                    }

                    if (orderPack.status == 4 )
                    {
                        CLSLogger.Info("重庆保税仓_推单时发现订单已出库", $"包裹号：{orderPack.orderPackNo}", LogTags);
                        return;
                    }

                    if (orderPack.status == 3)
                    {
                        CLSLogger.Info("重庆保税仓_推单时发现订单已包", $"包裹号：{orderPack.orderPackNo}", LogTags);
                        return;
                    }
                }


                GMCreateOrderInputDTO GMInput = orderPackTrade.OriginalInfo.ConvertFromJsonString<GMCreateOrderInputDTO>();
                UpdateOrderInfoAddress(GMInput, orderInfo);

                var useAltAccount = new List<int> { 81, 82, 83 }.Contains(orderPack.exportFlag.GetValueOrDefault());

                List<string> municipalities = new List<string> { "北京市", "天津市", "上海市", "重庆市" };
                string province = municipalities.Contains(GMInput.ReceiverProvince)
                                    ? GMInput.ReceiverProvince.Replace("市", "")
                                    : GMInput.ReceiverProvince;

                List<string> erpSkus = GMInput.GoodsItems.Select(x => x.SKU).ToList();
                var bondedProducts = _cqBondedProductRepository.GetAll().Where(x => erpSkus.Contains(x.ERPSKU)).ToList();

                List<SkuNum> skuNums = GMInput.GoodsItems
                                        .GroupBy(d => d.SKU)
                                        .Select(x =>
                                        {
                                            var bondedProduct = bondedProducts.FirstOrDefault(y => y.ERPSKU == x.First().SKU);

                                            return new SkuNum
                                            {
                                                skuNum = bondedProduct.GMSKU,
                                                orderQty = x.Sum(s => s.Qty),
                                                price = x.Sum(s => s.Qty),
                                                quality = "正品",
                                                skuComm = string.Empty,
                                                lot = string.Empty
                                            };
                                        }
                                    ).ToList();

              
                var siLuCreateOrderInputDto = new SiLuCreateOrderInputDto
                {
                    orders = new List<Order> {
                                    new Order {
                                        orderNum = orderNum,
                                        tbNum = string.Empty,
                                        tbNick = string.Empty,
                                        isSelfBox = string.Empty,
                                        slgXid = string.Empty,
                                        customCode = inputDto.ORDER_HEAD.BAR_CODE,
                                        daTouBi = inputDto.ORDER_HEAD.WL_RETINFO,
                                        expressCode = inputDto.ORDER_HEAD.LOGISTICS_NO,
                                        expressCompany = "STO",
                                        province = province,
                                        city = GMInput.ReceiverCity,
                                        orderAddress = string.Join(" ", new string[] {
                                            province,
                                            GMInput.ReceiverCity,
                                            GMInput.ReceiverDistrict, 
                                            GMInput.ReceiverAddress,
                                        } ),
                                        contact = GMInput.BuyerName,
                                        mobile = GMInput.ReceiverPhone,
                                        phone = string.Empty,
                                        preOutTime = string.Empty,
                                        preArriveTime = string.Empty,
                                        comm = string.Empty,
                                        skuNums = skuNums
                                    }
                }
                };

                var proxy = new BondedWarehouseProxy();
                var outOrderAddResponse = proxy.OutOrderAdd(siLuCreateOrderInputDto, useAltAccount);

                if (outOrderAddResponse.resultCode != "0")
                {
                    if (outOrderAddResponse.resultMessage.Contains("快递单号已经重复"))
                    {
                        // 快递单号已经重复 已经推送完订单，但是包裹未更新已包状态; 
                        // update ERP orders_tatus = Packed when ordered succeed! update ERP pack_time 
                        if (orderPack.status != 3 && orderPack.pack_time == null)
                        {
                            orderPack.status = (int)OrderPackStatusEnum.Packed;
                            orderPack.pack_time = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                            //TODO: 只有部分渠道需要一程面单操作
                            if (useAltAccount)
                            {
                                CreareOrderFirstExpress(orderNum, carrierId);
                            }
                        }
                        return;
                    }
                    else if (outOrderAddResponse.resultMessage.Contains("缺货"))
                    {
                        string barcode = "", sku = "", mfdsku = "";

                        string[] sArray = outOrderAddResponse.resultMessage.Split(new char[] { '缺', '货' });
                        barcode = sArray[0];
                        var orderPackDetail = _orderPackDetailRepository.FirstOrDefault(x => x.opid == orderPack.opID && x.barcode == barcode);
                        mfdsku = orderPackDetail.sku;

                        sku = mfdsku != "" ? " 魔法灯SKU: " + mfdsku : "";

                        // Push CMQ to Teambition_task Topic
                        // TeambitionMessage teambitionmesage = new TeambitionMessage
                        // {
                        //     OrderNo = orderNum, //订单号
                        //     Warehouse = WarehouseName,
                        //     Title = outOrderAddResponse.resultMessage,
                        //     OutOfStockSKU = mfdsku,
                        //     TriggerTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"),
                        //     MessageContent = $"请注意！订单号：{orderNum} 丝路创建订单失败，原因：{outOrderAddResponse.resultMessage}" + sku
                        // };
                        // TencentCMQProxy.PublishTopicMessage(TCConstants.CMQTopics.TOPIC_TEAMBITIONNOTIFICATION, teambitionmesage, messageTags, LogTags);

                        // Push message to Dingding
                        SendNotificationToDingDingJob ddJob = new SendNotificationToDingDingJob();
                        string MessageContent = $"<font color=#FF0000 face=黑体>告警内容: 缺货 </font> \n\n";
                        MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {orderNum}  \n\n";
                        MessageContent = MessageContent + $" ##### **保税仓 Barcode**:  {barcode}  \n\n";
                        MessageContent = MessageContent + $" ##### **魔法灯 SKU**:  {mfdsku}  \n\n";
                        MessageContent = MessageContent + $" ##### **计划重推时间 [北京时间]**:  { DateTime.UtcNow.AddHours(24 + 8).ToString("yyyy-MM-dd HH:mm:ss") }  \n\n";
                        MessageContent = MessageContent + $" ##### **API返回消息**:  {outOrderAddResponse.resultMessage}  ";

                        ddJob.SendBondedWarehouseChongQingAlert(MessageContent);

                        _backgroundJobManager.Enqueue<PushOrderToSiLu, ORDER_XT2WMS>(inputDto, BackgroundJobPriority.Normal, TimeSpan.FromDays(1));
                        return;
                    }
                    throw new Exception(outOrderAddResponse.resultMessage);
                }

                // update ERP orders_tatus = Packed when ordered succeed
                orderPack.status = (int)OrderPackStatusEnum.Packed;
                orderPack.pack_time = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);

                //TODO: 只有部分渠道需要一程面单操作
                if (useAltAccount)
                {
                    CreareOrderFirstExpress(orderNum, carrierId);
                }

                unitOfWork.Complete();
            }
        }


        public void CreareOrderFirstExpress(string orderNum,string carrierId) {

            var proxy = new BondedWarehouseProxy();

            GMOrderFirstExpressDTO gmOrderFirstExpressDto = new GMOrderFirstExpressDTO
            {
                ORDERS = new List<GMOrderExpressDTO>
                {
                    new GMOrderExpressDTO
                    {
                        ORDERNO = orderNum,
                        EXPRESSNO = carrierId
                    }
                }
            };

            var orderFirstExpressResponse = proxy.OrderFirstExpress(gmOrderFirstExpressDto, true);

            if (orderFirstExpressResponse.RESULTCODE != 0)
            {
                var notificationMessage = $"请注意！订单号：{orderNum} 丝路推送一程单号失败，原因：{orderFirstExpressResponse.RESULTMESSAGE}";
                
                // Push CMQ to Teambition_task Topic
                // TeambitionMessage teambitionmesage = new TeambitionMessage
                // {
                //     OrderNo = orderNum, //订单号
                //     Warehouse = WarehouseName,
                //     Title = orderFirstExpressResponse.RESULTMESSAGE,
                //     OutOfStockSKU = "",
                //     TriggerTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"),
                //     MessageContent = notificationMessage
                // };
                // TencentCMQProxy.PublishTopicMessage(TCConstants.CMQTopics.TOPIC_TEAMBITIONNOTIFICATION, teambitionmesage, messageTags, LogTags);

                SendNotificationToDingDingJob ddJob = new SendNotificationToDingDingJob();
                string MessageContent = $"<font color=#FF0000  face=黑体>告警内容: 丝路推送一程单号失败 </font> \n\n";
                MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {orderNum}  \n\n";
                MessageContent = MessageContent + $" ##### **API返回消息**:  {orderFirstExpressResponse.RESULTMESSAGE}  ";

                ddJob.SendBondedWarehouseChongQingAlert(MessageContent);

                throw new Exception(orderFirstExpressResponse.RESULTMESSAGE);
            }
        }

    }
}