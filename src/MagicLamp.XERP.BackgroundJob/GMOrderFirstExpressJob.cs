﻿using MagicLamp.PMS.DTOs.BondedWarehouse;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.XERP.BackgroundJob
{
    public class GMOrderFirstExpressJob : CommonBackgroundJob<string>
    {
        private HEERPDbContext _heerpContext;
        private SendNotificationToDingDingJob _dingdingNotificationJob;

        public GMOrderFirstExpressJob()
        {
            _heerpContext = new HEERPDbContextFactory().CreateDbContext();
            _dingdingNotificationJob = new SendNotificationToDingDingJob();
        }

        public override void Process(string orderno)
        {
            LogTags["method"] = "process";
            LogTags["trackid"] = orderno;

            try
            {
                CLSLogger.Info("重庆保税仓出库订单一程面单操作请求报文", orderno.ToJsonString(), LogTags);

                //find carrierid
                var orderPackInfo = _heerpContext.OrderPacks.FirstOrDefault(x => orderno.Contains("-") ? x.orderPackNo == orderno : x.orderID == orderno);
                string carrierId = orderPackInfo.carrierId.Trim();

                BondedWarehouseProxy bondedWarehouseProxy = new BondedWarehouseProxy();

                GMOrderFirstExpressDTO input = new GMOrderFirstExpressDTO
                {
                    ORDERS = new List<GMOrderExpressDTO>
                    {
                        new GMOrderExpressDTO
                        {
                            ORDERNO = orderno,
                            EXPRESSNO = carrierId
                        }
                    }
                };
                bool useIdCode2 = new List<int> {81, 82, 83}.Contains(orderPackInfo.exportFlag.GetValueOrDefault());
                var returnOutput = bondedWarehouseProxy.OrderFirstExpress(input, useIdCode2);

                if (returnOutput == null || returnOutput.RESULTCODE != 0)
                {
                    // _dingdingNotificationJob.SendBondedWarehouseChongQingAlert($"重庆保税仓出库订单一程面单操作失败，原因：{returnOutput.RESULTMESSAGE}");
                    throw new Exception($"重庆保税仓出库订单一程面单操作失败，原因：{returnOutput.RESULTMESSAGE}");
                }

            }
            catch (Exception ex)
            {
                CLSLogger.Error("重庆保税仓出库订单一程面单操作失败", ex, LogTags);
                throw ex;
            }
        }
    }
}
