﻿using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Proxy.BaXi.Models;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Abp.ObjectMapping;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.BaXi;
using MagicLamp.PMS.Infrastructure.Extensions;
using System.Threading.Tasks;
using MagicLamp.PMS.Domain;
using Abp.BackgroundJobs;
using NPOI.HSSF.Record;

namespace MagicLamp.XERP.BackgroundJob
{
    public class BaXiOrderProcessJob : CommonBackgroundJob<string>
    {
        private PMSDbContext pmsContext;
        private HEERPDbContext heerpContext;
        private IObjectMapper _objectMapper;
        private IBackgroundJobManager _backgroundJobManager;
        private BaXiClient baxiClient;
        private ProductDomainService productDomainService;
        private SendNotificationToDingDingJob ddJob;

        //八禧仓渠道代码
        private int exportFlagAUBaXi = 76;

        public BaXiOrderProcessJob(
            IObjectMapper objectMapper,
            IBackgroundJobManager backgroundJobManager
            ) : base()
        {
            pmsContext = new PMSDbContextFactory().CreateDbContext();
            heerpContext = new HEERPDbContextFactory().CreateDbContext();
            _objectMapper = objectMapper;
            _backgroundJobManager = backgroundJobManager;
            baxiClient = new BaXiClient();
            productDomainService = new ProductDomainService();
            ddJob = new SendNotificationToDingDingJob();
        }


        public override void Process(string inputDto)
        {
            ScanOrderPackPool(10);

            //process all pending orders
            ProcessBaXiOrderAsync(10);
        }

        /// <summary>
        /// SYNC BAXI Order Status
        /// SPark
        /// 23.09.2022
        /// </summary>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task SyncOrderDetailAsync(int pageSize)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = "BaXiOrderProcessJob",
                ["method"] = "SyncOrderDetail"
            };

            try
            {
                var listBaXiOrder = pmsContext.BaXiOrders.Where(x => x.Status == BaXiOrderStatusEnum.ordered.ToString()).ToList();

                CLSLogger.Info("八禧订单将同步1", $"listBaXiOrder明细：{listBaXiOrder.ToJsonString()}", clogTags);

                var totalCount = listBaXiOrder.Count();
                int totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;

                CLSLogger.Info("八禧订单将同步2", $"totalCount：{totalCount} totalPage: {totalPage} ", clogTags);
                //订单下所有包裹都出库的话，更新orderpack为出库，并记录freightid，以；分开
                for (int i = 0; i < totalPage; i++)
                {
                    var baxiOrderToProcess = listBaXiOrder.OrderBy(x => x.CreateTime).Skip(i * pageSize).Take(pageSize).ToList();

                    CLSLogger.Info("八禧订单将同步3", $"i：{i} detail: {baxiOrderToProcess.ToJsonString()} ", clogTags);


                    var listOpID = baxiOrderToProcess.Select(x => x.Opid).ToList();

                    CLSLogger.Info("八禧订单将同步状态一批", $"listopid为：{listOpID.ToJsonString()}", clogTags);

                    List<OrderPackEntity> listOrderPackNo = heerpContext.OrderPacks.Where(x => listOpID.Contains(x.opID)).ToList();

                    foreach (var order in baxiOrderToProcess)
                    {
                        await UpdateBaXiOrderStatusAsync(order, listOrderPackNo);
                    }
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("八禧订单同步状态出错", ex, clogTags);
            }
        }

        private async Task UpdateBaXiOrderStatusAsync(BaXiOrderEntity order, List<OrderPackEntity> listOrderPackNo)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = "UpdateBaXiOrderStatusAsync",
                ["method"] = "UpdateBaXiOrderStatusAsync"
            };


            var orderDetail = await baxiClient.GetOrderDetail(order.OrderPackNo);

            if (orderDetail.order == null)
            {
                CLSLogger.Info("八禧订单出库错误", $"OrderPackNo：{order.OrderPackNo}, 结果：{orderDetail.ToJsonString()} ", clogTags);
                return;
            }

            if (orderDetail.retCode == "1")
            {
                CLSLogger.Info("八禧订单出库错误", $"OrderPackNo：{order.OrderPackNo}, 结果：{orderDetail.ToJsonString()} ", clogTags);
                return;
            }
            

            StringBuilder listTrackingNo = new StringBuilder();

            bool isOutbound = true;

            
            foreach (var package in orderDetail.order.packages)
            {
                if (package.status != "已出库" || package.status == "已取消")
                {
                    isOutbound = false;
                }
                //else
                //{
                //    if (orderDetail.order.packages.Count() == 1)
                //    {
                //        listTrackingNo.Append($"{package.packageNo}");
                //    }
                //    else
                //    {
                //        listTrackingNo.Append($"{package.packageNo};");
                //    }
                //}
                if (package.status == "已出库" || package.status != "已取消")
                {
                    if (orderDetail.order.packages.Count() == 1)
                    {
                        listTrackingNo.Append($"{package.packageNo}");
                    }
                    else
                    {
                        listTrackingNo.Append($"{package.packageNo};");
                    }
                }
                

                // NOT Outbound Set Carrid ; fix；
                var orderPack = listOrderPackNo.FirstOrDefault(x => x.opID == order.Opid);
                orderPack.carrierId = listTrackingNo.ToString(); // Bound all carrierid to orderpack 20/03/2024  requirment from 榴莲@
                heerpContext.SaveChanges();
            }

            clogTags["orderpackno"] = order.OrderPackNo;


            if (isOutbound)
            {
                order.Status = BaXiOrderStatusEnum.done.ToString();
                order.UpdateTime = DateTime.UtcNow;

                if (order.ResponseData.IsNullOrEmpty() || order.ResponseData.Contains("orderNo已存在"))
                {
                    order.ResponseData = orderDetail.ToJsonString();
                }

                var orderPack = listOrderPackNo.FirstOrDefault(x => x.opID == order.Opid);

                CLSLogger.Info("八禧仓订单同步状态erp包裹", $"{orderPack.ToJsonString()}", clogTags);

                if (orderPack == null)
                {
                    //dingding notification
                    ddJob.SendBaXiAlert($"八禧仓更新订单失败，订单号为：{order.OrderPackNo}。失败详情：未找到erp对应包裹。", order.OrderPackNo);
                    return;
                }

                orderPack.freightId = "AUOD";

                if (listTrackingNo.Length > 500)
                {
                    ddJob.SendBaXiAlert($"八禧仓所分配物流号过长，无法记录。订单号为：{order.OrderPackNo}。物流号：{listTrackingNo}", order.OrderPackNo);
                }
                else
                {
                    orderPack.carrierId = listTrackingNo.ToString();
                }

                orderPack.status = (int)OrderPackStatusEnum.Outbound;
                orderPack.out_time = orderPack.out_time ?? DateTime.UtcNow.ConvertUTCTimeToNZTime();

                heerpContext.SaveChanges();
                pmsContext.SaveChanges();

                CLSLogger.Info("八禧订单同步状态完成，订单已出库", $"包裹号：{order.OrderPackNo}，明细：{orderDetail.ToJsonString()}", clogTags);
            }
            else
            {
                CLSLogger.Info("八禧订单同步状态完成，订单未出库", $"包裹号：{order.OrderPackNo}，明细：{orderDetail.ToJsonString()}", clogTags);
            }
        }

        private void ScanOrderPackPool(int pageSize)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = "BaXiOrderProcessJob",
                ["method"] = "ScanOrderPackPool"
            };

            StringBuilder logScan = new StringBuilder();

            try
            {
                var queryResult = heerpContext.OrderPacks
                .Where(x => x.exportFlag == exportFlagAUBaXi
                && x.status != (int)OrderPackStatusEnum.Outbound
                && x.freezeFlag == (int)OrderPackFreezeFlagEnum.未冻结
                && !x.orderPackNo.IsNullOrEmpty());

                var totalCount = queryResult.Count();
                int totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;

                logScan.Append($"待处理总数：{totalCount}，分页总页数：{totalPage}。");

                //List<OrderPackEntity> listOrderPackToProcess = new List<OrderPackEntity>();

                for (int i = 0; i < totalPage; i++)
                {
                    logScan.Append($"开始处理第{i}页。");

                    var listOrderPackToBeProcessed = queryResult.OrderBy(x => x.opt_time).Skip(i * pageSize).Take(pageSize).ToList();
                    var listOPID = listOrderPackToBeProcessed.Select(y => y.opID).ToList();


                    var existedBaXiOrder = pmsContext.BaXiOrders.Where(x => listOPID.Contains(x.Opid));

                    var listExistedBaXiOpid = existedBaXiOrder.Select(x => x.Opid).ToList();


                    var listOrderPackToAdd = listOrderPackToBeProcessed.Where(x => !listExistedBaXiOpid.Contains(x.opID)).ToList();

                    //add to BaXiOrder Table
                    AddToBaXiOrder(listOrderPackToAdd);

                    //listOrderPackToProcess.AddRange(listOrderPackToAdd);
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("八禧订单新增处理出错", ex, clogTags);
            }
        }

        private void AddToBaXiOrder(List<OrderPackEntity> listOrderPackToAdd)
        {
            foreach (var item in listOrderPackToAdd)
            {
                //save status and input info
                pmsContext.BaXiOrders.Add(new BaXiOrderEntity
                {
                    Opid = item.opID,
                    OrderID = item.orderID,
                    OrderPackNo = item.orderPackNo,
                    CreateTime = DateTime.UtcNow,
                    Status = BaXiOrderStatusEnum.pending.ToString()
                });

                pmsContext.SaveChanges();
            }
        }

        private async Task ProcessBaXiOrderAsync(int pageSize)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = "BaXiOrderProcessJob",
                ["method"] = "ProcessBaXiOrderAsync"
            };

            try
            {
                //scan pending baxiorder
                var queryResult = pmsContext.BaXiOrders.Where(x => x.Status == BaXiOrderStatusEnum.pending.ToString());

                var totalCount = queryResult.Count();
                int totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;

                for (int i = 0; i < totalPage; i++)
                {
                    var baxiOrdersToProcess = queryResult.OrderBy(x => x.CreateTime).Skip(i * pageSize).Take(pageSize).ToList();

                    var listOpid = baxiOrdersToProcess.Select(x => x.Opid).ToList();

                    //find related order pack 
                    List<OrderPackEntity> listOrderPackToProcess = heerpContext.OrderPacks.Where(x => listOpid.Contains(x.opID)).ToList();
                    List<OrderPackDetailEntity> listOrderPackDetail = heerpContext.OrderPackDetails.Where(x => listOpid.Contains(x.opid.Value)).ToList();

                    var listSKU = listOrderPackDetail.Select(x => x.sku).ToList();
                    var listProduct = productDomainService.GetProductsFromCache(listSKU);


                    foreach (var item in listOrderPackToProcess)
                    {
                        clogTags["orderpackno"] = item.orderPackNo;

                        if (item.freezeFlag != (int)OrderPackFreezeFlagEnum.未冻结 || item.status == (int)OrderPackStatusEnum.Outbound)
                        {
                            CLSLogger.Info("八禧订单处理时订单已在erp冻结或出库", $"包裹号为：{item.orderPackNo}，跳过处理。", clogTags);
                            continue;
                        }

                        var listCurrentOrderPackDetail = listOrderPackDetail.Where(x => x.opid == item.opID).ToList();

                        //list sku for current order pack
                        var listPackSKU = listCurrentOrderPackDetail.Select(x => x.sku).ToList();

                        //find product info

                        List<SimpleSKUDTO> listPackSKUDetail = new List<SimpleSKUDTO>();

                        int countNotFound = 0;

                        foreach (var detail in listCurrentOrderPackDetail)
                        {
                            if (detail.flag == 0)
                            {
                                //log.Append($"已在orderpackdetails中取消，");
                                continue;
                            }

                            //get barcode
                            var productInfo = listProduct.FirstOrDefault(x => x.SKU.StandardizeSKU() == detail.sku.StandardizeSKU());

                            if (productInfo == null || productInfo.Barcode1.IsNullOrEmpty())
                            {
                                countNotFound++;
                            }
                            else
                            {
                                var skuDetail = new SimpleSKUDTO
                                {
                                    SKU = productInfo.Barcode1,
                                    Qty = detail.qty
                                };

                                listPackSKUDetail.Add(skuDetail);
                            }
                        }

                        if (countNotFound > 0)
                        {
                            ddJob.SendBaXiAlert($"八禧仓创建订单失败，订单号为：{item.orderPackNo}。失败详情：订单所含商品信息缺失，请检查。SKU为：{listPackSKU.ToJsonString()}", item.orderPackNo);
                            continue;
                        }

                        //assemble BaXiOrderInfo
                        OrderPackDTO orderPackDTO = _objectMapper.Map<OrderPackDTO>(item);

                        BaXiClient client = new BaXiClient();

                        var orderInput = client.AssembleBaXiOrderInput(orderPackDTO, listPackSKUDetail);

                        //place order to BaXi
                        var response = await baxiClient.CreateOrder(orderInput, orderInput.orderNo);

                        var baxiOrder = baxiOrdersToProcess.FirstOrDefault(x => x.Opid == item.opID);

                        if (response.retCode == ((int)BaXiCreateOrderRetCodeEnum.成功).ToString()
                            || (response.retCode == ((int)BaXiCreateOrderRetCodeEnum.参数验证失败).ToString()
                            && response.retMsg == "orderNo已存在！"))
                        {
                            item.status = (int)OrderPackStatusEnum.Assigned;
                            item.assignTime = DateTime.Now; 
                           
                            StringBuilder listTrackingNo = new StringBuilder();

                            if (response.order != null)
                            {
                                foreach (var package in response.order.packages)
                                {
                                    if (response.order.packages.Count() == 1)
                                    {
                                        listTrackingNo.Append($"{package.packageNo}");
                                    }
                                    else
                                    {
                                        listTrackingNo.Append($"{package.packageNo};");
                                    }
                                }
                            }

                            item.freightId = "AUOD";

                            if (listTrackingNo.Length > 500)
                            {
                                ddJob.SendBaXiAlert($"八禧仓所分配物流号过长，无法记录。订单号为：{item.orderPackNo}。物流号：{listTrackingNo}", item.orderPackNo);
                            }
                            else
                            {
                                item.carrierId = listTrackingNo.ToString();
                            }

                            heerpContext.SaveChanges();


                            //save status and input info
                            if (baxiOrder != null)
                            {
                                baxiOrder.Status = BaXiOrderStatusEnum.ordered.ToString();
                                baxiOrder.OriginalInfo = orderInput.ToJsonString();
                                baxiOrder.ResponseData = response.ToJsonString();
                                baxiOrder.UpdateTime = DateTime.UtcNow;

                                _backgroundJobManager.Enqueue<LogBaXiOrderProductPrices, List<int>>(listCurrentOrderPackDetail.Select(x => x.opdid).ToList());
                            }
                            pmsContext.SaveChanges();
                        }
                        else
                        {
                            //dingding notification
                            ddJob.SendBaXiAlert($"八禧仓创建订单失败，订单号为：{orderInput.orderNo}。失败详情：{response.ToJsonString()}", orderInput.orderNo);

                            if (baxiOrder != null)
                            {
                                baxiOrder.Status = BaXiOrderStatusEnum.pending.ToString();
                                baxiOrder.OriginalInfo = orderInput.ToJsonString();
                                //baxiOrder.ResponseData = response.ToJsonString();
                                baxiOrder.UpdateTime = DateTime.UtcNow;
                            }
                            pmsContext.SaveChanges();

                            if (response.retCode == ((int)BaXiCreateOrderRetCodeEnum.商品库存不足或超过最大购买量).ToString())
                            {
                                //set orderpack to
                                item.status = (int)OrderPackStatusEnum.OutOfStock;
                                heerpContext.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("八禧订单处理出错", ex, clogTags);
                throw ex;
            }

        }
    }
}
