﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using MagicLamp.PMS.Domain;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace MagicLamp.XERP.BackgroundJob
{
    public class LockSKUWhenReservingJob : CommonBackgroundJob<LockSKUWhenReservingJobInputDTO>
    {
        private HEERPDbContext hEERPDbContext;
        public LockSKUWhenReservingJob()
        {
            hEERPDbContext = new HEERPDbContextFactory().CreateDbContext();
        }

        public override void Process(LockSKUWhenReservingJobInputDTO inputDto)
        {
            LogTags["orderid"] = inputDto.OrderID;

            StringBuilder logBuilder = new StringBuilder();

            try
            {
                string reqJson = inputDto.ToJsonString();
                CLSLogger.Info("LockSKUWhenReservingJob请求报文", reqJson, LogTags);


                if (inputDto == null || inputDto.ListSKU.IsNullOrEmpty())
                {
                    logBuilder.Append("input或ListSKU为空，不执行job");
                    return;
                }

                ProductDomainService productDomain = new ProductDomainService();

                int SecondsLocked = productDomain.FormalizeReserveSeconds(inputDto.SecondsReserved);

                if (!inputDto.SecondsReserved.HasValue)
                {
                    SecondsLocked = 172800;
                    logBuilder.Append($"预定时间为null，设为{SecondsLocked}秒。");
                }
                else
                {
                    logBuilder.Append($"预定时间为{SecondsLocked}秒。");
                }

                //get orderskulock based on orderid
                var listOrderSkuLocks = hEERPDbContext.OrderSkuLocks.Where(x => x.Orderid == inputDto.OrderID).ToList();

                foreach (var item in inputDto.ListSKU)
                {
                    var orderSkuLocksInfo = listOrderSkuLocks.FirstOrDefault(x => x.SKU == item.SKU);

                    //add
                    if (orderSkuLocksInfo == null)
                    {
                        OrderSkuLock orderSkuLocks = new OrderSkuLock
                        {
                            Orderid = inputDto.OrderID,
                            SKU = item.SKU,
                            Qty = item.Qty,
                            LockDuration = SecondsLocked,
                            Ctime = DateTime.UtcNow
                        };

                        hEERPDbContext.OrderSkuLocks.Add(orderSkuLocks);
                        logBuilder.Append($"新增一个Orders_Sku_Lock，详情：{orderSkuLocks.ToJsonString()}");
                    }
                    //update
                    else
                    {
                        orderSkuLocksInfo.Qty = item.Qty;
                        orderSkuLocksInfo.LockDuration = SecondsLocked;
                        orderSkuLocksInfo.Ctime = DateTime.UtcNow;
                        logBuilder.Append($"更新一个Orders_Sku_Lock，详情：{orderSkuLocksInfo.ToJsonString()}");
                    }
                }

                hEERPDbContext.SaveChanges();

            }
            catch (Exception ex)
            {
                CLSLogger.Error("LockSKUWhenReservingJob失败", ex, LogTags);
            }
            finally
            {
                CLSLogger.Info("LockSKUWhenReservingJob结束运行", logBuilder.ToString(), LogTags);
            }

        }
    }
}
