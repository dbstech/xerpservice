using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using MagicLamp.PMS.BaXi;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy;

namespace MagicLamp.XERP.BackgroundJob
{
    public class LogBaXiOrderProductPrices : AsyncBackgroundJob<List<int>>, ITransientDependency
    {
        private IRepository<ProductEntity, string> _productRepository;
        private IRepository<OrderPackDetailEntity, int> _orderPackDetailsRepository;
        private IRepository<OrderPackEntity, int> _orderPackRepository;
        private IRepository<BaXiOrderProductPrice, int> _baXiOrderProductPriceRepository;
        private IUnitOfWorkManager _unitOfWorkManager;
        private IBackgroundJobManager _backgroundJobManager;

        public LogBaXiOrderProductPrices(
            IRepository<ProductEntity, string> productRepository,
            IRepository<OrderPackDetailEntity, int> orderPackDetailsRepository,
            IRepository<OrderPackEntity, int> orderPackRepository,
            IRepository<BaXiOrderProductPrice, int> baXiOrderProductPriceRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IBackgroundJobManager backgroundJobManager
        )
        {
            _productRepository = productRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _orderPackDetailsRepository = orderPackDetailsRepository;
            _orderPackRepository = orderPackRepository;
            _baXiOrderProductPriceRepository = baXiOrderProductPriceRepository;
            _backgroundJobManager = backgroundJobManager;
        }
        protected override async Task ExecuteAsync(List<int> opdids)
        {
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                var orderPackDetails = _orderPackDetailsRepository.GetAll().Where(x => opdids.Contains(x.opdid)).ToList();
                //get opidlist
                var opids = orderPackDetails.Select(x => x.opid).ToList();
                //get orderpak
                var OrderpackDict = _orderPackRepository.GetAll().Where(x => opids.Contains(x.opID)).ToDictionary(x => x.opID.ToString(), x => x.orderPackNo);

                var skus = orderPackDetails.Select(x => x.sku).ToList();
                var skuUpcDict = _productRepository.GetAll().Where(x => skus.Contains(x.SKU)).ToDictionary(x => x.SKU, x => x.Barcode1);
                var upcs = skuUpcDict.Select(x => x.Value).ToList();
                var existingPrices = _baXiOrderProductPriceRepository.GetAll().Where(x => opdids.Contains(x.Opdid)).ToList();

                BaXiClient client = new BaXiClient();
                List<BaXiProduct> baXiProducts = await client.ProductsQuery(upcs);
                orderPackDetails.ForEach(x =>
                {
                    if (existingPrices.Any(y => y.Opdid == x.opdid) == false)
                    {
                        var upc = skuUpcDict[x.sku];
                        var orderpackno = OrderpackDict[x.opid.ToString()];

                        var baxiProduct = baXiProducts.FirstOrDefault(z => z.Upc == upc);
                        if (baxiProduct == null)
                        {
                            // TODO: send dingtalk notification and run job again 1 day later
                            //throw new Exception($"无法从八禧仓找到拥有该upc的产品: {upc}");
                            StringBuilder UnFindUPCMSG = new StringBuilder();
                            UnFindUPCMSG.Insert(0, "##### 无法从八禧仓找到拥有该upc的产品： \n\n");

                            List<string> staffToNotify = new List<string> { "+86-15953203327" };

                            string ddTokenBaXi = "34adddda7c173e972431bbd7a0f0f3ffbebb8746dbfdc66c4191f2e168e098ef";
                            string signature = "SECe480bad4be94d0ce5b516f826141d46a6130a54be731558507d68a583f675fb0";
                            DDNotificationProxy.MarkdownNotification($"八禧仓监控报警", UnFindUPCMSG.ToString() + upc + "\n\n 下单的包裹Orderpack:" + orderpackno, ddTokenBaXi, signature, staffToNotify, 100000);
                        }
                        else
                        {
                            _baXiOrderProductPriceRepository.Insert(new BaXiOrderProductPrice
                            {
                                Opdid = x.opdid,
                                Opid = x.opid ?? 0,
                                Sku = x.sku,
                                Upc = upc,
                                Price = Convert.ToDecimal(baxiProduct.Price),
                                Qty = x.qty
                            });
                        }
                        
                    }
                });

                unitOfWork.Complete();
            }
        }
    }
}