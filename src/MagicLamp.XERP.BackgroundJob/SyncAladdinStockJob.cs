﻿using MagicLamp.PMS.Domain;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.ThirdPart;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob.Common;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using NPOI.Util;
using Org.BouncyCastle.Crypto.Tls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

namespace MagicLamp.XERP.BackgroundJob
{

    /// <summary>
    /// 1：AU和NZ产品一起推送，Job半小时运行一次
    /// 2：同步时增加3个字段，originalstock（对应表里的qty_available字段），originalhhstock（对应表里的qty_hh字段），hhstock（逻辑计算过后的HH库存）,
    ///     originalbestbefore（对应表里的expired_date字段），originalhhbestbefore（对应表里的expiry_date_hh字段）
    /// 3：库存判断逻辑：
    ///1）如该SKU同步过HH库存，HH不共享魔法灯库存。MFD优先使用自家库存，当MFD库存不足且SKU为A类品时可共享HH库存。
    ///2）如HH未同步过库存，则使用MFDStock，此时HHStock，MFDStock一致
    /// </summary>
    public class SyncAladdinStockJob : CommonBackgroundJob<List<string>>
    {
        public override void Process(List<string> skus)
        {
            StringBuilder logBuilder = new StringBuilder();
            //get method caller info
            StackTrace stackTrace = new StackTrace();
            var caller = stackTrace.GetFrame(1).GetMethod();
            var methodName = caller.Name;
            var callerName = caller.DeclaringType.Name;
            logBuilder.Append($"调用接口：{callerName}。方法：{methodName}。");


            //log when input is an empty list
            if (skus != null && !skus.Any())
            {
                logBuilder.Append($"input为空list。");
            }

            SyncStock(logBuilder, skus);
        }

        /// <summary>
        /// Sync stock value of SKU in Aladdin to 188 and YangXiaoFan
        /// </summary>
        /// <param name="pageSize"></param>
        [SkipConcurrentExecution(60 * 120)]
        public void SyncStock(StringBuilder logBuilder, List<string> inputSkus = null)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>();
            clogTags["jobid"] = Guid.NewGuid().ToString();
            clogTags["type"] = "SyncAlladinStockJob";

            int total = 0;
            int pageSize = 200;


            try
            {
                HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
                PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();

                bool isFullSync = inputSkus == null ? true : false;

                int totalPage = GetTotalPage(inputSkus, isFullSync, pageSize, pmsContext);


                for (int i = 0; i < totalPage; i++)
                {
                    List<ProductEntity> listProductInfo;

                    //get product info
                    if (isFullSync)
                    {
                        listProductInfo = pmsContext.Products.OrderByDescending(x => x.SKU).Where(x => x.ActiveFlag == true).Select(x => new ProductEntity
                        {
                            SKU = x.SKU,
                            IsHHPurchase = x.IsHHPurchase
                        }).Skip(i * pageSize).Take(pageSize).ToList();
                    }
                    else
                    {
                        var currentPageSkus = inputSkus.OrderByDescending(x => x).Skip(i * pageSize).Take(pageSize).ToList();
                        listProductInfo = pmsContext.Products.Where(x => currentPageSkus.Contains(x.SKU) && x.ActiveFlag == true).Select(x => new ProductEntity
                        {
                            SKU = x.SKU,
                            IsHHPurchase = x.IsHHPurchase
                        }).ToList();
                    }

                    //refresh lock qty
                    //new RefreshSKULockQtyJob().Process(skus);


                    List<SyncAladdinStockDTO> listStockForSync = AssembleListSKUStockForSync(listProductInfo, heERPContext);

                    // logging
                    Dictionary<string, string> clogTagsERP = new Dictionary<string, string>();
                    clogTagsERP["jobid"] = Guid.NewGuid().ToString();
                    clogTagsERP["type"] = "同步188数据-Log";
                    CLSLogger.Info("同步188数据", $"Data：{listStockForSync.ToJsonString()} ", clogTagsERP);


                    //同步给188
                    EShopProxy eShop = new EShopProxy();
                    eShop.SyncStockTo188(listStockForSync);
                    logBuilder.Append("库存同步188完成。");

                    //同步给洋小范 Stoped by Spark 2022.12.04
                    //YangXiaoFanSyncStockPostDTO postData = new YangXiaoFanSyncStockPostDTO();
                    //postData.products = listStockForSync;

                    //YangXiaoFanProxy yxfProxy = new YangXiaoFanProxy();
                    //yxfProxy.SyncStock(postData);
                    //logBuilder.Append("库存同步洋小范完成。");

                    total += listStockForSync.Count;
                }
            }
            catch (Exception ex)
            {
                clogTags["method"] = "SyncStock";
                CLSLogger.Error("SyncAladdinStockJob.SyncStock失败", ex, clogTags);
            }
            finally
            {
                clogTags["method"] = "SyncStock";
                CLSLogger.Info("SyncAladdinStockJob.SyncStock完成", $"同步数量：{total}。Log：{logBuilder.ToString()}", clogTags);
            }
        }

        private int GetTotalPage(List<string> inputSkus, bool isFullSync, int pageSize, PMSDbContext pmsContext)
        {
            //get count
            int totalCount;

            if (isFullSync)
            {
                totalCount = pmsContext.Products.Where(x=>x.ActiveFlag == true).Count();
            }
            else
            {
                totalCount = inputSkus.Count();
            }

     

            int totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;

            return totalPage;
        }

        private List<SyncAladdinStockDTO> AssembleListSKUStockForSync(List<ProductEntity> listProductInfo, HEERPDbContext heERPContext)
        {
            PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();

            bool syncecinx;

            List<SyncAladdinStockDTO> listStockForSync = new List<SyncAladdinStockDTO>();

            //products for cache refresh
            List<ProductStockVO> productStockVOs = new List<ProductStockVO>();

            List<string> listSKUToProcess = listProductInfo.Select(x => x.SKU).ToList();

            ProductDomainService productDomainService = new ProductDomainService();

            StockShareDomainService stockShareDomainService = new StockShareDomainService();
            StockShareConfigVO configStockShare = stockShareDomainService.GetStockShareConfig();

            //get sku stock lock for hh
            List<OrderSKULockForHHEntity> listAllSKULockHH = productDomainService.GetCurrentSKUStockLockForHH(listSKUToProcess);

            //get stock info
            List<APIStockDTO> currentListApiStock = heERPContext.APIStocks.Where(x => listSKUToProcess.Contains(x.SKU))
                .Select(x => new APIStockDTO
                {
                    SKU = x.SKU,
                    Qty_available = x.Qty_available,
                    Qty_hh = x.Qty_hh,
                    Expired_date = x.Expired_date,
                    Expiry_date_hh = x.Expiry_date_hh,
                    Qty_locked = x.Qty_locked
                }).ToList();

            foreach (var item in currentListApiStock)
            {
                var dto = GetCalculatedSKUStock(item, listProductInfo, listAllSKULockHH, productDomainService, configStockShare);
                listStockForSync.Add(dto);

                syncecinx = pmsContext.Products.Where(x => x.SyncToEcinx == true && !string.IsNullOrWhiteSpace(x.ECinxSKU) && x.ActiveFlag == true && x.PurchasePeriodCode == "3rdParty" && x.SKU == item.SKU).ToList().Count > 0 ? true : false;


                //try
                //{


                //}
                //catch (Exception ex)
                //{
                //    Dictionary<string, string> clogTags = new Dictionary<string, string>();
                //    clogTags["jobid"] = Guid.NewGuid().ToString();
                //    clogTags["type"] = "SyncStockJob";
                //    CLSLogger.Info("SyncError", $"SKU:{item.SKU} Mesage:{ex.Message}", clogTags);
                //    throw;
                //}

                int qtyhh = 0;
                if (item.Qty_hh != null)
                {
                    qtyhh = int.Parse(item.Qty_hh.ToString());
                }
                else
                {
                    qtyhh = 0;
                }

               ProductStockVO productStockVO = new ProductStockVO
                {
                    Sku = item.SKU,
                    Qty_available = item.Qty_available.Value,
                    ExpiredDate = item.Expired_date,
                    OriginalBestBefore = item.Expired_date,
                    OriginalHHBestBefore = item.Expiry_date_hh,
                    OriginalStock = item.Qty_available.Value,
                    OriginalHHStock = item.Qty_hh,
                    LockQty = item.Qty_locked.Value,
                    //spark add stock 19/06/2022
                    Stock = item.SKU.StartsWith("AUBXD") ? item.Qty_available.Value - 2 : 
                            syncecinx ? item.Qty_available.Value + qtyhh : item.Qty_available.Value,
                };
                productStockVOs.Add(productStockVO);
            }

            //refresh cache stock
            productDomainService.UpdateProductStockCache(productStockVOs.ToArray());

            return listStockForSync;
        }

        /// <summary>
        /// 算出同步给前台的库存数，魔法灯和HH的库存共同使用，保质期用最差的那一方
        /// </summary>
        /// <param name="item"></param>
        /// <param name="listProductInfo"></param>
        /// <param name="listAllSKULockHH"></param>
        /// <param name="productDomainService"></param>
        /// <param name="configStockShare"></param>
        /// <returns></returns>
        public SyncAladdinStockDTO GetCalculatedSKUStock(APIStockDTO item, List<ProductEntity> listProductInfo, List<OrderSKULockForHHEntity> listAllSKULockHH, ProductDomainService productDomainService, StockShareConfigVO configStockShare)
        {

            try
            {
                PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();


                var stockMFD = item.Qty_available.Value;
                var expiryValue = item.Expired_date;

                var currentAvailableHHStock = item.Qty_hh.HasValue ? productDomainService.GetAvailableSKUHHStock(item.SKU, item.Qty_hh.Value, listAllSKULockHH) : 0;

                var stockHH = item.Qty_hh.HasValue ? currentAvailableHHStock : item.Qty_available;

                var expiryValueHH = pmsContext.Products.Where(x => x.SyncToEcinx == true && !string.IsNullOrWhiteSpace(x.ECinxSKU) && x.ActiveFlag == true && x.SKU == item.SKU).ToList().Count > 0 ? item.Expiry_date_hh : item.Expired_date;


                //determine whether sku is stock share
                if (configStockShare.EnableStockShare && configStockShare.TestSKU.Contains(item.SKU.Trim(), StringComparer.OrdinalIgnoreCase))
                {
                    var productInfo = listProductInfo.FirstOrDefault(x => x.SKU.StandardizeSKU() == item.SKU.StandardizeSKU());

                    if (item.Qty_hh.HasValue)
                    {
                        expiryValueHH = item.Expiry_date_hh;
                    }

                    if (productInfo.PurchasePeriodCode != "virtualStock")
                    {
                        //use both mfd and hh stock
                        if (currentAvailableHHStock > 0 && productInfo != null && productInfo.IsHHPurchase.HasValue && productInfo.IsHHPurchase.Value == true)
                        {
                            stockMFD += currentAvailableHHStock;

                            //get the worst expiry date
                            DateTime temp_Expiry_date_hh = DateTime.MinValue;
                            DateTime temp_Expired_date = DateTime.MinValue;

                            if (item.Expired_date.IsNullOrEmpty())
                            {
                                expiryValue = item.Expiry_date_hh;
                            }
                            else if (DateTime.TryParse(item.Expiry_date_hh, out temp_Expiry_date_hh) && DateTime.TryParse(item.Expired_date, out temp_Expired_date)
                                && temp_Expiry_date_hh < temp_Expired_date)
                            {
                                expiryValue = temp_Expiry_date_hh.ToString("MM/yyyy");
                            }
                        }
                        //MFD库存不足且SKU为A类品时
                        else if (item.Qty_hh.HasValue && item.Qty_available.HasValue && item.Qty_available <= 0
                            && productInfo != null && productInfo.IsHHPurchase.HasValue && productInfo.IsHHPurchase.Value == true)
                        {
                            //use qty and expiry date from hh
                            if (item.Qty_hh > item.Qty_available)
                            {
                                stockMFD = currentAvailableHHStock;
                                expiryValue = item.Expiry_date_hh;
                            }
                        }
                    }
                }
                //如HH未同步过库存，则使用MFDStock，此时HHStock，MFDStock一致
                else
                {
                    stockHH = stockMFD;
                }



                var listSKULockHH = pmsContext.Products.Where(x => x.SKU == item.SKU);
                //bool syncecinx = Convert.ToBoolean(listSKULockHH.FirstOrDefault().SyncToEcinx);

                int? _original_hh_stock = item.Qty_hh.HasValue ? productDomainService.GetAvailableSKUHHStock(item.SKU, item.Qty_hh.Value, listAllSKULockHH) : 0;


                DateTime temp_Expiry_date_hh1 = DateTime.MinValue;
                DateTime temp_Expired_date1 = DateTime.MinValue;
                if (DateTime.TryParse(item.Expiry_date_hh, out temp_Expiry_date_hh1) && DateTime.TryParse(expiryValue, out temp_Expired_date1)
                                && temp_Expiry_date_hh1 < temp_Expired_date1)
                {
                    expiryValue = item.Expiry_date_hh;
                }

                DateTime ecinx_Expiry_date = DateTime.MinValue;



                // Spark 增加保质期取值逻辑；
                // 1. 如果 HH 库存为0, MFD库存不为0，直接使用MFD的保质期，不比较最差保质期; 2022.12.23
                if (_original_hh_stock <= 0 && stockMFD != 0)
                {
                    expiryValue = item.Expired_date;
                }
                // 2. 如果 HH 库存为0, MFD 也为0，提示：请以实际到货为准  2022.12.23
                if (_original_hh_stock == 0 && stockMFD == 0)
                {
                    expiryValue = "请以实际到货为准";
                }
                // 3.如果 MFD 库存为0, HH 不为0，使用HH的库存  2022.12.23
                if (stockMFD == 0 && _original_hh_stock != 0)
                {
                    expiryValue = item.Expiry_date_hh;
                }
                //4. 两者都有库存，就用最差保质期推送给188 ； 2022.12.23


                //格式化Ecinx Expiredate 
                string EcinxExpireDate = "";
                // 如果Ecinx库存为空，用虚拟库存的保质期
                if (item.Expiry_date_hh.IsNullOrEmpty())
                {
                    EcinxExpireDate = item.Expired_date;
                }
                else
                {
                    DateTime.TryParse(item.Expiry_date_hh, out ecinx_Expiry_date);
                    EcinxExpireDate = ecinx_Expiry_date.ToString("MM/yyyy");
                }

                //assemble sku stock info for sync
                SyncAladdinStockDTO dto = new SyncAladdinStockDTO
                {
                    sku = item.SKU,
                    //八禧仓sku，减2个同步
                    stock = item.SKU.StartsWith("AUBXD") ? stockMFD - 2 :
                        pmsContext.Products.Where(x => x.SyncToEcinx == true && !string.IsNullOrWhiteSpace(x.ECinxSKU) && x.ActiveFlag == true && x.PurchasePeriodCode == "3rdParty" && x.SKU == item.SKU).ToList().Count > 0 ?
                            (stockMFD + int.Parse(_original_hh_stock.ToString())) : stockMFD,
                    best_before = pmsContext.Products.Where(x => x.SyncToEcinx == true && !string.IsNullOrWhiteSpace(x.ECinxSKU) && x.ActiveFlag == true && x.SKU == item.SKU).ToList().Count > 0 ?
                            EcinxExpireDate : expiryValue,
                    hh_best_before = expiryValueHH,
                    original_stock = item.Qty_available.Value,
                    original_hh_stock = _original_hh_stock,
                    hh_stock = stockHH,
                    original_best_before = item.Expired_date,
                    original_hh_best_before = item.Expiry_date_hh
                };


                return dto;
            }
            catch (Exception ex )
            {
                // logging
                Dictionary<string, string> clogTags = new Dictionary<string, string>();
                clogTags["jobid"] = Guid.NewGuid().ToString();
                clogTags["type"] = "SyncadinStock";
                CLSLogger.Error("SyncadinStock", $"result：{ex.ToJsonString()} ", clogTags);
                
                SyncAladdinStockDTO dto = new SyncAladdinStockDTO();

                return dto;

            }

        }
    }
}
