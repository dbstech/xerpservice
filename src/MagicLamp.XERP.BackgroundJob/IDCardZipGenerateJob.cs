﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using MagicLamp.PMS.DTO.Enums;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Hangfire;

namespace MagicLamp.XERP.BackgroundJob
{
    public class IDCardZipGenerateJob : BackgroundJob<GenerateIDCardZipInputDTO>, ITransientDependency
    {

        public IDCardZipGenerateJob()
        {


        }

        [AutomaticRetry(Attempts = 3)]
        [UnitOfWork]
        public override void Execute(GenerateIDCardZipInputDTO inputDto)
        {
            //Clear old files first
            DeleteOldFiles();

            if (inputDto == null || inputDto.TaskID == Guid.Empty)
            {
                return;
            }

            string ddToken = "7f8db451bf15f9ee3ec3c591aebf78be4db86fcda93a8c71d426c5c6eff76515";

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["taskid"] = inputDto.TaskID.ToString();
            tags["type"] = "dowloadidcardsjob";

            CLSLogger.Info("开始下载任务", string.Empty, tags);
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            PMSDbContext pmsDbContext = new PMSDbContextFactory().CreateDbContext();
            var orderIDCardRepository = pmsDbContext.OrderIDCards;
            var idcardRepository = pmsDbContext.IDCards;
            var downloadRecordsRepository = pmsDbContext.DownloadRecords;

            var downloadRecordEntity = downloadRecordsRepository.FirstOrDefault(x => x.TaskID == inputDto.TaskID);

            if (downloadRecordEntity == null)
            {
                CLSLogger.Info("未找到对应下载记录", string.Empty, tags);
                return;
            }

            try
            {
                CLSLogger.Info("开始查询订单身份证信息", inputDto.ToJsonString(), tags);
                var query = orderIDCardRepository.Where(x => !string.IsNullOrEmpty(x.IDCardNo));

                if (!string.IsNullOrWhiteSpace(inputDto.Name))
                {
                    var names = inputDto.Name.SplitByCommonSymbol();
                    query = query.Where(x => names.Contains(x.RecevierName));
                }
                if (!string.IsNullOrWhiteSpace(inputDto.CourierID))
                {
                    var courierIds = inputDto.CourierID.SplitByCommonSymbol();
                    query = query.Where(x => courierIds.Contains(x.CarrierID));
                }
                if (inputDto.ExportFlag > 0)
                {
                    query = query.Where(x => x.ExportFlag == inputDto.ExportFlag);
                }
                if (!string.IsNullOrWhiteSpace(inputDto.OrderID))
                {
                    var orderIds = inputDto.OrderID.SplitByCommonSymbol();
                    query = query.Where(x => orderIds.Contains(x.OrderID));
                }
                if (!string.IsNullOrWhiteSpace(inputDto.IDCard))
                {
                    query = query.Where(x => x.IDCardNo == inputDto.IDCard);
                }
                if (inputDto.Verification.HasValue)
                {
                    query = query.Where(x => x.Verification == inputDto.Verification);
                }
                if (inputDto.UpdateBeginTime > DateTime.MinValue)
                {
                    query = query.Where(x => x.IDCardUpdateTime >= inputDto.UpdateBeginTime);
                }
                if (inputDto.EndBeginTime > DateTime.MinValue)
                {
                    query = query.Where(x => x.IDCardUpdateTime <= inputDto.EndBeginTime);
                }
                if (!string.IsNullOrEmpty(inputDto.FreightID))
                {
                    query = query.Where(x => x.FreightID == inputDto.FreightID);
                }
                if (inputDto.OrderBeginTime > DateTime.MinValue)
                {
                    query = query.Where(x => x.CreationTime >= inputDto.OrderBeginTime);
                }
                if (inputDto.OrderEndBeginTime > DateTime.MinValue)
                {
                    query = query.Where(x => x.CreationTime <= inputDto.OrderEndBeginTime);
                }

                var orderIDCards = query.Select(x => new
                {
                    IDCardNo = x.IDCardNo,
                    Name = x.RecevierName,
                    URLBack = x.URLBack,
                    URLFont = x.URLFont,
                    CarrierID = x.CarrierID,
                    FreightID = x.FreightID
                }).ToList();

                if (orderIDCards == null || orderIDCards.Count == 0)
                {
                    CLSLogger.Error("未查询到订单身份证信息", inputDto.ToJsonString(), tags);
                    return;
                }

                int idcardCount = orderIDCards.Select(x => x.IDCardNo).Distinct().Count();

                stopwatch.Stop();
                CLSLogger.Info("下载查询信息耗时", stopwatch.ElapsedMilliseconds.ToString() + "ms", tags);

                if (idcardCount > 1000)
                {
                    CLSLogger.Error("生成身份证包裹数量过多", idcardCount.ToString(), tags);

                    StringBuilder ddMessage = new StringBuilder();
                    ddMessage.Append("下载任务失败，身份证数量超过1000条，请缩小查询范围 \n\n");
                    ddMessage.Append("----------------以下是导出条件--------------- \n\n");
                    ddMessage.Append(inputDto.ToJsonString());
                    DDNotificationProxy.MarkdownNotification($"下载任务失败，身份证数量超过1000条，请缩小查询范围", ddMessage.ToString(), ddToken);

                    downloadRecordEntity.Status = DownloadTaskStatusEnum.FAILED.ToString();
                    pmsDbContext.SaveChanges();
                    return;
                }

                //download from aliyunoss and save
                Ultities.CreateDirectory("/App_Data/IDCards");
                List<string> idCardImagePaths = new List<string>();
                List<string> ossKeys = new List<string>();
                List<string> needCarrierIDExpress = new List<string> { "auexpress" };

                StringBuilder log = new StringBuilder();
                foreach (var item in orderIDCards)
                {
                    stopwatch.Restart();
                    System.Drawing.Image imageBack = null;
                    System.Drawing.Image imageFront = null;

                    if (!ossKeys.Contains(item.URLFont))
                    {
                        imageFront = AliyunOSSProxy.Instance.DownloadImage(item.URLFont);
                        ossKeys.Add(item.URLFont);
                    }

                    if (!ossKeys.Contains(item.URLBack))
                    {
                        imageBack = AliyunOSSProxy.Instance.DownloadImage(item.URLBack);
                        ossKeys.Add(item.URLBack);
                    }

                    string number = string.Empty;

                    if (needCarrierIDExpress.Contains(item.FreightID))
                    {
                        number = item.CarrierID;
                    }
                    else
                    {
                        number = item.IDCardNo;
                    }

                    if (imageFront != null)
                    {
                        List<string> paths = new List<string>() {
                        "App_Data","IDCards",string.Format("{0}_{1}_a{2}",number,item.Name,imageFront.RawFormat.GetFileExtension()) };
                        string frontPath = SaveImage(paths, imageFront);
                        idCardImagePaths.Add(frontPath);
                    }

                    if (imageBack != null)
                    {
                        List<string> paths = new List<string>() {
                        "App_Data","IDCards",string.Format("{0}_{1}_b{2}",number,item.Name,imageBack.RawFormat.GetFileExtension())};
                        string backPath = SaveImage(paths, imageBack);
                        idCardImagePaths.Add(backPath);
                    }
                    stopwatch.Stop();
                    log.AppendFormat("{0}-{1} 下载耗时：{2} ms；", item.IDCardNo, item.Name, stopwatch.ElapsedMilliseconds);
                }

                CLSLogger.Info("身份证下载耗时", log.ToString(), tags);

                if (idCardImagePaths.Count == 0)
                {
                    CLSLogger.Info("此批次下未获取到身份证信息", string.Empty, tags);
                    return;
                }

                stopwatch.Restart();
                //Generate zip file
                string zipDirectory = string.Format("App_Data/IDCardZips/{0}", inputDto.TaskID);
                string physicalDirectoryPath = Ultities.CreateDirectory(zipDirectory);

                string zipFileName = string.Format("{0}-IDCard.zip", DateTime.Now.ToString("yyyyMMddHHmmss"));
                string zipFilePath = Path.Combine(physicalDirectoryPath, zipFileName);
                bool result = SharpZipUtility.ZipFiles(zipFilePath, idCardImagePaths);

                stopwatch.Stop();
                CLSLogger.Info("压缩耗时", stopwatch.ElapsedMilliseconds.ToString() + "ms", tags);

                if (result)
                {
                    downloadRecordEntity.Status = DownloadTaskStatusEnum.SUCCESS.ToString();
                    downloadRecordEntity.FilePath = zipFilePath;
                    downloadRecordEntity.UserID = inputDto.UserID;
                    StringBuilder ddMessage = new StringBuilder();
                    ddMessage.Append($"# 您的下载任务已完成 [点击下载](https://xerp-service.aladdin.nz/api/services/app/IDCard/DownloadIDCardZip?TaskId={inputDto.TaskID}) \n\n");
                    ddMessage.Append("----------------以下是导出条件--------------- \n\n");
                    ddMessage.Append(inputDto.ToJsonString());
                    DDNotificationProxy.MarkdownNotification($"{zipFileName}下载任务已完成", ddMessage.ToString(), ddToken);
                }
                else
                {
                    downloadRecordEntity.Status = DownloadTaskStatusEnum.FAILED.ToString();
                    DDNotificationProxy.MarkdownNotification($"{zipFileName}下载任务意外失败，请重试", $"# {zipFileName}下载任务意外失败，请重试", ddToken);
                }
                pmsDbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                CLSLogger.Error("Generate IDCard zip has exception occurred", ex, tags);
                DDNotificationProxy.MarkdownNotification("下载任务意外失败，请重试", "# 下载任务意外失败，请重试", ddToken);
                downloadRecordEntity.Status = DownloadTaskStatusEnum.FAILED.ToString();
                pmsDbContext.SaveChanges();
                throw ex;
            }
        }


        protected virtual string SaveImage(List<string> pathArr, System.Drawing.Image image)
        {
            pathArr.Insert(0, Directory.GetCurrentDirectory());
            string path = Path.Combine(pathArr.ToArray());
            path = path.Replace("/","_").ConvertToUTF8();
            image.Save(path);
            image.Dispose();
            return path;
        }

        protected void DeleteOldFiles()
        {
            try
            {
                string idCardImagesDirectory = Ultities.GetPhysicalPath("/App_Data/IDCards");
                string idCardZIPDirectory = Ultities.GetPhysicalPath("/App_Data/IDCardZips");

                int deletedImageCount = 0;
                int deletedZIPCount = 0;

                DateTime specificDate = DateTime.Now.Date;

                Directory.GetFiles(idCardImagesDirectory)
                  .Select(f => new FileInfo(f))
                  .Where(f => f.LastAccessTime < specificDate)
                  .ToList()
                  .ForEach(x =>
                  {
                      deletedImageCount++;
                      x.Delete();
                  });

                Directory.GetDirectories(idCardZIPDirectory)
                .Select(x => new DirectoryInfo(x))
                .Where(f => f.LastAccessTime < specificDate)
                .ToList()
                .ForEach(x =>
                {
                    deletedZIPCount++;
                    x.Delete(true);
                });

                CLSLogger.Info("ClearIDCardDataSuccessed", $"deletedImageCount:{deletedImageCount}, deletedZIPCount:{deletedZIPCount}");
            }
            catch (Exception ex)
            {
                CLSLogger.Error("ClearIDCardDataOccuredException", ex);
            }
        }

    }
}
