﻿using Hangfire;
using MagicLamp.XERP.BackgroundJob.Common;
using System;
using System.Collections.Generic;
using System.Text;
using MagicLamp.Flux.API.Models;
using MagicLamp.PMS.Proxy;
using Abp.UI;
using MagicLamp.PMS.Infrastructure.Extensions;


namespace MagicLamp.XERP.BackgroundJob
{
    public class SyncProductToFluxJob : CommonBackgroundJob<List<Product>>
    {

        public SyncProductToFluxJob()
        {

        }

        [Queue("critical")]
        public override void Process(List<Product> inputDtos)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>();
            logTags["type"] = "syncspecificskutofluxNew";

            FluxResponse fluxResponse = FluxProxy.PushSKUData(inputDtos.ToArray());

            if (fluxResponse == null || fluxResponse.Response == null || fluxResponse.Response.@return == null
                || fluxResponse.Response.@return.returnFlag != "1")
            {
                CLSLogger.Error("syncspecificskutofluxNew_同步产品信息发生异常", $"富勒返回异常，response：{fluxResponse.ToJsonString()}", logTags);
                //throw new UserFriendlyException("同步产品信息发生异常", "富勒返回异常");
            }
            else
            {
                CLSLogger.Info("同步XERP产品信息到Flux系统", "参数: " +inputDtos.ToJsonString() + " 返回结果: " + fluxResponse.ToJsonString(), logTags);
            }
        }
    }
    
}
