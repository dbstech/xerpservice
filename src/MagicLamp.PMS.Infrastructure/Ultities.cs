﻿using Abp.Dependency;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace MagicLamp.PMS.Infrastructure
{
    public class Ultities
    {
        /// <summary>
        /// 创建指定文件夹目录
        /// </summary>
        /// <param name="path"></param>
        /// <returns>返回目录完整的物理路径</returns>
        public static string CreateDirectory(string path)
        {
            string finalPath = GetPhysicalPath(path);
            if (!Directory.Exists(finalPath))
            {
                Directory.CreateDirectory(finalPath);

            }
            return finalPath;
        }

        public static string GetPhysicalPath(string virtualPath)
        {
            var paths = virtualPath.Split(new string[] { "/", "\\" }, StringSplitOptions.RemoveEmptyEntries);
            var pathList = paths.ToList();
            pathList.Insert(0, Directory.GetCurrentDirectory());
            string finalPath = Path.Combine(pathList.ToArray());

            return finalPath;
        }

        public static string GetContentTypeByFilePath(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private static Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".zip", "application/zip"},
                {".gzip", "application/gzip"},
                {".7z", "application/x-7z-compressed"},
                {".tar", "application/tar"},
                {".csv", "text/csv"}
            };
        }

        public static T GetConfig<T>() where T : class, new()
        {
            var config = IocManager.Instance.Resolve<IOptions<T>>();
            return config.Value;
        }



        public static int CaculatePageCount(int totalCount, int pageSize)
        {
            int pageCount = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
            return pageCount;
        }

        /// <summary>
        /// MD5 HASH 
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Upper string</returns>
        public static string GetMd5Hash(string input)
        {
            using (MD5 md5Hash = MD5.Create())
            {

                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString().ToUpper();
            }
        }


        public static string ConvertImageToBase64(System.Drawing.Image image)
        {
            try
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();
                    string base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
