﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.ComponentModel.DataAnnotations.Schema;
using MagicLamp.PMS.Infrastructure.Extensions;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AutoMapper.Configuration.Conventions;

namespace MagicLamp.PMS.Infrastructure
{
    public static class ReflectionUtilities
    {

        public static bool IsSimple(this Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                // nullable type, check if the nested type is simple.
                return IsSimple(type.GetGenericArguments()[0]);
            }
            return type.IsPrimitive
              || type.IsEnum
              || type.Equals(typeof(string))
              || type.Equals(typeof(decimal));
        }

        public static PropertyInfo[] GetProperties<T>(this T obj) where T : class
        {
            var properties = typeof(T).GetProperties();
            return properties;
        }

        public static PropertyInfo GetPrimaryKeyPropertyInfo(this Type obj)
        {
            var properties = obj.GetProperties();
            foreach (var item in properties)
            {
                if (item.HasAttribute<KeyAttribute>())
                {
                    return item;
                }
            }
            foreach (var item in properties)
            {
                if (item.Name.Equals("Id"))
                {
                    return item;
                }
            }
            return null;
        }

        public static bool HasAttribute<T>(this PropertyInfo propertyInfo) where T : Attribute
        {
            var primaryKeyAttr = propertyInfo.GetCustomAttribute<T>();
            return primaryKeyAttr != null;
        }

        public static bool HasAttribute<T>(this PropertyInfo propertyInfo, out T attr) where T : Attribute
        {
            var primaryKeyAttr = propertyInfo.GetCustomAttribute<T>();
            attr = primaryKeyAttr;
            return primaryKeyAttr != null;
        }

        /// <summary>
        /// 反射拼接where查询条件，自动忽略null，非值类型字段
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="parameters">where条件参数集合</param>
        /// <returns></returns>
        public static string GenerateSQLPredicate<TEntity, TDto>(TEntity obj, TDto dto, out object[] objArr, bool useColumnAttribute = false)
            where TEntity : class
            where TDto : class
        {
            objArr = null;
            if (obj == null || dto == null)
            {
                return null;
            }

            List<object> parameters = new List<object>();

            var entityProperties = typeof(TEntity).GetProperties();
            var dtoProperties = typeof(TDto).GetProperties();

            StringBuilder sqlBuilder = new StringBuilder();

            int index = 0;
            foreach (var item in entityProperties)
            {
                if (item == null || item.HasAttribute<NotMappedAttribute>()
                    || !item.PropertyType.IsSimple())
                {
                    continue;
                }

                var dtoProperty = dtoProperties.FirstOrDefault(x => x.Name == item.Name);
                if (dtoProperty == null)
                {
                    dtoProperty = dtoProperties.FirstOrDefault(x => x.HasAttribute<MapToAttribute>() &&
                    x.GetCustomAttribute<MapToAttribute>().MatchingName == item.Name);
                }
                if (dtoProperty != null)
                {
                    var dtoVal = dtoProperty.GetValue(dto);
                    if (dtoVal == null)
                    {
                        continue;
                    }
                }
                else 
                {
                    continue;
                }

                var val = item.GetValue(obj);

                if (val == null)
                {
                    continue;
                }

                var column = string.Empty;
                ColumnAttribute columnAttr = null;
                if (item.HasAttribute<ColumnAttribute>(out columnAttr) && useColumnAttribute)
                {
                    column = columnAttr.Name;
                }
                else
                {
                    column = item.Name;
                }

                string operateType = "=";

                if (dtoProperty.HasAttribute<SupportMultipleQueryAttribute>())
                {
                    operateType = "Contains";
                    parameters.Add(val.ToString().SplitByCommonSymbol());
                    sqlBuilder.Append($" @{index}.{operateType}({column}) and");
                }
                else if (dtoProperty.HasAttribute<SupportFuzzyQueryAttribute>())
                {
                    operateType = "Contains";
                    parameters.Add(val.ToString());
                    sqlBuilder.Append($" {column}.{operateType}(@{index}) and");
                }
                else if (typeof(DateTime).IsAssignableFrom(item.PropertyType) ||
                    typeof(DateTime?).IsAssignableFrom(item.PropertyType))
                {
                    List<string> beginKeywords = new List<string> { "begin", "start" };
                    List<string> endKeywords = new List<string> { "end" };
                    if (column.ToLower().AnyContains(beginKeywords))
                    {
                        operateType = ">=";
                    }
                    else if (column.ToLower().AnyContains(endKeywords))
                    {
                        operateType = "<=";
                    }
                    parameters.Add(val);
                    sqlBuilder.Append($" {column} {operateType} @{index} and");
                }
                else
                {
                    parameters.Add(val);
                    sqlBuilder.Append($" {column} {operateType} @{index} and");
                }

                index++;
            }

            if (sqlBuilder.Length > 0)
            {
                sqlBuilder.Length -= 3;
                objArr = parameters.ToArray();
                return sqlBuilder.ToString();
            }

            return null;
        }

        public static TAttribute GetAttribute<TAttribute>(this Type type) where TAttribute : Attribute
        {
            var att = type.GetCustomAttributes(
                typeof(TAttribute), true
            ).FirstOrDefault() as TAttribute;
            if (att != null)
            {
                return att;
            }
            return default(TAttribute);
        }

    }
}
