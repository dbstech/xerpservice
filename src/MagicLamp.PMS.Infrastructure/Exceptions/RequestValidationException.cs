﻿using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Infrastructure
{
    public class RequestValidationException : UserFriendlyException
    {

        public RequestValidationException(string errorMessage) : base(errorMessage)
        {

        }

    }


}
