﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NPOI.SS.UserModel;
using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace MagicLamp.PMS.Infrastructure.Extensions
{
    public static class CommonExtension
    {

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null || !enumerable.Any())
            {
                return true;
            }

            return false;
        }

        /// <summary>  
        /// DateTime时间格式转换为Unix时间戳格式  
        /// </summary>  
        /// <param name="time"> DateTime时间格式</param>  
        /// <returns>Unix时间戳格式</returns>  
        public static long ToUnixTimeInSeconds(this System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (long)(time - startTime).TotalSeconds;
        }

        /// <summary>  
        /// DateTime时间格式转换为Unix时间戳格式  
        /// </summary>  
        /// <param name="time"> DateTime时间格式</param>  
        /// <returns>Unix时间戳格式</returns>  
        public static long ToUnixTimeInMilliSeconds(this System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (long)(time - startTime).TotalMilliseconds;
        }

        public static string GetFileExtension(this ImageFormat format)
        {
            try
            {
                return ImageCodecInfo.GetImageEncoders()
                        .First(x => x.FormatID == format.Guid)
                        .FilenameExtension
                        .Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                        .First()
                        .Trim('*')
                        .ToLower();
            }
            catch (Exception)
            {
                return "." + format.ToString().ToLower();
            }
        }

        /// <summary>
        /// Paged query builder
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="pageIndex">Start from zero</param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static IQueryable<T> PagedQuery<T>(this IQueryable<T> query, int pageIndex, int pageSize)
        {
            return query.Skip((pageIndex) * pageSize).Take(pageSize);
        }


        public static string ToJsonString(this object obj)
        {
            if (obj == null)
            {
                return string.Empty;
            }
            string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(obj);

            return jsonString;
        }

        public static string ToJsonString(this object obj, JsonSerializerSettings serializerSettings = null)
        {
            if (serializerSettings == null)
            {
                return JsonConvert.SerializeObject(obj);
            }

            return JsonConvert.SerializeObject(obj, serializerSettings);
        }


        public static T ConvertFromJsonString<T>(this string jsonStirng)
        {
            T obj = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonStirng);

            return obj;
        }


        public static string ToXMLString<T>(this T obj)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            using (StringWriter sw = new Utf8StringWriter())
            {
                serializer.Serialize(sw, obj);
                string xml = sw.ToString();
                return xml;
            }
        }

        public static string ConvertBase64ToString(this string base64)
        {
            byte[] data = Convert.FromBase64String(base64);
            string decodedString = Encoding.UTF8.GetString(data);
            return decodedString;
        }

        public static T ConvertFromXMLString<T>(this string xml)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StringReader reader = new StringReader(xml);
            T result = (T)(serializer.Deserialize(reader));
            reader.Close();
            reader.Dispose();

            return result;
        }

        public static bool TryConvertFromXMLString<T>(this string xml, out T result)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StringReader reader = new StringReader(xml);

            try
            {
                result = (T)(serializer.Deserialize(reader));
                reader.Close();
                reader.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                result = default(T);
                return false;
            }
        }

        /// <summary>
        /// 将字符串base64，默认使用utf8编码
        /// </summary>
        /// <param name="str"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static string ToBase64String(this string str, Encoding encoding = null)
        {
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }
            var bytes = encoding.GetBytes(str);
            string base64 = Convert.ToBase64String(bytes);

            return base64;
        }

        public static JObject GetJObject(this IRestResponse<JObject> response)
        {
            if (response == null || string.IsNullOrEmpty(response.Content))
            {
                return null;
            }

            return JObject.Parse(response.Content);
        }

        public static DateTime SpecifyKindInLocal(this DateTime date)
        {
            return DateTime.SpecifyKind(date, DateTimeKind.Local);
        }

        public static DateTime SpecifyKindInUTC(this DateTime date)
        {
            return DateTime.SpecifyKind(date, DateTimeKind.Utc);
        }

        /// <summary>
        /// convert utc time to nz time
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime ConvertUTCTimeToNZTime(this DateTime dateTime)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(dateTime, TimeZoneConverter.TZConvert.GetTimeZoneInfo("Pacific/Auckland"));
        }

        /// <summary>
        /// convert china time to nz time
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime ConvertCNTimeToNZTime(this DateTime dateTime)
        {
            return TimeZoneInfo.ConvertTime(dateTime, TimeZoneConverter.TZConvert.GetTimeZoneInfo("Asia/Shanghai"), TimeZoneConverter.TZConvert.GetTimeZoneInfo("Pacific/Auckland"));
        }

        public static string GetIPAddress(this IHttpContextAccessor context)
        {
            var result = string.Empty;

            //first try to get IP address from the forwarded header
            if (context.HttpContext.Request.Headers != null)
            {
                //the X-Forwarded-For (XFF) HTTP header field is a de facto standard for identifying the originating IP address of a client
                //connecting to a web server through an HTTP proxy or load balancer

                var forwardedHeader = context.HttpContext.Request.Headers["X-Forwarded-For"];
                if (!forwardedHeader.IsNullOrEmpty())
                {
                    result = forwardedHeader.FirstOrDefault();
                }
            }

            //if this header not exists try get connection remote IP address
            if (string.IsNullOrEmpty(result) && context.HttpContext.Connection.RemoteIpAddress != null)
            {
                result = context.HttpContext.Connection.RemoteIpAddress.IsIPv4MappedToIPv6 ?
                    context.HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString() :
                    context.HttpContext.Connection.RemoteIpAddress.ToString();
            }

            return result;
        }

        public static string StandardizeSKU(this string sku)
        {
            return sku.Trim().ToUpper();
        }

        public static string GetDescription<T>(this T enumValue)
            where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
                return null;

            var description = enumValue.ToString();
            var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());

            if (fieldInfo != null)
            {
                var attrs = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                {
                    description = ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return description;
        }

        public static List<string> GetDisplayAttributeValues(this Type type)
        {
            return type
                        .GetProperties()
                        .Where(x => x.GetCustomAttributes(typeof(DisplayAttribute), false).Count() > 0)
                        .Select(x => ((DisplayAttribute)x.GetCustomAttributes(typeof(DisplayAttribute), false)[0]).Name)
                        .ToList();
        }

        public static string GetDisplayAttributeValue(this Type type, string propertyName)
        {
            var propertyInfo = type.GetProperty(propertyName);
            if (propertyInfo == null)
            {
                return null;
            }
            var displayAttributes = propertyInfo.GetCustomAttributes(typeof(DisplayAttribute), false);

            if (displayAttributes.Count() == 0)
            {
                return null;
            }

            return ((DisplayAttribute)displayAttributes[0]).Name;
        }

        public static IRow GetOrCreateRow(this ISheet sheet, int rownum)
        {
            return sheet.GetRow(rownum) ?? sheet.CreateRow(rownum);
        }
        public static ICell GetOrCreateCell(this IRow row, int colnum)
        {
            return row.GetCell(colnum) ?? row.CreateCell(colnum);
        }
        
        public static void InsertColumn(this ISheet sheet, int rowIndex, int columnIndex, IEnumerable<string> values)
        {
            foreach (var value in values)
            {
                Type type = typeof(int);
                IRow row = sheet.GetOrCreateRow(rowIndex);
                ICell cell = row.CreateCell(columnIndex);
                cell.SetCellValue(value);
                rowIndex++;
            }
        }

        public static void CopyRowTo(this IWorkbook workbook, ISheet sourceSheet, ISheet destSheet, int sourceRowNum, int destRowNum)
        {
            IRow destRow = destSheet.GetOrCreateRow(destRowNum);
            IRow sourceRow = sourceSheet.GetRow(sourceRowNum);
            for (var i = 0; i < sourceRow.LastCellNum; i++)
            {
                var value = sourceRow.GetCell(i).StringCellValue;
                ICell destCell = destRow.GetOrCreateCell(i);
                destCell.SetCellValue(value);
            }
        }
    }
}