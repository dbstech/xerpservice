﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Abp.Extensions;

namespace MagicLamp.PMS.Infrastructure.Extensions
{
    public static class StringExtension
    {

        public static bool AnyContains(this string str, List<string> sources)
        {
            bool result = false;
            foreach (var item in sources)
            {
                if (str.Contains(item))
                {
                    result = true;
                    return result;
                }
            }

            return result;
        }

        /// <summary>
        /// 转换字符串编码为gb2312
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ConvertToGB2312(this string str)
        {
            var gb2312 = Encoding.GetEncoding("GB2312");
            var bytes = Encoding.Default.GetBytes(str);
            var gb2312Bytes = Encoding.Convert(Encoding.Default, gb2312, bytes);
            return gb2312.GetString(gb2312Bytes);
        }

        /// <summary>
        /// 转换字符串编码为UTF8
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ConvertToUTF8(this string str)
        {
            var utf8 = Encoding.UTF8;
            var bytes = Encoding.Default.GetBytes(str);
            var utfBytes = Encoding.Convert(Encoding.Default, utf8, bytes);
            return utf8.GetString(utfBytes);
        }

        public static string ChangeIfEqual(this string str, string equalVal, string result)
        {
            if (str == null || equalVal == null)
            {
                return str;
            }
            if (str.ToLower() == equalVal.ToLower())
            {
                return result;
            }
            return str;
        }


        public static List<string> SplitByCommonSymbol(this string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                return new List<string>();
            }
            var result = source.Split(new string[] { ",", "，", "\n", "\r", "\\n", "\\r" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            result.ForEach(x => { x = x.Trim(); });
            return result;
        }

        public static List<int> SplitByCommonSymbolReturnInt(this string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                return new List<int>();
            }
            return source.Split(new string[] { ",", "，", "\n", "\r", "\\n", "\\r" }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => Convert.ToInt32(x)).ToList();
        }

        public static string UrlEncode(this string str)
        {
            if (str.IsNullOrWhiteSpace())
            {
                return str;
            }
            return HttpUtility.UrlEncode(str);
        }


        public static string TrimAndCheckNull(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }

            return str.Trim();
        }

        public static bool AnyStartsWith(this string str, List<string> sources)
        {
            bool result = false;
            foreach (var item in sources)
            {
                if (str.StartsWith(item, StringComparison.OrdinalIgnoreCase))
                {
                    result = true;
                    return result;
                }
            }

            return result;
        }

    }
}