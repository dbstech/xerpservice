﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Infrastructure
{
    public class SupportMultipleQueryAttribute : Attribute
    {
        public SupportMultipleQueryAttribute() { }

        public Type ActualColumnType { get; set; }

    }
}
