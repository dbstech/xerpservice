﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

using ICSharpCode.SharpZipLib.Zip;


namespace MagicLamp.PMS.Infrastructure
{
    public class SharpZipUtility
    {

        /// <summary>
        /// 压缩文件
        /// </summary>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        public static MemoryStream ZipFiles(List<string> filePaths)
        {
            MemoryStream ms = new MemoryStream();
            byte[] buffer = null;
            ICSharpCode.SharpZipLib.Zip.ZipStrings.UseUnicode = true;
            ICSharpCode.SharpZipLib.Zip.ZipStrings.CodePage = Encoding.UTF8.CodePage;
            using (ZipFile zip = ZipFile.Create(ms))
            {
                zip.BeginUpdate();

                filePaths.ForEach(x =>
                {
                    zip.Add(x, Path.GetFileName(x));
                });

                zip.CommitUpdate();
                buffer = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(buffer, 0, buffer.Length);
                return ms;
            }
        }


        /// <summary>
        /// 压缩多个文件
        /// </summary>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        public static bool ZipFiles(string zipFilePath, List<string> filePaths)
        {
            try
            {
                ICSharpCode.SharpZipLib.Zip.ZipStrings.UseUnicode = true;
                ICSharpCode.SharpZipLib.Zip.ZipStrings.CodePage = Encoding.UTF8.CodePage;

                //ICSharpCode.SharpZipLib.Zip.ZipConstants.DefaultCodePage = Encoding.GetEncoding("gbk").CodePage;
                using (ZipFile zip = ZipFile.Create(zipFilePath))
                {
                    zip.BeginUpdate();

                    foreach (var item in filePaths)
                    {
                        zip.Add(item, Path.GetFileName(item));
                    }

                    zip.CommitUpdate();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }



        /// <summary>
        /// 压缩文件夹
        /// </summary>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        public static MemoryStream ZipDirectory(string directoryPath)
        {
            MemoryStream ms = new MemoryStream();
            byte[] buffer = null;
            ICSharpCode.SharpZipLib.Zip.ZipStrings.UseUnicode = true;
            ICSharpCode.SharpZipLib.Zip.ZipStrings.CodePage = Encoding.UTF8.CodePage;
            using (ZipFile zip = ZipFile.Create(ms))
            {
                zip.BeginUpdate();

                zip.AddDirectory(directoryPath);

                zip.CommitUpdate();
                buffer = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(buffer, 0, buffer.Length);
                return ms;
            }
        }

        /// <summary>
        /// 压缩文件夹
        /// </summary>
        /// <param name="filePaths"></param>
        /// <returns></returns>
        public static bool ZipDirectory(string directoryPath, string zipFilePath)
        {
            try
            {
                ICSharpCode.SharpZipLib.Zip.ZipConstants.DefaultCodePage = Encoding.UTF8.CodePage;
                using (ZipFile zip = ZipFile.Create(zipFilePath))
                {
                    zip.BeginUpdate();

                    zip.AddDirectory(directoryPath);

                    zip.CommitUpdate();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


    }
}