﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Infrastructure.Common
{
    public class CacheKeys
    {
        public const string JOBCACHEKEYNAME = "hangfire.jobs";
        public const string ORDERIDCARDJOBKEY = "job.orderidcard";
        public const string HEORDERJOBKEY = "job.heordertoxerp";
        public const string PRODUCTROLEKEY = "data.productroles";
        public const string PUSHORDERTOFLUXJOBKEY = "job.pushordertoflux";
        public const string FLUXEXPIRYDESCRIPTION = "fluxdata.bascodes.udfexpiry";
        public const string APPSERVICECACHEKEYNAME = "ServiceApplications";
        public const string DINGDINGPERSISTENTTOKENKEY = "dingding.persistenttoken";
        public const string APICOMPANYTOKENKEY = "naky.dbo.apicompany";
        public const string GENERALCACHEMANAGERKEY = "CacheKeyManager.General";
        public const string APPCONFIGKEYFORMAT = "ApiConfig.{0}";
        public const string PRODUCTKEYFORMAT = "BASProduct.{0}";//product basic info
        public const string PRODUCTSTOCKKEYFORMAT = "ProductStock.{0}";//product stock info
        public const string RESERVESTOCKCACHEDORDERID = "OrderLock.{0}";//order reserve orderid
        public const string CANCELLEDORDERID = "OrderCancelled.{0}";//cancelled orderID
        public const string AUEXPRESS = "AUExpress.{0}";
        public const string ADDRESSERROR = "AddressError.{0}";
        public const string EXCHANGERATE188 = "exchangerate.188";
        public const string EXCHANGERATEFORMAT = "{0}/{1}";
        public const string BAXIORDERNOTIFICATIONKEY = "BaXiOrderNotification.{0}";
        public const string YIWUACCESSTOKEN = "YiWuAccessToken.{0}";
    }
}
