﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MagicLamp.PMS.Migrations
{
    public partial class CreateProductRelatedEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BAS.GoodsOwner",
                columns: table => new
                {
                    OwnerId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(nullable: true),
                    FluxCode = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BAS.GoodsOwner", x => x.OwnerId);
                });

            migrationBuilder.CreateTable(
                name: "BAS.Image",
                columns: table => new
                {
                    ImageId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ImageType = table.Column<string>(nullable: true),
                    ReferenceId = table.Column<string>(nullable: true),
                    ReferenceType = table.Column<string>(nullable: true),
                    OriginalPath = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BAS.Image", x => x.ImageId);
                });

            migrationBuilder.CreateTable(
                name: "BAS.WareHouse",
                columns: table => new
                {
                    WareHouseId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Location = table.Column<string>(nullable: true),
                    IsWMS = table.Column<bool>(nullable: false),
                    code = table.Column<string>(nullable: true),
                    FluxCode = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BAS.WareHouse", x => x.WareHouseId);
                });

           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
