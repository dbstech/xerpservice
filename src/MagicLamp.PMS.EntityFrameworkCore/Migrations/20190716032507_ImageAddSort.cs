﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MagicLamp.PMS.Migrations
{
    public partial class ImageAddSort : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                schema: "BAS",
                table: "CostType",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Sort",
                table: "BAS.Image",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sort",
                table: "BAS.Image");

            migrationBuilder.AlterColumn<int>(
                name: "Active",
                schema: "BAS",
                table: "CostType",
                nullable: true,
                oldClrType: typeof(bool),
                oldNullable: true);
        }
    }
}
