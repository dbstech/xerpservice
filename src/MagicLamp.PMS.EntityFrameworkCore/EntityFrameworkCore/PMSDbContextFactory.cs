﻿using Abp.Dependency;
using MagicLamp.PMS.Configuration;
using MagicLamp.PMS.Web;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace MagicLamp.PMS.EntityFrameworkCore
{
    /* This class is needed to run EF Core PMC commands. Not used anywhere else */
    public class PMSDbContextFactory : IDesignTimeDbContextFactory<PMSDbContext>
    {

        public PMSDbContext CreateDbContext(string[] args = null)
        {
            var hostingEnviorment = IocManager.Instance.Resolve<IHostingEnvironment>();
            var builder = new DbContextOptionsBuilder<PMSDbContext>();

            var configuration = AppConfigurations.Get(hostingEnviorment.ContentRootPath, hostingEnviorment.EnvironmentName);


            PMSDbContextOptionsConfigurer.Configure(
                builder,
                configuration.GetConnectionString(PMSConsts.ConnectionStringName)
            );

            return new PMSDbContext(builder.Options);
        }
    }

}