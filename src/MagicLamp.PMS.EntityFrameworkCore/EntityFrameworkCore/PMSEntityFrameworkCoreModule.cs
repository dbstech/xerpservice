﻿using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.EntityFrameworkCore.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MagicLamp.PMS.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace MagicLamp.PMS.EntityFrameworkCore
{
    [DependsOn(
        typeof(PMSCoreModule),
        typeof(AbpEntityFrameworkCoreModule))]
    public class PMSEntityFrameworkCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.EntityHistory.IsEnabled = true;
            Configuration.EntityHistory.IsEnabledForAnonymousUsers = true;

            Configuration.ReplaceService(typeof(IConnectionStringResolver), () =>
            {
                IocManager.Register<IConnectionStringResolver, MyConnectionStringResolver>();
            });

            // Configure first DbContext
            Configuration.Modules.AbpEfCore().AddDbContext<PMSDbContext>(options =>
            {
                if (options.ExistingConnection != null)
                {
                    PMSDbContextOptionsConfigurer.Configure(options.DbContextOptions, options.ExistingConnection.ConnectionString);
                }
                else
                {
                    PMSDbContextOptionsConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                }
            });

            // Configure second DbContext
            Configuration.Modules.AbpEfCore().AddDbContext<HEERPDbContext>(options =>
            {
                if (options.ExistingConnection != null)
                {
                    HEERPDbContextOptionsConfigurer.Configure(options.DbContextOptions, options.ExistingConnection.ConnectionString);
                }
                else
                {
                    HEERPDbContextOptionsConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                }
            });

            Configuration.Modules.AbpEfCore().AddDbContext<PMSMysqlDbContext>(options =>
            {
                var hostingEnviorment = Abp.Dependency.IocManager.Instance.Resolve<IHostingEnvironment>();
                var configuration = AppConfigurations.Get(hostingEnviorment.ContentRootPath, hostingEnviorment.EnvironmentName);
                string conStr = configuration.GetConnectionString(PMSConsts.PMSMysqlConnectionStringName);
                PMSMysqlDbContextOptionsConfigurer.Configure(options.DbContextOptions, conStr);
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PMSEntityFrameworkCoreModule).GetAssembly());
        }
    }
}