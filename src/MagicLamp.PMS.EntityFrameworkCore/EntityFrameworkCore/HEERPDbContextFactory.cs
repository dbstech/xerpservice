﻿using Abp.Dependency;
using MagicLamp.PMS.Configuration;
using MagicLamp.PMS.Web;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace MagicLamp.PMS.EntityFrameworkCore
{
    /* This class is needed to run EF Core PMC commands. Not used anywhere else */
    public class HEERPDbContextFactory : IDesignTimeDbContextFactory<HEERPDbContext>
    {
        public HEERPDbContext CreateDbContext(string[] args = null)
        {
            var hostingEnviorment = IocManager.Instance.Resolve<IHostingEnvironment>();
            var builder = new DbContextOptionsBuilder<HEERPDbContext>();

            var configuration = AppConfigurations.Get(hostingEnviorment.ContentRootPath, hostingEnviorment.EnvironmentName);


            HEERPDbContextOptionsConfigurer.Configure(
                builder,
                configuration.GetConnectionString(PMSConsts.HEERPConnectionStringName)
            );

            return new HEERPDbContext(builder.Options);
        }
    }



}