﻿using Abp.EntityFrameworkCore;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using Abp.EntityHistory;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using MagicLamp.PMS.YiTongBao;
namespace MagicLamp.PMS.EntityFrameworkCore
{
    public class PMSDbContext : AbpDbContext
    {
        //Add DbSet properties for your entities...
        public DbSet<ProductEntity> Products { get; set; }
        public DbSet<IDCardEntity> IDCards { get; set; }
        public DbSet<DownloadRecordsEntity> DownloadRecords { get; set; }
        public DbSet<OrderIDCardEntity> OrderIDCards { get; set; }
        public DbSet<ImageEntity> Images { get; set; }
        public DbSet<WareHouseEntity> WareHouses { get; set; }
        public DbSet<GoodsOwnerEntity> GoodsOwners { get; set; }
        public DbSet<CurrencyEntity> Currencies { get; set; }
        public DbSet<PackMaterialEntity> PackMaterials { get; set; }
        public DbSet<CostTypeEntity> CostTypes { get; set; }
        public DbSet<OrderLineCostAndPriceEntity> OrderLineCostAndPrice { get; set; }
        public DbSet<ProductBusinessCostEntity> ProductBusinessCost { get; set; }
        public DbSet<PurchasePeriodEntity> PurchasePeriod { get; set; }
        public DbSet<InventoryProductCategoryEntity> InventoryProductCategories { get; set; }
        public DbSet<XERPBusinessEntity> XERPBusinesses { get; set; }
        public DbSet<ProductSalesBusinessEntity> ProductSalesBusinesses { get; set; }
        public DbSet<FreightEntity> Freights { get; set; }
        public DbSet<FreightSetEntity> FreightSet { get; set; }
        public DbSet<BrandEntity> Brands { get; set; }
        public DbSet<FileEntity> Files { get; set; }
        public DbSet<PurchaseOrderLineEntity> PurchaseOrderLines { get; set; }
        public DbSet<CQBondedProductEntity> CQBondedProductEntities { get; set; }
        public DbSet<LogisticsFeeEntity> LogisticsFeeEntities { get; set; }
        public DbSet<OrderPackTradeEntity> OrderPackTrades { get; set; }
        public DbSet<EcinxOrdersPackTradeEntity> EcinxOrdersPackTrades { get; set; }
        public DbSet<EcinxPushOrderLogEntity> EcinxPushOrderLog { get; set; }
        public DbSet<UserLoginInfoEntity> UserLoginInfo { get; set; }
        public DbSet<CustomsUnitEntity> CustomsUnit { get; set; }


        public DbSet<AgentProductOrderCostEntity> AgentProductOrderCosts { get; set; }
        public DbSet<OrderPackWMSQueueEntity> OrderPackWMSQueues { get; set; }
        public DbSet<OrderPackOutOfStockEntity> OrderPackOutOfStocks { get; set; }
        public DbSet<OrderPackReservedHHSKUEntity> OrderPackReservedHHSKUs { get; set; }
        public DbSet<LoginSessionEntity> LoginSessions { get; set; }
        public DbSet<AccountEntity> Accounts { get; set; }
        public DbSet<EditLogEntity> EditLogs { get; set; }
        public DbSet<PurchaseOrderHeaderEntity> PurchaseOrderHeaders { get; set; }
        public DbSet<BanktransferEntity> BanktransferEntities { get; set; }
        public DbSet<HHBindedWaveNoEntity> HHBindedWaveNoEntities { get; set; }
        public DbSet<AppConfigurationEntity> AppConfigurationEntities { get; set; }
        public DbSet<UserActivityLogEntity> UserActivityLogEntities { get; set; }
        public DbSet<SyncProductCostPriceEntity> SyncProductCostPriceEntities { get; set; }
        public DbSet<CQBondedOrderSKUInNumEntity> CQBondedOrderSKUInNumEntities { get; set; }
        public DbSet<OrderPackDetailProductCostEntity> OrderPackDetailProductCosts { get; set; }
        public DbSet<OrderSKULockForHHEntity> OrderSKULockForHH { get; set; }
        public DbSet<BrandCategoryEntity> BrandCategory { get; set; }
        public DbSet<BWCountryEntity> BWCountry { get; set; }
        public DbSet<PaymentTransactionEntity> PaymentTransaction { get; set; }
        public DbSet<UnionPayPaymentTransactionEntity> UnionPayPaymentTransaction { get; set; }
        public DbSet<LatiPayPaymentTransactionEntity> LatiPayPaymentTransaction { get; set; }
        public DbSet<LatiPayPaymentTransactionV2Entity> LatiPayPaymentTransactionV2 { get; set; }

        public DbSet<AllInPayPaymentTransactionEntity> AllInPayPaymentTransaction { get; set; }
        public DbSet<PoliPayPaymentTransactionEntity> PoliPayPaymentTransaction { get; set; }
        public DbSet<EntityChange> EntityChanges { get; set; }
        public DbSet<EntityChangeSet> EntityChangeSets { get; set; }
        public DbSet<EntityPropertyChange> EntityPropertyChanges { get; set; }
        public DbSet<ExchangeRate> ExchangeRates { get; set; }
        public IEntityHistoryHelper EntityHistoryHelper { get; set; }
        public DbSet<DeliveryOrderConfirmEntity> DeliveryOrderConfirms { get; set; }
        public DbSet<BaXiOrderEntity> BaXiOrders { get; set; }
        public DbSet<BaXiOrderProductPrice> BaXiOrderProductPrices { get; set; }
        public DbSet<BaXiProductInfo> BaXiProductInfos { get; set; }
        public DbSet<HHProductPrice> HHProductPrices { get; set; }

       //public DbQuery<EcinxOrdersPackTradeEntity> EcinxOrderpac { get; set; }


        public PMSDbContext(DbContextOptions<PMSDbContext> options)
            : base(options)
        {

        }
        public override int SaveChanges()
        {
            var changeSet = EntityHistoryHelper?.CreateEntityChangeSet(ChangeTracker.Entries().ToList());

            var result = base.SaveChanges();

            EntityHistoryHelper?.Save(changeSet);

            return result;
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var changeSet = EntityHistoryHelper?.CreateEntityChangeSet(ChangeTracker.Entries().ToList());

            var result = await base.SaveChangesAsync(cancellationToken);

            if (EntityHistoryHelper != null)
            {
                await EntityHistoryHelper.SaveAsync(changeSet);
            }

            return result;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            var converter = new EnumToStringConverter<UserActivityObjectType>();
            modelBuilder
                .Entity<UserActivityLogEntity>()
                .Property(e => e.ObjectType)
                .HasConversion(converter);
            modelBuilder
                .Entity<SyncProductCostPriceEntity>()
                .Property(e => e.CreationTime)
                .HasConversion(
                    v => v,
                    v => DateTime.SpecifyKind(v, DateTimeKind.Utc)
                );
            modelBuilder
                .Entity<AccountEntity>()
                .HasAlternateKey(x => x.XerpUserId)
                .HasName("XerpUserId");
            modelBuilder
                .Entity<UnionPayPaymentTransactionEntity>()
                .HasKey(x => new { x.ReferenceNo, x.TraceNo});
            modelBuilder
                .Entity<ExchangeRate>()
                .HasKey(x => new { x.From, x.To });
            // TODO: trim sku or other string properties that may throw exception or cause missing items
        }
    }
}
