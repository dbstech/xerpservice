﻿using Microsoft.EntityFrameworkCore;

namespace MagicLamp.PMS.EntityFrameworkCore
{
    public static class PMSDbContextOptionsConfigurer
    {
        public static void Configure(
            DbContextOptionsBuilder<PMSDbContext> dbContextOptions, 
            string connectionString
            )
        {
            /* This is the single point to configure DbContextOptions for PMSDbContext */
            dbContextOptions.UseSqlServer(connectionString, db => db.UseRowNumberForPaging());
        }

    }
}
