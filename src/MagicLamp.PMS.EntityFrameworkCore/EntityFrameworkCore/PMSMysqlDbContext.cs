﻿using Abp.EntityFrameworkCore;
using MagicLamp.PMS.Entities;
using Microsoft.EntityFrameworkCore;

namespace MagicLamp.PMS.EntityFrameworkCore
{
    public class PMSMysqlDbContext : AbpDbContext
    {
        //Add DbSet properties for your entities...
        public DbSet<LotStockEntity> LotStocks { get; set; }

        public PMSMysqlDbContext(DbContextOptions<PMSMysqlDbContext> options)
            : base(options)
        {

        }
    }
}
