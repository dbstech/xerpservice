﻿using Abp.EntityFrameworkCore;
using MagicLamp.PMS.Entities;
using Microsoft.EntityFrameworkCore;

namespace MagicLamp.PMS.EntityFrameworkCore
{
    public class FluxWMSDbContext : AbpDbContext
    {
        //Add DbSet properties for your entities...
        public DbSet<FluxDocOrderDetailEntity> FluxDocOrderDetails { get; set; }
        public DbSet<FluxStockAllocationRule> FluxStockAllocationRules { get; set; }
        public DbSet<FluxDocOrderHeaderEntity> FluxDocOrderHeaders { get; set; }
        public DbSet<FluxInvLotEntity> FluxInvLots { get; set; }
        public DbSet<FluxDocWaveHeaderEntity> FluxDocWaveHeaders { get; set; }
        public DbSet<FluxDocWaveDetailEntity> FluxDocWaveDetails { get; set; }
        public DbSet<FluxInvLotLocIdEntity> FluxInvLotLocIds { get; set; }
        public DbSet<FluxInvLotAttEntity> FluxInvLotAtts { get; set; }
        public DbSet<FluxBasSkuEntity> FluxBasSkus { get; set; }
        public DbSet<FluxACTTransactionLogEntity> FluxACTTransactionLogs { get; set; }
        public DbSet<FluxBasCodeEntity> FluxBasCodes { get; set; }
        public DbSet<FluxDocASNHeaderEntity> FluxDocASNHeaders { get; set; }
        public DbSet<FluxACTAllocationDetailEntity> FluxACTAllocationDetails { get; set; }

        public FluxWMSDbContext(DbContextOptions<FluxWMSDbContext> options)
            : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<FluxStockAllocationRule>().HasKey(x => new { x.RotationID, x.LotAttName });

            modelBuilder.Entity<FluxDocOrderDetailEntity>().HasKey(x => new { x.OrderNo, x.OrderLineNo });

            modelBuilder.Entity<FluxDocWaveDetailEntity>().HasKey(x => new { x.WaveNo, x.OrderNo });

            modelBuilder.Entity<FluxInvLotLocIdEntity>().HasKey(x => new { x.LotNum, x.LocationID, x.TraceID });

            modelBuilder.Entity<FluxBasSkuEntity>().HasKey(x => new { x.SKU, x.CustomerID });
            modelBuilder.Entity<FluxBasCodeEntity>().HasKey(x => new { x.CodeID, x.Code });

        }

    }
}
