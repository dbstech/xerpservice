﻿using Abp.Dependency;
using MagicLamp.PMS.Configuration;
using MagicLamp.PMS.Web;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace MagicLamp.PMS.EntityFrameworkCore
{
    /* This class is needed to run EF Core PMC commands. Not used anywhere else */
    public class FluxWMSDbContextFactory : IDesignTimeDbContextFactory<FluxWMSDbContext>
    {
        public FluxWMSDbContext CreateDbContext(string[] args = null)
        {
            var hostingEnviorment = IocManager.Instance.Resolve<IHostingEnvironment>();
            var builder = new DbContextOptionsBuilder<FluxWMSDbContext>();

            var configuration = AppConfigurations.Get(hostingEnviorment.ContentRootPath, hostingEnviorment.EnvironmentName);


            FluxWMSDbContextOptionsConfigurer.Configure(
                builder,
                configuration.GetConnectionString(PMSConsts.FluxWMSConnectionStringName)
            );

            return new FluxWMSDbContext(builder.Options);
        }
    }



}