﻿using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using System;

namespace MagicLamp.PMS.EntityFrameworkCore
{
    public static class FluxWMSDbContextOptionsConfigurer
    {
        public static void Configure(
           DbContextOptionsBuilder<FluxWMSDbContext> dbContextOptions,
           string connectionString
           )
        {
            dbContextOptions.UseSqlServer(connectionString, db => db.UseRowNumberForPaging());
        }
    }
}
