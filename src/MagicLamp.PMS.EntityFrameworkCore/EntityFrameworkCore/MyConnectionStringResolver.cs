﻿using Abp.Configuration.Startup;
using Abp.Domain.Uow;
using MagicLamp.PMS.Configuration;
using MagicLamp.PMS.Web;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.EntityFrameworkCore
{
    public class MyConnectionStringResolver : DefaultConnectionStringResolver
    {
        private readonly IConfigurationRoot _appConfiguration;

        public MyConnectionStringResolver(IAbpStartupConfiguration configuration, IHostingEnvironment hostingEnvironment)
            : base(configuration)
        {
            _appConfiguration =
                AppConfigurations.Get(hostingEnvironment.ContentRootPath, hostingEnvironment.EnvironmentName);
        }

        public override string GetNameOrConnectionString(ConnectionStringResolveArgs args)
        {
            if (args["DbContextConcreteType"] as Type == typeof(HEERPDbContext))
            {
                return _appConfiguration.GetConnectionString(PMSConsts.HEERPConnectionStringName);
            }

            if (args["DbContextConcreteType"] as Type == typeof(FluxWMSDbContext))
            {
                return _appConfiguration.GetConnectionString(PMSConsts.FluxWMSConnectionStringName);
            }

            return base.GetNameOrConnectionString(args);
        }
    }
}
