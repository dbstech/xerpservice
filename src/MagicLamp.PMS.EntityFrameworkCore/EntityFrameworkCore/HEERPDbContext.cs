﻿using Abp.EntityFrameworkCore;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Entities.DBQueryTypes;
using Microsoft.EntityFrameworkCore;
using MagicLamp.PMS.ProductCategories;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System.Collections.Generic;
using System.Linq;
using System;
using Abp.Extensions;
using Abp.EntityHistory;
using System.Threading.Tasks;
using System.Threading;

namespace MagicLamp.PMS.EntityFrameworkCore
{
    public class HEERPDbContext : AbpDbContext
    {
        //Add DbSet properties for your entities...
        public DbSet<OrderPackEntity> OrderPacks { get; set; }
        public DbSet<OrderPackDetailEntity> OrderPackDetails { get; set; }
        public DbSet<OrderEntity> Orders { get; set; }
        //public DbSet<OrderStatusEntity> OrderStatus { get; set; }
        //public DbSet<OrderPackStatusEntity> OrderPackStatus { get; set; }
        //public DbSet<FrightEntity> Frights { get; set; }


        public DbSet<OrderDetailEntity> OrderDetails { get; set; }
        public DbSet<SeqEntity> Seqs { get; set; }
        public DbSet<BusinessEntity> Businesses { get; set; }
        public DbSet<ExportFlagEntity> ExportFlags { get; set; }
        public DbSet<ProductBrandEntity> ProductBrands { get; set; }
        public DbSet<SplitOrderProductCategoryEntity> SplitOrderCategories { get; set; }
        public DbSet<SkuSplitOrderCategoryEntity> SkuSplitOrderCategories { get; set; }
        public DbSet<APIStockEntity> APIStocks { get; set; }
        public DbSet<ParcelTrackNoEntity> ParcelTrackNos { get; set; }
        public DbSet<APIConfigEntity> APIConfigs { get; set; }
        public DbSet<OrderLogEntity> OrderLogs { get; set; }
        public DbSet<UserEnitity> Users { get; set; }
        public DbSet<SeqPackEntity> SeqPacks { get; set; }
        public DbSet<FreightsEntity> Freights { get; set; }
        public DbSet<FreightsSetsEntity> FreightsSets { get; set; }
        public DbSet<BarcodeSeqEntity> BarcodeSeqs { get; set; }
        public DbSet<CacheOrderEntity> CacheOrders { get; set; }
        public DbSet<PSeqStatusEntity> PSeqStatus { get; set; }
        public DbSet<PackRuleEntity> PackRule { get; set; }
        public DbSet<CustomKindEntity> CustomKind { get; set; }
        public DbSet<ProductRoleEntity> ProductRoles { get; set; }
        public DbSet<OrderSkuLock> OrderSkuLocks { get; set; }
        public DbSet<CacheOrderDetail> CacheOrderDetails { get; set; }
        public DbSet<APICompanyEntity> APICompanies { get; set; }
        public DbSet<FreightOrderSplitCategoryRule> FreightOrderSplitCategoryRules { get; set; }
        public IEntityHistoryHelper EntityHistoryHelper { get; set; }

        #region DBQuery

        public DbQuery<SimpleSkuQueryType> SimpleSkuDBQuery { get; set; }
        public DbQuery<OrderPackQueryType> OrderPackDBQuery { get; set; }
        public DbQuery<yiwuOrderPackwaitingPush> OrderPackYiwu { get; set; }

       




        #endregion

        public HEERPDbContext(DbContextOptions<HEERPDbContext> options)
            : base(options)
        {

        }
        public override int SaveChanges()
        {
            var changeSet = EntityHistoryHelper?.CreateEntityChangeSet(ChangeTracker.Entries().ToList());

            var result = base.SaveChanges();

            EntityHistoryHelper?.Save(changeSet);

            return result;
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var changeSet = EntityHistoryHelper?.CreateEntityChangeSet(ChangeTracker.Entries().ToList());

            var result = await base.SaveChangesAsync(cancellationToken);

            if (EntityHistoryHelper != null)
            {
                await EntityHistoryHelper.SaveAsync(changeSet);
            }

            return result;
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .Entity<FreightOrderSplitCategoryRule>()
                .Property(x => x.SameCategoryIds)
                .HasConversion(
                    new ValueConverter<List<int>, string>(
                        v => string.Join(',', v),
                        v => v.IsNullOrWhiteSpace() ? null : v.Split(',', StringSplitOptions.None).Select(x => int.Parse(x)).ToList()
                    )
                );
        }
    }
}
