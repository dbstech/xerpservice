﻿using Microsoft.EntityFrameworkCore;

namespace MagicLamp.PMS.EntityFrameworkCore
{
    public static class HEERPDbContextOptionsConfigurer
    {
        public static void Configure(
           DbContextOptionsBuilder<HEERPDbContext> dbContextOptions,
           string connectionString
           )
        {
            /* This is the single point to configure DbContextOptions for PMSDbContext */
            dbContextOptions.UseSqlServer(connectionString, db => db.UseRowNumberForPaging());
        }
    }
}
