﻿using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using System;

namespace MagicLamp.PMS.EntityFrameworkCore
{
    public static class PMSMysqlDbContextOptionsConfigurer
    {
        public static void Configure(
           DbContextOptionsBuilder<PMSMysqlDbContext> dbContextOptions,
           string connectionString
           )
        {
            /* This is the single point to configure DbContextOptions for PMSDbContext */
            dbContextOptions.UseMySql(connectionString, db => db.ServerVersion(new Version(5, 7, 17), ServerType.MySql));
            //dbContextOptions.UseMySql(connectionString, null);
        }
    }
}
