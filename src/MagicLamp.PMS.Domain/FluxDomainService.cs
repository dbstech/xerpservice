﻿using MagicLamp.Flux.API.Models;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.PMS.Domain
{
    public class FluxDomainService : BaseDomainService
    {
        private const string ClogTagType = "FluxDomainService";
        FluxWMSDbContext fluxWMSDbContext = null;
        PMSDbContext pmsDbContext = null;
        HEERPDbContext heerpDbContext = null;


        public FluxDomainService()
        {
            fluxWMSDbContext = new FluxWMSDbContextFactory().CreateDbContext();
            pmsDbContext = new PMSDbContextFactory().CreateDbContext();
            heerpDbContext = new HEERPDbContextFactory().CreateDbContext();
        }

        private bool SatisfyPushToFluxDirectly(StockShareConfigVO configStockShare, OrderPackEntity orderPackInfo, List<string> listSKU)
        {
            bool result = false;

            //conditions for directly push order to flux
            //no config || not stock share || orderpack is not freezed and orderpack doesnot contain shared sku
            if (configStockShare == null || !configStockShare.EnableStockShare || (orderPackInfo.freezeFlag == (int)OrderPackFreezeFlagEnum.未冻结 && !listSKU.Any(x => configStockShare.TestSKU.Contains(x))))
            {
                result = true;
                return result;
            }

            return result;
        }

        private bool SatisfyPushToWMSQueue(StockShareConfigVO configStockShare, OrderPackEntity orderPackInfo, List<string> listSKU)
        {
            bool result = false;

            if (orderPackInfo.freezeFlag == (int)OrderPackFreezeFlagEnum.未冻结 && listSKU.Any(x => configStockShare.TestSKU.Contains(x)))
            {
                result = true;
                return result;
            }

            return result;
        }

        /// <summary>
        /// Assemble Flux putSOData Header
        /// </summary>
        /// <param name="orderPackInfo"></param>
        /// <param name="orderInfo"></param>
        /// <param name="listOrderPackDetail"></param>
        /// <param name="seqInfo"></param>
        /// <param name="businessInfo"></param>
        /// <param name="freightsInfo"></param>
        /// <param name="orderPackDetailsForHH"></param>
        /// <param name="userDefine6"></param>
        /// <returns></returns>
        public List<FluxPutOrderDTO> MakeFluxPutSODataHeaderDTO(OrderPackEntity orderPackInfo, OrderEntity orderInfo, List<OrderPackDetailEntity> listOrderPackDetail,
            SeqEntity seqInfo, BusinessEntity businessInfo, FreightsEntity freightsInfo, List<OrderPackDetailDTO> orderPackDetailsForHH, string userDefine6 = null)
        {
            List<FluxPutOrderDTO> result = new List<FluxPutOrderDTO>();

            //判断是否已经分配了快递和快递单号
            if (string.IsNullOrWhiteSpace(orderPackInfo.carrierId) || string.IsNullOrWhiteSpace(orderPackInfo.freightId))
            {
                if (orderPackInfo.freezeFlag == 0 && !FluxWMSProxy.NoNeedCarrierIdExportFlags.Contains(orderPackInfo.exportFlag.Value))
                {
                    Dictionary<string, string> tags = new Dictionary<string, string>();
                    tags["type"] = ClogTagType;
                    tags["method"] = "MakeFluxPutOrderDTO";
                    tags["orderpackno"] = orderPackInfo.orderPackNo;
                    CLSLogger.Error("订单推富勒发生异常", orderPackInfo.ToJsonString() + "\n失败原因：订单未分配快递单号或未设置物流物流标志", tags);
                    //dingding 通知
                    DDNotificationProxy.MarkdownNotification("订单推富勒发生异常", orderPackInfo.ToJsonString() + "\n 失败原因：订单未分配快递单号或未设置物流物流标志", "07bd13e4883f31bbab094c38a3c08224bdb3f5e4fdf9b011f7de5369752bdc4f");
                    return null;
                }
            }

            if (orderPackInfo.freightId == "yto" && orderPackInfo.invoiceUrl.IsNullOrEmpty())
            {
                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = ClogTagType;
                tags["method"] = "MakeFluxPutOrderDTO";
                tags["orderpackno"] = orderPackInfo.orderPackNo;
                CLSLogger.Error("订单推富勒发生异常", "圆通快递PDF未获取成功", tags);
                //dingding 通知
                DDNotificationProxy.MarkdownNotification("订单推富勒发生异常", "失败原因：圆通快递PDF未获取成功", "07bd13e4883f31bbab094c38a3c08224bdb3f5e4fdf9b011f7de5369752bdc4f");
                return null;
            }

            //notes: 注意IsFlux需要添加新的exportflag
            if (orderPackInfo.freezeFlag == 0 && FluxWMSProxy.AllowSyncStatus.Contains(orderPackInfo.status.Value)
                && FluxWMSProxy.IsFlux((int)orderPackInfo.exportFlag))
            {
                var orderToBePushed = new FluxPutOrderDTO
                {
                    OrderNo = orderPackInfo.orderPackNo,
                    OrderType = "CM",
                    CustomerID = FluxWMSProxy.GetCustomerIdByExportAndBiz(orderPackInfo.exportFlag, seqInfo.bizID),
                    OrderTime = orderInfo.orderCreateTime.Value.ToString("yyyy-MM-dd hh:mm:ss"),
                    SOReference2 = orderPackInfo.orderID,
                    SOReference3 = orderInfo.store,
                    SOReference4 = businessInfo.bizName,
                    SOReference5 = orderPackInfo.carrierId == null ? "" : orderPackInfo.carrierId,
                    DeliveryNo = orderPackInfo.carrierId == null ? "" : orderPackInfo.carrierId,
                    ConsigneeName = orderPackInfo.rec_name,
                    C_Tel1 = orderPackInfo.rec_phone,
                    C_Province = orderInfo.province,
                    C_City = orderInfo.city,
                    //街道
                    C_Address1 = orderInfo.town,
                    //区
                    C_Address2 = orderInfo.district,
                    //全地址
                    C_Address3 = orderPackInfo.rec_addr,
                    Notes = orderInfo.notes,
                    Channel = orderPackInfo.exportFlag.ToString(),
                    CarrierId = orderPackInfo.freightId == null ? "" : orderPackInfo.freightId,
                    CarrierName = freightsInfo == null ? "" : freightsInfo.name,
                    //notes：根据exportflag确定仓库ID
                    WarehouseID = FluxWMSProxy.GetWarehouseId(orderPackInfo.exportFlag),
                    //身份证
                    H_EDI_01 = orderPackInfo.idNo == null ? "" : orderPackInfo.idNo,
                    H_EDI_02 = orderPackInfo.invoiceUrl,
                    H_EDI_03 = orderPackInfo.distributionCode,
                    //合作方
                    UserDefine5 = orderInfo.partner,
                    UserDefine6 = userDefine6
                };

                //顺丰线
                if (orderPackInfo.exportFlag == 16 || orderPackInfo.exportFlag == 25 || orderPackInfo.exportFlag == 26 || orderPackInfo.freightId == "SFNSF")
                {
                    orderToBePushed.H_EDI_01 = "TK";
                }

                //CCIC
                if (businessInfo.bizID != 1 && businessInfo.bizID != 888 || !orderInfo.partner.IsNullOrEmpty() || orderInfo.IsSelectedCCIC == false)
                {
                    if (orderPackInfo.freightId == "nsf" || orderPackInfo.freightId == "SFNSF")
                    {
                        orderToBePushed.CarrierId = "NSFNOQRCODE";
                        orderToBePushed.CarrierName = "NSFNOQRCODE";
                    }
                    else if (orderPackInfo.freightId == "NSFAU")
                    {
                        orderToBePushed.CarrierId = "NSFAUNOQRCODE";
                        orderToBePushed.CarrierName = "NSFAUNOQRCODE";
                    }
                }

                //长江物流特殊处理（AU奶粉，HH不要CCIC）
                if (orderPackInfo.freightId == "ChangJiangExpress")
                {
                    if (!orderInfo.partner.IsNullOrEmpty() || FluxWMSProxy.ChangJiangNOCCICExportFlags.Contains(orderPackInfo.exportFlag.Value) || orderInfo.IsSelectedCCIC == false)
                    {
                        orderToBePushed.CarrierId = "CHANGJIANGEXPRESSNOCCIC";
                        orderToBePushed.CarrierName = "CHANGJIANGEXPRESSNOCCIC";
                    }
                }

                //自提渠道
                if (orderPackInfo.exportFlag == 7)
                {
                    //fastway
                    if (
                        orderInfo.shipment.IndexOf("奥克兰") >= 0 || orderInfo.shipment.IndexOf("北岛") >= 0 || orderInfo.shipment.IndexOf("南岛") >= 0
                        )
                    {
                        orderToBePushed.CarrierId = "fastway";
                        orderToBePushed.CarrierName = "fastway";
                        orderToBePushed.DeliveryNo = orderPackInfo.orderID;
                        orderToBePushed.C_Address1 = orderPackInfo.rec_addr;

                        orderPackInfo.freightId = "fastway";
                    }
                    else if (orderInfo.shipment.IndexOf("Penrose自提点") >= 0)
                    {
                        orderToBePushed.CarrierId = "PenrosePUP";
                        orderToBePushed.CarrierName = "Penrose自提点";
                        orderToBePushed.DeliveryNo = orderPackInfo.orderID;
                        orderToBePushed.C_Address1 = orderPackInfo.rec_addr;

                        orderPackInfo.freightId = "PenrosePUP";
                        orderPackInfo.carrierId = orderPackInfo.orderPackNo;
                    }
                    else if (orderInfo.shipment.IndexOf("Rosedale自提点") >= 0)
                    {
                        orderToBePushed.CarrierId = "RosedalePUP";
                        orderToBePushed.CarrierName = "Rosedale自提点";
                        orderToBePushed.DeliveryNo = orderPackInfo.orderID;
                        orderToBePushed.C_Address1 = orderPackInfo.rec_addr;

                        orderPackInfo.freightId = "RosedalePUP";
                        orderPackInfo.carrierId = orderPackInfo.orderPackNo;
                    }
                    else if (orderInfo.shipment.IndexOf("Meadowlands自提点") >= 0)
                    {
                        orderToBePushed.CarrierId = "MeadowlandsPUP";
                        orderToBePushed.CarrierName = "Meadowlands自提点";
                        orderToBePushed.DeliveryNo = orderPackInfo.orderID;
                        orderToBePushed.C_Address1 = orderPackInfo.rec_addr;

                        orderPackInfo.freightId = "MeadowlandsPUP";
                        orderPackInfo.carrierId = orderPackInfo.orderPackNo;
                    }
                    //自提
                    else if (orderInfo.shipment.IndexOf("自提") >= 0)
                    {
                        orderToBePushed.CarrierId = "ziti";
                        orderToBePushed.CarrierName = "自提";
                        orderToBePushed.DeliveryNo = orderPackInfo.orderID;
                        orderToBePushed.C_Address1 = orderPackInfo.rec_addr;
                        orderPackInfo.freightId = "ziti";
                        orderPackInfo.carrierId = orderPackInfo.orderPackNo;
                    }
                }

                var orderPackDetails = listOrderPackDetail.Where(x => x.opid == orderPackInfo.opID).ToList();

                List<detailsItem> listFluxDetailsItem = new List<detailsItem>();

                for (int i = 0; i < orderPackDetails.Count; i++)
                {
                    if (orderPackDetails[i].flag == 1)
                    {
                        var item = new detailsItem
                        {
                            LineNo = i + 1,
                            //notes: 注意GetCustomID需要添加新的exportflag
                            CustomerID = FluxWMSProxy.GetCustomerIdByExportAndBiz(orderPackInfo.exportFlag, seqInfo.bizID),
                            SKU = orderPackDetails[i].sku.ToUpper(),
                            QtyOrdered = Convert.ToDecimal(orderPackDetails[i].qty),
                            Notes = orderPackDetails[i].notes,
                            LotAtt11 = orderInfo.partner,
                        };

                        if (!orderPackDetailsForHH.IsNullOrEmpty() && orderPackDetailsForHH.Any(x => x.sku.ToLower().Trim() == orderPackDetails[i].sku.ToLower().Trim()))
                        {
                            //需要HH配货的SKU，预留状态，为了之后更新成波次号
                            item.LotAtt07 = PMSConsts.USERDEFINE6FORSTOCKSHARE;
                        }

                        listFluxDetailsItem.Add(item);
                    }
                }

                orderToBePushed.detailsItem = listFluxDetailsItem.ToArray<detailsItem>();

                result.Add(orderToBePushed);
            }

            return result;
        }

        public FluxResponse PushOrderToFlux(OrderPackEntity orderPackInfo, OrderEntity orderInfo, List<OrderPackDetailEntity> listOrderPackDetail,
            SeqEntity seqInfo, BusinessEntity businessInfo, FreightsEntity freightsInfo, List<OrderPackDetailDTO> orderPackDetailsForHH, string userDefine6 = null)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["type"] = ClogTagType,
                ["method"] = "PushOrderToFlux",
                ["orderpackno"] = orderPackInfo.orderPackNo,
                ["trackid"] = orderPackInfo.opID.ToString(),
                ["orderid"] = orderPackInfo.orderID
            };



            List<FluxPutOrderDTO> listFluxOrder = MakeFluxPutSODataHeaderDTO(orderPackInfo, orderInfo, listOrderPackDetail, seqInfo, businessInfo, freightsInfo, orderPackDetailsForHH, userDefine6);

            if (listFluxOrder.IsNullOrEmpty())
            {
                return null;
            }


            FluxResponse fluxResponse = new FluxResponse();

            try
            {
                fluxResponse = FluxWMSProxy.PutSOData(listFluxOrder, orderPackInfo.exportFlag);
            }
            catch (Exception ex)
            {
                CLSLogger.Info("同步到富勒出错", ex.ToString(), logTags);
            }


            return fluxResponse;



        }

        public bool PlaceOrderInFlux(OrderPackEntity orderPackInfo, OrderEntity orderInfo, List<OrderPackDetailEntity> listOrderPackDetail,
            SeqEntity seqInfo, BusinessEntity businessInfo, FreightsEntity freightsInfo, List<OrderPackDetailDTO> orderPackDetailsForHH, ref string promptMessage, string userDefine6 = null)
        {
            bool result = false;

            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["type"] = ClogTagType,
                ["method"] = "PlaceOrderInFlux",
                ["orderpackno"] = orderPackInfo.orderPackNo,
                ["trackid"] = orderPackInfo.opID.ToString(),
                ["orderid"] = orderPackInfo.orderID
            };

            try
            {
                StockShareDomainService stockShareDomainService = new StockShareDomainService();
                var configStockShare = stockShareDomainService.GetStockShareConfig();

                var listSKU = listOrderPackDetail.Select(x => x.sku).ToList();


                if (SatisfyPushToFluxDirectly(configStockShare, orderPackInfo, listSKU))
                {
                    //directly place order in Flux
                    var responseFlux = PushOrderToFlux(orderPackInfo, orderInfo, listOrderPackDetail, seqInfo, businessInfo, freightsInfo, orderPackDetailsForHH);

                    //推失败
                    if (responseFlux == null || int.Parse(responseFlux.Response.@return.returnFlag) == (int)FluxReturnFlagEnum.Failed)
                    {
                        var reason = responseFlux == null ? "富勒接口返回null" : responseFlux.Response.@return.returnDesc;
                        promptMessage = reason;

                        CLSLogger.Error("订单推富勒发生异常", responseFlux == null ? "富勒接口返回null" : responseFlux.Response.@return.ToJsonString(), logTags);
                    }
                    //部分成功
                    else if (int.Parse(responseFlux.Response.@return.returnFlag) == (int)FluxReturnFlagEnum.PartialSuccess)
                    {
                        var reason = responseFlux.Response.@return.returnDesc;
                        promptMessage = reason;

                        result = true;
                    }
                    //成功
                    else if (int.Parse(responseFlux.Response.@return.returnFlag) == (int)FluxReturnFlagEnum.Success)
                    {
                        var reason = responseFlux.Response.@return.returnDesc;
                        promptMessage = reason;

                        CLSLogger.Info("订单推富勒成功", responseFlux.Response.@return.ToJsonString(), logTags);
                        result = true;
                    }

                    return result;
                }
                else if (SatisfyPushToWMSQueue(configStockShare, orderPackInfo, listSKU))
                {
                    var addedResult = AddToWMSQueue(orderPackInfo, ref promptMessage);

                    result = addedResult;
                    return result;
                }
                else
                {
                    promptMessage = "包裹状态不符合条件，无法推送富勒。请检查包裹状态是否已冻结或已推送到富勒。";
                    return result;
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("", ex, logTags);
                promptMessage = $"订单重推富勒出现异常，异常原因：{ex}";
                return result;
            }
        }

        public bool CancelOrderInFlux(OrderPackEntity orderPackInfo, string comment, ref string message)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["type"] = ClogTagType,
                ["method"] = "CancelOrderInFlux",
                ["orderpackno"] = orderPackInfo.orderPackNo,
                ["trackid"] = orderPackInfo.opID.ToString(),
                ["orderid"] = orderPackInfo.orderID
            };

            bool result = false;

            try
            {
                if (orderPackInfo == null)
                {
                    message = $"取消富勒失败，未找到对应包裹信息，opid：{orderPackInfo.opID}";
                    result = false;
                    return result;
                }

                var fluxDocOrderHeaderInfo = fluxWMSDbContext.FluxDocOrderHeaders.FirstOrDefault(x => x.OrderPackNo == orderPackInfo.orderPackNo);
                if (fluxDocOrderHeaderInfo == null)
                {
                    message = $"取消富勒订单成功，包裹：{orderPackInfo.orderPackNo} 在富勒无对应订单，无需取消富勒订单。";
                    result = true;
                    return result;
                }

                if (PMSConsts.FluxSOStatusNotAllowedToCancel.Contains(fluxDocOrderHeaderInfo.SOStatus))
                {
                    message = $"取消富勒订单失败，包裹：{fluxDocOrderHeaderInfo.OrderPackNo} 已经出库，如有必要请及时联系仓库或对应物流进行拦截。";
                }

                var orderToBeCanceled = new FluxCancelOrderDTO
                {
                    OrderNo = orderPackInfo.orderPackNo,
                    OrderType = "CM",
                    CustomerID = fluxDocOrderHeaderInfo.CustomerID,
                    WarehouseID = fluxDocOrderHeaderInfo.Warehouseid,
                    Reason = comment
                };

                FluxResponse fluxResponse = FluxWMSProxy.CancelSOData(orderToBeCanceled, orderPackInfo.exportFlag);

                //取消失败
                if (fluxResponse == null || fluxResponse.Response == null || fluxResponse.Response.@return == null)
                {
                    message = $"取消富勒订单发生异常，富勒取消返回null。";
                    CLSLogger.Error("取消富勒订单发生异常", message, logTags);
                    return result;
                }
                else if (int.Parse(fluxResponse.Response.@return.returnFlag) != 1)
                {
                    var reason = fluxResponse.Response.@return.returnDesc;
                    CLSLogger.Error("取消富勒订单失败", reason, logTags);
                    message = $"取消富勒订单失败，取消富勒订单发生异常。";
                    return result;
                }
                //取消成功
                else
                {
                    message = $"取消富勒订单成功。富勒返回报文：{fluxResponse.ToJsonString()}";
                    CLSLogger.Info("取消富勒订单成功", fluxResponse.ToJsonString(), logTags);
                    result = true;
                    return result;
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("FluxDomainService.FreezeOrderInFlux失败", ex, logTags);
                return result;
            }
        }

        public bool AddToWMSQueue(OrderPackEntity orderPackInfo, ref string promptMessage, bool ignoreDoneStatus = false)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["type"] = ClogTagType,
                ["method"] = "AddToWMSQueue",
                ["orderpackno"] = orderPackInfo.orderPackNo,
                ["trackid"] = orderPackInfo.opID.ToString(),
                ["orderid"] = orderPackInfo.orderID
            };

            bool result = true;

            if (orderPackInfo == null)
            {
                promptMessage = "重推结束，无包裹需要推送到OrderPacks_WMSQueue。";

                result = true;
                return result;
            }

            try
            {
                int opID = orderPackInfo.opID;
                var existedOrderPackInQueue = pmsDbContext.OrderPackWMSQueues.FirstOrDefault(x => x.Opid == opID);

                //update or skip
                if (existedOrderPackInQueue != null)
                {
                    if (ignoreDoneStatus || existedOrderPackInQueue.Status != QueueStatusEnum.Done.ToString())
                    {
                        var previousStatus = existedOrderPackInQueue.Status;
                        existedOrderPackInQueue.Status = QueueStatusEnum.Created.ToString();
                        existedOrderPackInQueue.UpdateTime = DateTime.UtcNow;

                        pmsDbContext.SaveChanges();
                        CLSLogger.Info("更新OrderPacks_WMSQueue中已存在包裹成功", $"包裹：{orderPackInfo.orderPackNo}，状态从{previousStatus}更新为{existedOrderPackInQueue.Status}", logTags);
                        promptMessage = "更新OrderPacks_WMSQueue中已存在包裹成功";
                        result = true;
                    }
                }
                //insert
                else
                {
                    var orderInfo = heerpDbContext.Orders.FirstOrDefault(x => x.oid == orderPackInfo.oID);

                    OrderPackWMSQueueEntity queueEntity = new OrderPackWMSQueueEntity
                    {
                        OrderPackNo = orderPackInfo.orderPackNo,
                        Status = QueueStatusEnum.Created.ToString(),
                        CreationTime = DateTime.UtcNow,
                        Opid = orderPackInfo.opID,
                        Orderid = orderInfo == null ? null : orderInfo.orderid,
                        Partner = orderInfo == null ? null : orderInfo.partner
                    };

                    pmsDbContext.Add(queueEntity);
                    pmsDbContext.SaveChanges();
                    CLSLogger.Info("新增一个至OrderPacks_WMSQueue成功", $"包裹：{orderPackInfo.orderPackNo}", logTags);
                    promptMessage = "新增一个至OrderPacks_WMSQueue成功";
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                promptMessage = $"订单重推OrderPacks_WMSQueue出现异常，异常原因：{ex}";
                result = false;
                return result;
            }
        }
    }
}
