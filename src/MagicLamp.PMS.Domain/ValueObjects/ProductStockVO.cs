﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Domain.ValueObjects
{
    public class ProductStockVO
    {

        public string Sku { get; set; }
        public int Qty_available { get; set; }
        public int LockQty { get; set; }
        public int Stock { get; set; }  //spark add stock 19/06/2022
        public string ExpiredDate { get; set; }
        public string OriginalBestBefore { get; set; }
        public string OriginalHHBestBefore { get; set; }
        public int OriginalStock { get; set; }
        public int? OriginalHHStock { get; set; }

    }


}
