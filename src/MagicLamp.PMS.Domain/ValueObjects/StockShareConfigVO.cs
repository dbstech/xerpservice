﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Domain.ValueObjects
{
    public class StockShareConfigVO
    {

        public bool EnableStockShare { get; set; }
        public bool IgnoreOutOfStock { get; set; }
        public List<String> TestSKU { get; set; }
        public List<String> WaveRule { get; set; }
    }
}
