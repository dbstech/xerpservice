using System;
using MagicLamp.PMS.Enums;

namespace MagicLamp.PMS.Domain.ValueObjects
{
    public class LogUserActivityVO
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public UserActivityObjectType ObjectType { get; set; }
        public string ObjectId { get; set; }
        public string Action { get; set; }
        public string Content { get; set; }
        public string Before { get; set; }
        public string After { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
