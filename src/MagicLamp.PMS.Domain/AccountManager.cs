
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using System;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Abp.Dependency;
using System.Security.Principal;
using Abp.Domain.Services;
using Abp.Domain.Repositories;

namespace MagicLamp.PMS.Domain
{
    public class AccountManager : DomainService, IAccountManager
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        protected HttpContext HttpContext => _httpContextAccessor.HttpContext;
        private IRepository<UserActivityLogEntity, long> _userActivityLogRepository;
        public AccountManager(
            IHttpContextAccessor httpContextAccessor,
            IRepository<UserActivityLogEntity, long> userActivityLogRepository
        )
        {
            _httpContextAccessor = httpContextAccessor;
            _userActivityLogRepository = userActivityLogRepository;
        }

        public IIdentity GetIdentity()
        {
            return HttpContext.User.Identity;
        }

        public string GetUserName()
        {
            if (!HttpContext.User.HasClaim(x => x.Type == ClaimTypes.Name))
            {
                throw new Exception("User not authenticated or method called where HttpContext is not accessible");
            }
            return HttpContext.User.FindFirstValue(ClaimTypes.Name);
        }

        public string GetUserId()
        {
            if (!HttpContext.User.HasClaim(x => x.Type == ClaimTypes.NameIdentifier))
            {
                throw new Exception("User not authenticated or method called where HttpContext is not accessible");
            }
            return HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        }

        public void LogUserActivity(LogUserActivityVO input)
        {
            if (input.UserId.IsNullOrEmpty() || input.UserName.IsNullOrEmpty())
            {
                if (HttpContext == null)
                {
                    throw new Exception("User not authenticated or method called where HttpContext is not accessible");
                }
                input.UserId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
                input.UserName = HttpContext.User.FindFirstValue(ClaimTypes.Name);
            }

            UserActivityLogEntity userActivityLog = new UserActivityLogEntity
            {
                ObjectType = input.ObjectType,
                ObjectId = input.ObjectId,
                Action = input.Action,
                Content = input.UserName + input.Content,
                CreatedAt = input.CreatedAt ?? DateTime.UtcNow,
                UserId = input.UserId,
                UserName = input.UserName,
                Before = input.Before,
                After = input.After
            };

            _userActivityLogRepository.Insert(userActivityLog);
        }
    }
}
