﻿using Abp.Dependency;
using Abp.Extensions;
using Abp.Runtime.Caching;
using MagicLamp.PMS.DTO;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.DTOs.ThirdPart;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Common;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Proxy.FreightsAPI;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.PMS.Domain
{
    public class SplitOrderDomainService
    {
        private const string ConfigAutoSplitOrderFreight = "AutoSplitOrderFreight";
        private const string ConfigAUExpressOrderConfig = "AUExpressOrderConfig";

        HEERPDbContext heerpDbContext = null;
        PMSDbContext pmsDBContext = null;


        public SplitOrderDomainService()
        {
            heerpDbContext = new HEERPDbContextFactory().CreateDbContext();
            pmsDBContext = new PMSDbContextFactory().CreateDbContext();
        }

        private List<AUExpressShipmentOrderDTO> AssembleAUExpressShipmentOrderDTO(AUExpressOrderConfig auExpressOrderConfig,
            OrderEntity orderInfo, OrderPackEntity orderPackInfo, List<OrderPackDetailEntity> listOrderPackDetail, ref string promptMessage)
        {
            List<AUExpressShipmentOrderDTO> result = new List<AUExpressShipmentOrderDTO>();

            AUExpressShipmentOrderDTO auShipmentOrderBasic = new AUExpressShipmentOrderDTO
            {
                ERPOrderID = orderPackInfo.orderPackNo,
                Opid = orderPackInfo.opID,
                Seq = orderInfo.seq,
                MemberId = auExpressOrderConfig.MemberId,
                //BrandId = auExpressOrderConfig.BrandId,
                BrandId = 3,
                SenderName = auExpressOrderConfig.SenderName,
                SenderPhone = auExpressOrderConfig.SenderPhone,
                SenderAddr1 = auExpressOrderConfig.SenderAddr1,
                ReceiverName = orderPackInfo.rec_name,
                ReceiverPhone = orderPackInfo.rec_phone,
                ReceiverProvince = orderInfo.province,
                ReceiverCity = orderInfo.city,
                ReceiverDistrict = orderInfo.district,
                ReceiverAddr1 = orderPackInfo.rec_addr,
                Town = orderInfo.town,
                ReceiverPhotoId = orderInfo.idno,
                OrderShipment = orderInfo.shipment
            };


            if (auShipmentOrderBasic.ReceiverProvince.IsNullOrEmpty() || auShipmentOrderBasic.ReceiverCity.IsNullOrEmpty())
            {
                promptMessage = string.Format("包裹号：{0}，省/市/区 信息为空，请尝试重新保存修改包裹收件地址；", orderPackInfo.orderPackNo);
                AUExpressProxy.SendAURelatedAddressErrorNotificationToDingDing(auShipmentOrderBasic, "省/市/区 信息为空，请尝试重新修改保存包裹收件地址");

                return null;
            }

            //如有精确地址则使用不含省市的地址
            if (!auShipmentOrderBasic.Town.IsNullOrWhiteSpace())
            {
                auShipmentOrderBasic.ReceiverAddr1 = string.Format("{0}{1}", auShipmentOrderBasic.ReceiverDistrict, auShipmentOrderBasic.Town);
            }

            //replace invalid symbol to empty string
            if (!auShipmentOrderBasic.ReceiverAddr1.IsNullOrWhiteSpace())
            {
                auShipmentOrderBasic.ReceiverAddr1 = auShipmentOrderBasic.ReceiverAddr1.Trim()
                    .Replace("?", string.Empty)
                    .Replace("？", string.Empty)
                    .Replace("。", string.Empty)
                    .Replace(".", string.Empty)
                    .Replace("，", string.Empty)
                    .Replace(",", string.Empty);
            }

            //对city做特殊处理
            if (auShipmentOrderBasic.ReceiverCity.Contains("省直辖县") && auShipmentOrderBasic.ReceiverDistrict.IsNullOrEmpty())
            {
                auShipmentOrderBasic.ReceiverCity = auShipmentOrderBasic.ReceiverDistrict;
            }

            if (auShipmentOrderBasic.ReceiverCity.IsNullOrWhiteSpace())
            {
                auShipmentOrderBasic.ReceiverCity = auShipmentOrderBasic.ReceiverCity.Trim();
            }


            foreach (var item in listOrderPackDetail)
            {
                AUExpressShipmentOrderDTO auExpressShipmentOrderItem = auShipmentOrderBasic;
                auExpressShipmentOrderItem.ShipmentContent = item.product_name;

                if (!auExpressOrderConfig.PrefixContent.IsNullOrEmpty())
                {
                    auExpressShipmentOrderItem.ShipmentContent = string.Format("{0}-{1}", auExpressOrderConfig.PrefixContent, auExpressShipmentOrderItem.ShipmentContent);
                }

                result.Add(auExpressShipmentOrderItem);
            }

            if (result.IsNullOrEmpty())
            {
                promptMessage = string.Format("包裹号：{0}，包裹详情为空。", orderPackInfo.orderPackNo);
                return null;
            }

            return result;
        }

        private FreightCreateOrderInputDTO AssembleFreightCreateOrderInputDTO(AutoSplitOrderFreightConfig freightSetting, OrderEntity orderInfo,
            OrderPackEntity orderPackInfo, List<OrderPackDetailEntity> listOrderPackDetail, ref string promptMessage)
        {
            FreightCreateOrderInputDTO result = new FreightCreateOrderInputDTO
            {
                AppKey = freightSetting.ThirdPartyAPIAppKey,
                AppSecret = freightSetting.ThirdPartyAPIAppSecret,
                FreightKey = freightSetting.FreightID,
                OrderID = orderInfo.orderid,
                FreightTrackNo = orderPackInfo.carrierId,
                ParcelWeight = orderPackInfo.packWeight.HasValue ? orderPackInfo.packWeight.Value : 0,
                ParcelNo = orderPackInfo.orderPackNo,
                SenderName = "Australia",
                SenderPhone = "0064 9 838 8681",
                SenderAddress = "",
                SenderPostCode = "1061",
                ReceiverName = orderPackInfo.rec_name,
                ReceiverPhone = orderPackInfo.rec_phone,
                ReceiverTown = orderInfo.town,
                ReceiverProvince = orderInfo.province,
                ReceiverCity = orderInfo.city,
                ReceiverDistrict = orderInfo.district,
                ReceiverAddress = orderPackInfo.rec_addr,
                ReceiverIdNo = orderInfo.idno,
                GoodItems = new List<GoodsItemDTO>()
            };

            if (result.ReceiverProvince.IsNullOrEmpty() || result.ReceiverCity.IsNullOrEmpty())
            {
                CLSLogger.Error("创建第三方物流订单失败", string.Format("订单号：{0}，省 / 市 / 区 信息为空，请尝试重新修改保存包裹收件地址", result.OrderID), null);
                promptMessage = "创建第三方物流订单失败，省 / 市 / 区 信息为空，请尝试重新修改保存包裹收件地址";
                return null;
            }

            //如有精确地址则使用不含省市的地址
            if (!result.ReceiverTown.IsNullOrWhiteSpace())
            {
                result.ReceiverAddress = string.Format("{0}{1}", result.ReceiverDistrict, result.ReceiverTown);
            }

            //对city做特殊处理
            if (result.ReceiverCity.Contains("省直辖县") && !result.ReceiverDistrict.IsNullOrEmpty())
            {
                result.ReceiverCity = result.ReceiverDistrict;
            }

            if (!result.ReceiverCity.IsNullOrWhiteSpace())
            {
                result.ReceiverCity = result.ReceiverCity.Trim();
            }

            foreach (var item in listOrderPackDetail)
            {
                result.GoodItems.Add(new GoodsItemDTO
                {
                    Name = item.product_name,
                    Quantity = item.qty.ToString(),
                    Price = item.price.HasValue ? item.price.Value : 0,
                    Weight = item.weight.HasValue ? item.weight.Value : 0,
                    SKU = item.sku,
                    Unit = "件"
                });
            }
            
            return result;
        }

        private void UpdateAUCarrierIDForOrderPack(HEERPDbContext heerpDbContext, OrderPackEntity orderPackInfo, AUExpressShipmentOrderResponseDTO responseAUExpress, ref string promptMessage)
        {
            int affectedCount = 0;

            foreach (var item in responseAUExpress.OrderInfos)
            {
                if (item.ERPOrderID.IsNullOrEmpty() || item.AUExpressShipmentOrderID.IsNullOrEmpty())
                {
                    promptMessage += "发现有订单号码为空；" + affectedCount;
                }

                orderPackInfo.carrierId = item.AUExpressShipmentOrderID;
            }

            heerpDbContext.SaveChanges();
        }

        private void UpdateThirdPartyCarrierIDForOrderPack(HEERPDbContext heerpDbContext, OrderPackEntity orderPackInfo, FreightCreateOrderOutputDTO freightCreateOrderOutput, AutoSplitOrderFreightConfig freightSetting)
        {
            orderPackInfo.carrierId = freightCreateOrderOutput.CourierTrackNo;
            orderPackInfo.freightId = freightSetting.FreightID;
            orderPackInfo.invoiceUrl = freightCreateOrderOutput.CourierReciept;

            heerpDbContext.SaveChanges();
        }

        public List<AutoSplitOrderFreightConfig> GetAutoSplitOrderFreightConfig()
        {
            var config = heerpDbContext.APIConfigs.FirstOrDefault(x => x.ConfigKey == ConfigAutoSplitOrderFreight);

            if (config == null || config.ConfigValue.IsNullOrEmpty())
            {
                return null;
            }

            List<AutoSplitOrderFreightConfig> listAutoSplitOrderFreightConfig = null;

            try
            {
                listAutoSplitOrderFreightConfig = config.ConfigValue.ConvertFromJsonString<List<AutoSplitOrderFreightConfig>>();
                return listAutoSplitOrderFreightConfig;
            }
            catch (Exception ex)
            {
                throw new Exception("converting AutoSplitOrderFreight experienced an exception", ex);
            }
        }

        public List<AUExpressOrderConfig> GetAUExpressOrderConfig()
        {
            var config = heerpDbContext.APIConfigs.FirstOrDefault(x => x.ConfigKey == ConfigAUExpressOrderConfig);

            if (config == null || config.ConfigValue.IsNullOrEmpty())
            {
                return null;
            }

            List<AUExpressOrderConfig> listAUExpressOrderConfig = null;

            try
            {
                listAUExpressOrderConfig = config.ConfigValue.ConvertFromJsonString<List<AUExpressOrderConfig>>();
                return listAUExpressOrderConfig;
            }
            catch (Exception ex)
            {
                throw new Exception("converting AUExpressOrderConfig experienced an exception", ex);
            }
        }

        /// <summary>
        /// 自动分配物流单号给包裹
        /// </summary>
        /// <param name="orderPackInfo"></param>
        /// <param name="promptMessage"></param>
        /// <returns></returns>
        public bool AutoFillCarrierId(AutoSplitOrderFreightConfig freightSetting, OrderPackEntity orderPackInfo, ref string promptMessage)
        {
            if (freightSetting == null)
            {
                return AutoFillCarrierIdBasedOnBarcodesSeq(orderPackInfo, ref promptMessage);
            }

            //澳邮
            if (freightSetting.IsAUExpress)
            {
                return CreateAUShipmentOrder(orderPackInfo, ref promptMessage, true);
            }
            //对接第三方物流API
            else if (freightSetting.IsCreateOrderByAPI)
            {
                return CreateThirdPartyShipmentOrderByOrderPackNo(freightSetting, orderPackInfo, ref promptMessage, null, false, true);
            }
            else
            {
                return AutoFillCarrierIdBasedOnBarcodesSeq(orderPackInfo, ref promptMessage);
            }
        }

        /// <summary>
        /// 自动生成运单
        /// </summary>
        /// <param name="orderPackInfo"></param>
        /// <param name="promptMessage"></param>
        /// <returns></returns>
        public bool AutoFillCarrierIdBasedOnBarcodesSeq(OrderPackEntity orderPackInfo, ref string promptMessage)
        {
            bool result = true;

            var barcodesSeqInfo = heerpDbContext.BarcodeSeqs.FirstOrDefault(x => x.freightid == orderPackInfo.freightId);

            if (barcodesSeqInfo == null)
            {
                promptMessage = "包裹对应物流不支持自动分配物流单号";
                result = false;
                return result;
            }

            if (barcodesSeqInfo.last_used.HasValue && barcodesSeqInfo.last_used >= barcodesSeqInfo.edno)
            {
                promptMessage = "上次使用段号已超出最大号段";
                result = false;
                return result;
            }

            if (barcodesSeqInfo.last_used.HasValue && barcodesSeqInfo.last_used + 1 > barcodesSeqInfo.edno)
            {
                promptMessage = "物流号段已不够本次自动分配，请更新号段";
                result = false;
                return result;
            }

            long currentCarrierNo = barcodesSeqInfo.last_used.Value + 1;

            string carrierIdTobeUsed = barcodesSeqInfo.prefix + currentCarrierNo.ToString() + barcodesSeqInfo.postfix;

            bool existedCarrierID = false;
            int maxAttempts = 10;
            int currentAttempts = 0;

            do
            {
                existedCarrierID = heerpDbContext.OrderPacks.Any(x => x.carrierId == carrierIdTobeUsed);
                if (existedCarrierID)
                {
                    currentCarrierNo++;
                    carrierIdTobeUsed = barcodesSeqInfo.prefix + currentCarrierNo.ToString() + barcodesSeqInfo.postfix;
                }
            }
            while (existedCarrierID && currentAttempts < maxAttempts);

            orderPackInfo.carrierId = carrierIdTobeUsed;
            barcodesSeqInfo.last_used = currentCarrierNo;

            heerpDbContext.SaveChanges();
            return result;
        }

        /// <summary>
        /// 创建澳邮运单
        /// </summary>
        /// <param name="orderPackInfo"></param>
        /// <param name="promptMessage"></param>
        /// <param name="ignoreFreezingStatus"></param>
        /// <returns></returns>
        public bool CreateAUShipmentOrder(OrderPackEntity orderPackInfo, ref string promptMessage, bool ignoreFreezingStatus = false)
        {
            bool result = false;
            AUExpressOrderConfig auExpressOrderConfig = null;
            try
            {
                if (!ignoreFreezingStatus && orderPackInfo.freezeFlag != (int)OrderPackFreezeFlagEnum.未冻结)
                {
                    promptMessage = "生成澳邮物流单失败，包裹为冻结状态，自动分配物流单号无法生成澳邮物流单";
                    return result;
                }

                var listAUExpressOrderConfig = GetAUExpressOrderConfig();

                if (listAUExpressOrderConfig.IsNullOrEmpty())
                {
                    promptMessage = $"生成澳邮物流单失败。失败原因：未找到AUExpressOrderConfig配置。请重试";
                    return result;
                }

                if (orderPackInfo.exportFlag.HasValue)
                {
                    auExpressOrderConfig = listAUExpressOrderConfig.FirstOrDefault(x => x.ExportFlag == orderPackInfo.exportFlag.Value);

                    if (auExpressOrderConfig == null)
                    {
                        promptMessage = $"生成澳邮物流单失败。失败原因：根据包裹exportFlag，未找到对应AUExpressOrderConfig配置，请检查澳邮创建运单配置";
                        return result;
                    }
                }

                var listOrderPackDetail = heerpDbContext.OrderPackDetails.Where(x => x.opid == orderPackInfo.opID && x.flag == 1).ToList();

                if (listOrderPackDetail.IsNullOrEmpty())
                {
                    promptMessage = $"生成澳邮物流单失败。失败原因：未找到包裹详情。";
                    return result;
                }

                var orderInfo = heerpDbContext.Orders.FirstOrDefault(x => x.oid == orderPackInfo.oID);

                if (orderInfo == null)
                {
                    promptMessage = $"生成澳邮物流单失败。失败原因：未找到相关订单。";
                    return result;
                }

                List<AUExpressShipmentOrderDTO> inputForAUExpressCreateShipment = new List<AUExpressShipmentOrderDTO>();

                //assemble
                var auExpressInput = AssembleAUExpressShipmentOrderDTO(auExpressOrderConfig, orderInfo, orderPackInfo, listOrderPackDetail, ref promptMessage);

                if (auExpressInput.IsNullOrEmpty())
                {
                    return result;
                }

                var config = heerpDbContext.APIConfigs.FirstOrDefault(x => x.ConfigKey == "AUExpressAccount");
                var jObject = JObject.Parse(config.ConfigValue);
                if (jObject == null || !jObject.HasValues)
                {
                    throw new Exception("请检查澳邮账号配置");
                }
                string username = jObject["UserName"].Value<string>();
                string password = jObject["Password"].Value<string>();

                AUExpressProxy auExpressProxy = new AUExpressProxy(username, password);

                var responseAUExpress = auExpressProxy.CreateShipmentOrder(auExpressInput);

                if (responseAUExpress == null || !responseAUExpress.IsSuccess || !responseAUExpress.ErrorMsg.IsNullOrEmpty())
                {
                    promptMessage = $"生成澳邮物流单失败：{responseAUExpress.ErrorMsg}";
                    return result;
                }

                //update carrierId for orderpack
                UpdateAUCarrierIDForOrderPack(heerpDbContext, orderPackInfo, responseAUExpress, ref promptMessage);

                result = true;
                return result;
            }
            catch (Exception ex)
            {
                promptMessage += "生成澳邮物流单发生异常：" + ex.ToString();
                result = false;
                return result;
            }
            
        }

        /// <summary>
        /// 创建第三方物流公司运单
        /// </summary>
        /// <returns></returns>
        public bool CreateThirdPartyShipmentOrderByOrderPackNo(AutoSplitOrderFreightConfig freightSetting, OrderPackEntity orderPackInfo, 
            ref string promptMessage, string createOrderFreightID, bool ignoreCarrierID = false, bool ignoreFreezingStatus = false)
        {
            bool result = false;

            try
            {
                if (!ignoreFreezingStatus && orderPackInfo.freezeFlag != (int)OrderPackFreezeFlagEnum.未冻结)
                {
                    promptMessage = "创建第三方物流订单失败，包裹为冻结状态，自动分配物流单号无法创建第三方物流公司运单";
                    return result;
                }

                if (!ignoreCarrierID && (orderPackInfo.carrierId != null || orderPackInfo.carrierId == ""))
                {
                    promptMessage = "创建第三方物流订单失败，包裹已有物流单号，无法重复生成。";
                    return result;
                }

                var orderInfo = heerpDbContext.Orders.FirstOrDefault(x => x.oid == orderPackInfo.oID);

                if (orderInfo == null)
                {
                    promptMessage = $"创建第三方物流订单失败。失败原因：未找到相关订单。";
                    return result;
                }

                var listOrderPackDetail = heerpDbContext.OrderPackDetails.Where(x => x.opid == orderPackInfo.opID && x.flag == 1).ToList();

                if (listOrderPackDetail.IsNullOrEmpty())
                {
                    promptMessage = $"创建第三方物流订单失败。失败原因：未找到包裹详情。";
                    return result;
                }

                //assemble third party
                var freightCreateOrderInput = AssembleFreightCreateOrderInputDTO(freightSetting, orderInfo, orderPackInfo, listOrderPackDetail, ref promptMessage);

                if (freightCreateOrderInput == null)
                {
                    return result;
                }

                if (!createOrderFreightID.IsNullOrWhiteSpace())
                {
                    freightCreateOrderInput.FreightKey = createOrderFreightID;
                }

                //create freight
                var freightCreateOrderOutput = CreateFreightOrder(freightCreateOrderInput);
                StringBuilder log = new StringBuilder();

                if (freightCreateOrderOutput == null || !freightCreateOrderOutput.Success)
                {
                    log.AppendFormat("创建第三方物流单失败 ：{0}，{1}；", (freightCreateOrderOutput == null ? string.Empty : freightCreateOrderOutput.Message),
                            freightCreateOrderInput.ParcelNo);
                    if (freightCreateOrderOutput.Message != null && freightCreateOrderOutput.Message.Contains("收件"))
                    {
                        SendAddressErrorNotificationToDingDing(freightCreateOrderInput, freightCreateOrderOutput.Message);
                    }

                    promptMessage = log.ToString();
                    return result;
                }

                if (freightCreateOrderOutput.CourierTrackNo.IsNullOrEmpty())
                {
                    log.AppendFormat("创建第三方物流单失败，物流追踪单号为空 {0}；", freightCreateOrderInput.FreightTrackNo);

                    promptMessage = log.ToString();
                    return result;
                }

                UpdateThirdPartyCarrierIDForOrderPack(heerpDbContext, orderPackInfo, freightCreateOrderOutput, freightSetting);

                result = true;
                return result;
            }
            catch (Exception ex)
            {
                promptMessage += "创建第三方物流单发生异常：" + ex.ToString();

                result = false;
                return result;
            }
        }

        public FreightCreateOrderOutputDTO CreateFreightOrder(FreightCreateOrderInputDTO inputDTO)
        {
            var tags = new Dictionary<string, string>
            {
                ["type"] = "SplitOrderDomainService",
                ["method"] = "CreateFreightOrder"
            };

            FreightCreateOrderOutputDTO result = new FreightCreateOrderOutputDTO();

            if (inputDTO != null)
            {
                tags["orderid"] = inputDTO.OrderID;
                tags["orderpackno"] = inputDTO.ParcelNo;
                string reqJson = inputDTO.ToJsonString();
                CLSLogger.Info("CreateFreightOrder请求报文", reqJson, tags);
            }
            else
            {
                result.Message = "request is null";
                CLSLogger.Info("CreateFreightOrder请求报文", "request is null", tags);
                return result;
            }


            if (!inputDTO.ReceiverAddress.IsNullOrEmpty())
            {
                inputDTO.ReceiverAddress = inputDTO.ReceiverAddress.Trim().Replace("?", string.Empty).Replace("？", string.Empty)
                    .Replace("。", string.Empty);
            }

            try
            {
                switch (inputDTO.FreightKey)
                {
                    case "ydtsf":
                        ParcelTrackNoEntity parcelTrackNoEntity = heerpDbContext.ParcelTrackNos.FirstOrDefault(x => x.IsUsed == 0 && x.FreightId == inputDTO.FreightKey);
                        if (parcelTrackNoEntity == null)
                        {
                            result.Message = "创建订单失败，原因：易达通顺丰已无可用运单号，请及时录入";
                            return result;
                        }
                        inputDTO.FreightTrackNo = parcelTrackNoEntity.TrackNo;
                        YdtFreightProxy ydtFreightProxy = new YdtFreightProxy();
                        result = ydtFreightProxy.CreateOrder(inputDTO);
                        if (result.Success && !result.CourierReciept.IsNullOrEmpty())
                        {
                            parcelTrackNoEntity.IsUsed = 1;
                        }
                        break;
                    //长江大客户
                    case "ChangJiangExpressCustomizeTrackNo":
                    case "ChangJiangExpress":
                        //get barcode
                        List<string> listProductBySKU = new List<string>();
                        foreach (var product in inputDTO.GoodItems)
                        {
                            listProductBySKU.Add(product.SKU);
                        }

                        var products = pmsDBContext.Products.Where(x => listProductBySKU.Contains(x.SKU)).ToList();
                        if (products.IsNullOrEmpty())
                        {
                            result.Message = "创建订单失败，原因：产品无对应Barcode，请检查SKU及对应Barcode";
                            return result;
                        }
                        else
                        {
                            for (int i = 0; i < inputDTO.GoodItems.Count; i++)
                            {
                                var barcode = products.FirstOrDefault(x => x.SKU == inputDTO.GoodItems[i].SKU);
                                if (barcode == null || barcode.Barcode1.IsNullOrWhiteSpace())
                                {
                                    result.Message = $"未找到：{inputDTO.GoodItems[i].SKU} 对应的barcode";
                                    return result;
                                }
                                inputDTO.GoodItems[i].Barcode = barcode.Barcode1;
                            }
                        }

                        if (inputDTO.FreightKey == "ChangJiangExpressCustomizeTrackNo")
                        {
                            result = ChangJiangExpressProxy.CreateNewOrder(inputDTO, "neworderext");
                        }
                        else
                        {
                            result = ChangJiangExpressProxy.CreateNewOrder(inputDTO);
                        }

                        break;
                    default:
                        result.Message = "invalid freight key";
                        return result;
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("CreateFreightOrder发生异常", ex, tags);

                result.Message = "CreateFreightOrder发生异常：" + ex.Message;
                return result;
            }
            finally
            {
                CLSLogger.Info("CreateFreightOrder响应报文", result.ToJsonString(), tags);
            }

            return result;
        }

        public void SendAddressErrorNotificationToDingDing(FreightCreateOrderInputDTO order, string error)
        {
            try
            {
                string key = string.Format(CacheKeys.ADDRESSERROR, order.ParcelNo);

                var Cache = IocManager.Instance.Resolve<ICacheManager>();

                var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);
                var val = cacheManager.GetOrDefault(key);

                if (val == null)
                {
                    string title = "订单包裹生成自动运单号失败";
                    StringBuilder text = new StringBuilder();
                    text.AppendFormat("### 以下订单包裹生成自动运单号失败，错误原因：' {0} ' \n\n", error);
                    text.AppendFormat("[{0}](https://xerp-next.aladdin.nz/#/orderManagement/parcel/form?orderPackNo={1})  \n\n", order.ParcelNo, order.ParcelNo);
                    text.AppendFormat("------------以下是收件信息------------- \n\n");
                    text.AppendFormat("收件人：{0} \n\n", order.ReceiverName);
                    text.AppendFormat("收件省份：{0} \n\n", order.ReceiverProvince);
                    text.AppendFormat("收件城市：{0} \n\n", order.ReceiverCity);
                    text.AppendFormat("收件地址：{0} \n\n", order.ReceiverAddress);
                    DDNotificationProxy.MarkdownNotification(title, text.ToString(), "07bd13e4883f31bbab094c38a3c08224bdb3f5e4fdf9b011f7de5369752bdc4f");
                    cacheManager.Set(key, 1, TimeSpan.Zero, TimeSpan.FromDays(1));
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
