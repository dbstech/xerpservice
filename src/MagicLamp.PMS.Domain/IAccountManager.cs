using Abp.Domain.Services;
using System.Security.Principal;
using MagicLamp.PMS.Domain.ValueObjects;

namespace MagicLamp.PMS.Domain
{
    public interface IAccountManager : IDomainService
    {
        IIdentity GetIdentity();
        string GetUserId();
        string GetUserName();
        void LogUserActivity(LogUserActivityVO input);
    }
}