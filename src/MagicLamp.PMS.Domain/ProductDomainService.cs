﻿using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Caching;
using Castle.DynamicProxy.Generators;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.DTO;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Entities.DBQueryTypes;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using MagicLamp.PMS.Infrastructure.Common;
using System.Threading.Tasks;
using MagicLamp.PMS.DTOs.Enums;
using Abp.Json;

namespace MagicLamp.PMS.Domain
{
    public class ProductDomainService : BaseDomainService
    {

        // private IRepository<ProductEntity, string> productRepository;


        // public ProductDomainService(
        //    IRepository<ProductEntity, string> _productRepository
        //)
        // {
        //     productRepository = _productRepository;
        // }

        PMSDbContext pMSDbContext = null;
        HEERPDbContext hEERPDbContext = null;
        FluxWMSDbContext fluxWMSDbContext = null;
        public ICacheManager Cache { get; set; }

        public ProductDomainService()
        {
            pMSDbContext = new PMSDbContextFactory().CreateDbContext();
            hEERPDbContext = new HEERPDbContextFactory().CreateDbContext();
            fluxWMSDbContext = new FluxWMSDbContextFactory().CreateDbContext();
            Cache = IocManager.Instance.Resolve<ICacheManager>();
        }


        public List<ProductEntity> GetProductsFromCache(List<string> skus)
        {
            var cacheKeys = skus.Select(x => string.Format(CacheKeys.PRODUCTKEYFORMAT, x.Trim().ToUpper())).ToList();
            var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);

            List<ProductEntity> products = new List<ProductEntity>();

            List<ProductEntity> cacheProducts = cacheManager.GetOrDefault<string, ProductEntity>(cacheKeys.ToArray()).ToList();
            cacheProducts.RemoveAll(x => x == null);

            List<string> noCacheSkus = new List<string>();

            foreach (var sku in skus)
            {
                var currentProduct = cacheProducts.FirstOrDefault(x => x.SKU.Trim().ToUpper() == sku.Trim().ToUpper());

                if (currentProduct == null)
                {
                    noCacheSkus.Add(sku);
                }
                else
                {
                    products.Add(currentProduct);
                }
            }

            if (!noCacheSkus.IsNullOrEmpty())
            {
                var dbProducts = pMSDbContext.Products.Where(x => noCacheSkus.Contains(x.SKU)).ToList();
                products.AddRange(dbProducts);

                dbProducts.ForEach(x =>
                {
                    cacheManager.Set(string.Format(CacheKeys.PRODUCTKEYFORMAT, x.SKU.Trim().ToUpper()), x, TimeSpan.FromHours(24));
                });
            }

            return products;
        }

        public async Task<List<ProductEntity>> GetProductsFromCacheAsync(List<string> skus)
        {
            var cacheKeys = skus.Select(x => string.Format(CacheKeys.PRODUCTKEYFORMAT, x.Trim().ToUpper())).ToList();
            var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);

            List<ProductEntity> products = new List<ProductEntity>();

            List<ProductEntity> cacheProducts = (await cacheManager.GetOrDefaultAsync<string, ProductEntity>(cacheKeys.ToArray())).ToList();
            cacheProducts.RemoveAll(x => x == null);

            List<string> noCacheSkus = new List<string>();

            foreach (var sku in skus)
            {
                var currentProduct = cacheProducts.FirstOrDefault(x => x.SKU.Trim().ToUpper() == sku.Trim().ToUpper());

                if (currentProduct == null)
                {
                    noCacheSkus.Add(sku);
                }
                else
                {
                    products.Add(currentProduct);
                }
            }

            if (!noCacheSkus.IsNullOrEmpty())
            {
                var dbProducts = await pMSDbContext.Products.Where(x => noCacheSkus.Contains(x.SKU)).ToListAsync();
                products.AddRange(dbProducts);

                dbProducts.ForEach(async x =>
                {
                    await cacheManager.SetAsync(string.Format(CacheKeys.PRODUCTKEYFORMAT, x.SKU.Trim().ToUpper()), x, TimeSpan.FromHours(24));
                });
            }

            return products;
        }

        /// <summary>
        /// get product stock infos by order partner
        /// </summary>
        /// <param name="partner"></param>
        /// <param name="orderSkus"></param>
        /// <returns></returns>
        public List<ProductStockVO> GetStocks(string partner, List<string> skus)
        {
            var proInfos = GetProductsFromCache(skus);

            return this.GetStocks(partner, skus, proInfos);
        }


        public List<ProductStockVO> GetStocks(string partner, List<string> skus, List<ProductEntity> proInfos, StringBuilder log = null)
        {
            //Dictionary<string, string> clogTags = new Dictionary<string, string>
            //{
            //    ["type"] = "GetStocks",
            //    ["method"] = "GetStocks",
            //    ["orderid"] = skus.ToJsonString()
            //};

            System.Diagnostics.Stopwatch sw = new Stopwatch();
            sw.Start();

            var cacheKeys = skus.Select(x => string.Format(CacheKeys.PRODUCTSTOCKKEYFORMAT, x)).ToList();
            var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);

            List<ProductStockVO> productStocks = cacheManager.GetOrDefault<string, ProductStockVO>(cacheKeys.ToArray()).ToList();
            productStocks.RemoveAll(x => x == null);

            sw.Stop();

            if (log != null)
            {
                log.Append($" get stock cache: {sw.ElapsedMilliseconds} ms ");
            }

            List<string> noCacheSkus = skus.Where(x => !productStocks.Any(pro => pro.Sku.Trim().ToLower() == x.Trim().ToLower())).ToList();
            //CLSLogger.Info("GetStocks0", noCacheSkus.ToJsonString(), clogTags);



            if (!noCacheSkus.IsNullOrEmpty())
            {
                //CLSLogger.Info("GetStocks1", noCacheSkus.ToJsonString(), clogTags);
                if (log != null)
                {
                    log.Append($" no cache sku: {string.Join(",", noCacheSkus)} ");
                }

                sw.Restart();

                var dbProductStocks = hEERPDbContext.APIStocks.Where(x => noCacheSkus.Contains(x.SKU)).Select(x => new ProductStockVO
                {
                    Sku = x.SKU,
                    Qty_available = x.Qty_available,
                    ExpiredDate = x.Expired_date,
                    OriginalBestBefore = x.Expired_date,
                    OriginalHHBestBefore = x.Expiry_date_hh,
                    OriginalStock = x.Qty_available,
                    OriginalHHStock = x.Qty_hh,
                    LockQty = x.Qty_locked
                }).ToList();

                sw.Stop();

                if (log != null)
                {
                    log.Append($"query APIStocks db: {sw.ElapsedMilliseconds} ms;");
                }

                //if (!x.ExpiredDate.IsNullOrEmpty())
                //{
                //    x.ExpiredDate = x.ExpiredDate.Replace("只填写数字，以天为单位！", string.Empty);
                //}

                productStocks.AddRange(dbProductStocks);

                sw.Restart();
                dbProductStocks.ForEach(x =>
                {
                    cacheManager.Set(string.Format(CacheKeys.PRODUCTSTOCKKEYFORMAT, x.Sku), x, TimeSpan.FromMinutes(5));
                });

                sw.Stop();

                if (log != null)
                {
                    log.Append($"set APIStocks cache: {sw.ElapsedMilliseconds} ms;");
                }
            }

            sw.Restart();

            List<OrderSKULockForHHEntity> listAllSKULockHH = GetCurrentSKUStockLockForHH(skus);

            //hh stock lock
            /*foreach (var item in productStocks)
            {
                item.OriginalHHStock = item.OriginalHHStock.HasValue ? GetAvailableSKUHHStock(item.Sku, item.OriginalHHStock.Value, listAllSKULockHH) : item.OriginalHHStock;

                var proInfo = proInfos.FirstOrDefault(x => x.SKU.Trim().ToLower() == item.Sku.Trim().ToLower());

                if ((!string.IsNullOrEmpty(partner) && partner == "HH") || (item.Qty_available <= 0 && proInfo != null && proInfo.IsHHPurchase == true))
                {
                    //HH订单 或者 MFD库存不足且为A类品  时使用HH的库存
                    if (item.OriginalHHStock != null)
                    {
                        item.Qty_available = item.OriginalHHStock.Value;
                        item.ExpiredDate = item.OriginalHHBestBefore;
                    }
                }
            }*/
            sw.Stop();

            if (log != null)
            {
                log.Append($"set HH cache: {sw.ElapsedMilliseconds} ms;");
            }
            //CLSLogger.Info("GetStocks3", productStocks.ToJsonString(), clogTags);
            return productStocks;
        }

        public async Task<List<ProductStockVO>> GetStocksAsync(string partner, List<string> skus, List<ProductEntity> proInfos, StringBuilder log = null)
        {
            System.Diagnostics.Stopwatch sw = new Stopwatch();
            sw.Start();

            var cacheKeys = skus.Select(x => string.Format(CacheKeys.PRODUCTSTOCKKEYFORMAT, x)).ToList();
            var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);

            List<ProductStockVO> productStocks = (await cacheManager.GetOrDefaultAsync<string, ProductStockVO>(cacheKeys.ToArray())).ToList();
            productStocks.RemoveAll(x => x == null);

            sw.Stop();

            if (log != null)
            {
                log.Append($" get stock cache: {sw.ElapsedMilliseconds} ms ");
            }

            List<string> noCacheSkus = skus.Where(x => !productStocks.Any(pro => pro.Sku.Trim().ToLower() == x.Trim().ToLower())).ToList();

            if (!noCacheSkus.IsNullOrEmpty())
            {
                if (log != null)
                {
                    log.Append($" no cache sku: {string.Join(",", noCacheSkus)} ");
                }

                sw.Restart();

                var dbProductStocks = await hEERPDbContext.APIStocks.Where(x => noCacheSkus.Contains(x.SKU)).Select(x => new ProductStockVO
                {
                    Sku = x.SKU,
                    Qty_available = x.Qty_available,
                    ExpiredDate = x.Expired_date,
                    OriginalBestBefore = x.Expired_date,
                    OriginalHHBestBefore = x.Expiry_date_hh,
                    OriginalStock = x.Qty_available,
                    OriginalHHStock = x.Qty_hh,
                    LockQty = x.Qty_locked
                }).ToListAsync();

                sw.Stop();

                if (log != null)
                {
                    log.Append($"query APIStocks db: {sw.ElapsedMilliseconds} ms;");
                }

                productStocks.AddRange(dbProductStocks);

                sw.Restart();
                dbProductStocks.ForEach(async x =>
                {
                    await cacheManager.SetAsync(string.Format(CacheKeys.PRODUCTSTOCKKEYFORMAT, x.Sku), x, TimeSpan.FromMinutes(5));
                });

                sw.Stop();

                if (log != null)
                {
                    log.Append($"set APIStocks cache: {sw.ElapsedMilliseconds} ms;");
                }
            }

            sw.Restart();

            foreach (var item in productStocks)
            {
                var proInfo = proInfos.FirstOrDefault(x => x.SKU.Trim().ToLower() == item.Sku.Trim().ToLower());

                if ((!string.IsNullOrEmpty(partner) && partner == "HH") || (item.Qty_available <= 0 && proInfo != null && proInfo.IsHHPurchase == true))
                {
                    //HH订单 或者 MFD库存不足且为A类品  时使用HH的库存
                    if (item.OriginalHHStock != null)
                    {
                        item.Qty_available = item.OriginalHHStock.Value;
                        item.ExpiredDate = item.OriginalHHBestBefore;
                    }
                }
            }

            sw.Stop();

            if (log != null)
            {
                log.Append($"set HH cache: {sw.ElapsedMilliseconds} ms;");
            }

            return productStocks;
        }

        public void UpdateProductStockCache(List<APIStockEntity> stocks)
        {
            if (stocks.IsNullOrEmpty())
            {
                return;
            }

            var stockVOs = stocks.Select(x => new ProductStockVO
            {
                Sku = x.SKU,
                Qty_available = x.Qty_available,
                ExpiredDate = x.Expired_date,
                OriginalBestBefore = x.Expired_date,
                OriginalHHBestBefore = x.Expiry_date_hh,
                OriginalStock = x.Qty_available,
                OriginalHHStock = x.Qty_hh,
                LockQty = x.Qty_locked
            }).ToArray();

            UpdateProductStockCache(stockVOs);
        }


        public void UpdateProductStockCache(params ProductStockVO[] stockInfos)
        {

            if (stockInfos.IsNullOrEmpty())
            {
                return;
            }

            try
            {
                var stocks = stockInfos.ToList();
                stocks.RemoveAll(x => x == null);

                int size = stocks.Count;

                var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);

                for (int i = 0; i < size; i++)
                {
                    cacheManager.Set(
                        string.Format(CacheKeys.PRODUCTSTOCKKEYFORMAT, stocks[i].Sku),
                        stocks[i],
                        null,
                        TimeSpan.FromMinutes(5)
                    );
                }
            }
            catch (Exception ex)
            {
                CLSLogger.ErrorWithSentry("UpdateProductStockCacheException", ex, null);
            }
        }

        public void AddHHStockLockByOrderID(List<OrderSKULockForHHEntity> listOrderSKULockForHH)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["orderid"] = listOrderSKULockForHH.Select(x => x.OrderID).FirstOrDefault(),
                ["method"] = "AddHHStockLockByOrderID",
                ["type"] = "ProductDomainService"
            };

            CLSLogger.Info("新增HH锁库存sku记录input", $"{listOrderSKULockForHH.ToJsonString()}", logTags);

            if (listOrderSKULockForHH.IsNullOrEmpty())
            {
                return;
            }

            try
            {
                foreach (var item in listOrderSKULockForHH)
                {
                    pMSDbContext.OrderSKULockForHH.Add(item);
                }

                pMSDbContext.SaveChanges();
                CLSLogger.Info("新增HH锁库存sku记录成功", $"新增：{listOrderSKULockForHH.ToJsonString()}", logTags);
            }
            catch (Exception ex)
            {
                CLSLogger.Error("新增HH锁库存sku记录失败", $"失败原因：{ex}", logTags);
            }
        }

        public void AddHHStockLockByOpid(OrderPackEntity orderPackInfo, OrderPackDetailEntity detailItem, string status)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["orderpackno"] = orderPackInfo.orderPackNo,
                ["orderid"] = orderPackInfo.orderID,
                ["method"] = "AddHHStockLockByOpid",
                ["type"] = "ProductDomainService"
            };


            try
            {
                OrderSKULockForHHEntity entry = new OrderSKULockForHHEntity
                {
                    OrderID = orderPackInfo.orderID,
                    SKU = detailItem.sku,
                    QTY = detailItem.qty,
                    LockDuration = 172800,
                    CreationTime = DateTime.UtcNow,
                    opid = orderPackInfo.opID
                };

                var listHHStockLock = pMSDbContext.OrderSKULockForHH.Where(x => (x.opid.HasValue && x.opid.Value == orderPackInfo.opID)).ToList();

                if (listHHStockLock.IsNullOrEmpty())
                {
                    //lock
                    pMSDbContext.OrderSKULockForHH.Add(entry);
                    pMSDbContext.SaveChanges();

                    CLSLogger.Info($"新增HH锁库存sku记录成功，opid此前无对应记录，包裹在queue中状态为{status}", $"新增：{entry.ToJsonString()}", logTags);
                }
                else
                {
                    if (status == QueueStatusEnum.Created.ToString())
                    {
                        if (!listHHStockLock.Any(x => x.SKU.StandardizeSKU() == detailItem.sku.StandardizeSKU()))
                        {
                            //lock
                            pMSDbContext.OrderSKULockForHH.Add(entry);
                            pMSDbContext.SaveChanges();

                            CLSLogger.Info($"新增HH锁库存sku记录成功，opid及此sku此前无对应记录，包裹在queue中状态为{status}", $"新增：{entry.ToJsonString()}", logTags);
                        }
                        else
                        {
                            CLSLogger.Info($"无需新增HH锁库存sku记录，opid及此sku此前有对应记录，包裹在queue中状态为{status}", $"已有记录：{listHHStockLock.ToJsonString()}", logTags);
                        }
                    }
                    else if (status == QueueStatusEnum.OutOfStock.ToString())
                    {
                        var listValidLock = listHHStockLock.Where(x => x.ExpireTime >= DateTime.UtcNow).ToList();
                        if (listValidLock.IsNullOrEmpty())
                        {
                            foreach (var item in listHHStockLock)
                            {
                                if (item.SKU == detailItem.sku)
                                {
                                    item.LockDuration += 172800;
                                }
                            }

                            pMSDbContext.SaveChanges();

                            CLSLogger.Info($"刷新HH锁库存sku记录，包裹下现有sku锁定时间增加2天，包裹在queue中状态为{status}", $"刷新后的记录：{listHHStockLock.ToJsonString()}", logTags);
                        }
                        else
                        {
                            CLSLogger.Info($"无需刷新HH锁库存sku记录，opid此前有对应记录且未过期，包裹在queue中状态为{status}", $"已有记录：{listHHStockLock.ToJsonString()}", logTags);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Info($"新增HH锁库存sku记录失败", $"失败原因：{ex}", logTags);
            }
        }

        public void FreeHHStockLockByOrderID(string orderid)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["orderid"] = orderid,
                ["method"] = "FreeHHStockLockByOrderID",
                ["type"] = "ProductDomainService"
            };

            CLSLogger.Info("释放HH锁库存sku记录input", $"订单号{orderid}", logTags);

            try
            {
                var listHHLock = pMSDbContext.OrderSKULockForHH.Where(x => x.OrderID == orderid && x.ExpireTime >= DateTime.UtcNow).ToList();

                if (!listHHLock.IsNullOrEmpty())
                {
                    StringBuilder previousRecord = new StringBuilder();

                    foreach (var item in listHHLock)
                    {
                        previousRecord.Append(item.ToJsonString());

                        pMSDbContext.OrderSKULockForHH.Remove(item);
                    }

                    pMSDbContext.SaveChanges();

                    CLSLogger.Info("订单存在未过期HH锁库存，全部释放。", $"原记录{previousRecord}", logTags);
                }
                else
                {
                    CLSLogger.Info("订单不存在未过期HH锁库存。", $"订单号{orderid}", logTags);
                }

            }
            catch (Exception ex)
            {
                CLSLogger.Error("释放HH锁库存sku记录失败", $"失败原因：{ex}", logTags);
            }
        }

        public void FreeHHStockLockByOpid(OrderPackEntity orderPackInfo)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["trackid"] = orderPackInfo.opID.ToString(),
                ["orderpackno"] = orderPackInfo.orderPackNo,
                ["orderid"] = orderPackInfo.orderID,
                ["method"] = "FreeHHStockLockByOpid",
                ["type"] = "ProductDomainService"
            };

            CLSLogger.Info("包裹推富勒成功时释放HH锁库存sku记录input", $"包裹号{orderPackInfo.orderPackNo}", logTags);

            try
            {
                var listHHLock = pMSDbContext.OrderSKULockForHH.Where(x => x.opid.HasValue && x.opid.Value == orderPackInfo.opID && x.ExpireTime >= DateTime.UtcNow).ToList();

                if (!listHHLock.IsNullOrEmpty())
                {
                    StringBuilder previousRecord = new StringBuilder();

                    foreach (var item in listHHLock)
                    {
                        previousRecord.Append(item.ToJsonString());

                        pMSDbContext.OrderSKULockForHH.Remove(item);
                    }

                    pMSDbContext.SaveChanges();

                    CLSLogger.Info("包裹存在未过期HH锁库存，全部释放。", $"原记录{previousRecord}", logTags);
                }
                else
                {
                    CLSLogger.Info("包裹不存在未过期HH锁库存。", $"包裹号{orderPackInfo.orderPackNo}", logTags);
                }

            }
            catch (Exception ex)
            {
                CLSLogger.Error("包裹推富勒成功时释放HH锁库存sku记录失败", $"失败原因：{ex}", logTags);
            }
        }


        public List<OrderSKULockForHHEntity> GetCurrentSKUStockLockForHH(List<string> listSKU)
        {
            var listSKULockHH = pMSDbContext.OrderSKULockForHH.Where(x => listSKU.Contains(x.SKU, StringComparer.OrdinalIgnoreCase));

            var listValidSKULockHH = listSKULockHH.Where(x => x.ExpireTime >= DateTime.UtcNow).ToList();

            return listValidSKULockHH;
        }

        /// <summary>
        /// 获取减去lock数量的sku HH stock
        /// </summary>
        /// <param name="stock"></param>
        /// <returns></returns>
        public int GetAvailableSKUHHStock(string SKU, int stock, List<OrderSKULockForHHEntity> listSKULockHH)
        {
            var ListSKUWihHHLock = listSKULockHH.Where(x => x.SKU.StandardizeSKU() == SKU.StandardizeSKU() && x.ExpireTime >= DateTime.UtcNow).ToList();

            if (ListSKUWihHHLock.IsNullOrEmpty())
            {
                return stock;
            }
            else
            {
                Dictionary<string, string> logTags = new Dictionary<string, string>
                {
                    ["sku"] = SKU,
                    ["type"] = "ProductDomainService",
                    ["method"] = "GetAvailableSKUHHStock"
                };

                int hhStockLocked = ListSKUWihHHLock.Sum(x => x.QTY ?? 0);

                int availableHHStock = stock - hhStockLocked;

                CLSLogger.Info("SKU存在HH锁定库存，扣除锁定。", $"{SKU}的HH现有库存数{stock}，未过期锁定数{hhStockLocked}，可使用数量为：{availableHHStock}", logTags);

                return availableHHStock;
            }
        }

        public List<ProductStockVO> RecalculateSkuLockQty(List<string> skus, bool updateAvailableQty = true)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>();
            logTags["sku"] = string.Join(";", skus);
            logTags["type"] = "RecaculateSkuLockQty";
            logTags["method"] = "RecaculateSkuLockQtyBySkus";

            StringBuilder log = new StringBuilder();
            Stopwatch sw = new Stopwatch();
            sw.Start();

            try
            {
                //cache order lock
                DateTime cacheOrderStartDate = DateTime.Now.AddDays(-3);
                var cacheOrderLock = hEERPDbContext.CacheOrderDetails
                    .Where(x => skus.Contains(x.SKU) && x.CTime > cacheOrderStartDate)
                    .GroupBy(x => x.SKU)
                    .Select(x =>
                  new SimpleSkuQueryType
                  {
                      SKU = x.Key,
                      Qty = x.Sum(item => item.Qty)
                  }).ToList();

                //order pack lock
                var freezeFlags = new List<int> { 0, 7, 8 };
                var startDate = DateTime.Now.AddMonths(-1);

                StringBuilder freezeFlagsSql = new StringBuilder();
                StringBuilder skusSql = new StringBuilder();
                List<SqlParameter> parameters = new List<SqlParameter>();
                for (int i = 0; i < freezeFlags.Count; i++)
                {
                    freezeFlagsSql.Append($"@freezeFLags{i},");
                    parameters.Add(new SqlParameter("@freezeFLags" + i, freezeFlags[i]));
                }

                for (int i = 0; i < skus.Count; i++)
                {
                    skusSql.Append($"@skus{i},");
                    parameters.Add(new SqlParameter("@skus" + i, skus[i]));
                }
                freezeFlagsSql.Length--;
                skusSql.Length--;

                string orderpackSQL = $@" SELECT detail.sku AS 'SKU', detail.qty AS 'QTY', pack.orderpackno as 'orderPackNo' FROM dbo.orders_pack AS pack WITH (NOLOCK)
                         INNER JOIN dbo.orders_pack_detail AS detail WITH(NOLOCK)
                         ON pack.opid = detail.opid
                         WHERE
                         detail.flag = 1
                         AND detail.sku IN ({skusSql})
                        AND pack.[freezeflag]  in ({freezeFlagsSql})
                        AND pack.assign_time IS NULL AND pack.status= 0 AND pack.out_time IS NULL
                         AND pack.opt_time>=dateadd(day,-60, getdate())";

                var orderPackLocks = hEERPDbContext.SimpleSkuDBQuery.FromSql(orderpackSQL, parameters.ToArray()).ToList();

                //unpaid order lock
                var unpaidOrderLock = hEERPDbContext.OrderSkuLocks.Where(x => skus.Contains(x.SKU)).GroupBy(x => x.SKU)
                    .Select(x =>
                new SimpleSkuQueryType
                {
                    SKU = x.Key,
                    Qty = x.Sum(item => item.Qty)
                }).ToList();

                //unsplit order lock
                var orderStartDate = DateTime.Now.AddDays(-3);
                var unSplitOrderLock = (from order in hEERPDbContext.Orders
                                        join orderDetail in hEERPDbContext.OrderDetails
                                        on order.oid equals orderDetail.oid
                                        where order.status == 0 && order.valflag == 1 && order.orderCreateTime > orderStartDate
                                        && skus.Contains(orderDetail.sku)
                                        group orderDetail by orderDetail.sku into result
                                        select new SimpleSkuQueryType
                                        {
                                            SKU = result.Key,
                                            Qty = result.Sum(item => item.qty)
                                        }).ToList();


                sw.Stop();
                log.Append($"query lock qty time elapsed: {sw.ElapsedMilliseconds} ms; ");

                return RecalculateSkuLockQty(skus, cacheOrderLock, orderPackLocks, unSplitOrderLock, unpaidOrderLock, updateAvailableQty);
            }
            catch (Exception ex)
            {
                CLSLogger.Error("RecaculateSkuLockQtyException", ex, logTags);
                return null;
            }

        }

        public List<ProductStockVO> RecalculateSkuLockQty(List<string> skus, List<SimpleSkuQueryType> cacheOrderLock, List<SimpleSkuQueryType> orderPackLocks,
            List<SimpleSkuQueryType> unSplitOrderLock, List<SimpleSkuQueryType> unpaidOrderLock, bool updateAvailableQty = true)
        {

            Dictionary<string, string> logTags = new Dictionary<string, string>();
            logTags["type"] = "RecaculateSkuLockQty";

            StringBuilder log = new StringBuilder();
            Stopwatch sw = new Stopwatch();
            sw.Start();

            try
            {
                var orderPackNos = orderPackLocks.Select(x => x.OrderPackNo).ToList();
                var fluxOrderNos = fluxWMSDbContext.FluxDocOrderHeaders.Where(x => orderPackNos.Contains(x.OrderPackNo))
                    .Select(x => x.FluxOrderNo).ToList();

                var fluxOrderDetails = fluxWMSDbContext.FluxDocOrderDetails
                                       .Where(x => fluxOrderNos.Contains(x.OrderNo))
                                       .Select(x => new
                                       {
                                           x.SKU,
                                           x.LotAtt07,
                                           x.QtyOrdered,
                                           x.QtyAllocated
                                       })
                                       .AsEnumerable()
                                       .Select(x => new
                                       {
                                           x.SKU,
                                           QtyAllocated = string.IsNullOrWhiteSpace(x.LotAtt07) ? x.QtyAllocated : x.QtyOrdered
                                           //if LotAtt07 is not empty that means the SKU will be allocated from HH inventory, so should not be considered as part of lock quantity
                                       })
                                       .GroupBy(x => x.SKU)
                                       .Select(x => new
                                       {
                                           SKU = x.Key,
                                           Qty = (int)x.Sum(y => y.QtyAllocated)
                                       }).ToList();

                orderPackLocks = orderPackLocks.GroupBy(x => x.SKU).Select(x => new SimpleSkuQueryType
                {
                    SKU = x.Key,
                    Qty = x.Sum(item => item.Qty) - (fluxOrderDetails.FirstOrDefault(y => x.Key == y.SKU)?.Qty ?? 0)
                }).ToList();

                //concreate all lock qty
                var totalSkuLockQty = cacheOrderLock.Concat(orderPackLocks).Concat(unpaidOrderLock).Concat(unSplitOrderLock)
                    .GroupBy(x => x.SKU).Select(x => new SimpleSKUDTO
                    {
                        SKU = x.Key,
                        Qty = x.Sum(item => item.Qty)
                    }).ToList();

                var apiStocks = hEERPDbContext.APIStocks.Where(x => skus.Contains(x.SKU)).ToList();

                sw.Stop();
                log.Append($"query apistock time elapsed: {sw.ElapsedMilliseconds} ms; ");

                sw.Restart();

                foreach (var item in apiStocks)
                {
                    var lockQty = totalSkuLockQty.FirstOrDefault(x => x.SKU.Trim().ToLower() == item.SKU.Trim().ToLower());

                    if (lockQty == null)
                    {
                        //no any lock qty
                        item.Qty_locked = 0;
                        item.Qty_cache = 0;
                        item.Qty_orders_pack = 0;
                        item.Qty_orders = 0;
                        item.Updated_time = DateTime.Now;
                        item.Qty_available = updateAvailableQty ? item.Qty.Value : item.Qty_available;
                    }
                    else
                    {
                        var cacheLock = cacheOrderLock.FirstOrDefault(x => x.SKU.Trim().ToLower() == item.SKU.Trim().ToLower());
                        var orderPackLock = orderPackLocks.FirstOrDefault(x => x.SKU.Trim().ToLower() == item.SKU.Trim().ToLower());
                        var unSplitLock = unSplitOrderLock.FirstOrDefault(x => x.SKU.Trim().ToLower() == item.SKU.Trim().ToLower());

                        item.Qty_locked = lockQty.Qty;
                        item.Qty_cache = cacheLock == null ? 0 : cacheLock.Qty;
                        item.Qty_orders_pack = orderPackLock == null ? 0 : orderPackLock.Qty;
                        item.Qty_orders = unSplitLock == null ? 0 : unSplitLock.Qty;
                        item.Updated_time = DateTime.Now;
                        item.Qty_available = updateAvailableQty ? item.Qty.Value - item.Qty_locked : item.Qty_available;
                    }
                }

                hEERPDbContext.SaveChanges();


                 
                sw.Stop();
                log.Append($"update apistock time elapsed: {sw.ElapsedMilliseconds} ms; ");


                sw.Restart();
                var productStocks = apiStocks.Select(x => new ProductStockVO
                {
                    Sku = x.SKU,
                    Qty_available = x.Qty_available,
                    ExpiredDate = x.Expired_date,
                    OriginalBestBefore = x.Expired_date,
                    OriginalHHBestBefore = x.Expiry_date_hh,
                    OriginalStock = x.Qty_available,
                    OriginalHHStock = x.Qty_hh,
                    LockQty = x.Qty_locked,
                    //spark add stock 19/06/2022
                    Stock = x.SKU.StartsWith("AUBXD") ? x.Qty_available - 5 : 
                    pMSDbContext.Products.Where(y => y.SKU == x.SKU).FirstOrDefault().ECinxSKU !=null
                    ? (x.Qty_available + int.Parse(x.Qty_hh.ToString())) : x.Qty_available,
                
                }).ToList();

                if (updateAvailableQty)
                {
                    UpdateProductStockCache(productStocks.ToArray());
                }

                sw.Stop();
                log.Append($"update stock cache: {sw.ElapsedMilliseconds} ms; ");

                log.Append($"Sku lock qty detail: {totalSkuLockQty.ToJsonString()} ;");

                return productStocks;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("RecaculateSkuLockQtyException", ex, logTags);
                return null;
            }
            finally
            {
                CLSLogger.Info("RecaculateSkuLockQtyTimeElapsed", log.ToString(), logTags);
            }

        }


        public void UpdateProductsCache(List<string> skus)
        {
            var dbProducts = pMSDbContext.Products.Where(x => skus.Contains(x.SKU)).ToList();
            var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);

            int size = dbProducts.Count;

            for (int i = 0; i < size; i++)
            {
                cacheManager.Set(
                    string.Format(CacheKeys.PRODUCTKEYFORMAT, dbProducts[i].SKU),
                    dbProducts[i],
                    null,
                    TimeSpan.FromHours(24)
                );
            }

        }

        public List<string> GetVirtualStockSkuPrefix()
        {
            var cacheManager = Cache.GetCache(CacheKeys.JOBCACHEKEYNAME);
            var val = cacheManager.GetOrDefault(CacheKeys.PRODUCTROLEKEY);

            if (val != null)
            {
                return (List<string>)val;
            }
            else
            {
                var prefixs = hEERPDbContext.ProductRoles.Select(x => x.SkuPrefix).AsEnumerable().Distinct().ToList();
                cacheManager.Set(CacheKeys.PRODUCTROLEKEY, prefixs, TimeSpan.FromHours(24));
                return prefixs;
            }
        }

        public int FormalizeReserveSeconds(int? secondsReserved)
        {
            return secondsReserved.HasValue ? secondsReserved.Value : 3600;
        }

    }


}
