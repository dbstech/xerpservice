﻿using Abp.Dependency;
using Abp.Runtime.Caching;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.DTO;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MagicLamp.PMS.Infrastructure.Common;

namespace MagicLamp.PMS.Domain
{
    public class StockShareDomainService : BaseDomainService
    {

        HEERPDbContext hEERPDbContext = null;
        PMSDbContext pmsDbContext = null;

        public ICacheManager Cache { get; set; }

        public StockShareDomainService()
        {
            hEERPDbContext = new HEERPDbContextFactory().CreateDbContext();
            pmsDbContext = new PMSDbContextFactory().CreateDbContext();
            Cache = IocManager.Instance.Resolve<ICacheManager>();
        }



        public StockShareConfigVO GetStockShareConfig()
        {
            string key = "StockShareConfig";
            string cacheKey = string.Format(CacheKeys.APPCONFIGKEYFORMAT, key);

            var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);
            var val = cacheManager.GetOrDefault(cacheKey);

            if (val == null)
            {
                var stockShareConfig = hEERPDbContext.APIConfigs.FirstOrDefault(x => x.ConfigKey == key);
                //IsHHPurchase means stock share
                var listSharedSKU = pmsDbContext.Products.Where(x => x.IsHHPurchase.HasValue && x.IsHHPurchase == true && x.WarehouseID == 6).Select(x => x.SKU).ToList();
                
                if (stockShareConfig == null)
                {
                    throw new Exception("can not find StockShareConfig in Apiconfig table");
                }

                StockShareConfigVO config = null;

                try
                {
                    config = stockShareConfig.ConfigValue.ConvertFromJsonString<StockShareConfigVO>();
                    config.TestSKU = listSharedSKU;
                }
                catch (Exception ex)
                {
                    throw new Exception("convert StockShareConfig exception occured", ex);
                }

                cacheManager.Set(cacheKey, config, null, TimeSpan.FromHours(6));

                return config;
            }
            else
            {
                return (StockShareConfigVO)val;
            }
        }

    }


}
