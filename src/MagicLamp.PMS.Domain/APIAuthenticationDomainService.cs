﻿using Abp.Domain.Repositories;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace MagicLamp.PMS.Domain
{
    public class APIAuthenticationDomainService : BaseDomainService
    {
        private HEERPDbContext heerpDbContext = null;

        public APIAuthenticationDomainService()
        {
            heerpDbContext = new HEERPDbContextFactory().CreateDbContext();
        }

        public string GenerateAPIKey()
        {
            var cryptoProvider = new RNGCryptoServiceProvider();

            byte[] secretKeyByteArray = new byte[32];

            cryptoProvider.GetBytes(secretKeyByteArray);
            var APIKey = Convert.ToBase64String(secretKeyByteArray);
            return APIKey;
        }

        public string GetAPIIdAndKeyFromAPIConfig()
        {
            return "";
        }
    }
}
