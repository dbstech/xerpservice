﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Abp.Extensions;
using Abp.Json;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.Infrastructure;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace MagicLamp.PMS.Proxy
{
    /// <summary>
    /// 钉钉消息推送助手
    /// </summary>
    public static class DDNotificationProxy
    {

        /// <summary>
        /// 推送markdown内容格式的提醒
        /// </summary>
        /// <param name="title"></param>
        /// <param name="text"></param>
        /// <param name="hookurl"></param>
        public static void MarkdownNotification(string title, string text, string token = null, string secret = null, List<string> listMobileForAt = null, int maxTextLength = 2000)
        {
            BusinessConfig businessConfig = Ultities.GetConfig<BusinessConfig>();

            if (!businessConfig.PushDDNotification)
            {
                return;
            }

            if (string.IsNullOrEmpty(token))
            {
                token = "8ff65b0fa81c034e263ccb8df38a5a769ca03ee50da6efe4a9e3dfd89ed22284";//erp dev 
            }

            string hookurl = string.Format("/robot/send?access_token={0}", token);

            var msg = new
            {
                msgtype = "markdown",
                markdown = new
                {
                    title = title,
                    text = text.Length > maxTextLength ? $"{text.Substring(0, maxTextLength)}......（省略后续内容最长显示{maxTextLength}个字符）" : text,
                },
                at = new
                {
                    atMobiles = listMobileForAt,
                    isAtAll = false
                }

            };

            RestClient client = new RestClient("https://oapi.dingtalk.com");
            client.Encoding = System.Text.Encoding.UTF8;
            RestRequest request = new RestRequest(hookurl);

            if (!string.IsNullOrEmpty(secret))
            {
                var timestamp = DateTime.Now.ToUTC();
                string stringToSign = $"{timestamp}\n{secret}";
                var b64 = getHmac(stringToSign, secret);
                var b64Str = Convert.ToBase64String(b64);
                var sign = HttpUtility.UrlEncode(b64Str);
                request.AddQueryParameter("timestamp", timestamp.ToString());
                request.AddQueryParameter("sign", sign);
            }

            request.AddJsonBody(msg);
            var response = client.Post(request);
            var content = JObject.Parse(response.Content);

            JToken errmsg;
            content.TryGetValue("errmsg", out errmsg);
            if (!errmsg.ToString().IsNullOrWhiteSpace())
            {
                CLSLogger.Error("DingTalk MarkdownNotification error", content.ToJsonString());
            }
        }

        private static byte[] getHmac(string message, string secret)
        {
            byte[] keyByte = Encoding.UTF8.GetBytes(secret);
            byte[] messageBytes = Encoding.UTF8.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return hashmessage;
            }
        }

        public static Int64 ToUTC(this DateTime time)
        {
            var zts = TimeZoneInfo.Local.BaseUtcOffset;
            var yc = new DateTime(1970, 1, 1).Add(zts);
            return (long)(DateTime.Now - yc).TotalMilliseconds;
        }

    }
}