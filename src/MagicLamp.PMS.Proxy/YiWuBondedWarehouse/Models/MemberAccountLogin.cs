using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace MagicLamp.PMS.YiWuBondedWarehouse
{
    [DataContract]
    public class MemberAccountLogin
    {
        [DataMember(Name = "member_code")]
        public string MemberCode { get; set; }
        [DataMember(Name = "password")]
        public string Password { get; set; }
    }
}