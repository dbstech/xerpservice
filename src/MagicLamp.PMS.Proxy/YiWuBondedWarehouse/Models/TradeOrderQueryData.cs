using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace MagicLamp.PMS.YiWuBondedWarehouse
{
    [DataContract]
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class TradeOrderQueryData
    {
        [DataMember(Name = "order_type")]
        public string orderType { get; set; } = "10";
        [DataMember(Name = "order_state")]
        public string orderState { get; set; }
        [DataMember(Name = "order_code")]
        public string orderCode { get; set; }
        [DataMember(Name = "ref_order_code")]
        public string refOrderCode { get; set; }
        [DataMember(Name = "key")]
        public string key { get; set; }
        [DataMember(Name = "page_index")]
        public string pageIndex { get; set; }
        [DataMember(Name = "page_size")]
        public string pageSize { get; set; }
        [DataMember(Name = "sorting")]
        public string sorting { get; set; }
    }
}