using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace MagicLamp.PMS.YiWuBondedWarehouse
{
    [DataContract]
    public class LoginResponse
    {
        [DataMember(Name = "member_id")]
        public string MemberId{ get; set; }
        [DataMember(Name = "token")]
        public string Token { get; set; }
        [DataMember(Name = "expire")]
        public string Expire { get; set; }
    }
}