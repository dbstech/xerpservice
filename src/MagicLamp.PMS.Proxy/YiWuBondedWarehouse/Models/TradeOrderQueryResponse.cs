namespace MagicLamp.PMS.YiWuBondedWarehouse
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class TradeOrderQueryResponse
    {
        [JsonProperty("ResultCode")]
        public string ResultCode { get; set; }

        [JsonProperty("ResultMsg")]
        public string ResultMsg { get; set; }

        [JsonProperty("ResultData")]
        public ResultData ResultData { get; set; }
    }

    public class ResultData
    {
        [JsonProperty("record_count")]
        public long RecordCount { get; set; }

        [JsonProperty("has_next")]
        public long HasNext { get; set; }

        [JsonProperty("orders")]
        public List<Order> Orders { get; set; }
    }

    public partial class Order
    {
        [JsonProperty("order_id")]
        public long OrderId { get; set; }

        [JsonProperty("order_code")]
        public string OrderCode { get; set; }

        [JsonProperty("ref_order_code")]
        public string RefOrderCode { get; set; }

        [JsonProperty("order_sum")]
        public decimal OrderSum { get; set; }

        [JsonProperty("supplier_id")]
        public long SupplierId { get; set; }

        [JsonProperty("supplier_name")]
        public string SupplierName { get; set; }

        [JsonProperty("province")]
        public string Province { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("district")]
        public string District { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("identity_no")]
        public string IdentityNo { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("pay_method_id")]
        public long PayMethodId { get; set; }

        [JsonProperty("pay_method_name")]
        public string PayMethodName { get; set; }

        [JsonProperty("pay_mode_id")]
        public long PayModeId { get; set; }

        [JsonProperty("pay_mode_name")]
        public string PayModeName { get; set; }

        [JsonProperty("ship_money")]
        public decimal ShipMoney { get; set; }

        [JsonProperty("tax_money")]
        public decimal TaxMoney { get; set; }

        [JsonProperty("packcharge_money")]
        public decimal PackchargeMoney { get; set; }

        [JsonProperty("state_code")]
        public long StateCode { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("state_alias")]
        public string StateAlias { get; set; }

        [JsonProperty("pay_state")]
        public string PayState { get; set; }

        [JsonProperty("ship_state")]
        public string ShipState { get; set; }

        [JsonProperty("ship_name")]
        public string ShipName { get; set; }

        [JsonProperty("ship_no")]
        public string ShipNo { get; set; }

        [JsonProperty("order_source")]
        public string OrderSource { get; set; }

        [JsonProperty("order_time")]
        // China Standard Time
        public DateTime OrderTime { get; set; }

        [JsonProperty("order_pro")]
        public List<OrderPro> OrderPro { get; set; }
    }

    public partial class OrderPro
    {
        [JsonProperty("pro_id")]
        public long ProId { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("image")]
        public Uri Image { get; set; }

        [JsonProperty("unit_id")]
        public long UnitId { get; set; }

        [JsonProperty("tax_rate")]
        public long TaxRate { get; set; }

        [JsonProperty("num")]
        public long Num { get; set; }

        [JsonProperty("exemption_postage")]
        public long ExemptionPostage { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("total")]
        public decimal Total { get; set; }
    }
}
