﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MagicLamp.PMS.YiWuBondedWarehouse
{
    [DataContract]
    public class CreateSupplierOrderResponse
    {
        /// <summary>
        /// 调用结果（调用返回状态码 00 表示成功）
        /// </summary>
        public string ResultCode { get; set; }

        /// <summary>
        /// 调用返回描述
        /// </summary>
        public string ResultMsg { get; set; }

        /// <summary>
        /// 调用返回数据
        /// </summary>
        public ResultList ResultData { get; set; }
    }

    public class ResultList
    {
        public List<OrderResult> resultList { get; set; }
    }

    public class OrderResult
    {
        /// <summary>
        /// 处理结果（100 成功；101 失败）
        /// </summary>
        public bool resultCode { get; set; }

        /// <summary>
        /// 处理消息
        /// </summary>
        public string resultMsg { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string orderCode { get; set; }
    }
}
