﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MagicLamp.PMS.YiWuBondedWarehouse
{
    [AutoMap(typeof(CreateSupplierOrderInput))]
    public class YiWuCreateOrderInput
    {
        /// <summary>
        /// 订单集合
        /// </summary>
        [Required]
        public List<YiWuSupplierOrder> Orders { get; set; }
    }

    public class YiWuSupplierOrder
    {
        /// <summary>
        /// 订单来源，默认0
        /// </summary>
        public string orderSource { get; set; } = "0";

        /// <summary>
        /// 订单编号。必填
        /// </summary>
        public string orderCode { get; set; }

        /// <summary>
        /// 供应商编码。非必填
        /// </summary>
        public string companyCode { get; set; } = "";

        /// <summary>
        /// 收货姓名。必填
        /// </summary>
        public string receiverName { get; set; }

        /// <summary>
        /// 手机号码。必填
        /// </summary>
        public string mobile { get; set; }

        /// <summary>
        /// 省份。必填
        /// </summary>
        public string province { get; set; }

        /// <summary>
        /// 市。必填
        /// </summary>
        public string city { get; set; }

        /// <summary>
        /// 区县。必填
        /// </summary>
        public string district { get; set; }

        /// <summary>
        /// 收货地址。必填
        /// </summary>
        public string receiverAddress { get; set; }

        public string receiverZip { get; set; }

        /// <summary>
        /// 支付公司类型。（见附表）必填
        /// </summary>
        public string payCompanyType { get; set; }

        /// <summary>
        /// 支付单号。必填
        /// </summary>
        public string payNumber { get; set; }

        /// <summary>
        /// 支付时间（例：2015-12-01 15:32:11）。必填
        /// </summary>
        public string payDate { get; set; }

        /// <summary>
        /// 物流公司编码，默认为空
        /// </summary>
        public string expressCode { get; set; } = "";

        /// <summary>
        /// 物流单号，默认为空
        /// </summary>
        public string expressNo { get; set; } = "";

        /// <summary>
        /// 支付金额
        /// </summary>
        public string payment { get; set; }

        /// <summary>
        /// 优惠金额。（没有优惠金额 默认0）必填
        /// </summary>
        public string couponAmount { get; set; }

        /// <summary>
        /// 订单总金额。必填
        /// </summary>
        public string orderTotalAmount { get; set; }

        /// <summary>
        /// 订单货款。必填
        /// </summary>
        public string orderGoodsAmount { get; set; }

        /// <summary>
        /// 订单税款。必填
        /// </summary>
        public string orderTaxAmount { get; set; }

        /// <summary>
        /// 运费。必填
        /// </summary>
        public string feeAmount { get; set; }

        /// <summary>
        /// 订单保费。（没保费时设置为0）必填
        /// </summary>
        public string insureAmount { get; set; }

        /// <summary>
        /// 证件类型代码。（01 身份证（试点期间只能是身份证）02 护照 03其他）必填
        /// </summary>
        public string paperType { get; set; } = "01";

        /// <summary>
        /// 证件姓名。必填
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 证件号码。必填
        /// </summary>
        public string paperNumber { get; set; }

        public string holdCode { get; set; }

        /// <summary>
        /// 默认false
        /// </summary>
        public string recept { get; set; } = "false";

        public string receptUrl { get; set; }

        /// <summary>
        /// 订单时间。（订单首次创建时间）必填
        /// </summary>
        public string orderDate { get; set; }

        public string memberId { get; set; } = "0";

        public string clientId { get; set; } = "198";

        public string clientIdentifier { get; set; } = null;

        public string ordertype { get; set; } = "0";

        /// <summary>
        /// 店铺订单号
        /// </summary>
        public string outOrderCode { get; set; }

        public string commodityCodeType { get; set; } = "1";

        public List<YiWuOrderDetail> OrderDtls { get; set; }
    }

    public class YiWuOrderDetail
    {
        /// <summary>
        /// 商品编码。必填
        /// </summary>
        public string commodityCode { get; set; }

        /// <summary>
        /// 商品名称。必填
        /// </summary>
        public string commodityName { get; set; }

        /// <summary>
        /// 商品数量。必填
        /// </summary>
        public string qty { get; set; }

        /// <summary>
        /// 商品单价。必填
        /// </summary>
        public string price { get; set; }

        /// <summary>
        /// 商品总价。必填
        /// </summary>
        public string amount { get; set; }
    }
}
