using System.Runtime.Serialization;

namespace MagicLamp.PMS.YiDaoBondedWarehouse
{
    [DataContract]
    public class ApiRequest<T>
    {
        [DataMember(Name = "method")]
        public string Method { get; set; }
        [DataMember(Name = "sign_type")]
        public string SignType { get; set; } = "md5";
        [DataMember(Name = "appid")]
        public string AppId { get; set; }
        [DataMember(Name = "version")]
        public string Version { get; set; } = "1.0";
        [DataMember(Name = "sign")]
        public string Sign { get; set; }
        [DataMember(Name = "data")]
        public T Data { get; set; }
    }
}