namespace MagicLamp.PMS.YiWuBondedWarehouse
{
    public class YiWuBondedWarehouseConfig
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string AppKey { get; set; }
        public string MemberCode { get; set; }
        public string Password { get; set; }
        public string Secret { get; set; }
    }
}
