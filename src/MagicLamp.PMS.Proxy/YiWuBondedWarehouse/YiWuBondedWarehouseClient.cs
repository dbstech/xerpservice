using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Extensions;
using Abp.Json;
using Abp.Runtime.Caching;
using AutoMapper;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Common;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace MagicLamp.PMS.YiWuBondedWarehouse
{
    public class YiWuBondedWarehouseClient : ITransientDependency
    {
        readonly IRestClient _restClient;
        private readonly ICacheManager _cacheManager;
        private YiWuBondedWarehouseConfig _config;
        public YiWuBondedWarehouseClient(
            ICacheManager cacheManager,
            YiWuAccountEnum account
        )
        {
            var config = Ultities.GetConfig<List<YiWuBondedWarehouseConfig>>();
            _config = config.FirstOrDefault(x => x.Name == account.ToString());
            _restClient = new RestClient(_config.Url);
            _cacheManager = cacheManager;
        }

        private string Md5(string message)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] utf8Bytes = Encoding.UTF8.GetBytes(message);
            byte[] hashedBytes = md5.ComputeHash(utf8Bytes);
            string hashedString = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            return hashedString;
        }
        private string CreateSign(object data)
        {
            string signString = (data.ToJsonString() + _config.Secret);
            return Md5(signString);
        }
        private async Task<JObject> ExecuteAsync(string code, object data, Method httpMethod = Method.POST, Dictionary<string, string> clogTags = null)
        {
            if (clogTags == null)
            {
                clogTags = new Dictionary<string, string>();
            }

            IRestRequest restRequest = new RestRequest()
            {
                Method = httpMethod
            };

            string sign = CreateSign(data);
            restRequest.AddParameter("appkey", _config.AppKey);
            restRequest.AddParameter("code", code);
            restRequest.AddParameter("data", data.ToJsonString());
            restRequest.AddParameter("sign", sign);

            if (code != "Member.Account.Login")
            {
                var cacheKey = string.Format(CacheKeys.YIWUACCESSTOKEN, _config.Name);
                var cache = _cacheManager.GetCache(cacheKey);
                string accessToken = cache.GetOrDefault<string, string>(cacheKey);

                if (accessToken == null)
                {
                    LoginResponse loginResponse = await Login();
                    accessToken = loginResponse.Token;
                    cache.Set(cacheKey, accessToken, null, TimeSpan.FromDays(14));
                }
                restRequest.AddParameter("access_token", accessToken);
            }

            CLSLogger.Info("义乌request", data.ToJsonString(), clogTags);
            IRestResponse response = await _restClient.ExecuteAsync(restRequest);

            JObject content = new JObject();
            try
            {
                if (response != null)
                {
                    content = JObject.Parse(response.Content);
                }
                else
                {
                    // response.Content IsNullOrWhiteSpace
                    CLSLogger.Info("义乌response.message:", "HttpStatusCode:" + response.StatusCode +
                                   "ResponseStatus: " + response.ResponseStatus + "ErrorMessage: " + response.ErrorMessage +
                                   "ErrorException: " + response.ErrorException + "IsSuccessful: " + response.IsSuccessful +
                                   "restRequest: " + restRequest.ToJsonString(), clogTags);
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Info("义乌response.content", response.Content.ToJsonString(), clogTags);
                throw ex;
            }

            CLSLogger.Info("义乌response", content.ToJsonString(), clogTags);

            if (content["ResultCode"].ToString() != "00")
            {
                throw new Exception(content["ResultMsg"].ToString());
            }

            return content;
        }

        public async Task<LoginResponse> Login()
        {
            JObject content = await ExecuteAsync(
                "Member.Account.Login",
                new MemberAccountLogin
                {
                    MemberCode = _config.MemberCode,
                    Password = _config.Password
                }
            );

            LoginResponse response = content["ResultData"].ToObject<LoginResponse>();
            return response;
        }

        public async Task<ResultList> CreateSupplierOrder(CreateSupplierOrderInput input)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "CreateSupplierOrder",
                ["orderid"] = input.Orders.FirstOrDefault().orderCode
            };

            var orderInput = input.MapTo<YiWuCreateOrderInput>();

            var inputOrder = input.Orders.FirstOrDefault();

            if (!(inputOrder.orderPackNo == null || inputOrder.orderPackNo == string.Empty))
            {
                orderInput.Orders.FirstOrDefault().orderCode = inputOrder.orderPackNo;
            }

            CLSLogger.Info("义乌保税仓下单input", orderInput.ToJsonString(), logTags);

            var response = await ExecuteAsync("Trade.Order.CreateSupplierOrder", orderInput);

            CLSLogger.Info("义乌保税仓下单response", response.ToJsonString(), logTags);

            var result = response["ResultData"].ToObject<ResultList>();

            if (result == null || result.resultList.IsNullOrEmpty())
            {
                throw new Exception($"义乌保税仓下单接口失败，订单号：{orderInput.Orders.FirstOrDefault().orderCode}");
            }

            return result;
        }

        public async Task<TradeOrderQueryResponse> TradeOrderQuery(TradeOrderQueryData data)
        {
          
            JObject result = await ExecuteAsync(
                "Trade.Order.Query",
                data,
                Method.POST,
                new Dictionary<string, string>()
                {
                    ["type"] = "yiWuTradeOrderQuery",
                    ["code"] = data.refOrderCode ?? data.orderCode
                }
            );
            if (result != null)
            {
                return result.ToObject<TradeOrderQueryResponse>();
            }
            else
            {
                return null;
            }
            
        }
    }
}
