﻿using Aliyun.OSS;
using Aliyun.OSS.Common;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MagicLamp.PMS.Proxy
{
    public class AliyunOSSProxy
    {
        BusinessConfig BusinessConfig = null;
        OssClient AliyunOssClient = null;
        OssClient BasicResourceOssClient = null;
        private static AliyunOSSProxy _instance;
        public static AliyunOSSProxy Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new AliyunOSSProxy();
                }
                return _instance;
            }
        }
        private AliyunOSSProxy()
        {
            BusinessConfig = Ultities.GetConfig<BusinessConfig>();
            if (BusinessConfig == null)
            {
                throw new Exception("未找到配置BusinessConfig，请检查配置文件");
            }
            AliyunOssClient = new OssClient(BusinessConfig.IDCardEndPoint, BusinessConfig.IDCardAPPKey, BusinessConfig.IDCardAPPSecret);
            BasicResourceOssClient = new OssClient(BusinessConfig.BasicResourceEndPoint, BusinessConfig.BasicResourceAPPKey, BusinessConfig.BasicResourceAPPSecret);

        }


        public Image DownloadImage(string key)
        {
            return DownloadImage(BusinessConfig.IDCardOSSBuketName, key);
        }

        public System.IO.Stream DownloadImageReturnStream(string key)
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "downloadImageFromOSS";
            try
            {
                var result = AliyunOssClient.GetObject(BusinessConfig.IDCardOSSBuketName, key);

                return result.Content;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("DownloadImage发生异常", ex, tags);
                return null;
            }
        }


        public Image DownloadImage(string bucketName, string key)
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "downloadImageFromOSS";
            try
            {
                var result = AliyunOssClient.GetObject(bucketName, key);

                var image = System.Drawing.Image.FromStream(result.Content);
                return image;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("DownloadImage发生异常", ex, tags);
                return null;
            }

        }


        public bool UploadImage(string filePath, string fileKey)
        {
            try
            {
                var response = BasicResourceOssClient.PutObject(BusinessConfig.BasicResourceBuketName, fileKey, filePath);
                return true;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("上传图片发生异常", ex);
                return false;
            }
        }

        /// <summary>
        /// 生成GET方法能访问的临时URL，默认过期时间1小时
        /// </summary>
        /// <param name="key">oss key</param>
        /// <returns></returns>
        public string GenerateIDCardPresignedUrl(string key, int width, int height, DateTime? expireDateTime = null)
        {
            if (!expireDateTime.HasValue)
            {
                expireDateTime = DateTime.Now.AddHours(1);
            }

            GeneratePresignedUriRequest generatePresignedUriRequest = new GeneratePresignedUriRequest(BusinessConfig.IDCardOSSBuketName, key);

            generatePresignedUriRequest.Method = SignHttpMethod.Get;
            generatePresignedUriRequest.Expiration = expireDateTime.Value;

            if (width > 0 && height > 0)
            {
                generatePresignedUriRequest.AddQueryParam("x-oss-process", string.Format("image/resize,m_fill,h_{0},w_{1}", height, width));
            }

            var uri = AliyunOssClient.GeneratePresignedUri(generatePresignedUriRequest);
            return uri.ToString();
        }



        /// <summary>
        /// 生成GET方法能访问的临时URL，默认过期时间1小时
        /// </summary>
        /// <param name="key">oss key</param>
        /// <returns></returns>
        public string GenerateIDCardPresignedUrl2(string key, DateTime? expireDateTime = null)
        {
            if (!expireDateTime.HasValue)
            {
                expireDateTime = DateTime.Now.AddHours(1);
            }

            GeneratePresignedUriRequest generatePresignedUriRequest = new GeneratePresignedUriRequest(BusinessConfig.IDCardOSSBuketName, key);

            generatePresignedUriRequest.Method = SignHttpMethod.Get;
            generatePresignedUriRequest.Expiration = expireDateTime.Value;

            generatePresignedUriRequest.AddQueryParam("x-oss-process", string.Format("image/quality,q_30"));

            var uri = AliyunOssClient.GeneratePresignedUri(generatePresignedUriRequest);
            return uri.ToString();
        }



        public bool UploadFile(string filePath, string fileKey)
        {
            try
            {
                var response = BasicResourceOssClient.PutObject(BusinessConfig.BasicResourceBuketName, fileKey, filePath);
                return true;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("上传文件发生异常", ex);
                return false;
            }
        }

    }
}