﻿using System;
using System.Collections.Generic;
using System.Text;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.DTOs.Configs;
using RestSharp;
using Newtonsoft.Json.Linq;
using MagicLamp.PMS.Infrastructure.Extensions;
using System.Security.Cryptography;
using System.IO;
using System.Web;
using MagicLamp.PMS.DTOs.ThirdPart;
using Abp.Extensions;

namespace MagicLamp.PMS.Proxy
{
    /// <summary>
    /// 长江大客户API，每次访问api需传输的公共参数有：
    /// apikey，
    /// timestamp（时间戳（即 1970 年至今秒数），时间戳误差不能超过 5 分钟），
    /// signature（验证串，为部分参数按字母顺序排序 md5 值。），
    /// apisecret（密匙）
    /// </summary>
    public class ChangJiangExpressProxy
    {
        //private static ChangJiangConfig config = Ultities.GetConfig<ChangJiangConfig>();
        

        public ChangJiangExpressProxy()
        {

        }

        private static string GetMD5(string input)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();
                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }
                // Return the hexadecimal string.
                return sBuilder.ToString();
            }
        }

        /// <summary>
        /// 下订单接口&订单修改接口（所有未修改和需要修改的值都需要传输）
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static FreightCreateOrderOutputDTO CreateNewOrder(FreightCreateOrderInputDTO input, string apiAction = "neworder")
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "ChangJiangCreateNewOrder";
            tags["orderpackno"] = input.ParcelNo;

            var config = Ultities.GetConfig<ChangJiangConfig>();

            string apikey = config.APIKey;
            string apisecret = config.APISecret;
            string url = config.Url;

            try
            {
                CLSLogger.Info("CreateNewOrder请求报文", input.ToJsonString(), tags);

                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(apiAction);
                int timestamp = (int)(DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds;
                int packCount = 1;
                input.ShippingMethod = "1";

                FixAddressProblems(input);

                string pdata = $"apikey={apikey}&apisecret={apisecret}&count={packCount}&line={input.ShippingMethod}";

                if (!input.FreightTrackNo.IsNullOrWhiteSpace())
                {
                    pdata += $"&numbers={input.FreightTrackNo}";
                    request.AddParameter("numbers", input.FreightTrackNo, ParameterType.GetOrPost);
                }

                string pdata2 = $"&r_address={input.ReceiverAddress}&r_city={input.ReceiverCity}&r_county={input.ReceiverDistrict}&r_mobile={input.ReceiverPhone}&r_name={input.ReceiverName}&r_province={input.ReceiverProvince}&s_mobile={input.SenderPhone}&s_name={input.SenderName}&timestamp={timestamp}";

                string signature = GetMD5(pdata + pdata2);

                request.AddParameter("s_name", input.SenderName, ParameterType.GetOrPost);
                request.AddParameter("s_mobile", input.SenderPhone, ParameterType.GetOrPost);
                request.AddParameter("r_name", input.ReceiverName, ParameterType.GetOrPost);
                request.AddParameter("r_mobile", input.ReceiverPhone, ParameterType.GetOrPost);
                request.AddParameter("r_province", input.ReceiverProvince, ParameterType.GetOrPost);
                request.AddParameter("r_city", input.ReceiverCity, ParameterType.GetOrPost);
                request.AddParameter("r_county", input.ReceiverDistrict, ParameterType.GetOrPost);
                request.AddParameter("r_address", input.ReceiverAddress, ParameterType.GetOrPost);
                request.AddParameter("line", input.ShippingMethod, ParameterType.GetOrPost);
                request.AddParameter("count", packCount, ParameterType.GetOrPost);
                request.AddParameter("memo", input.ShippingLabelRemark, ParameterType.GetOrPost);
                request.AddParameter("sys_memo", input.OrderRemark, ParameterType.GetOrPost);
                request.AddParameter("timestamp", timestamp, ParameterType.GetOrPost);
                request.AddParameter("apikey", apikey, ParameterType.GetOrPost);
                request.AddParameter("signature", signature, ParameterType.GetOrPost);

                if (input.ParcelWeight > 0)
                {
                    request.AddParameter("weights", decimal.ToInt32(input.ParcelWeight * 1000), ParameterType.GetOrPost);
                }

                // items
                // items_{p}, p 为包裹序号，起始号码为 1。items_1 即表示包裹 1 的物件
                string key = $"items_1";
                StringBuilder itemsContent = new StringBuilder();
                for (int i = 0; i < input.GoodItems.Count; i++)
                {
                    itemsContent.Append($"{input.GoodItems[i].Barcode}^{input.GoodItems[i].Quantity}|");
                }

                if (itemsContent.Length > 0)
                {
                    itemsContent.Length--;
                }

                request.AddParameter(key, itemsContent.ToString(), ParameterType.GetOrPost);

                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                var response = client.Post(request);

                JObject responseObject = JObject.Parse(response.Content);

                CLSLogger.Info("长江创建订单响应报文", response.Content, tags);

                var content = response.Content.ConvertFromJsonString<FreightCreateOrderOutputDTO>();

                if (!input.FreightTrackNo.IsNullOrWhiteSpace())
                {
                    content.CourierTrackNo = input.FreightTrackNo;
                }
                else if (content.Success)
                {
                    var courierTrackNo = responseObject["Data"]["WaybillNumbers"][0];
                    content.CourierTrackNo = courierTrackNo.ToString();
                }

                return content;

            }
            catch (Exception ex)
            {
                CLSLogger.Error("长江大客户下订单发生异常", ex, tags);
                return null;
            }
        }

        /// <summary>
        /// 身份证上传接口
        /// （身份证上传，正面反面需分开上传，两次访问接口姓名，手机，身份证号码都需一致传
        /// 输，不能省略，根据 type 区分正反面，当完整上传正面和反面后，系统才会导入系统中。)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static ChangJiangAPIOutputDTO UploadIDCard(CJIDCardInfoDTO input)
        {
            if (input == null || input.name.IsNullOrWhiteSpace() || input.idcard.IsNullOrWhiteSpace() || input.data.IsNullOrWhiteSpace())
            {
                throw new ArgumentException("Invalid dto, missing parameters name/idcard/data");
            }

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "ChangJiangUploadIDCard";
            tags["trackid"] = input.idcard;

            var config = Ultities.GetConfig<ChangJiangConfig>();

            string apikey = config.APIKey;
            string apisecret = config.APISecret;
            string url = config.Url;
            try
            {
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest("uploadidcard");

                int timestamp = (int)(DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds;
                string pdata = $"apikey={apikey}&apisecret={apisecret}&idcard={input.idcard}&mobile={input.mobile}&name={input.name}&timestamp={timestamp}&type={input.type}";
                string signature = GetMD5(pdata);

                request.AddParameter("apikey", apikey, ParameterType.GetOrPost);
                request.AddParameter("idcard", input.idcard, ParameterType.GetOrPost);
                request.AddParameter("mobile", input.mobile.IsNullOrWhiteSpace() ? "" : input.mobile, ParameterType.GetOrPost);
                request.AddParameter("name", input.name, ParameterType.GetOrPost);
                request.AddParameter("timestamp", timestamp, ParameterType.GetOrPost);
                request.AddParameter("type", input.type, ParameterType.GetOrPost);
                request.AddParameter("signature", signature, ParameterType.GetOrPost);
                request.AddParameter("data", input.data, ParameterType.GetOrPost);

                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                var response = client.Post(request);

                if (response == null || response.Content.IsNullOrEmpty())
                {
                    CLSLogger.Error("长江上传身份证api返回为null", string.Empty, tags);
                    return new ChangJiangAPIOutputDTO
                    {
                        Message = "长江上传身份证api返回为null"
                    };
                }
                else
                {
                    CLSLogger.Info("长江上传身份证响应报文", response.Content, tags);
                }

                return response.Content.ConvertFromJsonString<ChangJiangAPIOutputDTO>();
            }
            catch (Exception ex)
            {
                CLSLogger.Error("长江大客户身份证上传发生异常", ex, tags);
                return new ChangJiangAPIOutputDTO
                {
                    Message = "长江大客户身份证上传发生异常：" + ex.Message
                };
            }
        }


        private static void FixAddressProblems(FreightCreateOrderInputDTO input)
        {
            if (input == null)
            {
                return;
            }

            input.ReceiverProvince = input.ReceiverProvince.TrimAndCheckNull();
            input.ReceiverName = input.ReceiverName.TrimAndCheckNull();
            input.ReceiverDistrict = input.ReceiverDistrict.TrimAndCheckNull();
            input.ReceiverAddress = input.ReceiverAddress.TrimAndCheckNull();

            //直辖市特殊处理
            if (
                    input.ReceiverCity.Contains("直辖")
                    || input.ReceiverCity.Contains("市辖")
                    || input.ReceiverCity == "县"
                )
            {
                input.ReceiverCity = input.ReceiverProvince;
            }
        }

    }
}
