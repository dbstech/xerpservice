﻿using System;
using System.Collections.Generic;
using System.Text;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.DTOs.Configs;
using System.Net;
using System.IO;

namespace MagicLamp.PMS.Proxy
{
    public class GFCProxy
    {
        //private static GFCConfig config = Ultities.GetConfig<GFCConfig>();
        

        private const string ClogTagType = "GFCProxy";
        public static string Post(string content)
        {
            string result = string.Empty;
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = ClogTagType;
            tags["trackid"] = "GFCProxyPost";

            var config = Ultities.GetConfig<GFCConfig>();

            string method = "api/out/upload";
            string url = config.gfc_erp + method;
            string contentType = "application/json;charset=UTF-8";
            CLSLogger.Info("GFCProxy--Post Request", url, tags);

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.ServicePoint.ConnectionLimit = int.MaxValue;
                request.Method = "POST";
                request.ContentType = contentType;
                request.KeepAlive = false;  //设置不建立持久性连接
                byte[] data = Encoding.UTF8.GetBytes(content);
                request.ContentLength = data.Length;

                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(data, 0, data.Length);
                    reqStream.Close();
                }

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream stream = response.GetResponseStream();
                //获取响应内容  
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    result = reader.ReadToEnd();
                    response.Close();
                    stream.Close();
                }

            }
            catch (WebException ex)
            {
                HttpWebResponse response = (HttpWebResponse)ex.Response;
                if (response != null)
                {
                    Stream stream = response.GetResponseStream();
                    //获取响应内容  
                    using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        result = reader.ReadToEnd();
                        response.Close();
                        stream.Close();
                    }
                    string log = $"状态码：{(int)response.StatusCode}，状态解义：{response.StatusDescription}，异常内容：{ex.ToString()}，相应内容：{System.Web.HttpUtility.UrlDecode(result)}；";
                    CLSLogger.Error("GFCProxy--Post Request发生异常", log, tags);
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("GFCProxy--Post Request发生异常", ex.ToString(), tags);
            }
            finally
            {

            }

            return result;
        }
    }
}
