﻿using System;
using System.Collections.Generic;
using System.Text;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Infrastructure.Extensions;
using System.Linq;
using System.Security.Cryptography;
using MagicLamp.PMS.Infrastructure;
using Newtonsoft.Json.Linq;
using RestSharp;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.DTOs.BondedWarehouse;

namespace MagicLamp.PMS.Proxy
{
    public class GuanMaoProxy
    {
        //老关贸
        //const string APIDomain = "http://221.224.22.200:8888";
        //const string APIPath = "/HttpReceive3/Receive.aspx";

        //金二关贸
        const string APIDomain = "http://221.224.22.200:8888";
        const string APIPath = "/DataPostHander_JG/Receive.aspx";
        //BusinessConfig freightConfig = null;

        public GuanMaoProxy()
        {
            //freightConfig = Ultities.GetConfig<BusinessConfig>();
        }


        /// <summary>
        /// 推送订单到关贸接口
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public bool CreateOrder(DTC_Message inputDto)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>();
            clogTags["type"] = "gmcreateorder";
            clogTags["orderid"] = inputDto.MessageBody.DTCFlow.ORDER_HEAD.ORIGINAL_ORDER_NO;


            //call the create order api
            RestClient restClient = new RestClient(APIDomain);
            RestRequest request = new RestRequest(APIPath);

            CLSLogger.Info("关贸下单请求报文_OriginParameters参数", inputDto.ToXMLString(), clogTags);


            string payload = inputDto.ToXMLString().ToBase64String();
            CLSLogger.Info("关贸下单请求报文", payload, clogTags);

            request.AddParameter("data", payload);

            var response = restClient.Post(request);

            if (response == null || response.ErrorException != null)
            {
                return false;
            }

            CLSLogger.Info("关贸下单返回报文", response.Content, clogTags);


            return true;

        }


    }


}
