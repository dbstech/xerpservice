﻿using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.DTOs.ThirdPart;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy
{
    public class HHProxy
    {
        private const string ClogTagType = "HHProxy";

        /// <summary>
        /// get access token
        /// </summary>
        /// <returns></returns>
        private static HHTokenInfoDTO GetAccessToken()
        {
            HHTokenInfoDTO result = new HHTokenInfoDTO();

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = ClogTagType;
            tags["method"] = "GetAccessToken";

            var config = Ultities.GetConfig<HHConfig>();

            try
            {
                RestClient client = new RestClient(config.AccessTokenUrl);
                RestRequest request = new RestRequest("restapi/1.0/common/oauth2/access_token");
                request.AddParameter("client_id", config.APIKey);
                request.AddParameter("client_secret", config.APISecret);
                request.AddParameter("grant_type", config.RefreshToken);
                request.AddParameter("refresh_token", config.RefreshToken);

                var response = client.Get(request);
                string content = response.Content;

                //CLSLogger.Info("获取HH_access_token_URL:", config.AccessTokenUrl, tags);
                //CLSLogger.Info("获取HH_access_token_返回值:", content, tags);

                result = content.ConvertFromJsonString<HHTokenInfoDTO>();

                if (result.access_token.IsNullOrEmpty())
                {
                    CLSLogger.Info("获取HH access token失败", content.ToJsonString(), tags);
                    throw new Exception("获取HH access token失败");
                }
            }
            catch (Exception ex)
            {
                string content = "错误信息: " + ex.ToString();
                CLSLogger.Error("获取HH access token出错", $"{content}", tags);
                throw ex;
            }
            return result;
        }

        private static void AddTokenToHeader(RestRequest request)
        {
            HHTokenInfoDTO tokenFromHH = GetAccessToken();
            request.AddHeader("access_token", tokenFromHH.access_token);
        }

        /// <summary>
        /// 推送订单给hh
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static HHCreateOrderResultDTO PushOrderToHH(HHPushOrderInputDTO dto)
        {
            HHCreateOrderResultDTO result = new HHCreateOrderResultDTO();

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = ClogTagType;
            tags["orderpackno"] = dto.order_no;
            tags["method"] = "PushOrderToHH";

            var config = Ultities.GetConfig<HHConfig>();

            try
            {
                CLSLogger.Info("推送订单给HH请求报文", dto.ToJsonString(), tags);

                RestClient client = new RestClient(config.Url);
                RestRequest request = new RestRequest("create");

                AddTokenToHeader(request);

                request.AddJsonBody(dto);

                //HH create order often timeout, set the timeout for the request to be 10 second for fast processing order
                client.Timeout = 10000;

                var response = client.Post(request);

                string content = response.Content;

                CLSLogger.Info("推送订单给HH响应报文", $"response.IsSuccessful: {response.IsSuccessful.ToString()}, content: {content}", tags);

                if (response.IsSuccessful)
                {
                    result = content.ConvertFromJsonString<HHCreateOrderResultDTO>();
                }
                else
                {
                    result.result = -1;
                    var responseObject = content.ConvertFromJsonString<HHCreateOrderResultDTO>();
                    CLSLogger.Error("推送订单给HH失败", $"包裹号: {dto.order_no}, HTTP status code: {response.StatusCode}, " +
                        $"description: {response.StatusDescription}, 错误信息: {responseObject.ToJsonString()}", tags);
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.result = -1;
                string content = "错误信息: " + ex.ToString();
                CLSLogger.Error("推送订单给HH失败", $"包裹号: {dto.order_no}, {content}", tags);
            }
            finally
            {
                CLSLogger.Info("推送订单给HH完成", $"结果: {result.message}, 包裹号: {dto.order_no}, 详情: {dto.order_lines.ToJsonString()}", tags);
            }
            return result;
        }

        /// <summary>
        /// 发货前取消订单
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static HHCreateOrderResultDTO CancelOrderInHH(HHPushOrderInputDTO dto)
        {
            HHCreateOrderResultDTO result = new HHCreateOrderResultDTO();

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = ClogTagType;
            tags["orderpackno"] = dto.order_no;
            tags["method"] = "CancelOrderInHH";

            var config = Ultities.GetConfig<HHConfig>();

            try
            {
                CLSLogger.Info("HH发货前取消订单请求报文", dto.ToJsonString(), tags);

                RestClient client = new RestClient(config.Url);
                RestRequest request = new RestRequest("cancel");

                AddTokenToHeader(request);

                request.AddJsonBody(dto);

                var response = client.Post(request);

                string content = response.Content;

                CLSLogger.Info("HH发货前取消订单响应报文", content, tags);

                if (response.IsSuccessful)
                {
                    result = content.ConvertFromJsonString<HHCreateOrderResultDTO>();
                }
                else
                {
                    result.result = -1;
                    var responseObject = content.ConvertFromJsonString<HHCreateOrderResultDTO>();
                    CLSLogger.Error("HH发货前取消订单失败", $"包裹号: {dto.order_no}, HTTP status code: {response.StatusCode}, " +
                        $"description: {response.StatusDescription}, 错误信息: {responseObject.ToJsonString()}", tags);
                    return result;
                }


            }
            catch (Exception ex)
            {
                result.result = -1;
                string content = "错误信息: " + ex.ToString();
                CLSLogger.Error("HH发货前取消订单失败", $"包裹号: {dto.order_no}, {content}", tags);
            }
            finally
            {
                CLSLogger.Info("HH发货前取消订单完成", $"结果: {result.message}, 包裹号: {dto.order_no}", tags);
            }
            return result;
        }

        /// <summary>
        /// 绑定波次和订单
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static HHCreateOrderResultDTO BindWaveAndOrder(HHBindWaveAndOrderInputDTO dto)
        {
            HHCreateOrderResultDTO result = new HHCreateOrderResultDTO();

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = ClogTagType;
            tags["trackid"] = dto.wave_picking_no;
            tags["method"] = "BindWaveAndOrder";

            var config = Ultities.GetConfig<HHConfig>();
            string log = string.Empty;

            try
            {
                CLSLogger.Info("HH波次绑定请求报文", dto.ToJsonString(), tags);

                RestClient client = new RestClient(config.Url);
                RestRequest request = new RestRequest("bind_wave_pickings");

                AddTokenToHeader(request);

                request.AddJsonBody(dto);

                var response = client.Post(request);

                string content = response.Content;

                CLSLogger.Info("HH波次绑定响应报文", content, tags);

                if (response.IsSuccessful)
                {
                    result = content.ConvertFromJsonString<HHCreateOrderResultDTO>();
                }
                else
                {
                    result.result = -1;
                    var responseObject = content.ConvertFromJsonString<HHCreateOrderResultDTO>();
                    log = $"HH波次绑定失败： HTTP status code: {response.StatusCode}, description: {response.StatusDescription}, 错误信息: {responseObject.ToJsonString()}";
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.result = -1;
                log = $"HH波次绑定发生异常：{ex.ToString()}";
            }
            finally
            {
                CLSLogger.Info("HH绑定波次和订单", $"{log}, 结果: {result.message}, 波次: {dto.wave_picking_no}, 包裹号: {dto.orders.ToJsonString()}", tags);
            }
            return result;
        }

        /// <summary>
        /// 绑定出库时间
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static HHCreateOrderResultDTO SyncOutboundDateToHH(HHSyncOutboundDateInputDTO dto)
        {
            HHCreateOrderResultDTO result = new HHCreateOrderResultDTO();

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "SyncOutboundDateToHH";
            tags["orderpackno"] = dto.order_no;

            var config = Ultities.GetConfig<HHConfig>();

            try
            {
                CLSLogger.Info("HH绑定阿拉丁出库时间请求报文", dto.ToJsonString(), tags);
              


                RestClient client = new RestClient(config.Url);
                RestRequest request = new RestRequest("bind_delivery_date");

                AddTokenToHeader(request);
                request.AddJsonBody(dto);
                var response = client.Post(request);

                //CLSLogger.Info("HH绑定阿拉丁出库时间请求报文URL", request.ToJsonString(), tags);

                string content = response.Content;

                CLSLogger.Info("HH绑定阿拉丁出库时间响应报文", content, tags);

                if (response.IsSuccessful)
                {
                    result = content.ConvertFromJsonString<HHCreateOrderResultDTO>();
                }
                else
                {
                    result.result = -1;
                    var responseObject = content.ConvertFromJsonString<HHCreateOrderResultDTO>();
                    CLSLogger.Error("HH绑定阿拉丁出库时间失败", $"包裹号: {dto.order_no}, HTTP status code: {response.StatusCode}, " +
                        $"description: {response.StatusDescription}, 错误信息: {responseObject.ToJsonString()}", tags);
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.result = -1;
                string content = "错误信息: " + ex.ToString();
                CLSLogger.Error("HH绑定阿拉丁出库时间失败", $"包裹号: {dto.order_no}, {content}", tags);
            }
            finally
            {
                CLSLogger.Info("HH绑定阿拉丁出库时间完成", $"包裹号: {dto.order_no}", tags);
            }
            return result;
        }

        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="dto"></param>
        public static HHGetOrderOutputDTO GetOrder(HHPushOrderInputDTO dto)
        {
            HHGetOrderOutputDTO result = new HHGetOrderOutputDTO();

            Dictionary<string, string> tags = new Dictionary<string, string>
            {
                ["type"] = ClogTagType,
                ["orderpackno"] = dto.order_no,
                ["method"] = "GetOrder"
            };

            var config = Ultities.GetConfig<HHConfig>();

            try
            {
                CLSLogger.Info("HH查询订单请求报文", dto.ToJsonString(), tags);
                RestClient client = new RestClient(config.Url);
                RestRequest request = new RestRequest("get_order");

                AddTokenToHeader(request);

                request.AddJsonBody(dto);

                var response = client.Post(request);

                string content = response.Content;

                CLSLogger.Info("HH查询订单响应报文", "parameters: "+ dto.ToJsonString() + " result: " +content, tags);

                if (response.IsSuccessful)
                {
                    result = content.ConvertFromJsonString<HHGetOrderOutputDTO>();
                }
                else
                {
                    result.result = -1;
                    var responseObject = content.ConvertFromJsonString<HHGetOrderOutputDTO>();

                    CLSLogger.Error("查询HH订单失败", $"包裹号: {dto.order_no}, HTTP status code: {response.StatusCode}, " +
                        $"description: {response.StatusDescription}, 错误信息: {responseObject.ToJsonString()}", tags);
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.result = -1;
                string content = "错误信息: " + ex.ToString();
                CLSLogger.Error("查询HH订单出错", $"包裹号: {dto.order_no}, {content}", tags);
            }

            return result;
        }
    }
}
