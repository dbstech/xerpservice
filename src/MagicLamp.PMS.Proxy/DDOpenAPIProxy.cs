﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Abp.Dependency;
using Abp.Runtime.Caching;
using Abp.UI;
using MagicLamp.PMS.DTO;
using MagicLamp.PMS.Infrastructure.Extensions;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace MagicLamp.PMS.Proxy
{
    /// <summary>
    /// 钉钉open api 代理
    /// </summary>
    public class DDOpenAPIProxy
    {

        public const string OpenAPIDomain = "https://oapi.dingtalk.com";


        private static void CheckError(IRestResponse<JObject> response)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>();
            logTags["type"] = "ddopenapi";

            if (response == null || !response.IsSuccessful || string.IsNullOrEmpty(response.Content))
            {
                CLSLogger.Error("钉钉openapi调用发生异常", response.ErrorException, logTags);
                throw new UserFriendlyException("Restclient request occurs exception");
            }

            var jobject = response.GetJObject();

            int errcode = jobject["errcode"].Value<int>();
            string errmsg = jobject["errmsg"].Value<string>();

            if (errcode != 0)
            {
                CLSLogger.Error("钉钉openapi调用返回错误", response.Content, logTags);
                throw new UserFriendlyException($"DD OPEN API 返回错误：{errmsg}");
            }

        }

        public static string GetAccessToken(string appID, string appSecret)
        {
            string resource = $"/gettoken?appkey={appID}&appsecret={appSecret}";

            RestClient client = new RestClient(OpenAPIDomain);
            RestRequest request = new RestRequest(resource);
            var response = client.Get<JObject>(request);

            CheckError(response);

            return response.GetJObject()["access_token"].Value<string>();
        }


        public static string GetUserID(string accessCode, string tempAuthCode)
        {
            string resource = $"/user/getuserinfo?access_token={accessCode}&code={tempAuthCode}";

            RestClient client = new RestClient(OpenAPIDomain);
            RestRequest request = new RestRequest(resource);
            var response = client.Get<JObject>(request);

            CheckError(response);
            return response.GetJObject()["userid"].Value<string>();
        }

        public static JObject GetDingTalkUserInfoByUserID(string accessToken, string userid)
        {
            string resource = $"/user/get?access_token={accessToken}&userid={userid}";

            RestClient client = new RestClient(OpenAPIDomain);
            RestRequest request = new RestRequest(resource);
            var response = client.Get<JObject>(request);

            CheckError(response);
            return response.GetJObject();
        }


        public static JObject GetDingTalkUserInfoByCode(string appID, string appSecret, string tempAuthCode)
        {
            string accessToken = GetAccessToken(appID, appSecret);
            if (string.IsNullOrEmpty(accessToken))
            {
                throw new UserFriendlyException("get accesstoken failed");
            }

            string userid = GetUserID(accessToken, tempAuthCode);
            if (string.IsNullOrEmpty(accessToken))
            {
                throw new UserFriendlyException("get userid failed");
            }

            return DDOpenAPIProxy.GetDingTalkUserInfoByUserID(accessToken, userid);

        }


    }
}