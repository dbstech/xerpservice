﻿using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using MagicLamp.PMS.Infrastructure.Extensions;
using Abp.Runtime.Caching;
using Abp.Dependency;
using System.Net;
using System.IO;
using RestSharp;

namespace MagicLamp.PMS.Proxy
{
    public class NSFProxy
    {
        private static string tokenAuthorization = "Bearer ";
        private const string ClogTagType = "NSFProxy";
        private static List<string> listNZ = new List<string> { "sfnsf", "nsf", "nsfyd" };
        private static List<string> listAU = new List<string> { "nsfau", "nsfaunoqrcode" };
        private static List<int> listExportFlag = new List<int> { 16, 25 };

        //private static NSFConfig config = Ultities.GetConfig<NSFConfig>();

        private const string NSFCACHE = "NSFToken";
        private const string NZNSFKEY = "NZNSFToken";
        private const string AUNSFKEY = "AUNSFToken";
        private static ICacheManager Cache { get; set; }

        public NSFProxy()
        {

        }

        /// <summary>
        /// Access user token
        /// </summary>
        /// <param name="accountInfo">阿拉丁账户信息</param>
        /// <returns></returns>
        public static string Login(NSFAccountInfoDTO accountInfo)
        {
            var config = Ultities.GetConfig<NSFConfig>();

            try
            {
                NSFPostDataDTO postData = new NSFPostDataDTO
                {
                    url = config.Token_url,
                    parameters = $"username={accountInfo.UserName}&password={accountInfo.Password}&grant_type={accountInfo.Grant_type}",
                    dataEncode = Encoding.UTF8,
                    authorization = "Basic bnNmLmV4cHJlc3M6bnNmI2V4cHJlc3MjMjAxNw==",
                    contentType = "application/x-www-form-urlencoded",
                };

                string response = string.Empty;
                try
                {
                    //TODO: fix http post to NSF
                    byte[] byteArray = postData.dataEncode.GetBytes(postData.parameters);

                    //var client = new RestClient(postData.url);
                    //client.Encoding = postData.dataEncode;
                    //RestRequest request = new RestRequest(Method.POST);
                    //request.AddHeader("content-type", postData.contentType);
                    //request.AddHeader("Authorization", postData.authorization);
                    //request.(postData.parameters,);
                    //response = client.Execute(request).Content;

                    //Naky old http method
                    HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(new Uri(postData.url));
                    webReq.KeepAlive = false; // 
                    webReq.Method = "POST";
                    webReq.Timeout = 1000 * 200;
                    webReq.ContentType = postData.contentType;
                    webReq.Headers.Add("Authorization", postData.authorization);
                    webReq.ContentLength = byteArray.Length;
                    Stream newStream = webReq.GetRequestStream();
                    newStream.Write(byteArray, 0, byteArray.Length);//写入参数
                    newStream.Close();
                    HttpWebResponse responseH = (HttpWebResponse)webReq.GetResponse();
                    StreamReader sr = new StreamReader(responseH.GetResponseStream(), Encoding.UTF8);
                    response = sr.ReadToEnd();
                    sr.Close();
                    responseH.Close();
                    newStream.Close();
                }
                catch (Exception ex)
                {
                    Dictionary<string, string> tags = new Dictionary<string, string>();
                    tags["type"] = ClogTagType;
                    tags["method"] = "Login";
                    tags["parameters"] = postData.parameters;
                    //1成功；0失败
                    tags["status"] = "0";
                    //string content = "错误信息: " + ex.InnerException == null ? ex.Message : ex.InnerException.ToString();
                    CLSLogger.Info("NSFProxy错误信息", ex.Message, tags);
                }

                var token = response.ConvertFromJsonString<NSFTokenInfoDTO>();
                return token.access_token;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// 根据包裹下单(原APIApplicationByPack)
        /// </summary>
        /// <param name="listOrderInfo"></param>
        /// <param name="listOrderDetail"></param>
        /// <returns></returns>
        public static NSFCarrierResultDTO CreateOrder(NSFOrderDTO op)
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = ClogTagType;
            tags["method"] = "CreateOrder";

            var config = Ultities.GetConfig<NSFConfig>();

            NSFCarrierResultDTO returnResult = new NSFCarrierResultDTO();
            try
            {
                NSFCarrierDTO carrierInfo = new NSFCarrierDTO
                {
                    orderid = op.carrierid //接口
                };

                if (op.NSFOrderInfo == null)
                {
                    tags["orderid"] = op.orderid.ToString();
                    CLSLogger.Error("根据包裹下单失败", $"orderpackno={op.orderpackno}找不到订单！freightid={op.freightid} carrierid={op.carrierid}", tags);
                    returnResult.IsSuccessful = false;
                    return returnResult;
                }

                //sender
                NSFContactInfoDTO contactInfo = new NSFContactInfoDTO
                {
                    Mobile = "0"
                };
                NSFSenderDTO senderInfo = new NSFSenderDTO
                {
                    Name = "匿名用户",
                    Address = "匿名地址",
                    ContactInfo = contactInfo,
                };
                carrierInfo.Sender = senderInfo;

                //receiver
                NSFContactInfoDTO contactInfo2 = new NSFContactInfoDTO
                {
                    Mobile = op.NSFOrderInfo.rec_phone
                };
                NSFReceiverDTO receiverInfo = new NSFReceiverDTO
                {
                    Name = op.NSFOrderInfo.rec_name.Length > 20 ? op.NSFOrderInfo.rec_name.Substring(0, 20) : op.NSFOrderInfo.rec_name,
                    Street = op.NSFOrderInfo.rec_addr,
                    IdentityNumber = op.NSFOrderInfo.idno,
                    ContactInfo = contactInfo2,
                };
                carrierInfo.Receiver = receiverInfo;
                //寄件方式：1网点寄件 2上门取件
                carrierInfo.SendStyle = 1;
                op.freightid = op.freightid.IsNullOrEmpty() ? "" : op.freightid;

                //此处分澳洲和新西兰
                //快递类型 1特快线
                if (listExportFlag.Contains(op.exportflag) || op.freightid.ToLower() == "sfnsf")
                {
                    carrierInfo.ExpressType = 1;
                }
                //0普快线
                else
                {
                    carrierInfo.ExpressType = 0;
                }

                List<NSFOrderItemListDTO> orderList = new List<NSFOrderItemListDTO>();
                foreach (var oDetail in op.listOrderDetail)
                {
                    if (oDetail.flag == 1)
                    {
                        NSFOrderItemListDTO order = new NSFOrderItemListDTO
                        {
                            GoodsName = oDetail.product_name,
                            Number = oDetail.Number,
                            Weight = oDetail.Weight
                        };
                        orderList.Add(order);
                    }
                }
                carrierInfo.OrderItemList = orderList;
                carrierInfo.TotalWeight = op.packweight.HasValue ? op.packweight.Value : 0;
                carrierInfo.NotUseIdentityProtocol = 1;
                carrierInfo.CCICStatus = 0; //推送ccic, 1推送, 0不推送 目前提供推送
                carrierInfo.PickupDirectly = true; //是否直接揽件

                //NZ
                if (listNZ.Contains(op.freightid.ToLower()))
                {
                    carrierInfo.NetPointId = config.NZ_account_net_point_id;

                    returnResult = CreateCarrierModel(carrierInfo, "nsf");

                    //成功
                    if (returnResult.Code == 200)
                    {
                        returnResult.IsSuccessful = true;
                    }
                    //订单Id已存在
                    else if (returnResult.Code == -13)
                    {
                        returnResult.IsSuccessful = false;
                    }
                    else
                    {
                        CLSLogger.Error("创建物流单号失败", $"Carrier Information: {carrierInfo.ToJsonString()}, Return Value: {returnResult.ToJsonString()}", tags);
                        returnResult.IsSuccessful = false;
                        return returnResult;
                    }
                }
                //AU
                if (listAU.Contains(op.freightid.ToLower()))
                {
                    carrierInfo.NetPointId = config.AU_account_net_point_id;
                    returnResult = CreateCarrierModel(carrierInfo, "nsfau");

                    //成功
                    if (returnResult.Code == 200)
                    {
                        returnResult.IsSuccessful = true;
                    }
                    //订单Id已存在
                    else if (returnResult.Code == -13)
                    {
                        returnResult.IsSuccessful = false;
                    }
                    else
                    {

                        CLSLogger.Error("创建物流单号失败", $"Carrier Information: {carrierInfo.ToJsonString()}, Return Value: {returnResult.ToJsonString()}", tags);
                        returnResult.IsSuccessful = false;
                        return returnResult;
                    }
                }

            }
            catch (Exception ex)
            {
                string content = "错误信息: " + ex.InnerException == null ? ex.Message : ex.InnerException.ToString();
                CLSLogger.Error("创建物流单号失败", $"Carrier ID: {op.carrierid}, {content}", tags);
                returnResult.IsSuccessful = false;
                return returnResult;
            }
            return returnResult;
        }


        /// <summary>
        /// 业务出库，创建物流单号
        /// </summary>
        /// <param name="carrierInfo">参数信息</param>
        /// <param name="account">账户类型</param>
        /// <returns></returns>
        private static NSFCarrierResultDTO CreateCarrierModel(NSFCarrierDTO carrierInfo, string account)
        {
            Dictionary<string, string> tags = new Dictionary<string, string>
            {
                ["type"] = ClogTagType,
                ["method"] = "CreateCarrierModel",
                ["trackid"] = carrierInfo.orderid
            };

            var config = Ultities.GetConfig<NSFConfig>();

            string token = string.Empty;
            token = GetNSFToken(account);

            var json = carrierInfo.ToJsonString();
            NSFPostDataDTO parametersObjects = new NSFPostDataDTO
            {
                url = config.Carrier_id_url_submit,
                parameters = json,
                dataEncode = Encoding.UTF8
            };

            parametersObjects.authorization = tokenAuthorization + token;
            parametersObjects.contentType = "application/json";

            string response = string.Empty;
            try
            {
                byte[] byteArray = parametersObjects.dataEncode.GetBytes(parametersObjects.parameters);


                /*var client = new RestClient(parametersObjects.url);
                client.Encoding = parametersObjects.dataEncode;
                RestRequest request = new RestRequest(Method.POST);
                request.AddHeader("content-type", parametersObjects.contentType);
                request.AddHeader("Authorization", parametersObjects.authorization);
                request.AddParameter(parametersObjects.contentType, byteArray, ParameterType.RequestBody);
                response = client.Execute(request).Content;*/

                CLSLogger.Info("新顺丰创建物流单号请求报文", parametersObjects.ToJsonString(), tags);


                HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(new Uri(parametersObjects.url));
                webReq.KeepAlive = false; // 
                webReq.Method = "POST";
                webReq.Timeout = 1000 * 200;
                webReq.ContentType = parametersObjects.contentType;
                webReq.Headers.Add("Authorization", parametersObjects.authorization);
                webReq.ContentLength = byteArray.Length;
                Stream newStream = webReq.GetRequestStream();
                newStream.Write(byteArray, 0, byteArray.Length);//写入参数
                newStream.Close();
                HttpWebResponse responseH = (HttpWebResponse)webReq.GetResponse();
                StreamReader sr = new StreamReader(responseH.GetResponseStream(), Encoding.UTF8);
                response = sr.ReadToEnd();
                sr.Close();
                responseH.Close();
                newStream.Close();

                CLSLogger.Info("新顺丰创建物流单号响应报文", response, tags);
            }
            catch (Exception ex)
            {
                //1成功；0失败
                tags["status"] = "0";
                string content = "错误信息: " + ex.InnerException == null ? ex.Message : ex.InnerException.ToString();
                CLSLogger.Info("NSFProxy错误信息", content, tags);
            }

            return response.ConvertFromJsonString<NSFCarrierResultDTO>();
        }

        /// <summary>
        /// Get token from cache manager
        /// </summary>
        /// <param name="cache">Cache Manager</param>
        /// <param name="account">account type</param>
        /// <returns></returns>
        private static string GetNSFToken(string account)
        {
            var config = Ultities.GetConfig<NSFConfig>();

            string returnToken = string.Empty;

            Cache = IocManager.Instance.Resolve<ICacheManager>();
            var cacheManager = Cache.GetCache(NSFCACHE);

            //differentiate account (nsf, nsfau)
            switch (account)
            {
                case "nsf":
                    var NZNSFValue = cacheManager.GetOrDefault<string, string>(config.NZ_token_key);
                    returnToken = NZNSFValue;

                    if (NZNSFValue == null)
                    {
                        //账户信息
                        NSFAccountInfoDTO nzAccount = new NSFAccountInfoDTO
                        {
                            UserName = config.NZ_account_user,
                            Password = config.NZ_account_password,
                            Grant_type = "password"
                        };
                        string tokenValue = Login(nzAccount);
                        returnToken = tokenValue;
                        cacheManager.Set(NZNSFKEY, tokenValue, TimeSpan.Zero, DateTime.UtcNow.TimeOfDay.Add(TimeSpan.FromDays(7)));
                    }
                    break;
                case "nsfau":
                    var AUNSFValue = cacheManager.GetOrDefault<string, string>(config.AU_token_key);
                    returnToken = AUNSFValue;

                    if (AUNSFValue == null)
                    {
                        //账户信息
                        NSFAccountInfoDTO auAccount = new NSFAccountInfoDTO
                        {
                            UserName = config.NZ_account_user,
                            Password = config.NZ_account_password,
                            Grant_type = "password"
                        };
                        string tokenValue = Login(auAccount);
                        returnToken = tokenValue;
                        cacheManager.Set(AUNSFKEY, tokenValue, TimeSpan.Zero, DateTime.UtcNow.TimeOfDay.Add(TimeSpan.FromDays(7)));
                    }
                    break;
            }
            return returnToken;
        }
    }
}
