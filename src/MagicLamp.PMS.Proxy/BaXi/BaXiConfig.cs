namespace MagicLamp.PMS.DTOs.Configs
{
    public class BaXiConfig
    {
        public string Url { get; set; }
        public string CustomerCode { get; set; }
        public string SecretKey { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
