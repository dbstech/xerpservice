using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.BaXi
{
    public class BaXiException
     : Exception
    {
        public BaXiException ()
        {

        }
        public BaXiException (string message) : base(message)
        {

        }
    }
}
