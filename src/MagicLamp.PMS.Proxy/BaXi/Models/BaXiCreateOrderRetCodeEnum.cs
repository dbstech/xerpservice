﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy.BaXi.Models
{
    public enum BaXiCreateOrderRetCodeEnum
    {
        成功 = 0,
        参数验证失败 = 1,
        商品库存不足或超过最大购买量 = 2,
        分包失败 = 3,
        执行失败 = 4
    }
}
