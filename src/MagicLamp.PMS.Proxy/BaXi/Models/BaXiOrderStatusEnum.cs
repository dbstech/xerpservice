﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy.BaXi.Models
{
    public enum BaXiOrderStatusEnum
    {
        cancelled = -1,
        done = 0,
        pending = 1,
        ordered = 2
    }
}
