﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy.BaXi.Models
{
   public class BaxiIDCardResponse
    {
        /// <summary>
        /// 状态
        /// 0表示操作失败，1表示操作成功
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// 返回信息
        /// 失败时告知原因
        /// </summary>
        public string msg { get; set; }

        /// <summary>
        /// 订单信息
        /// </summary>
        public List<Data> data { get; set; }


        public class Data
        {
            /// <summary>
            /// 身份证图片1
            /// Base64 编码的身份证图片，人像面(Photo side)
            /// </summary>
            public string Image1 { get; set; }

            /// <summary>
            /// 身份证图片2
            /// Base64 编码的身份证图片，国徽面(Emblem Side)
            /// </summary>
            public string Image2 { get; set; }

            /// <summary>
            /// 收件人中文姓名
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 手机号
            /// 11 位中国手机号，以数字 1 开头
            /// </summary>
            public string mobile { get; set; }

            /// <summary>
            /// 中国身份证号
            /// 18 位数字。有效中国身份证号
            /// </summary>
            public string idCardNo { get; set; }

            /// <summary>
            /// 身份证有效期
            /// </summary>
            public string validityPeriod { get; set; }

            /// <summary>
            /// 运单号
            /// </summary>
            public string wayBillCode { get; set; }
        }

    }
}
