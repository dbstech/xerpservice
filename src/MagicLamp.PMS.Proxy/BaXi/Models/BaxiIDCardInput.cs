﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy.BaXi.Models
{
    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    public  class BaxiIDCardInput
    {
        [JsonProperty("waybillcode")]
        public string wayBillCode { get; set; }

        [JsonProperty("sign")]
        public string Sign { get; set; }
    }
}
