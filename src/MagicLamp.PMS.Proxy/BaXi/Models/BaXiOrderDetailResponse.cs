﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy.BaXi.Models
{
    public class BaXiOrderDetailResponse
    {
        /// <summary>
        /// 返回代码
        /// </summary>
        public string retCode { get; set; }

        /// <summary>
        /// 返回结果说明
        /// </summary>
        public string retMsg { get; set; }

        /// <summary>
        /// 订单信息
        /// </summary>
        public Order order { get; set; }

        public class Order
        {
            /// <summary>
            /// 商户订单编号
            /// </summary>
            public string orderNo { get; set; }

            /// <summary>
            /// 订单总价
            /// </summary>
            public string total { get; set; }

            public string totalPostCost { get; set; }

            /// <summary>
            /// 创建时间
            /// </summary>
            public string createTime { get; set; }

            /// <summary>
            /// 状态
            /// </summary>
            public string status { get; set; }

            /// <summary>
            /// 多个包裹数据packages
            /// </summary>
            public Package[] packages { get; set; }
        }

        public class Package
        {
            /// <summary>
            /// 包裹编号
            /// </summary>
            public string packageNo { get; set; }

            /// <summary>
            /// 包裹状态
            /// </summary>
            public string status { get; set; }

            /// <summary>
            /// 快递公司名称
            /// </summary>
            public string fastMail { get; set; }

            /// <summary>
            /// 物流查询地址
            /// </summary>
            public string fastUrl { get; set; }

            /// <summary>
            /// 包裹预估重量
            /// </summary>
            public string weight { get; set; }

            /// <summary>
            /// 包裹商品：packages.goods
            /// </summary>
            public Good[] goods { get; set; }
        }

        public class Good
        {
            /// <summary>
            /// 商品条码
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 商品upc
            /// </summary>
            public string upc { get; set; }

            /// <summary>
            /// 商品名称
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 商品数量
            /// </summary>
            public string number { get; set; }
        }
    }
}
