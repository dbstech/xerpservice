﻿using MagicLamp.PMS.BaXi;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MagicLamp.PMS.Proxy.BaXi.Models
{
    [DataContract]
    public class OrderDetailInputData : BaseData
    {
        [DataMember(Name = "orderNo")]
        public string orderNo { get; set; }
    }
}
