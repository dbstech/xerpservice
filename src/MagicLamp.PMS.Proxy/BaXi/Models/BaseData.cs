using System.Runtime.Serialization;

namespace MagicLamp.PMS.BaXi
{
    [DataContract]
    public class BaseData
    {
        [DataMember(Name = "customerCode")]
        public string CustomerCode { get; set; }
        [DataMember(Name = "secretKey")]        
        public string SecretKey { get; set; }
        [DataMember(Name = "timestamp")]
        public string Timestamp { get; set; }
        [DataMember(Name = "flag")]
        public string Flag { get; set; }
        [DataMember(Name = "token")]
        public string Token { get; set; }
    }
}