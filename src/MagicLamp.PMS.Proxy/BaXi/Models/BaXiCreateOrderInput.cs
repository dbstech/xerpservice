﻿using MagicLamp.PMS.BaXi;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MagicLamp.PMS.Proxy.BaXi.Models
{
    [DataContract]
    public class BaXiCreateOrderInput : BaseData
    {
        /// <summary>
        /// 订单编号（必填）
        /// </summary>
        [DataMember(Name = "orderNo")]
        public string orderNo { get; set; }

        /// <summary>
        /// 发件人（必填）
        /// </summary>
        [DataMember(Name = "sender")]
        public string sender { get; set; }

        /// <summary>
        /// 发件人电话（必填）
        /// </summary>
        [DataMember(Name = "senderPhone")]
        public string senderPhone { get; set; }

        /// <summary>
        /// 收件人（必填）
        /// </summary>
        [DataMember(Name = "consignee")]
        public string consignee { get; set; }

        /// <summary>
        /// 收件人电话（必填）
        /// </summary>
        [DataMember(Name = "phone")]
        public string phone { get; set; }

        /// <summary>
        /// 收件人省/直辖市（必填）
        /// </summary>
        [DataMember(Name = "province")]
        public string province { get; set; }

        /// <summary>
        /// 收件人市/区（必填）
        /// </summary>
        [DataMember(Name = "city")]
        public string city { get; set; }

        /// <summary>
        /// 收件人完整地址
        /// </summary>
        [DataMember(Name = "address")]
        public string address { get; set; }

        /// <summary>
        /// 奶粉快递编码
        /// </summary>
        [DataMember(Name = "powderList")]
        public List<string> powderList { get; set; }

        /// <summary>
        /// 杂货快递编码
        /// </summary>
        [DataMember(Name = "groceriesList")]
        public List<string> groceriesList { get; set; }

        /// <summary>
        /// 多个商品信息goods
        /// </summary>
        [DataMember(Name = "goods")]
        public List<Good> goods { get; set; }

        [DataContract]
        public class Good
        {
            /// <summary>
            /// 商品ID（必填）
            /// </summary>
            [DataMember(Name = "id")]
            public string id { get; set; }

            /// <summary>
            /// 商品数量（必填）
            /// </summary>
            [DataMember(Name = "number")]
            public string number { get; set; }
        }
    }
}
