namespace MagicLamp.PMS.BaXi
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class BaXiProduct
    {
        [JsonProperty("upc")]
        public string Upc { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("ename")]
        public string Ename { get; set; }

        [JsonProperty("pname")]
        public string Pname { get; set; }

        [JsonProperty("brand")]
        public string Brand { get; set; }

        [JsonProperty("category1")]
        public string Category1 { get; set; }

        [JsonProperty("category2")]
        public string Category2 { get; set; }

        [JsonProperty("stock")]
        public int Stock { get; set; }

        [JsonProperty("price")]
        public string Price { get; set; }

        [JsonProperty("deadline")]
        public DateTimeOffset? Deadline { get; set; }

        [JsonProperty("surl")]
        public string Surl { get; set; }

        [JsonProperty("urls")]
        public string Urls { get; set; }

        [JsonProperty("detailmageUrl")]
        public string DetailmageUrl { get; set; }

        [JsonProperty("trafficWeight")]
        public string TrafficWeight { get; set; }

        [JsonProperty("weight")]
        public string Weight { get; set; }

        [JsonProperty("express")]
        public string Express { get; set; }
        [JsonProperty("flag")]
        public string Flag { get; set; }
    }
}
