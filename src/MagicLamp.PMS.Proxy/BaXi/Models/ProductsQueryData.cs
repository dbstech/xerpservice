using System.Runtime.Serialization;

namespace MagicLamp.PMS.BaXi
{
    [DataContract]
    public class ProductsQueryData : BaseData
    {
        [DataMember(Name = "upcs")]
        public string Upcs { get; set; }
    }
}