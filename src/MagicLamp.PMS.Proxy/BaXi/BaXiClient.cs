using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.Infrastructure;
using RestSharp;
using System;
using System.Text;
using System.Security.Cryptography;
using System.Threading;
using TimeZoneConverter;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Abp.Json;
using Newtonsoft.Json;
using MagicLamp.PMS.Proxy;
using MagicLamp.Flux.API.Models;
using System.Collections.Generic;
using Abp.Dependency;
using MagicLamp.PMS.Proxy.BaXi.Models;
using MagicLamp.PMS.DTOs;
using System.Linq;
using Hangfire.Common;
using MagicLamp.PMS.Proxy.YiTongBaoOMSAPI.DeliveryOrderConfirm;
using static MagicLamp.PMS.Proxy.BaXi.Models.BaxiIDCardResponse;

namespace MagicLamp.PMS.BaXi
{
    public class BaXiClient : ITransientDependency
    {
        private BaXiConfig _config = Ultities.GetConfig<BaXiConfig>();
        private List<string> listOfCity = new List<string> { "北京市", "上海市", "天津市", "重庆市" };
        readonly IRestClient _restClient;
       

        public BaXiClient()
        {
            _restClient = new RestClient(_config.Url);
        }

        // todo: refactor Md5 functions as common extensions
        private string ByteToHex(byte[] bytes)
        {
            StringBuilder sign = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                String hex = (bytes[i] & 0xFF).ToString("X");
                if (hex.Length == 1)
                {
                    sign.Append("0");
                }
                sign.Append(hex.ToUpper());
            }
            return sign.ToString();
        }

        private string Md5(string message)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] input = Encoding.UTF8.GetBytes(message);
            byte[] buff = md5.ComputeHash(input);
            return ByteToHex(buff);
        }

        private string CreateToken(string flag, string timestamp)
        {
            string sign = $"customerCode={_config.CustomerCode}&secretKey={_config.SecretKey}&flag={flag}&timestamp={timestamp}";
            return Md5(sign).ToLower();
        }

        private async Task<JObject> ExecuteAsync(BaseData data, string uri = null, Method method = Method.POST)
        {
            var timeZone = TZConvert.GetTimeZoneInfo("Asia/Shanghai");
            string timestamp = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone).ToString("yyyyMMddHHmmss");

            string token = CreateToken(data.Flag, timestamp);
            data.CustomerCode = _config.CustomerCode;
            data.SecretKey = _config.SecretKey;
            data.Timestamp = timestamp;
            data.Token = token;

            IRestRequest request = new RestRequest(uri == null ? data.Flag : uri)
            {
                Method = method
            };

            request.AddParameter("data", data.ToJsonString());

            CLSLogger.Info("八禧request", data.ToJsonString());

            IRestResponse response = await _restClient.ExecuteAsync(request);


            JObject content = new JObject();


            try
            {
                content = JObject.Parse(response.Content);
            }
            catch (Exception ex)
            {
                CLSLogger.Info("八禧response.content", response.Content);
            }

            CLSLogger.Info("八禧response", content.ToJsonString());

            if (content.ToJsonString() == "{}")
            {
                return content;
            }
            else
                if (content["retCode"].ToObject<int>() != 0 && uri == null)
            {
                throw new BaXiException(content["retMsg"].ToString());
            }
            else {
                return content;
            }
            //return content;
        }

        public async Task<List<BaXiProduct>> ProductsQuery(List<String> upcs)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["upcs"] = upcs.ToJsonString()
            };

            ProductsQueryData requestData = new ProductsQueryData
            {
                Flag = "productsQuery",
                Upcs = string.Join(",", upcs)
            };

            try
            {
                JObject response = await ExecuteAsync(requestData);
                
                if (response == null || !response.ContainsKey("data"))
                {
                    CLSLogger.Info("八禧client库存返回值缺少 'data' 节点", response.ToJsonString());
                    return new List<BaXiProduct>();
                }

                JToken data = response["data"];

                List<BaXiProduct> products = ((JArray)data)
                    .ToObject<List<BaXiProduct>>()
                    .Where(x => x.Flag == "true")
                    .ToList();
                return products;

            }
            catch (Exception ex)
            {
                CLSLogger.Error("八禧client库存返回值", ex.ToJsonString());
                // 返回空列表作为失败时的默认值
                return new List<BaXiProduct>();
                
            }
            
        }


        public async Task<List<BaXiProduct>> ProductsOffQuery(List<String> upcs)
        {
            ProductsQueryData requestData = new ProductsQueryData
            {
                Flag = "productsQuery",
                Upcs = string.Join(",", upcs)
            };

            JObject response = await ExecuteAsync(requestData);
            JToken data = response.GetValue("data");

            List<BaXiProduct> products = ((JArray)data).ToObject<List<BaXiProduct>>().Where(x => x.Flag == "off").ToList();

            return products;
        }


        public async Task<(List<BaXiProduct> offProducts, List<BaXiProduct> trueProducts)> BAXIProductsQuery(List<String> upcs)
        {
            ProductsQueryData requestData = new ProductsQueryData
            {
                Flag = "productsQuery",
                Upcs = string.Join(",", upcs)
            };

            JObject response = await ExecuteAsync(requestData);
            JToken data = response.GetValue("data");

            var products = ((JArray)data).ToObject<List<BaXiProduct>>();

            var offProducts = products.Where(x => x.Flag == "off").ToList();
            var trueProducts = products.Where(x => x.Flag == "true").ToList();

            return (offProducts, trueProducts);
        }

        public async Task<List<BaXiProduct>> ProductsQueryAll()
        {
            BaseData requestData = new BaseData {
                Flag = "productsQueryAll"
            };

            JObject response = await ExecuteAsync(requestData);
            JToken data = response.GetValue("data");

            List<BaXiProduct> products = ((JArray)data).ToObject<List<BaXiProduct>>();

            return products;
        }

        public async Task<BaXiCreateOrderResponse> CreateOrder(BaseData input, string orderNo)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["orderpackno"] = orderNo
            };

            input.Flag = "createOrder";

            JObject response = await ExecuteAsync(input, "createOrderByMinFreight");
            //JToken data = response.GetValue("data");

            BaXiCreateOrderResponse result = null;

            CLSLogger.Info("八禧client创建订单结果", response.ToJsonString(), clogTags);

            try
            {
                result = response.ToObject<BaXiCreateOrderResponse>();
            }
            catch (Exception)
            {
                CLSLogger.Error("八禧client创建订单出错", response.ToJsonString(), clogTags);
            }

            return result;

        }

        public async Task<BaXiOrderDetailResponse> GetOrderDetail(string orderNo)
        {
            OrderDetailInputData requestData = new OrderDetailInputData
            {
                Flag = "orderDetail",
                orderNo = orderNo
            };

            JObject response = await ExecuteAsync(requestData, "orderDetail");

            return response.ToObject<BaXiOrderDetailResponse>();
        }

        public BaXiCreateOrderInput AssembleBaXiOrderInput(OrderPackDTO orderPackDTO, List<SimpleSKUDTO> listSKU)
        {
            var addressDetail = orderPackDTO.rec_addr.Split(" ");

            BaXiCreateOrderInput result = new BaXiCreateOrderInput
            {
                orderNo = orderPackDTO.orderPackNo,
                //发件人
                sender = "ALA",
                senderPhone = "0296000000",
                consignee = orderPackDTO.rec_name,
                phone = orderPackDTO.rec_phone,

                province = addressDetail[0],

                //直辖市在八禧，得用区
                city = ProcessCityForBaXi(addressDetail),

                address = ProcessAddressForBaXi(addressDetail, orderPackDTO),
                
                //物流
                //澳德标准线 4
                //ewe 1 (用于西藏，新疆地区)
                powderList = new List<string> { "1","4","3","10" },
                //groceriesList = new List<string> { "1","4","3","19" }, 
                //你们现在调用接口的时候，groceriesList 这个参数传的值可以有【1，EWE  ，4 澳德标准线 ，3 澳德专线，19 活动专线】； 如果传groceriesList=[1,4,3,9]，就会在4个物流中选择运费最便宜的。如果只传groceriesList=[1]，那么就只会走 ewe。
                groceriesList = new List<string> { "1" },
                goods = new List<BaXiCreateOrderInput.Good>()
            };

            foreach (var sku in listSKU)
            {
                result.goods.Add(new BaXiCreateOrderInput.Good
                {
                    id = sku.SKU,
                    number = sku.Qty.ToString()
                });
            }

            return result;
        }

        private string ProcessCityForBaXi(string[] addressDetail)
        {

            //匹配格式：市 市 区 
            //或者      市 市辖区 区 
            //或者      市 区
            return listOfCity.Contains(addressDetail[0], StringComparer.OrdinalIgnoreCase) 
                ? addressDetail[2] : addressDetail[1];
        }

        private string ProcessAddressForBaXi(string[] addressDetail, OrderPackDTO orderPackDTO)
        {
            //address里两个市，删除一个市。address里有市辖区，删除
            return listOfCity.Contains(addressDetail[0], StringComparer.OrdinalIgnoreCase) 
                ? orderPackDTO.rec_addr.Remove(3, 5) : orderPackDTO.rec_addr;
        }
    }
}