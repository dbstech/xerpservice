﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Data.SqlClient;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using System.Threading.Tasks;

namespace MagicLamp.PMS.Proxy
{
    public class UserLoginLogProxy
    {
      

        public static void AddUserLoginInfo(UserLoginInfo userLoginInfo)
        {
            
            PMSDbContext _pmsContext = new PMSDbContextFactory().CreateDbContext();

            UserLoginInfoEntity _userlogininfo = new UserLoginInfoEntity();
            _userlogininfo.UserName = userLoginInfo.UserName;
            _userlogininfo.IpAddress = userLoginInfo.IpAddress;
            _userlogininfo.UserAgent = userLoginInfo.UserAgent;
            _userlogininfo.LoginResult = userLoginInfo.LoginResult;
            _userlogininfo.city = userLoginInfo.City;
            _userlogininfo.region = userLoginInfo.Region;
            _userlogininfo.country = userLoginInfo.Country;
            _userlogininfo.loc = userLoginInfo.Loc;
            _userlogininfo.org = userLoginInfo.Org;
            _userlogininfo.timezone = userLoginInfo.Timezone;
            _pmsContext.UserLoginInfo.Add(_userlogininfo);
            _pmsContext.SaveChanges();
       
        }

        public class UserLoginInfo
        {
            public string UserName { get; set; }
            public string IpAddress { get; set; }
            public string UserAgent { get; set; }
            public string LoginResult { get; set; }
            public string City { get; set; }
            public string Region { get; set; }
            public string Country { get; set; }
            public string Loc { get; set; }
            public string Org { get; set; }
            public string Timezone { get; set; }
        }


        public static dynamic GetCountryFromIP(string ip, string apiKey)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = "GetCountryFromIP"
            };

            try
            {
                var url = $"http://ipinfo.io/{ip}?token={apiKey}";

                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(url).Result;
                    var json = response.Content.ReadAsStringAsync().Result;
                    dynamic result = JsonConvert.DeserializeObject(json);

                    //CLSLogger.Error("GetCountryFromIPSuccess", JsonConvert.SerializeObject(result), clogTags);
                    return result;
                }
            }
            catch (Exception ex)
            {
               CLSLogger.Error("GetCountryFromIP失败", ex, clogTags);
                return null;

            }
            
        }

        public static async Task<dynamic> GetCountryFromIPAsync(string ip, string apiKey)
        {
            var url = $"http://ipinfo.io/{ip}?token={apiKey}";

            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(url);
                var json = await response.Content.ReadAsStringAsync();
                dynamic result = JsonConvert.DeserializeObject(json);

                return result;
            }
        }

    }
}
