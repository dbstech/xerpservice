﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

using ProtoBuf;

using MagicLamp.PMS.DTOs.ThirdPart;
using System.IO;
using System.Net;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Infrastructure;
using Sentry;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.DTO;
using Newtonsoft.Json.Linq;

namespace MagicLamp.PMS.Proxy
{
    /// <summary>
    /// 日志级别
    /// </summary>
    public enum LogLevel
    {
        Info = 0,
        Warnning = 1,
        Error = 2
    }

    public class CLSLogger
    {

        //const string SHANGHAIREGION = "ap-shanghai";
        private static TencentCloudConfig CLSConfig = Ultities.GetConfig<TencentCloudConfig>();

        private static RestClient client = new RestClient(CLSConfig.CLSDomain);

        /// <summary>
        /// 记录错误级别日志
        /// </summary>
        /// <param name="title">日志标题</param>
        /// <param name="content">日志主要内容</param>
        /// <param name="tags">日志索引标签key-val组合</param>
        /// <returns>true:成功， false：失败</returns>
        public static void Error(string title, string content, Dictionary<string, string> tags = null)
        {
            CLSLogger.LogAsync(title, content, LogLevel.Error, tags);
            //SentrySdk.CaptureException(new Exception(content));
        }


        /// <summary>
        /// 记录错误级别日志
        /// </summary>
        /// <param name="title">日志标题</param>
        /// <param name="content">日志主要内容</param>
        /// <param name="tags">日志索引标签key-val组合</param>
        /// <returns>true:成功， false：失败</returns>
        public static void Error(string title, Exception ex, Dictionary<string, string> tags = null)
        {
            CLSLogger.LogAsync(title, ex.ToString(), LogLevel.Error, tags);
            //SentrySdk.CaptureException(ex);
        }

        /// <summary>
        /// 记录错误级别日志
        /// </summary>
        /// <param name="title">日志标题</param>
        /// <param name="content">日志主要内容</param>
        /// <param name="tags">日志索引标签key-val组合</param>
        /// <returns>true:成功， false：失败</returns>
        public static void ErrorWithSentry(string title, Exception ex, Dictionary<string, string> tags = null)
        {
            CLSLogger.LogAsync(title, ex.ToString(), LogLevel.Error, tags);
            SentrySdk.CaptureException(ex);
        }


        /// <summary>
        /// 记录错误级别日志
        /// </summary>
        /// <param name="title">日志标题</param>
        /// <param name="content">日志主要内容</param>
        /// <param name="tags">日志索引标签key-val组合</param>
        /// <returns>true:成功， false：失败</returns>
        public static void ErrorWithoutSentry(string title, Exception ex, Dictionary<string, string> tags = null)
        {
            CLSLogger.LogAsync(title, ex.ToString(), LogLevel.Error, tags);
        }

        /// <summary>
        /// 记录信息级别日志
        /// </summary>
        /// <param name="title">日志标题</param>
        /// <param name="content">日志主要内容</param>
        /// <param name="tags">日志索引标签key-val组合</param>
        /// <returns>true:成功， false：失败</returns>
        public static void Info(string title, string content, Dictionary<string, string> tags = null)
        {
            CLSLogger.LogAsync(title, content, LogLevel.Info, tags);
        }

        public static void LogRequest<T>(string requestType, T request) where T : class
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = requestType;
            CLSLogger.LogAsync($"{requestType} Request", request.ToJsonString(), LogLevel.Info, tags);
        }

        public static void LogResponse<T>(string requestType, T response) where T : class
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = requestType;
            CLSLogger.LogAsync($"{requestType} Response", response.ToJsonString(), LogLevel.Info, tags);
        }

        /// <summary>
        /// 记录警告级别日志
        /// </summary>
        /// <param name="title">日志标题</param>
        /// <param name="content">日志主要内容</param>
        /// <param name="tags">日志索引标签key-val组合</param>
        /// <returns>true:成功， false：失败</returns>
        public static void Warn(string title, string content, Dictionary<string, string> tags = null)
        {
            CLSLogger.LogAsync(title, content, LogLevel.Warnning, tags);
        }

        private static void LogAsync(string title, string content, LogLevel logLevel, Dictionary<string, string> tags)
        {
            try
            {
                CLSLogger.Log(title, content, logLevel, tags);
            }
            catch (Exception ex)
            {
            }
        }

        private static void Log(string title, string content, LogLevel logLevel, Dictionary<string, string> tags)
        {
            string uri = "/structuredlog";
            string logSetId = CLSConfig.CLSLogSetID;
            string topicId = CLSConfig.CLSLogTopicID;
            string secretId = CLSConfig.APPSecretID;
            string secretKey = CLSConfig.APPSecretKey;
            DateTime startTime = DateTime.Now;
            DateTime endTime = DateTime.Now.AddDays(1);
            string authorazation = CLSLogger.GenerateAuthorazationString(secretId, secretKey, "post", uri, startTime, endTime);

            if (tags == null)
            {
                tags = new Dictionary<string, string>();
            }

            LogGroupList logGroups = new LogGroupList
            {
                logGroupList = new List<LogGroup>()
            };
            LogGroup logGroup = new LogGroup
            {
                logs = new List<Log>(),
                contextFlow = "",//保持上下文的 UID，该字段目前暂无效用
                source = ""//日志来源，一般使用机器 IP 作为标识
            };
            logGroups.logGroupList.Add(logGroup);

            Log log = new Log
            {
                time = DateTime.Now.ToUnixTimeInMilliSeconds(),
                contents = new List<Content>()
            };
            logGroup.logs.Add(log);

            Content titleContent = new Content
            {
                key = "title",
                value = title
            };
            Content logContent = new Content
            {
                key = "content",
                value = content
            };
            Content levelContent = new Content
            {
                key = "loglevel",
                value = logLevel.ToString()
            };
            Content appid = new Content
            {
                key = "appid",
                value = "xerpservice"
            };
            log.contents.Add(titleContent);
            log.contents.Add(logContent);
            log.contents.Add(levelContent);
            log.contents.Add(appid);


            foreach (var item in tags)
            {
                log.contents.Add(new Content
                {
                    key = item.Key,
                    value = item.Value
                });
            }

            var protoBufBytes = ProtoBufHelper.Serialize(logGroups);


            RestRequest request = new RestRequest(uri);
            request.Method = Method.POST;
            request.AddQueryParameter("topic_id", topicId);
            request.AddHeader("Authorization", authorazation);
            request.AddParameter("application/x-protobuf", protoBufBytes, ParameterType.RequestBody);
            //client.Post(request);
            client.PostAsync(request, (res, callback) =>
            {
                //do things when get response
            });
        }

        /// <summary>
        /// 对字符串进行SHA1加密
        /// </summary>
        /// <param name="strIN">需要加密的字符串</param>
        /// <returns>密文</returns>
        private static string SHA1_Encrypt(string Source_String)
        {
            byte[] StrRes = Encoding.Default.GetBytes(Source_String);
            HashAlgorithm iSHA = new SHA1CryptoServiceProvider();
            StrRes = iSHA.ComputeHash(StrRes);
            StringBuilder EnText = new StringBuilder();
            foreach (byte iByte in StrRes)
            {
                EnText.AppendFormat("{0:x2}", iByte);
            }
            return EnText.ToString().ToLower();
        }

        private static string ToHMACSHA1(string encryptText, string encryptKey)
        {
            //HMACSHA1加密
            HMACSHA1 hmacsha1 = new HMACSHA1();
            hmacsha1.Key = System.Text.Encoding.UTF8.GetBytes(encryptKey);
            byte[] dataBuffer = System.Text.Encoding.UTF8.GetBytes(encryptText);
            byte[] hashBytes = hmacsha1.ComputeHash(dataBuffer);
            string str = BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
            return str;
        }

        /// <summary>
        /// 生成请求认证串
        /// </summary>
        /// <param name="secretId"></param>
        /// <param name="secretKey"></param>
        /// <param name="method"></param>
        /// <param name="uri"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="region"></param>
        /// <param name="logSetId"></param>
        /// <returns></returns>
        private static string GenerateAuthorazationString(string secretId, string secretKey, string method, string uri, DateTime startTime, DateTime endTime)
        {
            //https://cloud.tencent.com/document/product/614/12445
            string qsigntime = string.Format("{0};{1}", startTime.ToUnixTimeInSeconds(), endTime.ToUnixTimeInSeconds());
            string qkeytime = qsigntime;

            Dictionary<string, string> keys = new Dictionary<string, string>();
            keys["q-sign-algorithm"] = "sha1";
            keys["q-ak"] = secretId;
            keys["q-sign-time"] = qsigntime;
            keys["q-key-time"] = qkeytime;
            keys["q-header-list"] = "";
            keys["q-url-param-list"] = "";
            keys["q-signature"] = CLSLogger.GenerateSignature(secretId, secretKey, qsigntime, qkeytime, method, uri, "", "");

            StringBuilder authorazationStr = new StringBuilder();
            foreach (var item in keys)
            {
                authorazationStr.AppendFormat("{0}={1}&", item.Key, item.Value);
            }
            authorazationStr.Length--;

            return authorazationStr.ToString();
        }

        /// <summary>
        /// 生成签名认证
        /// </summary>
        /// <param name="secretId"></param>
        /// <param name="secretKey"></param>
        /// <param name="qsigntime"></param>
        /// <param name="qkeytime"></param>
        /// <param name="method"></param>
        /// <param name="uri"></param>
        /// <param name="formatedParameters"></param>
        /// <param name="formatedHeaders"></param>
        /// <returns></returns>
        private static string GenerateSignature(string secretId, string secretKey, string qsigntime, string qkeytime, string method, string uri, string formatedParameters, string formatedHeaders)
        {
            //文档：https://cloud.tencent.com/document/product/614/12445

            string httpRequestInfo = string.Format("{0}\n{1}\n{2}\n{3}\n", method.ToLower(), uri.ToLower(), formatedParameters, formatedHeaders);
            string stringToSign = string.Format("{0}\n{1}\n{2}\n", "sha1", qsigntime, CLSLogger.SHA1_Encrypt(httpRequestInfo));
            string signKey = CLSLogger.ToHMACSHA1(qkeytime, secretKey);
            string signature = CLSLogger.ToHMACSHA1(stringToSign, signKey);
            return signature;
        }

        public static CLSSearchLogOutputDto SearchLog(CLSSearchLogInputDto input)
        {
            string uri = "/searchlog";
            RestRequest request = new RestRequest(uri, Method.GET);
            string logSetId = CLSConfig.CLSLogSetID;
            string secretId = CLSConfig.APPSecretID;
            string secretKey = CLSConfig.APPSecretKey;
            DateTime startTime = DateTime.Now;
            DateTime endTime = DateTime.Now.AddDays(1);            
            string authorization = CLSLogger.GenerateAuthorazationString(secretId, secretKey, "get", uri, startTime, endTime);

            // TODO: DRY request quer parameters
            request.AddHeader("Authorization", authorization);
            request.AddQueryParameter("topic_ids", CLSConfig.CLSLogTopicID);
            request.AddQueryParameter("logset_id", CLSConfig.CLSLogSetID);
            request.AddQueryParameter("start_time", input.startTime);
            request.AddQueryParameter("end_time", input.endTime);
            request.AddQueryParameter("query_string", input.queryString);
            request.AddQueryParameter("limit", input.limit.ToString());
            request.AddQueryParameter("sort", input.sort.ToString());

            var response = client.Get(request);

            JObject content = new JObject();
            content = JObject.Parse(response.Content);
            var output = content.ToObject<CLSSearchLogOutputDto>();
            return output;
        }
    }
}