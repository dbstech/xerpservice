﻿using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.Infrastructure;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy
{
    /// <summary>
    /// EverAustralia专门给客户用的UGG代发和查库存
    /// </summary>
    public class UGGProxy
    {
        //private static EverAustraliaConfig config = Ultities.GetConfig<EverAustraliaConfig>();

        /// <summary>
        /// 获取密钥
        /// </summary>
        /// <returns></returns>
        public static string GetToken()
        {
            var config = Ultities.GetConfig<EverAustraliaConfig>();

            try 
            {
                RestClient client = new RestClient(config.Url);
                RestRequest request = new RestRequest("Api/Token/getToken");

                request.AddParameter("user", config.Username, ParameterType.GetOrPost);
                request.AddParameter("password", config.Password, ParameterType.GetOrPost);

                var response = client.Get(request);
                string content = response.Content;
                JObject obj = JObject.Parse(content);
                if (obj != null && obj["result"]["token"] != null)
                {
                    return obj["result"].Value<string>("token");
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// 查全部澳洲库
        /// </summary>
        /// <returns></returns>
        public static List<UGGStockResultDTO> GetAllAuStock()
        {
            string au = "Api/Stock/AuStock";
            return UGGProxy.GetAllStock(au);
        }

        /// <summary>
        /// 查全部中国库
        /// </summary>
        /// <returns></returns>
        public static List<UGGStockResultDTO> GetAllCnStock()
        {
            string cn = "Api/Stock/ZjStock";
            return UGGProxy.GetAllStock(cn);
        }

        public static List<UGGStockResultDTO> GetAllStock(string requestAPI)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>();
            clogTags["jobid"] = Guid.NewGuid().ToString();
            clogTags["type"] = "StockForUGGStock";


            var config = Ultities.GetConfig<EverAustraliaConfig>();

            List<UGGStockResultDTO> listResult = new List<UGGStockResultDTO>();

            string tokenResult = UGGProxy.GetToken();
            try
            {
                RestClient client = new RestClient(config.Url);
                RestRequest request = new RestRequest(requestAPI);

                request.AddParameter("token", tokenResult, ParameterType.GetOrPost);
                var response = client.Get(request);
                string content = response.Content;
                JObject obj = JObject.Parse(content);
                if (obj != null && obj["result"] != null)
                {
                    listResult = obj["result"].ToJsonString().ConvertFromJsonString<List<UGGStockResultDTO>>();
                }

                CLSLogger.Info("UGGAPI_result"+requestAPI.ToString(), obj.ToJsonString(), clogTags);
            }
            catch (Exception ex)
            {
                CLSLogger.Info("UGGAPI_error" + requestAPI.ToString(), ex.ToJsonString(), clogTags);
                return null;
            }
            return listResult;
        }
    }
}
