using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.DTOs;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using RestSharp;
using MagicLamp.PMS.DTOs.BondedWarehouse;
using MagicLamp.PMS.Infrastructure.Extensions;
using System.Text;
using MagicLamp.PMS.DTO.BondedWarehouse;

namespace MagicLamp.PMS.Proxy
{
    public class BondedWarehouseProxy
    {
        BondedWarehouseConfig Config = Ultities.GetConfig<BondedWarehouseConfig>();

        readonly IRestClient restClient;
        public BondedWarehouseProxy()
        {
            restClient = new RestClient(Config.Url);
        }

        public List<BondedWarehouseProductDTO> GetAllProducts(bool idCode2 = false)
        {
            var tags = new Dictionary<string, string>();
            tags["type"] = "getAllProductsFromBondedWarehouse";
            tags["trackid"] = Guid.NewGuid().ToString();

            var request = new RestRequest(Method.POST);
            var data = new
            {
                IDCODE = idCode2 ? Config.IdCode2 : Config.IdCode,
                SKUNUMS = new string[] { },
                ISALL = "Y"
            };

            request.AddParameter("me", "invLotQuery");
            request.AddParameter("data", JsonConvert.SerializeObject(data));
            request.ToString();

            IRestResponse response = restClient.Execute(request);

            if (response == null || response.ErrorException != null)
            {
                CLSLogger.Info("getAllProductsFromBondedWarehouse响应报文", "返回为空，发生异常", tags);
                return null;
            }

            CLSLogger.Info("getAllProductsFromBondedWarehouse响应报文", "Response: " + response.Content, tags);

            JObject content = JObject.Parse(response.Content);

            if ((int)content["RESULTCODE"] != 0)
            {
                CLSLogger.Info("getAllProductsFromBondedWarehouse响应报文", (string)content["RESULTMESSAGE"], tags);
                return null;
            }

            List<BondedWarehouseProductDTO> products = ((JArray)content["INVENTORYS"]).Select(
                x =>
                {
                    JObject product = (JObject)x;

                    int quantity;
                    DateTime expiryDate;

                    int.TryParse(product["AVAQTY"].ToString(), out quantity);
                    DateTime.TryParse(product["LOT"].ToString(), out expiryDate);

                    return new BondedWarehouseProductDTO
                    {
                        Barcode = product["SKUNUM"].ToString(),
                        Quantity = quantity,
                        ExpiryDate = expiryDate
                    };
                }
            ).ToList();

            return products;
        }


        public List<BondedWarehouseProductDTO> T3GetAllProductsQty(bool idCode2 = false)
        {
            var tags = new Dictionary<string, string>();
            tags["type"] = "getAllProductsinvQuery";
            tags["trackid"] = Guid.NewGuid().ToString();

            var request = new RestRequest(Method.POST);
            var data = new
            {
                IDCODE = idCode2 ? Config.IdCode2 : Config.IdCode,
                SKUNUMS = new string[] { },
                ISALL = "Y"
            };

            request.AddParameter("me", "invQuery");
            request.AddParameter("data", JsonConvert.SerializeObject(data));
            request.ToString();

            IRestResponse response = restClient.Execute(request);

            if (response == null || response.ErrorException != null)
            {
                CLSLogger.Info("getAllProductsinvQuery响应报文", "返回为空，发生异常", tags);
                return null;
            }

            CLSLogger.Info("getAllProductsinvQuery响应报文", "Response: " + response.Content, tags);

            JObject content = JObject.Parse(response.Content);

            if ((int)content["RESULTCODE"] != 0)
            {
                CLSLogger.Info("getAllProductsinvQuery响应报文", (string)content["RESULTMESSAGE"], tags);
                return null;
            }

            List<BondedWarehouseProductDTO> products = ((JArray)content["INVENTORYS"]).Select(
                x =>
                {
                    JObject product = (JObject)x;
                    int quantity;

                    int.TryParse(product["AVAQTY"].ToString(), out quantity);

                    return new BondedWarehouseProductDTO
                    {
                        Barcode = product["SKUNUM"].ToString(),
                        Quantity = quantity
                    };
                }
            ).ToList();

            return products;
        }

        public SiLuCreateOrderOutputDto OutOrderAdd(SiLuCreateOrderInputDto inputDto, bool idCode2 = false)
        {
            inputDto.idCode = idCode2 ? Config.IdCode2 : Config.IdCode;
            var tags = new Dictionary<string, string>();
            tags["type"] = "BondedWarehouseOutOrderAdd";
            tags["trackid"] = Guid.NewGuid().ToString();
            tags["orderNum"] = inputDto.orders.First().orderNum;

            var request = new RestRequest(Method.POST);
            request.AddParameter("me", "outOrderAdd");
            request.AddParameter("data", JsonConvert.SerializeObject(inputDto));

            CLSLogger.Info("BondedWarehouseOutOrderAddRequest", inputDto.ToJsonString(), tags);

            IRestResponse response = restClient.Execute(request);

            if (response == null || response.ErrorException != null)
            {
                CLSLogger.Info("BondedWarehouseOutOrderAddResponse", "返回为空，发生异常", tags);
                throw new Exception("Empty response");
            }

            CLSLogger.Info("BondedWarehouseOutOrderAddResponse", "Response: " + response.Content.ToJsonString(), tags);

            JObject content = JObject.Parse(response.Content);

            SiLuCreateOrderOutputDto outputDto = content.ToObject<SiLuCreateOrderOutputDto>();
            return outputDto;
        }



        /// <summary>
        /// 出库订单一程面单
        /// </summary>
        /// <param name="inputDTO"></param>
        /// <returns></returns>
        public GMOrderFirstExpressOutputDTO OrderFirstExpress(GMOrderFirstExpressDTO inputDTO, bool idCode2 = false)
        {
            GMOrderFirstExpressOutputDTO returnOutput = null;

            inputDTO.IDCODE = idCode2 ? Config.IdCode2 : Config.IdCode;

            Dictionary<string, string> clogTags = new Dictionary<string, string>();
            clogTags["type"] = "orderfirstexpress";
            clogTags["orderid"] = inputDTO.ORDERS.FirstOrDefault().ORDERNO;

            CLSLogger.Info("OrderFirstExpressInput", inputDTO.ToJsonString(), clogTags);

            //form里面每一个name都要加进来
            string postString = $"me=orderFirstExpress&data={inputDTO.ToJsonString()}";
            byte[] postData = Encoding.UTF8.GetBytes(postString);

            string url = "http://61.128.133.130:8292/T3/ownererp.jsp";

            using (System.Net.WebClient webClient = new System.Net.WebClient())
            {
                webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                byte[] responseData = webClient.UploadData(url, "POST", postData);
                string srcString = Encoding.UTF8.GetString(responseData);
                var returnObject = JObject.Parse(srcString);
                returnOutput = returnObject.ToObject<GMOrderFirstExpressOutputDTO>();
                CLSLogger.Info("OrderFirstExpressResponse", returnObject.ToJsonString(), clogTags);
            }

            return returnOutput;
        }
    }
}