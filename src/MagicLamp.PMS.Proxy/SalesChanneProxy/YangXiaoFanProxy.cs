﻿using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.DTOs.ThirdPart;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace MagicLamp.PMS.Proxy
{
    public class YangXiaoFanProxy
    {
        private const string ClogTagType = "YangXiaoFanProxy";
        //private static string url = "https://weapp.mofahaitao.com/heapi/update_products";
        //private static string key = "tldj9iilzOtewrQqqMoqfk";

        public void SyncStock(YangXiaoFanSyncStockPostDTO input)
        {
            var config = Ultities.GetConfig<YangXiaoFanConfig>();

            //add key
            input.key = config.Key;

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = ClogTagType;
            tags["trackid"] = Guid.NewGuid().ToString();
            tags["method"] = "SyncStock";

            string postData = input.ToJsonString();
            CLSLogger.Info("库存同步洋小范请求报文", postData, tags);

            string method = "update_products";
            string url = $"{config.Url}{method}";

            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/json";
            string json = input.ToJsonString();
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(json);

            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }


            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    string resJson = reader.ReadLine();

                    if (response != null && !resJson.IsNullOrEmpty())
                    {
                        CLSLogger.Info("库存同步洋小范响应报文", resJson, tags);
                    }
                    else
                    {
                        CLSLogger.Info("库存同步洋小范响应报文", "洋小范返回为空", tags);
                    }
                }
            }
        }
    }
}
