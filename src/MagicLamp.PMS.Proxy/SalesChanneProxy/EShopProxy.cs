﻿using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace MagicLamp.PMS.Proxy
{


    public class EShopProxy
    {

        RestClient EShopRestClient = null;

        public EShopProxy()
        {
            EShopRestClient = new RestClient("https://188.nz");
        }


        public void SyncStockTo188(List<SyncAladdinStockDTO> dtos)
        {
            var config = Ultities.GetConfig<EShopConfig>();

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["trackid"] = Guid.NewGuid().ToString();  

            string requestJson = dtos.ToJsonString();
            CLSLogger.Info("库存同步188请求报文", requestJson, tags);

            string method = "update_products";
            string api = $"{config.Url}{method}?key={config.Key}";


            HttpWebRequest request = WebRequest.Create(api) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/json";
            string json = dtos.ToJsonString();
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(json);

            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }


            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            
            {
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    string resJson = reader.ReadLine();

                    if (response != null && !resJson.IsNullOrEmpty())
                    {
                        CLSLogger.Info("库存同步188响应报文", resJson, tags);
                    }
                    else
                    {
                        CLSLogger.Info("库存同步188响应报文", "188返回为空", tags);
                    }
                }
            }

        }
    }
}
