﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using MagicLamp.PMS.DTOs.ThirdPart;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.DTO;
using Abp.Dependency;
using Abp.Runtime.Caching;
using MagicLamp.PMS.Infrastructure.Common;

namespace MagicLamp.PMS.Proxy
{
    public class AUExpressProxy
    {

        public string UserName { get; set; }
        public string Password { get; set; }

        public AUExpressProxy(string username, string password)
        {
            UserName = username;
            Password = password;
        }

        /// <summary>
        /// 获取授权token
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string GetAccessToken(string username, string password)
        {
            try
            {
                RestClient client = new RestClient("http://auth.auexpress.com/");
                RestRequest request = new RestRequest("api/token");
                request.AddJsonBody(new
                {
                    MemberId = username,
                    Password = password
                });

                var response = client.Post(request);
                string content = response.Content;
                JObject obj = JObject.Parse(content);
                if (obj != null && obj["Token"] != null)
                {
                    return obj.Value<string>("Token");
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public bool UploadIDCard(AUExpressUploadIDCardDTO dto, ref string errorMsg)
        {
            string ddToken = "7f8db451bf15f9ee3ec3c591aebf78be4db86fcda93a8c71d426c5c6eff76515";
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "auuploadidcard";
            tags["orderid"] = string.Join(';', dto.OrderIDs);
            string reqjson = string.Empty;
            string resjson = string.Empty;
            long duration = 0;
            try
            {
                RestClient client = new RestClient("http://aueapi.auexpress.com/");
                RestRequest request = new RestRequest("api/PhotoIdUpload");
                AddTokentoRequest(request);
                request.AddJsonBody(dto);

                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                sw.Start();

                var response = client.Post(request);

                sw.Stop();
                duration = sw.ElapsedMilliseconds;

                string photoFront = dto.PhotoFront;
                string photoRear = dto.PhotoRear;

                dto.PhotoFront = string.Empty;
                dto.PhotoRear = string.Empty;
                reqjson = JsonConvert.SerializeObject(dto);
                resjson = response.Content;

                dto.PhotoFront = photoFront;
                dto.PhotoRear = photoRear;

                JObject obj = JObject.Parse(response.Content);
                if (obj == null || !obj.HasValues)
                {
                    return false;
                }

                string message = obj["Message"].Value<string>();
                int code = obj["Code"].Value<int>();
                string returnResult = obj["ReturnResult"].Value<string>();

                if (returnResult != "Success")
                {
                    errorMsg = message;
                    string title = "澳邮自动上传身份证出错";
                    string text = $"包裹号：{dto.OrderPackNo}，身份证号：{dto.PhotoID}，运单号：{string.Join(";", dto.OrderIDs)},原因：{message}";
                    CLSLogger.Error(title, text, tags);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("澳邮上传身份证发生异常", ex, tags);
                return false;
            }
            finally
            {
                CLSLogger.Info("澳邮上传身份证API", $"请求：{reqjson}     响应：{resjson}   总耗时：{duration} ms", tags);
            }

        }

        protected bool AddTokentoRequest(RestRequest request)
        {

            if (string.IsNullOrEmpty(UserName) || string.IsNullOrWhiteSpace(Password))
            {
                throw new Exception("请检查澳邮账号配置");
            }

            string token = GetAccessToken(UserName, Password);

            if (string.IsNullOrWhiteSpace(token))
            {
                return false;
            }

            request.AddHeader("Authorization", string.Format("Bearer {0}", token));

            return true;
        }

        public static void SendAURelatedAddressErrorNotificationToDingDing(AUExpressShipmentOrderDTO order, string error)
        {
            try
            {
                if (error.Contains("Invaild AuExpress Brand Id"))
                {
                    return;
                }

                string key = string.Format(CacheKeys.AUEXPRESS, order.ERPOrderID);


                var Cache = IocManager.Instance.Resolve<ICacheManager>();

                var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);
                var val = cacheManager.GetOrDefault(key);


                if (val == null)
                {
                    string title = "订单包裹生成澳邮运单号失败";
                    StringBuilder text = new StringBuilder();
                    text.AppendFormat("### 以下订单包裹生成澳邮运单号失败，错误原因：' {0} ' \n\n", error);
                    text.AppendFormat("[{0}](https://xerp-next.aladdin.nz/#/orderManagement/parcel/form?orderPackNo={1})  \n\n", order.ERPOrderID, order.ERPOrderID);
                    text.AppendFormat("------------以下是收件信息------------- \n\n");
                    text.AppendFormat("收件人：{0} \n\n", order.ReceiverName);
                    text.AppendFormat("收件省份：{0} \n\n", order.ReceiverProvince);
                    text.AppendFormat("收件城市：{0} \n\n", order.ReceiverCity);
                    text.AppendFormat("收件地址：{0} \n\n", order.ReceiverAddr1);
                    DDNotificationProxy.MarkdownNotification(title, text.ToString(), "07bd13e4883f31bbab094c38a3c08224bdb3f5e4fdf9b011f7de5369752bdc4f");
                    cacheManager.Set(key, 1, TimeSpan.Zero, TimeSpan.FromDays(1));
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// 创建AU运单
        /// </summary>
        /// <param name="orders"></param>
        /// <returns></returns>
        public AUExpressShipmentOrderResponseDTO CreateShipmentOrder(List<AUExpressShipmentOrderDTO> orders)
        {
            var orderPackNo = orders.Select(x => x.ERPOrderID).FirstOrDefault();
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["orderpackno"] = orderPackNo,
                ["type"] = "AUExpressProxy"
            };

            AUExpressShipmentOrderResponseDTO orderResponse = new AUExpressShipmentOrderResponseDTO();
            orderResponse.OrderInfos = new List<AUExpressShipmentOrderInfoDTO>();
            string reqjson = string.Empty;
            string resjson = string.Empty;
            //string apikey = "AUCreateShipmentOrder";
            try
            {
                RestClient client = new RestClient("http://aueapi.auexpress.com/");
                RestRequest request = new RestRequest("api/AgentShipmentOrder/Create");
                AddTokentoRequest(request);
                request.AddJsonBody(orders);
                reqjson = JsonConvert.SerializeObject(request.Parameters);

                var response = client.Post(request);
                string content = response.Content;
                resjson = content;
                JObject obj = JObject.Parse(content);
                if (obj == null || !obj.HasValues)
                {
                    return orderResponse;
                }

                string message = obj["Message"].Value<string>();
                string returnResult = obj["ReturnResult"].Value<string>();
                string errors = obj["Errors"].HasValues ? obj["Errors"].ToString() : string.Empty;
                int code = obj["Code"].Value<int>();
                orderResponse.Code = code;
                orderResponse.Message = message;
                orderResponse.ErrorMsg = string.IsNullOrEmpty(errors) ? string.Empty : string.Format("{0}；message：{1}；", errors, message);
                orderResponse.IsSuccess = code == 0;

                if (string.IsNullOrWhiteSpace(message))
                {
                    return orderResponse;
                }

                var auOrderIDs = message.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                if (auOrderIDs == null || auOrderIDs.Length == 0)
                {
                    return orderResponse;
                }

                if (auOrderIDs.Length != orders.Count)
                {
                    orderResponse.ErrorMsg += "异常：创建订单数与返回快递单号个数不一致";
                    orderResponse.IsSuccess = false;
                    return orderResponse;
                }

                int errorIndex = 0;
                for (int i = 0; i < auOrderIDs.Length; i++)
                {
                    if (auOrderIDs[i] == "-1")
                    {
                        string error = string.Empty;
                        if (obj["Errors"] != null && obj["Errors"].ElementAt(errorIndex) != null)
                        {
                            error = obj["Errors"].ElementAt(errorIndex)["Message"].ToString();
                        }
                        SendAURelatedAddressErrorNotificationToDingDing(orders[i], error);
                        errorIndex++;
                    }

                    AUExpressShipmentOrderInfoDTO item = new AUExpressShipmentOrderInfoDTO
                    {
                        AUExpressShipmentOrderID = auOrderIDs[i],
                        ERPOrderID = orders[i].ERPOrderID,
                    };
                    orderResponse.OrderInfos.Add(item);
                }
                orderResponse.IsSuccess = true;
                return orderResponse;
            }
            catch (Exception ex)
            {
                orderResponse.ErrorMsg += "创建AU快递单发生异常：" + ex.ToString();
                return orderResponse;
            }
            finally
            {
                CLSLogger.Info("AUCreateShipmentOrder结束", $"结果：{resjson}。参数：{reqjson}", logTags);
            }
        }

    }
}