﻿using MagicLamp.PMS.DTOs;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy.FreightsAPI
{
    public abstract class BaseFreightProxy
    {

        protected RestClient RestSharpClient = null;


        protected void SetRestSharpClientDomain(string domain)
        {
            RestSharpClient = new RestClient(domain);
        }


        public abstract FreightCreateOrderOutputDTO CreateOrder(FreightCreateOrderInputDTO inputDto);


    }
}
