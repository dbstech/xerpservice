﻿using System;
using System.Collections.Generic;
using System.Text;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Infrastructure.Extensions;
using System.Linq;
using System.Security.Cryptography;
using MagicLamp.PMS.Infrastructure;
using Newtonsoft.Json.Linq;
using RestSharp;
using MagicLamp.PMS.DTOs.Configs;

namespace MagicLamp.PMS.Proxy.FreightsAPI
{
    public class YdtFreightProxy : BaseFreightProxy
    {
        BusinessConfig freightConfig = null;

        public YdtFreightProxy()
        {
            freightConfig = Ultities.GetConfig<BusinessConfig>();
            SetRestSharpClientDomain(freightConfig.YdtAPIDomain);
        }


        /// <summary>
        /// 创建物流订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public override FreightCreateOrderOutputDTO CreateOrder(FreightCreateOrderInputDTO input)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>();
            clogTags["type"] = "ydtcreateorder";
            clogTags["trackid"] = input.ParcelNo;
            clogTags["orderid"] = input.OrderID;

            var goodsItems = input.GoodItems.Select(x => new
            {
                GoodsName = x.Name,
                GoodsQuantity = x.Quantity,
                GoodsUnit = x.Unit,
                GoodsPrice = x.Price
            }).ToList();

            var order = new
            {
                AppID = freightConfig.YdtAPPID,
                Nonce = Guid.NewGuid().ToString("N"),
                Timestamp = DateTime.UtcNow.ToUnixTimeInSeconds(),
                SfOrderNumber = input.FreightTrackNo,
                OutOrderNumber = input.ParcelNo,
                SenderMan = input.SenderName,
                SenderAddr = input.SenderAddress,
                SenderPhone = input.SenderPhone,
                SenderPostCode = input.SenderPostCode,
                ConsigneeMan = input.ReceiverName,
                ConsigneeAddr = input.ReceiverAddress,
                ConsigneePhone = input.ReceiverPhone,
                ConsigneePostCode = input.ReceiverPostCode,
                IdNo = input.ReceiverIdNo,
                SfOrdersGoods = goodsItems,
                AppSecret = freightConfig.YdtAppSecret
            };

            string jsonStr = order.ToJsonString();
            var sign = Ultities.GetMd5Hash(jsonStr);

            var jObject = JObject.Parse(jsonStr);
            jObject["Sign"] = sign;

            string reqJson = jObject.ToString();
            CLSLogger.Info("易达通顺丰下单请求报文", reqJson, clogTags);

            //call the create order api
            var apiPath = "sforders/add";
            RestRequest request = new RestRequest(apiPath);
            request.Method = Method.POST;
            request.AddJsonBody(reqJson);
            var response = RestSharpClient.Post<JObject>(request);

            FreightCreateOrderOutputDTO output = new FreightCreateOrderOutputDTO();

            if (response == null)
            {
                output.Message = "Unknow error, api return null";
            }

            var resJObject = response.GetJObject();
            CLSLogger.Info("易达通顺丰下单返回报文", resJObject.ToString(), clogTags);

            output.Success = resJObject.Value<bool>("Success");
            output.Code = resJObject.Value<string>("Code");
            output.Message = resJObject.Value<string>("Msg");

            if (resJObject["Data"] != null)
            {
                var dataJObject = JObject.Parse(resJObject["Data"].Value<string>());
                output.CourierTrackNo = dataJObject["SfOrderNumber"].Value<string>();
                output.CourierReciept = dataJObject["MdFile"].Value<string>();
            }

            output.Success = true;
            return output;

        }


    }


}
