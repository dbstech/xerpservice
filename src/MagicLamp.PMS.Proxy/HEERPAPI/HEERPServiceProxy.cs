﻿using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy
{
    public class HEERPServiceProxy
    {

        RestClient HEERPServiceClient = null;
        string ServiceKey = string.Empty;

        public HEERPServiceProxy()
        {
            var config = Ultities.GetConfig<BusinessConfig>();
            HEERPServiceClient = new RestClient(config.HealthElementWCFAddress);
            ServiceKey = config.HealthElementWCFServiceKey;
        }


        public string SyncIncrementalStock(string jsonData)
        {
            var tags = new Dictionary<string, string>();
            tags["type"] = "syncincrementalstock";
            tags["trackid"] = Guid.NewGuid().ToString();

            var payload = new { Key = ServiceKey, Data = jsonData };

            CLSLogger.Info("SyncIncrementalStock请求报文", payload.ToJsonString(), tags);

            RestRequest request = new RestRequest("stock/SyncIncrementalStock");
            request.AddJsonBody(payload);
            var response = HEERPServiceClient.Post(request);

            if (response == null || response.ErrorException != null)
            {
                CLSLogger.Info("SyncIncrementalStock响应报文", "返回为空，发生异常", tags);
                return null;
            }

            CLSLogger.Info("SyncIncrementalStock响应报文", response.Content, tags);

            return response.Content;

        }

        //[{"OrderpackNoOrCarrieridOrOrderID":["123456","789012","ABC1","BD3"],"Status":"0","Operator":"John"}]
        //public string SetERPOrderPackStatus(string jsonData)
        //{
        //    var tags = new Dictionary<string, string>();
        //    tags["type"] = "SetERPOrderPackStatus";
        //    tags["trackid"] = Guid.NewGuid().ToString();

        //    var payload = new { Key = ServiceKey, Data = jsonData };

        //    CLSLogger.Info("SetERPOrderPackStatus请求报文", payload.ToJsonString(), tags);

        //    RestRequest request = new RestRequest("order/ConfirmOrderPackStatusFromEshop");
        //    request.AddJsonBody(payload);
        //    var response = HEERPServiceClient.Post(request);

        //    if (response == null || response.ErrorException != null)
        //    {
        //        CLSLogger.Info("ConfirmOrderPackStatusFromEshop", "返回为空，发生异常", tags);
        //        return null;
        //    }

        //    CLSLogger.Info("ConfirmOrderPackStatusFromEshop", response.Content, tags);

        //    return response.Content;

        //}


        //public string SetERPOrderPackCarrierid(string jsonData)
        //{
        //    var tags = new Dictionary<string, string>();
        //    tags["type"] = "SetERPOrderPackCarrierID";
        //    tags["trackid"] = Guid.NewGuid().ToString();

        //    var payload = new { Key = ServiceKey, Data = jsonData };

        //    CLSLogger.Info("SetERPOrderPackCarrierID请求报文", payload.ToJsonString(), tags);

        //    RestRequest request = new RestRequest("order/SetOrderPackCarreridFromEshop");
        //    request.AddJsonBody(payload);
        //    var response = HEERPServiceClient.Post(request);

        //    if (response == null || response.ErrorException != null)
        //    {
        //        CLSLogger.Info("SetOrderPackCarreridFromEshop", "返回为空，发生异常", tags);
        //        return null;
        //    }

        //    CLSLogger.Info("SetOrderPackCarreridFromEshop", response.Content, tags);

        //    return response.Content;

        //}


    }


}
