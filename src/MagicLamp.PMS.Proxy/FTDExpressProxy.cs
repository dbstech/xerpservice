﻿using System;
using System.Collections.Generic;
using System.Text;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.DTOs.Configs;
using RestSharp;
using Newtonsoft.Json.Linq;
using MagicLamp.PMS.Infrastructure.Extensions;
using System.Security.Cryptography;
using System.IO;
using System.Web;
using MagicLamp.PMS.DTOs.ThirdPart;
using Abp.Extensions;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace MagicLamp.PMS.Proxy
{
   public class FTDExpressProxy
    {
        public FTDExpressProxy()
        {

        }
        public static async Task<FTDAPIOutputDTO> UploadIDCard(FTDIDCardInfoDTO input)
        {
            if (input == null || string.IsNullOrWhiteSpace(input.id) || string.IsNullOrWhiteSpace(input.name) || input.card_front == null || input.card_back == null )
            {
                throw new ArgumentException("Invalid dto, missing parameters name/idcard/data");
            }
            
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "FTDloadIDCard";
            tags["IDCard"] = input.id;

            var config = Ultities.GetConfig<FTDConfig>();

            string url = config.uploadIDCardUrl; //没获取参数值
                   //url = "http://api.ftd.nz/idcard/upload";
                   url = "http://api.ftdlogistics.cn/idcard/upload";
            try
            {

                HttpClient client = new HttpClient
                {
                    Timeout = TimeSpan.FromSeconds(120) 
                };

                var postContent = new MultipartFormDataContent();

                string boundary = string.Format("--{0}", DateTime.Now.Ticks.ToString("x"));
                postContent.Headers.Add("ContentType", $"multipart/form-data, boundary={boundary}");
                postContent.Add(new StringContent(input.name), "name");
                postContent.Add(new StringContent(input.id), "id");
                postContent.Add(new ByteArrayContent(input.card_front), "card_front", System.IO.Path.GetFileName(input.card_frontname));
                postContent.Add(new ByteArrayContent(input.card_back), "card_back", System.IO.Path.GetFileName(input.card_backtname));

                HttpResponseMessage response = await client.PostAsync(url, postContent);
                string str = await response.Content.ReadAsStringAsync();

                if (response == null || string.IsNullOrWhiteSpace(str))
                {
                    CLSLogger.Error("富腾达上传身份证api返回为null", string.Empty, tags);
                    return new FTDAPIOutputDTO
                    {
                        success =false,
                        data = "富腾达上传身份证api返回为null"
                    };
                }
                else
                {
                    CLSLogger.Info("富腾达上传身份证响应报文", $"name:{input.name} id:{input.id} " + Regex.Unescape(str), tags);

                    return new FTDAPIOutputDTO
                    {
                        success = true,
                        data = Regex.Unescape(str)
                    };

                }
            }
            catch (TaskCanceledException ex)
            {
                CLSLogger.Error("请求超时或任务被取消", ex, tags);
                return new FTDAPIOutputDTO
                {
                    success = false,
                    data = "请求超时或任务被取消：" + ex.Message
                };
            }
            catch (Exception ex)
            {
                CLSLogger.Error("富腾达身份证上传发生异常", ex, tags);
                return new FTDAPIOutputDTO
                {
                    success = false,
                    data = "富腾达身份证上传发生异常：" + ex.Message
                };
            }
        }


    }
}
