﻿using MagicLamp.PMS.DTOs;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MagicLamp.PMS.Infrastructure.Extensions;



namespace MagicLamp.PMS.Proxy
{
   public class FENGTIANExpressProxy
    {
        readonly IRestClient _restClient;
        string url = "https://www.fengtianexpress.com/api/icard_submitApi ";
        public FENGTIANExpressProxy()
        {
            _restClient = new RestClient(url);
        }

        public  string UploadIDCard (FENGTIANIDCardInfoDTO input)
        {
            JObject content = new JObject();

            if (input == null || string.IsNullOrWhiteSpace(input.name) || string.IsNullOrWhiteSpace(input.inum) || input.phone == null || input.f_img == null || input.b_img == null)
            {
                throw new ArgumentException("Invalid dto, missing parameters name/idcard/data");
            }

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "FENGTIANloadIDCard";
            tags["IDCard"] = input.phone;

            try
            {
                IRestRequest restRequest = new RestRequest()
                {
                    Method = Method.POST
                };

                JObject jo = JObject.FromObject(input);

                //CLSLogger.Info("FENGTIANloadIDCardRequest", jo.ToJsonString(), tags);

                restRequest.AddParameter("application/json; charset=utf-8", jo.ToJsonString(), ParameterType.RequestBody);
                IRestResponse response = _restClient.Execute(restRequest);

                try
                {
                    if (response != null)
                    {
                        content = JObject.Parse(response.Content);
                    }
                    else
                    {
                        CLSLogger.Info("message:", "HttpStatusCode:" + response.StatusCode +
                                       "ResponseStatus: " + response.ResponseStatus + "ErrorMessage: " + response.ErrorMessage +
                                       "ErrorException: " + response.ErrorException + "IsSuccessful: " + response.IsSuccessful +
                                       "restRequest: " + restRequest.ToJsonString(), tags);
                    }
                }
                catch (Exception ex)
                {
                    CLSLogger.Info("response.content", response.Content.ToJsonString(), tags);
                    throw ex;
                }
              
            }
            catch (Exception ex)
            {
                CLSLogger.Error("奉天身份证上传发生异常", ex, tags);
            }

            return content.ToJsonString();

        }
    }
}
