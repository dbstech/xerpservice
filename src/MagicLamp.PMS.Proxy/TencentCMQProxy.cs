﻿
using Cmq_SDK;
using Cmq_SDK.Cmq;
using Cmq_SDK.Exception;
using MagicLamp.PMS.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.PMS.Proxy
{
    public class TencentCMQProxy
    {

        public static void PublishTopicMessage(string topicName, object msgbody, List<string> msgTags = null, Dictionary<string, string> cLogTags = null)
        {
            TencentCMQProxy.PublishTopicMessage(topicName, msgbody.ToJsonString(), msgTags, cLogTags);
        }

        /// <summary>
        /// 向腾讯CMQ推送消息
        /// </summary>
        /// <param name="msgbody"></param>
        public static void PublishTopicMessage(string topicName, string msgbody, List<string> msgTags = null, Dictionary<string, string> cLogTags = null)
        {
            if (cLogTags == null)
            {
                cLogTags = new Dictionary<string, string>();
            }

            if (!cLogTags.ContainsKey("type"))
            {
                cLogTags["type"] = "pushcmqtopicmessage";
            }

            if (!cLogTags.ContainsKey("trackid"))
            {
                cLogTags["trackid"] = Guid.NewGuid().ToString();
            }

            try
            {
                string secretId = TCConstants.SECRETID;
                string secretKey = TCConstants.SECRETKEY;
                string endpoint = TCConstants.EXTERNAL_CMQ_HK_ENDPOINT;

                CmqAccount account = new CmqAccount(endpoint, secretId, secretKey);
                Topic topic = account.getTopic(topicName);
                msgTags = msgTags == null ? new List<string>() : msgTags;

                CLSLogger.Info("推送CMQ请求报文", $"TopicName：{topicName}；MsgTags：{string.Join(",", msgTags)}；MsgBody：{msgbody}", cLogTags);

                var response = topic.publishMessage(msgbody, msgTags, "");

                CLSLogger.Info("推送CMQ响应报文", $"TopicName：{topicName}；Response：{response}", cLogTags);
            }
            catch (ServerException ex)
            {
                CLSLogger.Error("推送CMQ消息发生异常", ex.ToString(), cLogTags);
            }
            catch (ClientException ex)
            {
                CLSLogger.Error("推送CMQ消息发生异常", ex.ToString(), cLogTags);
            }
            catch (Exception ex)
            {
                CLSLogger.Error("推送CMQ消息发生异常", ex.ToString(), cLogTags);
            }
        }

        /// <summary>
        /// 向腾讯CMQ推送消息
        /// </summary>
        /// <param name="msgbody"></param>
        public static void PublishQueueMessage(string queueName, string msgbody)
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "cmqException";
            tags["messageId"] = Guid.NewGuid().ToString();
            try
            {
                string secretId = TCConstants.SECRETID;
                string secretKey = TCConstants.SECRETKEY;
                string endpoint = TCConstants.EXTERNAL_CMQ_HK_ENDPOINT;

                CmqAccount account = new CmqAccount(endpoint, secretId, secretKey);

                Queue queue = account.getQueue(queueName);
                queue.sendMessage(msgbody);
            }
            catch (ServerException ex)
            {
                CLSLogger.Error("推送CMQ消息发生异常", ex.ToString(), tags);
                CLSLogger.Error("推送CMQ消息发生异常--消息体内容", msgbody, tags);
            }
            catch (ClientException ex)
            {
                CLSLogger.Error("推送CMQ消息发生异常", ex.ToString(), tags);
                CLSLogger.Error("推送CMQ消息发生异常--消息体内容", msgbody, tags);
            }
            catch (Exception ex)
            {
                CLSLogger.Error("推送CMQ消息发生异常", ex.ToString(), tags);
                CLSLogger.Error("推送CMQ消息发生异常--消息体内容", msgbody, tags);
            }
        }

        public static List<Message> BatchReceiveAndDeleteMessages(string queueName, int numOfMsg = 3, int pollingWaitSeconds = 0)
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "cmqException";
            tags["messageId"] = Guid.NewGuid().ToString();
            List<Message> messages = new List<Message> { };
            try
            {
                string secretId = TCConstants.SECRETID;
                string secretKey = TCConstants.SECRETKEY;
                string endpoint = TCConstants.EXTERNAL_CMQ_HK_ENDPOINT;

                CmqAccount account = new CmqAccount(endpoint, secretId, secretKey);

                Queue queue = account.getQueue(queueName);
                messages = queue.batchReceiveMessage(numOfMsg, pollingWaitSeconds);
                queue.batchDeleteMessage(messages.Select(x => x.receiptHandle).ToList());
            }
            catch (ServerException ex)
            {
                CLSLogger.Error("接收CMQ消息发生异常", ex.ToString(), tags);
            }
            catch (ClientException ex)
            {
                CLSLogger.Error("接收CMQ消息发生异常", ex.ToString(), tags);
            }
            catch (Exception ex)
            {
                CLSLogger.Error("接收CMQ消息发生异常", ex.ToString(), tags);
            }
            // Log exchange rate
            return messages;
        }
    }
}
