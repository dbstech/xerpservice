﻿using MagicLamp.PMS.Proxy.ECinx.Models;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MagicLamp.PMS.Infrastructure.Extensions;
using System.Net;

namespace MagicLamp.PMS.Proxy.ECinx
{
    public class EcinxClientV2
    {
        private string Md5(string message)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] utf8Bytes = Encoding.UTF8.GetBytes(message);
            byte[] hashedBytes = md5.ComputeHash(utf8Bytes);
            string hashedString = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            return hashedString;
        }
        private string CreateSign(string Secret, object data)
        {
            string signString = (Secret + data.ToJsonString() + Secret);
            return Md5(signString);
        }

        public async Task<EcinxResponse> CreateOrderV2(int exportFlag, EcinxOrder orderInput)
        {

            EcinxConfigerData config = EcinxConfigerData.Configuration;
            ExportResponse _exportresponse = config.GetExportByFlag(exportFlag);

            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "CreateEcinxOrderInput",
                ["orderid"] = orderInput.platformOrderCode
            };

            string method = "order.add";

            orderInput.appkey = _exportresponse.Appkey;
            orderInput.method = method;
            orderInput.shopCode = _exportresponse.Export.ShopCode;  // 店铺编码
            orderInput.warehouseCode = _exportresponse.Export.WarehouseCode; // 重庆保税仓-迈集客BS
            orderInput.notifyUrl = _exportresponse.NotifyUrl; // 异步通知

            var response = await ExecuteAsync(_exportresponse.Url, _exportresponse.Secret, method, orderInput);

            CLSLogger.Info("Ecinx下单response", response.ToJsonString(), logTags);

            var result = response.ToObject<EcinxResponse>();

            return result;
        }

        public async Task<EcinxResponse> ordercancelV2(int exportFlag, OrderDeleteInput orderInput)
        {

            EcinxConfigerData config = EcinxConfigerData.Configuration;
            ExportResponse _exportresponse = config.GetExportByFlag(exportFlag);

            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "ordercancel",
                ["orderid"] = orderInput.orderCode
            };

            string method = "order.cancel.v2";

            orderInput.appkey = _exportresponse.Appkey;
            orderInput.method = method;

            CLSLogger.Info("ordercancel_input", orderInput.ToJsonString(), logTags);

            var response = await ExecuteAsync(_exportresponse.Url, _exportresponse.Secret, method, orderInput);

            CLSLogger.Info("ordercancel_response", response.ToJsonString(), logTags);

            var result = response.ToObject<EcinxResponse>();

            return result;
        }


        public async Task<EcinxQueryOrderResult> QueryOrderV2(int exportFlag, EcinxOrderQueryInput orderInput)
        {

            EcinxConfigerData config = EcinxConfigerData.Configuration;
            ExportResponse _exportresponse = config.GetExportByFlag(exportFlag);

            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "QueryEcinxOrderInput",
                ["orderid"] = orderInput.declareOrderCode
            };

            string method = "order.query";

            orderInput.appkey = _exportresponse.Appkey;
            orderInput.method = method;

            CLSLogger.Info("Ecinx查询input", orderInput.ToJsonString(), logTags);

            var response = await ExecuteAsync(_exportresponse.Url, _exportresponse.Secret, method, orderInput);

            CLSLogger.Info("Ecinx查询response", response.ToJsonString(), logTags);

            EcinxQueryOrderResult result = JsonConvert.DeserializeObject<EcinxQueryOrderResult>(response.ToJsonString());

            if (!result.success)
            {
                throw new Exception($"Ecinx查询接口失败，订单号：{orderInput.declareOrderCode.ToJsonString()}");
            }

            return result;
        }


        public async Task<EcinxDeliveryResponse> QueryDeliveryV2(int exportFlag, EcinxDeliveryInput orderInput)
        {

            EcinxConfigerData config = EcinxConfigerData.Configuration;
            ExportResponse _exportresponse = config.GetExportByFlag(exportFlag);

            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "EcinxDeliveryInput",
                ["orderid"] = orderInput.declareOrderCode
            };

            string method = "delivery.query";

            orderInput.appkey = _exportresponse.Appkey;
            orderInput.method = method;
            orderInput.shopCode = _exportresponse.Export.ShopCode; // 店铺编码
            orderInput.warehouseCode = _exportresponse.Export.WarehouseCode;  // 重庆保税仓-迈集客BS

            CLSLogger.Info("Ecinx发货单查询input", orderInput.ToJsonString(), logTags);

            var response = await ExecuteAsync(_exportresponse.Url, _exportresponse.Secret, method, orderInput);

            var result = response.ToObject<EcinxDeliveryResponse>();

            //CLSLogger.Info("Ecinx发货单查询result", "返回值："+ response + " 序列化结果:" + result.ToJsonString(), logTags);

            if (!result.success)
            {
                throw new Exception($"Ecinx发货单查询接口失败，订单号：{orderInput.declareOrderCode.ToJsonString()}");
            }

            return result;
        }


        private async Task<JObject> ExecuteAsync(string url, string Secret, string code, object data,
                                         Method httpMethod = Method.POST,
                                         Dictionary<string, string> clogTags = null)
        {
            IRestClient _restClient = new RestClient(url);
            if (clogTags == null)
            {
                clogTags = new Dictionary<string, string>();
            }

            IRestRequest restRequest = new RestRequest()
            {
                Method = httpMethod
            };

            string sign = CreateSign(Secret, data);
            JObject jo = JObject.FromObject(data);
            jo.Add("sign", sign);

            restRequest.AddParameter("application/json; charset=utf-8", jo.ToJsonString(), ParameterType.RequestBody);

            CLSLogger.Info("Ecinx_request", jo.ToJsonString(), clogTags);

            JObject content = new JObject();

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;

            IRestResponse response = await _restClient.ExecuteAsync(restRequest);

            try
            {
                string responseContent = response.Content ?? "";
                int statusCode = (int)response.StatusCode;
                string statusDescription = response.StatusDescription ?? "No Status Description";

                CLSLogger.Info("Ecinx_response_value", $"Response Code: {statusCode}, Description: {statusDescription}, Data: {responseContent}," +
                    $" response.StatusCode:{response.StatusCode},ErrorMessage:{response.ErrorMessage} ErrorException:{response.ErrorException}", clogTags);

                if (!string.IsNullOrEmpty(responseContent))
                {
                    content = JObject.Parse(responseContent);
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("Ecinx_response_Error_EcinxClinetV2",
                                $"RequestData: {jo.ToJsonString()}, Response Code: {(int)response.StatusCode}, " +
                                $"Description: {response.StatusDescription}, ResponseData: {response.Content}, Exception: {ex.Message}",
                                clogTags);
            }

            CLSLogger.Info("Ecinx_response", content.ToJsonString(), clogTags);

            return content;
        }

    }


}
