﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.PMS.Proxy.ECinx
{
   public class EcinxConfig
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string NotifyUrl { get; set; }
        public string Appkey { get; set; }
        public string Secret { get; set; }
        public string ShopCode { get; set; }
        public string WarehouseCode { get; set; }
    }

    //Canvas 1.0
    public class Export
    {
        public int ExportFlag { get; set; }
        public string Name { get; set; }
        public string ShopCode { get; set; }
        public string WarehouseCode { get; set; }
        public string WarehouseName { get; set; }
        public string providerCode { get; set; } //供应商代码
    }

    public class ExportResponse
    {
        public string Url { get; set; }
        public string NotifyUrl { get; set; }
        public string Appkey { get; set; }
        public string Secret { get; set; }
        public Export Export { get; set; }
    }

    public class ChannelConfig
    {
        public string Url { get; set; }
        public string NotifyUrl { get; set; }
        public string Appkey { get; set; }
        public string Secret { get; set; }
        public Dictionary<int, Export> ExportMapping { get; set; }

        public Export GetExportByFlag(int exportFlag)
        {
            Export export;
            if (ExportMapping.TryGetValue(exportFlag, out export))
            {
                return export;
            }
            return null;
        }
    }

    public class EcinxConfigerData
    {
        public ChannelConfig MFD { get; set; }
        public ChannelConfig WHYC { get; set; }
        public int[] supplySalesExportFlag { get; set; } = new int[ 114 ];
        public int[] PushWHYCMFDWarehouseID { get; set; } = new[] { 48, 49, 50, 51 };
        // Static initialization
        public static EcinxConfigerData Configuration { get; } = new EcinxConfigerData
        {
            MFD = new ChannelConfig
            {
                Url = "https://gateway.ecinx.com/api/v1",
                NotifyUrl = "https://xerp-service.aladdin.nz/api/services/app/Ecinx/SyncCreateOrderStatus",
                Appkey = "ZDZW7N5G",
                Secret = "7c0b98031c474573bc09cc4d61595505",
                ExportMapping = new Dictionary<int, Export>
                {
                    { 83, new Export { ExportFlag = 83, Name = "CQZGC", ShopCode = "KJED", WarehouseCode = "CQZGC", WarehouseName="重庆保税仓-迈集客ZG",providerCode="" } },
                    { 93, new Export { ExportFlag = 93, Name = "YOBS", ShopCode = "mofadeng", WarehouseCode = "YOCHYDF",WarehouseName="渝欧-代发仓",providerCode=""  } },
                    { 66, new Export { ExportFlag = 66, Name = "CQBSC", ShopCode = "KJED", WarehouseCode = "CQBSC",WarehouseName="重庆保税仓-迈集客BS",providerCode="" } },
                    { 85, new Export { ExportFlag = 85, Name = "YWC", ShopCode = "KJED", WarehouseCode = "YWOYBSCNFMX",WarehouseName="义乌欧一保税仓-奶粉慢线",providerCode=""  } },
                    { 79, new Export { ExportFlag = 79, Name = "YWCBS", ShopCode = "KJED", WarehouseCode = "YWOYBSCYWBS",WarehouseName="义乌欧一保税仓-义乌保税",providerCode="" } },// warehousecode = "YWOYBSCYWBS"
                    { 84, new Export { ExportFlag = 84, Name = "YWCNFKX", ShopCode = "KJED", WarehouseCode = "YWOYBSCNFKX",WarehouseName="义乌欧一保税仓-奶粉快线",providerCode=""  } },
                    { 88, new Export { ExportFlag = 88, Name = "YWCJPZX", ShopCode = "KJED", WarehouseCode = "YWOYBSCBJPZX",WarehouseName="义乌欧一保税仓-保健品专线",providerCode=""  } },
                    { 95, new Export { ExportFlag = 95, Name = "NSFYWC", ShopCode = "KJED", WarehouseCode = "NSFYWZYDFC",WarehouseName="NSF义乌直邮代发仓",providerCode="" } },
                    { 94, new Export { ExportFlag = 94, Name = "QDXHZYC", ShopCode = "KJED", WarehouseCode = "QDXHZYC",WarehouseName="青岛现货直邮仓",providerCode="" } },
                    { 96, new Export { ExportFlag = 96, Name = "GXCY", ShopCode = "KJED", WarehouseCode = "NBZ005",WarehouseName="共享菜园",providerCode="" } },
                    { 97, new Export { ExportFlag = 97, Name = "YWDHC", ShopCode = "KJED", WarehouseCode = "YWOYDMC",WarehouseName="义乌大货仓",providerCode="" } },
                    { 98, new Export { ExportFlag = 98, Name = "YWGXCY", ShopCode = "KJED", WarehouseCode = "ZX-YWZ007",WarehouseName="义乌共享菜园",providerCode="" } },
                    { 99, new Export { ExportFlag = 99, Name = "QDXHC", ShopCode = "KJED", WarehouseCode = "QDXHCNC",WarehouseName="青岛现货仓",providerCode="" } },
                    { 100, new Export { ExportFlag = 100, Name = "YWTJC", ShopCode = "KJED", WarehouseCode = "YWOYBSCBLP",WarehouseName="义乌特价仓",providerCode="" } },
                    { 101, new Export { ExportFlag = 101, Name = "YWNM", ShopCode = "KJED", WarehouseCode = "YWOYBSCBSNM",WarehouseName="NM义乌保税",providerCode="" } },
                    { 81, new Export { ExportFlag = 81, Name = "CQNFJSC", ShopCode = "KJED", WarehouseCode = "CQZGCFTF2",WarehouseName="重庆保税仓-奶粉快线",providerCode="" } },
                    { 82, new Export { ExportFlag = 82, Name = "CQNFPTX", ShopCode = "KJED", WarehouseCode = "CQZGCSTF",WarehouseName="重庆保税仓-奶粉慢线",providerCode="" } },
                    { 102, new Export { ExportFlag = 102, Name = "YBMYC", ShopCode = "EDMDF", WarehouseCode = "OTHER",WarehouseName="一般贸易仓",providerCode=""  } },
                    { 103, new Export { ExportFlag = 103, Name = "HKCENTER", ShopCode = "KJED", WarehouseCode = "HKZ005" ,WarehouseName="菜园-香港自营中心仓",providerCode=""} },
                    { 108, new Export { ExportFlag = 108, Name = "FZPANDA", ShopCode = "KJED", WarehouseCode = "FZZ005",WarehouseName="菜园-福州龙猫中心仓",providerCode="" } },
                    { 109, new Export { ExportFlag = 109, Name = "GZZYC", ShopCode = "KJED", WarehouseCode = "GZZ003",WarehouseName="共享菜园(广州中远)",providerCode="" } },
                    { 110, new Export { ExportFlag = 110, Name = "YWZBKC", ShopCode = "KJED", WarehouseCode = "YWZ806",WarehouseName="菜园-义乌综保凯昌中心仓",providerCode="" } },
                    { 111, new Export { ExportFlag = 111, Name = "CQSLZX", ShopCode = "KJED", WarehouseCode = "CQZXC001",WarehouseName="菜园-重庆丝路中心仓",providerCode="" } },
                    { 112, new Export { ExportFlag = 112, Name = "YWJXZXC", ShopCode = "KJED", WarehouseCode = "YWZ804",WarehouseName="菜园-义乌金迅中心仓",providerCode="" } },
                    { 113, new Export { ExportFlag = 113, Name = "QDXHAZXC", ShopCode = "KJED", WarehouseCode = "QDZ801",WarehouseName="菜园-青岛西海岸B保中心仓",providerCode="" } },
                    { 114, new Export { ExportFlag = 114, Name = "CQSYGX", ShopCode = "EDMGX", WarehouseCode = "",WarehouseName="跨境E店(供销)",providerCode="CQSY" } },//分销
                    { 115, new Export { ExportFlag = 115, Name = "CWYIHSZXC", ShopCode = "KJED", WarehouseCode = "YWZ805",WarehouseName="菜园-义乌海石中心仓",providerCode="" } },
                    { 116, new Export { ExportFlag = 116, Name = "CWYIDTZXC", ShopCode = "KJED", WarehouseCode = "YWZ803",WarehouseName="菜园-义乌代塔中心仓",providerCode="" } },
                }
            },
            WHYC = new ChannelConfig
            {
                Url = "https://gateway.ecinx.com/api/v1",
                NotifyUrl = "https://xerp-service.aladdin.nz/api/services/app/Ecinx/SyncCreateOrderStatus",
                Appkey = "34CWJ195",
                Secret = "1b78435d3d1c4017a13e25561ee46a37",
                ExportMapping = new Dictionary<int, Export>
                {
                    { 104, new Export { ExportFlag = 104, Name = "YIWUYINGCHI", ShopCode = "MFDE", WarehouseCode = "YCYWBS" ,WarehouseName="盈驰-义乌欧一保税",providerCode="" } },
                    { 105, new Export { ExportFlag = 105, Name = "YCYWBJZX", ShopCode = "MFDE", WarehouseCode = "YCYWBJZX",WarehouseName="盈驰-义乌欧一保健品专线",providerCode=""  } },
                    { 106, new Export { ExportFlag = 106, Name = "YCYWMX", ShopCode = "MFDE", WarehouseCode = "YCYWMX",WarehouseName="盈驰-义乌欧一奶粉慢线",providerCode=""  } },
                    { 107, new Export { ExportFlag = 107, Name = "YCYWKX", ShopCode = "MFDE", WarehouseCode = "YCYWKX",WarehouseName="盈驰-义乌欧一奶粉快线",providerCode=""  } }
                }
            }
        };

        public ExportResponse GetExportByFlag(int exportFlag)
        {
            Export export = MFD.GetExportByFlag(exportFlag) ?? WHYC.GetExportByFlag(exportFlag);
            if (export != null)
            {
                ChannelConfig channelConfig = MFD.ExportMapping.ContainsKey(exportFlag) ? MFD : WHYC;
                return new ExportResponse
                {
                    Url = channelConfig.Url,
                    NotifyUrl = channelConfig.NotifyUrl,
                    Appkey = channelConfig.Appkey,
                    Secret = channelConfig.Secret,
                    Export = export
                };
            }
            return null;
        }
    }
}
