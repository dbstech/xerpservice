﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy.ECinx.Models
{
    public class EcinxInventoryQueryInput
    {
        public string appkey { set; get; }
        public string method { set; get; }
        public string goodsCode { set; get; }
        //public string barCode { set; get; }
        //public string brandCode { set; get; }
        //public string catalogCode { set; get; }
        //public string goodsCode { set; get; }
        //public string goodsName { set; get; }
        public string warehouseCode { set; get; }
        //public int page { set; get; }
        //public int rows { set; get; }
    }

    public class AllInventoryDto
    {
        public string appkey { set; get; }
        public string method { set; get; }
        public string warehouseCode { set; get; }
        public int page { set; get; }
        public int rows { set; get; }

    }
}
