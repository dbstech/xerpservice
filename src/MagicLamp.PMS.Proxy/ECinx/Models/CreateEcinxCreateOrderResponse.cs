﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy.ECinx.Models
{
   public class EcinxResponse
    {
        public bool success { get; set; }
        public object errorCode { get; set; }
        public string errorMsg { get; set; }
        public object result { get; set; }
    }
}
