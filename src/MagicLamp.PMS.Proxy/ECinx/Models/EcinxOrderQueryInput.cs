﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy.ECinx.Models
{
    public class EcinxOrderQueryInput
    {
        public string appkey { get; set; }
        public string method { get; set; }
        public string declareOrderCode { get; set; }
        //public int allocationStatus { get; set; }
        //public int audited { get; set; }
        //public string type { get; set; }
        //public string distributorsCode { get; set; }
        //public string providerCode { get; set; }
        //public int deliveryStatus { get; set; }
        //public DateTime? taxendTime { get; set; }
        //public string expressCode { get; set; }
        public bool includeCancel { get; set; }
        //public int isNormal { get; set; }
        //public string orderCode { get; set; }
        //public string orderFlagCode { get; set; }
        //public int page { get; set; }
        //public string platformOrderCode { get; set; }
        //public string receiverTel { get; set; }
        //public int rows { get; set; }
        //public string shopCode { get; set; }
        //public DateTime? startTime { get; set; }
        //public string warehouseCode { get; set; }


    }
}
