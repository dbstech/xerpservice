﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy.ECinx.Models
{
    public class EcinxQueryOrderResult
    {
        public bool success { get; set; }
        public object errorCode { get; set; }
        public object errorMsg { get; set; }
        public Result result { get; set; }
    }

    public class Result
    {
        public int currentPage { get; set; }
        public int pageSize { get; set; }
        public List<Row> rows { get; set; }
        public int total { get; set; }
    }

    public class Row
    {
        public string orderCode { get; set; }
        public string platformOrderCode { get; set; }
        public string declareOrderCode { get; set; }
        public int type { get; set; }
        public object distributorsCode { get; set; }
        public object distributorsName { get; set; }
        public object providerCode { get; set; }
        public object providerName { get; set; }
        public string shopCode { get; set; }
        public string shopName { get; set; }
        public string expressCode { get; set; }
        public string expressName { get; set; }
        public string warehouseCode { get; set; }
        public string warehouseName { get; set; }
        public object orderFlagCode { get; set; }
        public object orderFlagName { get; set; }
        public string customsPlatformCode { get; set; }
        public string customsPlatformName { get; set; }
        public double tax { get; set; }
        public double goodsAmount { get; set; }
        public double freight { get; set; }
        public double discount { get; set; }
        public double deduction { get; set; }
        public double payAmount { get; set; }
        public double totalAmount { get; set; }
        public string buyerRemark { get; set; }
        public string receiver { get; set; }
        public string receiverTel { get; set; }
        public string receiverProvince { get; set; }
        public string receiverCity { get; set; }
        public string receiverArea { get; set; }
        public string receiverAddress { get; set; }
        public int allocationStatus { get; set; }
        public int deliveryStatus { get; set; }
        public int audited { get; set; }
        public int cancel { get; set; }
        public int isNormal { get; set; }
        public string exception { get; set; }
        public string purchaser { get; set; }
        public string purchaserTel { get; set; }
        public int purchaserCardType { get; set; }
        public string purchaserCardNo { get; set; }
        public string orderTime { get; set; }
        public string createTime { get; set; }
        public List<QueryOrderDetail> details { get; set; }
        public List<OriginalDetail> originalDetails { get; set; }
        public List<QueryOrderPayment> payments { get; set; }
        public List<object> deliveries { get; set; }
    }

    public class QueryOrderDetail
    {
        public string orderDetailId { get; set; }
        public string goodsCode { get; set; }
        public string goodsName { get; set; }
        public int qty { get; set; }
        public double price { get; set; }
        public string platformGoodsCode { get; set; }
        public object platformGoodsName { get; set; }
        public string remark { get; set; }
    }

    public class OriginalDetail
    {
        public int? goodsType { get; set; }
        public string goodsCode { get; set; }
        public string goodsName { get; set; }
        public string platformGoodsCode { get; set; }
        public object platformGoodsName { get; set; }
        public int? qty { get; set; }
        public double? price { get; set; }
        public object remark { get; set; }
    }

    public class QueryOrderPayment
    {
        public string paymentCode { get; set; }
        public string paymentName { get; set; }
        public double amount { get; set; }
        public string payTime { get; set; }
        public string tradeNo { get; set; }
    }

}
