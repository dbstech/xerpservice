﻿using System;
using System.Collections.Generic;
using System.Text;


namespace MagicLamp.PMS.Proxy.ECinx.Models
{
    public class EcinxOrderInput
    {

        public string appkey { get; set; }
        public string method { get; set; }

        public string shopCode { get; set; }
        public string warehouseCode { get; set; }
        public string notifyUrl { get; set; }


        public double deduction { get; set; }
        public double discount { get; set; }
        public double freight { get; set; }
        public string orderTime { get; set; }
        public string platformOrderCode { get; set; }
        public string declareOrderCode { get; set; }

        public string receiver { get; set; }
        public string receiverTel { get; set; }
        public string receiverProvince { get; set; }
        public string receiverCity { get; set; }
        public string receiverArea { get; set; }
        public string receiverAddress { get; set; }
        public double tax { get; set; }
        public int type { get; set; }

        public List<EcinxDetail> details { get; set; }

        public string customsPlatformCode { get; set; }
        public string customsPlatformName { get; set; }

        public string purchaser { get; set; }
        public string purchaserCardNo { get; set; }

        public int purchaserCardType { get; set; } = 1; //default value;

        public string purchaserTel { get; set; }
      

        //public string expressCode { get; set; }
        //public string buyerRemark { get; set; }

        /// <summary>
        /// 支付参数
        /// </summary>
        public object customsPaymentData { get; set; }

        public List<EcinxPayment> payments { get; set; }

        public expressSheetInfo expressSheetInfo { get; set; }
        public int presell { get; set; }
        public string planDeliveryTime { set; get; }

        public string purchaserCardFrontPic { set; get; }
        public string purchaserCardBackPic { set; get; }


    }
    public class EcinxDetail
    {
        public string goodsCode { get; set; }
        public int qty { get; set; }
        public double price { get; set; }
        public string remark { get; set; }
    }
    public class EcinxPayment
    {
        public string paymentCode { get; set; }
        public double amount { get; set; }
        public string payTime { get; set; }
        public string tradeNo { get; set; }
    }
    public class expressSheetInfo
    {
        public string logisticsCode { set; get; }
        public string expressCode { set; get; }

    }




    public class EcinxOrder
    {

        public string appkey { get; set; }
        public string method { get; set; }

        public string shopCode { get; set; }
        public string warehouseCode { get; set; }

        public double deduction { get; set; }
        public double discount { get; set; }
        public double freight { get; set; }
        public string orderTime { get; set; }
        public string platformOrderCode { get; set; }
        public string declareOrderCode { get; set; }

        public string receiver { get; set; }
        public string receiverTel { get; set; }
        public string receiverProvince { get; set; }
        public string receiverCity { get; set; }
        public string receiverArea { get; set; }
        public string receiverAddress { get; set; }
        public double tax { get; set; }
        public int type { get; set; }
        public int? ExportFlag { get; set; }


        public List<EcinxDetail> details { get; set; }

        public string customsPlatformCode { get; set; }
        public string customsPlatformName { get; set; }

        public string purchaser { get; set; }
        public string purchaserCardNo { get; set; }

        public int purchaserCardType { get; set; } = 1; //default value;

        public string purchaserTel { get; set; }
        public string notifyUrl { get; set; }
        public string providerCode { get; set; }

        //public string expressCode { get; set; }
        //public string buyerRemark { get; set; }

        public List<EcinxPayment> payments { get; set; }

        public expressSheetInfo expressSheetInfo { get; set; }
        public int presell { get; set; }
        public string planDeliveryTime { set; get; }

        public string purchaserCardFrontPic { set; get; }
        public string purchaserCardBackPic { set; get; }


    }

}
