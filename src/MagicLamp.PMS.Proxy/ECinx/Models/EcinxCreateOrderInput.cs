﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;



namespace MagicLamp.PMS.Proxy.ECinx.Models
{

    public class EcinxCreateOrderInput
    {
        [NotMapped]
        public string appkey { get; set; }
        [NotMapped]
        public string method { get; set; }
        [NotMapped]
        public string sign { get; set; }

        public string platformOrderCode { get; set; }
        public string declareOrderCode { get; set; }
        public string shopCode { get; set; }
        public string expressCode { get; set; }
        public string warehouseCode { get; set; }
        //public string customsPlatformCode { get; set; }
        //public string customsPlatformName { get; set; }
        public double tax { get; set; }
        public double freight { get; set; }
        public double discount { get; set; }
        public double deduction { get; set; }
        public string buyerRemark { get; set; }
        public string receiver { get; set; }
        public string receiverTel { get; set; }
        public string receiverProvince { get; set; }
        public string receiverCity { get; set; }
        public string receiverArea { get; set; }
        public string receiverAddress { get; set; }
        //public string purchaser { get; set; }
        //public string purchaserTel { get; set; }
        //public int purchaserCardType { get; set; }
        //public string purchaserCardNo { get; set; }
        public string orderTime { get; set; }
        public List<Detail> details { get; set; }
        //public List<Payment> payments { get; set; }

     
    }
    public class Detail
    {
        public string goodsCode { get; set; }
        public int qty { get; set; }
        public double price { get; set; }
        public string remark { get; set; }
    }
    //public class Payment
    //{
    //    public string paymentCode { get; set; }
    //    public double amount { get; set; }
    //    public string payTime { get; set; }
    //    public string tradeNo { get; set; }
    //}

}
