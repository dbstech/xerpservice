﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy.ECinx.Models
{
   public class EcinxDeliverysendOutInput
    {
        public string appkey { get; set; }
        public string method { get; set; }
        public string deliveryCode { get; set; }
        public string expressCode { get; set; }
        public string logisticCode { get; set; }
    }
}
