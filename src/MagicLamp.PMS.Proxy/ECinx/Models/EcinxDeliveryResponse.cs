﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy.ECinx.Models
{
    public class EcinxDeliveryResponse
    {
        public bool success { get; set; }
        public object errorCode { get; set; }
        public object errorMsg { get; set; }
        public DeliveryResult result { get; set; }
    }
    public class DeliveryResult
    {
        public int currentPage { get; set; }
        public int pageSize { get; set; }
        public List<DeliveryRow> rows { get; set; }
        public int total { get; set; }
    }

    
    public class DeclareElement
    {
        public double tax { get; set; }
        public double goodsAmount { get; set; }
        public double freight { get; set; }
        public double discount { get; set; }
        public double deduction { get; set; }
        public double payAmount { get; set; }
        public double totalAmount { get; set; }
        public string purchaser { get; set; }
        public string purchaserTel { get; set; }
        public int purchaserCardType { get; set; }
        public string purchaserCardNo { get; set; }
        public string paymentCode { get; set; }
        public string paymentName { get; set; }
        public string payTime { get; set; }
        public string tradeNo { get; set; }
        public string customsPlatformCode { get; set; }
        public string customsPlatformName { get; set; }
    }

    public class DeliveryBatch
    {
        public string batchNum { get; set; }
        public int qty { get; set; }
        public object produceDate { get; set; }
        public object expiryDate { get; set; }
    }

    public class DeliveryDetail
    {
        public int qty { get; set; }
        public double price { get; set; }
        public double subtotal { get; set; }
        public string goodsCode { get; set; }
        public string warehouseGoodsCode { get; set; }
        public string goodsName { get; set; }
        public List<DeliveryBatch> batches { get; set; }
    }

    public class DeliveryRow
    {
        public string deliveryCode { get; set; }
        public string shopCode { get; set; }
        public string shopName { get; set; }
        public string orderCode { get; set; }
        public string platformOrderCode { get; set; }
        public string declareOrderCode { get; set; }
        public string receiver { get; set; }
        public string receiverTel { get; set; }
        public string receiverProvince { get; set; }
        public string receiverCity { get; set; }
        public string receiverArea { get; set; }
        public string receiverAddress { get; set; }
        public string orderTime { get; set; }
        public object buyerRemark { get; set; }
        public string warehouseCode { get; set; }
        public string warehouseName { get; set; }
        public string expressCode { get; set; }
        public string expressName { get; set; }
        public string logisticCode { get; set; }
        public string createTime { get; set; }
        public int status { get; set; }
        public string deliveryTime { get; set; }
        public int delivered { get; set; }
        public int logisticPrinted { get; set; }
        public int platformDelivered { get; set; }
        public DeclareElement declareElement { get; set; }
        public List<DeliveryDetail> details { get; set; }
        public int sendExternalWarehouseStatus { get; set; }
        public int interceptStatus { get; set; }
        public object weight { get; set; }
    }
}
