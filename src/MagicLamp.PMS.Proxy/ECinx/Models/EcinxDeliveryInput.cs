﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy.ECinx.Models
{
    public class EcinxDeliveryInput
    {
        public string appkey { get; set; }
        public string method { get; set; }
        public string declareOrderCode { get; set; }
        public string warehouseCode { get; set; }
        public string shopCode { get; set; }

    }
}
