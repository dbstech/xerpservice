﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Dependency;
using Abp.Extensions;
using Abp.Json;
using Abp.Runtime.Caching;
using AutoMapper;
using MagicLamp.Flux.API.Models;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Common;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Proxy.ECinx.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;


namespace MagicLamp.PMS.Proxy.ECinx
{
    public class ECinxClient : ITransientDependency
    {
        readonly IRestClient _restClient;
        private readonly ICacheManager _cacheManager;
        private EcinxConfig _config;

        public enum EcinxAccountEnum
        {
            CQBSC = 3,  //重庆保税仓 
            CQNFPTX = 17, // CQ奶粉普通线 82
            YIWUYINGCHI = 20, // 盈驰仓_义乌 104
        }

        public ECinxClient(
            ICacheManager cacheManager,
            EcinxAccountEnum account
        )
        {
            var config = Ultities.GetConfig<List<EcinxConfig>>();
            _config = config.FirstOrDefault(x => x.Name == account.ToString());
            _restClient = new RestClient(_config.Url);
            _cacheManager = cacheManager;
        }


        private string Md5(string message)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] utf8Bytes = Encoding.UTF8.GetBytes(message);
            byte[] hashedBytes = md5.ComputeHash(utf8Bytes);
            string hashedString = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            return hashedString;
        }
        private string CreateSign(object data)
        {
            string signString = (_config.Secret+data.ToJsonString() + _config.Secret);
            return Md5(signString);
        }


        private async Task<JObject> ExecuteAsync(string code, object data, Method httpMethod = Method.POST, Dictionary<string, string> clogTags = null)
        {
            if (clogTags == null)
            {
                clogTags = new Dictionary<string, string>();
            }

            IRestRequest restRequest = new RestRequest()
            {
                Method = httpMethod
            };

            string sign = CreateSign(data);

            JObject jo = JObject.FromObject(data);
            jo.Add("sign", sign);

            restRequest.AddParameter("application/json; charset=utf-8", jo.ToJsonString(), ParameterType.RequestBody);

            CLSLogger.Info("Ecinx_request", jo.ToJsonString(), clogTags);

            JObject content = new JObject();
            IRestResponse response = await _restClient.ExecuteAsync(restRequest);

            try
            {
                if (response != null & response.Content.ToJsonString() != "")
                {
                    content = JObject.Parse(response.Content);
                }
                //else
                //{
                //    CLSLogger.Info("Ecinx_response.message:", "HttpStatusCode:" + response.StatusCode +
                //                   "ResponseStatus: " + response.ResponseStatus + "ErrorMessage: " + response.ErrorMessage +
                //                   "ErrorException: " + response.ErrorException + "IsSuccessful: " + response.IsSuccessful +
                //                   "restRequest: " + restRequest.ToJsonString(), clogTags);
                //    return content;
                //}
            }
            catch (Exception ex)
            {
                CLSLogger.Info("Ecinx_response_Error_EcinxClinetV1", "RequestData:"+ jo.ToJsonString() + " responseData: " + response.Content.ToJsonString() +"ex: " + ex.Message.ToJsonString(), clogTags);
                //throw ex;
            }

            CLSLogger.Info("Ecinx_response", content.ToJsonString(), clogTags);

            return content;


        }

        public async Task<EcinxResponse> CreateOrder(EcinxOrder orderInput)
        {
            
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "CreateEcinxOrderInput",
                ["orderid"] = orderInput.platformOrderCode
            };

            string method = "order.add";

            orderInput.appkey = _config.Appkey;
            orderInput.method = method;
            orderInput.shopCode = _config.ShopCode;  // 店铺编码
            orderInput.warehouseCode = orderInput.ExportFlag == 79 ? "": _config.WarehouseCode; // 重庆保税仓-迈集客BS
            orderInput.notifyUrl = _config.NotifyUrl; // 异步通知

            var response = await ExecuteAsync(method, orderInput);

            CLSLogger.Info("Ecinx下单response", response.ToJsonString(), logTags);

            var result = response.ToObject<EcinxResponse>();
           
            return result;
        }
        
        public async Task<EcinxQueryOrderResult> QueryOrder(EcinxOrderQueryInput orderInput)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "QueryEcinxOrderInput",
                ["orderid"] = orderInput.declareOrderCode
            };

            string method = "order.query";

            orderInput.appkey = _config.Appkey;
            orderInput.method = method;

            CLSLogger.Info("Ecinx查询input", orderInput.ToJsonString(), logTags);

            var response = await ExecuteAsync(method, orderInput);

            CLSLogger.Info("Ecinx查询response", response.ToJsonString(), logTags);

            EcinxQueryOrderResult result = JsonConvert.DeserializeObject<EcinxQueryOrderResult>(response.ToJsonString());

            if (!result.success)
            {
                throw new Exception($"Ecinx查询接口失败，订单号：{orderInput.declareOrderCode.ToJsonString()}");
            }

            return result;
        }

        //库存查询
        public async Task<EcinxInventoryQueryResponse> QueryInventory(EcinxInventoryQueryInput orderInput)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "inventoryqueryInput",
                ["goodscode"] = orderInput.goodsCode
            };
            EcinxInventoryQueryResponse result = null;
            try
            {
                string method = "inventory.query";
                orderInput.appkey = _config.Appkey;
                orderInput.method = method;

                var response = await ExecuteAsync(method, orderInput);

                if (result.ToJsonString() == "")
                { 
                    return JsonConvert.DeserializeObject<EcinxInventoryQueryResponse>(response.ToJsonString());
                }

                result = response.ToObject<EcinxInventoryQueryResponse>();
                if (!result.success)
                {
                    throw new Exception($"Ecinx查询接口失败，订单号：{orderInput.goodsCode.ToJsonString()}, 消息: {result.errorMsg.ToJsonString()}");
                }
                return result;
            }
            catch (Exception ex)
            {
                CLSLogger.Info("QueryInventory异常", $"输入值: {orderInput.ToJsonString() } ,返回结果：{result.ToJsonString()} 错误信息:{ex.ToJsonString()}", logTags);
                throw;
            }
           
        }

        //全量库存
        public async Task<EcinxInventoryQueryResponse> QueryWarehouseInventory(AllInventoryDto orderInput)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "Allinventoryquery",
                ["Data"] = "AllInventory"
            };

            EcinxInventoryQueryResponse result = null;

            try
            {

                string method = "inventory.query";
                orderInput.appkey = _config.Appkey;
                orderInput.method = method;
                var response = await ExecuteAsync(method, orderInput);
                result = response.ToObject<EcinxInventoryQueryResponse>();

                if (!result.success)
                {
                    throw new Exception($"Ecinx查询接口失败, 消息: {result.errorMsg.ToJsonString()}");
                }

                return result;
            }
            catch (Exception ex)
            {
                CLSLogger.Info("QueryInventory异常", $" 返回结果：{result.ToJsonString()} 错误信息:{ex.ToJsonString()}", logTags);
                throw;
            }
        }

        //查询生成的发货单
        public async Task<EcinxDeliveryResponse> QueryDelivery(EcinxDeliveryInput orderInput)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "EcinxDeliveryInput",
                ["orderid"] = orderInput.declareOrderCode
            };

            string method = "delivery.query";

            orderInput.appkey = _config.Appkey;
            orderInput.method = method;
            orderInput.shopCode = _config.ShopCode;  // 店铺编码
            orderInput.warehouseCode = _config.WarehouseCode; // 重庆保税仓-迈集客BS

            CLSLogger.Info("Ecinx发货单查询input", orderInput.ToJsonString(), logTags);

            var response = await ExecuteAsync(method, orderInput);

            var result = response.ToObject<EcinxDeliveryResponse>();

            //CLSLogger.Info("Ecinx发货单查询result", "返回值："+ response + " 序列化结果:" + result.ToJsonString(), logTags);


            if (!result.success)
            {
                throw new Exception($"Ecinx发货单查询接口失败，订单号：{orderInput.declareOrderCode.ToJsonString()}");
            }

            return result;
        }

        //发货单发货
        public async Task<EcinxResponse> DeliverySendOut(EcinxDeliverysendOutInput orderInput)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "EcinxDeliverySendOut",
                ["deliveryCode"] = orderInput.deliveryCode
            };

            string method = "delivery.sendOut";

            orderInput.appkey = _config.Appkey;
            orderInput.method = method;

            CLSLogger.Info("Ecinx发货单发货input", orderInput.ToJsonString(), logTags);

            var response = await ExecuteAsync(method, orderInput);

            CLSLogger.Info("Ecinx发货单发货iresponse", response.ToJsonString(), logTags);

            var result = response.ToObject<EcinxResponse>();

            if (!result.success)
            {
                throw new Exception($"Ecinx发货单查询i接口失败，订单号：{orderInput.ToJsonString()}");
            }

            return result;
        }

        public async Task<GoodsQueryResponse> GoodsQuery(GoodsQueryInput orderInput)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "GoodsQueryInput",
                ["orderid"] = orderInput.code
            };

            string method = "goods.query";

            orderInput.appkey = _config.Appkey;
            orderInput.method = method;

            CLSLogger.Info("GoodsQueryInput", orderInput.ToJsonString(), logTags);

            var response = await ExecuteAsync(method, orderInput);

            CLSLogger.Info("GoodsQueryresponse", response.ToJsonString(), logTags);

            var result = response.ToObject<GoodsQueryResponse>();
          
            return result;
        }


        public async Task<EcinxResponse> GoodsAdd(GoodsAddInput orderInput)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "goodsadd",
                ["orderid"] = orderInput.code
            };

            string method = "goods.add";

            orderInput.appkey = _config.Appkey;
            orderInput.method = method;

            CLSLogger.Info("goodsadd_input", orderInput.ToJsonString(), logTags);

            var response = await ExecuteAsync(method, orderInput);

            CLSLogger.Info("goodsadd_response", response.ToJsonString(), logTags);

            var result = response.ToObject<EcinxResponse>();

            return result;
        }

        public async Task<EcinxResponse> GoodsUpdate(GoodsAddInput orderInput)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "GoodsUpdate",
                ["orderid"] = orderInput.code
            };

            string method = "goods.update";

            orderInput.appkey = _config.Appkey;
            orderInput.method = method;

            CLSLogger.Info("GoodsUpdate_input", orderInput.ToJsonString(), logTags);

            var response = await ExecuteAsync(method, orderInput);

            CLSLogger.Info("GoodsUpdate_response", response.ToJsonString(), logTags);

            var result = response.ToObject<EcinxResponse>();

            return result;
        }

        /// <summary>
        /// orderdelete
        /// </summary>
        /// <param name="orderInput"></param>
        /// <returns></returns>
        public async Task<EcinxResponse> orderdelete(OrderDeleteInput orderInput)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "orderdelete",
                ["orderid"] = orderInput.orderCode
            };

            string method = "order.delete.v2";// "order.delete";

            orderInput.appkey = _config.Appkey;
            orderInput.method = method;

            CLSLogger.Info("orderdelete_input", orderInput.ToJsonString(), logTags);

            var response = await ExecuteAsync(method, orderInput);

            CLSLogger.Info("orderdelete_response", response.ToJsonString(), logTags);

            var result = response.ToObject<EcinxResponse>();

            return result;
        }

        public async Task<EcinxResponse> ordercancel(OrderDeleteInput orderInput)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["method"] = "ordercancel",
                ["orderid"] = orderInput.orderCode
            };

            string method = "order.cancel.v2";

            orderInput.appkey = _config.Appkey;
            orderInput.method = method;

            CLSLogger.Info("ordercancel_input", orderInput.ToJsonString(), logTags);

            var response = await ExecuteAsync(method, orderInput);

            CLSLogger.Info("ordercancel_response", response.ToJsonString(), logTags);

            var result = response.ToObject<EcinxResponse>();

            return result;
        }

    }
    public class GoodsAddInput
    {
        public string appkey { get; set; }
        public string method { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string barCode { set; get; }

        public string catalogCode { set; get; }
        //public string brandCode { set;get; }
        //public decimal? height { set; get; }
        //public decimal? length { set; get; }

    }

    public class GoodsQueryInput
    {
        public string appkey { get; set; }
        public string method { get; set; }
        public string code { get; set; }
    }

    public class OrderDeleteInput
    {
        public string appkey { get; set; }
        public string method { get; set; }
        //public string declareOrderCode { get; set; }
        //public string shopCode { get; set; }
        public string orderCode { get; set; }
    }

    public class rows
    {
        public string code { get; set; }
        public string name { get; set; }
        public string barCode { get; set; }
        public object brandCode { get; set; }
        public object brandName { get; set; }
        public string catalogCode { get; set; }
        public string catalogName { get; set; }
        public object hsCode { get; set; }
        public int status { get; set; }
        public int enableBatch { get; set; }
        public double weight { get; set; }
        public int length { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public object salePrice { get; set; }
        public object minPrice { get; set; }
        public object shelfLife { get; set; }
        public object remark { get; set; }
        public string createTime { get; set; }
    }

    public class GoodsQueryResult
    {
        public int currentPage { get; set; }
        public int pageSize { get; set; }
        public List<rows> rows { get; set; }
        public int total { get; set; }
    }

    public class GoodsQueryResponse
    {
        public bool success { get; set; }
        public object errorCode { get; set; }
        public object errorMsg { get; set; }
        public GoodsQueryResult result { get; set; }
    }

    //库存查询的输出Json
    public class EcinxInventoryQueryResponse
    {
        public bool success { get; set; }
        public object errorCode { get; set; }
        public object errorMsg { get; set; }
        public InventoryResult result { get; set; }
    }

    public class InventoryResult
    {
        public int currentPage { get; set; }
        public int pageSize { get; set; }
        public List<ResultRow> rows { get; set; }
        public int total { get; set; }
    }
    public class ResultRow
    {
        public string goodsCode { get; set; }
        public string goodsName { get; set; }
        public string barCode { get; set; }
        public string catalogCode { get; set; }
        public string catalogName { get; set; }
        public object brandCode { get; set; }
        public object brandName { get; set; }
        public string warehouseCode { get; set; }
        public string warehouseName { get; set; }
        public double costPrice { get; set; }
        public int inventory { get; set; }
        public int canPickingNum { get; set; }
        public int canSalesNum { get; set; }
        public int orderOccupiedNum { get; set; }
        public int transitNum { get; set; }
        public int enableBatch { get; set; }
        public object enableNegativeStock { get; set; }
        public object enableStorageLocation { get; set; }
        public List<InventoryBatch> batches { get; set; }
    }
    public class InventoryBatch
    {
        public string batchNum { get; set; }
        public int inventory { get; set; }
        public int canPickingNum { get; set; }
        public string expiryDate { get; set; }
        public string produceDate { get; set; }
        public object shelfLife { get; set; }
        public string storageDate { get; set; }
    }
}
