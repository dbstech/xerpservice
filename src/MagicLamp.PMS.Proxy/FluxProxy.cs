﻿using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy
{
    public class FluxProxy
    {
        /// <summary>
        /// 推送商品到Flux
        /// </summary>
        /// <param name="message">商品信息列表</param>
        /// <param name="client_customerid">来源配置文件</param>
        /// <param name="APIAddress">推送地址.来源配置文件</param>
        /// <param name="apptoken">来源配置文件</param>
        /// <param name="appkey">来源配置文件</param>
        /// <param name="client_db">来源配置文件</param>
        /// <returns></returns>
        public static MagicLamp.Flux.API.Models.FluxResponse PushSKUData(object[] message)
        {
            string data = Newtonsoft.Json.JsonConvert.SerializeObject(
                            new MagicLamp.Flux.API.Models.Data()
                            {
                                xmldata = new MagicLamp.Flux.API.Models.XmlData()
                                {
                                    header = message
                                }
                            });

            FluxConfig config = Ultities.GetConfig<FluxConfig>();

            MagicLamp.Flux.API.FluxHttp http = new MagicLamp.Flux.API.FluxHttp();
            string response = http.Send(config.Url, "putSKUData", "SKU",
                config.Client_customerid,
                config.Apptoken,
                config.Appkey,
                config.Client_db,
                data);
            response = System.Web.HttpUtility.UrlDecode(response);
            MagicLamp.Flux.API.Models.FluxResponse fluxResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<MagicLamp.Flux.API.Models.FluxResponse>(response);
            return fluxResponse;
        }
    }
}
