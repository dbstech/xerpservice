﻿using MagicLamp.Flux.API;
using MagicLamp.Flux.API.Models;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.DTOs.Flux;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy
{
    public class FluxWMSProxy
    {
        /// <summary>
        /// 新西兰渠道标识
        /// </summary> 
        private static int[] exportFlagNz = { 1, 6, 7, 14, 16, 23, 25, 33, 34, 37, 38, 40, 46, 64, 70, 72, 77, 89,92 };
        /// <summary>
        /// 澳洲渠道标识
        /// </summary>
        private static int[] exportFlagAu = { 11, 12, 17, 26, 29, 47, 48, 61, 67 };
        private static int[] yushang = { 1, 888, 999, 99 };
        /// <summary>
        /// 不需要先生成物流单号的渠道标识
        /// </summary>
        public static List<int> NoNeedCarrierIdExportFlags = new List<int> { 7 };
        /// <summary>
        /// 允许同步的状态
        /// </summary>
        public static List<int> AllowSyncStatus = new List<int> { 0, 1, 2, 40, };
        /// <summary>
        /// 长江不需要CCIC渠道标识
        /// </summary>
        public static List<int> ChangJiangNOCCICExportFlags = new List<int> { 17 };

        /// <summary>
        /// Cancel order in Flux.
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="exportFlag"></param>
        /// <returns></returns>
        public static FluxResponse CancelSOData(FluxCancelOrderDTO dto, int? exportFlag)
        {
            string methodName = "cancelSOData";
            string messageId = "SOC";

            FluxResponse result = new FluxResponse();
            FluxCancelOrderNos orderNos = new FluxCancelOrderNos();
            orderNos.ordernos = dto;
            FluxMessage fMessage = new FluxMessage();
            fMessage.data = orderNos;
            FluxRequest fRequest = new FluxRequest();
            fRequest.xmldata = fMessage;

            string data = fRequest.ToJsonString();

            FluxConfig config = Ultities.GetConfig<FluxConfig>();
            FluxHttp fluxHttp = new FluxHttp();
            string response = fluxHttp.Send(
                config.Url,
                methodName,
                messageId,
                //GetCustomerId(exportFlag),
                config.Client_customerid,
                config.Apptoken,
                config.Appkey,
                config.Client_db,
                data
                );
            response = System.Web.HttpUtility.UrlDecode(response);
            result = response.ConvertFromJsonString<FluxResponse>();
            return result;
        }

        public static FluxResponse PutSOData(List<FluxPutOrderDTO> dto, int? exportFlag)
        {
            string methodName = "putSOData";
            string messageId = "SO";

            FluxResponse result = new FluxResponse();
            XmlData fXmlData = new XmlData();
            fXmlData.header = dto.ToArray();
            Data fData = new Data();
            fData.xmldata = fXmlData;

            string data = fData.ToJsonString();

            CLSLogger.Info("富勒Request:", data, null);
            string response = "";

            try
            {
                FluxConfig config = Ultities.GetConfig<FluxConfig>();
                FluxHttp fluxHttp = new FluxHttp();
                response = fluxHttp.Send(
                    config.Url,
                    methodName,
                    messageId,
                    //GetCustomerId(exportFlag),
                    config.Client_customerid,
                    config.Apptoken,
                    config.Appkey,
                    config.Client_db,
                    data
                    );
            }
            catch (Exception ex)
            {
                CLSLogger.Info("富勒ProxyException", "富勒Request:" + data +  ex.Message, null);
                throw;
            }

            response = System.Web.HttpUtility.UrlDecode(response);

            CLSLogger.Info("富勒Proxy返回报文", response, null);

            result = response.ConvertFromJsonString<FluxResponse>();
            return result;

        }

        public static FluxResponse PutDBASNData(List<FluxPutDBASNDataDTO> dto)
        {
            string methodName = "putDBASNData";
            string messageId = "DBASN";

            FluxResponse result = new FluxResponse();

            XmlData fXmlData = new XmlData();
            fXmlData.header = dto.ToArray();
            Data fData = new Data();
            fData.xmldata = fXmlData;

            string data = fData.ToJsonString();

            FluxConfig config = Ultities.GetConfig<FluxConfig>();
            FluxHttp fluxHttp = new FluxHttp();
            string response = fluxHttp.Send(
                config.Url,
                methodName,
                messageId,
                //GetCustomerId(exportFlag),
                config.Client_customerid,
                config.Apptoken,
                config.Appkey,
                config.Client_db,
                data
                );
            response = System.Web.HttpUtility.UrlDecode(response);
            result = response.ConvertFromJsonString<FluxResponse>();
            return result;
        }

        public static string GetCustomerId(int? exportFlag)
        {
            if (Array.IndexOf(exportFlagNz, exportFlag) > -1)
            {
                return "nz";
            }
            else if (Array.IndexOf(exportFlagAu, exportFlag) > -1)
            {
                return "au";
            }
            else
            {
                return "no";
            }
        }

        //根据渠道判断所属仓库
        public static string GetWarehouseId(int? exportFlag)
        {
            if (Array.IndexOf(exportFlagNz, exportFlag) > -1)
            {
                return "WH01";
            }
            else if (Array.IndexOf(exportFlagAu, exportFlag) > -1)
            {
                return "WH02";
            }
            else
            {
                return "no";
            }
        }

        /// <summary>
        /// 根据渠道和业务判断货主
        /// </summary>
        /// <param name="exportFlag"></param>
        /// <param name="biz"></param>
        /// <returns></returns>
        public static string GetCustomerIdByExportAndBiz(int? exportFlag, int? biz)
        {
            if (Array.IndexOf(exportFlagNz, exportFlag) > -1 && Array.IndexOf(yushang, biz) > -1)
            {
                return "aladdin";
            }
            else if ((Array.IndexOf(exportFlagAu, exportFlag) > -1 && Array.IndexOf(yushang, biz) > -1)) //后面这个比较是为了HE官网卖AU商品准备的
            {
                return "au";
            }
            else
            {
                return "he";
            }
        }

        /// <summary>
        /// 判断是否是仓库需要操作的业务
        /// </summary>
        /// <param name="exportFlag">渠道</param>
        /// <returns></returns>
        public static bool IsFlux(int exportFlag)
        {
            if (PMSConsts.NeedPushFluxExportFlag.Contains(exportFlag))
            {
                return true;
            }
            return false;
        }
    }
}
