﻿using System;
using RestSharp;

namespace MagicLamp.Flux.API
{
    public class FluxHttp
    {
        public string Send(string url, string method, string messageid, string client_customerid,
                           string apptoken, string appkey, string client_db, string data)
        {
            string sign = CodingMessage.Encrypt(appkey, data);
            data = System.Web.HttpUtility.UrlEncode(data);
            var client = new RestClient(url);
            var request = new RestRequest("/datahubWeb/FLUXWMSJSONAPI/", Method.POST);
            request.AddParameter("method", method);
            request.AddParameter("messageid", messageid);
            request.AddParameter("client_customerid", client_customerid);
            request.AddParameter("apptoken", apptoken);
            request.AddParameter("appkey", appkey);
            request.AddParameter("client_db", client_db);
            request.AddParameter("data", data);
            request.AddParameter("sign", sign);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}
