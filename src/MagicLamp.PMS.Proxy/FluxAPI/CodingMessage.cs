﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace MagicLamp.Flux.API
{
    public static class CodingMessage
    {
        /// <summary>
        /// 生成签名
        /// </summary>
        /// <param name="appSecret">密匙</param>
        /// <param name="msg">消息</param>
        /// <returns>签名</returns>
        public static string Encrypt(string appSecret, string msg)
        {
            //组合appSecret和msg
            string data_digest = appSecret + msg + appSecret;
            //MD5
            byte[] result = Encoding.UTF8.GetBytes(data_digest);
            MD5 md5 = new MD5CryptoServiceProvider();
            result = md5.ComputeHash(result);
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                sBuilder.Append(result[i].ToString("x2"));
            }
            data_digest = sBuilder.ToString();
            //Base64
            result = Encoding.Default.GetBytes(data_digest);
            data_digest = Convert.ToBase64String(result);
            //Upper
            data_digest = data_digest.ToUpper();
            //utf-8 urlEncoding
            data_digest = System.Web.HttpUtility.UrlEncode(data_digest);
            return data_digest;
        }
    }
}
