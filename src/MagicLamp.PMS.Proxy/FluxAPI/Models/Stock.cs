﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.Flux.API.Models
{
    public class QueryINV
    {
        public string CustomerID { get; set; }
        public string WarehouseID { get; set; }
        public string SKU { get; set; }
    }
    public class QINVHeader
    {
        public QueryINV header { get; set; }
    }
    public class QINVData
    {
        public QINVHeader data { get; set; }
    }
    public class QINVXmlData
    {
        public QINVData xmldata { get; set; }
    }
    public class Items
    {
        public Item[] item { get; set; }
    }
    public class Item
    {
        public string CustomerID { get; set; }
        public string WarehouseID { get; set; }
        public string SKU { get; set; }
        public Double Qty { get; set; }
        public string Lotatt01 { get; set; }
        public string Lotatt02 { get; set; }
        public string Lotatt03 { get; set; }
        public string Lotatt04 { get; set; }
        public string Lotatt05 { get; set; }
        public string Lotatt06 { get; set; }
        public string Lotatt07 { get; set; }
        public string Lotatt08 { get; set; }
        public string Lotatt09 { get; set; }
        public string Lotatt10 { get; set; }
        public string Lotatt11 { get; set; }
        public string Lotatt12 { get; set; }
        public string Udf1 { get; set; }
        public string Udf2 { get; set; }
        public string Udf3 { get; set; }
        public string Udf4 { get; set; }
        public string Udf5 { get; set; }
        public string Udf6 { get; set; }
        public string Udf7 { get; set; }
        public string Udf8 { get; set; }
        public string Udf9 { get; set; }
        public string Udf10 { get; set; }
    }
}
