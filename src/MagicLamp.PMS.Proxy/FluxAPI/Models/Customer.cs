﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.Flux.API.Models
{
    public class Customer
    {
        public string CustomerID { get; set; }
        /// <summary>
        /// 客户类型：OW-客户，VE-供应商,CA-快递,WH-仓库
        /// </summary>
        public string Customer_Type { get; set; }
        public string Descr_C { get; set; }
        public string Descr_E { get; set; }
        public string Address1 { get; set; }
        public string Contact1_Tel1 { get; set; }
        public string Contact1_Tel2 { get; set; }
        public string Contact1_Email { get; set; }
        public string NOTES { get; set; }
        /// <summary>
        /// Y:激活 N:不激活
        /// </summary>
        public string Active_Flag { get; set; }
    }
}
