﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.Flux.API.Models
{
    public class Orders
    {
        public OrderNos ordernos { get; set; }
    }
    public class OrderNos
    {
        public string CustomerID { get; set; }
        public string OrderNo { get; set; }
        public string OrderType { get; set; }
        public string WarehouseID { get; set; }
    }
    public class POHearder
    {
        public string OrderNo { get; set; } // nvarchar(50)
        public string OrderType { get; set; } // varchar(2)
        public string CustomerID { get; set; } // varchar(60)
        public string ASNCreationTime { get; set; } // datetime
        public string ExpectedArriveTime1 { get; set; } // datetime
        public string ExpectedArriveTime2 { get; set; } // datetime
        public string ASNReference2 { get; set; } // nvarchar(50)
        public string ASNReference3 { get; set; } // nvarchar(50)
        public string ASNReference4 { get; set; } // nvarchar(50)
        public string ASNReference5 { get; set; } // nvarchar(50)
        public string UserDefine1 { get; set; } // nvarchar(50)
        public string UserDefine2 { get; set; } // nvarchar(50)
        public string UserDefine3 { get; set; } // nvarchar(50)
        public string UserDefine4 { get; set; } // nvarchar(50)
        public string UserDefine5 { get; set; } // nvarchar(50)
        public string Notes { get; set; } // varchar(120)
        public string SupplierID { get; set; } // varchar(120)
        public string WarehouseID { get; set; } // varchar(4)
        public List<PoLine> detailsItem { get; set; } // varchar(4)
    }
    public class PoLine
    {
        public string CustomerID { get; set; } // nvarchar(60)
        public string SKU { get; set; } // varchar(150)
        public decimal ExpectedQty { get; set; } // numeric(8, 4)
        public string UserDefine1 { get; set; } // varchar(1)
        public string UserDefine2 { get; set; } // varchar(1)
        public string UserDefine3 { get; set; } // varchar(1)
        public string UserDefine4 { get; set; } // varchar(1)
        public string UserDefine5 { get; set; } // varchar(1)
        public string Notes { get; set; } // varchar(1)
    }
}
