﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.Flux.API.Models
{
    /// <summary>
    /// 通用的返回消息
    /// </summary>
    public class Response
    {
        public string returnCode { get; set; }
        public string returnDesc { get; set; }
        public string returnFlag { get; set; }
        public resultInfo[] resultInfo { get; set; }
    }
    public class resultInfo
    {
        public string OrderNo { get; set; }
        public string OrderType { get; set; }
        public string CustomerID { get; set; }
        public string WarehouseID { get; set; }
        public string errorcode { get; set; }
        public string errordescr { get; set; }
    }
    public class FluxResponse
    {
        public FluxReturn Response { get; set; }
    }
    public class FluxReturn
    {
        public Response @return { get; set; }
        public object items { get; set; }
    }
    public class Data
    {
        public XmlData xmldata { get; set; }
    }
    public class XmlData
    {
        public object[] header { get; set; }
    }

    public class FluxRequest
    {
        public FluxMessage xmldata { get; set; }
    }
    public class FluxMessage
    {
        public object data { get; set; }
    }

    public class FluxCancelOrderNos
    {
        public object ordernos { get; set; }
    }

    public class FluxXmldata
    {
        public object xmldata { get; set; }
    }
    public class FluxData
    {
        public object data { get; set; }
    }
    public class FluxOrderinfo
    {
        public OrderInfo[] orderinfo { get; set; }
    }
    public class OrderInfo
    {
        public string OrderNo { get; set; }
        public string OrderType { get; set; }
        public string CustomerID { get; set; }
        public string WarehouseID { get; set; }
        public string Status { get; set; }
        public object[] item { get; set; }
    }
    public class ASNOrderItem
    {
        public string SKU { get; set; }
        public string ReceivedQty { get; set; }
        public string ReceivedTime { get; set; }
    }
}

