﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.Flux.API.Models
{
    public class Product
    {
        /// <summary>
        /// 货主
        /// </summary>
        public string CustomerID { get; set; }
        public string SKU { get; set; }
        public string Active_Flag { get; set; }
        public string Descr_C { get; set; }
        public string Descr_E { get; set; }
        public decimal GrossWeight { get; set; }
        public decimal NetWeight { get; set; }
        public decimal Cube { get; set; }
        public decimal Price { get; set; }
        public decimal SKULength { get; set; }
        public decimal SKUWidth { get; set; }
        public decimal SKUHigh { get; set; }
        public string Alternate_SKU1 { get; set; }
        public string Alternate_SKU2 { get; set; }
        public string Alternate_SKU3 { get; set; }
        public string Alternate_SKU4 { get; set; }
        public string Alternate_SKU5 { get; set; }
        /// <summary>
        /// ERP原始sku
        /// </summary>
        public string ReservedField02 { get; set; }
        /// <summary>
        /// 包材
        /// </summary>
        public string ReservedField01 { get; set; }
        /// <summary>
        /// 包材分数
        /// </summary>
        public string ReservedField09 { get; set; }
        /// <summary>
        /// 包材成本
        /// </summary>
        public string ReservedField08 { get; set; }

        /// <summary>
        /// 采购账期
        /// </summary>
        public string SKU_Group9 { get; set; }
    }
}
