using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Proxy.YiTongBaoOMSAPI
{
    public class YiTongBaoServerException
     : Exception
    {
        public YiTongBaoServerException ()
        {

        }
        public YiTongBaoServerException (string message) : base(message)
        {

        }
    }
}
