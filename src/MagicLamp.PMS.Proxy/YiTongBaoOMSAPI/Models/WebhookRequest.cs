using System;
using Newtonsoft.Json.Linq;

namespace MagicLamp.PMS.Proxy.YiTongBaoOMSAPI
{
    public class WebhookRequest
    {
        public string name { get; set; }
        public string app_key { get; set; }
        public string data { get; set; }
        public string timestamp { get; set; }
        public string sign { get; set; }
    }
}
