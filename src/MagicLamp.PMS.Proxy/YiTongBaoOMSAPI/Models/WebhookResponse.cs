using System;
using Newtonsoft.Json.Linq;

namespace MagicLamp.PMS.Proxy.YiTongBaoOMSAPI
{
    public class WebhookResponse
    {
        public string code { get; set; }
        public bool success { get; set; }
        public string msg { get; set; }
    }
}
