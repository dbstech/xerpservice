using System;
using System.Collections.Generic;

namespace MagicLamp.PMS.Proxy.YiTongBaoOMSAPI.DeliveryOrderConfirm
{
    public class DeliveryOrderConfirm
    {
        public string ApiMethodName { get; set; }
        public DeliveryOrder DeliveryOrder { get; set; }
        public List<OrderLine> OrderLines { get; set; }
        public List<Package> Packages { get; set; }
        public string ResponseClass { get; set; }
        public string Version { get; set; }
    }

    public partial class DeliveryOrder
    {
        public string DeliveryOrderCode { get; set; }
        public string DeliveryOrderId { get; set; }
        public string OrderType { get; set; }
        public string OutBizCode { get; set; }
        public string WarehouseCode { get; set; }
    }

    public partial class OrderLine
    {
        public long ActualQty { get; set; }
        public long? BatchCode { get; set; }
        public string ItemCode { get; set; }
        public string ItemId { get; set; }
    }

    public partial class Package
    {
        public string ExpressCode { get; set; }
        public List<Item> Items { get; set; }
        public string LogisticsCode { get; set; }
    }

    public partial class Item
    {
        public string ItemCode { get; set; }
        public string ItemId { get; set; }
        public long Quantity { get; set; }
    }
}