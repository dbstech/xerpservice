using System;
using Newtonsoft.Json.Linq;

namespace MagicLamp.PMS.Proxy.YiTongBaoOMSAPI
{
    public class Request
    {
        public JObject data { get; set;}
        public string sign { get; set; }
        public string name { get; set; }
        public string format { get; set ; } = "json";
        public string app_key { get; set; }
        public string version { get; set; } = "";
        public string timestamp { get; set; }
    }
}
