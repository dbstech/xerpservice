using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MagicLamp.PMS.Proxy.YiTongBaoOMSAPI.OrderProcessReport
{
    public class OrderProcessReport
    {
        // TODO: check if ordercode is orderid or oid and should it be string or int
        public string OrderCode { get; set; }
        public string OrderType { get; set; }
        public string ProcessStatus { get; set; }
        public string LogisticNo { get; set; }
        public string LogisticsCode { get; set; }
    }

    public enum ProcessStatus
    {
        [Description("仓库接单")]
        ACCEPT,
        [Description("部分收货完成")]
        PARTFULFILLED,
        [Description("收货完成")]
        FULFILLED,
        [Description("打印")]
        PRINT,
        [Description("捡货")]
        PICK,
        [Description("复核")]
        CHECK,
        [Description("打包")]
        PACKAGE,
        [Description("称重")]
        WEIGH,
        [Description("待提货")]
        READY,
        [Description("已发货")]
        DELIVERED,
        [Description("异常")]
        EXCEPTION,
        [Description("关闭")]
        CLOSED,
        [Description("取消")]
        CANCELED,
        [Description("仓库拒单")]
        REJECT,
        [Description("其他")]
        OTHER,
        [Description("部分发货完成")]
        PARTDELIVERED,
        [Description("地方海关处理成功")]
        SUBCUSTOMFILLED,
        [Description("总署清单新增成功")]
        DECLARADDSUCCESS,
        [Description("总署清单逻辑校验通过")]
        DECLARLOGIC,
        [Description("清关通过")]
        CUSTOMSCLEARANCE,
        [Description("海关查验")]
        CUSTOMSGM,
        [Description("海关人工审单")]
        CUSTOMSRM,
        [Description("海关其他状态")]
        CUSTOMSOTHER,
        [Description("开始清关")]
        STARTDECLARE,
        [Description("海关退单")]
        CUSTOMSREDECL,
        [Description("清关失败")]
        CUSTOMSREFUND,
        [Description("打包失败")]
        PACKAGEFAILED
    }
}