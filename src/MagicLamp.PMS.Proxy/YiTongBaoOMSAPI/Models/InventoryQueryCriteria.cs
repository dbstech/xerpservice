using System;
namespace MagicLamp.PMS.Proxy.YiTongBaoOMSAPI
{
    public class InventoryQueryCriteria
    {
        public string inventoryType { get; set; } = "ZP";
        public string itemCode { get; set; }
        public string ownerCode { get; set; }
        public string warehouseCode { get; set; } = "QDQB";
    }
}
