using System;
namespace MagicLamp.PMS.Proxy.YiTongBaoOMSAPI
{
    public class InventoryQueryResult
    {
        public string itemId { get; set; }
        public string produceCode { get; set; }
        public string inventoryType { get; set; }
        public int availableQty { get; set; }
        public int quantity { get; set; }
        public string batchCode { get; set; }
        public string itemCode { get; set; }
        public int lockQuantity { get; set; }
        public string expireDate { get; set;}
        public string productDate { get; set;}
    }
}
