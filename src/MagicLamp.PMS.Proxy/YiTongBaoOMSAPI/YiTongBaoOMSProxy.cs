﻿using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.DTOs;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using RestSharp;
using MagicLamp.PMS.Proxy.YiTongBaoOMSAPI;
using System.Text;
using System.Security.Cryptography;
using Abp.AutoMapper;
using System.Threading.Tasks;
using MagicLamp.PMS.DTOs.BondedWarehouseQingDao;
using Abp.Json;
using Abp.Extensions;

namespace MagicLamp.PMS.Proxy
{
    public class YiTongBaoOMSProxy
    {
        YiTongBaoOMSConfig config = Ultities.GetConfig<YiTongBaoOMSConfig>();

        readonly IRestClient restClient;
        public YiTongBaoOMSProxy()
        {
            restClient = new RestClient(config.Url);
        }
        public bool ValidateWebhookRequest(WebhookRequest request)
        {
            string sign = CreateSign(request);
            return request.sign == sign;
        }

        private string CreateSign<T>(T request) where T : class
        {
            SortedDictionary<string, string> requestParams = new SortedDictionary<string, string>();

            foreach (var property in request.GetType().GetProperties())
            {
                if (property.Name != "sign")
                {
                    requestParams.Add(property.Name, property.GetValue(request).ToString());
                }
            }

            var sign = new StringBuilder();
            sign.Append(config.AppSecret);

            foreach (var item in requestParams)
            {
                if (item.Value != null)
                {
                    sign.Append(item.Key).Append(item.Value);
                }
            }

            sign.Append(config.AppSecret);

            return Md5(sign.ToString());
        }
        public void SignRequest(Request request)
        {
            request.app_key = config.AppKey;
            TimeZoneInfo chinaStandardTime = TimeZoneInfo.FindSystemTimeZoneById("Asia/Shanghai");
            request.timestamp = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, chinaStandardTime).ToString("yyyy-MM-dd HH:mm:ss");

            SortedDictionary<string, string> requestParams = new SortedDictionary<string, string>();
            foreach (var property in request.GetType().GetProperties())
            {
                if (property.GetValue(request) != null)
                {
                    requestParams.Add(property.Name, property.GetValue(request).ToString());
                }
            }

            var sign = new StringBuilder();
            sign.Append(config.AppSecret);

            foreach (var item in requestParams)
            {
                sign.Append(item.Key).Append(item.Value);
            }

            sign.Append(config.AppSecret);

            request.sign = Md5(sign.ToString());
        }

        public string Md5(string message)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] input = Encoding.UTF8.GetBytes(message);
            byte[] buff = md5.ComputeHash(input);
            return ByteToHex(buff);
        }

        public string ByteToHex(byte[] bytes)
        {
            StringBuilder sign = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                String hex = (bytes[i] & 0xFF).ToString("X");
                if (hex.Length == 1)
                {
                    sign.Append("0");
                }
                sign.Append(hex.ToUpper());
            }
            return sign.ToString();
        }
        public IEnumerable<InventoryQueryResult> InventoryQuery(IEnumerable<InventoryQueryCriteria> criterias)
        {
            var criteriaList = criterias.ToList();
            criteriaList.ForEach(x => x.ownerCode = config.EbcOmsCode);

            JObject data = new JObject(
                    new JProperty("criteriaList", JArray.FromObject(criteriaList))
                );

            IRestResponse response = GetResponse("inventory.query.wms", data);

            JObject content = JObject.Parse(response.Content);
            
            IEnumerable<InventoryQueryResult> result = ((JArray)content["data"]["items"]).Select(
                x => JsonConvert.DeserializeObject<InventoryQueryResult>(x.ToJsonString())
            );
            return result;
        }

        public IRestResponse CreateOrder(YiTongBaoDeliveryOrderDTO input)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>
            {
                ["orderid"] = input.orderNo
            };

            CLSLogger.Info("易通宝下单input", input.ToJsonString(), logTags);

            JObject data = new JObject(JObject.FromObject(input));

            IRestResponse response = GetResponse("deliveryorder.create", data);

            //CLSLogger.Info("易通宝下单response", response.ToString(), logTags);

            return response;
        }

        private IRestResponse GetResponse(string methodName, JObject data)
        {
            var requestObject = new Request
            {
                name = methodName,
                data = data
            };

            SignRequest(requestObject);

            var request = new RestRequest(Method.POST);
            request.AddObject(requestObject);

            CLSLogger.Info("易通宝request", JsonConvert.SerializeObject(requestObject));

            IRestResponse response = restClient.Execute(request);

            JObject content = JObject.Parse(response.Content);

            CLSLogger.Info("易通宝response", content.ToJsonString());

            if (content["code"].ToObject<int>() != 0)
            {
                throw new YiTongBaoServerException(content["msg"].ToString());
            }

            return response;
        }

        public YiTongBaoDeliveryOrderDTO AssembleYiTongBaoCreateOrderInput(QingDaoBondedOrderInputDTO input, int exportFlag,
            string carrierID)
        {
            YiTongBaoDeliveryOrderDTO orderInputDTO = new YiTongBaoDeliveryOrderDTO
            {
                modifyMark = "ADD",

                ebcOmsCode = exportFlag == 73 ? "MFDQD" : "MJKQDO",
                //电商平台海关备案十位号
                ebpOmsCode = "37026601GH",

                logisticsOmsCode = "YTO",
                payOmsCode = input.payOmsCode,
                orderNo = input.orderNo,
                soNo = exportFlag == 73 ? carrierID : null,
                payNo = input.payNo,

                buyerIdNumber = input.buyerIdNumber,
                buyerName = input.buyerName,
                buyerRegNo = input.buyerRegNo,
                buyerTelephone = input.buyerTelephone,
                consigneeAddress = input.consigneeAddress,
                consigneePhone = input.consigneeTelephone,
                consigneeName = input.consignee,
                province = input.province,
                city = input.city,
                area = input.area,
                freight = input.freight,
                discount = input.discount,
                goodsValue = input.goodsValue,
                taxTotal = input.taxTotal,
                acturalPaid = input.acturalPaid,

                //pending confrmation when deployed to production
                grossWeight = 0,
                netWeight = 0,

                senderName = input.senderName,
                senderAddress = input.senderAddress,
                senderPhone = input.senderPhone,
                orderTime = input.orderTime,
                orderItems = new List<orderItems>()
            };

            int itemNumber = 1;

            foreach (var item in input.orderDetail)
            {
                orderItems thing = new orderItems
                {
                    itemCode = item.itemNo,
                    itemSort = itemNumber++,
                    qty = item.qty,
                    price = item.price
                };

                orderInputDTO.orderItems.Add(thing);
            }

            return orderInputDTO;

        }
    }
}