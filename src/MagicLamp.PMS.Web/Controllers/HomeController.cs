using Microsoft.AspNetCore.Mvc;

namespace MagicLamp.PMS.Web.Controllers
{
    public class HomeController : PMSControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}