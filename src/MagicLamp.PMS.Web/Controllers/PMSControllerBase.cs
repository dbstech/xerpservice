using Abp.AspNetCore.Mvc.Controllers;

namespace MagicLamp.PMS.Web.Controllers
{
    public abstract class PMSControllerBase: AbpController
    {
        protected PMSControllerBase()
        {
            LocalizationSourceName = PMSConsts.LocalizationSourceName;
        }
    }
}