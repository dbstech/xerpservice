﻿using Abp.AspNetCore;
using Abp.AspNetCore.Configuration;
using Abp.Hangfire;
using Abp.Hangfire.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Runtime.Caching.Redis;
using MagicLamp.PMS.Configuration;
using MagicLamp.PMS.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.CodeAnalysis.Options;
using Microsoft.Extensions.Configuration;

namespace MagicLamp.PMS.Web.Startup
{
    [DependsOn(
        typeof(PMSApplicationModule),
        typeof(PMSEntityFrameworkCoreModule),
        typeof(AbpAspNetCoreModule),
        typeof(AbpHangfireAspNetCoreModule),
        typeof(AbpRedisCacheModule))]
    public class PMSWebModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public PMSWebModule(IHostingEnvironment env)
        {
            _appConfiguration = AppConfigurations.Get(env.ContentRootPath, env.EnvironmentName);
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(PMSConsts.ConnectionStringName);

            Configuration.Navigation.Providers.Add<PMSNavigationProvider>();

            Configuration.Modules.AbpAspNetCore()
                .CreateControllersForAppServices(
                    typeof(PMSApplicationModule).GetAssembly()
                );

            Configuration.BackgroundJobs.UseHangfire();

            Configuration.Caching.UseRedis(options =>
            {
                options.ConnectionString = _appConfiguration["RedisCache:ConnectionString"];
                //options.DatabaseId = _appConfiguration.GetValue<int>("RedisCache:DatabaseId");
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PMSWebModule).GetAssembly());
        }
    }
}