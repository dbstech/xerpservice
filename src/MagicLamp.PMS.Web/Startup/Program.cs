﻿using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace MagicLamp.PMS.Web.Startup
{
    public class Program
    {


        public static IWebHost InitWebHost(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
                    config.SetBasePath(env.ContentRootPath);
                    config.AddInMemoryCollection(new[]
                           {
                         new KeyValuePair<string,string>("the-key", "the-value")
                           })
                           .AddJsonFile("appsettings.json", reloadOnChange: true, optional: false)
                           .AddJsonFile($"appsettings.{env}.json", optional: true)
                           .AddEnvironmentVariables();
                })
                .UseIISIntegration()
                .UseSentry("https://3586e55479cf4b42b71e83ad8731a1f4@sentry.io/1444837")
                .UseDefaultServiceProvider((context, options) =>
                {
                    options.ValidateScopes = context.HostingEnvironment.IsDevelopment();
                })
                .UseStartup<Startup>()
                .Build();

            return host;
        }

        public static void Main(string[] args)
        {
            InitWebHost(args).Run();
        }




    }
}
