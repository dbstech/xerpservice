﻿using System;
using Abp.AspNetCore;
using Abp.Castle.Logging.Log4Net;
using Abp.EntityFrameworkCore;
using Abp.Authorization;
using MagicLamp.PMS.EntityFrameworkCore;
using Castle.Facilities.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using Hangfire;
using MagicLamp.XERP.BackgroundJob;
using System.Collections.Generic;
using Abp.Domain.Repositories;
using MagicLamp.PMS.Entities;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Diagnostics;
using Abp.Events.Bus.Exceptions;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.Infrastructure;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc.Formatters;
using MagicLamp.PMS.Infrastructure.Hangfire;
using MagicLamp.PMS.EventHandlers;
using Microsoft.AspNetCore.Authentication.Cookies;
using MagicLamp.PMS.DTOs;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using MagicLamp.PMS.YiWuBondedWarehouse;
using MagicLamp.PMS.Proxy.ECinx;

namespace MagicLamp.PMS.Web.Startup
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {

            services.AddHealthChecks();

            //Configure DbContext
            services.AddAbpDbContext<PMSDbContext>(options =>
            {
                PMSDbContextOptionsConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
            });

            services.AddAbpDbContext<HEERPDbContext>(options =>
            {
                HEERPDbContextOptionsConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
            });

            services.AddAbpDbContext<PMSMysqlDbContext>(options =>
            {
                PMSMysqlDbContextOptionsConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
            });

            services.AddAbpDbContext<FluxWMSDbContext>(options =>
            {
                FluxWMSDbContextOptionsConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
            });

            //config
            services.Configure<BusinessConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("BusinessConfig"));
            services.Configure<FluxConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("FluxConfig"));
            services.Configure<CMQTopicConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("CMQTopicConfig"));
            services.Configure<NSFConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("NSFConfig"));
            services.Configure<GFCConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("GFCConfig"));
            services.Configure<ChangJiangConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("ChangJiangConfig"));
            services.Configure<EverAustraliaConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("EverAustraliaConfig"));
            services.Configure<PictureAddressConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("PictureAddressConfig"));
            services.Configure<HHConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("HHConfig"));
            services.Configure<EShopConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("EShopConfig"));
            services.Configure<YangXiaoFanConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("YangXiaoFanConfig"));
            services.Configure<BondedWarehouseConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("BondedWarehouseConfig"));
            services.Configure<TencentCloudConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("TencentCloudConfig"));
            services.Configure<YiTongBaoOMSConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("YiTongBaoOMS"));
            services.Configure<CmqQueueConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("CmqQueue"));
            services.Configure<BaXiConfig>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("BaXi"));
            services.Configure<List<YiWuBondedWarehouseConfig>>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("YiWu"));
            services.Configure<List<EcinxConfig>>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("ecinx"));
            services.Configure<List<FTDConfig>>(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("FTD"));



            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    var origins = Ultities.GetConfig<BusinessConfig>().AccessControlAllowOrigin.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    builder
                        .WithOrigins(origins)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                });
            });

            services.AddMvc(options =>
            {
                //options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
                options.Filters.AddService(typeof(CustomizeExceptionFilter), order: 1);
                options.InputFormatters.Add(new TextPlainInputFormatter());
            });

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "Aladdin Xerp API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);
            });

            services.AddHangfire(config =>
            {
                _ = config.UseSqlServerStorage(Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection("ConnectionStrings:Default").Value);
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, x =>
                {
                    x.Cookie.Name = Constant.AuthenticationCookieAlias;
                    x.Events.OnRedirectToLogin = context =>
                    {
                        context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                        return Task.CompletedTask;
                    };
                });


            //using (var connection = JobStorage.Current.GetConnection())
            //{
            //    foreach (var recurringJob in connection.GetRecurringJobs())
            //    {
            //        RecurringJob.RemoveIfExists(recurringJob.Id);
            //    }
            //}

            //Configure Abp and Dependency Injection
            return services.AddAbp<PMSWebModule>(options =>
            {
                //Configure Log4Net logging
                options.IocManager.IocContainer.AddFacility<LoggingFacility>(
                    f => f.UseAbpLog4Net().WithConfig("log4net.config")
                );
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            //Allow cross origin 
            app.UseCors(MyAllowSpecificOrigins);

            app.UseHealthChecks("/api/services/checkhealth");

            app.UseAbp(); //Initializes ABP framework.

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseSwagger();
            //Enable middleware to serve swagger - ui assets(HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Aladdin Xerp API V1");
            }); //URL: /swagger 

            app.UseAuthentication();
            //app.UseAuthorization();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            //Hangfire
            app.UseHangfireServer(options: new BackgroundJobServerOptions
            {
                Queues = new[] { "critical", "default" },
                WorkerCount = Environment.ProcessorCount * 5 < 10 ? 10 : Environment.ProcessorCount * 5
            });
            app.UseHangfireDashboard("/jobs", new DashboardOptions
            {
                Authorization = new[] { new HangfireDashboardAuthorizationFilter() },
                IgnoreAntiforgeryToken = true
            });
            //Run all Hangfire recurrent jobs
            HangFireRecurrentJobsManager.PerformRecurrentJobs();
        }
    }
}
