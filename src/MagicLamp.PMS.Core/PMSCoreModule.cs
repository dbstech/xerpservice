﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using MagicLamp.PMS.Localization;
using MagicLamp.PMS.Proxy;

namespace MagicLamp.PMS
{
    public class PMSCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Auditing.IsEnabledForAnonymousUsers = true;

            PMSLocalizationConfigurer.Configure(Configuration.Localization);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PMSCoreModule).GetAssembly());
        }
        public override void PostInitialize()
        {
            IocManager.Resolve<OdooProxy>().LoadSettings();
        }
    }
}