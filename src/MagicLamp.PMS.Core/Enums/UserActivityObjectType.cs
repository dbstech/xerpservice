namespace MagicLamp.PMS.Enums
{
    public enum UserActivityObjectType
    {
        Unknown = 0 ,        
        Product = 1,
        Product_IsHHPurchase = 2,
        ApiStock_QtyAvailable = 3,
        Brand = 4,
        LogisticsFee = 5,
        FileName = 6,
    }
}
