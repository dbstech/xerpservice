﻿using Abp.UI.Inputs;
using System.Collections.Generic;

namespace MagicLamp.PMS
{
    public class PMSConsts
    {
        public static int PageSize = 15;


        public const string LocalizationSourceName = "PMS";

        public const string ConnectionStringName = "Default";
        public const string HEERPConnectionStringName = "HEERP";
        public const string PMSMysqlConnectionStringName = "PMSMYSQL";
        public const string FluxWMSConnectionStringName = "FluxWMS";

        public const string IDCardDownloadRecordType = "IDCard";

        public const string ProductImgKeyPrefix = "erp/products/";

        public const string MULTIKEYSPLITER = "$Multi-Key$";

        public const string USERDEFINE6FORSTOCKSHARE = "StockShare";

        public const string UGGProductsSkipUpdateStockKey = "UGGProductsSkipUpdateStock";

        /// <summary>
        /// bizID: 1 = 188(yishan)
        /// </summary>
        public const int BizID188 = 1;

        /// <summary>
        /// bizID: 888 = 魔法海淘/洋小范(motan)
        /// </summary>
        public const int BizIDYangXiaoFan = 888;

        /// <summary>
        /// 排除的O1-01-01库位（有可能退给HH）
        /// </summary>
        public static string excludedLocation = "O1-01-01";

        //list used for mapping freights info for mqdto to push to CMQ
        public static Dictionary<string, List<string>> FREIGHT_MAPPING = new Dictionary<string, List<string>> {
            { "qexpress", new List<string>{ "ydtma", "ydtmh" } },
            { "nsf", new List<string>{ "nsf6t", "NSFAU" } },
            { "diyparcel", new List<string>{ "Anmum two"} },
        };

        public static Dictionary<string, List<string>> FREIGHTName_MAPPING = new Dictionary<string, List<string>> {
            { "DIYParcel", new List<string>{ "安满2罐装" } },
        };

        /// <summary>
        /// 排除的lost库位
        /// </summary>
        public static List<string> UnqualifiedLocationId = new List<string>
        {
            "LOST_WH01", "LOST_WH02", "REVERSEPICK_WH01", "REVERSEPICK_WH02", "SORTATIONWH01", "SORTATIONWH02", "STAGEWH01", "STAGEWH02"
        };



        /// <summary>
        /// the exportflag of order which need to be pushed to flux
        /// </summary>
        public static List<int> NeedPushFluxExportFlag = new List<int> { 1, 6, 7, 11, 12, 14, 16, 17, 23, 25, 26, 29, 33, 34, 37, 38, 40, 46, 47, 48, 51, 61, 64, 67, 70, 72, 77,89,92};

        public const string FluxStockLastSyncConfigKey = "FluxLastSyncTransactionID";

        public const string FluxAllocationDetailLastSyncConfigKey = "FluxLastSyncAllocationDetailID";

        public const string FluxMiniumStockConfigKey = "FluxNoMiniumStockSkus";

        /// <summary>
        /// 富勒sostatus状态，出库99，取消90
        /// </summary>
        public static List<string> FluxSoStatusDone = new List<string> { "99", "90" };

        /// <summary>
        /// 虚拟库存前缀
        /// </summary>
        public static List<string> VirtualStockSkuPrefix = new List<string> { "XCN", "BSCH", "HK", "THB", "KA", "ZNZMI", "AUEXC", "AUYDT", "ZNZATYDT", "XNZ", "HEXH", "HYXH", "HHXH", "HENZMK", "HYNZMK", "HHNZMK" };

        public static List<string> ExcludedLotAtt07 = new List<string> { "HHOutOfStock" };

        /// <summary>
        /// 完全装箱，订单完成（已出库订单）
        /// </summary>
        public static List<string> FluxSOStatusNotAllowedToCancel = new List<string> { "63", "99" };
    }
}