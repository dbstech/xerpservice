using System;
using Newtonsoft.Json;

namespace MagicLamp.PMS.Proxy.Odoo
{
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Product
    {
        public string default_code { get; set; }
        public string cn_name { get; set; }
        public string name { get; set; }
        public string specs { get; set; }
        public string barcode { get; set; }
        public long? brand_id { get; set; }
        public decimal? netweight { get; set; }
        public decimal? weight { get; set; }
        public decimal? length { get; set; }
        public decimal? wide { get; set; }
        public decimal? high { get; set; }
        public decimal? volume { get; set; }
        public string supplier_barcode { get; set; }
        public string tracking { get; set; }
        public string hs_code { get; set; }
        public string origin_country { get; set; }
        public string producer { get; set; }
        public string description { get; set; }
        public int? boxqty { get; set; }
        public long? categ_id { get; set; }
        public string type { get; set; }
        public int? life_time { get; set; }
        public int? alert_time { get; set; }
        // hardcoded to user_id of 2(admin)
        public long responsible_id { get; set; } = 2;
        // 销项税默认为Sale(15%)和R-Sale (15%)
        public object[] taxes_id { get; set; } = new object[1] { new object[3] { 6, 0, new int[] { 1, 10 } } };
        // 进项税默认为Purch(15%)和R-GST Inc Sale(15%)
        public object[] supplier_taxes_id { get; set; } = new object[1] { new object[3] { 6, 0, new int[] { 4, 14 } } };
        // 公司默认为H&H Supply Chain Ltd, 2为H&H Retail
        public long? company_id { get; set; } = 2;
        public decimal? standard_price { get; set; }
    }
}
