using System.Threading.Tasks;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using OdooRpc.CoreCLR.Client;
using OdooRpc.CoreCLR.Client.Interfaces;
using OdooRpc.CoreCLR.Client.Models;
using OdooRpc.CoreCLR.Client.Models.Parameters;
using System;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Proxy;
using JsonRpc.CoreCLR.Client.Models;
using MagicLamp.PMS.Proxy.Odoo;
using System.Collections.Generic;
using Abp.Dependency;
using JsonRpc.CoreCLR.Client.Interfaces;
using JsonRpc.CoreCLR.Client;

namespace MagicLamp.PMS.Proxy
{
    public class OdooProxy : ISingletonDependency
    {
        private OdooConnectionInfo _odooConnection = new OdooConnectionInfo();
        private IOdooRpcClient _odooRpcClient;
        private IJsonRpcClient _jsonRpcClient;
        private long _odooUserId = new long();
        public OdooProxy()
        {
            LoadSettings();
        }
        public void LoadSettings()
        {
            try
            {

                JObject settings = JsonConvert.DeserializeObject<JObject>(File.ReadAllText("appsettings.json"));
                _odooConnection = settings["OdooConnection"].ToObject<OdooConnectionInfo>();
                _odooConnection.IsSSL = true;

                _odooRpcClient = new OdooRpcClient(_odooConnection);
                _odooUserId = settings["OdooConnection"]["UserId"].ToObject<long>();
                _odooRpcClient.SetUserId(_odooUserId);
                Uri rpcEndpoint = new Uri($"https://{_odooConnection.Host}:{_odooConnection.Port}/jsonrpc");
                _jsonRpcClient = new JsonRpcWebClient(rpcEndpoint);
            }
            catch (RpcCallException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception($"Exception: {ex.Message}");
            }
        }
        public async Task LoginToOdoo()
        {
            try
            {
                _odooRpcClient = new OdooRpcClient(_odooConnection);

                var odooVersion = await _odooRpcClient.GetOdooVersion();

                // Console.WriteLine($"Odoo Version: {odooVersion.ServerVersion} - {odooVersion.ProtocolVersion}");

                await _odooRpcClient.Authenticate();

                if (_odooRpcClient.SessionInfo.IsLoggedIn)
                {
                    // Console.WriteLine($"Login successful => User Id: {_odooRpcClient.SessionInfo.UserId}");
                }
                else
                {
                    throw new Exception("Login failed");
                }
            }
            catch (RpcCallException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error connecting to Odoo: {ex.Message}");
            }
        }

        public async Task<JObject[]> GetProductBrands(OdooDomainFilter filter, OdooFieldParameters fieldParams)
        {
            try
            {
                var reqParams = new OdooSearchParameters(
                    "product.brand",
                    filter
                );

                var productBrands = await _odooRpcClient.Get<JObject[]>(reqParams, fieldParams);
                return productBrands;
            }
            catch (RpcCallException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error getting product brands from Odoo: {ex.Message}");

            }
        }
        public async Task<JObject[]> GetAllProductBrands()
        {
            JObject[] brands;
            var fieldParams = new OdooFieldParameters();
            fieldParams.Add("id");
            fieldParams.Add("cn_name");
            fieldParams.Add("name");
            fieldParams.Add("brand_level");


            try
            {
                brands = await _odooRpcClient.GetAll<JObject[]>("product.brand", fieldParams, new OdooPaginationParameters());
                return brands;
            }
            catch (RpcCallException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error getting product brands from Odoo: {ex.Message}");
            }
        }

        public async Task<long> CreateProductBrand(string name, string chineseName, string brandCategory = null)
        {
            try
            {
                var id = await _odooRpcClient.Create<dynamic>("product.brand", new
                {
                    name = name,
                    cn_name = chineseName,
                    brand_level = brandCategory
                });
                return id;
            }
            catch (RpcCallException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error creating productBrand in Odoo: {ex.Message}");
            }
        }

        public async Task UpdateProductBrand(long id, string name, string chineseName, string brandCategory = null)
        {
            try
            {
                await _odooRpcClient.Update<dynamic>(
                    "product.brand",
                    id,
                    new
                    {
                        name = name,
                        cn_name = chineseName,
                        brand_level = brandCategory
                    });
            }
            catch (RpcCallException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error updating productBrand in Odoo: {ex.Message}");
            }
        }
        public async Task<JObject[]> GetProducts(OdooDomainFilter filter, OdooFieldParameters fieldParams)
        {
            try
            {
                var reqParams = new OdooSearchParameters(
                    "product.template",
                    filter
                );

                var products = await _odooRpcClient.Get<JObject[]>(reqParams, fieldParams);
                return products;
            }
            catch (RpcCallException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error getting products from Odoo: {ex.Message}");

            }
        }

        public async Task<long> CreateProduct(Product product)
        {
            try
            {
                var id = await _odooRpcClient.Create<JObject>(
                    "product.template",
                    JObject.FromObject(product)
                );
                return id;
            }
            catch (RpcCallException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error creating product in Odoo: {ex.Message}");
            }
        }
        public async Task UpdateProduct(long id, Product product)
        {
            try
            {
                await _odooRpcClient.Update<JObject>(
                    "product.template",
                    id,
                    JObject.FromObject(product)
                );
            }
            catch (RpcCallException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error updating product in Odoo: {ex.Message}");
            }
        }
        public async Task<JObject[]> GetAllProducts()
        {
            JObject[] brands;
            var fieldParams = new OdooFieldParameters();
            fieldParams.Add("id");
            fieldParams.Add("default_code");
            fieldParams.Add("cn_name");
            fieldParams.Add("name");
            fieldParams.Add("responsible_id");

            try
            {
                brands = await _odooRpcClient.GetAll<JObject[]>("product.template", fieldParams, new OdooPaginationParameters());
                return brands;
            }
            catch (RpcCallException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error getting products from Odoo: {ex.Message}");
            }
        }
        // TODO: accept JObject[] or odooFieldParameters arguement
        public async Task<JObject[]> GetAllCategories()
        {
            JObject[] categories;
            var fieldParams = new OdooFieldParameters();
            fieldParams.Add("id");
            fieldParams.Add("name");
            fieldParams.Add("parent_id");
            fieldParams.Add("complete_name");

            try
            {
                categories = await _odooRpcClient.GetAll<JObject[]>("product.category", fieldParams, new OdooPaginationParameters());
                return categories;
            }
            catch (RpcCallException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error getting product categories from Odoo: {ex.Message}");
            }
        }

        public async Task<JObject[]> GetAllUsers()
        {
            JObject[] users;
            var fieldParams = new OdooFieldParameters();
            fieldParams.Add("name");

            try
            {
                users = await _odooRpcClient.GetAll<JObject[]>("res.users", fieldParams, new OdooPaginationParameters());
                return users;
            }
            catch (RpcCallException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error getting users from Odoo: {ex.Message}");
            }
        }

        public async Task<JObject[]> GetAllAccountTaxes()
        {
            JObject[] accountTaxes;
            var fieldParams = new OdooFieldParameters();
            fieldParams.Add("name");
            fieldParams.Add("company_id");

            try
            {
                accountTaxes = await _odooRpcClient.GetAll<JObject[]>("account.tax", fieldParams, new OdooPaginationParameters());
                return accountTaxes;
            }
            catch (RpcCallException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error getting account taxes from Odoo: {ex.Message}");
            }
        }

        public async Task CreateProduct(Product product, OptionalParameters optionalParameters)
        {
            var response = await _jsonRpcClient.InvokeAsync<JValue>("call", new
            {
                service = "object",
                method = "execute_kw",
                args = new object[]
                {
                    _odooConnection.Database,
                    _odooUserId,
                    _odooConnection.Password,
                    "product.template",
                    "create",
                    new object[]
                    {
                        JObject.FromObject(product)
                    },
                    new
                    {
                        context = JObject.FromObject(optionalParameters)
                    },
                },
            });

            if (response.Error != null)
            {
                throw new RpcCallException(response.Error);
            }
        }
        public async Task UpdateProduct(long id, Product product, OptionalParameters optionalParameters)
        {
            var response = await _jsonRpcClient.InvokeAsync<JValue>("call", new
            {
                service = "object",
                method = "execute_kw",
                args = new object[]
                {
                    _odooConnection.Database,
                    _odooUserId,
                    _odooConnection.Password,
                    "product.template",
                    "write",
                    new object[]
                    {
                        new long[] { id },
                        JObject.FromObject(product)
                    },
                    new
                    {
                        context = JObject.FromObject(optionalParameters)
                    },
                },
            });

            if (response.Error != null)
            {
                throw new RpcCallException(response.Error);
            }
        }
    }
}