
using Newtonsoft.Json;
using System;

namespace MagicLamp.PMS.Proxy.Odoo
{
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class OptionalParameters
    {
        public long force_company { get; set; }
    }
}