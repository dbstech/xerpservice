using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace MagicLamp.PMS.YiTongBao
{
    [Table("YiTongBaoDeliveryOrderConfirm")]
    public class DeliveryOrderConfirmEntity : Entity<long>, IHasCreationTime
    {
        public string OrderId { get; set; }
        public string ExpressCode { get; set; }
        public string CarrierId { get; set; }
        public string OrderPackNo { get; set; }
        public DateTime CreationTime { get; set; }
        public DeliveryOrderConfirmEntity()
        {
            CreationTime = DateTime.UtcNow;
        }
    }
}