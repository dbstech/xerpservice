using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MagicLamp.PMS.Entities;

namespace MagicLamp.PMS.ProductCategories
{
    [Table("SO.FreightOrderSplitCategoryRule")]
    public class FreightOrderSplitCategoryRule : BaseEntity<int>
    {
        [NotMapped]
        public override int Id { get { return CategoryRuleID; } set { CategoryRuleID = value; } }
        [Key]
        public int CategoryRuleID { get; set; }
        public int RuleID { get; set; }
        [ForeignKey("SO.ProductCategory")]
        public int ProductCategoryID { get; set; }
        public int MaxQty { get; set; }
        public bool ForMixed { get; set; }
        public DateTime? CreateTime { get; set; }
        public bool NoWeightLimited { get; set; }
        public bool NoMixed { get; set; }
        public decimal MaxPrice { get; set; }
        public List<int> SameCategoryIds { get; set; }
        public virtual SplitOrderProductCategoryEntity ProductCategory { get; set; }
        public string GetCNName()
        {
            if (ProductCategory != null)
            {
                return ProductCategory.CNName;
            }
            return null;
        }
    }
}