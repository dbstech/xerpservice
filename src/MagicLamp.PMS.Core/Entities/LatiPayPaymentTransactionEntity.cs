using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Extensions;

namespace MagicLamp.PMS.Entities
{
    [Table("LatiPayPaymentTransaction", Schema = "dbo")]
    public class LatiPayPaymentTransactionEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id
        {
            get { return LatiPayTransactionId; }
            set { LatiPayTransactionId = value; }
        }
        [Display(Name = "CREATEDATE")]
        public string CreateDate { get; set; }
        [Display(Name = "CREATETIME")]
        public string CreateTime { get; set; }
        [Display(Name = "LATIPAYTRANSACTIONID")]
        [Key]
        public string LatiPayTransactionId { get; set; }
        [Display(Name = "TYPE")]
        public string Type { get; set; }
        [Display(Name = "PAYMENTMETHOD")]
        public string PaymentMethod { get; set; }
        [Display(Name = "REFERENCE")]
        public string Reference { get; set; }
        [Display(Name = "AMOUNT")]
        public decimal Amount { get; set; }
        [Display(Name = "CURRENCY")]
        public string Currency { get; set; }
        [Display(Name = "AMOUNTCNY")]
        public decimal AmountCNY { get; set; }
        [Display(Name = "WALLETNAME")]
        public string WalletName { get; set; }
        [Display(Name = "STATUS")]
        public string Status { get; set; }
        [Display(Name = "ORDERID")]
        public string OrderId { get; set; }
        [Display(Name = "PRODUCTNAME")]
        public string PrdouctName { get; set; }
        [Display(Name = "UPDATEDATE")]
        public string UpadteDate { get; set; }
        [Display(Name = "UPDATETIME")]
        public string UpdateTime { get; set; }
        [Display(Name = "USERNAME")]
        public string UserName { get; set; }
        [Display(Name = "USERID")]
        public string UserId { get; set; }
        [Display(Name = "WALLETID")]
        public string WalletId { get; set; }
        [Display(Name = "ORIGINALAMOUNT")]
        public decimal OriginalAmount { get; set; }
        [Display(Name = "RATE")]
        public decimal Rate { get; set; }
        [Display(Name = "COUPON")]
        public string Coupon { get; set; }
        [Display(Name = "REDEMPTIONFEE")]
        public decimal RedemptionFee { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}