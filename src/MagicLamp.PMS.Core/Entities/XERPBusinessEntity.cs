﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("Business")]
    public class XERPBusinessEntity : BaseEntity<string>
    {

        [NotMapped]
        public override string Id { get { return Name; } set { Name = value; } }

        [Key]
        public string Name { get; set; }
        public string APIKey { get; set; }
        public bool Active { get; set; }
        public string DisplayName { get; set; }

    }
}
