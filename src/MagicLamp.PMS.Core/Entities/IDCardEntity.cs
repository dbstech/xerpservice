﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("IDCard")]
    public class IDCardEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id
        {
            get { return Number; }
            set { Number = value; }
        }

        [Key]
        public string Number { get; set; }
        public string Name { get; set; }
        [Column("UpadteTime")]
        public DateTime UpdateTime { get; set; }
        public int Verification { get; set; }
        public string URLFont { get; set; }
        public string URLBack { get; set; }

        public DateTime? date_of_birth { get; set; }
        public DateTime? expired_date { get; set; }


    }
}
