﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("SO.SKUProductCategory")]
    public class SkuSplitOrderCategoryEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id { get { return SKU; } set { SKU = value; } }

        public int ProductCategoryId { get; set; }

        [Key]
        public string SKU { get; set; }

    }
}
