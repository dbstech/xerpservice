using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace MagicLamp.PMS.Entities
{
    [Table("HHProductPrice")]
    public class HHProductPrice : Entity<string>, IHasCreationTime
    {
        [NotMapped]
        public override string Id { get => HHSKU; set => HHSKU = value; }
        [Key]
        public string HHSKU { get; set; }
        public decimal Price { get; set; }
        public decimal WholeSalePrice { get; set; }
        public DateTime LastModified { get; set; }
        public virtual DateTime CreationTime { get; set; }
        public HHProductPrice()
        {
            CreationTime = DateTime.UtcNow;
            LastModified = DateTime.UtcNow;
        }
    }
}