﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("api_company")]
    public class APICompanyEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id { get => apikey; set => apikey = value; }

        public string company_id { get; set; }

        public string company_name { get; set; }

        [Key]
        public string apikey { get; set; }

        public int? flag { get; set; }

        public int? bizid { get; set; }
    }
}
