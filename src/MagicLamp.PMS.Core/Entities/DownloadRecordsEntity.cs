﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("DownloadRecords")]
    public class DownloadRecordsEntity : BaseEntity<Guid>
    {
        [NotMapped]
        public override Guid Id { get { return TaskID; } set { TaskID = value; } }

        [Key]
        public Guid TaskID { get; set; }
        public string Type { get; set; }
        public string FilePath { get; set; }
        public string UserID { get; set; }
        public DateTime? CreationTime { get; set; }
        public string Status { get; set; }


    }
}
