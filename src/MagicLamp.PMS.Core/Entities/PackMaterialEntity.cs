﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("PackagingMaterials", Schema = "BAS")]
    public class PackMaterialEntity : BaseEntity<int>
    {

        [NotMapped]
        public override int Id { get { return MaterialsID; } set { MaterialsID = value; } }

        [Key]
        public int MaterialsID { get; set; }
        public string Name { get; set; }
        public double? Score { get; set; }
        public string Note { get; set; }
        public double? Cost { get; set; }
        public double? LaborCost { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? Cdate { get; set; } = DateTime.UtcNow;
    }
}
