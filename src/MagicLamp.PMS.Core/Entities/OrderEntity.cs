﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using static ICSharpCode.SharpZipLib.Zip.ExtendedUnixData;

namespace MagicLamp.PMS.Entities
{
    [Table("orders")]
    public class OrderEntity : BaseEntity<int>
    {

        [NotMapped]
        public override int Id
        {
            get { return oid; }
            set { oid = value; }
        }

        [Key]
        public int oid { get; set; }
        public string orderid { get; set; }
        public string store { get; set; }
        public string rec_name { get; set; }
        public string rec_addr { get; set; }
        public string rec_phone { get; set; }
        public string idno { get; set; }
        public string shipment { get; set; }
        public string notes { get; set; }
        public int seq { get; set; }
        public int? status { get; set; }
        public DateTime? update_time { get; set; }
        public DateTime? sendtime { get; set; }
        //public DateTime? pick_time { get; set; }
        //public DateTime? pack_time { get; set; }

        [Column("ordercreate_time")]
        public DateTime? orderCreateTime { get; set; }
        public string province { get; set; }
        public string city { get; set; }
        public string district { get; set; }
        public string town { get; set; }
        public bool? ISGetCost { get; set; }
        public string partner { get; set; }
        public bool? IsSelectedCCIC { get; set; }
        public int? valflag { get; set; }
    }
    [Table("orders_detail")]
    public class OrderDetailEntity : BaseEntity<int>
    {

        [NotMapped]
        public override int Id
        {
            get { return detailid; }
            set { detailid = value; }
        }

        [Key]
        public int detailid { get; set; }
        public int oid { get; set; }
        public string sku { get; set; }
        public string product_name { get; set; }
        public string enname { get; set; }
        public int qty { get; set; }
        public decimal? price { get; set; }
        public bool? oversale { get; set; }
    }
    [Table("seqs")]
    public class SeqEntity : BaseEntity<int>
    {
        [NotMapped]
        public override int Id
        {
            get { return seq; }
            set { seq = value; }
        }
        [Key]
        public int seq { get; set; }
        public int bizID { get; set; }
        public DateTime? up_time { get; set; }
        public Guid? userid { get; set; }
    }

    [Table("seqs_pack")]
    public class SeqPackEntity : BaseEntity<int>
    {
        [NotMapped]
        public override int Id
        {
            get { return pseq; }
            set { pseq = value; }
        }

        [Key]
        public int pseq { get; set; }
        /// <summary>
        /// 生成时间
        /// </summary>
        public DateTime? gen_time { get; set; }
        public DateTime? print_time { get; set; }
        public DateTime? out_time { get; set; }
        /// <summary>
        /// userid
        /// </summary>
        public Guid? gen_opt_id { get; set; }
        public Guid? out_opt_id { get; set; }
        public Guid? print_opt_id { get; set; }
    }

    [Table("business")]
    public class BusinessEntity : BaseEntity<int>
    {
        [NotMapped]
        public override int Id
        {
            get { return bizID; }
            set { bizID = value; }
        }
        [Key]
        public int bizID { get; set; }
        public string bizName { get; set; }
    }


    //[Table("orders_status")]
    //public class OrderStatusEntity : BaseEntity<int>
    //{
    //    [NotMapped]
    //    public override int Id
    //    {
    //        get { return flag; }
    //        set { flag = value; }
    //    }
    //    [Key]
    //    public int flag { get; set; }
    //    public string name { get; set; }
    //    public string public_desc { get; set; }

    //}

    //[Table("orders_pack_status")]
    //public class OrderPackStatusEntity : BaseEntity<int>
    //{
    //    [NotMapped]
    //    public override int Id
    //    {
    //        get { return flag; }
    //        set { flag = value; }
    //    }
    //    [Key]
    //    public int flag { get; set; }
    //    public string name { get; set; }
    //    public string notes { get; set; }

    //}

    //[Table("freights")]
    //public class FrightEntity : BaseEntity<string>
    //{
    //    [NotMapped]
    //    public override string Id
    //    {
    //        get { return id; }
    //        set { id = value; }
    //    }
    //    [Key]
    //    public string id { get; set; }

    //    public string name { get; set; }
    //    public string notes { get; set; }
    //    public string prefix { get; set; }
    //    public int code { get; set; }
    //    public int size { get; set; }
    //    public decimal? costPerKg { get; set; }
    //    public decimal? pricePerKg { get; set; }
    //    public string currencyCode { get; set; }
    //    public bool? active { get; set; }
    //    public DateTime? updateDate { get; set; }
    //    public string freightSetName { get; set; }
    //}




}
