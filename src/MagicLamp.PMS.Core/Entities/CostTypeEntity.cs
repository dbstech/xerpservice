﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("CostType", Schema = "BAS")]
    public class CostTypeEntity : BaseEntity<string>
    {

        [NotMapped]
        public override string Id { get { return Name; } set { Name = value; } }

        [Key]
        public string Name { get; set; }
        public bool? Active { get; set; }
        public string Note { get; set; }
    }
}
