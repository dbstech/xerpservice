﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("barcodes_carrier")]
    public class ParcelTrackNoEntity : BaseEntity<string>
    {

        [Column("carrier_no")]
        public string TrackNo { get; set; }
        [Column("opt_time")]
        public DateTime OperateTime { get; set; }
        public string FreightId { get; set; }
        [Column("is_use")]
        public int IsUsed { get; set; }

    }
}
