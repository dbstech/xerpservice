﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
	[Table("EditLog")]
	public class EditLogEntity : BaseEntity<int>
	{
		[NotMapped]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override int Id { get => ID; set => ID = value; }

		[Key]
		public int ID { get; set; }

		public string System { get; set; }

		public string Type { get; set; }

		public string Content { get; set; }

		public string Uesr { get; set; }

		public DateTime? DateTime { get; set; }
	}
}
