﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("InventoryProductCategory")]
    public class InventoryProductCategoryEntity : BaseEntity<int>
    {
        [NotMapped]
        public override int Id { get { return CategoryID; } set { CategoryID = value; } }

        [Key]
        public int CategoryID { get; set; }

        public string Code { get; set; }
        public string Name { get; set; }
        public string Remark { get; set; }
        public int ParentID { get; set; }

        private DateTime? _creationTime;
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? CreationTime
        {
            get
            {
                if (_creationTime == DateTime.MinValue)
                {
                    _creationTime = DateTime.UtcNow;
                }
                return _creationTime;
            }
            set
            {
                if (value != DateTime.MinValue)
                {
                    _creationTime = value;
                }
            }
        }


    }
}
