﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
	[Table("HHBindedWaveNo")]
	public class HHBindedWaveNoEntity : BaseEntity<string>
	{
		[NotMapped]
		public override string Id { get => WaveNo; set => WaveNo = value; }

		/// <summary>
		/// 波次号
		/// </summary>
		[Key]
		public string WaveNo { get; set; }
		/// <summary>
		/// 状态
		/// </summary>
		public string Status { get; set; }
		/// <summary>
		/// HH返回消息
		/// </summary>
		public string Message { get; set; }
		/// <summary>
		/// 写入表的时间
		/// </summary>
		public DateTime? CreateTime { get; set; }
		/// <summary>
		/// 更新表的时间
		/// </summary>
		public DateTime? UpdateTime { get; set; }
		/// <summary>
		/// 波次含有的订单
		/// </summary>
		public string OrdersIncluded { get; set; }
	}
}
