﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{

    [Table("banktransfer", Schema = "Payment")]
    public class BanktransferEntity : BaseEntity<long>
    {
        /// <summary>
        /// 用户订单号
        /// </summary>
        public string OrderNO { get; set; }
        /// <summary>
        /// 汇款币种
        /// </summary>
        public string TargetCurrency { get; set; }
        /// <summary>
        /// 支付方式
        /// </summary>
        public string PayMethod { get; set; }
        /// <summary>
        /// 换汇日期
        /// </summary>
        public DateTime? ExchangeDate { get; set; }
        /// <summary>
        /// 换汇汇率
        /// </summary>
        public decimal? CurrencyRate { get; set; }
        /// <summary>
        /// 汇款币种金额
        /// </summary>
        public decimal? Total { get; set; }
        public DateTime? Updated_at { get; set; } = System.DateTime.UtcNow;

    }
}
