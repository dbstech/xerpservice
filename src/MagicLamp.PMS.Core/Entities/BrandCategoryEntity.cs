using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MagicLamp.PMS.Entities
{
    [Table("BrandCategory", Schema = "BAS")]
    public class BrandCategoryEntity : BaseEntity<int>
    {
        public string Name { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
    }
}