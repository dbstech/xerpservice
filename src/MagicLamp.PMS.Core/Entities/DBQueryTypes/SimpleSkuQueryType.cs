﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.Entities.DBQueryTypes
{
    public class SimpleSkuQueryType
    {

        public string SKU { get; set; }
        public int Qty { get; set; }
        public string OrderPackNo { get; set; }
    }


}
