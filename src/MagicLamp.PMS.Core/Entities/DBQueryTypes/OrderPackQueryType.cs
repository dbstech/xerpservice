using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace MagicLamp.PMS.Entities.DBQueryTypes
{
    public class OrderPackQueryType
    {
        public string Sku { get; set; }
        [Column("orderid")]
        public string OrderId { get; set; }
        [Column("orderpackno")]
        public string OrderPackNo { get; set; }
        [Column("product_name")]
        public string ProductName { get; set; }
        public bool? Oversale { get; set;}
        [Column("qty")]
        public int LockedQty { get; set; }
        [Column("opt_time")]
        public DateTime CreatedAt { get; set; }
    }


    public class yiwuOrderPackwaitingPush
    {
        [Column("orderid")]
        public string OrderId { get; set; }

        [Column("orderpackno")]
        public string orderPackno { get; set; }

        [Column("oid")]
        public string oId { get; set; }

        [Column("rowid")]
        public string rowId { get; set; }

        [Column("ordercreate_time")]
        public DateTime ordercreate_Time { get; set; }
    }
}