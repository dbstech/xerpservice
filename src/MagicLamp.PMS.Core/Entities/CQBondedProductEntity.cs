﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Auditing;

namespace MagicLamp.PMS.Entities
{
    [Table("CQBondedProduct")]
    [Audited]

    public class CQBondedProductEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id { get => ERPSKU; set => ERPSKU = value; }

        [StringLength(50)]
        /// <summary>
        /// 关贸SKU（海关备案SKU）
        /// </summary>
        public string GMSKU { get; set; }

        [Key]
        /// <summary>
        /// ERP--SKU
        /// </summary>
        public string ERPSKU { get; set; }

        public string GOODS_SPEC { get; set; }
        public string CURRENCY_CODE { get; set; }
        public string COUNTRY { get; set; }
        public string BAR_CODE { get; set; }
        public string UNIT_CODE { get; set; }
        public string GOODS_NAME { get; set; }
        public string HS_CODE { get; set; }

        /// <summary>
        /// 增值税率
        /// </summary>
        public decimal VATRate { get; set; }


        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UNIT2 { get; set; }
        public string QTY2 { get; set; }

    }


}
