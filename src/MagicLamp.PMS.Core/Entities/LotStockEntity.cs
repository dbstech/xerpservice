﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("LotStock")]
    public class LotStockEntity : BaseEntity<long>, IHasCreationTime, IHasModificationTime
    {

        /// <summary>
        /// SKU编号
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// 在手库存
        /// </summary>
        public int StockQty { get; set; }
        /// <summary>
        /// 供应链生成批次编号(lot12)
        /// </summary>
        public string LotNo { get; set; }
        /// <summary>
        /// 自定义效期（lot4，eg.: 开封后xx天过期）
        /// </summary>
        public string PredefinedExpireDate { get; set; }

        /// <summary>
        /// 有效期Bestbefore（lot2）
        /// </summary>
        public DateTime? ExpireDate { get; set; }

        /// <summary>
        /// 不含税成本
        /// </summary>
        public decimal Cost { get; set; }

        /// <summary>
        /// 含税成本
        /// </summary>
        public decimal TaxedCost { get; set; }

        public DateTime CreationTime { get; set; }
        public DateTime? LastModificationTime { get; set; }

    }
}
