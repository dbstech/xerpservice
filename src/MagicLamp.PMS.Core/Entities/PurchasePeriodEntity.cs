﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("PurchasePeriod", Schema = "BAS")]
    public class PurchasePeriodEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id { get { return Code; } set { Code = value; } }

        [Key]
        public string Code { get; set; }

        public string Name { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreationTime { get; set; } = DateTime.UtcNow;
    }
}
