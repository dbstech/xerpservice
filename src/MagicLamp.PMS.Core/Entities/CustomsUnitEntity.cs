﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{

    [Table("CustomsUnit", Schema = "BAS")]
    public class CustomsUnitEntity : BaseEntity<int>
    {
        [Key]
        public int id { get; set; }
        public string unit_measure { get; set; }
        public int? tax_standard { get; set; }
        public DateTime? Cdate { get; set; } = DateTime.UtcNow;
    }

}
