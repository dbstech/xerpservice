﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
	[Table("OrdersSKULockForHH")]
    public class OrderSKULockForHHEntity : BaseEntity<long>
    {
		//public long ID { get; set; }

		public string OrderID { get; set; }

		public string SKU { get; set; }

		public int? QTY { get; set; }

		public int? LockDuration { get; set; }

		public DateTime? CreationTime { get; set; }

		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? ExpireTime { get; set; }
		public int? opid { get; set; }
	}
}
