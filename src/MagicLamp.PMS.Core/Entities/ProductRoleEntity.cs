﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{

    [Table("products_roles")]
    public class ProductRoleEntity : BaseEntity<int>
    {

        [Column("sku_head")]
        public string SkuPrefix { get; set; }

    }


}
