﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("orders_pack")]
    public class OrderPackEntity : BaseEntity<int>
    {

        [NotMapped]
        public override int Id
        {
            get { return opID; }
            set { opID = value; }
        }

        [Key]
        [Column("opid")]
        public int opID { get; set; }
        [Column("oid")]
        public int? oID { get; set; }
        [Column("orderid")]
        public string orderID { get; set; }
        [Column("packno")]
        public int? packNo { get; set; }
        [Column("orderpackno")]
        public string orderPackNo { get; set; }
        [Column("oidpackno")]
        public string oIdPackNo { get; set; }
        [Column("rec_name")]
        public string rec_name { get; set; }
        [Column("rec_addr")]
        public string rec_addr { get; set; }
        [Column("rec_phone")]
        public string rec_phone { get; set; }
        [Column("idno")]
        public string idNo { get; set; }
        [Column("carrierid")]
        public string carrierId { get; set; }
        [Column("bonded_warehouse_carrier_id")]
        public string bonded_warehouse_carrier_id { get; set; }
        [Column("bonded_warehouse_carrier_company")]
        public string bonded_warehouse_carrier_company { get; set; }
        [Column("freightid")]
        public string freightId { get; set; }
        [Column("exportflag")]
        public int? exportFlag { get; set; }
        [Column("freezeflag")]
        public int? freezeFlag { get; set; }
        [Column("freeze_comment")]
        public string freezeComment { get; set; }
        [Column("freeze_time")]
        public DateTime? freezeTime { get; set; }
        /// <summary>
        /// 出库时间
        /// </summary>
        [Column("out_time")]
        public DateTime? out_time { get; set; }
        [Column("out_id")]
        public Guid? outId { get; set; }
        [Column("status")]
        public int? status { get; set; }
        [Column("taxval")]
        public decimal? taxVal { get; set; }
        [Column("weightval")]
        public decimal? weightVal { get; set; }


        //[Column("USERID")]
        //public string USERID { get; set; }

        //[Column("notes")]
        //public string notes { get; set; }


        [Column("missflag")]
        public int? missFlag { get; set; }
        [Column("missflag_time")]
        public DateTime? missFlagTime { get; set; }


        [Column("opt_time")]
        public DateTime? opt_time { get; set; }
        
        [Column("refundflag")]
        public int? refundFlag { get; set; }
        [Column("refundflag_time")]
        public DateTime? refundFlagTime { get; set; }

        [Column("syc_flag")]
        public bool? syc_flag { get; set; }
        [Column("attchment_name")]
        public string attchment_name { get; set; }
        [Column("packweight")]
        public decimal? packWeight { get; set; }
        [Column("packweight_real")]
        public decimal? packWeightReal { get; set; }
        [Column("sycnsf_flag")]
        public short? sycNSFFlag { get; set; }
        [Column("pick_time")]
        public DateTime? pick_time { get; set; }
        [Column("pack_time")]
        public DateTime? pack_time { get; set; }
        [Column("pick_id_real")]
        public string pickIdReal { get; set; }
        [Column("pack_id_real")]
        public string packIdReal { get; set; }
        [Column("pseq")]
        public int? pseq { get; set; }
        [Column("updateflag")]
        public int? updateFlag { get; set; }
        /// <summary>
        /// 上传时间
        /// </summary>
        [Column("updatetime")]
        public DateTime? updateTime { get; set; }
        [Column("assign_time")]
        public DateTime? assignTime { get; set; }
        [Column("scan_time")]
        public DateTime? scanTime { get; set; }
        [Column("scan_id")]
        public Guid? scanId { get; set; }
        [Column("push_order_time")]
        public DateTime? pushOrderTime { get; set; }
        [Column("push_order_id")]
        public string pushOrderId { get; set; }
        [Column("invoice_url")]
        public string invoiceUrl { get; set; }
        [Column("distribution_code")]
        public string distributionCode { get; set; }

    }
}
