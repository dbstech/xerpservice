﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
	[Table("PoOrderHeader", Schema = "PO")]
    public class PurchaseOrderHeaderEntity : BaseEntity<string>
    {
		[NotMapped]
		public override string Id { get => PurchasingOrderNo; set => PurchasingOrderNo = value; }

		/// <summary>
		/// 采购单号
		/// </summary>
		[Key]
		[Column("Reference1")]
		public string PurchasingOrderNo { get; set; }

		//public int PoId { get; set; }

		/// <summary>
		/// 组织ID
		/// </summary>
		[Description("组织ID")]
		public string OrgId { get; set; }

		/// <summary>
		/// 订单类型  短码维护表维护 01: Product; 02: Service
		/// </summary>
		[Description("订单类型  短码维护表维护 01: Product; 02: Service")]
		public string OrderType { get; set; }

		/// <summary>
		/// 供应商
		/// </summary>
		[Description("供应商")]
		public string VendorId { get; set; }

		/// <summary>
		/// 订单状态 10: 新建 20: 下发 30: 已入库 40: 待付款 50: 关闭
		/// </summary>
		[Description("订单状态 10: 新建 20: 下发 30: 已入库 40: 待付款 50: 关闭")]
		public string OrderStatus { get; set; }

		/// <summary>
		/// 采购员
		/// </summary>
		[Description("采购员")]
		public int AgentId { get; set; }

		/// <summary>
		/// 币别
		/// </summary>
		[Description("币别")]
		public string Currency { get; set; }

		/// <summary>
		/// 汇率
		/// </summary>
		[Description("汇率")]
		public decimal? CurrencyRate { get; set; }

		/// <summary>
		/// 订单日期
		/// </summary>
		[Description("订单日期")]
		public DateTime? OrderDate { get; set; }

		/// <summary>
		/// 付款方式
		/// </summary>
		[Description("付款方式")]
		public string PaymentType { get; set; }

		/// <summary>
		/// 创建人
		/// </summary>
		[Description("创建人")]
		public string CreateBy { get; set; }

		/// <summary>
		/// 创建日期
		/// </summary>
		[Description("创建日期")]
		public DateTime? CreateDate { get; set; }

		/// <summary>
		/// 最后更新人
		/// </summary>
		[Description("最后更新人")]
		public string LastUpdateBy { get; set; }

		/// <summary>
		/// 最后更新日期
		/// </summary>
		[Description("最后更新日期")]
		public DateTime LastUpdateDate { get; set; }

		public decimal? TotalAmount { get; set; }

		/// <summary>
		/// 预售时间
		/// </summary>
		[Description("预售时间")]
		public DateTime? ExpectedArriveTime1 { get; set; }

		/// <summary>
		/// 到货时间
		/// </summary>
		[Description("到货时间")]
		public DateTime? ExpectedArriveTime2 { get; set; }

		public string Notes { get; set; }

		public string Reference2 { get; set; }

		public string Reference3 { get; set; }

		public string Reference4 { get; set; }

		public string Reference5 { get; set; }

		public string UDF1 { get; set; }

		public string UDF2 { get; set; }

		public string UDF3 { get; set; }

		public string UDF4 { get; set; }

		public string UDF5 { get; set; }

		public string UDF6 { get; set; }

		/// <summary>
		/// 发送状态; 0:待发送;1:已发送;90发送失败;100:发送成功
		/// </summary>
		[Description("发送状态; 0:待发送;1:已发送;90发送失败;100:发送成功")]
		public string SendFlag { get; set; }

		/// <summary>
		/// 实际支付金额
		/// </summary>
		[Description("实际支付金额")]
		public decimal? PaymentAmount { get; set; }

		public string PaymentBy { get; set; }

		public DateTime? PaymentDate { get; set; }

		public string PaymentNotes { get; set; }

		/// <summary>
		/// 发送失败原因
		/// </summary>
		[Description("发送失败原因")]
		public string FailReason { get; set; }

		/// <summary>
		/// 入库以后商品持有人，默认空是Aladdin
		/// </summary>
		[Description("入库以后商品持有人，默认空是Aladdin")]
		public string ProductHolder { get; set; }
	}
}
