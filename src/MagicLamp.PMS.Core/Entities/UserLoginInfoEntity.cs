﻿using Abp.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{

    [Audited]
    [Table("UserLoginInfo")]
    public class UserLoginInfoEntity : BaseEntity<int>
    {
        [NotMapped]
        public override int Id { get; set; }
        [Key]
        public string UserName { get; set; }
        public string IpAddress { get; set; }
        public string city { get; set; }
        public string region { get; set; }
        public string country { get; set; }
        public string loc { get; set; }
        public string org { get; set; }
        public string timezone { get; set; }
        public string UserAgent { get; set; }
        public string LoginResult { get; set; }

        [NotMapped]
        public DateTime Cdate { get; set; }

    }
}
