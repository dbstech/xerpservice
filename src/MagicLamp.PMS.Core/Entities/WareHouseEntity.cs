﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("WareHouse", Schema = "BAS")]
    public class WareHouseEntity : BaseEntity<int>
    {
        [NotMapped]
        public override int Id { get { return WareHouseId; } set { WareHouseId = value; } }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WareHouseId { get; set; }

        public string Location { get; set; }
        /// <summary>
        /// 是否对接实仓储
        /// </summary>
        public bool IsWMS { get; set; }

        public string code { get; set; }

        public string FluxCode { get; set; }
        public string UserId { get; set; }

        private DateTime _creationTime;
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreationTime
        {
            get
            {
                if (_creationTime == DateTime.MinValue)
                {
                    _creationTime = DateTime.UtcNow;
                }
                return _creationTime;
            }
            set
            {
                if (value != DateTime.MinValue)
                {
                    _creationTime = value;
                }
            }
        }

    }
}
