﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("OrderIDCard")]
    public class OrderIDCardEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id
        {
            get { return OrderPackNo; }
            set { OrderPackNo = value; }
        }

        [Key]
        public string OrderPackNo { get; set; }
        public string OrderID { get; set; }
        public string CarrierID { get; set; }
        public string FreightID { get; set; }
        public int ExportFlag { get; set; }
        public string RecevierName { get; set; }
        public string IDCardNo { get; set; }
        public string IDCardName { get; set; }
        public DateTime? IDCardUpdateTime { get; set; }
        public DateTime? CreationTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string URLFont { get; set; }
        public string URLBack { get; set; }
        public int? Verification { get; set; }

        public string ReceiverPhone { get; set; }

        public DateTime? DateOfBirth { get; set; }
        public DateTime? ExpiredDate { get; set; }
    }
}
