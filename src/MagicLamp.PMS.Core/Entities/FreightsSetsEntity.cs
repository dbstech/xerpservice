using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MagicLamp.PMS.Entities
{
    [Table("freights_sets", Schema = "dbo")]
    public class FreightsSetsEntity : BaseEntity<int>
    {
        [ForeignKey("freights")]
        public string FreightId { get; set; }
        [Column("freightset")]
        public override int Id { get; set; }
        public string Keyword { get; set; }
        public string Valflag { get; set; }
        public decimal? Price { get; set; }
        public decimal? Cost { get; set; }
        [ForeignKey("FreightId")]
        public virtual FreightsEntity Freights { get; set; }
    }
}
