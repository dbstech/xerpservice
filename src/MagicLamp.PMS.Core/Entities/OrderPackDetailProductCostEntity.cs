﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
	[Table("OrderPackDetails_ProductCost")]
    public class OrderPackDetailProductCostEntity : BaseEntity<long>
    {
		/*public long ID { get; set; }*/

		public string OrderID { get; set; }

		public string SKU { get; set; }

		public int Qty { get; set; }

		public decimal? Cost { get; set; }

		public DateTime CreationTime { get; set; }
		public string Currency { get; set; }
		public DateTime? UpdateTime { get; set; }
		public DateTime PushOrderTime { get; set; }
	}
}
