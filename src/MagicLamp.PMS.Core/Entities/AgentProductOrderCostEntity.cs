﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("AgentProductOrderCost")]
    public class AgentProductOrderCostEntity : BaseEntity<long>
    {

        /*public override long Id { get => this.ID; set => this.ID = value; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ID { get; set; }*/

        public string OrderPackNo { get; set; }
        public string OrderID { get; set; }
        public string CustomerID { get; set; }
        public string SKU { get; set; }
        public string ProductName { get; set; }
        public int Qty { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal Cost { get; set; }
        /// <summary>
        /// 总价
        /// </summary>
        public decimal TotalCost { get; set; }
        /// <summary>
        /// 下单时间
        /// </summary>
        public DateTime OrderTime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }
        /// <summary>
        /// 出库时间
        /// </summary>
        public DateTime? OutboundTime { get; set; }
        /// <summary>
        /// 采购币种
        /// </summary>
        public string PurchasingCurrency { get; set; }
        /// <summary>
        /// 采购价格
        /// </summary>
        public decimal? PurchasingPrice { get; set; }
        /// <summary>
        /// 基准价格（NZD）
        /// </summary>
        public decimal? BenchmarkPrice { get; set; }
        /// <summary>
        /// 基准价格汇率
        /// </summary>
        public decimal? BenchmarkPriceExchangeRate { get; set; }
        /// <summary>
        /// 供应商
        /// </summary>
        public string Agency { get; set; }
    }
}
