﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
	[Table("cache_orders")]
    public class CacheOrderEntity : BaseEntity<int>
    {
		[NotMapped]
		public override int Id
		{
			get { return id; }
			set { id = value; }
		}

		public int id { get; set; }

		public string orderid { get; set; }

		public string store { get; set; }

		public string rec_name { get; set; }

		public string rec_phone { get; set; }

		public string rec_addr { get; set; }

		public string idno { get; set; }

		public string shipment { get; set; }

		public string notes { get; set; }

		public string purchase_notes { get; set; }

		public decimal? shippingfee { get; set; }

		public decimal? toget { get; set; }

		public decimal? shouldpay { get; set; }

		public string sender_name { get; set; }

		public string sender_addr { get; set; }

		public string sender_phone { get; set; }

		public string email { get; set; }

		public decimal? weight { get; set; }

		public DateTime? update_time { get; set; }

		public int? bizid { get; set; }

		public string freightno { get; set; }

		public string province { get; set; }

		public string city { get; set; }

		public string district { get; set; }

		public string town { get; set; }

		public string platform { get; set; }

		public DateTime? ordercreate_time { get; set; }

		public string sender_rank { get; set; }

		public string payway { get; set; }

		public decimal? paymentfee { get; set; }

		/// <summary>
		/// 合作方.(NULL表示Aladdin)
		/// </summary>
		[Description("合作方.(NULL表示Aladdin)")]
		public string partner { get; set; }

		public bool? IsSelectedCCIC { get; set; }

		public DateTime? Ctime { get; set; }
	}
}
