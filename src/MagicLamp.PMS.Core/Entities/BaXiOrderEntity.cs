﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
	[Table("BaXiOrder")]
    public class BaXiOrderEntity : BaseEntity<int>
    {
		[NotMapped]
		public override int Id { get => Opid; set => Opid = value; }

		[Key]
		public int Opid { get; set; }

		public string OrderID { get; set; }

		public string OrderPackNo { get; set; }

		public DateTime? CreateTime { get; set; }

		public string ResponseData { get; set; }

		public string OriginalInfo { get; set; }

		public string Status { get; set; }

		public DateTime? UpdateTime { get; set; }
	}
}
