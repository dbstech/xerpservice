﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("V_ProductBusinessCost")]
    public class ProductBusinessCostEntity : BaseEntity<Guid>
    {
        [Key]
        public override Guid Id { get; set; }
        public string SKU { get; set; }
        public string CustomerID { get; set; }
        public string ChineseName { get; set; }
        public string EnglishName { get; set; }
        public string Business { get; set; }
        public string Formula1 { get; set; }
        public string Formula2 { get; set; }
        public string CostType { get; set; }
        public string Currency { get; set; }
        public decimal? TaxRate { get; set; }
        public decimal? Purchasecost { get; set; }
        public double? MaterialCost { get; set; }
        public double? LaborCost { get; set; }
        public double? TransportCost { get; set; }
        public decimal? DefaultCost { get; set; }
        /// <summary>
        /// 总成本
        /// </summary>
        public double? Cost { get; set; }
        public decimal? BusinessCost { get; set; }
        public decimal? DropShippingCost { get; set; }
    }
}
