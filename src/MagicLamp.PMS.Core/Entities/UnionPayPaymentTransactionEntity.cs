using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Extensions;

namespace MagicLamp.PMS.Entities
{
    [Table("UnionPayPaymentTransaction", Schema = "dbo")]
    public class UnionPayPaymentTransactionEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id
        {
            get { return ReferenceNo.ToString() + TraceNo.ToString(); }
            set { }
        }
        [Display(Name = "txn type")]
        public string TxnType { get; set; }
        [Display(Name = "txn Account No.")]
        public string TxnNo { get; set; }
        [Display(Name = "reference No.")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ReferenceNo { get; set; }
        [Display(Name = "txn time")]
        public string TxnTime { get; set; }
        [Display(Name = "trace No")]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string TraceNo { get; set; }
        [Display(Name = "Terminal ID")]
        public string TerminalId { get; set; }
        [Display(Name = "Transaction Amt")]
        public decimal TransactionAmt { get; set; }
        [Display(Name = "Service Charge")]
        public decimal ServiceCharge { get; set; }
        [Display(Name = "Payment Amt")]
        public decimal PaymentAmt { get; set; }
        [Display(Name = "Holding Amt")]
        private string _orderNumber;
        public decimal HoldingAmt { get; set; }
        [Display(Name = "Order Number")]
        public string OrderNumber
        {
            get
            {
                return _orderNumber;
            }
            set
            {    
                if(!value.IsNullOrWhiteSpace())     
                {
                    _orderNumber = value.Trim();
                }
                else
                {

                    _orderNumber = value;
                }
            }
        }        
        [Display(Name = "Product Category")]
        public string ProductCategory { get; set; }
        public decimal ExchangeRate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
