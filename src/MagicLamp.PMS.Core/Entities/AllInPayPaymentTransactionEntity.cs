using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Extensions;

namespace MagicLamp.PMS.Entities
{
    public class AllInPayPaymentTransactionEntity : BaseEntity<long>
    {
        [NotMapped]
        public override long Id
        {
            get { return TransactionId; }
            set { TransactionId = value; }
        }
        [Display(Name = "交易单号")]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long TransactionId { get; set; }
        [Display(Name = "商户号")]
        public string BusinessId { get; set; }
        [Display(Name = "商户名称")]
        public string BusinessName { get; set; }
        [Display(Name = "户名")]
        public string AccountName { get; set; }
        [Display(Name = "门店名称")]
        public string StoreName { get; set; }
        [Display(Name = "终端号")]
        public string TerminalId { get; set; }
        [Display(Name = "提交时间")]
        public DateTime SubmissionTime { get; set; }
        [Display(Name = "完成时间")]
        public DateTime CompleteTime { get; set; }
        [Display(Name = "完成日期")]
        public string CompleteDate { get; set; }
        [Display(Name = "交易类型")]
        public string TransactionType { get; set; }
        [Display(Name = "交易账号")]
        public string TransactionAccout { get; set; }
        [Display(Name = "卡类型")]
        public string CardType { get; set; }
        [Display(Name = "所属银行")]
        public string Bank { get; set; }
        [Display(Name = "原始金额")]
        public decimal OriginalAmount { get; set; }
        [Display(Name = "收支方向")]
        public string RevenueOrExpenditure { get; set; }
        [Display(Name = "交易金额")]
        public decimal TransactionAmount { get; set; }
        [Display(Name = "手续费")]
        public decimal TransactionFee { get; set; }
        [Display(Name = "处理状态")]
        public string Status { get; set; }
        [Display(Name = "订单号")]
        public string OrderId { get; set; }
        [Display(Name = "原因")]
        public string Reason { get; set; }
        [Display(Name = "原交易单号")]
        public long? OriginalTransactionId { get; set; }
        [Display(Name = "备注")]
        public string Remark { get; set; }
        // [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }

}