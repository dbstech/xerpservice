using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using MagicLamp.PMS.Enums;

namespace MagicLamp.PMS.Entities
{
    [Table("UserActivityLog", Schema = "dbo")]
    public class UserActivityLogEntity : BaseEntity<long>
    {
		[NotMapped]
		public override long Id { get => ID; set => ID = value; }

		[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long ID { get; set; }
        public Guid? SessionId { get; set; }
        public UserActivityObjectType ObjectType { get; set; }
        public string ObjectId { get; set; }
        public string Action { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Before { get; set; }
        public string After { get; set; }
    }
}
