using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MagicLamp.PMS.Entities
{
    [Table("PaymenTransaction")]
    public class PaymentTransactionEntity : BaseEntity<long>
    {
        public long TransactionId { get; set; }
        public string PaymentType { get; set; }
        public string PaymentMethod { get; set; }
        public string OrderId { get; set; }
        public string Status { get; set; }
        public DateTime CompletionTime { get; set; }
        public string CurrencyCode { get; set; }
        public decimal TransactionAmount { get; set; }
        public decimal ServiceCharge { get; set; }
        public decimal SettlementAmount { get ; set; }
        public decimal ExchangeRate { get; set; }
        public decimal RefundAmount { get; set; }
        public decimal RefundServiceCharge { get; set; }
        public decimal RefundExchangeRate { get; set; }
    }
}