﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("DOC_Order_Details")]
    public class FluxDocOrderDetailEntity : BaseEntity<string>
    {

        [NotMapped]
        public override string Id
        {
            get { return $"{OrderNo}{PMSConsts.MULTIKEYSPLITER}{OrderLineNo}"; }
        }

        public string OrderNo { get; set; }

        public int OrderLineNo { get; set; }
        public string SKU { get; set; }
        public string LineStatus { get; set; }
        public string LotAtt07 { get; set; }
        /// <summary>
        /// 订货数
        /// </summary>
        public decimal QtyOrdered { get; set; }
        /// <summary>
        /// 分配数
        /// </summary>
        public decimal QtyAllocated { get; set; }
        /// <summary>
        /// 拣货数
        /// </summary>
        public decimal QtyPicked { get; set; }


    }
}
