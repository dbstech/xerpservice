﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
	[Table("ACT_Allocation_Details")]
    public class FluxACTAllocationDetailEntity : BaseEntity<string>
    {
		[NotMapped]
		public override string Id { get => this.AllocationDetailsID; set => this.AllocationDetailsID = value; }

		[Key]
		public string AllocationDetailsID { get; set; }

		public string OrderNo { get; set; }

		public int OrderLineNO { get; set; }

		public int SKULineNO { get; set; }

		public string WaveNo { get; set; }

		public string Status { get; set; }

		public string CustomerID { get; set; }

		public string SKU { get; set; }

		public string LotNUM { get; set; }

		public string Location { get; set; }

		public string TraceID { get; set; }

		public string PackID { get; set; }

		public string UOM { get; set; }

		public decimal? Qty { get; set; }

		public decimal? Qty_Each { get; set; }

		public decimal? QtyPicked_Each { get; set; }

		public decimal? QtyShipped_Each { get; set; }

		public string PickToLocation { get; set; }

		public string PickToTraceID { get; set; }

		public DateTime? PickedTime { get; set; }

		public string PickedWho { get; set; }

		public DateTime? ShipmentTime { get; set; }

		public string ShipmentWho { get; set; }

		public string ReasonCode { get; set; }

		public string SoftAllocationDetailsID { get; set; }

		public decimal Cubic { get; set; }

		public decimal GrossWeight { get; set; }

		public decimal NetWeight { get; set; }

		public decimal? Price { get; set; }

		public string Notes { get; set; }

		public string AddWho { get; set; }

		public DateTime AddTime { get; set; }

		public string EditWho { get; set; }

		public DateTime EditTime { get; set; }

		public string PackFlag { get; set; }

		public string CheckWho { get; set; }

		public DateTime? CheckTime { get; set; }

		public string PrintFlag { get; set; }

		public string SortationLocation { get; set; }

		public string dropID { get; set; }

		public string DoubleCheckBy { get; set; }

		public string ShipmentConfirmBy { get; set; }

		public int? CartonSeqNo { get; set; }

		public string PickingTransactionID { get; set; }

		public string CartonID { get; set; }

		public string Palletize { get; set; }

		public string WorkStation { get; set; }

		public string UDFPrintFlag1 { get; set; }

		public string CartonGroup { get; set; }

		public string CheckModule { get; set; }

		public string PutToLight_Flag { get; set; }

		public string PTL_Flag { get; set; }
	}
}
