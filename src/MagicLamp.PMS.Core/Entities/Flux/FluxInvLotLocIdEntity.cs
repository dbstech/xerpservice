﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
	[Table("INV_LOT_LOC_ID")]
	public class FluxInvLotLocIdEntity : BaseEntity<string>
	{
		[NotMapped]
		public override string Id
		{
			get { return $"{this.LotNum}$multi-key${this.LocationID}$multi-key${this.TraceID}"; }
		}
		public string LotNum { get; set; }

		public string LocationID { get; set; }

		public string TraceID { get; set; }

		public string CustomerID { get; set; }

		public string SKU { get; set; }

		public decimal Qty { get; set; }

		public decimal QtyAllocated { get; set; }

		public decimal QtyRPIn { get; set; }

		public decimal QtyRPOut { get; set; }

		public decimal QtyMVIN { get; set; }

		public decimal QtyMVOut { get; set; }

		public decimal QtyOnHold { get; set; }

		public int OnHoldLocker { get; set; }

		public decimal GrossWeight { get; set; }

		public decimal NetWeight { get; set; }

		public decimal Cubic { get; set; }

		public decimal Price { get; set; }

		public DateTime AddTime { get; set; }

		public string AddWho { get; set; }

		public DateTime EditTime { get; set; }

		public string EditWho { get; set; }

		public string LPN { get; set; }

		public decimal? QTYPA { get; set; }

		public string QCStatus { get; set; }

		public DateTime? LastMaintenanceDate { get; set; }
	}
}
