﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
	[Table("DOC_Wave_Details")]
	public class FluxDocWaveDetailEntity : BaseEntity<string>
    {
		[NotMapped]
		public override string Id 
		{
			get { return $"{this.WaveNo}$multi-key${this.OrderNo}"; }
		}

		/// <summary>
		/// 波次编号
		/// </summary>
		//[Key]
		public string WaveNo { get; set; }
		/// <summary>
		/// 订单号
		/// </summary>
		//[Key]
		public string OrderNo { get; set; }

		public string LineStatus { get; set; }

		public int? SeqNo { get; set; }

		public DateTime AddTime { get; set; }

		public string AddWho { get; set; }

		public DateTime EditTime { get; set; }

		public string EditWho { get; set; }
	}
}
