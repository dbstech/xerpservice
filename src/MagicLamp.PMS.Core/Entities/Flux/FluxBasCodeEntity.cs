﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
	[Table("BAS_Codes")]
    public class FluxBasCodeEntity : BaseEntity<string>
    {
		[NotMapped]
		public override string Id
		{
			get { return $"{this.CodeID}$multi-key${this.Code}"; }
		}

		public string CodeID { get; set; }

		public string Code { get; set; }

		public string CodeName_C { get; set; }

		public string CodeName_E { get; set; }

		public int? Show_Sequence { get; set; }

		public string UDF1 { get; set; }

		public string UDF2 { get; set; }

		public string UDF3 { get; set; }

		public DateTime? AddTime { get; set; }

		public string AddWho { get; set; }

		public DateTime? EditTime { get; set; }

		public string EditWho { get; set; }

		public string UDF_OPR_CHK { get; set; }
	}
}
