﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("DOC_Order_Header")]
    public class FluxDocOrderHeaderEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id
        {
            get { return FluxOrderNo; }
            set { FluxOrderNo = value; }
        }
        /// <summary>
        /// 订单号
        /// </summary>
        [Key]
        [Column("OrderNo")]
        public string FluxOrderNo { get; set; }
        /// <summary>
        /// 订单类型
        /// </summary>
        public string OrderType { get; set; }
        /// <summary>
        /// 订单创建时间
        /// </summary>
        public DateTime OrderTime { get; set; }
        /// <summary>
        /// 订单状态
        /// </summary>
        public string SOStatus { get; set; }
        /// <summary>
        /// 货主ID
        /// </summary>
        public string CustomerID { get; set; }
        [Column("SOReference1")]
        /// <summary>
        /// 对应erp orderpackno
        /// </summary>
        public string OrderPackNo { get; set; }
        /// <summary>
        /// 收货人
        /// </summary>
        public string ConsigneeID { get; set; }
        /// <summary>
        /// 收货人名称
        /// </summary>
        public string ConsigneeName { get; set; }
        public string UserDefine5 { get; set; }
        public string UserDefine6 { get; set; }
        /// <summary>
        /// 仓库ID
        /// </summary>
        public string Warehouseid { get; set; }
        public DateTime AddTime { get; set; }
        public DateTime EditTime { get; set; }
        public string WAVENO { get; set; }
        /// <summary>
        /// 出库时间
        /// </summary>
        public DateTime? LastShipmentTime { get; set; }
    }
}
