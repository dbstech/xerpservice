﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
	[Table("DOC_ASN_Header")]
	public class FluxDocASNHeaderEntity : BaseEntity<string>
	{
		[NotMapped]
		public override string Id { get => ASNNo; set => ASNNo = value; }

		[Key]
		public string ASNNo { get; set; }

		public string ASNType { get; set; }

		public string ASNStatus { get; set; }

		public string CustomerID { get; set; }

		public DateTime ASNCreationTime { get; set; }

		public DateTime? ExpectedArriveTime1 { get; set; }

		public DateTime? ExpectedArriveTime2 { get; set; }

		/// <summary>
		/// 单号
		/// </summary>
		public string ASNReference1 { get; set; }

		public string ASNReference2 { get; set; }

		public string ASNReference3 { get; set; }

		public string ASNReference4 { get; set; }

		public string ASNReference5 { get; set; }

		public string Door { get; set; }

		public string CarrierID { get; set; }

		public string CarrierName { get; set; }

		public string CarrierAddress1 { get; set; }

		public string CarrierAddress3 { get; set; }

		public string CarrierAddress2 { get; set; }

		public string CarrierAddress4 { get; set; }

		public string CarrierCity { get; set; }

		public string CarrierProvince { get; set; }

		public string CarrierCountry { get; set; }

		public string CarrierZip { get; set; }

		public string CountryOfOrigin { get; set; }

		public string CountryOfDestination { get; set; }

		public string PlaceOfLoading { get; set; }

		public string PlaceOfDischarge { get; set; }

		public string PlaceofDelivery { get; set; }

		public string DeliveryVehicleNo { get; set; }

		public string Driver { get; set; }

		public string DeliveryTerms { get; set; }

		public string DeliveryTermsDescr { get; set; }

		public string PaymentTerms { get; set; }

		public string PaymentTermsDescr { get; set; }

		public string UserDefine1 { get; set; }

		public string UserDefine2 { get; set; }

		public string UserDefine3 { get; set; }

		public string UserDefine4 { get; set; }

		public string UserDefine5 { get; set; }

		public string Notes { get; set; }

		public string PONO { get; set; }

		public DateTime AddTime { get; set; }

		public string AddWho { get; set; }

		public DateTime EditTime { get; set; }

		public string EditWho { get; set; }

		public string CreateSource { get; set; }

		public string ByTrace_Flag { get; set; }

		public string Reserve_Flag { get; set; }

		public long? ReceiveID { get; set; }

		public string SupplierID { get; set; }

		public string Supplier_Name { get; set; }

		public string Supplier_Address1 { get; set; }

		public string Supplier_Address2 { get; set; }

		public string Supplier_Address3 { get; set; }

		public string Supplier_Address4 { get; set; }

		public string Supplier_City { get; set; }

		public string Supplier_Province { get; set; }

		public string Supplier_Country { get; set; }

		public string Supplier_ZIP { get; set; }

		public string BillingClass_Group { get; set; }

		public DateTime? EDISendTime { get; set; }

		public string H_EDI_01 { get; set; }

		public string H_EDI_02 { get; set; }

		public string H_EDI_03 { get; set; }

		public string H_EDI_04 { get; set; }

		public string H_EDI_05 { get; set; }

		public string H_EDI_06 { get; set; }

		public string H_EDI_07 { get; set; }

		public string H_EDI_08 { get; set; }

		public decimal? H_EDI_09 { get; set; }

		public decimal? H_EDI_10 { get; set; }

		public string IssuePartyID { get; set; }

		public string IssuePartyName { get; set; }

		public string I_Address1 { get; set; }

		public string I_Address2 { get; set; }

		public string I_Address3 { get; set; }

		public string I_Address4 { get; set; }

		public string I_City { get; set; }

		public string I_Province { get; set; }

		public string I_Country { get; set; }

		public string I_Zip { get; set; }

		public string DeliveryVehicleType { get; set; }

		public DateTime? LastReceivingTime { get; set; }

		public string EDISendFlag { get; set; }

		public string BillingID { get; set; }

		public string BillingName { get; set; }

		public string B_Address1 { get; set; }

		public string B_Address2 { get; set; }

		public string B_Address3 { get; set; }

		public string B_Address4 { get; set; }

		public string B_City { get; set; }

		public string B_Province { get; set; }

		public string B_Country { get; set; }

		public string B_ZIP { get; set; }

		public string CarrierContact { get; set; }

		public string CarrierMail { get; set; }

		public string CarrierFax { get; set; }

		public string CarrierTel1 { get; set; }

		public string CarrierTel2 { get; set; }

		public string Supplier_Contact { get; set; }

		public string Supplier_Mail { get; set; }

		public string Supplier_Fax { get; set; }

		public string Supplier_Tel1 { get; set; }

		public string Supplier_Tel2 { get; set; }

		public DateTime? EDISendTime2 { get; set; }

		public DateTime? EDISendTime3 { get; set; }

		public DateTime? EDISendTime4 { get; set; }

		public DateTime? EDISendTime5 { get; set; }

		public string B_Contact { get; set; }

		public string B_Mail { get; set; }

		public string B_Fax { get; set; }

		public string B_Tel1 { get; set; }

		public string B_Tel2 { get; set; }

		public string I_Contact { get; set; }

		public string I_Mail { get; set; }

		public string I_Fax { get; set; }

		public string I_Tel1 { get; set; }

		public string I_Tel2 { get; set; }

		public string UserDefine6 { get; set; }

		public string ASN_Print_Flag { get; set; }

		public string Return_Print_Flag { get; set; }

		public string QCStatus { get; set; }

		public string UserDefine7 { get; set; }

		public string UserDefine8 { get; set; }

		public string PackMaterialConsume { get; set; }

		public string ReleaseStatus { get; set; }

		public string WarehouseID { get; set; }

		public string Priority { get; set; }

		public string zonegroup { get; set; }

		public DateTime? MedicalXMLTime { get; set; }

		public string SerialNoCatch { get; set; }

		public string FollowUp { get; set; }

		public string UserDefineA { get; set; }

		public string UserDefineB { get; set; }

		public string ARCHIVEFLAG { get; set; }

		public DateTime? ActualArriveTime { get; set; }
	}
}
