﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
	[Table("DOC_Wave_Header")]
    public class FluxDocWaveHeaderEntity : BaseEntity<string>
    {
		[NotMapped]
		public override string Id { get => this.WaveNo; set => this.WaveNo = value; }
		/// <summary>
		/// 波次编号
		/// </summary>
		[Key]
		public string WaveNo { get; set; }
		/// <summary>
		/// 状态
		/// </summary>
		public string WaveStatus { get; set; }

		public string Descr { get; set; }

		public string UserDefine1 { get; set; }

		public string UserDefine2 { get; set; }

		public string UserDefine3 { get; set; }

		public string UserDefine4 { get; set; }

		public string UserDefine5 { get; set; }

		public decimal? GrossWeight { get; set; }

		public decimal? Cubic { get; set; }

		public string ReleaseStatus { get; set; }

		public string WarehouseID { get; set; }

		public DateTime AddTime { get; set; }

		public string AddWho { get; set; }

		public DateTime EditTime { get; set; }

		public string EditWho { get; set; }

		public string customerid { get; set; }

		public string TaskDispatch { get; set; }

		public string CrossArea { get; set; }

		public string TaskStatus { get; set; }

		public string WaveRule { get; set; }

		public string CarrierID { get; set; }

		public int? GroupNumber { get; set; }

		public string SortationType { get; set; }

		public string UDFPrintFlag1 { get; set; }

		public string UDFPrintFlag2 { get; set; }

		public string UDFPrintFlag3 { get; set; }

		public string WorkingArea { get; set; }

		public string WaveDispatchID { get; set; }

		public string WaveGroupNo { get; set; }

		public string WaveGroupPrintFlag { get; set; }

		public string AssortingID { get; set; }
	}
}
