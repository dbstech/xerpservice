﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MagicLamp.PMS.Entities
{
    /// <summary>
    /// 按批次查询
    /// </summary>
    [Table("INV_LOT")]
    public class FluxInvLotEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id
        {
            get { return LotNum; }
            set { LotNum = value; }
        }
        [Key]
        /// <summary>
        /// 批次号
        /// </summary>
        public string LotNum { get; set; }
        /// <summary>
        /// 货主
        /// </summary>
        public string CustomerID { get; set; }
        /// <summary>
        /// 产品
        /// </summary>
        public string SKU { get; set; }
        /// <summary>
        /// 冻结数
        /// </summary>
        public decimal QtyOnHold { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public decimal? Qty { get; set; }
        /// <summary>
        /// 毛重
        /// </summary>
        public decimal GrossWeight { get; set; }
        /// <summary>
        /// 净重
        /// </summary>
        public decimal NetWeight { get; set; }
        /// <summary>
        /// 体积
        /// </summary>
        public decimal Cubic { get; set; }
        /// <summary>
        /// 价值
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 预配数量
        /// </summary>
        public decimal? QtyPreAllocated { get; set; }
        /// <summary>
        /// 分配数量
        /// </summary>
        public decimal? QtyAllocated { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime AddTime { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string AddWho { get; set; }
        /// <summary>
        /// 编辑时间
        /// </summary>
        public DateTime EditTime { get; set; }
        /// <summary>
        /// 编辑人
        /// </summary>
        public string EditWho { get; set; }
    }
}
