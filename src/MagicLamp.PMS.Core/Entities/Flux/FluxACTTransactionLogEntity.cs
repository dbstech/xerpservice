﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MagicLamp.PMS.Entities
{
    [Table("ACT_Transaction_Log")]
    public class FluxACTTransactionLogEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id
        {
            get { return this.TransactionID; }
            set { this.TransactionID = value; }
        }


        [Column("AddTime")]
        public DateTime AddTime { get; set; }

        [StringLength(35)]
        [Column("AddWho")]
        public string AddWho { get; set; }

        [StringLength(20)]
        [Column("CallModule")]
        public string CallModule { get; set; }

        [StringLength(20)]
        [Column("CallWorkStation")]
        public string CallWorkStation { get; set; }

        [Column("DocLineNo")]
        public int? DocLineNo { get; set; }

        [StringLength(20)]
        [Column("DocNo")]
        public string DocNo { get; set; }

        [StringLength(10)]
        [Column("DocType")]
        public string DocType { get; set; }

        [Column("EDICancelTransactionTime")]
        public DateTime? EDICancelTransactionTime { get; set; }

        [StringLength(1)]
        [Column("EdiSendFlag")]
        public string EdiSendFlag { get; set; }

        [Column("EDITransactionTime")]
        public DateTime? EDITransactionTime { get; set; }

        [Column("EditTime")]
        public DateTime EditTime { get; set; }

        [StringLength(35)]
        [Column("EditWho")]
        public string EditWho { get; set; }

        [StringLength(30)]
        [Column("FMCustomerID")]
        public string FMCustomerID { get; set; }

        [StringLength(30)]
        [Column("FMID")]
        public string FMID { get; set; }

        [StringLength(20)]
        [Column("FMLocation")]
        public string FMLocation { get; set; }

        [StringLength(10)]
        [Column("FMLotNum")]
        public string FMLotNum { get; set; }

        [StringLength(40)]
        [Column("FMPackID")]
        public string FMPackID { get; set; }

        [Column("FMQty")]
        public decimal? FMQty { get; set; }

        [Column("FMQty_Each")]
        public decimal? FMQty_Each { get; set; }

        [StringLength(50)]
        [Column("FMSKU")]
        public string FMSKU { get; set; }

        [StringLength(10)]
        [Column("FMUOM")]
        public string FMUOM { get; set; }

        [StringLength(35)]
        [Column("Operator")]
        public string Operator { get; set; }

        [StringLength(1)]
        [Column("PA_Flag")]
        public string PA_Flag { get; set; }

        [Column("PA_Sequence")]
        public int? PA_Sequence { get; set; }

        [StringLength(10)]
        [Column("PA_TaskID")]
        public string PA_TaskID { get; set; }

        [StringLength(1)]
        [Column("QC_Flag")]
        public string QC_Flag { get; set; }

        [StringLength(1)]
        [Column("QC_Sequence")]
        public string QC_Sequence { get; set; }

        [StringLength(10)]
        [Column("QC_TaskID")]
        public string QC_TaskID { get; set; }

        [StringLength(60)]
        [Column("Reason")]
        public string Reason { get; set; }

        [StringLength(2)]
        [Column("ReasonCode")]
        public string ReasonCode { get; set; }

        [StringLength(2)]
        [Column("Status")]
        public string Status { get; set; }

        [StringLength(30)]
        [Column("ToCustomerID")]
        public string ToCustomerID { get; set; }

        [StringLength(30)]
        [Column("ToID")]
        public string ToID { get; set; }

        [StringLength(20)]
        [Column("ToLocation")]
        public string ToLocation { get; set; }

        [StringLength(10)]
        [Column("ToLotnum")]
        public string ToLotnum { get; set; }

        [StringLength(40)]
        [Column("ToPackID")]
        public string ToPackID { get; set; }

        [Column("ToQty")]
        public decimal? ToQty { get; set; }

        [Column("ToQty_Each")]
        public decimal? ToQty_Each { get; set; }

        [StringLength(50)]
        [Column("ToSku")]
        public string ToSku { get; set; }

        [Column("TotalCubic")]
        public decimal? TotalCubic { get; set; }

        [Column("TotalGrossWeight")]
        public decimal TotalGrossWeight { get; set; }

        [Column("TotalNetWeight")]
        public decimal TotalNetWeight { get; set; }

        [Column("TotalPrice")]
        public decimal? TotalPrice { get; set; }

        [StringLength(10)]
        [Column("ToUOM")]
        public string ToUOM { get; set; }

        [Key]
        [StringLength(10)]
        [Column("TransactionID")]
        public string TransactionID { get; set; }

        [Column("TransactionTime")]
        public DateTime? TransactionTime { get; set; }

        [StringLength(2)]
        [Column("TransactionType")]
        public string TransactionType { get; set; }

        [StringLength(100)]
        [Column("UserDefine1")]
        public string UserDefine1 { get; set; }

        [StringLength(100)]
        [Column("UserDefine2")]
        public string UserDefine2 { get; set; }

        [StringLength(100)]
        [Column("UserDefine3")]
        public string UserDefine3 { get; set; }

        [StringLength(100)]
        [Column("UserDefine4")]
        public string UserDefine4 { get; set; }

        [StringLength(100)]
        [Column("UserDefine5")]
        public string UserDefine5 { get; set; }

        [StringLength(30)]
        [Column("WarehouseID")]
        public string WarehouseID { get; set; }
    }
}
