﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MagicLamp.PMS.Entities
{
    [Table("BAS_SKU")]
    public class FluxBasSkuEntity : BaseEntity<string>
    {

        [NotMapped]
        public override string Id
        {
            get { return $"{this.SKU}$multi-key${this.CustomerID}"; }
        }

        [StringLength(1)]
        [Column("Active_Flag")]
        public string Active_Flag { get; set; }

        [Column("AddTime")]
        public DateTime AddTime { get; set; }

        [StringLength(35)]
        [Column("AddWho")]
        public string AddWho { get; set; }

        [StringLength(20)]
        [Column("AllocationRule")]
        public string AllocationRule { get; set; }

        [StringLength(1)]
        [Column("AllowReceiving")]
        public string AllowReceiving { get; set; }

        [StringLength(1)]
        [Column("AllowShipment")]
        public string AllowShipment { get; set; }

        [StringLength(100)]
        [Column("Alternate_SKU1")]
        public string Alternate_SKU1 { get; set; }

        [StringLength(100)]
        [Column("Alternate_SKU2")]
        public string Alternate_SKU2 { get; set; }

        [StringLength(100)]
        [Column("Alternate_SKU3")]
        public string Alternate_SKU3 { get; set; }

        [StringLength(100)]
        [Column("Alternate_SKU4")]
        public string Alternate_SKU4 { get; set; }

        [StringLength(100)]
        [Column("Alternate_SKU5")]
        public string Alternate_SKU5 { get; set; }

        [StringLength(10)]
        [Column("Alternative_PutawayZone1")]
        public string Alternative_PutawayZone1 { get; set; }

        [StringLength(10)]
        [Column("Alternative_PutawayZone2")]
        public string Alternative_PutawayZone2 { get; set; }

        [StringLength(200)]
        [Column("ApprovalNo")]
        public string ApprovalNo { get; set; }

        [StringLength(1)]
        [Column("BreakCS")]
        public string BreakCS { get; set; }

        [StringLength(1)]
        [Column("BreakEA")]
        public string BreakEA { get; set; }

        [StringLength(1)]
        [Column("BreakIP")]
        public string BreakIP { get; set; }

        [StringLength(20)]
        [Column("CartonGroup")]
        public string CartonGroup { get; set; }

        [Column("CAS_ULD_PHR")]
        public int? CAS_ULD_PHR { get; set; }

        [StringLength(10)]
        [Column("CHK_SCN_UOM")]
        public string CHK_SCN_UOM { get; set; }

        [StringLength(1)]
        [Column("CopyPackIDToLotAtt12")]
        public string CopyPackIDToLotAtt12 { get; set; }

        [Column("Cube")]
        public decimal? Cube { get; set; }

        [StringLength(30)]
        [Column("CustomerID")]
        public string CustomerID { get; set; }

        [StringLength(1)]
        [Column("CycleClass")]
        public string CycleClass { get; set; }

        [StringLength(10)]
        [Column("DefaultCartonType")]
        public string DefaultCartonType { get; set; }

        [StringLength(10)]
        [Column("DefaultHold")]
        public string DefaultHold { get; set; }

        [StringLength(10)]
        [Column("DefaultReceivingUOM")]
        public string DefaultReceivingUOM { get; set; }

        [StringLength(10)]
        [Column("DefaultShipmentUOM")]
        public string DefaultShipmentUOM { get; set; }

        [StringLength(30)]
        [Column("DefaultSupplierID")]
        public string DefaultSupplierID { get; set; }

        [StringLength(200)]
        [Column("Descr_C")]
        public string Descr_C { get; set; }

        [StringLength(200)]
        [Column("Descr_E")]
        public string Descr_E { get; set; }

        [Column("EditTime")]
        public DateTime EditTime { get; set; }

        [StringLength(35)]
        [Column("EditWho")]
        public string EditWho { get; set; }

        [Column("FirstInboundDate")]
        public DateTime? FirstInboundDate { get; set; }

        [StringLength(1)]
        [Column("FirstOP")]
        public string FirstOP { get; set; }

        [StringLength(20)]
        [Column("FreightClass")]
        public string FreightClass { get; set; }

        [Column("GrossWeight")]
        public decimal? GrossWeight { get; set; }

        [StringLength(5)]
        [Column("Hazard_Flag")]
        public string Hazard_Flag { get; set; }

        [StringLength(15)]
        [Column("HSCode")]
        public string HSCode { get; set; }

        [StringLength(200)]
        [Column("ImageAddress")]
        public string ImageAddress { get; set; }

        [Column("InboundLifeDays")]
        public int? InboundLifeDays { get; set; }

        [StringLength(20)]
        [Column("InboundSerialNoQtyControl")]
        public string InboundSerialNoQtyControl { get; set; }

        [StringLength(1)]
        [Column("InvChgWithShipment")]
        public string InvChgWithShipment { get; set; }

        [StringLength(1)]
        [Column("KitFlag")]
        public string KitFlag { get; set; }

        [Column("LastCycleCount")]
        public DateTime? LastCycleCount { get; set; }

        [Column("LastMaintenanceDate")]
        public DateTime? LastMaintenanceDate { get; set; }

        [StringLength(10)]
        [Column("LotID")]
        public string LotID { get; set; }

        [StringLength(20)]
        [Column("MaintenanceReason")]
        public string MaintenanceReason { get; set; }

        [StringLength(15)]
        [Column("MedicalType")]
        public string MedicalType { get; set; }

        [StringLength(1)]
        [Column("MedicineSpecicalControl")]
        public string MedicineSpecicalControl { get; set; }

        [Column("NetWeight")]
        public decimal? NetWeight { get; set; }

        [Column("NOTES")]
        public string NOTES { get; set; }

        [StringLength(1)]
        [Column("OneStepAlllocation")]
        public string OneStepAlllocation { get; set; }

        [StringLength(1)]
        [Column("OneStepAllocation")]
        public string OneStepAllocation { get; set; }

        [StringLength(100)]
        [Column("OrderBySQL")]
        public string OrderBySQL { get; set; }

        [Column("OutboundLifeDays")]
        public int OutboundLifeDays { get; set; }

        [StringLength(20)]
        [Column("OutboundSerialNoQtyControl")]
        public string OutboundSerialNoQtyControl { get; set; }

        [Column("OverRcvPercentage")]
        public decimal? OverRcvPercentage { get; set; }

        [StringLength(1)]
        [Column("OverReceiving")]
        public string OverReceiving { get; set; }

        [StringLength(40)]
        [Column("PackID")]
        public string PackID { get; set; }

        [Column("Price")]
        public decimal? Price { get; set; }

        [StringLength(1)]
        [Column("PrintMedicineQcReport")]
        public string PrintMedicineQcReport { get; set; }

        [StringLength(20)]
        [Column("PutawayLocation")]
        public string PutawayLocation { get; set; }

        [StringLength(20)]
        [Column("PutawayRule")]
        public string PutawayRule { get; set; }

        [StringLength(10)]
        [Column("PutawayZone")]
        public string PutawayZone { get; set; }

        [StringLength(20)]
        [Column("QCRULE")]
        public string QCRULE { get; set; }

        [Column("QCTime")]
        public decimal? QCTime { get; set; }

        [Column("QtyMax")]
        public decimal? QtyMax { get; set; }

        [Column("QtyMin")]
        public decimal? QtyMin { get; set; }

        [Column("ReorderQty")]
        public decimal? ReorderQty { get; set; }

        [StringLength(20)]
        [Column("ReplenishRule")]
        public string ReplenishRule { get; set; }

        [StringLength(10)]
        [Column("ReportUOM")]
        public string ReportUOM { get; set; }

        [StringLength(2)]
        [Column("ReserveCode")]
        public string ReserveCode { get; set; }

        [StringLength(200)]
        [Column("ReservedField01")]
        public string ReservedField01 { get; set; }

        [StringLength(200)]
        [Column("ReservedField02")]
        public string ReservedField02 { get; set; }

        [StringLength(200)]
        [Column("ReservedField03")]
        public string ReservedField03 { get; set; }

        [StringLength(200)]
        [Column("ReservedField04")]
        public string ReservedField04 { get; set; }

        [StringLength(200)]
        [Column("ReservedField05")]
        public string ReservedField05 { get; set; }

        [StringLength(200)]
        [Column("ReservedField06")]
        public string ReservedField06 { get; set; }

        [StringLength(200)]
        [Column("ReservedField07")]
        public string ReservedField07 { get; set; }

        [StringLength(200)]
        [Column("ReservedField08")]
        public string ReservedField08 { get; set; }

        [StringLength(200)]
        [Column("ReservedField09")]
        public string ReservedField09 { get; set; }

        [StringLength(200)]
        [Column("ReservedField10")]
        public string ReservedField10 { get; set; }

        [StringLength(200)]
        [Column("ReservedField11")]
        public string ReservedField11 { get; set; }

        [StringLength(200)]
        [Column("ReservedField12")]
        public string ReservedField12 { get; set; }

        [StringLength(200)]
        [Column("ReservedField13")]
        public string ReservedField13 { get; set; }

        [StringLength(200)]
        [Column("ReservedField14")]
        public string ReservedField14 { get; set; }

        [StringLength(200)]
        [Column("ReservedField15")]
        public string ReservedField15 { get; set; }

        [StringLength(30)]
        [Column("ReservedField16")]
        public string ReservedField16 { get; set; }

        [StringLength(30)]
        [Column("ReservedField17")]
        public string ReservedField17 { get; set; }

        [StringLength(30)]
        [Column("ReservedField18")]
        public string ReservedField18 { get; set; }

        [StringLength(10)]
        [Column("RotationID")]
        public string RotationID { get; set; }

        [StringLength(1)]
        [Column("ScanWhenCasePicking")]
        public string ScanWhenCasePicking { get; set; }

        [StringLength(1)]
        [Column("ScanWhenCheck")]
        public string ScanWhenCheck { get; set; }

        [StringLength(1)]
        [Column("ScanWhenINVScan")]
        public string ScanWhenINVScan { get; set; }

        [StringLength(1)]
        [Column("ScanWhenMove")]
        public string ScanWhenMove { get; set; }

        [StringLength(1)]
        [Column("ScanWhenPack")]
        public string ScanWhenPack { get; set; }

        [StringLength(1)]
        [Column("ScanWhenPiecePicking")]
        public string ScanWhenPiecePicking { get; set; }

        [StringLength(1)]
        [Column("ScanWhenPutaway")]
        public string ScanWhenPutaway { get; set; }

        [StringLength(1)]
        [Column("ScanWhenQC")]
        public string ScanWhenQC { get; set; }

        [StringLength(1)]
        [Column("ScanWhenReceive")]
        public string ScanWhenReceive { get; set; }

        [StringLength(1)]
        [Column("ScanWhenShip")]
        public string ScanWhenShip { get; set; }

        [StringLength(1)]
        [Column("SecondSerialNoCatch")]
        public string SecondSerialNoCatch { get; set; }

        [StringLength(1)]
        [Column("SerialNoCatch")]
        public string SerialNoCatch { get; set; }

        [Column("ShelfLife")]
        public int? ShelfLife { get; set; }

        [Column("ShelfLifeAlertDays")]
        public int? ShelfLifeAlertDays { get; set; }

        [StringLength(1)]
        [Column("ShelfLifeFlag")]
        public string ShelfLifeFlag { get; set; }

        [StringLength(1)]
        [Column("ShelfLifeType")]
        public string ShelfLifeType { get; set; }

        [StringLength(50)]
        [Column("SKU")]
        public string SKU { get; set; }

        [StringLength(100)]
        [Column("SKU_Group1")]
        public string SKU_Group1 { get; set; }

        [StringLength(100)]
        [Column("SKU_Group2")]
        public string SKU_Group2 { get; set; }

        [StringLength(100)]
        [Column("SKU_Group3")]
        public string SKU_Group3 { get; set; }

        [StringLength(100)]
        [Column("SKU_Group4")]
        public string SKU_Group4 { get; set; }

        [StringLength(100)]
        [Column("SKU_Group5")]
        public string SKU_Group5 { get; set; }

        [StringLength(100)]
        [Column("SKU_Group6")]
        public string SKU_Group6 { get; set; }

        [StringLength(100)]
        [Column("SKU_Group7")]
        public string SKU_Group7 { get; set; }

        [StringLength(100)]
        [Column("SKU_Group8")]
        public string SKU_Group8 { get; set; }

        [StringLength(100)]
        [Column("SKU_Group9")]
        public string SKU_Group9 { get; set; }

        [Column("SKUHigh")]
        public decimal SKUHigh { get; set; }

        [Column("SKULength")]
        public decimal SKULength { get; set; }

        [Column("SKUWidth")]
        public decimal SKUWidth { get; set; }

        [StringLength(1)]
        [Column("SN_ASN_QTY")]
        public string SN_ASN_QTY { get; set; }

        [StringLength(1)]
        [Column("SN_SO_QTY")]
        public string SN_SO_QTY { get; set; }

        [StringLength(20)]
        [Column("SoftAllocationRule")]
        public string SoftAllocationRule { get; set; }

        [StringLength(1)]
        [Column("SpecialMaintenance")]
        public string SpecialMaintenance { get; set; }

        [Column("Tare")]
        public decimal? Tare { get; set; }

        [StringLength(10)]
        [Column("TariffID")]
        public string TariffID { get; set; }
    }
}
