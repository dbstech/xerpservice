﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("INV_LOT_ATT")]
    public class FluxInvLotAttEntity : BaseEntity<string>
    {
        [NotMapped] 
        public override string Id
        {
            get => this.LotNum;
            set => this.LotNum = value;
        }

        [Key]
        public string LotNum { get; set; }

        public string CustomerID { get; set; }

        public string SKU { get; set; }

        public string LotAtt01 { get; set; }

        public string LotAtt02 { get; set; }

        public string LotAtt03 { get; set; }

        public string LotAtt04 { get; set; }

        public string LotAtt05 { get; set; }

        public string LotAtt06 { get; set; }

        public string LotAtt07 { get; set; }

        public string LotAtt08 { get; set; }

        public string LotAtt09 { get; set; }

        public string LotAtt10 { get; set; }

        public string LotAtt11 { get; set; }

        public string LotAtt12 { get; set; }

        public DateTime? ReceivingTime { get; set; }

        public string QCReportFileName { get; set; }

        public DateTime AddTime { get; set; }

        public string AddWho { get; set; }

        public DateTime EditTime { get; set; }

        public string EditWho { get; set; }
	}
}
