﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("RUL_Rotation_Details")]
    public class FluxStockAllocationRule : BaseEntity<string>
    {


        [NotMapped]
        public override string Id
        {
            get { return $"{this.RotationID}$multi-key${this.LotAttName}"; }
        }

        //[Key]
        //[Column(Order = 1)]
        public string RotationID { get; set; }

        //[Key]
        //[Column(Order = 2)]
        public string LotAttName { get; set; }
        public int SequenceNo { get; set; }
        public string Sorting { get; set; }
        public DateTime EditTime { get; set; }
        /// <summary>
        /// 匹配模式
        /// --Y  精确匹配--N 模糊匹配--C 优先匹配
        /// </summary>
        public string MatchValue { get; set; }
    }

}
