﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    public class BaseEntity<T> : Entity<T>
    {
        public override bool IsTransient()
        {
            return false;
        }

    }
}
