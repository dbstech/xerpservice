﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("Product_Brands")]
    public class ProductBrandEntity : BaseEntity<int>
    {
        public string EnBrand { get; set; }
        public string CnBrand { get; set; }
        public string BrandImg { get; set; }
        public string BrandImgM { get; set; }
        public string BrandCode { get; set; }
    }
}
