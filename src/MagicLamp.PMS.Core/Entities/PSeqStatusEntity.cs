using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MagicLamp.PMS.Entities
{
    public class PSeqStatusEntity : BaseEntity<int>
    {

        [NotMapped]
        public override int Id
        {
            get { return pSeq; }
            set { pSeq = value; }
        }

        [Key]        
        [Column("Pseq")]
        public int pSeq { get; set; }
        [Column("IsExport")]
        public bool isExport { get; set; }
        [Column("UpdateTime")]
        public DateTime updateTime { get; set; }
    }

}