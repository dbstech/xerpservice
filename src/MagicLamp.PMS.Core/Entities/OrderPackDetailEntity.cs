﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("orders_pack_detail")]
    public class OrderPackDetailEntity : BaseEntity<int>
    {

        [NotMapped]
        public override int Id
        {
            get { return opdid; }
            set { opdid = value; }
        }

        [Key]
        public int opdid { get; set; }
        public int? oid { get; set; }

        public int? opid { get; set; }
        public string sku { get; set; }
        public string product_name { get; set; }
        public string enname { get; set; }
        public int qty { get; set; }
        public int? inputqty { get; set; }
        public decimal? price { get; set; }
        public decimal? weight { get; set; }
        public decimal? taxrate { get; set; }
        public decimal? total_w { get; set; }
        public decimal? total_t { get; set; }
        public int flag { get; set; }
        public string notes { get; set; }
        public string barcode { get; set; }
        public int status { get; set; }
        public int? pda { get; set; }
        public int? csflag { get; set; }
        public bool? oversale { get; set; }
    }
}
