using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MagicLamp.PMS.Entities
{
    [Table("custom_kind")]
    public class CustomKindEntity : BaseEntity<string>
    {
        [NotMapped]        
        public override string Id
        {
            get { return kind; }
            set { kind = value; }
        }
        [Key]
        public string kind { get; set; }
        public string name { get; set; }
        public string notes { get; set; }

        [Column("lmt_tax")]
        public decimal? lmtTax { get; set; }

        [Column("lmt_weight")]
        public decimal? lmtWeight { get; set; }

        [Column("lmt_qty")]
        public int? lmtQty { get; set; }

        [Column("lmt_qty_grp")]
        public int? lmtQtyGrp { get; set; }
        public int? lv { get; set; }
    }
}