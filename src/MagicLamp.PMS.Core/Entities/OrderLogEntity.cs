﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("orders_log")]
    public class OrderLogEntity : BaseEntity<int>
    {
        [NotMapped]
        public override int Id { get { return OLID; } set { OLID = value; } }

        /// <summary>
        /// order log id
        /// </summary>
        [Key]
        [Column("olid")]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int OLID { get; set; }

        /// <summary>
        /// order id
        /// </summary>
        [Column("oid")]
        public int? OID { get; set; }

        [Column("opid")]
        public int? OPID { get; set; }

        [Column("opt_date")]
        public DateTime OptDate { get; set; }

        [Column("comments")]
        public string Comments { get; set; }

        /// <summary>
        /// Operator ID
        /// </summary>
        [Column("opt_id")]
        public Guid OptID { get; set; }

        /// <summary>
        /// Operator Name
        /// </summary>
        [Column("opt_name")]
        public string OptName { get; set; }
        public string picpath { get; set; }
        public int flag { get; set; }

        public string notes1 { get; set; }
        public string notes2 { get; set; }
        public string notes3 { get; set; }

        [Column("rel_flag_val")]
        public int? relFlagVal { get; set; }
        [Column("rel_flag_title")]
        public string relFlagTitle { get; set; }

        [Column("rel_flag1_val")]
        public int? relFlag1Val { get; set; }
        [Column("rel_flag1_title")]
        public string relFlag1Title { get; set; }

        [Column("rel_flag2_val")]
        public int? relFlag2Val { get; set; }
        [Column("rel_flag2_title")]
        public string relFlag2Title { get; set; }
        
    }
}
