﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Auditing;
namespace MagicLamp.PMS.Entities
{
    [Audited]
    [Table("Product", Schema = "BAS")]
    public class ProductEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id { get { return SKU; } set { SKU = value; } }

        [Key]
        public string SKU { get; set; }
        public string CustomerID { get; set; }
        /// <summary>
        /// 激活
        /// </summary>
        public bool? ActiveFlag { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        public int? WarehouseID { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string ChineseName { get; set; }
        /// <summary>
        /// 英文名称
        /// </summary>
        public string EnglishName { get; set; }
        public decimal? Price { get; set; }
        private decimal? _cost;
        public decimal? Cost
        {
            get
            {
                return _cost;
            }
            set
            {
                _cost = value.HasValue ? Math.Round(value.Value, 2) : value;
            }
        }
        /// <summary>
        /// 自定义成本
        /// </summary>
        public decimal? DefaultCost { get; set; }
        /// <summary>
        /// 成本分组
        /// </summary>
        public string CostType { get; set; }
        /// <summary>
        /// 币种
        /// </summary>
        public string Currency { get; set; }
        /// <summary>
        /// 毛重
        /// </summary>
        public decimal? GrossWeight { get; set; }
        /// <summary>
        /// 净重
        /// </summary>
        public decimal? NetWeight { get; set; }
        /// <summary>
        /// 长
        /// </summary>
        public decimal? Length { get; set; }
        /// <summary>
        /// 宽
        /// </summary>
        public decimal? Wide { get; set; }
        /// <summary>
        /// 高
        /// </summary>
        public decimal? High { get; set; }
        /// <summary>
        /// 库存数量
        /// </summary>
        public int Qty { get; set; }
        public int WarehouseStockFlag { get; set; }
        /// <summary>
        /// 产品供应链分类ID
        /// </summary>
        public int CategoryID { get; set; }
        /// <summary>
        /// 默认发货渠道
        /// </summary>
        public string DefaultFreightID { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        public string Note { get; set; }
        /// <summary>
        /// 条码1
        /// </summary>
        public string Barcode1 { get; set; }
        /// <summary>
        /// 条码2
        /// </summary>
        public string Barcode2 { get; set; }
        /// <summary>
        /// 条码3
        /// </summary>
        public string Barcode3 { get; set; }
        /// <summary>
        /// 条码4
        /// </summary>
        public string Barcode4 { get; set; }
        /// <summary>
        /// 税率
        /// </summary>
        public decimal? TaxRate { get; set; }
        /// <summary>
        /// 包装材料
        /// </summary>
        public int MaterialsID { get; set; }
        public decimal? MinThreshold { get; set; }
        public decimal? MaxThreshold { get; set; }
        /// <summary>
        /// 分单分类
        /// </summary>
        public int? SplitOrderCategoryID { get; set; }

        /// <summary>
        ///采购账期code
        /// </summary>
        public string PurchasePeriodCode { get; set; }

        private DateTime _createTime;
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreateTime
        {
            get
            {
                if (_createTime == DateTime.MinValue)
                {
                    _createTime = DateTime.UtcNow;
                }
                return _createTime;
            }
            set
            {
                if (value != DateTime.MinValue)
                {
                    _createTime = value;
                }
            }
        }
        public string CreateUser { get; set; }

        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 品牌1
        /// </summary>
        // public string Brand1 { get; set; }


        public string Keywords { get; set; }
        public bool? IsHHPurchase { get; set; }
        public long? OdooProductID { get; set; }
        public long? OdooUserID { get; set; }
        // private string _hHSKU;
        public string HHSKU
        { get; set; }

        // {
        //     get
        //     {
        //         return _hHSKU;
        //     }
        //     set
        //     {
        //         _hHSKU = value.Trim();
        //     }
        // }
        public string Specs { get; set; }
        public string SupplierBarcode { get; set; }
        public string TrackingType { get; set; }
        public string HSCode { get; set; }
        [ForeignKey("BWCountry")]
        public int? CountryId { get; set; }
        public virtual BWCountryEntity Country { get; set; }
        public string Producer { get; set; }
        public int? BoxQty { get; set; }
        public long? OdooCategoryID { get; set; }
        public int? LifeTime { get; set; }
        public int? AlertTime { get; set; }
        public virtual BrandEntity Brand { get; set; }
        public bool? SyncToOdoo { get; set; }
        public string PersonInCharge { get; set; }
        public string OdooSyncStatus { get; set; }
        public string PersonInChargeID { get; set; }
        public long? OdooBrandID { get; set; }
        public int? TakeTime { get; set; }
        [ForeignKey("Brand")]
        public int? BrandId { get; set; }

        public int? CustomsUnitID { get; set; }
        public decimal? CustomsUnitAmount { get; set; }

        public string GetBrandName()
        {
            if (Brand != null)
            {
                return Brand.ChineseName;
            }
            return null;
        }
        public string BondedWarehouseItemCode { get; set; }

        public string ECinxSKU { get; set; }
        public bool? SyncToEcinx { get; set; }
    }
}