﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Auditing;

namespace MagicLamp.PMS.Entities
{
    [Table("api_stocks")]
    public class APIStockEntity : BaseEntity<string>
    {

        [NotMapped]
        [Audited]
        public override string Id { get { return SKU; } set { SKU = value; } }

        [Key]
        public string SKU { get; set; }
        /// <summary>
        /// 阿拉丁库存
        /// </summary>
        public int Qty_aladdin { get; set; }
        /// <summary>
        /// 实际库存
        /// </summary>
        public int? Qty { get; set; }
        /// <summary>
        /// 可用库存（阿拉丁库存--锁定库存）
        /// </summary>
        public int Qty_available { get; set; }
        public int? Qty_baseline { get; set; }
        public int? Qty_heweb { get; set; }
        /// <summary>
        /// 锁定库存
        /// </summary>
        public int Qty_locked { get; set; }
        /// <summary>
        /// 是否需要同步到销售端
        /// </summary>
        public bool? Syc_flag { get; set; }
        [Column("updatedataTime")]
        public DateTime? Updated_time { get; set; }

        public string Expired_date { get; set; }

        //public string wanted_notes { get; set; }
        
        /// <summary>
        /// HH库存数量
        /// </summary>
        public int? Qty_hh { get; set; }
        /// <summary>
        /// HH保质期
        /// </summary>
        public string Expiry_date_hh { get; set; }
        [Column("en")]
        public string ProductNameEN { get; set; }
        [Column("cn")]
        public string ProductNameCN { get; set; }
        public int? Flag { get; set; }

        [Column("sku_flag")]
        public int? SkuFlag { get; }

        [Column("qty_cache")]
        public int? Qty_cache { get; set; }

        [Column("qty_orders")]
        public int? Qty_orders { get; set; }

        [Column("qty_orders_pack")]
        public int? Qty_orders_pack { get; set; }

        [Column("Status")]
        public short? Status { get; set; }
    }
}
