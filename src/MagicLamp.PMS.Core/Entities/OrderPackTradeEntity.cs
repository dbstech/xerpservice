﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Auditing;

namespace MagicLamp.PMS.Entities
{
    [Audited]
    [Table("OrdersPackTrade")]
    public class OrderPackTradeEntity: BaseEntity<int>
    {
        [NotMapped]
        public override int Id { get { return Opid; } set { Opid = value; } }

        [Key]
        public int Opid { get; set; }

        public string OrderID { get; set; }

        public string OrderPackNo { get; set; }

        /// <summary>
        /// custom response
        /// </summary>
        public string CustomsPaymentData { get; set; }

        
        public DateTime CreateTime { get; set; }

        public bool? HasBeenOrdered { get; set; }
        /// <summary>
        /// original GMCreateOrderInput
        /// </summary>
        public string OriginalInfo { get; set; }
    }
}
