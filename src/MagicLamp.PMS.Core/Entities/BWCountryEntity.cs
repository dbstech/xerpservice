using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("BWCountry", Schema = "dbo")]
    public class BWCountryEntity : BaseEntity<int>
    {
        [NotMapped]
        public override int Id { get { return Code; } set { Code = value; } }

        [Key]
        public int Code { get; set; }

        public string CountryName { get; set; }
    }
}
