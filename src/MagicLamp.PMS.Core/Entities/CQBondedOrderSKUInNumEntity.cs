﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("CQBondedOrderSKUInNum")]
    public class CQBondedOrderSKUInNumEntity : BaseEntity<long>
    {
        public string OrderID { get; set; }

        public string GMSKU { get; set; }

        public int Qty { get; set; }

        public string InNum { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
