﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("PoOrderline", Schema = "PO")]
    public class PurchaseOrderLineEntity : BaseEntity<int>
    {
        [NotMapped]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override int Id
        {
            get
            {
                return PoLineId;
            }
            set { PoLineId = value; }
        }

        [Key]
        public int PoLineId { get; set; }
        public string PoHeaderId { get; set; }
        /// <summary>
        /// 采购订单行类型 2:产品;3:服务;4:礼品 -- 此字段暂不使用和维护
        /// </summary>
        public string OrderLineType { get; set; }

        [Column("ItemId")]
        /// <summary>
        /// SKU编号
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// 产品说明
        /// </summary>
        public string ItemDescription { get; set; }
        /// <summary>
        /// 采购数量
        /// </summary>
        public decimal QTY { get; set; }

        /// <summary>
        /// 币种
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string UOM { get; set; }

        [Column("UnitPrice")]
        /// <summary>
        /// 不含税成本
        /// </summary>
        public decimal Cost { get; set; }

        [Column("UnitPriceTax")]
        /// <summary>
        /// 含税成本
        /// </summary>
        public decimal? TaxedCost { get; set; }

        /// <summary>
        /// 税率
        /// </summary>
        public decimal? TaxRate { get; set; }

        /// <summary>
        /// WMS接收数量
        /// </summary>
        public decimal? RecieveQTY { get; set; }

        public int? LineStatus { get; set; }

        /// <summary>
		/// 创建人
		/// </summary>
		[Description("创建人")]
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        [Description("创建日期")]
        public DateTime? CreateDate { get; set; }

        /// <summary>
        /// 最后更新人
        /// </summary>
        [Description("最后更新人")]
        public string LastUpdateBy { get; set; }

        /// <summary>
        /// 最后更新日期
        /// </summary>
        [Description("最后更新日期")]
        public DateTime? LastUpdateDate { get; set; }

        /// <summary>
        /// 客户
        /// </summary>
        public string OrgId { get; set; }

        public string WareHouse { get; set; }

        /// <summary>
        /// 接收时间
        /// </summary>
        public DateTime? ReceiveDate { get; set; }

        /// <summary>
        /// 供应链生成批次编号(lot12)
        /// </summary>
        public string LotNo { get; set; }

        /// <summary>
        /// 批次所属渠道
        /// </summary>
        public string LotHolder { get; set; }
    }
}
