﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("OrderHeader", Schema = "SO")]
    public class OrderHeaderEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id { get { return OrderID; } set { OrderID = value; } }

        [Key]
        public string OrderID { get; set; }
        
        public string Store { get; set; }
        public string RecipientName { get; set; }
        public string RecipientPhone { get; set; }
        public string RecipientProvince { get; set; }
        public string RecipientCity { get; set; }
        public string RecipientDistrict { get; set; }
        public string RecipientTown { get; set; }
        public string IDNumber { get; set; }
        public string SenderName { get; set; }
        public string SenderAddreess { get; set; }
        public string SenderPhone { get; set; }
        public string Notes { get; set; }
        public string Status { get; set; }
        public decimal? Fee { get; set; }
        /// <summary>
        /// 上游订单创建时间
        /// </summary>
        public DateTime? OrderCreateTime { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? SplitTime { get; set; }
        public string BusinessName { get; set; }
    }
}
