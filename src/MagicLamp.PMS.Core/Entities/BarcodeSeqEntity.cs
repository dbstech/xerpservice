﻿using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS
{
	[Table("barcodes_seq")]
    public class BarcodeSeqEntity : BaseEntity<int>
    {
		[NotMapped]
		public override int Id
		{
			get { return seq; }
			set { seq = value; }
		}

		[Key]
		public int seq { get; set; }

		public long? stno { get; set; }

		public long? edno { get; set; }

		public long? last_used { get; set; }

		public DateTime? optdate { get; set; }

		public string optid { get; set; }

		public string freightid { get; set; }

		public string flag { get; set; }

		public string prefix { get; set; }

		public string postfix { get; set; }

		public int? size { get; set; }

		public int? freightset { get; set; }
	}
}
