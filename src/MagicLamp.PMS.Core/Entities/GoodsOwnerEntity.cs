﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("GoodsOwner", Schema = "BAS")]
    public class GoodsOwnerEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id { get { return Code; } set { Code = value; } }   

        [Key]
        public string Code { get; set; }

        public string FluxCode { get; set; }
        public string UserId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreationTime { get; set; } = DateTime.UtcNow;
    }
}
