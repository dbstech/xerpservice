﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Auditing;


namespace MagicLamp.PMS.Entities
{
    [Audited]
    [Table("EcinxPushOrderLog")]
    public  class EcinxPushOrderLogEntity : BaseEntity<int>
    {
        [NotMapped]
        public override int Id { get; set; }
        [Key]
        public string OrderPackNo { get; set; }
        public int WarningLevel { get; set; }
        public int LackStockDays { get; set; }
        public string MessageType { get; set; }
        public string FaildReason { get; set; }
        public string MessageDetail { get; set; }
        [NotMapped]
        public DateTime Cdate { get; set; }

    }
}
