
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace MagicLamp.PMS.Entities
{
    [Table("ExchangeRate")]
    public class ExchangeRate : Entity<string>
    {
        [NotMapped]
        public override string Id { 
            get { return From + To; }
            set {} 
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string From { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string To { get; set; }
        public decimal Rate { get; set; }

        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

    }
}
