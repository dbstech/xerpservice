﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("ProductSalesBusiness")]
    public class ProductSalesBusinessEntity : BaseEntity<int>
    {

        public string BusinessName { get; set; }
        public string SKU { get; set; }
        public bool Active { get; set; }
        public DateTime CreationTime { get; set; }

    }
}
