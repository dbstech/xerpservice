﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Auditing;


namespace MagicLamp.PMS.Entities
{
    [Audited]
    [Table("EcinxOrdersPackTrade")]
    public  class EcinxOrdersPackTradeEntity : BaseEntity<int>
    {
        [NotMapped]
        public override int Id { get { return Opid; } set { Opid = value; } }
        [Key]
        public int Opid { get; set; }
        public string OrderPackNo { get; set; }
        /// <summary>
        /// custom response
        /// </summary>
        public string CustomsPaymentData { get; set; }
        public DateTime CreateTime { get; set; }
        public string OrderID { get; set; }
        /// <summary>
        /// // 0: Created Ecinx Success 1: Ordered Ecinx Success 2: Ordered Ecinx Failed
        /// </summary>
        public int Status { get; set; }
        public string DeliveryCode { get; set; }
        public int? ExportFlag { get; set; }
        public string EcinxResponse { get; set; }
        /// <summary>
        /// original GMCreateOrderInput
        /// </summary>
        public string OriginalInfo { get; set; }

        public DateTime PayTime { get; set; }
        public string EcinxOrderCode { get; set; }

    }
}
