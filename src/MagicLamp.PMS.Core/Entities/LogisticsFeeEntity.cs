﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("LogisticsFee", Schema = "Courier")]
   public class LogisticsFeeEntity : BaseEntity<long>
    {
        public string CourierName { get; set; }
        /// <summary>
        /// 货主
        /// </summary>
        public string CustomerName { get; set; }
        /// <summary>
        /// 运单号
        /// </summary>
        public string CourierNO { get; set; }

        /// <summary>
        /// 计费重量（KG）
        /// </summary>
        public decimal? Weight { get; set; }
        /// <summary>
        /// 结算金额
        /// </summary>
        public decimal? Fee { get; set; }
        /// <summary>
        /// 结算货币
        /// </summary>
        public string CurrencyType { get; set; }
        /// <summary>
        /// 结算日期
        /// </summary>
        public DateTime? SettlementDate { get; set; }
        /// <summary>
        /// 结算账单编号
        /// </summary>
        public string SettlementAccount { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? Updated_at { get; set; } = System.DateTime.UtcNow;
        public string InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
    }
}
