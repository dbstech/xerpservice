﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("Brand", Schema = "BAS")]
    public class BrandEntity : BaseEntity<int>
    {
        public int Id { get; set; }        
        public string ShortCode { get; set; }
        public bool ActiveFlag { get; set; }
        public string ChineseName { get; set; }
        public string EnglishName { get; set; }
        public string Basis { get; set; }

        private DateTime _createTime;
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreateTime
        {
            get
            {
                if (_createTime == DateTime.MinValue)
                {
                    _createTime = DateTime.UtcNow;
                }
                return _createTime;
            }
            set
            {
                if (value != DateTime.MinValue)
                {
                    _createTime = value;
                }
            }
        }
        public string CreateUser { get; set; }

        public long? OdooBrandId { get; set; } = null;
        public int? BrandCategoryId { get; set; }
        public virtual BrandCategoryEntity BrandCategory { get; set; }
        public string GetBrandCategoryName()
        {
            if (BrandCategory != null)
            {
                return BrandCategory.Name;
            }
            return null;
        }
        public string OdooSyncStatus { get; set; }
    }
}
