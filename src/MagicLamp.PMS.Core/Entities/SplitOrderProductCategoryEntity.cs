﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("SO.ProductCategory")]
    public class SplitOrderProductCategoryEntity : BaseEntity<int>
    {
        [NotMapped]
        public override int Id { get { return CategoryId; } set { CategoryId = value; } }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoryId { get; set; }

        public string CNName { get; set; }

    }
}
