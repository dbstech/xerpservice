﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("Image", Schema = "BAS")]
    public class ImageEntity : BaseEntity<long>
    {
        [NotMapped]
        public override long Id { get { return ImageId; } set { ImageId = value; } }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ImageId { get; set; }

        public string ImageType { get; set; }

        public string ReferenceId { get; set; }

        public string ReferenceType { get; set; }

        public string OriginalPath { get; set; }
        public string UserId { get; set; }

        public int Sort { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreationTime { get; set; } = DateTime.UtcNow;
    }
}
