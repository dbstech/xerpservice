﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("OrderLine", Schema = "SO")]
    public class OrderLineEntity : BaseEntity<int>
    {
        [NotMapped]
        public override int Id { get { return OrderLineID; } set { OrderLineID = value; } }

        [Key]
        public int OrderLineID { get; set; }

        public string OrderID { get; set; }
        public string SKU { get; set; }
        public string CustomerID { get; set; }
        public string ChineseName { get; set; }
        public string EnglishName { get; set; }
        public decimal? Qty { get; set; }
        public bool? Oversale { get; set; }
    }
}
