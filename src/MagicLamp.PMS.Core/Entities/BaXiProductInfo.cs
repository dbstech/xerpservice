using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace MagicLamp.PMS.Entities
{
    [Table("BaXiProductInfo")]
    public class BaXiProductInfo : Entity<int>, IHasCreationTime
    {
        public string Upc { get; set; }
        // public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal? PreviousPrice { get; set; }
        public int Stock { get; set; }
        public virtual DateTime CreationTime { get; set; }
        public DateTime? LastModified { get; set; }
        public BaXiProductInfo()
        {
            CreationTime = DateTime.UtcNow;
        }
    }
}
