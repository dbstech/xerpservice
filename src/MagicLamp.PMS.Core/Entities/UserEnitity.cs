﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("Users")]
    public class UserEnitity : BaseEntity<Guid>
    {
        [NotMapped]
        public override Guid Id
        {
            get { return UserId; }
            set { UserId = value; }
        }
        
        [Key]
        public Guid UserId { get; set; }
        public Guid ApplicationId { get; set; } 
        public string UserName { get; set; }
    }
}
