using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace MagicLamp.PMS.Entities
{
    [Table("pack_rules")]
    public class PackRuleEntity : BaseEntity<int>
    {
        [NotMapped]
        public override int Id
        {
            get { return pid; }
            set { pid = value; }
        }
        [Key]
        [Column("pid")]
        public int pid { get; set; }
        [Column("cpcode_head")]
        public string cpCodeHead { get; set; }
        [Column("key1")]
        public string key1 { get; set; }
        [Column("key2")]
        public string key2 { get; set; }
        [Column("qty")]
        public int? qty { get; set; }
        [Column("lvl")]
        public int? lvl { get; set; }
        [Column("exportflag")]
        public int? exportFlag { get; set; }
        [Column("ckind")]
        public string cKind { get; set; }
    }
}