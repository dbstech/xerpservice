﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("SyncProductCostPrice")]
    public class SyncProductCostPriceEntity : BaseEntity<long>
    {
		[ForeignKey("Product")]
        public string SKU { get; set; }
        /// <summary>
        /// 原价格
        /// </summary>
        public decimal BeforePrice { get; set; }
        /// <summary>
        /// 现价格
        /// </summary>
        public decimal AfterPrice { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreationTime { get; set; }
        /// <summary>
        /// 差价
        /// </summary>
        public decimal? PriceGap { get; set; }

        public virtual ProductEntity Product { get; set; }

        public string GetProductName()
        {
            if (Product != null)
            {
                return Product.ChineseName;
            }
            return null;
        }

        public string GetBarcode()
        {
            if (Product != null)
            {
                return Product.Barcode1;
            }
            return null;            
        }
        public string GetBrandShortCode()
        {
            if (Product != null && Product.Brand != null)
            {
                return Product.Brand.ShortCode;
            }
            return null;            
        }

        public string GetBrand()
        {
            if (Product != null && Product.Brand != null)
            {
                return Product.Brand.ChineseName;
            }
            return null;
        }
    }
}
