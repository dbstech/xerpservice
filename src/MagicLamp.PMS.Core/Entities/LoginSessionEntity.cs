using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("LoginSession", Schema = "dbo")]
    public class LoginSessionEntity : BaseEntity<Guid>
    {
        [NotMapped]
        public override Guid Id
        {
            get { return SessionId; }
            set { SessionId = value; }
        }

        [Key]
        public Guid SessionId { get; set; }
        public string UserInfo { get; set; }
        public string UserID { get; set; }
        public string LoginMethod { get; set; }
        public DateTime ExpiryTime { get; set; }
        public DateTime LastActivity { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreationTime { get; set; } = DateTime.UtcNow;
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] 
        public bool Active { get; set; }

    }
}
