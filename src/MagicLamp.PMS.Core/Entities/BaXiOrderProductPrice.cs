using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace MagicLamp.PMS.Entities
{
    [Table("BaXiOrderProductPrice")]
    public class BaXiOrderProductPrice : Entity<int>, IHasCreationTime
    {
        [NotMapped]
        public override int Id { get => Opdid; set => Opdid = value; }
        [Key] 
        public int Opdid { get; set; }
        public int Opid { get; set; }
        public string Sku { get; set; }
        public string Upc { get; set; }
        public decimal Price { get; set; }
        public int Qty { get; set; }
        public virtual DateTime CreationTime { get; set; }
        public BaXiOrderProductPrice()
        {
            CreationTime = DateTime.UtcNow;
        }
    }
}
