﻿using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("OrderPacks_WMSQueue")]
    public class OrderPackWMSQueueEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id { get => OrderPackNo; set => OrderPackNo = value; }
        /// <summary>
        /// 包裹号
        /// </summary>
        [Key]
        public string OrderPackNo { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 条目创建时间
        /// </summary>
        public DateTime? CreationTime { get; set; }
        /// <summary>
        /// 条目更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
        public int Opid { get; set; }
        public string Orderid { get; set; }
        public string Partner { get; set; }
        /// <summary>
        /// 富勒接口返回信息
        /// </summary>
        public string ReturnMessageFromHH { get; set; }
        /// <summary>
        /// HH接口返回信息
        /// </summary>
        public string ReturnMessageFromFlux { get; set; }
    }
}
