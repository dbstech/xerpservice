﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MagicLamp.PMS.Entities
{
    [Table("Orders_SKU_Lock")]
    public class OrderSkuLock : BaseEntity<int>
    {
        public string Orderid { get; set; }

        public string SKU { get; set; }

        public int Qty { get; set; }

        public int? LockDuration { get; set; }

        public DateTime? Ctime { get; set; }

        //public DateTime? UnLockTime { get; set; }
    }


}
