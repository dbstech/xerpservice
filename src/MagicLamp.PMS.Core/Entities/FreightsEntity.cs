﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("freights")]
    public class FreightsEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id
        {
            get { return id; }
            set { id = value; }
        }

        [Key]
        public string id { get; set; }
        public string name { get; set; }
        public string notes { get; set; }
        public string prefix { get; set; }
        public int? code { get; set; }
        public int? size { get; set; }
        [InverseProperty("Freights")]
        public virtual FreightsSetsEntity FreightsSets { get; set; }
        public string GetFreightSet()
        {
            if (FreightsSets != null)
            {
                return FreightsSets.Keyword;
            }
            return null;
        }
        public string CurrencyCode { get; set; }
        public decimal CostPerKg { get; set; }
        public decimal PricePerKg { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreightSetName { get; set; }
        public bool? Active { get; set; }
    }
}
