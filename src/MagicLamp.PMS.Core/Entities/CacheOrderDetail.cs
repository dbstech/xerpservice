﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MagicLamp.PMS.Entities
{
    [Table("cache_orders_detail")]
    public class CacheOrderDetail : BaseEntity<int>
    {
        public string OrderID { get; set; }
        public string SKU { get; set; }
        public int Qty { get; set; }
        public DateTime? CTime { get; set; }
        [Column("product_name")]
        public string ProductName { get; set; }
        public bool? Oversale { get; set; }
    }


}
