﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{

    [Table("AppConfig")]
    public class APIConfigEntity : BaseEntity<string>
    {

        [NotMapped]
        public override string Id
        {
            get
            {
                return ConfigKey;
            }
            set { ConfigKey = value; }
        }

        [Key]
        public string ConfigKey { get; set; }
        public string ConfigValue { get; set; }

    }


}
