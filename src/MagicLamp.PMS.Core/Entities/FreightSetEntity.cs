using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MagicLamp.PMS.Entities
{
    [Table("FreightSet", Schema = "SO")]
    public class FreightSetEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id { get { return Name; } set { Name = value; } }

        [Key]
        public string Name { get; set; }
        public string Logo { get; set; }
        public string Tel { get; set; }
        public string Website { get; set; }
    }
}
