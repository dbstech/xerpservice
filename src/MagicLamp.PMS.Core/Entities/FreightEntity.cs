﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("freight", Schema = "SO")]
    public class FreightEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id { get { return FreightID; } set { FreightID = value; } }

        [Key]
        public string FreightID { get; set; }

        public string Name { get; set; }
        public string FreightSet { get; set; }

    }
}
