﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("OrderPacks_ReservedHHSKU")]
    public class OrderPackReservedHHSKUEntity : BaseEntity<long>
    {

        /// <summary>
        /// 包裹号
        /// </summary>
        public string OrderPackNo { get; set; }
        /// <summary>
        /// SKU
        /// </summary>
        public string SKU { get; set; }
        /// <summary>
        /// 价格成本
        /// </summary>
        public decimal? Price { get; set; }
        /// <summary>
        /// 订单时间
        /// </summary>
        public DateTime? CreationTime { get; set; }
        /// <summary>
        /// 合作方
        /// </summary>
        public string Partner { get; set; }
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 出库时间
        /// </summary>
        public DateTime? OutboundTime { get; set; }
        public int? Qty { get; set; }
    }
}
