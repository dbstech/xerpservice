﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("orders_pack_exportflag")]
    public class ExportFlagEntity : BaseEntity<int>
    {
        [NotMapped]
        public override int Id
        {
            get { return Flag; }
            set { Flag = value; }
        }

        [Key]
        public int Flag { get; set; }
        public string Name { get; set; }
    }
}
