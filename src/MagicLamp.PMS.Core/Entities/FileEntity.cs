﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [Table("File", Schema = "BAS")]
    public class FileEntity : BaseEntity<long>
    {
        [NotMapped]
        public override long Id { get { return FileId; } set { FileId = value; } }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long FileId { get; set; }

        public string FileType { get; set; }

        public string ReferenceId { get; set; }

        public string ReferenceType { get; set; }

        public string OriginalPath { get; set; }
        public string UserId { get; set; }

        public int Sort { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime CreationTime { get; set; } = DateTime.UtcNow;
    }
}
