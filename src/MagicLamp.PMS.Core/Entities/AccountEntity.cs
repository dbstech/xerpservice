using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MagicLamp.PMS.Entities
{
    [Table("User", Schema = "dbo")]
    public class AccountEntity : BaseEntity<string>
    {
        [NotMapped]
        public override string Id
        {
            get { return UserID; }
            set { UserID = value; }
        } 

        [Key]
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public char ActiveFlag { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public int ErroPasswordTimes { get; set; }
        public string RoleID { get; set; }
        public string Email { get; set; }
        public string Belong { get; set; }
        public string DingTalkNick { get; set; }
        
        [Column("DingTalkUnionid")]
        public string DingTalkUnionId { get; set; }
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long XerpUserId { get; set; }
    }
}
