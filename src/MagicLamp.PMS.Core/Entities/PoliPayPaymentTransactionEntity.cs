using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CsvHelper.Configuration.Attributes;
using System;

namespace MagicLamp.PMS.Entities
{
    public class PoliPayPaymentTransactionEntity : BaseEntity<long>
    {
        [NotMapped]
        [Ignore]
        public override long Id
        {
            get { return PoliId; }
            set { PoliId = value; }
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Name("POLi ID")]
        public long PoliId { get; set; }
        [Name("Txn Date")]
        public string TxnDate { get; set; }
        [Name("Completion Date")]
        public string CompletionDate { get; set; }
        [Name("Status")]
        public string Status { get; set; }
        [Name("Bank")]
        public string Bank { get; set; }   
        [Name("Merchant Data")]
        public string MerchantData { get; set; }  
        [Name("Merchant Reference")]
        public string MerchantReference { get; set; }  
        [Name("Amount")]
        public decimal Amount { get; set; } 
        [Name("Customer Reference")] 
        public string CustomerReference { get; set; }
        [Name("POLiLink URL")]
        public string PoliLinkUrl { get; set; }
        [Ignore]
        public DateTime? CreatedDate { get; set; }
        [Ignore]
        public DateTime? UpdatedDate { get; set;}
    }
}