﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
	[Table("OrderPacks_OutOfStock")]
    public class OrderPackOutOfStockEntity : BaseEntity<long>
    {
		/*[NotMapped]
		public override long Id { get => ID; set => ID = value; }

		[Key]
		public long ID { get; set; }*/


		/// <summary>
		/// 包裹号
		/// </summary>
		public string OrderPackNo { get; set; }
		/// <summary>
		/// SKU
		/// </summary>
		public string SKU { get; set; }
		/// <summary>
		/// 缺的库存数量
		/// </summary>
		public int LackQty { get; set; }
		/// <summary>
		/// 状态
		/// </summary>
		public string Status { get; set; }
		/// <summary>
		/// 条目创建时间
		/// </summary>
		public DateTime? CreationTime { get; set; }
		/// <summary>
		/// 条目更新时间
		/// </summary>
		public DateTime? UpdateTime { get; set; }
		public bool? Oversale { get; set; }
	}
}
