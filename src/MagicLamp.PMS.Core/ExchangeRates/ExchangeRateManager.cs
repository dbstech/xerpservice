using Abp.Domain.Repositories;
using Abp.Runtime.Caching;
using MagicLamp.PMS.Infrastructure.Common;
using MagicLamp.PMS.Entities;
using System;
using Abp.Domain.Services;

namespace MagicLamp.PMS.ExchangeRates
{
    public class ExchangeRateManager : DomainService, IExchangeRateManager
    {
        private readonly ICacheManager _cacheManager;
        private IRepository<ExchangeRate, string> _exchangeRateRepository;
        public ExchangeRateManager(
            IRepository<ExchangeRate, string> exchangeRateRepository,
            ICacheManager cacheManager
        )
        {
            _exchangeRateRepository = exchangeRateRepository;
            _cacheManager = cacheManager;
        }
        public decimal GetRate(string from, string to)
        {
            var cache = _cacheManager.GetCache(CacheKeys.EXCHANGERATE188);
            decimal? rate = cache.GetOrDefault<string, decimal?>(string.Format(CacheKeys.EXCHANGERATEFORMAT, from, to));
            if (rate.HasValue)
            {
                return rate.GetValueOrDefault();
            }
            var exchangeRate = _exchangeRateRepository.FirstOrDefault(x => x.From == from && x.To == to);
            if (exchangeRate != null)
            {
                cache.Set(string.Format(CacheKeys.EXCHANGERATEFORMAT, from, to), exchangeRate.Rate, TimeSpan.FromHours(24));
                return exchangeRate.Rate;
            }
            throw new Exception("Unable to find exchange rate");
        }


        public decimal ConvertTo(string from, string to, decimal value)
        {
            decimal rate = GetRate(from, to);
            return value * rate;
        }
    }
}