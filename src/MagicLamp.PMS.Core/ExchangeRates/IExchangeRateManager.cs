using Abp.Domain.Services;
using System.Security.Principal;

namespace MagicLamp.PMS.ExchangeRates
{
    public interface IExchangeRateManager : IDomainService
    {
        decimal GetRate(string from, string to);
        decimal ConvertTo(string from, string to, decimal value);
    }
}