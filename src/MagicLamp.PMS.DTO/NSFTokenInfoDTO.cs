﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    /// <summary>
    /// Token information
    /// </summary>
    public class NSFTokenInfoDTO
    {
        public string access_token { get; set; }
        public string expires_in { get; set; }
    }
}
