using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(FreightsSetsDTO))]
    public class FreightsSetsDTO : EntityDto<int>
    {
        public string FreightId { get; set; }
        public int FreightSet { get; set; }
        public string Keyword { get; set; }
        public string Valflag { get; set; }
        public decimal? Price { get; set; }
        public decimal? Cost { get; set; }
    }
}
