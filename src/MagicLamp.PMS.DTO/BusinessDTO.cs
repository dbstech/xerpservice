﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(BusinessEntity))]
    public class BusinessDTO : EntityDto<int>
    {
        public int bizID { get; set; }
        public string bizName { get; set; }
    }
}
