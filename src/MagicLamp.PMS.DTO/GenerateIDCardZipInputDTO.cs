﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [Serializable]
    public class GenerateIDCardZipInputDTO
    {

        public DateTime UpdateBeginTime { get; set; }
        public DateTime EndBeginTime { get; set; }
        public int ExportFlag { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
        public string CourierID { get; set; }
        public string OrderID { get; set; }
        public string IDCard { get; set; }
        public Guid TaskID { get; set; }

        public int? Verification { get; set; }

        public string UserID { get; set; }

        public string FreightID { get; set; }
        public DateTime OrderBeginTime { get; set; }
        public DateTime OrderEndBeginTime { get; set; }

    }


}
