﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class LockSKUWhenReservingJobInputDTO
    {
        public string OrderID { get; set; }
        public int? SecondsReserved { get; set; }
        public List<SimpleSKUWithFlagDTO> ListSKU { get; set; }
        public List<string> ListOrderSKU { get; set; }
    }
}
