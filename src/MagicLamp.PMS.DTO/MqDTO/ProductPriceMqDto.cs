﻿using MagicLamp.PMS.DTOs.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class ProductPriceMqDto
    {

        public string SKU { get; set; }
        /// <summary>
        /// 产品价格
        /// </summary>
        public decimal? ProductPrice { get; set; }
        /// <summary>
        /// 操作费用
        /// </summary>
        public decimal? DropShippingPrice { get; set; }
        public string BusinessCode { get; set; }

        public string Currency { get; set; }
        public string Type { get; set; }
    }
}
