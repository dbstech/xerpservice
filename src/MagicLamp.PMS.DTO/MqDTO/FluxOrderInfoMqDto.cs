﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class FluxOrderInfoMqDto
    {
        /// <summary>
        /// 来源单据号
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 总重量
        /// </summary>
        public string Weight { get; set; }
        /// <summary>
        /// 预留字段
        /// </summary>
        public string Udf1 { get; set; }
    }
}
