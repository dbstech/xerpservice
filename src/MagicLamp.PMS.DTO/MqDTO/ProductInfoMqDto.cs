﻿using MagicLamp.PMS.DTOs.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class ProductInfoMqDto
    {

        public ProductDTO Before { get; set; }
        public ProductDTO After { get; set; }
        public MqActionEnum Action { get; set; }
    }
}
