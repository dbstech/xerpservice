﻿using MagicLamp.PMS.DTOs.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.MqDTO
{
    public class BaseUpdateMqDTO<T> where T : class
    {

        public T Before { get; set; }
        public T After { get; set; }
        public MqActionEnum Action { get; set; }

    }

}
