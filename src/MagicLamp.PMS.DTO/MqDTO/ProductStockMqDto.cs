﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class ProductStockMqDto
    {

        public string Type { get; set; }

        public string Data { get; set; }

    }
}
