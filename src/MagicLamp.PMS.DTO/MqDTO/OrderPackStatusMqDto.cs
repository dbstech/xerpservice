﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class OrderPackStatusMqDto
    {
        //OrderID: "1111111",
        //OrderPackNo: "1111111-1",
        //OrderCreateDate: "",
        //CourierCode: "nsf",
        //CourierName: "新顺丰",
        //ParcelTrackingNo: "1111111",

        public string OrderID { get; set; }
        public string OrderPackNo { get; set; }
        public DateTime OrderCreateDate { get; set; }
        public string CourierCode { get; set; }
        public string CourierName { get; set; }
        /// <summary>
        /// 包裹快递单号
        /// </summary>
        public string ParcelTrackingNo { get; set; }
        /// <summary>
        /// 订单出库时间
        /// </summary>
        public DateTime? OrderDeliveryDate { get; set; }
        /// <summary>
        /// 包裹配货时间
        /// </summary>
        public DateTime? ParcelPickDate { get; set; }
        /// <summary>
        /// 包裹打包时间
        /// </summary>
        public DateTime? ParcelPackDate { get; set; }
        public string ParcelStatus { get; set; }
        public string ParcelStatusDesc { get; set; }
        public string OrderStatus { get; set; }
        public string OrderStatusDesc { get; set; }
        public int bizId { get; set; }
    }
}
