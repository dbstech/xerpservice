﻿using MagicLamp.PMS.DTOs.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.MqDTO
{
    public class IDCardUploadMqDto
    {

        public string OrderPackNo { get; set; }
        public List<string> TrackNos { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverPhone { get; set; }
        public string IDCardNo { get; set; }
        public string PhotoFront { get; set; }
        public string PhotoRear { get; set; }
        public FreightCodeEnum FreightCode { get; set; }

    }


}
