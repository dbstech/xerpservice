﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{

    /// <summary>
    /// The request body of CMQ message
    /// </summary>
    public class CMQPushMsgDTO
    {

        //request smaple
        //{"TopicOwner":100015036,"topicName":"MyTopic","subscriptionName":"mysubscription","msgId":"6942316962","msgBody":"test message","publishTime":11203432}

        public int TopicOwner { get; set; }
        public string TopicName { get; set; }
        public string SubscriptionName { get; set; }
        public string MsgId { get; set; }
        public string MsgBody { get; set; }
        public long PublishTime { get; set; }

    }
}
