﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs.Flux
{
    public class FluxSyncSKUItemDTO
    {

        /// <summary>
        /// SKU
        /// </summary>
        public string SKU { get; set; }
        /// <summary>
        /// 库存数量
        /// </summary>
        public string Qty { get; set; }


        public int? _qtyNumber = null;
        [NotMapped]
        /// <summary>
        /// 库存数量
        /// </summary>
        public int QtyNumber
        {
            get
            {
                if (_qtyNumber.HasValue)
                {
                    return _qtyNumber.Value;
                }

                decimal temp;
                if (!decimal.TryParse(this.Qty, out temp))
                {
                    _qtyNumber = 0;
                    return 0;
                }

                _qtyNumber = decimal.ToInt32(temp);

                return _qtyNumber.Value;
            }
            set
            {
                _qtyNumber = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Lotatt01 { get; set; }
        /// <summary>
        /// 有效期 2022-01-01
        /// </summary>
        public string Lotatt02 { get; set; }

        private DateTime? _bestBefore = null;

        /// <summary>
        /// 转换后的有效期
        /// </summary>
        [NotMapped]
        public DateTime? BestBefore
        {
            get
            {
                if (_bestBefore == null)
                {
                    DateTime temp;
                    if (DateTime.TryParse(this.Lotatt02, out temp))
                    {
                        _bestBefore = temp;
                    }
                }
                return _bestBefore;
            }
            set
            {
                _bestBefore = value;
            }
        }


        /// <summary>
        /// 自定义有效期（非日期类型）
        /// </summary>
        [NotMapped]
        public string PredefinedExpireDate
        {
            get
            {
                return this.Lotatt04;
            }
            set
            {
                this.Lotatt04 = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Lotatt03 { get; set; }
        /// <summary>
        /// 自定义有效期（非日期类型）
        /// </summary>
        public string Lotatt04 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Lotatt05 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Lotatt06 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Lotatt07 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Lotatt08 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Lotatt09 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Lotatt10 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Lotatt11 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Lotatt12 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Udf1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Udf2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Udf3 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Udf4 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Udf5 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Udf6 { get; set; }

    }


}
