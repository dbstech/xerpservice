﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Flux
{
    public class FluxPutDBASNDataDTO
    {
        public string OrderNo { get; set; }
        public string OrderType { get; set; }
        public string CustomerID { get; set; }
        public string ASNCreationTime { get; set; }
        public string ExpectedArriveTime1 { get; set; }
        public string ExpectedArriveTime2 { get; set; }
        public string ASNReference2 { get; set; }
        public string ASNReference3 { get; set; }
        public string ASNReference4 { get; set; }
        public string ASNReference5 { get; set; }
        public string UserDefine1 { get; set; }
        public string UserDefine2 { get; set; }
        public string UserDefine3 { get; set; }
        public string UserDefine4 { get; set; }
        public string UserDefine5 { get; set; }
        public string Notes { get; set; }
        public string SupplierID { get; set; }
        public string WarehouseID { get; set; }
        public List<FluxPoLineDTO> detailsItem { get; set; }
    }

    public class FluxPoLineDTO
    {
        public string CustomerID { get; set; }
        public string SKU { get; set; }
        public decimal ExpectedQty { get; set; }
        public string UserDefine1 { get; set; }
        public string UserDefine2 { get; set; }
        public string UserDefine3 { get; set; }
        public string UserDefine4 { get; set; }
        public string UserDefine5 { get; set; }
        public string Notes { get; set; }
        /// <summary>
        /// 持有人批次属性
        /// </summary>
        public string LotAtt11 { get; set; }
        /// <summary>
        /// 采购批次
        /// </summary>
        public string LotAtt12 { get; set; }
        public string LotAtt07 { get; set; }
    }
}
