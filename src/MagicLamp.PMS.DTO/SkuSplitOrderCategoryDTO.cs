﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(SkuSplitOrderCategoryEntity))]
    public class SkuSplitOrderCategoryDTO : EntityDto<string>
    {

        public int ProductCategoryId { get; set; }

        public string SKU { get; set; }

    }
}
