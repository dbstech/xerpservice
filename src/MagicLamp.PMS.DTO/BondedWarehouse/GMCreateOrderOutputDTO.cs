﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class GMCreateOrderOutputDTO
    {

        public string Code { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }


    }


}
