﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class GMCreateOrderInputDTO
    {

        [Required]
        /// <summary>
        /// erp订单号
        /// </summary>
        public string OrderID { get; set; }
        public string OrderPackNo { get; set; }

        //[Required]
        ///// <summary>
        ///// 企业海关十位编码
        ///// </summary>
        //public string CustomCode { get; set; }


        //[Required]
        ///// <summary>
        ///// 电商企业代码
        ///// </summary>
        //public string EShopEnterpriseCode { get; set; }

        //[Required]
        ///// <summary>
        ///// 电商企业名称
        ///// </summary>
        //public string EShopEnterpriseName { get; set; }


        public string SenderName { get; set; }
        public string SenderPhone { get; set; }
        public string SenderAddress { get; set; }
        public string SenderPostCode { get; set; }
        [Required]
        public string ReceiverName { get; set; }
        [Required]
        public string ReceiverPhone { get; set; }
        public string ReceiverCountry { get; set; }
        [Required]
        public string ReceiverProvince { get; set; }
        [Required]
        public string ReceiverCity { get; set; }
        [Required]
        public string ReceiverDistrict { get; set; }
        [Required]
        public string ReceiverAddress { get; set; }
        public string ReceiverPostCode { get; set; }

        [Required]
        /// <summary>
        /// 收件人身份证号
        /// </summary>
        public string ReceiverIdNo { get; set; }

        /// <summary>
        /// 订单实付总金额
        /// </summary>
        public decimal OrderAmount { get; set; }

        /// <summary>
        /// 运费
        /// </summary>
        public decimal ShippingFee { get; set; }

        /// <summary>
        /// 会员号
        /// </summary>
        public string MembershipNo { get; set; }

        public string BuyerName { get; set; }

        public string BuyerIdNo { get; set; }

        [Required]
        /// <summary>
        /// 商品详情信息
        /// </summary>
        public List<GMGoodsDTO> GoodsItems { get; set; }

        //public string OutTradeNo { get; set; }

        public string TransactionID { get; set; }
        /// <summary>
        /// 支付参数
        /// </summary>
        public object customsPaymentData { get; set; }
        public decimal? Discount { get; set; }
        public decimal? OrderTax { get; set; }
    }


}
