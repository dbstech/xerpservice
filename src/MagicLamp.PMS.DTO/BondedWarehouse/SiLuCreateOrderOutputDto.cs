using Newtonsoft.Json;

namespace MagicLamp.PMS.DTO.BondedWarehouse
{
    public class SiLuCreateOrderOutputDto
    {
        [JsonProperty("RESULTCODE")]
        public string resultCode { get; set; }
        [JsonProperty("RESULTMESSAGE")]
        public string resultMessage { get; set; }
    }
}