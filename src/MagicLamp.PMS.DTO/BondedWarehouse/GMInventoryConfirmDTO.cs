﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace MagicLamp.PMS.DTOs.BondedWarehouse
{
    /// <summary>
    /// 清单回执请求报文
    /// </summary>
    [XmlRoot(ElementName = "InventoryReturn")]
    public class InventoryReturn
    {
        [XmlElement(ElementName = "guid")]
        public string Guid { get; set; }
        [XmlElement(ElementName = "customsCode")]
        public string CustomsCode { get; set; }
        [XmlElement(ElementName = "ebpCode")]
        public string EbpCode { get; set; }
        [XmlElement(ElementName = "ebcCode")]
        public string EbcCode { get; set; }
        [XmlElement(ElementName = "agentCode")]
        public string AgentCode { get; set; }
        [XmlElement(ElementName = "copNo")]
        public string CopNo { get; set; }
        [XmlElement(ElementName = "preNo")]
        public string PreNo { get; set; }
        [XmlElement(ElementName = "invNo")]
        public string InvNo { get; set; }
        [XmlElement(ElementName = "returnStatus")]
        public string ReturnStatus { get; set; }
        [XmlElement(ElementName = "returnTime")]
        public string ReturnTime { get; set; }
        [XmlElement(ElementName = "returnInfo")]
        public string ReturnInfo { get; set; }
    }

    /// <summary>
    /// 清单同步回执
    /// </summary>
    [XmlRoot(ElementName = "CEB622Message")]
    public class CEB622Message
    {
        [XmlElement(ElementName = "InventoryReturn")]
        public InventoryReturn InventoryReturn { get; set; }
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }
    }


}
