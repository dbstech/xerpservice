﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace MagicLamp.PMS.DTOs.BondedWarehouse
{

    /// <summary>
    /// 订单入库请求报文
    /// </summary>
    [XmlRoot(ElementName = "B2BRET")]
    public class GMOrderConfirmDTO
    {
        [XmlElement(ElementName = "MSGTYPE")]
        public string MSGTYPE { get; set; }
        [XmlElement(ElementName = "SCODE")]
        public string SCODE { get; set; }
        [XmlElement(ElementName = "BIZNO")]
        public string BIZNO { get; set; }
        [XmlElement(ElementName = "CREATENO")]
        public string CREATENO { get; set; }
        [XmlElement(ElementName = "RETN")]
        public string RETN { get; set; }
        [XmlElement(ElementName = "MSG")]
        public string MSG { get; set; }
        [XmlElement(ElementName = "OPDATE")]
        public string OPDATE { get; set; }
        [XmlElement(ElementName = "ATTMSG")]
        public string ATTMSG { get; set; }
    }


}
