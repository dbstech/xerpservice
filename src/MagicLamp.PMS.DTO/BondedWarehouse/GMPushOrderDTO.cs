﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace MagicLamp.PMS.DTOs.BondedWarehouse
{
    [XmlRoot(ElementName = "MessageHead")]
    public class MessageHead
    {
        [XmlElement(ElementName = "MessageType")]
        public string MessageType { get; set; }
        [XmlElement(ElementName = "MessageId")]
        public string MessageId { get; set; }
        [XmlElement(ElementName = "ActionType")]
        public string ActionType { get; set; }
        [XmlElement(ElementName = "MessageTime")]
        public string MessageTime { get; set; }
        [XmlElement(ElementName = "SenderId")]
        public string SenderId { get; set; }
        [XmlElement(ElementName = "ReceiverId")]
        public string ReceiverId { get; set; }
        [XmlElement(ElementName = "SenderAddress")]
        public string SenderAddress { get; set; }
        [XmlElement(ElementName = "ReceiverAddress")]
        public string ReceiverAddress { get; set; }
        [XmlElement(ElementName = "PlatFormNo")]
        public string PlatFormNo { get; set; }
        [XmlElement(ElementName = "CustomCode")]
        public string CustomCode { get; set; }
        [XmlElement(ElementName = "SeqNo")]
        public string SeqNo { get; set; }
        [XmlElement(ElementName = "Note")]
        public string Note { get; set; }
        [XmlElement(ElementName = "UserNo")]
        public string UserNo { get; set; }
        [XmlElement(ElementName = "Password")]
        public string Password { get; set; }
    }

    [XmlRoot(ElementName = "ORDER_DETAIL")]
    public class ORDER_DETAIL
    {
        /// <summary>
        /// 商品货号。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "SKU")]
        public string SKU { get; set; }

        /// <summary>
        /// 规格型号。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "GOODS_SPEC")]
        public string GOODS_SPEC { get; set; }

        /// <summary>
        /// 币制代码。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "CURRENCY_CODE")]
        public string CURRENCY_CODE { get; set; }

        /// <summary>
        /// 商品单价。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "PRICE")]
        public string PRICE { get; set; }

        /// <summary>
        /// 商品数量。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "QTY")]
        public string QTY { get; set; }

        /// <summary>
        /// 商品总价。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "GOODS_FEE")]
        public string GOODS_FEE { get; set; }

        /// <summary>
        /// 税款金额。
        /// 进口必填，出口非必填。
        /// </summary>
        [XmlElement(ElementName = "TAX_FEE")]
        public string TAX_FEE { get; set; }

        /// <summary>
        /// 原产国。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "COUNTRY")]
        public string COUNTRY { get; set; }

        /// <summary>
        /// 序号。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "G_NUM")]
        public string G_NUM { get; set; }

        /// <summary>
        /// 国际通用的商品条形码，一般由前缀部分、制造厂商代码、商品代码和校验码组成。
        /// 是否必填：否
        /// </summary>
        [XmlElement(ElementName = "BAR_CODE")]
        public string BAR_CODE { get; set; }

        /// <summary>
        /// 填写海关标准的参数代码，参照《JGS-20 海关业务代码集》- 计量单位代码。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "UNIT_CODE")]
        public string UNIT_CODE { get; set; }

        /// <summary>
        /// 备注。
        /// 是否必填：否。
        /// </summary>
        [XmlElement(ElementName = "NOTE")]
        public string NOTE { get; set; }

        /// <summary>
        /// 品名。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "GOODS_NAME")]
        public string GOODS_NAME { get; set; }

        /// <summary>
        /// 商品编码。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "HS_CODE")]
        public string HS_CODE { get; set; }

        /// <summary>
        /// 第二单位。
        /// 是否必填：否。
        /// </summary>
        [XmlElement(ElementName = "UNIT2")]
        public string UNIT2 { get; set; }

        /// <summary>
        /// 第二数量。
        /// 是否必填：否。
        /// </summary>
        [XmlElement(ElementName = "QTY2")]
        public string QTY2 { get; set; }

        /// <summary>
        /// 贸易国。
        /// 是否必填：否。
        /// </summary>
        [XmlElement(ElementName = "TRADE_COUNTRY")]
        public string TRADE_COUNTRY { get; set; }
    }

    [XmlRoot(ElementName = "ORDER_HEAD")]
    public class ORDER_HEAD
    {
        /// <summary>
        /// 申报海关代码。
        /// 必填。（海关参数填报）
        /// </summary>
        [XmlElement(ElementName = "CUSTOMS_CODE")]
        public string CUSTOMS_CODE { get; set; }

        /// <summary>
        /// 业务类型。
        /// 必填。（直购进口：I10, 网购保税进口：I20）
        /// </summary>
        [XmlElement(ElementName = "BIZ_TYPE_CODE")]
        public string BIZ_TYPE_CODE { get; set; }

        /// <summary>
        /// 分拣线ID。
        /// 必填。（分拣线标识：
        /// SORTLINE01：代表寸滩空港
        /// SORTLINE02：代表重庆西永
        /// SORTLINE03：代表寸滩水港
        /// SORTLINE04：代表邮政EMS
        /// SORTLINE05：代表潍坊分拣线01
        /// SORTLINE06：代表南彭保仓分拣线
        /// SORTLINE07  这是团结村铁路那边的分拣线）
        /// </summary>
        [XmlElement(ElementName = "SORTLINE_ID")]
        public string SORTLINE_ID { get; set; }

        /// <summary>
        /// 原始订单编号。
        /// 必填。（和支付单进行关联）
        /// </summary>
        [XmlElement(ElementName = "ORIGINAL_ORDER_NO")]
        public string ORIGINAL_ORDER_NO { get; set; }

        /// <summary>
        /// 电商企业代码。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "ESHOP_ENT_CODE")]
        public string ESHOP_ENT_CODE { get; set; }

        /// <summary>
        /// 电商企业名称。
        /// 必填。（企业备案的企业全称）
        /// </summary>
        [XmlElement(ElementName = "ESHOP_ENT_NAME")]
        public string ESHOP_ENT_NAME { get; set; }

        /// <summary>
        /// 起运国。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "DESP_ARRI_COUNTRY_CODE")]
        public string DESP_ARRI_COUNTRY_CODE { get; set; }

        /// <summary>
        /// 运输方式。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "SHIP_TOOL_CODE")]
        public string SHIP_TOOL_CODE { get; set; }

        /// <summary>
        /// 收货人身份证号码。
        /// 进口必填，出口非必填。
        /// </summary>
        [XmlElement(ElementName = "RECEIVER_ID_NO")]
        public string RECEIVER_ID_NO { get; set; }

        /// <summary>
        /// 收货人姓名。
        /// 进口必填，出口非必填。
        /// </summary>
        [XmlElement(ElementName = "RECEIVER_NAME")]
        public string RECEIVER_NAME { get; set; }

        /// <summary>
        /// 收货人地址。
        /// 进口必填，出口非必填。
        /// </summary>
        [XmlElement(ElementName = "RECEIVER_ADDRESS")]
        public string RECEIVER_ADDRESS { get; set; }

        /// <summary>
        /// 收货人电话。
        /// 进口必填，出口非必填。
        /// </summary>
        [XmlElement(ElementName = "RECEIVER_TEL")]
        public string RECEIVER_TEL { get; set; }

        /// <summary>
        /// 货款总额。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "GOODS_FEE")]
        public string GOODS_FEE { get; set; }

        /// <summary>
        /// 税金总额。
        /// 进口必填，出口非必填。（包含 增值税、消费税、关税）
        /// </summary>
        [XmlElement(ElementName = "TAX_FEE")]
        public string TAX_FEE { get; set; }

        /// <summary>
        /// 毛重。
        /// 是否必填：选择。
        /// </summary>
        [XmlElement(ElementName = "GROSS_WEIGHT")]
        public string GROSS_WEIGHT { get; set; }

        /// <summary>
        /// 代理企业代码。
        /// 是否必填：选择。
        /// </summary>
        [XmlElement(ElementName = "PROXY_ENT_CODE")]
        public string PROXY_ENT_CODE { get; set; }

        /// <summary>
        /// 代理企业名称。
        /// 是否必填：选择。（代理境外电商代码）
        /// </summary>
        [XmlElement(ElementName = "PROXY_ENT_NAME")]
        public string PROXY_ENT_NAME { get; set; }

        /// <summary>
        /// 运费。
        /// 必填。（无则为0）
        /// </summary>
        [XmlElement(ElementName = "TRANSPORT_FEE")]
        public string TRANSPORT_FEE { get; set; }

        /// <summary>
        /// 仓储企业代码。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "CC_TRADE_CODE")]
        public string CC_TRADE_CODE { get; set; }

        /// <summary>
        /// 仓储企业名称。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "CC_TRADE_NAME")]
        public string CC_TRADE_NAME { get; set; }

        /// <summary>
        /// 联营电商代码。
        /// 是否必填：选择。（物流企业代码）
        /// </summary>
        [XmlElement(ElementName = "UNI_ESHOP_ENT_CODE")]
        public string UNI_ESHOP_ENT_CODE { get; set; }

        /// <summary>
        /// 联营电商名称。
        /// 是否必填：选择。（物流企业全称）
        /// </summary>
        [XmlElement(ElementName = "UNI_ESHOP_ENT_NAME")]
        public string UNI_ESHOP_ENT_NAME { get; set; }

        /// <summary>
        /// 验证类型。
        /// 进口必填，出口非必填。（进口必填，R:订购人P:支付人）
        /// </summary>
        [XmlElement(ElementName = "CHECK_TYPE")]
        public string CHECK_TYPE { get; set; }

        /// <summary>
        /// 发送企业代码。
        /// 必填。（发送报文的企业代码）
        /// </summary>
        [XmlElement(ElementName = "SEND_ENT_CODE")]
        public string SEND_ENT_CODE { get; set; }

        /// <summary>
        /// 订购人注册号。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "BUYER_REG_NO")]
        public string BUYER_REG_NO { get; set; }

        /// <summary>
        /// 订购人姓名。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "BUYER_NAME")]
        public string BUYER_NAME { get; set; }

        /// <summary>
        /// 订购人证件类型。
        /// 必填。（1=身份证，2=其他）
        /// </summary>
        [XmlElement(ElementName = "BUYER_ID_TYPE")]
        public string BUYER_ID_TYPE { get; set; }

        /// <summary>
        /// 订购人证件号码。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "BUYER_ID")]
        public string BUYER_ID { get; set; }

        /// <summary>
        /// 优惠减免金额。
        /// 必填。（无则为0）
        /// </summary>
        [XmlElement(ElementName = "DISCOUNT")]
        public string DISCOUNT { get; set; }

        /// <summary>
        /// 实际支付金额。
        /// 必填。（货款+运费+税款+保费-优惠金额，与支付保持一致）
        /// </summary>
        [XmlElement(ElementName = "ACTUAL_PAID")]
        public string ACTUAL_PAID { get; set; }

        /// <summary>
        /// 保费。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "INSURED_FEE")]
        public string INSURED_FEE { get; set; }

        /// <summary>
        /// 电商平台编码。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "EBP_CODE")]
        public string EBP_CODE { get; set; }

        /// <summary>
        /// 电商平台名称。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "EBP_NAME")]
        public string EBP_NAME { get; set; }

        /// <summary>
        /// 支付企业的海关注册登记编号。
        /// 是否必填：否。
        /// </summary>
        [XmlElement(ElementName = "PAY_CODE")]
        public string PAY_CODE { get; set; }

        /// <summary>
        /// 支付企业在海关注册登记的企业名称。
        /// 是否必填：否。
        /// </summary>
        [XmlElement(ElementName = "PAY_NAME")]
        public string PAY_NAME { get; set; }

        /// <summary>
        /// 商品批次号。
        /// 是否必填：否。
        /// </summary>
        [XmlElement(ElementName = "BATCH_NUMBERS")]
        public string BATCH_NUMBERS { get; set; }

        /// <summary>
        /// 参照国家统计局公布的国家行政区划标准填制。
        /// 是否必填：否。
        /// </summary>
        [XmlElement(ElementName = "CONSIGNEE_DISTRICT")]
        public string CONSIGNEE_DISTRICT { get; set; }

        /// <summary>
        /// 备注。
        /// 非必填。
        /// </summary>
        [XmlElement(ElementName = "NOTE")]
        public string NOTE { get; set; }

        /// <summary>
        /// 货物提单或运单的编号。
        /// 购进口必填，保税进口不必填。
        /// </summary>
        [XmlElement(ElementName = "BILL_NO")]
        public string BILL_NO { get; set; }

        /// <summary>
        /// 支付企业唯一的支付流水号。
        /// 非必填。
        /// </summary>
        [XmlElement(ElementName = "TRANSACTION_ID")]
        public string TRANSACTION_ID { get; set; }

        /// <summary>
        /// 货物进出境的运输工具的名称或运输工具编号。填报内容应与运输部门向海关申报的载货清单所列相应内容一致；同报关单填制规范。
        /// 直购进口必填，保税进口不必填。
        /// </summary>
        [XmlElement(ElementName = "TRAF_NO")]
        public string TRAF_NO { get; set; }

        /// <summary>
        /// 货物进出境的运输工具的航次编号。
        /// 直购进口必填，保税进口不必填。
        /// </summary>
        [XmlElement(ElementName = "VOYAGE_NO")]
        public string VOYAGE_NO { get; set; }

        /// <summary>
        /// 针对同一申报地海关下有多个跨境电子商务的监管场所,需要填写区分。
        /// 非必填。
        /// </summary>
        [XmlElement(ElementName = "LOCT_NO")]
        public string LOCT_NO { get; set; }

        /// <summary>
        /// 商务主管部门及其授权发证机关签发的进出口货物许可证件的编号。
        /// 是否必填：否。
        /// </summary>
        [XmlElement(ElementName = "LICENSE_NO")]
        public string LICENSE_NO { get; set; }

        /// <summary>
        /// 担保扣税的企业海关注册登记编号，只限清单的电商平台企业、电商企业、物流企业。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "ASSURE_CODE")]
        public string ASSURE_CODE { get; set; }

        /// <summary>
        /// 配送的商品信息，包括商品名称、数量等。
        /// 是否必填：否。
        /// </summary>
        [XmlElement(ElementName = "GOODSINFO")]
        public string GOODSINFO { get; set; }

        /// <summary>
        /// 施检机构代码。
        /// 非必填。
        /// </summary>
        [XmlElement(ElementName = "ORG_CODE")]
        public string ORG_CODE { get; set; }

        /// <summary>
        /// 净重。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "NET_WEIGHT")]
        public string NET_WEIGHT { get; set; }

        /// <summary>
        /// 订购人电话。
        /// 必填。
        /// </summary>
        [XmlElement(ElementName = "BUYER_TELEPHONE")]
        public string BUYER_TELEPHONE { get; set; }


        [XmlElement(ElementName = "ORDER_DETAIL")]
        public List<ORDER_DETAIL> ORDER_DETAIL { get; set; }
    }

    [XmlRoot(ElementName = "DTCFlow")]
    public class DTCFlow
    {
        [XmlElement(ElementName = "ORDER_HEAD")]
        public ORDER_HEAD ORDER_HEAD { get; set; }
    }

    [XmlRoot(ElementName = "MessageBody")]
    public class MessageBody
    {
        [XmlElement(ElementName = "DTCFlow")]
        public DTCFlow DTCFlow { get; set; }
    }

    [XmlRoot(ElementName = "DTC_Message")]
    public class DTC_Message
    {
        [XmlElement(ElementName = "MessageHead")]
        public MessageHead MessageHead { get; set; }
        [XmlElement(ElementName = "MessageBody")]
        public MessageBody MessageBody { get; set; }
    }

}
