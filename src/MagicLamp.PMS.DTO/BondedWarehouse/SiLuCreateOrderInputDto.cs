using System.Collections.Generic;
using Newtonsoft.Json;

namespace MagicLamp.PMS.DTO.BondedWarehouse
{
    public class SiLuCreateOrderInputDto
    {
        /// <summary>
        /// 登录分配授权码
        /// </summary>
        /// <value></value>
        [JsonProperty("IDCODE")]
        public string idCode { get; set; }
        /// <summary>
        /// 订单列表
        /// </summary>
        /// <value></value>
        [JsonProperty("ORDERS")]
        public List<Order> orders { get; set; }
    }
    public class Order
    {
        /// <summary>
        /// 订单号 40位的字母，数字组合，是你们的订单号,要求必须有值，而且必须唯一。
        /// </summary>
        /// <value></value>
        [JsonProperty("ORDERNUM")]
        public string orderNum { get; set; }

        /// <summary>
        /// 淘宝订单号, 非必须
        /// </summary>
        /// <value></value>
        [JsonProperty("TBNUM")]
        public string tbNum { get; set; }

        /// <summary>
        /// 非必须 是否用自主的包装箱 给值Y或者N
        /// </summary>
        /// <value></value>
        [JsonProperty("ISSELFBOX")]
        public string isSelfBox { get; set; } = "N";

        /// <summary>
        /// 淘宝买家账户, 非必须
        /// </summary>
        /// <value></value>
        [JsonProperty("TBNICK")]
        public string tbNick { get; set; }

        /// <summary>
        ///  扩展订单号, 非必须
        /// </summary>
        /// <value></value>
        [JsonProperty("SLGXID")]
        public string slgXid { get; set; }

        /// <summary>
        /// 海关条码
        /// </summary>
        /// <value></value>
        [JsonProperty("CUSTOMCODE")]
        public string customCode { get; set; }

        /// <summary>
        /// 圆通大头笔 , 如果是圆通的单据则需要有值，从圆通抓取面单号 会有此信息
        /// </summary>
        /// <value></value>
        [JsonProperty("DATOUBI")]
        public string daTouBi { get; set; }

        /// <summary>
        /// 运单号  必须有值，从快递公司抓取得到的面单号
        /// </summary>
        /// <value></value>
        [JsonProperty("EXPRESSCODE")]
        public string expressCode { get; set; }

        /// <summary>
        /// 运输公司代码 必须有值，有固定几个选项
        /// </summary>
        /// <value></value>
        [JsonProperty("EXPRESSCOMPANY")]
        public string expressCompany { get; set; }

        /// <summary>
        /// 省份, 省份名称 ，参见附录省份列表，必须相同
        /// </summary>
        /// <value></value>
        [JsonProperty("PROVINCE")]
        public string province { get; set; }

        /// <summary>
        /// 城市
        /// </summary>
        /// <value></value>
        [JsonProperty("CITY")]
        public string city { get; set; }

        /// <summary>
        ///  送货地址
        /// </summary>
        /// <value></value>
        [JsonProperty("ORDERADDRESS")]
        public string orderAddress { get; set; }

        /// <summary>
        /// 收货人
        /// </summary>
        /// <value></value>
        [JsonProperty("CONTACT")]
        public string contact { get; set; }

        /// <summary>
        /// 电话，可为空
        /// </summary>
        /// <value></value>
        [JsonProperty("PHONE")]
        public string phone { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        /// <value></value>
        [JsonProperty("MOBILE")]
        public string mobile { get; set; }

        /// <summary>
        /// 预计出库时间  格式为yyyy-MM-dd
        /// </summary>
        /// <value></value>
        [JsonProperty("PREOUTTIME")]
        public string preOutTime { get; set; }

        /// <summary>
        /// 预计到货时间 格式为yyyy-MM - dd
        /// </summary>
        /// <value></value>
        [JsonProperty("PREARRIVETIME")]
        public string preArriveTime { get; set; }

        /// <summary>
        /// 订单备注
        /// </summary>
        /// <value></value>
        [JsonProperty("COMM")]
        public string comm { get; set; }

        [JsonProperty("SKUNUMS")]
        public List<SkuNum> skuNums { get; set; }
    }

    public class SkuNum
    {
        /// <summary>
        /// SKU编码
        /// </summary>
        /// <value></value>
        [JsonProperty("SKUNUM")]
        public string skuNum { get; set; }

        /// <summary>
        /// 订货数量
        /// </summary>
        /// <value></value>
        [JsonProperty("ORDERQTY")]
        public int orderQty { get; set; }

        /// <summary>
        /// 订货金额, 总价
        /// </summary>
        /// <value></value>
        [JsonProperty("PRICE")]
        public int price { get; set; }

        /// <summary>
        /// 批号 可为空
        /// </summary>
        /// <value></value>
        [JsonProperty("LOT")]

        public string lot { get; set; }

        /// <summary>
        /// SKU备注
        /// </summary>
        /// <value></value>
        [JsonProperty("SKUCOMM")]
        public string skuComm { get; set; }

        /// <summary>
        /// 品质， 可为空，值可以是【正品】
        /// </summary>
        /// <value></value>
        [JsonProperty("QUALITY")]
        public string quality { get; set; }
    }
}