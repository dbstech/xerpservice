﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(CQBondedProductEntity))]
    public class CQBondedSKUDTO : EntityDto<string>
    {
        [Display(Name = "GMSKU(料号)")]
        public string GMSKU { get; set; }

        [Display(Name = "ERPSKU")]
        public string ERPSKU { get; set; }

        [Display(Name = "GOODS_SPEC(商品规格)")]
        public string GOODS_SPEC { get; set; }

        [Display(Name = "CURRENCY_CODE(币制代码)")]
        public string CURRENCY_CODE { get; set; }

        [Display(Name = "COUNTRY(原产国)")]
        public string COUNTRY { get; set; }

        [Display(Name = "BAR_CODE(商品条形码)")]
        public string BAR_CODE { get; set; }

        [Display(Name = "UNIT_CODE(申报单位)")]
        public string UNIT_CODE { get; set; }

        [Display(Name = "GOODS_NAME(商品名称)")]
        public string GOODS_NAME { get; set; }

        [Display(Name = "HSCODE(商品编码)")]
        public string HS_CODE { get; set; }

        [Display(Name = "VATRate")]
        public decimal VATRate { get; set; }
        
        public DateTime? CreateTime { get; set; }
        
        public DateTime? UpdateTime { get; set; }

        [Display(Name = "UNIT2(第二单位)")]
        public string UNIT2 { get; set; }

        [Display(Name = "QTY2(第二数量)")]
        public string QTY2 { get; set; }
    }
}
