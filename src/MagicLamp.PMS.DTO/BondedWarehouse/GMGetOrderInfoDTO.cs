﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class GMGetOrderInfoDTO
    {
        [Required]
        public string OrderID { get; set; }
    }
}
