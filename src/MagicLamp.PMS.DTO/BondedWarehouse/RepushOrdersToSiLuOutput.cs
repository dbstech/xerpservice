using System.Collections.Generic;

namespace MagicLamp.PMS.DTO.BondedWarehouse
{
    public class Error
    {
        public string orderId { get; set; }
        public string errorMessage { get; set; }
    }
    public class RepushOrdersToSiLuOutput
    {
        public List<Error> errors { get; set; }
    }
}