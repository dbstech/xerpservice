﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.BondedWarehouse
{
    public class GMOrderFirstExpressOutputDTO
    {
        public int RESULTCODE { get; set; }

        public string RESULTMESSAGE { get; set; }
    }
}
