﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class GMGoodsDTO
    {
        public string SKU { get; set; }
        public int Qty { get; set; }
        public decimal Price { get; set; }

    }


}
