﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.BondedWarehouse
{
    public class GMOrderFirstExpressDTO
    {
        public string IDCODE { get; set; }
        public List<GMOrderExpressDTO> ORDERS { get; set; }
    }

    public class GMOrderExpressDTO
    {
        public string ORDERNO { get; set; }

        public string EXPRESSNO { get; set; }
    }
}
