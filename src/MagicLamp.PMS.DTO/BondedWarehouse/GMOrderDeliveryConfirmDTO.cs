﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace MagicLamp.PMS.DTOs.BondedWarehouse
{


    [XmlRoot(ElementName = "ORDER_HEAD")]
    public class DELIVERYORDER_HEAD
    {
        [XmlElement(ElementName = "ORDERTIME")]
        public string ORDERTIME { get; set; }
        [XmlElement(ElementName = "CUSTOMS_CODE")]
        public string CUSTOMS_CODE { get; set; }
        [XmlElement(ElementName = "EBP_CODE")]
        public string EBP_CODE { get; set; }
        [XmlElement(ElementName = "BIZ_TYPE_CODE")]
        public string BIZ_TYPE_CODE { get; set; }
        [XmlElement(ElementName = "SORTLINE_ID")]
        public string SORTLINE_ID { get; set; }
        [XmlElement(ElementName = "ESHOP_ENT_CODE")]
        public string ESHOP_ENT_CODE { get; set; }
        [XmlElement(ElementName = "ESHOP_ENT_NAME")]
        public string ESHOP_ENT_NAME { get; set; }
        [XmlElement(ElementName = "RECEIVER_NAME")]
        public string RECEIVER_NAME { get; set; }
        [XmlElement(ElementName = "RECEIVER_ID_NO")]
        public string RECEIVER_ID_NO { get; set; }
        [XmlElement(ElementName = "RECEIVER_ADDRESS")]
        public string RECEIVER_ADDRESS { get; set; }
        [XmlElement(ElementName = "RECEIVER_TEL")]
        public string RECEIVER_TEL { get; set; }
        [XmlElement(ElementName = "GOODS_FEE")]
        public string GOODS_FEE { get; set; }
        [XmlElement(ElementName = "TAX_FEE")]
        public string TAX_FEE { get; set; }
        [XmlElement(ElementName = "TRANSPORT_FEE")]
        public string TRANSPORT_FEE { get; set; }
        [XmlElement(ElementName = "GOODS_COUNT")]
        public string GOODS_COUNT { get; set; }
        [XmlElement(ElementName = "GOODS_SUM")]
        public string GOODS_SUM { get; set; }
        [XmlElement(ElementName = "CC_TRADE_CODE")]
        public string CC_TRADE_CODE { get; set; }
        [XmlElement(ElementName = "CC_TRADE_NAME")]
        public string CC_TRADE_NAME { get; set; }
        [XmlElement(ElementName = "ORDER_NO")]
        public string ORDER_NO { get; set; }
        [XmlElement(ElementName = "ORIGINAL_ORDER_NO")]
        public string ORIGINAL_ORDER_NO { get; set; }
        [XmlElement(ElementName = "BAR_CODE")]
        public string BAR_CODE { get; set; }
        [XmlElement(ElementName = "BUYER_REG_NO")]
        public string BUYER_REG_NO { get; set; }
        [XmlElement(ElementName = "BUYER_NAME")]
        public string BUYER_NAME { get; set; }
        [XmlElement(ElementName = "BUYER_ID_TYPE")]
        public string BUYER_ID_TYPE { get; set; }
        [XmlElement(ElementName = "BUYER_ID")]
        public string BUYER_ID { get; set; }
        [XmlElement(ElementName = "DISCOUNT")]
        public string DISCOUNT { get; set; }
        [XmlElement(ElementName = "ACTUAL_PAID")]
        public string ACTUAL_PAID { get; set; }
        [XmlElement(ElementName = "INSURED_FEE")]
        public string INSURED_FEE { get; set; }
        [XmlElement(ElementName = "LOGISTICS_NO")]
        public string LOGISTICS_NO { get; set; }
        [XmlElement(ElementName = "LOGISTICS_CODE")]
        public string LOGISTICS_CODE { get; set; }
        [XmlElement(ElementName = "WL_RETINFO")]
        public string WL_RETINFO { get; set; }
    }

    [XmlRoot(ElementName = "ORDER_DETAIL")]
    public class DELIVERYORDER_DETAIL
    {
        [XmlElement(ElementName = "SKU")]
        public string SKU { get; set; }
        [XmlElement(ElementName = "GOODS_SPEC")]
        public string GOODS_SPEC { get; set; }
        [XmlElement(ElementName = "CURRENCY_CODE")]
        public string CURRENCY_CODE { get; set; }
        [XmlElement(ElementName = "PRICE")]
        public string PRICE { get; set; }
        [XmlElement(ElementName = "QTY")]
        public string QTY { get; set; }
        [XmlElement(ElementName = "GOODS_FEE")]
        public string GOODS_FEE { get; set; }
        [XmlElement(ElementName = "TAX_FEE")]
        public string TAX_FEE { get; set; }
        [XmlElement(ElementName = "COUNTRY")]
        public string COUNTRY { get; set; }
    }

    /// <summary>
    /// 订单反抛请求报文
    /// </summary>
    [XmlRoot(ElementName = "ORDER_XT2WMS")]
    public class ORDER_XT2WMS
    {
        [XmlElement(ElementName = "ORDER_HEAD")]
        public DELIVERYORDER_HEAD ORDER_HEAD { get; set; }

        [XmlElement(ElementName = "ORDER_DETAIL")]
        public List<DELIVERYORDER_DETAIL> ORDER_DETAIL { get; set; }
    }


}
