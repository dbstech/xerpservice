﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using AutoMapper.Configuration.Conventions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Entities;
using Newtonsoft.Json;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(OrderPackDetailEntity))]
    public class OrderPackDetailDTO : EntityDto<int>
    {
        public int opdid { get; set; }

        public int? opid { get; set; }
        public int? oid { get; set; }
        public string sku { get; set; }
        [JsonProperty("cnProductName")]
        public string product_name { get; set; }
        public string enname { get; set; }
        [JsonProperty("quantity")]
        public int qty { get; set; }
        public int? inputqty { get; set; }
        public decimal? price { get; set; }
        public decimal? weight { get; set; }
        //public DateTime? Pick_time { get; set; }
        //public DateTime? Pack_time { get; set; }
        //public DateTime? Out_time { get; set; }
        public decimal? taxrate { get; set; }
        public decimal? total_w { get; set; }
        public decimal? total_t { get; set; }
        public int flag { get; set; }
        public string notes { get; set; }
        public string barcode { get; set; }
        public int status { get; set; }
        public int? pda { get; set; }
        public int? csflag { get; set; }
        public bool? oversale { get; set; }

        [NotMapped]
        public string statusName { get; set; }

        [NotMapped]
        public string orderPackNo { get; set; }

        //Flux
        [NotMapped]
        public int QtyAllocated { get; set; }
        [NotMapped]
        public int Stock { get; set; }
        [NotMapped]
        public string WarehouseStatus { get; set; }
        
    }


}
