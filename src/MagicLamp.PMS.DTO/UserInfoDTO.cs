﻿using System.Collections.Generic;

namespace MagicLamp.PMS.DTOs
{

    public class UserInfoDTO
    {

        public string Name { get; set; }
        public string OpenID { get; set; }
        public string UnionID { get; set; }
        public string UserID { get; set; }
        public string WorkPlace { get; set; }
        public string Mobile { get; set; }
        public string StateCode { get; set; }
        /// <summary>
        /// 头像url
        /// </summary>
        public string Avatar { get; set; }
        public bool Active { get; set; }
        /// <summary>
        /// 职位
        /// </summary>
        public string Position { get; set; }
        /// <summary>
        /// 钉钉工作邮箱
        /// </summary>
        public string OrgEmail { get; set; }
        /// <summary>
        /// 钉钉部门编号
        /// </summary>
        public List<int> Department { get; set; }

        public string Authority { get; set; }
    }


}
