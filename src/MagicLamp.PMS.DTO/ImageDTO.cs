﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class ImageDTO : EntityDto<long>
    {

        public long ImageId { get; set; }

        public string ImageType { get; set; }

        public string ReferenceId { get; set; }

        public string ReferenceType { get; set; }

        public string OriginalPath { get; set; }
        public string UserId { get; set; }
        public int Sort { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
