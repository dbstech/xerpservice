﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class BatchGetOrderPackInfoOutDTO
    {

        public string OrderPackNo { get; set; }
        public List<OrderPackDetailDTO> OrderPackDetails { get; set; }

    }


}
