﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class NSFSenderDTO
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public NSFContactInfoDTO ContactInfo { get; set; }
    }
}
