﻿using Npoi.Mapper.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class ExcelOrderPackDetailDTO
    {
        //order.store
        [Column("备注")]
        public string store { get; set; }

        //orderpack.orderpackno
        [Column("订单号1")]
        public string orderPackNo { get; set; }

        //orderpack.oidpackno
        [Column("订单号2")]
        public string oidPackNo { get; set; }

        //orderpackdetail.sku
        [Column("sku")]
        public string sku { get; set; }

        //orderpackdetail.product_name
        [Column("产品名称")]
        public string productName { get; set; }

        //orderpackdetail.qty
        [Column("产品数量")]
        public int qty { get; set; }

        //orderpack.rec_name
        [Column("姓名")]
        public string name { get; set; }

        //orderpack.rec_addr
        [Column("地址")]
        public string address { get; set; }

        //orderpack.rec_phone
        [Column("电话")]
        public string phone { get; set; }

        //orderpack.idno
        [Column("身份证")]
        public string idNo { get; set; }

        //orderpack.carrierid
        [Column("运单号")]
        public string carrierId { get; set; }

        //orderpack.weightval
        [Column("计重")]
        public decimal? weightValue { get; set; }

        //orderpack.packweight
        [Column("实重")]
        public decimal? packWeight { get; set; }

        //seq.up_time
        [Column("上传时间")]
        public string uploadTime { get; set; }

        //seq.bizid
        [Column("平台")]
        public string platform { get; set; }

        //orderpack.pseq
        [Column("批次号")]
        public int? pseq { get; set; }
    }
}
