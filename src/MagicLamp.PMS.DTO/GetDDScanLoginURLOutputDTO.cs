﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{

    public class GetDDScanLoginURLOutputDTO
    {

        public string ScanLoginUrl { get; set; }
        public string State { get; set; }
    }


}
