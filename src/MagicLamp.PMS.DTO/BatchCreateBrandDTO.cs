using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using AutoMapper.Configuration.Conventions;
using MagicLamp.PMS.DTOs.Attributes;
using MagicLamp.PMS.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(BrandEntity))]
    public class BatchCreateBrandDTO : EntityDto<string>
    {
        [Display(Name = "短码")]
        [Required]
        public string ShortCode { get; set; }
        [Display(Name = "名称")]
        [Required]
        public string ChineseName { get; set; }
        [Display(Name="英文名称")]
        [Required]
        public string EnglishName { get; set; }
        public bool ActiveFlag { get; set; } = true;
        public int? RowNumber { get; set; }
        [Display(Name="分级")]
        public string BrandCategoryName { get; set;}
    }
}
