﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    /// <summary>
    /// 创建物流订单 -- 请求
    /// </summary>
    public class NSFCarrierDTO
    {
        public string orderid { get; set; }
        public NSFSenderDTO Sender { get; set; }
        public NSFReceiverDTO Receiver { get; set; }
        public int SendStyle { get; set; }
        public int ExpressType { get; set; }
        public List<NSFOrderItemListDTO> OrderItemList { get; set; }
        public decimal TotalWeight { get; set; }
        public int NotUseIdentityProtocol { get; set; }
        public string NetPointId { get; set; }
        public int CCICStatus { get; set; }
        public bool PickupDirectly { get; set; }
    }
}
