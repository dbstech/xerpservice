﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class ExcelImportOutputDTO
    {

        public bool IsSuccess { get; set; }
        public string Message { get; set; }

    }

}
