using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(FreightSetEntity))]
    public class FreightSetDTO : EntityDto<string>
    {
        [SupportFuzzyQuery]
        public string Name { get; set; }
        [SupportFuzzyQuery]
        public string Logo { get; set; }
        [SupportFuzzyQuery]
        public string Tel { get; set; }
        [SupportFuzzyQuery]
        public string Website { get; set; }
    }
}
