﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{

   

    public class NSFOrderDTO
    {

        public NSFBackgroundJobActionEnum Action { get; set; }
        public int opid { get; set; }
        public int oid { get; set; }
        public string orderid { get; set; }
        public string orderpackno { get; set; }
        public string carrierid { get; set; }
        public string freightid { get; set; }
        public int exportflag { get; set; }
        public int status { get; set; }
        public decimal? packweight { get; set; }
        public NSFOrderInfoDTO NSFOrderInfo { get; set; }
        public List<NSFOrderDetailDTO> listOrderDetail { get; set; }

    }
}
