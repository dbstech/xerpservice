﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class PagedQueryInputBaseDTO<T>
    {
        private int _pagesize;
        public int PageSize
        {
            get
            {
                if (_pagesize <= 0)
                {
                    _pagesize = PMSConsts.PageSize;
                }
                return _pagesize;
            }
            set { _pagesize = value; }
        }
        private int _pageindex;
        public int PageIndex
        {
            get
            {
                return _pageindex;
            }
            set { _pageindex = value; }
        }

        private string _sortType { get; set; }
        public string SortType
        {
            get
            {
                if (string.IsNullOrEmpty(_sortType))
                {
                    _sortType = "id";
                }
                return _sortType;
            }
            set
            {
                _sortType = value;
            }
        }

        private string _sort { get; set; }
        public string Sort
        {
            get
            {
                if (string.IsNullOrEmpty(_sort))
                {
                    _sort = "DESC";
                }
                return _sort;
            }
            set { _sort = value; }
        }


        private T _filter = default(T);
        public T Filter { get { return _filter; } set { _filter = value; } }
    }
}
