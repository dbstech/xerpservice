﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(PackMaterialEntity))]

    public class PackMaterialDTO : EntityDto<int>
    {
        public int MaterialsID { get; set; }
        public string Name { get; set; }
        public double? Score { get; set; }
        public string Note { get; set; }
        public double? Cost { get; set; }
        public double? LaborCost { get; set; }
        public DateTime? Cdate { get; set; }
    }
}
