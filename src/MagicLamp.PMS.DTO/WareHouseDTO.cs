﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(WareHouseEntity))]
    public class WareHouseDTO : EntityDto<int>
    {
        public int? WareHouseId { get; set; }

        public string Location { get; set; }
        /// <summary>
        /// 是否对接实仓储
        /// </summary>
        public bool? IsWMS { get; set; }

        public string code { get; set; }

        public string FluxCode { get; set; }
        public string UserId { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
