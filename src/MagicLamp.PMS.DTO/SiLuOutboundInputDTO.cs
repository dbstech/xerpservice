﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class SiLuOutboundInputDTO
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 快递单号
        /// </summary>
        public string ExpressNo { get; set; }
        /// <summary>
        /// 出库时间
        /// </summary>
        public DateTime OutTime { get; set; }
        /// <summary>
        /// 快递公司
        /// </summary>
        public string ExpressComp { get; set; }
        /// <summary>
        /// 产品
        /// </summary>
        public List<SiLuInputItem> Items { get; set; }
    }

    public class SiLuInputItem
    {
        /// <summary>
        /// 料号（GMSKU）
        /// </summary>
        public string SkuNum { get; set; }
        /// <summary>
        /// 入库单号
        /// </summary>
        public string InNum { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int Qty { get; set; }
    }
}
