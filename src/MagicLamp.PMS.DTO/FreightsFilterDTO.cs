using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(FreightsEntity))]
    public class FreightsFilterDTO : EntityDto<string>
    {
        [SupportFuzzyQuery]
        public string id { get; set; }
        [SupportFuzzyQuery]
        public string name { get; set; }
        [SupportFuzzyQuery]
        public string notes { get; set; }
        [SupportFuzzyQuery]
        public string prefix { get; set; }
        public int? code { get; set; }
        public int? size { get; set; }
        public string CurrencyCode { get; set; }
        public int? FreightSet { get; set; }
        public string Currency { get; set; }
        public string FreightSetName { get; set; }
        public bool? Active { get; set; }
    }
}
