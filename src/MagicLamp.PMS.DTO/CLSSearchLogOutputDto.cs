using Newtonsoft.Json;

namespace MagicLamp.PMS.DTO
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class CLSSearchLogOutputDto
    {
        [JsonProperty("context")]
        public string Context { get; set; }

        [JsonProperty("listover")]
        public bool Listover { get; set; }

        [JsonProperty("results")]
        public List<Result> Results { get; set; }
    }

    public class Result
    {
        [JsonProperty("timestamp")]
        public DateTimeOffset Timestamp { get; set; }

        [JsonProperty("topic_id")]
        public string TopicId { get; set; }

        [JsonProperty("topic_name")]
        public string TopicName { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }
    }
}
