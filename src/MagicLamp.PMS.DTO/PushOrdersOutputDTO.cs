using System.Collections.Generic;

namespace MagicLamp.PMS.DTOs
{
    public class PushOrdersOutputDTO
    {
        public bool Success { get; set; }
        public int PushedOrdersCount { get; set; }
        public int FailedOrdersCount { get; set; }
        public List<string> IdsNotFound { get; set; }        
    }
}