﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using AutoMapper.Configuration.Conventions;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(PurchaseOrderLineEntity))]
    public class PurchaseOrderLineDTO : EntityDto<int>
    {
        public int PoLineId { get; set; }
        public string PoHeaderId { get; set; }
        /// <summary>
        /// 采购订单行类型 2:产品;3:服务;4:礼品 -- 此字段暂不使用和维护
        /// </summary>
        public string OrderLineType { get; set; }

        /// <summary>
        /// SKU编号
        /// </summary>
        [MapTo("Sku")]
        public string ItemId { get; set; }

        /// <summary>
        /// 产品说明
        /// </summary>
        public string ItemDescription { get; set; }

        /// <summary>
        /// 采购数量
        /// </summary>
        public decimal QTY { get; set; }

        /// <summary>
        /// 币种
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        /// 单位
        /// </summary>
        public string UOM { get; set; }

        /// <summary>
        /// 不含税成本
        /// </summary>
        [MapTo("Cost")]
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// 含税成本
        /// </summary>
        [MapTo("TaxedCost")]
        public decimal UnitPriceTax { get; set; }

        /// <summary>
        /// 税率
        /// </summary>
        public decimal? TaxRate { get; set; }

        /// <summary>
        /// WMS接收数量
        /// </summary>
        public decimal? RecieveQTY { get; set; }

        public int? LineStatus { get; set; }

        /// <summary>
		/// 创建人
		/// </summary>
        public string CreateBy { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime? CreateDate { get; set; }

        /// <summary>
        /// 最后更新人
        /// </summary>
        public string LastUpdateBy { get; set; }

        /// <summary>
        /// 最后更新日期
        /// </summary>
        public DateTime? LastUpdateDate { get; set; }

        /// <summary>
        /// 客户
        /// </summary>
        public string OrgId { get; set; }

        public string WareHouse { get; set; }

        /// <summary>
        /// 接收时间
        /// </summary>
        public DateTime? ReceiveDate { get; set; }

        /// <summary>
        /// 供应链生成批次编号(lot12)
        /// </summary>
        public string LotNo { get; set; }

        /// <summary>
        /// 批次所属渠道
        /// </summary>
        public string LotHolder { get; set; }
    }
}
