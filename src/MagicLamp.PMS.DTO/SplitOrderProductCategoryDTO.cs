﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using MagicLamp.PMS.Entities;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(SplitOrderProductCategoryEntity))]
    public class SplitOrderProductCategoryDTO : EntityDto<int>
    {

        public int CategoryId { get; set; }

        public string CNName { get; set; }

    }
}
