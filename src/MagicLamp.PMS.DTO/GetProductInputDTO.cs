﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class GetProductInputDTO
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "invalid sku")]
        public string SKU { get; set; }

    }
}
