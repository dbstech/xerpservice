using Newtonsoft.Json;

namespace MagicLamp.PMS.DTO
{
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class CLSSearchLogInputDto
    {
        // [JsonProperty("logset_id")]
        // string logsetId { get; set; }
        // [JsonProperty("topid_ids")]
        // public string topicIds { get; set; }
        [JsonProperty("start_time")]
        public string startTime { get; set; }
        [JsonProperty("end_time")]
        public string endTime { get; set; }
        [JsonProperty("query_string")]
        public string queryString { get; set; }
        [JsonProperty("limit")]
        public int limit { get; set; } = 100;
        // [JsonProperty("context")]
        // string context { get; set; }
        [JsonProperty("sort")]
        public string sort { get; set; } = "desc";
    }
}