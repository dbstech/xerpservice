﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class NSFOrderInfoDTO
    {
        public string sender_name { get; set; }
        public string sender_addr { get; set; }
        public string sender_phone { get; set; }
        public string rec_name { get; set; }
        public string rec_addr { get; set; }
        /// <summary>
        /// ID of receiver
        /// </summary>
        public string idno { get; set; }
        public string rec_phone { get; set; }
    }
}
