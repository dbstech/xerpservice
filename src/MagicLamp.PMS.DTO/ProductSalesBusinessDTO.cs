﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(ProductSalesBusinessEntity))]
    public class ProductSalesBusinessDTO : EntityDto<int>
    {

        public string BusinessName { get; set; }
        public string SKU { get; set; }
        public bool Active { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
