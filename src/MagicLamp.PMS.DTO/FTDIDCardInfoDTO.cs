﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class FENGTIANIDCardInfoDTO
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 身份证号
        /// </summary>
        public string inum { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string phone { get; set; }
        /// <summary>
        /// 身份证正面图片（身份证正面图片，若传递 card_full 则该参数可为空
        /// </summary>
        public string f_img { get; set; }
        /// <summary>
        /// 身份证背面图片（身份证背面图片，若传递 card_full 则该参数可为空
        /// </summary>
        public string b_img { get; set; }

    }
}
