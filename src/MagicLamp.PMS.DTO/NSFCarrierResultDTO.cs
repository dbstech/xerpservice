﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class NSFCarrierResultDTO
    {
        public NSFDataDTO Data { get; set; }
        public int Code { get; set; }
        public string Msg { get; set; }
        public bool IsSuccessful { get; set; }
    }
}
