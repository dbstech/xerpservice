namespace MagicLamp.PMS.DTOs
{

    public class AccountLoginInputDTO
    {
        public string UserName { get; set; }
        public string Password { get; set; }

    }
}
