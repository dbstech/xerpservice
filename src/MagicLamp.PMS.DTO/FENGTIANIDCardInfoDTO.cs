﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class FTDIDCardInfoDTO
    {
        /// <summary>
        /// 姓名，加密时无需 UrlEncode
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 身份证号
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 身份证图片（若正反面在同一张图片则传递该参数，否则为空
        /// </summary>
        public byte[] card_full { get; set; }
        /// <summary>
        /// 身份证正面图片（身份证正面图片，若传递 card_full 则该参数可为空
        /// </summary>
        public byte[] card_front { get; set; }
        /// <summary>
        /// 身份证背面图片（身份证背面图片，若传递 card_full 则该参数可为空
        /// </summary>
        public byte[] card_back { get; set; }

        public string card_frontname { get; set; }
        public string card_backtname { get; set; }

    }
}
