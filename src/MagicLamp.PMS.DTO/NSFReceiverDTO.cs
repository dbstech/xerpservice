﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class NSFReceiverDTO
    {
        public string Name { get; set; }
        public string Street { get; set; }
        public string IdentityNumber { get; set; }
        public NSFContactInfoDTO ContactInfo { get; set; }
    }
}
