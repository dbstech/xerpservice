﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class UGGStockResultDTO
    {
        public int StockQty { get; set; }
        public int AvaiStockQty { get; set; }
        public string Barcode { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ColorName { get; set; }
        public string Size { get; set; }
        public string ETADate { get; set; }
        public string ETAQty { get; set; }
    }
}
