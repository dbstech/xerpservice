﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(InventoryProductCategoryEntity))]
    public class InventoryProductCategoryDTO : EntityDto<int>
    {

        public int CategoryID { get; set; }

        [Display(Name = "编码")]
        public string Code { get; set; }

        [Display(Name = "产品分类")]
        public string Name { get; set; }

        [Display(Name = "备注")]
        public string Remark { get; set; }
        public int ParentID { get; set; }
        public DateTime? CreationTime { get; set; }
    }
}
