﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class IsStockFulfilledOutputDTO
    {
        public bool IsFulfilled { get; set; }
        public int LackQty { get; set; }
        /// <summary>
        /// list of SKU that need to be ordered in HH
        /// </summary>
        public List<OrderPackDetailDTO> ListOrderPackDetailForHH { get; set; }
        /// <summary>
        /// list of SKU that are out of stock
        /// </summary>
        public List<OrderPackDetailDTO> ListOutOfStockSKU { get; set; }
        /// <summary>
        /// list of SKU that used Flux stock
        /// </summary>
        public List<OrderPackDetailDTO> ListOrderedFluxSKU { get; set; }
    }
}
