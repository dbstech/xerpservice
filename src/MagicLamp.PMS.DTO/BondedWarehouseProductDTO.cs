using System;

namespace MagicLamp.PMS.DTOs
{    public class BondedWarehouseProductDTO
    {
        public string Barcode { get; set; }
        public int Quantity { get; set; }
        public DateTime? ExpiryDate { get; set; }
    }
}
