﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class BatchGetOrderPackInfoInputDTO
    {
        //[Required(ErrorMessage = "Invalid order pack no")]
        private List<string> _orderPackNos = null;
        public List<string> OrderPackNos
        {
            get
            {
                if (_orderPackNos == null)
                {
                    _orderPackNos = new List<string>();
                }
                return _orderPackNos;
            }
            set { _orderPackNos = value; }
        }

        private List<string> _orderIDs = null;
        public List<string> OrderIDs
        {
            get
            {
                if (_orderIDs == null)
                {
                    _orderIDs = new List<string>();
                }
                return _orderIDs;
            }
        }

    }


}
