﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class NSFAccountInfoDTO
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Grant_type { get; set; }
    }
}
