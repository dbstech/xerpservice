namespace MagicLamp.PMS.DTOs
{

    public class AccountDTO
    {
        public string UserID { get; set; }
        public string UserName { get; set; }
    }
}
