﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class NSFContactInfoDTO
    {
        public string Mobile { get; set; }
    }
}
