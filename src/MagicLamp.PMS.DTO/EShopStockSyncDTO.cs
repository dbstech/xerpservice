﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class EShopStockSyncDTO
    {
        [JsonProperty("sku")]
        public string Sku { get; set; }
        [JsonProperty("stock")]
        public int Stock { get; set; }
        [JsonProperty("best_before")]
        public string Best_before { get; set; }

    }
}
