namespace MagicLamp.PMS.DTOs
{
    public class SyncAladdinStockOutputDTO
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
    }
}
