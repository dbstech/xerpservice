﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using AutoMapper.Configuration.Conventions;
using MagicLamp.PMS.DTOs.Attributes;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{

    [AutoMap(typeof(LogisticsFeeEntity))]
    [UploadTemplate(Label = "下载物流运费Excel模板", TemplateName = "20191127-XERP物流运费模板.xls")]
    public class LogisticsFeeDTO : EntityDto<long>
    {

        [SupportFuzzyQuery]
        /// <summary>
        /// 承运方
        /// </summary>
        //[MapTo("CourierName")]
        [Display(Name = "承运方")]
        public string CourierName { get; set; }
        /// <summary>
        /// 货主
        /// </summary>
        //[MapTo("CustomerName")]
        [Required]
        [Display(Name = "货主")]
        public string CustomerName { get; set; }

        /// <summary>
        /// 运单号
        /// </summary>
        //[MapTo("CourierNO")]
        [Display(Name = "运单号")]
        public string CourierNO { get; set; }

        /// <summary>
        /// 计费重量（KG）
        /// </summary>
        // [MapTo("Weight")]
        [Required]
        [Display(Name = "计费重量（KG）")]
        public string Weight { get; set; }

        /// <summary>
        /// 结算金额
        /// </summary>
        //[MapTo("Fee")]
        [Display(Name = "结算金额")]
        public string Fee { get; set; }

        /// <summary>
        /// 结算货币
        /// </summary>
        //[MapTo("CurrencyType")]
        [Display(Name = "结算货币")]
        public string CurrencyType { get; set; }

        /// <summary>
        /// 结算日期
        /// </summary>
        //[MapTo("SettlementDate")]
        [Display(Name = "结算日期")]
        public string SettlementDate { get; set; }

        /// <summary>
        /// 结算账单编号
        /// </summary>
        //[MapTo("SettlementAccount")]
        [Required]
        [Display(Name = "结算账单编号")]
        public string SettlementAccount { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        //[MapTo("Updated_at")]
        [Display(Name = "更新时间")]
        public DateTime? Updated_at { get; set; } = System.DateTime.UtcNow;
        [Display(Name = "发票号码")]
        public string InvoiceNumber { get; set; }
        [Display(Name = "发票日期")]
        public DateTime? InvoiceDate { get; set; }
    }
}
