﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class FluxPutOrderDTO
    {
        /// <summary>
        /// 来源订单号
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 订单类型
        /// </summary>
        public string OrderType { get; set; }
        /// <summary>
        /// 订单创建时间
        /// </summary>
        public string OrderTime { get; set; }
        /// <summary>
        /// 货主ID
        /// </summary>
        public string CustomerID { get; set; }
        /// <summary>
        /// 参考编号2（平台订单号）
        /// </summary>
        public string SOReference2 { get; set; }
        /// <summary>
        /// 参考编号3（店铺名称）
        /// </summary>
        public string SOReference3 { get; set; }
        /// <summary>
        /// 参考编号4 (业务)
        /// </summary>
        public string SOReference4 { get; set; }
        /// <summary>
        /// 参考编号5（快递单号, 无DeliveryNo字段时使用该字段)
        /// </summary>
        public string SOReference5 { get; set; }
        /// <summary>
        /// 快递单号
        /// </summary>
        public string DeliveryNo { get; set; }
        /// <summary>
        /// 下单平台
        /// </summary>
        public string ConsigneeID { get; set; }
        /// <summary>
        /// 收货人
        /// </summary>
        public string ConsigneeName { get; set; }
        /// <summary>
        /// 国家编号
        /// </summary>
        public string C_Country { get; set; }
        /// <summary>
        /// 省
        /// </summary>
        public string C_Province { get; set; }
        /// <summary>
        /// 市
        /// </summary>
        public string C_City { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public string C_Tel1 { get; set; }
        /// <summary>
        /// 固话
        /// </summary>
        public string C_Tel2 { get; set; }
        /// <summary>
        /// 邮编
        /// </summary>
        public string C_ZIP { get; set; }
        public string C_Mail { get; set; }
        public string C_Address1 { get; set; }
        public string C_Address2 { get; set; }
        public string C_Address3 { get; set; }
        public string UserDefine1 { get; set; }
        public string UserDefine2 { get; set; }
        public string UserDefine3 { get; set; }
        public string UserDefine4 { get; set; }
        public string UserDefine5 { get; set; }
        public string UserDefine6 { get; set; }
        public string InvoicePrintFlag { get; set; }
        public string Notes { get; set; }
        /// <summary>
        /// 仓库ID
        /// </summary>
        public string WarehouseID { get; set; }
        public string RouteCode { get; set; }
        public string Stop { get; set; }
        public string CarrierMail { get; set; }
        public string CarrierFax { get; set; }
        public string Channel { get; set; }
        public string CarrierId { get; set; }
        public string CarrierName { get; set; }
        /// <summary>
        /// 发货详情
        /// </summary>
        public detailsItem[] detailsItem { get; set; }
        
        //public string invoiceItem { get; set; }//发票详情

        public string H_EDI_01 { get; set; }
        public string H_EDI_02 { get; set; }
        public string H_EDI_03 { get; set; }
    }

    /// <summary>
    /// 明细信息
    /// </summary>
    public class detailsItem
    {
        /// <summary>
        /// 行号
        /// </summary>
        public int LineNo { get; set; }
        /// <summary>
        /// 客户编号
        /// </summary>
        public string CustomerID { get; set; }
        /// <summary>
        /// 产品
        /// </summary>
        public string SKU { get; set; }
        /// <summary>
        /// 批次属性01（生产日期，格式YYYY-MM-DD）
        /// </summary>
        public string LotAtt11 { get; set; }
        /// <summary>
        /// 波次号
        /// </summary>
        public string LotAtt07 { get; set; }
        /// <summary>
        /// 订货数
        /// </summary>
        public decimal QtyOrdered { get; set; }
        public string UserDefine1 { get; set; }
        public string UserDefine2 { get; set; }
        public string UserDefine3 { get; set; }
        public string UserDefine4 { get; set; }
        public string UserDefine5 { get; set; }
        public string UserDefine6 { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Notes { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        public decimal Price { get; set; }
    }

    /// <summary>
    /// 发票信息
    /// </summary>
    public class invoiceItem
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 行编号
        /// </summary>
        public int LineNumber { get; set; }
        /// <summary>
        /// 发票抬头
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 电商平台的发票串号
        /// </summary>
        public string Reference1 { get; set; }
        /// <summary>
        /// 产品名称
        /// </summary>
        public string SKU { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        public string UOM { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public decimal QTY { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal UnitPrice { get; set; }
        /// <summary>
        /// 小计
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 税率
        /// </summary>
        public decimal TAXRATE { get; set; }
        /// <summary>
        /// 税额
        /// </summary>
        public decimal TAXAMOUNT { get; set; }
        /// <summary>
        /// 发票产品名称
        /// </summary>
        public string SKUDESCR { get; set; }
        /// <summary>
        /// 发票型号
        /// </summary>
        public string DetailTitle { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string NOTES { get; set; }
        public string UserDefine1 { get; set; }
        public string UserDefine2 { get; set; }
        public string UserDefine3 { get; set; }
        public string UserDefine4 { get; set; }
        public string UserDefine5 { get; set; }
    }
}
