﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class NSFPostDataDTO
    {
        /// <summary>
        /// 接口地址
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// 提交的JSON数据
        /// </summary>
        public string parameters { get; set; }
        /// <summary>
        /// 编码方式
        /// </summary>
        public Encoding dataEncode { get; set; }
        public string authorization { get; set; }
        public string contentType { get; set; }
    }
}
