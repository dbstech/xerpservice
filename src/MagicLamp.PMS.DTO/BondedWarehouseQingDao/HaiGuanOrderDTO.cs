﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace MagicLamp.PMS.DTOs.BondedWarehouseQingDao
{
    /// <summary>
    /// CEB311Message 为订单报文 XML 根节点名称
    /// </summary>
    [XmlRoot(ElementName = "CEB311Message")]
    public class HaiGuanOrderDTO
    {
        /// <summary>
        /// 订单业务节点（放置订单业务的相关信息）必填
        /// </summary>
        [XmlElement(ElementName = "Order")]
        public Order Order { get; set; }

        /// <summary>
        /// 基础报文传输实体节点（放置报文传输企业信息。）必填
        /// </summary>
        [XmlElement(ElementName = "BaseTransfer")]
        public BaseTransfer BaseTransfer { get; set; }

        /// <summary>
        /// 基础回执订阅实体节点（用于第三方提供数据的订阅下发）非必填
        /// </summary>
        [XmlElement(ElementName = "BaseSubscribe")]
        public string BaseSubscribe { get; set; }

        /// <summary>
        /// 签名节点（放置报文加业务签信息）非必填
        /// </summary>
        [XmlElement(ElementName = "Signature")]
        public string Signature { get; set; }

        /// <summary>
        /// 扩展自定义数据实体节点（用于扩展数据实体用）非必填
        /// </summary>
        [XmlElement(ElementName = "ExtendMessage")]
        public string ExtendMessage { get; set; }

        #region attribute
        /// <summary>
        /// 报文编号（报文的36位系统唯一序号（英文字母大写）。）必填
        /// </summary>
        [XmlAttribute(attributeName: "guid")]
        public string guid { get => this.Order.OrderHead.guid; set => this.guid = value; }

        /// <summary>
        /// 版本号（报文版本号，默认为1.0）必填
        /// </summary>
        [XmlAttribute(attributeName: "version")]
        public string version { get; set; } = "1.0";
        #endregion
    }

    /// <summary>
    /// 订单业务节点
    /// </summary>
    [XmlRoot(ElementName = "Order")]
    public class Order
    {
        /// <summary>
        /// 电子订单表头
        /// </summary>
        [XmlElement(ElementName = "OrderHead")]
        public OrderHead OrderHead { get; set; }

        /// <summary>
        /// 电子订单商品表体
        /// </summary>
        [XmlElement(ElementName = "OrderList")]
        public List<OrderList> OrderList { get; set; }
    }

    /// <summary>
    /// 电子订单表头
    /// </summary>
    [XmlRoot(ElementName = "OrderHead")]
    public class OrderHead
    {
        /// <summary>
        /// 系统唯一序号（企业系统生成36位唯一序号（英文字母大写）。）必填
        /// </summary>
        [XmlElement(ElementName = "guid")]
        public string guid { get; set; } = Guid.NewGuid().ToString().ToUpper();

        /// <summary>
        /// 报送类型（企业报送类型。1-新增 2-变 更 3-删除。默认为1。）必填
        /// </summary>
        [XmlElement(ElementName = "appType")]
        public string appType { get; set; }

        /// <summary>
        /// 报送时间（企 业 报 送 时 间 。 格 式:YYYYMMDDhhmmss。）必填
        /// </summary>
        [XmlElement(ElementName = "appTime")]
        public string appTime { get; set; }

        /// <summary>
        /// 业务状态（业务状态:1-暂存,2-申报,默 认为2。填写2时,Signature节点必须填写。）必填
        /// </summary>
        [XmlElement(ElementName = "appStatus")]
        public string appStatus { get; set; }

        /// <summary>
        /// 订单类型（电子订单类型：I进口）必填
        /// </summary>
        [XmlElement(ElementName = "orderType")]
        public string orderType { get; set; } = "I";

        /// <summary>
        /// 订单编号（交易平台的订单编号，同一交易平台的订单编号应唯一。订单编号长度不能超过60位。）必填
        /// </summary>
        [XmlElement(ElementName = "orderNo")]
        public string orderNo { get; set; }

        /// <summary>
        /// 电商平台代码（电商平台的海关注册登记编号；电商平台未在海关注册登记，由电商企业发送订单的，以中国电子口岸发布的电商平台标识编号为准。）必填
        /// </summary>
        [XmlElement(ElementName = "ebpCode")]
        public string ebpCode { get; set; }

        /// <summary>
        /// 电商平台名称（电商平台的海关注册登记名称；电商平台未在海关注册登记，由电商企业发送订单的，以中国电子口岸发布的电商平台名称为准。）必填
        /// </summary>
        [XmlElement(ElementName = "ebpName")]
        public string ebpName { get; set; }

        /// <summary>
        /// 电商企业代码（电商企业的海关注册登记编号。）必填
        /// </summary>
        [XmlElement(ElementName = "ebcCode")]
        public string ebcCode { get; set; }

        /// <summary>
        /// 电商企业名称（电商企业的海关注册登记名称。）必填
        /// </summary>
        [XmlElement(ElementName = "ebcName")]
        public string ebcName { get; set; }

        /// <summary>
        /// 商品价格（商品实际成交价，含非现金抵扣金额。）必填
        /// </summary>
        [XmlElement(ElementName = "goodsValue")]
        public decimal goodsValue { get; set; }

        /// <summary>
        /// 运杂费（不包含在商品价格中的运杂费，无则填写"0"。）必填
        /// </summary>
        [XmlElement(ElementName = "freight")]
        public decimal freight { get; set; }

        /// <summary>
        /// 非现金抵扣金额（使用积分、虚拟货币、代金券等非现金支付金额，无则填写"0"。）必填
        /// </summary>
        [XmlElement(ElementName = "discount")]
        public decimal discount { get; set; }

        /// <summary>
        /// 代扣税款（企业预先代扣的税款金额，无则填写“0”）必填
        /// </summary>
        [XmlElement(ElementName = "taxTotal")]
        public decimal taxTotal { get; set; }

        /// <summary>
        /// 实际支付金额（商品价格+运杂费+代扣税款- 非现金抵扣金额，与支付凭证 的支付金额一致。）必填
        /// </summary>
        [XmlElement(ElementName = "acturalPaid")]
        public decimal acturalPaid { get; set; }

        /// <summary>
        /// 币制（限定为人民币，填写“142”。）必填
        /// </summary>
        [XmlElement(ElementName = "currency")]
        public string currency { get; set; } = "142";

        /// <summary>
        /// 订购人注册号（订购人的交易平台注册号。）必填
        /// </summary>
        [XmlElement(ElementName = "buyerRegNo")]
        public string buyerRegNo { get; set; }

        /// <summary>
        /// 订购人姓名（订购人的真实姓名。）必填
        /// </summary>
        [XmlElement(ElementName = "buyerName")]
        public string buyerName { get; set; }

        /// <summary>
        /// 订购人电话（海关监管对象的电话，要求实 际联系电话）必填
        /// </summary>
        [XmlElement(ElementName = "buyerTelephone")]
        public string buyerTelephone { get; set; }

        /// <summary>
        /// 订购人证件类型（1-身份证,2-其它。限定为身 份证，填写“1”。）必填
        /// </summary>
        [XmlElement(ElementName = "buyerIdType")]
        public string buyerIdType { get; set; } = "1";

        /// <summary>
        /// 订购人证件号码（订购人的身份证件号码。）必填
        /// </summary>
        [XmlElement(ElementName = "buyerIdNumber")]
        public string buyerIdNumber { get; set; }

        /// <summary>
        /// 支付企业代码（支付企业的海关注册登记编 号。）非必填
        /// </summary>
        [XmlElement(ElementName = "payCode")]
        public string payCode { get; set; }

        /// <summary>
        /// 支付企业名称（支付企业在海关注册登记的 企业名称。）非必填
        /// </summary>
        [XmlElement(ElementName = "payName")]
        public string payName { get; set; }

        /// <summary>
        /// 支付交易编号（支付企业唯一的支付流水号。）非必填
        /// </summary>
        [XmlElement(ElementName = "payTranscationId")]
        public string payTranscationId { get; set; }

        /// <summary>
        /// 商品批次号（商品批次号。）非必填
        /// </summary>
        [XmlElement(ElementName = "batchNumbers")]
        public string batchNumbers { get; set; }

        /// <summary>
        /// 收货人姓名（收货人姓名，必须与电子运单 的收货人姓名一致。）必填
        /// </summary>
        [XmlElement(ElementName = "consignee")]
        public string consignee { get; set; }

        /// <summary>
        /// 收货人电话（收货人联系电话，必须与电子 运单的收货人电话一致。）必填
        /// </summary>
        [XmlElement(ElementName = "consigneeTelephone")]
        public string consigneeTelephone { get; set; }

        /// <summary>
        /// 收货地址（收货地址，必须与电子运单的 收货地址一致。）必填
        /// </summary>
        [XmlElement(ElementName = "consigneeAddress")]
        public string consigneeAddress { get; set; }

        /// <summary>
        /// 收货地址行政区 划代码（参照国家统计局公布的国家 行政区划标准填制。）非必填
        /// </summary>
        [XmlElement(ElementName = "consigneeDistrict")]
        public string consigneeDistrict { get; set; }

        /// <summary>
        /// 备注 非必填
        /// </summary>
        [XmlElement(ElementName = "note")]
        public string note { get; set; }
    }

    /// <summary>
    /// 电子订单商品表体
    /// </summary>
    [XmlRoot(ElementName = "OrderList")]
    public class OrderList
    {
        /// <summary>
        /// 商品序号（从1开始的递增序号。）必填
        /// </summary>
        [XmlElement(ElementName = "gnum")]
        public int gnum { get; set; }

        /// <summary>
        /// 企业商品货号（电商企业自定义的商品货号 （SKU）。）非必填
        /// </summary>
        [XmlElement(ElementName = "itemNo")]
        public string itemNo { get; set; }

        /// <summary>
        /// 企业商品名称（交易平台销售商品的中文名 称。）必填
        /// </summary>
        [XmlElement(ElementName = "itemName")]
        public string itemName { get; set; }

        /// <summary>
        /// 商品规格型号（满足海关归类、审价以及监管 的要求为准。包括：品名、牌 名、规格、型号、成份、含量、 等级等）必填
        /// </summary>
        [XmlElement(ElementName = "gModel")]
        public string gModel { get; set; }

        /// <summary>
        /// 企业商品描述（交易平台销售商品的描述信 息。）非必填
        /// </summary>
        [XmlElement(ElementName = "itemDescribe")]
        public string itemDescribe { get; set; }

        /// <summary>
        /// 条形码（国际通用的商品条形码，一般 由前缀部分、制造厂商代码、 商品代码和校验码组成。）非必填
        /// </summary>
        [XmlElement(ElementName = "barCode")]
        public string barCode { get; set; }

        /// <summary>
        /// 单位（填写海关标准的参数代码，参 照《JGS-20 海关业务代码集》 - 计量单位代码。）必填
        /// </summary>
        [XmlElement(ElementName = "unit")]
        public string unit { get; set; }

        /// <summary>
        /// 数量（商品实际数量。）必填
        /// </summary>
        [XmlElement(ElementName = "qty")]
        public int qty { get; set; }

        /// <summary>
        /// 单价（商品单价。赠品单价填写为 “0”。）必填
        /// </summary>
        [XmlElement(ElementName = "price")]
        public decimal price { get; set; }

        /// <summary>
        /// 总价（商品总价，等于单价乘以数 量。）必填
        /// </summary>
        [XmlElement(ElementName = "totalPrice")]
        public decimal totalPrice { get; set; }

        /// <summary>
        /// 币制（限定为人民币，填写“142”。）必填
        /// </summary>
        [XmlElement(ElementName = "currency")]
        public string currency { get; set; } = "142";

        /// <summary>
        /// 原产国（填写海关标准的参数代码，参 照《JGS-20 海关业务代码集》 -国家（地区）代码表。）必填
        /// </summary>
        [XmlElement(ElementName = "country")]
        public string country { get; set; }

        /// <summary>
        /// 备注（促销活动，商品单价偏离市场 价格的，可以在此说明。）非必填
        /// </summary>
        [XmlElement(ElementName = "note")]
        public string note { get; set; }
    }

    /// <summary>
    /// 基础报文传输实体节点
    /// </summary>
    [XmlRoot(ElementName = "BaseTransfer")]
    public class BaseTransfer
    {
        /// <summary>
        /// 传输企业代码（报文传输的企业代码（需要与接入客户端的企 业身份一致））必填
        /// </summary>
        [XmlElement(ElementName = "copCode")]
        public string copCode { get; set; }

        /// <summary>
        /// 传输企业名称（报文传输的企业名称）必填
        /// </summary>
        [XmlElement(ElementName = "copName")]
        public string copName { get; set; }

        /// <summary>
        /// 报文传输模式（默认为DXP；指中国电子口岸数据交换平台）必填
        /// </summary>
        [XmlElement(ElementName = "dxpMode")]
        public string dxpMode { get; set; }

        /// <summary>
        /// 报文传输编号（向中国电子口岸数据中心申请数据交换平台 的用户编号）必填
        /// </summary>
        [XmlElement(ElementName = "dxpId")]
        public string dxpId { get; set; }

        /// <summary>
        /// 备注 非必填
        /// </summary>
        [XmlElement(ElementName = "note")]
        public string note { get; set; }
    }
}
