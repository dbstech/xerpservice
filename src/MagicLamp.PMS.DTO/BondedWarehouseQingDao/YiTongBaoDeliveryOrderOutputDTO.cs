﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.BondedWarehouseQingDao
{
    public class YiTongBaoDeliveryOrderOutputDTO
    {
        public string code { get; set; }
        public bool success { get; set; }
        public string msg { get; set; }
        public object data { get; set; }
    }
}
