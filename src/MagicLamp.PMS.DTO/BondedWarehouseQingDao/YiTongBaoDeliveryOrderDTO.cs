﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.BondedWarehouseQingDao
{
    public class YiTongBaoDeliveryOrderDTO
    {
        /// <summary>
        /// 订单状态标识(ADD新增，UPDATE更新) 必填
        /// </summary>
        public string modifyMark { get; set; }

        /// <summary>
        /// 版本号(请填4) 必填
        /// </summary>
        public int version { get; set; } = 4;

        /// <summary>
        /// 仓库编码(郑州综保ZZZB，青岛青保QDQB) 非必填
        /// </summary>
        public string warehouseCode { get; set; } = "QDQB";

        /// <summary>
        /// 提运单号(填自定义的固定值，例如YT2019) 必填
        /// </summary>
        public string billNo { get; set; } = "YT2019";

        /// <summary>
        /// 电商OMS代码(易通指定的货主代码) 必填
        /// MFDQD OR MJKQDO
        /// </summary>
        public string ebcOmsCode { get; set; }

        /// <summary>
        /// 平台OMS代码(易通指定的电商平台代码) 必填
        /// </summary>
        public string ebpOmsCode { get; set; }

        /// <summary>
        /// 物流OMS代码(联系技术获取。万里目拼多多中通 ZTOPDDWLM；万里目中通尊享 ZTOP) 必填
        /// </summary>
        public string logisticsOmsCode { get; set; }

        /// <summary>
        /// 支付OMS代码(ALIPAY支付宝，WYZX网银在线，TENPAY财付通，UNIONPAY银联，SFUPAY盛付通，更多支付代码联系技术获取。) 必填
        /// </summary>
        public string payOmsCode { get; set; }

        /// <summary>
        /// 电商平台订单号(用于海关四单比对的订单号) 必填
        /// </summary>
        public string orderNo { get; set; }

        /// <summary>
        /// 电商平台发货单号(电商平台发货单号) 非必填
        /// </summary>
        public string soNo { get; set; }

        /// <summary>
        /// 快递单号(若不使用易通合作物流企业，请填写) 非必填
        /// </summary>
        //public string logisticsNo { get; set; }

        /// <summary>
        /// 支付企业支付单号(支付企业交易流水号) 必填
        /// </summary>
        public string payNo { get; set; }

        /// <summary>
        /// 担保企业海关编号(一般填写电商企业海关代码) 必填
        /// </summary>
        public string assureCode { get; set; } = "MFDQD";

        /// <summary>
        /// 订购人证件类型(填1) 必填
        /// </summary>
        public string buyerIdType { get; set; } = "1";

        /// <summary>
        /// 订购人证件号码(18位身份证号码) 必填
        /// </summary>
        public string buyerIdNumber { get; set; }

        /// <summary>
        /// 订购人姓名(跟支付单一致的支付人姓名) 必填
        /// </summary>
        public string buyerName { get; set; }

        /// <summary>
        /// 订购人平台注册号(用户网站注册账号) 必填
        /// </summary>
        public string buyerRegNo { get; set; }

        /// <summary>
        /// 订购人电话 必填
        /// </summary>
        public string buyerTelephone { get; set; }

        /// <summary>
        /// 收件人地址([重要]包含省市区的收件地址) 必填
        /// </summary>
        public string consigneeAddress { get; set; }

        /// <summary>
        /// 收件人电话 必填
        /// </summary>
        public string consigneePhone { get; set; }

        /// <summary>
        /// 收件人姓名 必填
        /// </summary>
        public string consigneeName { get; set; }

        /// <summary>
        /// 收件人所在省([重要]必填保证准确性) 必填
        /// </summary>
        public string province { get; set; }

        /// <summary>
        /// 收件人所在市([重要]必填保证准确性) 必填
        /// </summary>
        public string city { get; set; }

        /// <summary>
        /// 收件人所在区([重要]必填保证准确性) 必填
        /// </summary>
        public string area { get; set; }

        /// <summary>
        /// 航次航班号 非必填
        /// </summary>
        //public string voyageNo { get; set; }

        /// <summary>
        /// 运费(如果包邮，请填写0) 必填
        /// </summary>
        public decimal freight { get; set; }

        /// <summary>
        /// 非现金抵扣金额(若使用优惠卷等，请填写抵扣金额，若无，请填写0) 必填
        /// </summary>
        public decimal discount { get; set; }

        /// <summary>
        /// 商品成交价(所有商品的价值和) 必填
        /// </summary>
        public decimal goodsValue { get; set; }

        /// <summary>
        /// 代扣税款(若不确定，请填0) 必填
        /// </summary>
        public decimal taxTotal { get; set; }

        /// <summary>
        /// 实际支付金额(支付单支付金额) 必填
        /// </summary>
        public decimal acturalPaid { get; set; }

        /// <summary>
        /// 包裹毛重(若不确定，请填0) 必填
        /// </summary>
        public decimal grossWeight { get; set; }

        /// <summary>
        /// 包裹净重(若不确定，请填0) 必填
        /// </summary>
        public decimal netWeight { get; set; }

        /// <summary>
        /// 发货人名称(电商企业名称) 必填
        /// </summary>
        public string senderName { get; set; }

        /// <summary>
        /// 发货人地址(郑州新郑综合保税区) 必填
        /// </summary>
        public string senderAddress { get; set; }

        /// <summary>
        /// 发货人电话(电商企业售后电话) 必填
        /// </summary>
        public string senderPhone { get; set; }

        /// <summary>
        /// 电商平台订单时间(订单创建时间) 必填
        /// </summary>
        public string orderTime { get; set; }

        /// <summary>
        /// 卖家备注(卖家自定义备注) 非必填
        /// </summary>
        public string sellerNote { get; set; }

        /// <summary>
        /// 扩展字段(特殊平台需求,Map类型,拼多多电子面单加密数据推送见下方报文样例。) 非必填
        /// </summary>
        public string extendsProps { get; set; }

        public List<orderItems> orderItems { get; set; }
    }

    public class orderItems
    {
        /// <summary>
        /// 商品编码(电商企业自己的商品编码/货号) 必填
        /// </summary>
        public string itemCode { get; set; }

        /// <summary>
        /// 申报序号(可以进行排序的数值或字符，订单报文和清单报文生成排序使用) 必填
        /// </summary>
        public int itemSort { get; set; }

        /// <summary>
        /// 商品数量(申报数量) 必填
        /// </summary>
        public int qty { get; set; }

        /// <summary>
        /// 商品销售价格(商品销售单价) 必填
        /// </summary>
        public decimal price { get; set; }

        /// <summary>
        /// 订单商品行号(可以进行排序的数值或字符，订单报文和清单报文生成排序使用) 非必填
        /// </summary>
        public string lineNo { get; set; }

        /// <summary>
        /// 商品批次号(指定商品批次进行发货) 非必填
        /// </summary>
        public string batchNo { get; set; }

        /// <summary>
        /// 商品备注 非必填
        /// </summary>
        public string note { get; set; }
    }
}
