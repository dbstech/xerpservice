﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.BondedWarehouseQingDao
{
    public class QingDaoBondedOrderInputDTO
    {
        /// <summary>
        /// HaiGuan 订单编号（交易平台的订单编号，同一交易平台的订单编号应唯一。订单编号长度不能超过60位。）必填
        /// YiTongBao 电商平台订单号(用于海关四单比对的订单号) 必填
        /// </summary>
        public string orderNo { get; set; }

        /// <summary>
        /// HaiGuan 商品价格（商品实际成交价，含非现金抵扣金额。）必填
        /// YiTongBao 商品成交价(所有商品的价值和) 必填
        /// </summary>
        public decimal goodsValue { get; set; }

        /// <summary>
        /// HaiGuan 运杂费（不包含在商品价格中的运杂费，无则填写"0"。）必填
        /// YiTongBao 运费(如果包邮，请填写0) 必填
        /// </summary>
        public decimal freight { get; set; }

        /// <summary>
        /// HaiGuan 非现金抵扣金额（使用积分、虚拟货币、代金券等非现金支付金额，无则填写"0"。）必填
        /// YiTongBao 非现金抵扣金额(若使用优惠卷等，请填写抵扣金额，若无，请填写0) 必填
        /// </summary>
        public decimal discount { get; set; }

        /// <summary>
        /// HaiGuan 代扣税款（企业预先代扣的税款金额，无则填写“0”）必填
        /// YiTongBao 代扣税款(若不确定，请填0) 必填
        /// </summary>
        public decimal taxTotal { get; set; }

        /// <summary>
        /// HaiGuan 实际支付金额（商品价格+运杂费+代扣税款- 非现金抵扣金额，与支付凭证 的支付金额一致。）必填
        /// YiTongBao 实际支付金额(支付单支付金额) 必填
        /// </summary>
        public decimal acturalPaid { get; set; }

        /// <summary>
        /// HaiGuan 订购人注册号（订购人的交易平台注册号。）必填
        /// YiTongBao 订购人平台注册号(用户网站注册账号) 必填
        /// </summary>
        public string buyerRegNo { get; set; }

        /// <summary>
        /// HaiGuan 订购人姓名（订购人的真实姓名。）必填
        /// YiTongBao 订购人姓名(跟支付单一致的支付人姓名) 必填
        /// </summary>
        public string buyerName { get; set; }

        /// <summary>
        /// HaiGuan 订购人电话（海关监管对象的电话，要求实 际联系电话）必填
        /// YiTongBao 订购人电话 必填
        /// </summary>
        public string buyerTelephone { get; set; }

        /// <summary>
        /// HaiGuan 订购人证件号码（订购人的身份证件号码。）必填
        /// YiTongBao 订购人证件号码(18位身份证号码) 必填
        /// </summary>
        public string buyerIdNumber { get; set; }

        /// <summary>
        /// HaiGuan 收货人姓名（收货人姓名，必须与电子运单 的收货人姓名一致。）必填
        /// YiTongBao 收件人姓名 必填
        /// </summary>
        public string consignee { get; set; }

        /// <summary>
        /// HaiGuan 收货人电话（收货人联系电话，必须与电子 运单的收货人电话一致。）必填
        /// YiTongBao 收件人电话 必填
        /// </summary>
        public string consigneeTelephone { get; set; }

        /// <summary>
        /// HaiGuan 收货地址（收货地址，必须与电子运单的 收货地址一致。）必填
        /// YiTongBao 收件人地址([重要]包含省市区的收件地址) 必填
        /// </summary>
        public string consigneeAddress { get; set; }

        /// <summary>
        /// YiTongBao 收件人所在省([重要]必填保证准确性) 必填
        /// </summary>
        public string province { get; set; }

        /// <summary>
        /// YiTongBao 收件人所在市([重要]必填保证准确性) 必填
        /// </summary>
        public string city { get; set; }

        /// <summary>
        /// YiTongBao 收件人所在区([重要]必填保证准确性) 必填
        /// </summary>
        public string area { get; set; }

        /// <summary>
        /// YiTongBao 提运单号(填自定义的固定值，例如YT2019) 必填
        /// </summary>
        public string billNo { get; set; }

        /// <summary>
        /// YiTongBao 物流OMS代码(联系技术获取。万里目拼多多中通 ZTOPDDWLM；万里目中通尊享 ZTOP) 必填
        /// </summary>
        public string logisticsOmsCode { get; set; }

        /// <summary>
        /// YiTongBao 支付OMS代码(ALIPAY支付宝，WYZX网银在线，TENPAY财付通，UNIONPAY银联，SFUPAY盛付通，更多支付代码联系技术获取。) 必填
        /// </summary>
        public string payOmsCode { get; set; }

        /// <summary>
        /// YiTongBao 快递单号(若不使用易通合作物流企业，请填写) 非必填
        /// </summary>
        public string logisticsNo { get; set; }

        /// <summary>
        /// YiTongBao 支付企业支付单号(支付企业交易流水号) 必填
        /// </summary>
        public string payNo { get; set; }

        /// <summary>
        /// YiTongBao 包裹毛重(若不确定，请填0) 必填
        /// </summary>
        public decimal grossWeight { get; set; }

        /// <summary>
        /// YiTongBao 包裹净重(若不确定，请填0) 必填
        /// </summary>
        public decimal netWeight { get; set; }

        /// <summary>
        /// YiTongBao 发货人名称(电商企业名称) 必填
        /// </summary>
        public string senderName { get; set; }

        /// <summary>
        /// YiTongBao 发货人地址(郑州新郑综合保税区) 必填
        /// </summary>
        public string senderAddress { get; set; }

        /// <summary>
        /// YiTongBao 发货人电话(电商企业售后电话) 必填
        /// </summary>
        public string senderPhone { get; set; }

        /// <summary>
        /// YiTongBao 电商平台订单时间(订单创建时间) 必填
        /// </summary>
        public string orderTime { get; set; }

        public List<QingDaoOrderDetailDTO> orderDetail { get; set; }

        /// <summary>
        /// 支付参数(188用)
        /// </summary>
        public object customsPaymentData { get; set; }

        /// <summary>
        /// naky分单用
        /// </summary>
        public List<SimpleSKUDTO> listSKUForOrderSplit { get; set; }

    }

    public class QingDaoOrderDetailDTO
    {
        /// <summary>
        /// HaiGuan 企业商品货号（电商企业自定义的商品货号 （SKU）。）非必填
        /// YiTongBao 商品编码(电商企业自己的商品编码/货号) 必填
        /// </summary>
        public string itemNo { get; set; }

        /// <summary>
        /// HaiGuan 数量（商品实际数量。）必填
        /// YiTongBao 商品数量(申报数量) 必填
        /// </summary>
        public int qty { get; set; }

        /// <summary>
        /// HaiGuan 单价（商品单价。赠品单价填写为 “0”。）必填
        /// YiTongBao 商品销售价格(商品销售单价) 必填
        /// </summary>
        public decimal price { get; set; }

        /// <summary>
        /// HaiGuan 总价（商品总价，等于单价乘以数 量。）必填
        /// </summary>
        public decimal totalPrice { get; set; }
    }
}
