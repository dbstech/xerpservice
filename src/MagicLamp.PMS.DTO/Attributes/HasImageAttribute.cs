﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Attributes
{
    public class HasImageAttribute : Attribute
    {

        public HasImageAttribute() { }

        public string ImageType { get; set; }
        public string ReferenceType { get; set; }

    }
}
