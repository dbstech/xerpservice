﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Attributes
{
    public class HasFileAttribute : Attribute
    {

        public HasFileAttribute() { }

        public string FileType { get; set; }
        public string ReferenceType { get; set; }

    }
}
