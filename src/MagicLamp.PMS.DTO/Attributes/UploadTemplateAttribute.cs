﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Attributes
{
    public class UploadTemplateAttribute : Attribute
    {

        public UploadTemplateAttribute() { }

        public string Label { get; set; }
        public string TemplateName { get; set; }

    }
}
