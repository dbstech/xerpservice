using System;
using MagicLamp.PMS.Infrastructure;

namespace MagicLamp.PMS.DTOs
{
    public class BrandFilterDTO
    {
        private string _shortCode;
        [SupportFuzzyQuery]
        public string ShortCode
        {
            get
            {
                return _shortCode;
            }
            set
            {
                _shortCode = value?.Trim() ?? string.Empty;
            }
        }
        public bool? ActiveFlag { get; set; }

        private string _chineseName;

        [SupportFuzzyQuery]
        public string ChineseName
        {
            get
            {
                return _chineseName;
            }
            set
            {
                _chineseName = value?.Trim() ?? null;
            }
        }

        private string _englishName;

        [SupportFuzzyQuery]
        public string EnglishName
        {
            get
            {
                return _englishName;
            }
            set
            {
                _englishName = value?.Trim() ?? null;
            }
        }

        public DateTime? CreateTime { get; set; }
        public string CreateUser { get; set; }
        public long? OdooBrandId { get; set; } = null;
        public int? BrandCategoryId { get; set; }
        public string BrandCategoryName { get; set; }
    }
}
