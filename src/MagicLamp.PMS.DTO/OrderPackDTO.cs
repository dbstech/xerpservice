﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using AutoMapper.Configuration.Conventions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Entities;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(OrderPackEntity))]
    public class OrderPackDTO : EntityDto<int>
    {

        public int? opId { get; set; }
        [SupportMultipleQuery]
        public string orderId { get; set; }
        public int? packNo { get; set; }
        [SupportMultipleQuery]
        [Display(Name = "包裹号")]
        public string orderPackNo { get; set; }
        public string oIdPackNo { get; set; }
        public string rec_name { get; set; }
        public string rec_addr { get; set; }
        public string rec_phone { get; set; }
        public string idNo { get; set; }
        [SupportMultipleQuery]
        [Display(Name = "运单号")]
        public string carrierId { get; set; }
        public string bonded_warehouse_carrier_id { get; set; }
        public string bonded_warehouse_carrier_company { get; set; }
        [SupportMultipleQuery]
        public string freightId { get; set; }
        [NotMapped]
        public string freightName { get; set; }
        public int? exportFlag { get; set; }
        [NotMapped]
        public List<int> exportFlags { get; set; }
        [NotMapped]
        public string exportFlagName { get; set; }
        public int? status { get; set; }
        [NotMapped]
        public List<int> listStatus { get; set; }
        public decimal? taxVal { get; set; }
        public decimal? weightVal { get; set; }
        public int? freezeFlag { get; set; }
        [NotMapped]
        public List<int> freezeFlags { get; set; }
        public string freezeComment { get; set; }
        public DateTime? freezeTime { get; set; }
        [NotMapped]
        public string freezeFlagName { get; set; }

        public int? missFlag { get; set; }
        public DateTime? missFlagTime { get; set; }

        public int? pseq { get; set; }
        public DateTime? opt_time { get; set; }

        public int? refundFlag { get; set; }
        public DateTime? refundFlagTime { get; set; }
        [NotMapped]
        public string warehouseStatus { get; set; }
        public bool? syc_flag { get; set; }
        public decimal? packWeight { get; set; }
        public decimal? packWeightReal { get; set; }
        public string attchment_name { get; set; }
        public int? updateFlag { get; set; }
        /// <summary>
        /// 上传时间
        /// </summary>
        public DateTime? updateTime { get; set; }
        /// <summary>
        /// 出库时间
        /// </summary>
        public DateTime? out_time { get; set; }
        [NotMapped]
        public int? bizId { get; set; }
        [NotMapped]
        public DateTime? ordercreate_time_begin { get; set; }
        [NotMapped]
        public DateTime? ordercreate_time_end { get; set; }
        [NotMapped]
        public DateTime? out_time_begin { get; set; }
        [NotMapped]
        public DateTime? out_time_end { get; set; }
        [NotMapped]
        public string trackingNo { get; set; }
        /// <summary>
        /// reason for canceling
        /// </summary>
        [NotMapped]
        public string comment { get; set; }
        [NotMapped]
        public string shippingMethod { get; set; }
        [NotMapped]
        public string store { get; set; }
        /// <summary>
        /// The time order was created.
        /// </summary>
        [NotMapped]
        public DateTime? orderCreateTime { get; set; }
        [NotMapped]
        public bool? isExport { get; set; }
        [NotMapped]
        public string sku { get; set; }
    }
}
