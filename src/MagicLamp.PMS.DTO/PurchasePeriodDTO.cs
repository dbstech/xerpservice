﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using MagicLamp.PMS.Entities;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(PurchasePeriodEntity))]
    public class PurchasePeriodDTO : EntityDto<string>
    {

        public string Code { get; set; }

        public string Name { get; set; }

        public DateTime CreationTime { get; set; } 
    }
}
