using Abp.Application.Services.Dto;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MagicLamp.PMS.Infrastructure;

namespace MagicLamp.PMS.DTOs
{
    public class PackRuleDTO : EntityDto<int>
    {
        public int? pid { get; set; }

        [SupportFuzzyQuery]
        public string cpCodeHead { get; set; }
        public string key1 { get; set; }
        public string key2 { get; set; }
        public int? qty { get; set; }
        public int? lvl { get; set; }
        public int? exportFlag { get; set; }
        [NotMapped]
        public string exportFlagName { get; set; }
        public string cKind { get; set; }
        [NotMapped]
        public string cKindName { get; set; }

    }
}
