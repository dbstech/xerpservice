using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(BrandCategoryEntity))]
    public class BrandCategoryDTO : EntityDto<int>
    {
        public string Name { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? CreatedAt { get; set; }

    }
}