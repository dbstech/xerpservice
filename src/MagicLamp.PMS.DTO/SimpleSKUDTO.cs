﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class SimpleSKUDTO
    {
        public string SKU { get; set; }
        public int Qty { get; set; }
    }


}
