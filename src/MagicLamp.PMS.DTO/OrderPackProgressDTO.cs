﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class OrderPackProgressDTO
    {
        public OrderPackProgressDTO()
        {
            progress = new List<OrderPackProgressActionDTO>();
        }
        public List<OrderPackProgressActionDTO> progress { get; set; }
        public int opId { get; set; }
        public int Id { get; set; }
        public string orderPackNo { get; set; }
    }
}
