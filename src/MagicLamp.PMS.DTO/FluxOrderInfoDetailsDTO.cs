﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    /// <summary>
    /// 出库单明细
    /// </summary>
    public class FluxOrderInfoDetailsDTO
    {
        /// <summary>
        /// 来源订单号
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 来源行号
        /// </summary>
        public string LineNo { get; set; }
        /// <summary>
        /// 产品
        /// </summary>
        public string SKU { get; set; }
        /// <summary>
        /// 实收发运数量
        /// </summary>
        public int QtyShipped { get; set; }
        /// <summary>
        /// 实际发运时间YYYY-MM-DD HH:MM:SS
        /// </summary>
        public DateTime ShippedTime { get; set; }
        /// <summary>
        /// 快递单号
        /// </summary>
        public string DeliveryNo { get; set; }
        /// <summary>
        /// 重量
        /// </summary>
        public string Weight { get; set; }
    }
}
