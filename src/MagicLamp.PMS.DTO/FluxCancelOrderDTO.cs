﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class FluxCancelOrderDTO
    {
        /// <summary>
        /// 出库单单号
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 出库单类型 
        /// </summary>
        public string OrderType { get; set; }
        /// <summary>
        /// 货主ID
        /// </summary>
        public string CustomerID { get; set; }
        /// <summary>
        /// 仓库ID
        /// </summary>
        public string WarehouseID { get; set; }
        /// <summary>
        /// 取消原因
        /// </summary>
        public string Reason { get; set; }
    }
}
