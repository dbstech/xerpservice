using MagicLamp.PMS.Entities;
using System;
namespace MagicLamp.PMS.DTOs
{
    public class OrderInfoDTO
    {
        public OrderPackEntity orderPack { get; set; }
        public bool isExport { get; set;}
        public DateTime? orderCreateTime { get; set; }  
    }
}