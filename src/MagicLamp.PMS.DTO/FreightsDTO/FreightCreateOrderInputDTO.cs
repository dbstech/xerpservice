﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class FreightCreateOrderInputDTO
    {

        public string AppKey { get; set; }
        public string AppSecret { get; set; }
        /// <summary>
        /// 物流公司标识
        /// </summary>
        public string FreightKey { get; set; }

        /// <summary>
        /// erp订单号
        /// </summary>
        public string OrderID { get; set; }

        /// <summary>
        /// 物流运单追踪号
        /// </summary>
        public string FreightTrackNo { get; set; }

        public decimal ParcelWeight { get; set; }
        /// <summary> 
        /// ERP包裹编号
        /// </summary>
        public string ParcelNo { get; set; }
        public string SenderName { get; set; }
        public string SenderPhone { get; set; }
        public string SenderAddress { get; set; }
        public string SenderPostCode { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverPhone { get; set; }
        public string ReceiverTown { get; set; }
        public string ReceiverCountry { get; set; }
        public string ReceiverProvince { get; set; }
        public string ReceiverCity { get; set; }
        public string ReceiverDistrict { get; set; }
        public string ReceiverAddress { get; set; }
        public string ReceiverPostCode { get; set; }
        /// <summary>
        /// 收件人身份证号
        /// </summary>
        public string ReceiverIdNo { get; set; }

        /// <summary>
        /// 商品详情信息
        /// </summary>
        public List<GoodsItemDTO> GoodItems { get; set; }


        /// <summary>
        /// 线路：0-普线 1-快线
        /// </summary>
        public string ShippingMethod { get; set; }

        /// <summary>
        /// 面单备注
        /// </summary>
        public string ShippingLabelRemark { get; set; }

        /// <summary>
        /// 订单备注（部分物流支持）
        /// </summary>
        public string OrderRemark { get; set; }
    }


}
