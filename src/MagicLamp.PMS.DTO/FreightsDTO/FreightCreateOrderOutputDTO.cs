﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class FreightCreateOrderOutputDTO
    {

        public string Code { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
        /// <summary>
        /// 快递面单
        /// </summary>
        public string CourierReciept { get; set; }

        /// <summary>
        /// 快递单号
        /// </summary>
        public string CourierTrackNo { get; set; }
    }
}
