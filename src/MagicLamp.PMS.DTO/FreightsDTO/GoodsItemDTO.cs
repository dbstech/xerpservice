﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class GoodsItemDTO
    {

        public string Name { get; set; }
        public string SKU { get; set; }
        public string Quantity { get; set; }
        public string Unit { get; set; }
        public decimal Price { get; set; }
        public decimal Weight { get; set; }
        public string Barcode { get; set; }

    }


}
