using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace MagicLamp.PMS.DTOs
{
    public class SkuLockQtyDto
    {
        public string Sku { get; set; }
        public string LockType { get; set; }
        public string OrderId { get; set; }
        public string OrderPackNo { get; set; }
        public string ProductName { get; set; }
        public bool? Oversale { get; set; }
        public int LockedQty { get; set; }
        private DateTime? _createdAt;

        public DateTime? CreatedAt
        {
            get
            {
                return _createdAt;
            }
            set
            {
                _createdAt = value?.ToUniversalTime();
            }
        }
    }
}