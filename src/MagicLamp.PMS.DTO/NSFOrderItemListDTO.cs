﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class NSFOrderItemListDTO
    {
        public string GoodsName { get; set; }
        public int Number { get; set; }
        public decimal Weight { get; set; }
    }
}
