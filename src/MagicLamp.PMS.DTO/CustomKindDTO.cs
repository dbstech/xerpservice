using Abp.Application.Services.Dto;

namespace MagicLamp.PMS.DTOs
{
    public class CustomKindDTO : EntityDto<string>
    {
        public string kind { get; set; }
        public string name { get; set; }
        public string notes { get; set; }
        public decimal? lmtTax { get; set; }
        public decimal? lmtWeight { get; set; }
        public int? lmtQty { get; set; }
        public int? lmtQtyGrp { get; set; }
        public int? lv { get; set; }

    }
}
