﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.ThirdPart
{
    public class AUExpressUploadIDCardDTO
    {
        public string OrderPackNo { get; set; }

        /// <summary>
        /// 澳邮运单号
        /// </summary>
        public List<string> OrderIDs { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverPhone { get; set; }
        public string PhotoID { get; set; }
        public string PhotoFront { get; set; }
        public string PhotoRear { get; set; }

    }


}
