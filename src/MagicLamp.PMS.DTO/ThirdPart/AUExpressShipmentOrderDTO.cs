﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.ThirdPart
{
    public class AUExpressShipmentOrderDTO
    {
        /// <summary>
        /// erp订单号
        /// </summary>
        public string ERPOrderID { get; set; }
        public int Opid { get; set; }
        public int Seq { get; set; }
        public int MemberId { get; set; }
        public int BrandId { get; set; }
        public string SenderName { get; set; }
        public string SenderPhone { get; set; }
        public string SenderAddr1 { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverPhone { get; set; }
        public string ReceiverCountry { get; set; }
        public string ReceiverProvince { get; set; }
        public string ReceiverCity { get; set; }
        public string ReceiverDistrict { get; set; }
        public string ReceiverAddr1 { get; set; }
        /// <summary>
        /// 订单主表上的地址（不含省市区）
        /// </summary>
        public string Town { get; set; }
        public string ReceiverPostCode { get; set; }
        public string ReceiverPhotoId { get; set; }
        public string ShipmentContent { get; set; }
        public string OrderShipment { get; set; }
    }
}
