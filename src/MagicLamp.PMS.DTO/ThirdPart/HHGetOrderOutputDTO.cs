﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.ThirdPart
{
    public class HHGetOrderOutputDTO
    {
        public int result { get; set; }
        public string message { get; set; }
        public List<HHSKUWithQty> data { get; set; }
    }

    public class HHSKUWithQty
    {
        public string sku { get; set; }
        public decimal price { get; set; }
        public decimal qty { get; set; }
    }
}
