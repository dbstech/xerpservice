﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.ThirdPart
{
    public class ChangJiangAPIOutputDTO
    {

        //{"Success":false,"Message":"当前 ApiKey 未开通","Data":{},"ResultType":0}
        public bool Success { get; set; }
        public string Message { get; set; }
        public dynamic Data { get; set; }
        public int ResultType { get; set; }

    }


}
