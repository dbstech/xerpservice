﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.ThirdPart
{
    public class AUExpressShipmentOrderResponseDTO
    {
        public bool IsSuccess { get; set; }
        public int Code { get; set; }
        public string ErrorMsg { get; set; }
        public string Message { get; set; }
        public List<AUExpressShipmentOrderInfoDTO> OrderInfos { get; set; }
    }

    public class AUExpressShipmentOrderInfoDTO
    {
        public string ERPOrderID { get; set; }
        public string AUExpressShipmentOrderID { get; set; }

    }
}
