﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.ThirdPart
{
    public class HHSyncOutboundDateInputDTO
    {
        public string order_no { get; set; }
        public string delivery_date { get; set; }
        public decimal package_weight { get; set; }
        public string express_no { get; set; }
        public string express_company { get; set; }
    }
}
