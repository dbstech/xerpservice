﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.ThirdPart
{
    public class HHCreateOrderResultDTO
    {
        public int result { get; set; }
        public string message { get; set; }
        public List<HHSKUInfo> data { get; set; }
        public string OrderPackNo { get; set; }
    }
    
    public class HHSKUInfo
    {
        public string sku { get; set; }
        public decimal price { get; set; }
    }
}
