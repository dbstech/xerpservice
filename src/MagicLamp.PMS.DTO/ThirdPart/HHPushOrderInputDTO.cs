﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.ThirdPart
{
    public class HHPushOrderInputDTO
    {
        public string order_no { get; set; }
        public string origin { get; set; }
        public List<HHOrderLineDTO> order_lines { get; set; }
    }

    public class HHOrderLineDTO
    {
        public string sku { get; set; }
        public int qty { get; set; }
    }
}
