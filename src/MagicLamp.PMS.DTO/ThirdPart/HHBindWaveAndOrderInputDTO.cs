﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.ThirdPart
{
    public class HHBindWaveAndOrderInputDTO
    {
        public string wave_picking_no { get; set; }
        /// <summary>
        /// format must be yyyy-MM-dd HH:mm:ss
        /// </summary>
        public string create_time { get; set; }
        public List<string> orders { get; set; }
    }
}
