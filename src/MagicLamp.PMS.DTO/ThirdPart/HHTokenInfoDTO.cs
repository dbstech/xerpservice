﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.ThirdPart
{
    public class HHTokenInfoDTO
    {
        public string access_token { get; set; }
        public string access_token_validity { get; set; }
        public string token_type { get; set; }
        public string refresh_token { get; set; }
    }
}
