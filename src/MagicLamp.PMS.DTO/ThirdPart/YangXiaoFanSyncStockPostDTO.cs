﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.ThirdPart
{
    public class YangXiaoFanSyncStockPostDTO
    {
        public string key { get; set; }
        public List<SyncAladdinStockDTO> products { get; set; }
    }
}
