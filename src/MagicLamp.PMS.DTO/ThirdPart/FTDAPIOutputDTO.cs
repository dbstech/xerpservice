﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.ThirdPart
{
    public class FTDAPIOutputDTO
    {
        public bool success { get; set; }
        public string data { get; set; }
    }
}
