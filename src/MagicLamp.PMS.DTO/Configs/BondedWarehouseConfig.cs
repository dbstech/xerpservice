namespace MagicLamp.PMS.DTOs.Configs
{
    public class BondedWarehouseConfig
    {
        public string Url { get; set; }
        public string IdCode { get; set; }
        public string IdCode2 { get; set; }
    }
}
