﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Configs
{
    public class HHConfig
    {
        public string Url { get; set; }
        public string APIKey { get; set; }
        public string APISecret { get; set; }
        public string RefreshToken { get; set; }
        public string AccessTokenUrl { get; set; }
    }
}
