﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Configs
{
    public class NSFConfig
    {
        public string Token_url { get; set; }
        public string Order_url { get; set; }
        public string Carrier_id_url_submit { get; set; }
        public string Carrier_id_url_apply { get; set; }
        public string NZ_token_key { get; set; }
        public string NZ_account_user { get; set; }
        public string NZ_account_password { get; set; }
        public string NZ_account_net_point_id { get; set; }
        public string AU_token_key { get; set; }
        public string AU_account_user { get; set; }
        public string AU_account_password { get; set; }
        public string AU_account_net_point_id { get; set; }
        public string Flux_pic_path { get; set; }
        public string Mfht_key { get; set; }
        public string Nzgo_key { get; set; }

    }
}
