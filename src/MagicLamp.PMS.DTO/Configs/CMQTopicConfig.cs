﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Configs
{
    public class CMQTopicConfig
    {

        public string TOPIC_SKUINFO { get; set; }
        // public string TOPIC_SKUPRICEINFO { get; set; }
        // public string TOPIC_BRANDINFO { get; set; }
        public string TOPIC_ORDERNOTIFICATION { get; set; }
        // public string TOPIC_TEAMBITIONNOTIFICATION { get; set; }


    }
}
