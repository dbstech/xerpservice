﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Configs
{
    public class BusinessConfig
    {

        #region aliyun oss config
        public string IDCardOSSBuketName { get; set; }
        public string IDCardAPPKey { get; set; }
        public string IDCardAPPSecret { get; set; }
        public string IDCardEndPoint { get; set; }

        public string BasicResourceBuketName { get; set; }
        public string BasicResourceAPPKey { get; set; }
        public string BasicResourceAPPSecret { get; set; }
        public string BasicResourceEndPoint { get; set; }
        #endregion


        public string AccessControlAllowOrigin { get; set; }

        public string DDAppID { get; set; }

        public string DDAppSecret { get; set; }
        public bool PushDDNotification { get; set; }


        #region ydt

        public string YdtAPIDomain { get; set; }
        public string YdtAPPID { get; set; }
        public string YdtAppSecret { get; set; }


        #endregion


        #region HealthElementWCFService

        public string HealthElementWCFServiceKey { get; set; }
        public string HealthElementWCFAddress { get; set; }


        #endregion

    }
}
