﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Configs
{
    public class PictureAddressConfig
    {
        public string Url { get; set; }
    }
}
