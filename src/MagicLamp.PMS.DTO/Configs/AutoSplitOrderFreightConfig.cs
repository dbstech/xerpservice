﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MagicLamp.PMS.DTOs.Configs
{
    public class AutoSplitOrderFreightConfig
    {
        public int ExportFlag { get; set; }
        public string FreightID { get; set; }
        public string PDFTemplateFile { get; set; }
        /// <summary>
        /// 是否生成为澳邮渠道
        /// </summary>
        public bool IsAUExpress { get; set; }

        /// <summary>
        /// 是否为AU新顺丰发货
        /// </summary>
        public bool IsAUNSFExpress { get; set; }
        /// <summary>
        /// 新顺丰EMS面单模板
        /// </summary>
        public string NSFEMSTemplateFile { get; set; }

        /// <summary>
        /// 是否不需要生成物流单号
        /// </summary>
        public bool NoNeedGenerateExpress { get; set; }

        /// <summary>
        /// 是否为华夏物流渠道
        /// </summary>
        public bool IsHuaXiaExpress { get; set; }

        /// <summary>
        /// 是否对接第三方物流API
        /// </summary>
        public bool IsCreateOrderByAPI { get; set; }

        /// <summary>
        /// 第三方物流公司API AppKey
        /// </summary>
        public string ThirdPartyAPIAppKey { get; set; }

        /// <summary>
        /// 第三方物流公司API AppSecret
        /// </summary>
        public string ThirdPartyAPIAppSecret { get; set; }


        /// <summary>
        /// 是否不需要推送到富勒
        /// </summary>
        public bool NoNeedSyncToFlux { get; set; }

        /// <summary>
        /// 不含CCIC电子面单模板
        /// </summary>
        public string NoCCICTemplate { get; set; }

    }
}