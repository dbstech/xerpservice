namespace MagicLamp.PMS.DTOs.Configs
{
    public class YiTongBaoOMSConfig
    {
        public string Url { get; set; }
        public string AppKey { get; set; }
        public string AppSecret { get; set; }
        public string EbcOmsCode { get; set; }
    }
}
