﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Configs
{
    public class ChangJiangConfig
    {
        public string Url { get; set; }
        public string APIKey { get; set; }
        public string APISecret { get; set; }
    }
}
