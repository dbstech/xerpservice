﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Configs
{
    public class FluxConfig
    {


        public string Client_customerid { get; set; }
        public string Url { get; set; }
        public string Apptoken { get; set; }
        public string Appkey { get; set; }
        public string Client_db { get; set; }


    }
}
