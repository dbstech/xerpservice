﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Configs
{
    public class EShopConfig
    {
        public string Url { get; set; }
        public string Key { get; set; }
    }
}
