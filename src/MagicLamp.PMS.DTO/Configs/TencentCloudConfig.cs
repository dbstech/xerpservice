﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Configs
{
    public class TencentCloudConfig
    {
        public string APPSecretID { get; set; }
        public string APPSecretKey { get; set; }
        public string CLSDomain { get; set; }
        public string CLSLogSetID { get; set; }
        public string CLSLogTopicID { get; set; }
    }
}
