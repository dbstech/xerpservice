﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Configs
{
    public class AUExpressOrderConfig
    {
        public int MemberId { get; set; }
        public int BrandId { get; set; }
        public string SenderName { get; set; }
        public string SenderPhone { get; set; }
        public string SenderAddr1 { get; set; }
        public int ExportFlag { get; set; }
        public string PrefixContent { get; set; }
    }
}
