using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using MagicLamp.PMS.Infrastructure;

namespace MagicLamp.PMS.DTOs
{
    public class SyncProductCostPriceFilterDTO
    {
        [SupportMultipleQuery]
        public string SKU { get; set; }
        private DateTime? _creationTimeBegin;
        public DateTime? CreationTimeBegin { 
            get
            {
                return _creationTimeBegin;
            }
            set
            {
                _creationTimeBegin = value?.ToUniversalTime();
            } 

        }
        private DateTime? _creationTimeEnd;
        public DateTime? CreationTimeEnd { 
            get
            {
                return _creationTimeEnd;
            }
            set
            {
                _creationTimeEnd = value?.ToUniversalTime();
            } 
        }
        public string Barcode { get; set; }
        public int? BrandId { get; set; }
        public string ProductId { get; set; }
        public string Display { get; set; }
    }

}
