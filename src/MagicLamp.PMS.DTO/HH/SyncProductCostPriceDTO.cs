﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(SyncProductCostPriceEntity))]
    public class SyncProductCostPriceDTO: EntityDto<long>
    {

        public string SKU { get; set; }
        public decimal BeforePrice { get; set; }
        public decimal AfterPrice { get; set; }
        public DateTime CreationTime { get; set; }
        public decimal? PriceGap { get; set; }
        public string ProductName { get; set; }
    
        public string Barcode { get; set; }
        public string Brand { get; set; }
    }

}
