using MagicLamp.PMS.DTOs.Attributes;
using MagicLamp.PMS.Infrastructure;

namespace MagicLamp.PMS.DTOs
{
    public class BrandCategoryFilterDTO
    {
        [SupportFuzzyQuery]
        public string Name { get; set; }
    }
}