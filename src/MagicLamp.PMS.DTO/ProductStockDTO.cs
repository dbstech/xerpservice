﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class ProductStockDTO
    {
        public string Sku { get; set; }
        public int Qty_available { get; set; }
        public int LockQty { get; set; }
        public int Stock { get; set; }  //spark add stock 19/06/2022

        public string ExpiredDate { get; set; }
        public string OriginalBestBefore { get; set; }
        public string OriginalHHBestBefore { get; set; }
        public int OriginalStock { get; set; }
        public int? OriginalHHStock { get; set; }
    }
    public class ProductStockChangedDTO
    {
        [Key]
        public string Sku { get; set; }
        public int Qty_availableBefore { get; set; }
        public int Qty_upload { get; set; }
        public int Qty_availableAfter { get; set; }

    }
}
