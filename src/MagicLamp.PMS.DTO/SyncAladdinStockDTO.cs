﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class SyncAladdinStockDTO
    {
        public string sku { get; set; }
        public int stock { get; set; }
        public string best_before { get; set; }
        public string hh_best_before { get; set; }
        /// <summary>
        /// refer to qty_available
        /// </summary>
        public int original_stock { get; set; }
        /// <summary>
        /// refer to qty_hh
        /// </summary>
        public int? original_hh_stock { get; set; }
        /// <summary>
        /// calculated HH stock
        /// </summary>
        public int? hh_stock { get; set; }
        /// <summary>
        /// refer to expired_date
        /// </summary>
        public string original_best_before { get; set; }
        /// <summary>
        /// refer to expiry_date_hh
        /// </summary>
        public string original_hh_best_before { get; set; }

    }
}
