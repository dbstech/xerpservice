﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using AutoMapper.Configuration.Conventions;
using MagicLamp.PMS.DTOs.Attributes;
using MagicLamp.PMS.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using MagicLamp.PMS.Infrastructure;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(BrandEntity))]
    public class BrandDTO : EntityDto<int>
    {
        private string _shortCode;
        [SupportFuzzyQuery]
        public string ShortCode
        {
            get
            {
                return _shortCode;
            }
            set
            {
                _shortCode = value?.Trim() ?? string.Empty;
            }
        }
        public bool? ActiveFlag { get; set; }

        private string _cnBrand;
        [MapTo("ChineseName")]
        [SupportFuzzyQuery]
        public string CnBrand
        {
            get
            {
                return _cnBrand;
            }
            set
            {
                _cnBrand = value?.Trim() ?? null;
            }
        }

        private string _enBrand;
        [MapTo("EnglishName")]
        [SupportFuzzyQuery]
        public string EnBrand
        {
            get
            {
                return _enBrand;
            }
            set
            {
                _enBrand = value?.Trim() ?? null;
            }
        }
        public string Basis { get; set; }

        private List<FileDTO> _AuthorizationFiles = null;
        [NotMapped]
        [HasFile]
        public List<FileDTO> AuthorizationFiles
        {
            get
            {
                if (_AuthorizationFiles == null)
                {
                    _AuthorizationFiles = new List<FileDTO>();
                }
                return _AuthorizationFiles;
            }
            set { _AuthorizationFiles = value; }
        }

        private List<ImageDTO> _Banners = null;
        [NotMapped]
        [HasImage()]
        public List<ImageDTO> Banners
        {
            get
            {
                if (_Banners == null)
                {
                    _Banners = new List<ImageDTO>();
                }
                return _Banners;
            }
            set { _Banners = value; }
        }

        private List<ImageDTO> _Logos = null;
        [NotMapped]
        [HasImage()]
        public List<ImageDTO> Logos
        {
            get
            {
                if (_Logos == null)
                {
                    _Logos = new List<ImageDTO>();
                }
                return _Logos;
            }
            set { _Logos = value; }
        }
        public DateTime CreateTime { get; set; }
        public string CreateUser { get; set; }
        public long? OdooBrandId { get; set; } = null;
        public int? BrandCategoryId { get; set; }
        public string BrandCategoryName { get; set; }
    }
}
