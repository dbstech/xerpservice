﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class BatchGetProductStockInputDTO
    {

        public List<string> SKUs { get; set; }

        public string Partner { get; set; }

    }


}
