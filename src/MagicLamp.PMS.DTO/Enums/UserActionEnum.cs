namespace MagicLamp.PMS.DTOs.Enums
{
    public enum UserActionEnum
    {
        Unknown = 0,
        Create = 1,
        Read = 2,
        Update = 3,
        Delete = 4,
        UploadFile = 5,
    }
}
