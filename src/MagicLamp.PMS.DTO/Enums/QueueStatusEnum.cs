﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Enums
{
    public enum QueueStatusEnum
    {
        Created = 0,
        Done = 1,
        OutOfStock = 2,
        Refund = 3,
        OrderFailed = 4,
        BindFailed = 5,
        CachedInHH = 6
    }
}
