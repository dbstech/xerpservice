﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Enums
{
    public enum FluxReturnFlagEnum
    {
        NoResponse = -1,
        Failed = 0,
        Success = 1,
        PartialSuccess = 2
    }
}
