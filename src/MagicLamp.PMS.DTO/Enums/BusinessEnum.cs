﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Enums
{
    public enum BusinessEnum
    {
        yishan = 1,
        Taobao = 2,
        TMall = 3,
        YMT = 4,
        APP = 5,
        TMall2 = 6,
        YMT2 = 7,
        YBHGNM = 8,
        Other = 9,
        Milk = 10,
        Goodhealth = 11,
        AU秦岭 = 12,
        Wanjia = 13,
        Healtheries海外旗舰店 = 14,
        网易考拉 = 15,
        yunhou = 16,
        见证健康 = 17,
        GreenZoo = 18,
        中免德鸿 = 19,
        HEWEB = 20,
        Merino海外旗舰店 = 21,
        Vogels海外旗舰店 = 22,
        lindenleaves海外旗舰店 = 23,
        RenRenDian = 24,
        越洋猫 = 25,
        RedBook = 26,
        JingDong = 27,
        StoreDelivery = 28,
        StoreDirectPost = 29,
        ChainWholeSale = 30,
        ddlm = 31,
        LifeStream海外旗舰店 = 32,
        中旅 = 33,
        wechatmall = 35,
        he = 99,
        motan = 888,
        nzgo = 999
    }
}
