﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTO.Enums
{
    public enum DownloadTaskStatusEnum
    {

        UNKNOW = 0,
        SUCCESS = 1,
        FAILED = 2,
        PROCESSING = 3

    }
}
