﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Enums
{
    public enum ProductImageTypeEnum
    {

        Unknow = 0,
        Main = 1,
        Detail = 2,

    }
}
