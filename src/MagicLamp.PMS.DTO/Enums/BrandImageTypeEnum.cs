﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Enums
{
    public enum BrandImageTypeEnum
    {

        Unknow = 0,
        Logo = 1,
        Banner = 2,

    }
}
