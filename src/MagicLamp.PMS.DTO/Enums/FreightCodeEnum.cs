﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Enums
{
    public enum FreightCodeEnum
    {

        Unknow = 0,
        AUExpress = 1,
        ChangJiangExpress = 2,

        //FTD
        FTD = 3,
        FTDEXPRESSML =4,
        FTDSF = 5,
        HYFTDMILK = 6,

        //FENGTIAN
        FENGTIAN=7,

    }

}
