﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Enums
{
    public enum MqActionEnum
    {

        Unknow = 0,
        Create = 1,
        Update = 2,
        Delete = 3
    }
}
