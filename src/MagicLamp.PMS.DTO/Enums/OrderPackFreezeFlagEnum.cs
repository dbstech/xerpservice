﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Enums
{
    public enum OrderPackFreezeFlagEnum
    {
        未冻结 = 0,
        作废 = 1,
        整单退款 = 2,
        缺货退款 = 3,
        换货退款 = 4,
        换货补款 = 5,
        无差价换货 = 6,
        信息待确认 = 7,
        改标志 = 8,
        其他 = 9
    }
}
