﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Enums
{
    public enum OrderLogFlagEnum
    {
        订单处理环节 = 0,
        客服跟踪环节 = 1,
        缺货跟踪环节 = 2,
        破损丢失环节 = 3,
        退补款环节 = 4,
        重发环节 = 5
    }
}
