﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public enum NSFBackgroundJobActionEnum
    {
        Unknown = 0,
        CreateOrder = 1,
        Login = 2
    }
}
