using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Enums
{
    public enum OdooSyncStatusEnum
    {
        Success = 0,
        Failed = 1,
        Pending = 2,
    }
}
