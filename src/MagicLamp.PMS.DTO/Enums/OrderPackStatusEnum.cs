﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Enums
{
    public enum OrderPackStatusEnum
    {
        /// <summary>
        /// 未配
        /// </summary>
        Unassigned = 0,
        /// <summary>
        /// 已配
        /// </summary>
        Assigned = 1,
        /// <summary>
        /// 缺货
        /// </summary>
        OutOfStock = 2,
        /// <summary>
        /// 已包
        /// </summary>
        Packed = 3,
        /// <summary>
        /// 已出库
        /// </summary>
        Outbound = 4
    }
}
