﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs.Enums
{
    public enum OrderPackReservedStatusEnum
    {
        Refund = 0,
        Created = 1,
        Outbound = 2,
        Binded = 3
    }
}
