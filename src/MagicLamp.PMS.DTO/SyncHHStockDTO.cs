﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class SyncHHStockDTO
    {
        /// <summary>
        /// SKU编号
        /// </summary>
        public string SKU { get; set; }
        /// <summary>
        /// 库存数量
        /// </summary>
        public int StockQty { get; set; }
        /// <summary>
        /// Barcode
        /// </summary>
        public string Barcode { get; set; }
        /// <summary>
        /// 保质期
        /// </summary>
        public string ExpiryDate { get; set; }
        /// <summary>
        /// 批次号
        /// </summary>
        public string Pseq { get; set; }
        public decimal Price { get; set; }
        public decimal WholesalePrice { get; set; }
    }
}
