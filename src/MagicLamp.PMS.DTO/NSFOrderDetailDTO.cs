﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class NSFOrderDetailDTO
    {
        public string product_name { set; get; }
        public int Number { set; get; }
        public decimal Weight { set; get; }
        /// <summary>
        /// 1: 有效SKU; 0: 无效SKU
        /// </summary>
        public int flag { set; get; }
    }
}
