﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class SimpleSKUWithFlagDTO
    {
        public string SKU { get; set; }
        public int Qty { get; set; }
        public bool AllowOverselling { get; set; }
        public int Stock { get; set; }
        public int LackQty { get; set; }
        public int OriginalStock { get; set; }
        public int? OriginalHHStock { get; set; }
        public SimpleSKUWithFlagDTO()
        {
            OriginalHHStock = 0; // 设置默认值为0
        }
    }
}
