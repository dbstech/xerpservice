﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class CJNewOrderDTO
    {
        /// <summary>
        /// 寄件人姓名
        /// </summary>
        public string s_name { get; set; }
        /// <summary>
        /// 寄件人电话
        /// </summary>
        public string s_mobile { get; set; }
        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string r_name { get; set; }
        /// <summary>
        /// 收件人电话
        /// </summary>
        public string r_mobile { get; set; }
        /// <summary>
        /// 收件人省（必须为标准省市区）
        /// </summary>
        public string r_province { get; set; }
        /// <summary>
        /// 收件人市（必须为标准省市区）
        /// </summary>
        public string r_city { get; set; }
        /// <summary>
        /// 收件人区（必须为标准省市区）
        /// </summary>
        public string r_county { get; set; }
        /// <summary>
        /// 收件人详细地址
        /// </summary>
        public string r_address { get; set; }
        /// <summary>
        /// 线路：0-普线 1-快线
        /// </summary>
        public string line { get; set; }
        /// <summary>
        /// 包裹总数
        /// </summary>
        public int count { get; set; }
        /// <summary>
        /// items_{p}, p 为包裹序号，起始号码为 1。items_1 即表示包裹 1 的物件
        /// </summary>
        public List<string> items { get; set; }
        /// <summary>
        /// 备注，此备注将会打印到热敏单，不可修改
        /// </summary>
        public string memo { get; set; }
        /// <summary>
        /// 系统备注，此备注可在大客户系统修改，自我管理
        /// </summary>
        public string sys_memo { get; set; }
        /// <summary>
        /// 可以不传，重量为克，多包裹时，用英文逗号分隔
        /// </summary>
        //public string weights { get; set; }
        /// <summary>
        /// 可以不传，客户单号，多包裹时，用英文逗号分隔
        /// </summary>
        //public string custom_numbers { get; set; }
    }
}
