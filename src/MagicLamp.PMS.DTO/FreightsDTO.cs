﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(FreightsEntity))]
    public class FreightsDTO : EntityDto<string>
    {
        public string id { get; set; }
        public string name { get; set; }
        public string notes { get; set; }
        public string prefix { get; set; }
        public int? code { get; set; }
        public int? size { get; set; }
        public string CurrencyCode { get; set; }
        public string FreightSet { get; set; }
        public string Currency { get; set; }
        public decimal costPerKg { get; set; }
        public decimal pricePerKg { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string FreightSetName { get; set; }
        public bool? Active { get; set; }
    }
}
