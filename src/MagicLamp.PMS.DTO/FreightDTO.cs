﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(FreightEntity))]
    public class FreightDTO : EntityDto<string>
    {

        public string FreightID { get; set; }
        public string Name { get; set; }
        public string FreightSet { get; set; }
    }
}
