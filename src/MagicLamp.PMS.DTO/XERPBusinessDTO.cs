﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(XERPBusinessEntity))]
    public class XERPBusinessDTO : EntityDto<string>
    {


        public string Name { get; set; }
        //public string APIKey { get; set; }
        public bool Active { get; set; }
        public string DisplayName { get; set; }

    }
}
