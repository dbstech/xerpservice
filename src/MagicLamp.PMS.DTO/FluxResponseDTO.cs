﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    /// <summary>
    /// Flux WMS请求处理结果
    /// </summary>
    public class FluxResponseDTO
    {
        /// <summary>
        /// 成功或失败标记。1成功，0失败，2部分成功
        /// </summary>
        public string returnFlag { get; set; }
        /// <summary>
        /// 失败代码
        /// </summary>
        public string returnCode { get; set; }
        /// <summary>
        /// 失败原因描述
        /// </summary>
        public string returnDesc { get; set; }
        /// <summary>
        /// 错误结果列表（字符串数组）
        /// </summary>
        public string resultInfo { get; set; }
    }
}
