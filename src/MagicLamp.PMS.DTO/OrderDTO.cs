﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(OrderEntity))]
    public class OrderDTO : EntityDto<int>
    {
        public int oid { get; set; }
        public string orderid { get; set; }
        public string store { get; set; }
        public string rec_name { get; set; }
        public string rec_addr { get; set; }
        public string rec_phone { get; set; }
        public string idno { get; set; }
        public string shipment { get; set; }
        public string notes { get; set; }
        public int seq { get; set; }
        public int? status { get; set; }
        public DateTime? update_time { get; set; }
        public DateTime? sendtime { get; set; }
        public DateTime? ordercreate_time { get; set; }
        public string province { get; set; }
        public string city { get; set; }
        public string district { get; set; }
        public string town { get; set; }
        public bool? ISGetCost { get; set; }
        public string partner { get; set; }
        public bool? IsSelectedCCIC { get; set; }
    }
}
