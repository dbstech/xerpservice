﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class OutboundNotifyHHInputDTO
    {
        public string OrderNo { get; set; }
        public string OrderType { get; set; }
        public string CustomerID { get; set; }
        public string WarehouseID { get; set; }
        public string Status { get; set; }
        public decimal Weight { get; set; }
        public string CarrierId { get; set; }
        public string CarrierName { get; set; }

        public DateTime OutBoundTime { get; set; }
    }


}
