﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(CustomsUnitEntity))]
    public class CustomsUnitDTO : EntityDto<int>
    {
        public int id { get; set; }
        public string unit_measure { get; set; }
        public int? tax_standard { get; set; }
        public DateTime? Cdate { get; set; } 
    }
}
