﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class CJIDCardInfoDTO
    {
        /// <summary>
        /// 姓名，加密时无需 UrlEncode
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 手机，若为空，请传空字符串，不能忽略参数
        /// </summary>
        public string mobile { get; set; }
        /// <summary>
        /// 身份证号
        /// </summary>
        public string idcard { get; set; }
        /// <summary>
        /// 图片二进制的 Base64 字符串
        /// </summary>
        public string data { get; set; }
        /// <summary>
        /// 图片类型，a 为身份证正面，b 为身份证反面
        /// </summary>
        public string type { get; set; }
    }
}
