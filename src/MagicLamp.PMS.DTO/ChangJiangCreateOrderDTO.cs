using Abp.AutoMapper;
using System.Collections.Generic;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(FreightCreateOrderInputDTO))]
    public class ChangJiangCreateOrderDTO
    {

        public string AppKey { get; set; }
        public string AppSecret { get; set; }
        public string FreightKey { get; set; }
        public string FreightTrackNo { get; set; }
        public decimal? ParcelWeight { get; set; }
        public string OrderID { get; set; }
        public string ParcelNo { get; set; }
        public string SenderName { get; set; }
        public string SenderPhone { get; set; }
        public string SenderAddress { get; set; }
        public string SenderPostCode { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverPhone { get; set; }
        public string ReceiverTown { get; set; }
        public string ReceiverCountry { get; set; }
        public string ReceiverProvince { get; set; }
        public string ReceiverCity { get; set; }
        public string ReceiverDistrict { get; set; }
        public string ReceiverAddress { get; set; }
        public string ReceiverPostCode { get; set; }
        public string ReceiverIdNo { get; set; }
        public List<GoodsItemDTO> GoodItems { get; set; }
        public int Seq { get; set; }
        public int OPId { get; set; }
        public string ProductName { get; set; }
        public int Qty { get; set; }
        public string SKU { get; set; }
        public decimal? ProductPrice { get; set; }
        public decimal? ProductWeight { get; set; }
    }


}
