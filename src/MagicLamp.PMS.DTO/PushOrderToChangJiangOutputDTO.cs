using System;
using System.Collections.Generic;

namespace MagicLamp.PMS.DTOs
{
    public class PushOrderToChangJiangOutputDTO
    {
        public int PushedOrdersCount { get; set; }
        public int FailedOrdersCount { get; set; }
        public string FailedOrders { get; set; }
        public List<string> CarrierIdsNotFound { get; set; }
    }

}


