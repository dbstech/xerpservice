﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;
using MagicLamp.PMS.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(OrderLogEntity))]
    public class OrderLogDTO : EntityDto<int>
    {
        /// <summary>
        /// order log id
        /// </summary>
        [Display(Name = "olid")]
        public int OLID { get; set; }

        /// <summary>
        /// order id
        /// </summary>
        [Display(Name = "oid")]
        public int? OID { get; set; }

        [Display(Name = "opid")]
        public int? OPID { get; set; }

        [Display(Name = "opt_date")]
        public DateTime OptDate { get; set; }

        [Display(Name = "comments")]
        public string Comments { get; set; }

        /// <summary>
        /// Operator ID
        /// </summary>
        [Display(Name = "opt_id")]
        public Guid OptID { get; set; }

        /// <summary>
        /// Operator Name
        /// </summary>
        [Display(Name = "opt_name")]
        public string OptName { get; set; }
        public string picPath { get; set; }
        public int flag { get; set; }

        public string notes1 { get; set; }
        public string notes2 { get; set; }
        public string notes3 { get; set; }
        [NotMapped]
        public string status { get; set; }

        public int? relFlagVal { get; set; }
        public string relFlagTitle { get; set; }

        public int? relFlag1Val { get; set; }
        public string relFlag1Title { get; set; }

        public int? relFlag2Val { get; set; }
        public string relFlag2Title { get; set; }
    }
}