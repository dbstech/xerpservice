﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.DTOs.Attributes;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(BanktransferEntity))]
    [UploadTemplate(Label = "下载通联提现流水Excel模板", TemplateName = "通联提现流水Excel模板.xls")]

    public class BanktransferDTO : EntityDto<long>
    {
        [SupportFuzzyQuery]
        /// <summary>
        /// 订单号
        /// </summary>
        //[MapTo("OrderNO")]
        [Display(Name = "商户订单号")]
        public string OrderNO { get; set; }
        /// <summary>
        /// 币种
        /// </summary>
        //[MapTo("TargetCurrency")]
        [Display(Name = "汇出币种")]
        public string TargetCurrency { get; set; }
        /// <summary>
        /// 支付方式
        /// </summary>
        [Display(Name = "支付方式")]
        public string PayMethod { get; set; }

        /// <summary>
        /// 汇款日期
        /// </summary>
        //[MapTo("ExchangeDate")]
        [Display(Name = "汇出时间")]
        public string ExchangeDate { get; set; }

        /// <summary>
        /// 汇款汇率
        /// </summary>
       // [MapTo("CurrencyRate")]
        [Display(Name = "汇款时汇率")]
        public string CurrencyRate { get; set; }

        /// <summary>
        /// 汇款金额
        /// </summary>
        //[MapTo("Total")]
        [Display(Name = "汇出外币")]
        public string Total { get; set; }


        /// <summary>
        /// 更新时间
        /// </summary>
        //[MapTo("Total")]
        [Display(Name = "更新时间")]
        public DateTime Updated_at { get; set; } = System.DateTime.UtcNow;
    }
}
