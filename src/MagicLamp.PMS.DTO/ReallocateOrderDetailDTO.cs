﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class ReallocateOrderDetailDTO
    {
        public string currentOrderPackNo { get; set; }
        public string targetOrderPackNo { get; set; }
        public List<int> listOpdid { get; set; }
    }
}
