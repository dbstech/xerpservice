﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class SiLuOutboundOutputDTO
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
