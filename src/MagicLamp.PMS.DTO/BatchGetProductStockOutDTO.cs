﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class BatchGetProductStockOutDTO
    {

        public string Sku { get; set; }
        public int Qty { get; set; }
        public string ExpiredDate { get; set; }


        /// <summary>
        /// refer to qty_available
        /// </summary>
        public int OriginalStock { get; set; }
        /// <summary>
        /// refer to qty_hh
        /// </summary>
        public int? OriginalHHStock { get; set; }
        /// <summary>
        /// refer to expired_date
        /// </summary>
        public string OriginalBestBefore { get; set; }
        /// <summary>
        /// refer to expiry_date_hh
        /// </summary>
        public string OriginalHHBestBefore { get; set; }

    }


}
