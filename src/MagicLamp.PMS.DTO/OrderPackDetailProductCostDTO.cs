﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(OrderPackDetailProductCostEntity))]
    public class OrderPackDetailProductCostDTO : EntityDto<long>
    {
        public string OrderID { get; set; }

        public string SKU { get; set; }

        public int Qty { get; set; }

        public decimal? Cost { get; set; }

        public DateTime CreationTime { get; set; }
        public string Currency { get; set; }
        public DateTime? UpdateTime { get; set; }
        public DateTime PushOrderTime { get; set; }
    }
}
