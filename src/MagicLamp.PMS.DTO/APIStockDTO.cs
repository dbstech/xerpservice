﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(APIStockEntity))]
    public class APIStockDTO : EntityDto<string>
    {
        [SupportMultipleQuery]
        public string SKU { get; set; }
        /// <summary>
        /// 阿拉丁库存
        /// </summary>
        public int? Qty_aladdin { get; set; }
        /// <summary>
        /// 实际库存
        /// </summary>
        public int? Qty { get; set; }
        /// <summary>
        /// 可用库存（阿拉丁库存--锁定库存）
        /// </summary>
        public int? Qty_available { get; set; }
        public int? Qty_baseline { get; set; }
        public int? Qty_heweb { get; set; }
        /// <summary>
        /// 锁定库存
        /// </summary>
        public int? Qty_locked { get; set; }
        /// <summary>
        /// 是否需要同步到销售端
        /// </summary>
        public bool? Syc_flag { get; set; }
        public DateTime Updated_time { get; set; }

        public string Expired_date { get; set; }
        /// <summary>
        /// HH库存数量
        /// </summary>
        public int? Qty_hh { get; set; }
        /// <summary>
        /// HH保质期
        /// </summary>
        public string Expiry_date_hh { get; set; }

        public string ProductNameEN { get; set; }

        public string ProductNameCN { get; set; }

        public int? Flag { get; set; }

        public int? SkuFlag { get; }

        [NotMapped]
        public bool? IsHHPurchase { get; set; }

        [NotMapped]
        public DateTime? CreateTime { get; set; }
        [NotMapped]
        public bool IsVirtualStock { get; set; } = false;

        [NotMapped]
        public List<string> skuList { get; set; }
        
        [NotMapped]
        public bool? NoProduct { get; set; }
        public int? Qty_cache { get; set; }
        public int? Qty_orders { get; set; }
        public int? Qty_orders_pack { get; set; }

        public short? Status { get; set; }
        
    }
}
