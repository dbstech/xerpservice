﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    /// <summary>
    /// 出库单
    /// </summary>
    public class FluxOrderInfoDTO
    {
        /// <summary>
        /// 来源单据号
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 来源单据类型 
        /// </summary>
        public string OrderType { get; set; }
        /// <summary>
        /// 货主ID
        /// </summary>
        public string CustomerID { get; set; }
        /// <summary>
        /// 仓库ID
        /// </summary>
        public string WarehouseID { get; set; }
        /// <summary>
        /// 快递单号
        /// </summary>
        public string DeliveryNo { get; set; }
        /// <summary>
        /// 总重量
        /// </summary>
        public string Weight { get; set; }
        /// <summary>
        /// 承运商ID
        /// </summary>
        public string CarrierId { get; set; }
        /// <summary>
        /// 承运商名称
        /// </summary>
        public string CarrierName { get; set; }
        /// <summary>
        /// 预留字段
        /// </summary>
        public string Soreference1 { get; set; }
        /// <summary>
        /// 预留字段
        /// </summary>
        public string Soreference2 { get; set; }
        /// <summary>
        /// 预留字段
        /// </summary>
        public string Soreference3 { get; set; }
        /// <summary>
        /// 预留字段
        /// </summary>
        public string Soreference4 { get; set; }
        /// <summary>
        /// 预留字段
        /// </summary>
        public string Soreference5 { get; set; }
        /// <summary>
        /// 预留字段
        /// </summary>
        public string Udf1 { get; set; }
        /// <summary>
        /// 出库单明细
        /// </summary>
        //public object[] FluxOrderInfoDetailsDto { get; set; }
    }
}
