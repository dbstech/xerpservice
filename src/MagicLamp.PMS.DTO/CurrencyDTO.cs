﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(CurrencyEntity))]
    public class CurrencyDTO : EntityDto<string>
    {

        public string Code { get; set; }

        public string Name { get; set; }

        public DateTime CreationTime { get; set; } 
    }
}
