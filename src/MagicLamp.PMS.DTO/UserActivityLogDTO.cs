using System.Collections.Generic;
using System;
namespace MagicLamp.PMS.DTOs
{
    public class UserActivityLogDTO
    {
        private DateTime _time;
        public DateTime Time { 
            get
            {
                return _time;
            } 
            set
            {
                _time = DateTime.SpecifyKind(value, DateTimeKind.Utc);
            }
        }
        public string Activity { get; set; }
        public string User { get; set; }
    }
}
