﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class OrderPackProgressActionDTO
    {
        public string action { get; set; }
        public DateTime? time { get; set; }
        public string operatorName { get; set; }
    }

    public class OrderPackProgressActionValueDTO
    {
        private OrderPackProgressActionValueDTO(string value) { Value = value; }

        public string Value { get; set; }

        public static OrderPackProgressActionValueDTO Uploaded { get { return new OrderPackProgressActionValueDTO("订单上传"); } }
        public static OrderPackProgressActionValueDTO Generated { get { return new OrderPackProgressActionValueDTO("生成配货"); } }
        public static OrderPackProgressActionValueDTO CarrierCreated { get { return new OrderPackProgressActionValueDTO("快递打单"); } }
        public static OrderPackProgressActionValueDTO Assigned { get { return new OrderPackProgressActionValueDTO("完全分配"); } }
        public static OrderPackProgressActionValueDTO Picked { get { return new OrderPackProgressActionValueDTO("配货时间"); } }
        public static OrderPackProgressActionValueDTO Packed { get { return new OrderPackProgressActionValueDTO("包装时间"); } }
        public static OrderPackProgressActionValueDTO Outbounded { get { return new OrderPackProgressActionValueDTO("出库时间"); } }
        public static OrderPackProgressActionValueDTO Scanned { get { return new OrderPackProgressActionValueDTO("扫描上传"); } }

    }
}
