﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class SearchIDCardsInputDTO
    {

        public DateTime UpdateBeginTime { get; set; }
        public DateTime EndBeginTime { get; set; }
        public int ExportFlagID { get; set; }
        public string Name { get; set; }
        public string CourierID { get; set; }
        public string OrderID { get; set; }
        public string IDCard { get; set; }
        public int? Verification { get; set; }
        public string FreightID { get; set; }
        public DateTime OrderBeginTime { get; set; }
        public DateTime OrderEndBeginTime { get; set; }


    }
}
