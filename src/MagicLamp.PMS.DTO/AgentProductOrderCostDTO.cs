﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using Npoi.Mapper.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(AgentProductOrderCostEntity))]
    public class AgentProductOrderCostDTO : EntityDto<long>
    {
        [Npoi.Mapper.Attributes.Column("包裹号")]
        [Display(Name = "包裹号")]
        public string OrderPackNo { get; set; }
        [Npoi.Mapper.Attributes.Column("订单号")]
        public string OrderID { get; set; }
        [Npoi.Mapper.Attributes.Column("客户ID")]
        public string CustomerID { get; set; }
        [Npoi.Mapper.Attributes.Column("SKU")]
        [Display(Name = "SKU")]
        public string SKU { get; set; }
        [Npoi.Mapper.Attributes.Column("产品名称")]
        [Display(Name = "产品名称")]
        public string ProductName { get; set; }
        [Npoi.Mapper.Attributes.Column("产品数量")]
        [Display(Name = "产品数量")]
        public int Qty { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        [Npoi.Mapper.Attributes.Column("单价")]
        [Display(Name = "单价")]
        public decimal Cost { get; set; }
        /// <summary>
        /// 总价
        /// </summary>
        [Npoi.Mapper.Attributes.Column("总价")]
        [Display(Name = "总价")]
        public decimal TotalCost { get; set; }
        /// <summary>
        /// 下单时间
        /// </summary>
        [Npoi.Mapper.Attributes.Column("下单时间", CustomFormat = "dd/MM/yyyy hh.mm.ss")]
        [Display(Name = "下单时间")]
        public DateTime OrderTime { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Npoi.Mapper.Attributes.Column("创建时间", CustomFormat = "dd/MM/yyyy hh.mm.ss")]
        [Display(Name = "创建时间")]
        public DateTime CreationTime { get; set; }
        /// <summary>
        /// 出库时间
        /// </summary>
        [Npoi.Mapper.Attributes.Column("出库时间", CustomFormat = "dd/MM/yyyy hh.mm.ss")]
        [Display(Name = "出库时间")]
        public DateTime? OutboundTime { get; set; }
        /// <summary>
        /// 采购币种
        /// </summary>
        [Npoi.Mapper.Attributes.Column("采购币种")]
        [Display(Name = "采购币种")]
        public string PurchasingCurrency { get; set; }
        /// <summary>
        /// 采购价格
        /// </summary>
        [Npoi.Mapper.Attributes.Column("采购价格")]
        [Display(Name = "采购价格")]
        public decimal? PurchasingPrice { get; set; }
        /// <summary>
        /// 基准价格（NZD）
        /// </summary>
        [Npoi.Mapper.Attributes.Column("基准价格（NZD）")]
        [Display(Name = "基准价格（NZD）")]
        public decimal? BenchmarkPrice { get; set; }
        /// <summary>
        /// 基准价格汇率
        /// </summary>
        [Npoi.Mapper.Attributes.Column("基准价格汇率")]
        [Display(Name = "基准价格汇率")]
        public decimal? BenchmarkPriceExchangeRate { get; set; }
        /// <summary>
        /// 供应商
        /// </summary>
        [Npoi.Mapper.Attributes.Column("供应商")]
        [Display(Name = "供应商")]
        public string Agency { get; set; }
        [Ignore]
        [NotMapped]
        public DateTime? CreationTimeBegin { get; set; }
        [Ignore]
        [NotMapped]
        public DateTime? CreationTimeEnd { get; set; }
        [Ignore]
        [NotMapped]
        public DateTime? OrderTimeBegin { get; set; }
        [Ignore]
        [NotMapped]
        public DateTime? OrderTimeEnd { get; set; }
    }
}
