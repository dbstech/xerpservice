﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(GoodsOwnerEntity))]
    public class GoodsOwnerDTO : EntityDto<string>
    {
        public string Code { get; set; }

        public string FluxCode { get; set; }
        public string UserId { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
