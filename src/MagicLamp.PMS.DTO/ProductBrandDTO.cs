﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(ProductBrandEntity))]
    public class ProductBrandDTO : EntityDto<int>
    {
        public string EnBrand { get; set; }
        public string CnBrand { get; set; }
        public string BrandImg { get; set; }
        public string BrandImgM { get; set; }
        public string BrandCode { get; set; }
    }
}
