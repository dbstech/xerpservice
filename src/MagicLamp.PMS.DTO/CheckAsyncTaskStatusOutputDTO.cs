﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class CheckAsyncTaskStatusOutputDTO
    {

        public Guid TaskID { get; set; }

        public string Status { get; set; }


    }


}
