﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MagicLamp.PMS.Infrastructure;
using System.ComponentModel.DataAnnotations.Schema;
using AutoMapper;
using AutoMapper.Configuration.Conventions;
using MagicLamp.PMS.DTOs.Attributes;
using MagicLamp.PMS.Enums;

namespace MagicLamp.PMS.DTOs
{
    [Abp.AutoMapper.AutoMap(typeof(ProductEntity))]
    [UploadTemplate(Label = "下载上新Excel模板", TemplateName = "20190813-XERP产品上新模板.xls")]
    public class ProductDTO : EntityDto<string>
    {
        //[Required(AllowEmptyStrings = false, ErrorMessage = "SKU filed is required")]
        [SupportFuzzyQuery]
        [Display(Name = "SKU")]
        public string SKU { get; set; }

        [Display(Name = "货主")]
        public string CustomerID { get; set; }
        /// <summary>
        /// 品牌Code
        /// </summary>
        // [MapTo("Brand1")]
        public int? BrandId { get; set; }
        /// <summary>
        /// 激活
        /// </summary>
        [Display(Name = "激活")]
        public bool? ActiveFlag { get; set; }
        /// <summary>
        /// 仓库
        /// </summary>
        public int? WarehouseID { get; set; }
        [Display(Name = "仓库")]
        [IgnoreMap]
        public string Warehouse { get; set; }
        [Display(Name = "中文名称")]
        [SupportFuzzyQuery]
        /// <summary>
        /// 名称
        /// </summary>
        public string ChineseName { get; set; }
        [Display(Name = "英文名称")]
        [SupportFuzzyQuery]
        /// <summary>
        /// 英文名称
        /// </summary>
        public string EnglishName { get; set; }
        private decimal? _price { get; set; }
        public decimal? Price
        {
            get
            {
                if (_price == null)
                {
                    _price = 0;
                }
                return _price;
            }
            set { _price = value; }
        }
        [Display(Name = "自定义成本")]
        public decimal? Cost { get; set; }
        /// <summary>
        /// 成本分组ID
        /// </summary>
        [MapTo("CostType")]
        public string CostTypeID { get; set; }
        /// <summary>
        /// 自定义成本
        /// </summary>
        public decimal? DefaultCost { get; set; }
        [Display(Name = "币种")]
        /// <summary>
        /// 币种
        /// </summary>
        [MapTo("Currency")]
        public string CurrencyID { get; set; }
        [Display(Name = "权重(克)")]
        /// <summary>
        /// 毛重
        /// </summary>
        public decimal? GrossWeight { get; set; }
        [Display(Name = "净重(克)")]
        /// <summary>
        /// 净重
        /// </summary>
        public decimal? NetWeight { get; set; }
        [Display(Name = "长(毫米)")]
        /// <summary>
        /// 长
        /// </summary>
        public decimal? Length { get; set; }
        [Display(Name = "宽(毫米)")]
        /// <summary>
        /// 宽
        /// </summary>
        public decimal? Wide { get; set; }
        [Display(Name = "高(毫米)")]
        /// <summary>
        /// 高
        /// </summary>
        public decimal? High { get; set; }
        /// <summary>
        /// 库存数量
        /// </summary>
        public int? Qty { get; set; }
        public int? WarehouseStockFlag { get; set; }
        /// <summary>
        /// 产品供应链分类ID
        /// </summary>
        public int? CategoryID { get; set; }

        [Display(Name = "产品供应链分类")]
        [NotMapped]
        /// <summary>
        /// 产品供应链分类
        /// </summary>
        public string CategoryName { get; set; }
        [Display(Name = "默认发货渠道")]
        /// <summary>
        /// 默认发货渠道
        /// </summary>
        public string DefaultFreightID { get; set; }

        [Display(Name = "说明")]
        /// <summary>
        /// 说明
        /// </summary>
        public string Note { get; set; }

        [Display(Name = "关键字")]
        public string Keywords { get; set; }


        [Display(Name = "条码1")]
        [SupportFuzzyQuery]
        /// <summary>
        /// 条码1
        /// </summary>
        public string Barcode1 { get; set; }
        /// <summary>
        /// 条码2
        /// </summary>
        public string Barcode2 { get; set; }
        /// <summary>
        /// 条码3
        /// </summary>
        public string Barcode3 { get; set; }
        /// <summary>
        /// 条码4
        /// </summary>
        public string Barcode4 { get; set; }
        [Display(Name = "税率")]
        /// <summary>
        /// 税率
        /// </summary>
        public decimal? TaxRate { get; set; }
        [Display(Name = "包装材料")]
        [IgnoreMap]
        public string Materials { get; set; }
        [MapTo("MaterialsID")]
        public int? PackMaterialID { get; set; }
        public decimal? MinThreshold { get; set; }
        public decimal? MaxThreshold { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreateUser { get; set; }
        /// <summary>
        /// 分单分类ID
        /// </summary>
        public int? SplitOrderCategoryID { get; set; }

        [Display(Name = "分单分类")]
        [NotMapped]
        /// <summary>
        /// 分单分类
        /// </summary>
        public string SplitOrderCategoryName { get; set; }

        [Display(Name = "库存类型")]
        /// <summary>
        ///采购账期code
        /// </summary>
        public string PurchasePeriodCode { get; set; }

        private List<ImageDTO> _mainImages = null;
        [NotMapped]
        public List<ImageDTO> MainImages
        {
            get
            {
                if (_mainImages == null)
                {
                    _mainImages = new List<ImageDTO>();
                }
                return _mainImages;
            }
            set
            {
                _mainImages = value;
            }
        }


        private List<ImageDTO> _detailImages = null;
        [NotMapped]
        public List<ImageDTO> DetailImages
        {
            get
            {
                if (_detailImages == null)
                {
                    _detailImages = new List<ImageDTO>();
                }
                return _detailImages;
            }
            set
            {
                _detailImages = value;
            }
        }

        /// <summary>
        /// 产品销售渠道
        /// </summary>
        [NotMapped]
        public List<string> Business { get; set; }

        public DateTime? UpdateTime { get; set; }

        [Display(Name = "虚拟库存")]
        [NotMapped]
        /// <summary>
        /// 虚拟库存
        /// </summary>
        public int? VirtualStockQuantity { get; set; }

        [Display(Name = "有效期")]
        [NotMapped]
        /// <summary>
        /// 保质期
        /// </summary>
        public string Bestbefore { get; set; }
        [Display(Name = "同步HH库存")]
        public bool? IsHHPurchase { get; set; }
        public long? OdooProductID { get; set; }
        public long? OdooBrandID { get; set; }
        public long? OdooUserID { get; set; }
        [Display(Name = "产品分级")]
        public string OdooCategory { get; set; }

        [Display(Name = "HHSKU ")]
        public string HHSKU { get; set; }


        [Display(Name = "规格")]
        public string Specs { get; set; }
        [Display(Name = "供货商代码")]
        public string SupplierBarcode { get; set; }
        [Display(Name = "追踪属性")]
        public string TrackingType { get; set; }
        [Display(Name = "海关代码(HScode)")]
        public string HSCode { get; set; }
        [Display(Name = "原产国")]
        public string CountryName { get; set; }
        public int? CountryId { get; set; }
        [Display(Name = "生产商")]
        public string Producer { get; set; }
        [Display(Name = "箱规")]
        public int? BoxQty { get; set; }
        public long? OdooCategoryID { get; set; }
        [Display(Name = "保质期(天数)")]
        public int? LifeTime { get; set; }
        [Display(Name = "预警天数")]
        public int? AlertTime { get; set; }
        [Display(Name = "同步odoo")]
        public bool? SyncToOdoo { get; set; }
        public string PersonInCharge { get; set; }
        [Display(Name = "负责人")]
        public string PersonInChargeID { get; set; }
        [Display(Name = "服用天数")]
        public int? TakeTime { get; set; }
        [Display(Name = "品牌")]
        public string BrandName { get; set; }
        [Display(Name = "保税仓商品编码")]
        public string BondedWarehouseItemCode { get; set; }
        /// <summary>
        /// 千克,毫升,件,张,片
        /// </summary>
        [Display(Name = "海关备案单位ID")]
        public int? CustomsUnitID { get; set; }

        /// <summary>
        /// 备案法定数量 
        /// </summary>
        [Display(Name = "备案法定数量")]
        public Decimal? CustomsUnitAmount { get; set; }

        /// <summary>
        /// Ecinx商品编码 
        /// </summary>
        [Display(Name = "Ecinx商品编码")]
        public string ECinxSKU { get; set; }
        /// <summary>
        /// 同步Ecinx 
        /// </summary>
        [Display(Name = "同步Ecinx")]
        public bool? SyncToEcinx { get; set; }
     
    }

}
