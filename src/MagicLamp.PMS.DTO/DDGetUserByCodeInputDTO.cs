﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{

    public class DDGetUserByCodeInputDTO
    {

        public string Code { get; set; }

        public string State { get; set; }

        public string AppID { get; set; }
        public string AppSecret { get; set; }

    }
}
