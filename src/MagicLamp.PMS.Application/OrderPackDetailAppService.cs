﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Cmq_SDK;
using MagicLamp.PMS.Domain;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.DTOs.ThirdPart;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using TimeZoneConverter;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class OrderPackDetailAppService : PMSCrudAppServiceBase<OrderPackEntity, OrderPackDTO, OrderPackDTO, int>
    {
        private const string ClogTagType = "OrderPackDetail";
        private List<string> messageTags = new List<string> { "delivery" };

        private IRepository<OrderPackEntity, int> orderPackRepository;
        private IRepository<OrderPackDetailEntity, int> orderPackDetailRepository;
        private IRepository<OrderEntity, int> orderRepository;
        private IRepository<SeqEntity, int> seqRepository;
        private IRepository<UserEnitity, Guid> userRepository;
        private IRepository<SeqPackEntity, int> seqPackRepository;
        private IRepository<FluxDocOrderHeaderEntity, string> fluxDocOrderHeaderRepository;
        private IRepository<BusinessEntity, int> businessRepository;
        private IRepository<FreightsEntity, string> freightsRepository;
        private IRepository<OrderLogEntity, int> orderLogRepository;
        private IRepository<ProductEntity, string> productRepository;
        private IRepository<BarcodeSeqEntity, int> barcodeSeqRepository;
        private IRepository<ExportFlagEntity, int> exportFlagRepository;
        private IRepository<OrderPackReservedHHSKUEntity, long> orderPackReservedHHSKURepository;
        private IBackgroundJobManager backgroundJobManager;

        public OrderPackDetailAppService(IRepository<OrderPackEntity, int> _orderPackRepository,
            IRepository<OrderPackDetailEntity, int> _orderPackDetailRepository,
            IRepository<SeqEntity, int> _seqRepository,
            IRepository<OrderEntity, int> _orderRepository,
            IRepository<UserEnitity, Guid> _userRepository,
            IRepository<SeqPackEntity, int> _seqPackRepository,
            IRepository<FluxDocOrderHeaderEntity, string> _fluxDocOrderHeaderRepository,
            IRepository<BusinessEntity, int> _businessRepository,
            IRepository<FreightsEntity, string> _freightsRepository,
            IRepository<OrderLogEntity, int> _orderLogRepository,
            IRepository<ProductEntity, string> _productRepository,
            IRepository<BarcodeSeqEntity, int> _barcodeSeqRepository,
            IRepository<ExportFlagEntity, int> _exportFlagRepository,
            IRepository<OrderPackReservedHHSKUEntity, long> _orderPackReservedHHSKURepository,
            IBackgroundJobManager _backgroundJobManager) : base(_orderPackRepository)
        {
            orderPackRepository = _orderPackRepository;
            orderPackDetailRepository = _orderPackDetailRepository;
            orderRepository = _orderRepository;
            seqRepository = _seqRepository;
            userRepository = _userRepository;
            seqPackRepository = _seqPackRepository;
            fluxDocOrderHeaderRepository = _fluxDocOrderHeaderRepository;
            businessRepository = _businessRepository;
            freightsRepository = _freightsRepository;
            orderLogRepository = _orderLogRepository;
            productRepository = _productRepository;
            barcodeSeqRepository = _barcodeSeqRepository;
            exportFlagRepository = _exportFlagRepository;
            orderPackReservedHHSKURepository = _orderPackReservedHHSKURepository;
            backgroundJobManager = _backgroundJobManager;
        }

        private string GetOrderPackFreezeFlagName(OrderPackDTO dto)
        {
            switch (dto.freezeFlag)
            {
                case 0:
                    return "未冻结";
                case 1:
                    return "作废";
                case 2:
                    return "整单退款";
                case 3:
                    return "缺货退款";
                case 4:
                    return "换货退款";
                case 5:
                    return "换货补款";
                case 6:
                    return "无差价换货";
                case 7:
                    return "信息待确认";
                case 8:
                    return "改标志";
                case 9:
                    return "其他";
                default:
                    return "未知状态";
            }
        }

        private string GetOrderPackExportFlagName(OrderPackDTO dto)
        {
            var exportFlags = exportFlagRepository.GetAllList();
            var entity = exportFlags.FirstOrDefault(x => x.Flag == dto.exportFlag);
            if (entity == null)
            {
                return string.Empty;
            }
            return entity.Name;
        }

        private string GetFreightsName(OrderPackDTO dto)
        {
            var freights = freightsRepository.GetAllList();
            var entity = freights.FirstOrDefault(x => x.id == dto.freightId);
            if (entity == null)
            {
                return string.Empty;
            }
            return entity.name;
        }

        private void RecalculateSkuLockQtyForOrderPackDetail(List<string> listSKU, string orderPackNo, string orderID)
        {
            RefreshPorductStockInputDTO refreshPorductStockInputDTO = new RefreshPorductStockInputDTO
            {
                Skus = listSKU
            };

            //Recalculate sku lock
            Dictionary<string, string> logRecalculateSKULock = new Dictionary<string, string>
            {
                ["sku"] = string.Join(";", listSKU),
                ["type"] = ClogTagType,
                ["method"] = "RecalculateSkuLockQtyForOrderPackDetail",
                ["orderpackno"] = orderPackNo,
                ["orderid"] = orderID
            };

            CLSLogger.Info("RecalculateSkuLockQty.Request", refreshPorductStockInputDTO.ToJsonString(), logRecalculateSKULock);

            ProductDomainService productDomainService = new ProductDomainService();
            List<ProductStockVO> stockVOs = productDomainService.RecalculateSkuLockQty(refreshPorductStockInputDTO.Skus);

            if (!stockVOs.IsNullOrEmpty())
            {
                RefreshPorductStockOutDTO outputDTO = new RefreshPorductStockOutDTO();
                outputDTO.ProductStocks = stockVOs.Select(x => new ProductStockDTO
                {
                    Sku = x.Sku,
                    Qty_available = x.Qty_available,
                    ExpiredDate = x.ExpiredDate,
                    OriginalBestBefore = x.OriginalBestBefore,
                    OriginalHHBestBefore = x.OriginalHHBestBefore,
                    OriginalStock = x.Qty_available,
                    OriginalHHStock = x.OriginalHHStock,
                    LockQty = x.LockQty
                }).ToList();

                //notify 188, motan
                backgroundJobManager.Enqueue<SyncAladdinStockJob, List<string>>(refreshPorductStockInputDTO.Skus, BackgroundJobPriority.High);

                CLSLogger.Info("RecalculateSkuLockQty.Response", outputDTO.ToJsonString(), logRecalculateSKULock);
            }
        }

        /// <summary>
        /// send message to message queue
        /// </summary>
        /// <param name="orderPackInfo"></param>
        /// <param name="seqInfo"></param>
        /// <param name="tags"></param>
        private void NotifyOrderPackStatus(OrderPackEntity orderPackInfo, SeqEntity seqInfo, Dictionary<string, string> tags)
        {
            // 1：188 yishan
            // 888：魔法海淘/洋小范 motan
            if (orderPackInfo.freezeFlag == 0 && (seqInfo.bizID == PMSConsts.BizID188 || seqInfo.bizID == PMSConsts.BizIDYangXiaoFan))
            {
                OrderPackStatusMqDto orderPackStatus = new OrderPackStatusMqDto
                {
                    OrderID = orderPackInfo.orderID,
                    OrderPackNo = orderPackInfo.orderPackNo,
                    ParcelTrackingNo = orderPackInfo.carrierId,
                    ParcelPickDate = orderPackInfo.pick_time,
                    ParcelPackDate = orderPackInfo.pack_time,
                    CourierCode = orderPackInfo.freightId,
                    ParcelStatus = orderPackInfo.status.ToString(),
                    bizId = seqInfo.bizID
                };

                var name = (BusinessEnum)orderPackStatus.bizId;
                messageTags.Add(name.ToString());

                TencentCMQProxy.PublishTopicMessage(TCConstants.CMQTopics.TOPIC_ORDERNOTIFICATION, orderPackStatus, messageTags, tags);
            }
        }


        /// <summary>
        /// 地址信息
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public OrderPackDTO GetOrderPackInfo(OrderPackDTO dto)
        {
            if (dto.orderPackNo == null)
            {
                throw new RequestValidationException("input cannot be null");
            }

            OrderPackDTO result = new OrderPackDTO();
            var orderPackInfo = orderPackRepository.FirstOrDefault(x => x.orderPackNo == dto.orderPackNo);
            var orderInfo = orderRepository.FirstOrDefault(x => x.orderid == orderPackInfo.orderID);

            result = orderPackInfo.MapTo<OrderPackDTO>();
            result.shippingMethod = orderInfo.shipment;
            result.store = orderInfo.store;
            result.freightName = GetFreightsName(result);
            result.exportFlagName = GetOrderPackExportFlagName(result);
            return result;
        }

        [HttpPut]
        public void UpdateOrderPackInfo(OrderPackDTO dto)
        {
            try
            {
                var orderPackInfo = orderPackRepository.FirstOrDefault(x => x.orderPackNo == dto.orderPackNo);

                if (orderPackInfo == null)
                {
                    throw new RequestValidationException($"失败。失败原因：找不到包裹号。orderPackNo：{dto.orderPackNo}");
                }

                var orderInfo = orderRepository.FirstOrDefault(x => x.orderid == orderPackInfo.orderID);
                if (orderInfo == null)
                {
                    throw new RequestValidationException($"失败。失败原因：找不到订单。orderId：{orderPackInfo.orderID}");
                }

                //检查地址是否是4段式
                var addressInfo = dto.rec_addr.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                if (addressInfo.Length != 4)
                {
                    throw new RequestValidationException($"失败。失败原因：收件地址请按照 省 市 区 详细地址 ，四段式格式修改，以空格隔开，直辖市省和市保持一致。");
                }

                //未配才能冻结
                if (orderPackInfo.status == (int)OrderPackStatusEnum.Unassigned)
                {
                    dto.freezeFlag = (int)OrderPackFreezeFlagEnum.信息待确认;
                    dto.freezeComment = "修改地址信息";
                    //未冻结不能修改
                    //优化查富勒 波次，波次有没有打印
                    if (orderPackInfo.freezeFlag != 0)
                    {
                        FreezeOrder(dto);
                    }

                    string logComment = String.Empty;

                    string oldCarrierId = orderPackInfo.carrierId == null ? "" : orderPackInfo.carrierId;
                    string oldRecName = orderPackInfo.rec_name == null ? "" : orderPackInfo.rec_name;
                    string oldRecAddr = orderPackInfo.rec_addr == null ? "" : orderPackInfo.rec_addr;
                    string oldRecPhone = orderPackInfo.rec_phone == null ? "" : orderPackInfo.rec_phone;
                    string oldIdNo = orderPackInfo.idNo == null ? "" : orderPackInfo.idNo;

                    int oldExportFlag = orderPackInfo.exportFlag.Value;
                    string oldFreightId = orderPackInfo.freightId == null ? "" : orderPackInfo.freightId;

                    orderPackInfo.carrierId = dto.carrierId;
                    orderPackInfo.rec_name = dto.rec_name;
                    orderPackInfo.rec_addr = dto.rec_addr;
                    orderPackInfo.rec_phone = dto.rec_phone;
                    orderPackInfo.idNo = dto.idNo;

                    orderPackInfo.exportFlag = dto.exportFlag;
                    orderPackInfo.freightId = dto.freightId;
                    CurrentUnitOfWork.SaveChanges();

                    if (!oldCarrierId.Equals(orderPackInfo.carrierId))
                    {
                        logComment += $"修改运单号{oldCarrierId}为{orderPackInfo.carrierId}。";
                    }
                    if (!oldRecName.Equals(orderPackInfo.rec_name))
                    {
                        logComment += $"修改收件人{oldRecName}为{orderPackInfo.rec_name}。";
                    }
                    if (!oldRecAddr.Equals(orderPackInfo.rec_addr))
                    {
                        logComment += $"修改收件地址{oldRecAddr}为{orderPackInfo.rec_addr}。";
                    }
                    if (!oldRecPhone.Equals(orderPackInfo.rec_phone))
                    {
                        logComment += $"修改收件人电话{oldRecPhone}为{orderPackInfo.rec_phone}。";
                    }
                    if (!oldIdNo.Equals(orderPackInfo.idNo))
                    {
                        logComment += $"修改身份证{oldIdNo}为{orderPackInfo.idNo}。";
                    }
                    if (oldExportFlag != orderPackInfo.exportFlag)
                    {
                        logComment += $"修改标志{oldExportFlag}为{orderPackInfo.exportFlag}。";
                    }
                    if (!oldFreightId.Equals(orderPackInfo.freightId))
                    {
                        logComment += $"修改物流{oldExportFlag}为{orderPackInfo.exportFlag}。";
                    }

                    //解冻订单
                    UnfreezeOrder(dto);

                    var orderLogInfo = new OrderLogEntity
                    {
                        OID = orderInfo.oid,
                        OPID = orderPackInfo.opID,
                        OptDate = DateTime.UtcNow,
                        Comments = $"修改地址信息：{orderPackInfo.orderPackNo}。修改备注：{logComment}",
                        flag = (int)OrderLogFlagEnum.客服跟踪环节
                    };
                    orderLogRepository.InsertAsync(orderLogInfo);
                }
                else
                {
                    throw new RequestValidationException($"更新包裹信息失败，失败原因：包裹状态不为未配，无法修改。");
                }
            }
            catch (RequestValidationException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = ClogTagType;
                tags["method"] = "UpdateOrderPackInfo";
                tags["orderpackno"] = dto.orderPackNo;
                CLSLogger.Error("更新包裹信息发生异常", ex.Message, tags);
                throw new RequestValidationException($"更新包裹信息发生异常，失败原因：{ex.Message}");
            }
        }

        public OrderPackProgressDTO GetOrderPackProgress(OrderPackDTO dto)
        {
            try
            {
                var timeZoneNZ = TZConvert.GetTimeZoneInfo("Pacific/Auckland");

                OrderPackProgressDTO result = new OrderPackProgressDTO();
                if (dto.orderPackNo == null)
                {
                    throw new RequestValidationException("input cannot be null");
                }

                var orderPackInfo = orderPackRepository.FirstOrDefault(x => x.orderPackNo == dto.orderPackNo);
                if (orderPackInfo == null)
                {
                    throw new RequestValidationException($"失败。失败原因：找不到包裹号。orderPackNo：{dto.orderPackNo}");
                }
                else
                {
                    result.opId = orderPackInfo.opID;
                    result.Id = result.opId;
                    result.orderPackNo = orderPackInfo.orderPackNo;
                }
                var orderInfo = orderRepository.FirstOrDefault(x => x.orderid == orderPackInfo.orderID);
                if (orderInfo == null)
                {
                    throw new RequestValidationException($"失败。失败原因：找不到订单。orderId：{orderPackInfo.orderID}");
                }

                //取订单上传时间
                var seqInfo = seqRepository.FirstOrDefault(x => x.seq == orderInfo.seq);
                if (seqInfo == null)
                {
                    result.progress.Add(new OrderPackProgressActionDTO
                    {
                        action = OrderPackProgressActionValueDTO.Uploaded.Value,
                        time = null,
                        operatorName = null
                    });
                }
                else
                {
                    var userId = seqInfo.userid;
                    var userInfo = userRepository.FirstOrDefault(x => x.UserId == userId);
                    if (userInfo == null)
                    {
                        result.progress.Add(new OrderPackProgressActionDTO
                        {
                            action = OrderPackProgressActionValueDTO.Uploaded.Value,
                            time = seqInfo.up_time.HasValue ? TimeZoneInfo.ConvertTimeToUtc(seqInfo.up_time.Value, timeZoneNZ) : seqInfo.up_time,
                            operatorName = null
                        });
                    }
                    else
                    {
                        result.progress.Add(new OrderPackProgressActionDTO
                        {
                            action = OrderPackProgressActionValueDTO.Uploaded.Value,
                            time = seqInfo.up_time.HasValue ? TimeZoneInfo.ConvertTimeToUtc(seqInfo.up_time.Value, timeZoneNZ) : seqInfo.up_time,
                            operatorName = userInfo.UserName
                        });
                    }
                }

                //取生成配货时间
                var seqPackInfo = seqPackRepository.FirstOrDefault(x => x.pseq == orderPackInfo.pseq);
                if (seqPackInfo == null)
                {
                    //无生成配货时间
                    result.progress.Add(new OrderPackProgressActionDTO
                    {
                        action = OrderPackProgressActionValueDTO.Generated.Value,
                        time = null,
                        operatorName = null
                    });
                    //无快递打单时间
                    result.progress.Add(new OrderPackProgressActionDTO
                    {
                        action = OrderPackProgressActionValueDTO.CarrierCreated.Value,
                        time = null,
                        operatorName = null
                    });
                }
                else
                {
                    var genTime = seqPackInfo.gen_time;
                    var genUserId = seqPackInfo.gen_opt_id;
                    var genUserInfo = userRepository.FirstOrDefault(x => x.UserId == genUserId);
                    if (genUserInfo == null)
                    {
                        result.progress.Add(new OrderPackProgressActionDTO
                        {
                            action = OrderPackProgressActionValueDTO.Generated.Value,
                            time = genTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(genTime.Value, timeZoneNZ) : genTime,
                            operatorName = null
                        });
                    }
                    else
                    {
                        result.progress.Add(new OrderPackProgressActionDTO
                        {
                            action = OrderPackProgressActionValueDTO.Generated.Value,
                            time = genTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(genTime.Value, timeZoneNZ) : genTime,
                            operatorName = genUserInfo.UserName
                        });
                    }

                    //取快递打单时间
                    var printTime = seqPackInfo.print_time;
                    var printUserId = seqPackInfo.print_opt_id;
                    var printUserInfo = userRepository.FirstOrDefault(x => x.UserId == printUserId);
                    if (printUserInfo == null)
                    {
                        result.progress.Add(new OrderPackProgressActionDTO
                        {
                            action = OrderPackProgressActionValueDTO.CarrierCreated.Value,
                            time = printTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(printTime.Value, timeZoneNZ) : printTime,
                            operatorName = null
                        });
                    }
                    else
                    {
                        result.progress.Add(new OrderPackProgressActionDTO
                        {
                            action = OrderPackProgressActionValueDTO.CarrierCreated.Value,
                            time = printTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(printTime.Value, timeZoneNZ) : printTime,
                            operatorName = printUserInfo.UserName
                        });
                    }
                }

                //取完全分配时间
                var assignTime = orderPackInfo.assignTime;
                if (assignTime == null)
                {
                    result.progress.Add(new OrderPackProgressActionDTO
                    {
                        action = OrderPackProgressActionValueDTO.Assigned.Value,
                        time = null,
                        operatorName = null
                    });
                }
                else
                {
                    result.progress.Add(new OrderPackProgressActionDTO
                    {
                        action = OrderPackProgressActionValueDTO.Assigned.Value,
                        time = assignTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(assignTime.Value, timeZoneNZ) : assignTime,
                        operatorName = null
                    });
                }

                //取配货时间
                var pickTime = orderPackInfo.pick_time;
                if (pickTime == null)
                {
                    result.progress.Add(new OrderPackProgressActionDTO
                    {
                        action = OrderPackProgressActionValueDTO.Picked.Value,
                        time = null,
                        operatorName = null
                    });
                }
                else
                {
                    var pickUserName = orderPackInfo.pickIdReal;
                    result.progress.Add(new OrderPackProgressActionDTO
                    {
                        action = OrderPackProgressActionValueDTO.Picked.Value,
                        time = pickTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(pickTime.Value, timeZoneNZ) : pickTime,
                        operatorName = pickUserName
                    });
                }

                //取包装时间
                var packTime = orderPackInfo.pack_time;
                if (packTime == null)
                {
                    result.progress.Add(new OrderPackProgressActionDTO
                    {
                        action = OrderPackProgressActionValueDTO.Packed.Value,
                        time = null,
                        operatorName = null
                    });
                }
                else
                {
                    var packUserName = orderPackInfo.packIdReal;

                    result.progress.Add(new OrderPackProgressActionDTO
                    {
                        action = OrderPackProgressActionValueDTO.Packed.Value,
                        time = packTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(packTime.Value, timeZoneNZ) : packTime,
                        operatorName = packUserName
                    });
                }

                //取出库时间
                var outboundTime = orderPackInfo.out_time;
                if (outboundTime == null)
                {
                    result.progress.Add(new OrderPackProgressActionDTO
                    {
                        action = OrderPackProgressActionValueDTO.Outbounded.Value,
                        time = null,
                        operatorName = null
                    });
                }
                else
                {
                    var outboundUserId = orderPackInfo.outId;
                    if (outboundUserId == null)
                    {
                        result.progress.Add(new OrderPackProgressActionDTO
                        {
                            action = OrderPackProgressActionValueDTO.Outbounded.Value,
                            time = outboundTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(outboundTime.Value, timeZoneNZ) : outboundTime,
                            operatorName = null
                        });
                    }
                    else
                    {
                        var outboundUser = userRepository.FirstOrDefault(x => x.UserId == outboundUserId);
                        result.progress.Add(new OrderPackProgressActionDTO
                        {
                            action = OrderPackProgressActionValueDTO.Outbounded.Value,
                            time = outboundTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(outboundTime.Value, timeZoneNZ) : outboundTime,
                            operatorName = outboundUser.UserName
                        });
                    }
                }

                //取扫描上传时间
                var scanTime = orderPackInfo.scanTime;
                if (scanTime == null)
                {
                    result.progress.Add(new OrderPackProgressActionDTO
                    {
                        action = OrderPackProgressActionValueDTO.Scanned.Value,
                        time = null,
                        operatorName = null
                    });
                }
                else
                {
                    var scanUserId = orderPackInfo.scanId;
                    if (scanUserId == null)
                    {
                        result.progress.Add(new OrderPackProgressActionDTO
                        {
                            action = OrderPackProgressActionValueDTO.Scanned.Value,
                            time = scanTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(scanTime.Value, timeZoneNZ) : scanTime,
                            operatorName = null
                        });
                    }
                    else
                    {
                        var scanUser = userRepository.FirstOrDefault(x => x.UserId == scanUserId);
                        result.progress.Add(new OrderPackProgressActionDTO
                        {
                            action = OrderPackProgressActionValueDTO.Scanned.Value,
                            time = scanTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(scanTime.Value, timeZoneNZ) : scanTime,
                            operatorName = scanUser.UserName
                        });
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = ClogTagType;
                tags["method"] = "GetOrderPackProgress";
                tags["orderpackno"] = dto.orderPackNo;
                CLSLogger.Error("查询包裹进度发生异常", ex, tags);
                throw new RequestValidationException($"查询包裹进度发生异常，失败原因：{ex.Message}");
            }
        }

        [HttpPut]
        public void FreezeOrderWithoutCheckingBinded(OrderPackDTO dto)
        {
            dto.comment += "-强制冻结";
            FreezeOrder(dto, "true");
        }

        /// <summary>
        /// freeze order pack
        /// </summary>
        /// <param name="dto"></param>
        [HttpPut]
        public void FreezeOrder(OrderPackDTO dto, string compulsory = null)
        {
            Dictionary<string, string> tags = new Dictionary<string, string>
            {
                ["type"] = ClogTagType,
                ["method"] = "FreezeOrder",
                ["orderpackno"] = dto.orderPackNo
            };

            if (dto.orderPackNo == null)
            {
                throw new RequestValidationException("input cannot be null");
            }

            var orderPackInfo = orderPackRepository.FirstOrDefault(x => x.orderPackNo == dto.orderPackNo);
            if (orderPackInfo == null)
            {
                throw new RequestValidationException($"失败。失败原因：找不到包裹号。orderPackNo：{dto.orderPackNo}");
            }

            var orderInfo = orderRepository.FirstOrDefault(x => x.orderid == orderPackInfo.orderID);
            if (orderInfo == null)
            {
                throw new RequestValidationException($"失败。失败原因：找不到订单。orderId：{orderPackInfo.orderID}");
            }

            //if orderpackno is already binded to waveno in HH
            if (compulsory != "true")
            {
                var bindedHHOrderPack = orderPackReservedHHSKURepository.FirstOrDefault(x => x.OrderPackNo == dto.orderPackNo && x.Status == OrderPackReservedStatusEnum.Binded.ToString());
                if (bindedHHOrderPack != null)
                {
                    throw new RequestValidationException($"订单已进入仓库配货流程不允许取消！特殊情况请联系仓库（订单已在HH绑定过波次）！");
                }
            }

            var listOrderPackDetail = orderPackDetailRepository.GetAll().Where(x => x.opid == orderPackInfo.opID).ToList();

            var seqInfo = seqRepository.FirstOrDefault(x => x.seq == orderInfo.seq);

            string promptMessage = string.Empty;

            //推送过富勒的订单需先取消富勒订单，否则会出现ERP冻结成功但富勒实际未取消，最终还是发货的问题
            if (orderPackInfo.pushOrderTime.HasValue || orderPackInfo.pack_time.HasValue || orderPackInfo.out_time.HasValue)
            {
                FluxDomainService fluxDomainService = new FluxDomainService();

                var result = fluxDomainService.CancelOrderInFlux(orderPackInfo, dto.comment, ref promptMessage);

                //取消富勒失败
                if (result == false)
                {
                    throw new RequestValidationException(promptMessage);
                }
            }

            try
            {
                orderPackInfo.freezeFlag = dto.freezeFlag.Value;
                orderPackInfo.freezeComment = dto.comment;
                orderPackInfo.freezeTime = DateTime.UtcNow;

                var orderLogInfo = new OrderLogEntity
                {
                    OID = orderInfo.oid,
                    OPID = orderPackInfo.opID,
                    OptDate = DateTime.UtcNow,
                    Comments = $"冻结订单：{orderPackInfo.orderPackNo}。状态为：{GetOrderPackFreezeFlagName(dto)}，备注：{dto.comment}",
                    flag = (int)OrderLogFlagEnum.客服跟踪环节
                };
                orderLogRepository.InsertAsync(orderLogInfo);

                //try canceling order in HH
                HHPushOrderInputDTO hhInput = new HHPushOrderInputDTO
                {
                    order_no = orderPackInfo.orderPackNo
                };
                backgroundJobManager.Enqueue<CancelHHOrderJob, HHPushOrderInputDTO>(hhInput);

                //update sku in orderPackDetail
                if (!listOrderPackDetail.IsNullOrEmpty())
                {
                    var listSKU = listOrderPackDetail.Select(x => x.sku.Trim()).Distinct().ToList();
                    RecalculateSkuLockQtyForOrderPackDetail(listSKU, orderPackInfo.orderPackNo, orderPackInfo.orderID);
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("取消富勒订单发生异常", ex.Message, tags);
                throw new RequestValidationException($"取消富勒订单失败，失败原因：{ex.Message}");
            }


            //通知状态变化给188, 洋小范
            NotifyOrderPackStatus(orderPackInfo, seqInfo, tags);
        }

        /// <summary>
        /// unfreeze order
        /// </summary>
        /// <param name="dto"></param>
        [HttpPut]
        public void UnfreezeOrder(OrderPackDTO dto)
        {
            Dictionary<string, string> tags = new Dictionary<string, string>
            {
                ["type"] = ClogTagType,
                ["method"] = "UnfreezeOrder",
                ["orderpackno"] = dto.orderPackNo
            };

            List<FluxPutOrderDTO> listOrder = new List<FluxPutOrderDTO>();

            if (dto.orderPackNo == null)
            {
                throw new RequestValidationException("input cannot be null");
            }

            var orderPackInfo = orderPackRepository.FirstOrDefault(x => x.orderPackNo == dto.orderPackNo);
            if (orderPackInfo == null)
            {
                throw new RequestValidationException($"失败。失败原因：找不到包裹号。orderPackNo：{dto.orderPackNo}");
            }

            var orderPackDetails = orderPackDetailRepository.GetAll().Where(x => x.opid == orderPackInfo.opID).ToList();

            var orderInfo = orderRepository.FirstOrDefault(x => x.orderid == orderPackInfo.orderID);
            if (orderInfo == null)
            {
                throw new RequestValidationException($"失败。失败原因：找不到订单。orderId：{orderPackInfo.orderID}");
            }

            var seqInfo = seqRepository.FirstOrDefault(x => x.seq == orderInfo.seq);

            var businessInfo = businessRepository.FirstOrDefault(x => x.bizID == seqInfo.bizID);

            var freightsInfo = freightsRepository.FirstOrDefault(x => x.id.Equals(orderPackInfo.freightId));

            //判断是否是仓库可操作的业务
            //TODO: 不是so_sts 90的才可以取消，如果是99，则钉钉通报异常？不允许？
            if (!FluxWMSProxy.IsFlux(orderPackInfo.exportFlag.Value))
            {
                orderPackInfo.freezeFlag = 0;
                orderPackInfo.freezeComment = "解冻";
                orderPackInfo.freezeTime = DateTime.UtcNow;

                var orderLogInfo = new OrderLogEntity
                {
                    OID = orderInfo.oid,
                    OPID = orderPackInfo.opID,
                    OptDate = DateTime.UtcNow,
                    Comments = $"解冻订单：{orderPackInfo.orderPackNo}。",
                    flag = (int)OrderLogFlagEnum.客服跟踪环节
                };
                orderLogRepository.InsertAsync(orderLogInfo);

                //通知状态变化给188, 洋小范
                NotifyOrderPackStatus(orderPackInfo, seqInfo, tags);

                return;
            }

            //上传出库单到flux
            try
            {
                string promptMessage = string.Empty;

                //temporarily unfreeze orderpack
                var previousFreezeFlag = orderPackInfo.freezeFlag;
                orderPackInfo.freezeFlag = 0;

                FluxDomainService fluxDomainService = new FluxDomainService();
                var isSuccessful = fluxDomainService.PlaceOrderInFlux(orderPackInfo, orderInfo, orderPackDetails, seqInfo, businessInfo, freightsInfo, null, ref promptMessage);

                //失败
                if (!isSuccessful)
                {
                    orderPackInfo.freezeFlag = previousFreezeFlag;
                    CLSLogger.Error("订单重推富勒发生异常", promptMessage, tags);
                    throw new RequestValidationException($"订单重推富勒失败，原因：{promptMessage}");
                }
                //成功
                else
                {
                    orderPackInfo.freezeFlag = 0;
                    orderPackInfo.freezeComment = "解冻";
                    orderPackInfo.freezeTime = DateTime.UtcNow;

                    var orderLogInfo = new OrderLogEntity
                    {
                        OID = orderInfo.oid,
                        OPID = orderPackInfo.opID,
                        OptDate = DateTime.UtcNow,
                        Comments = $"解冻订单：{orderPackInfo.orderPackNo}。",
                        flag = (int)OrderLogFlagEnum.客服跟踪环节
                    };
                    orderLogRepository.InsertAsync(orderLogInfo);

                    //通知状态变化给188, 洋小范
                    NotifyOrderPackStatus(orderPackInfo, seqInfo, tags);

                    //update product stock
                    var listSKU = orderPackDetailRepository.GetAll().Where(x => x.opid == orderPackInfo.opID).Select(x => x.sku.Trim()).Distinct().ToList();
                    RecalculateSkuLockQtyForOrderPackDetail(listSKU, orderPackInfo.orderPackNo, orderPackInfo.orderID);
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("订单重推富勒发生异常", ex, tags);
                throw new RequestValidationException($"订单重推富勒失败，失败原因：{ex}");
            }
        }

        [HttpPost]
        public void AddOrderPackDetail(OrderPackDetailDTO dto)
        {
            try
            {
                var orderPackInfo = orderPackRepository.FirstOrDefault(x => x.orderPackNo == dto.orderPackNo);
                if (orderPackInfo == null)
                {
                    throw new RequestValidationException($"失败。失败原因：找不到包裹号。orderPackNo：{dto.orderPackNo}");
                }

                var productInfo = productRepository.FirstOrDefault(x => x.SKU == dto.sku);
                if (productInfo == null)
                {
                    throw new RequestValidationException($"失败。失败原因：找不到SKU信息。SKU = {dto.sku}");
                }

                if (orderPackInfo.status == (int)OrderPackStatusEnum.Outbound)
                {
                    throw new RequestValidationException($"失败。失败原因：包裹状态为：已出库。无法修改。orderPackNo = {dto.orderPackNo}");
                }

                var orderInfo = orderRepository.FirstOrDefault(x => x.orderid == orderPackInfo.orderID);
                if (orderInfo == null)
                {
                    throw new RequestValidationException($"失败。失败原因：找不到订单。orderId：{orderPackInfo.orderID}");
                }

                //var orderPackDeatilInfo = orderPackDetailRepository.FirstOrDefault(x => x.opid == orderPackInfo.opID);
                var listOrderPackDetail = orderPackDetailRepository.GetAll().Where(x => x.opid == orderPackInfo.opID).ToList();
                var orderPackDetailToBeAdded = dto.MapTo<OrderPackDetailEntity>();

                orderPackDetailToBeAdded.opid = orderPackInfo.opID;
                orderPackDetailToBeAdded.oid = orderPackInfo.oID;
                orderPackDetailToBeAdded.status = 0;
                orderPackDetailToBeAdded.flag = 1;
                orderPackDetailToBeAdded.pda = 0;
                orderPackDetailToBeAdded.total_w = orderPackDetailToBeAdded.qty * orderPackDetailToBeAdded.weight;
                orderPackDetailToBeAdded.taxrate = 0;
                orderPackDetailToBeAdded.total_t = 0;
                orderPackDetailToBeAdded.inputqty = 0;
                orderPackDetailToBeAdded.csflag = 0;
                orderPackDetailToBeAdded.product_name = productInfo.ChineseName;
                orderPackDetailToBeAdded.barcode = productInfo.Barcode1;
                orderPackDetailToBeAdded.weight = productInfo.GrossWeight;

                orderPackDetailRepository.InsertAsync(orderPackDetailToBeAdded);

                var totalTax = listOrderPackDetail.Where(x => x.flag == 1).Sum(x => x.total_t);
                var totalWeight = listOrderPackDetail.Where(x => x.flag == 1).Sum(x => x.total_w);

                totalTax += orderPackDetailToBeAdded.total_t;
                totalWeight += orderPackDetailToBeAdded.total_w;

                orderPackInfo.taxVal = totalTax;
                orderPackInfo.weightVal = totalWeight;
                orderPackInfo.updateFlag = 1;
                orderPackInfo.updateTime = DateTime.UtcNow;

                var orderLogInfo = new OrderLogEntity
                {
                    OID = orderInfo.oid,
                    OPID = orderPackInfo.opID,
                    OptDate = DateTime.UtcNow,
                    picpath = $"img/products/{orderPackDetailToBeAdded.sku}.jpg",
                    Comments = $"增加一个明细，SKU：{orderPackDetailToBeAdded.sku}，名称：{orderPackDetailToBeAdded.product_name}",
                    flag = (int)OrderLogFlagEnum.订单处理环节
                };
                orderLogRepository.InsertAsync(orderLogInfo);
            }
            catch (Exception ex)
            {
                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = ClogTagType;
                tags["method"] = "AddOrderPackDetail";
                tags["orderpackno"] = dto.orderPackNo;
                CLSLogger.Error("新增包裹详情发生异常", ex.Message, tags);
                throw new RequestValidationException($"更新包裹信息发生异常，失败原因：{ex.Message}");
            }
        }

        [HttpPut]
        public void UpdateOrderPackDetail(OrderPackDetailDTO dto)
        {
            try
            {
                var orderPackInfo = orderPackRepository.FirstOrDefault(x => x.orderPackNo == dto.orderPackNo);

                if (orderPackInfo == null)
                {
                    throw new RequestValidationException($"失败。失败原因：找不到包裹号。orderPackNo = {dto.orderPackNo}");
                }

                var orderInfo = orderRepository.FirstOrDefault(x => x.orderid == orderPackInfo.orderID);
                if (orderInfo == null)
                {
                    throw new RequestValidationException($"失败。失败原因：找不到订单。orderId：{orderPackInfo.orderID}");
                }


                //
                var listOrderPackDetail = orderPackDetailRepository.GetAll().Where(x => x.opid == orderPackInfo.opID).ToList();
                var orderPackDetailInfo = listOrderPackDetail.FirstOrDefault(x => x.opdid == dto.opdid);

                var oldQty = orderPackDetailInfo.qty;
                orderPackDetailInfo.qty = dto.qty;
                orderPackDetailInfo.total_w = orderPackDetailInfo.weight * orderPackDetailInfo.qty;
                orderPackDetailInfo.total_t = orderPackDetailInfo.taxrate * orderPackDetailInfo.qty;

                var totalTax = listOrderPackDetail.Where(x => x.flag == 1).Sum(x => x.total_t);
                var totalWeight = listOrderPackDetail.Where(x => x.flag == 1).Sum(x => x.total_w);

                orderPackInfo.taxVal = totalTax;
                orderPackInfo.weightVal = totalWeight;
                orderPackInfo.updateFlag = 1;
                orderPackInfo.updateTime = DateTime.UtcNow;

                var orderLogInfo = new OrderLogEntity
                {
                    OID = orderInfo.oid,
                    OPID = orderPackInfo.opID,
                    OptDate = DateTime.UtcNow,
                    picpath = $"img/products/{orderPackDetailInfo.sku}.jpg",
                    Comments = $"更新一个明细，SKU：{orderPackDetailInfo.sku}，名称：{orderPackDetailInfo.product_name}，数量更新为{orderPackDetailInfo.qty}，原数量为{oldQty}",
                    flag = (int)OrderLogFlagEnum.订单处理环节
                };
                orderLogRepository.InsertAsync(orderLogInfo);
            }
            catch (Exception ex)
            {
                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = ClogTagType;
                tags["method"] = "UpdateOrderPackDetail";
                tags["orderpackno"] = dto.orderPackNo;
                CLSLogger.Error("更新包裹详情发生异常", ex.Message, tags);
                throw new RequestValidationException($"更新包裹详情发生异常，失败原因：{ex.Message}");
            }
        }

        [HttpPut]
        public void CancelOrderPackDetail(OrderPackDetailDTO dto)
        {
            try
            {
                var orderPackInfo = orderPackRepository.FirstOrDefault(x => x.orderPackNo == dto.orderPackNo);

                if (orderPackInfo == null)
                {
                    throw new RequestValidationException($"失败。失败原因：找不到包裹号。orderPackNo = {dto.orderPackNo}");
                }

                if (orderPackInfo.status == null && orderPackInfo.status == (int)OrderPackStatusEnum.Outbound)
                {
                    throw new RequestValidationException($"失败。失败原因：包裹状态为：已出库。无法修改。orderPackNo = {dto.orderPackNo}");
                }

                var orderInfo = orderRepository.FirstOrDefault(x => x.orderid == orderPackInfo.orderID);
                if (orderInfo == null)
                {
                    throw new RequestValidationException($"失败。失败原因：找不到订单。orderId：{orderPackInfo.orderID}");
                }

                //var orderPackDetailInfo = orderPackDetailRepository.FirstOrDefault(x => x.opid == orderPackInfo.opID && x.sku == dto.sku);
                var listOrderPackDetail = orderPackDetailRepository.GetAll().Where(x => x.opid == orderPackInfo.opID).ToList();
                var orderPackDetailInfo = listOrderPackDetail.FirstOrDefault(x => x.opdid == dto.opdid);

                orderPackDetailInfo.flag = 0;

                var totalTax = listOrderPackDetail.Where(x => x.flag == 1).Sum(x => x.total_t);
                var totalWeight = listOrderPackDetail.Where(x => x.flag == 1).Sum(x => x.total_w);

                orderPackInfo.updateFlag = 1;
                orderPackInfo.updateTime = DateTime.UtcNow;
                orderPackInfo.taxVal = totalTax;
                orderPackInfo.weightVal = totalWeight;

                var orderLogInfo = new OrderLogEntity
                {
                    OID = orderInfo.oid,
                    OPID = orderPackInfo.opID,
                    OptDate = DateTime.UtcNow,
                    picpath = $"img/products/{orderPackDetailInfo.sku}.jpg",
                    Comments = $"取消一个明细，SKU：{orderPackDetailInfo.sku}，名称：{orderPackDetailInfo.product_name}",
                    flag = (int)OrderLogFlagEnum.订单处理环节
                };
                orderLogRepository.InsertAsync(orderLogInfo);
            }
            catch (Exception ex)
            {
                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = ClogTagType;
                tags["method"] = "DeleteOrderPackDetail";
                tags["orderpackno"] = dto.orderPackNo;
                CLSLogger.Error("取消包裹详情发生异常", ex.Message, tags);
                throw new RequestValidationException($"取消包裹详情发生异常，失败原因：{ex.Message}");
            }
        }

        /// <summary>
        /// 自动填充物流
        /// </summary>
        /// <param name="dto"></param>
        [HttpPut]
        public void AutoFillCarrierId(OrderPackDTO dto)
        {
            SplitOrderDomainService splitOrderDomainService = new SplitOrderDomainService();
            string promptMessage = string.Empty;

            if (dto.orderPackNo == null)
            {
                throw new RequestValidationException("input cannot be null");
            }

            var orderPackInfo = orderPackRepository.FirstOrDefault(x => x.orderPackNo == dto.orderPackNo);
            if (orderPackInfo == null)
            {
                throw new RequestValidationException($"失败。失败原因：找不到包裹号。orderPackNo：{dto.orderPackNo}");
            }

            if (!orderPackInfo.carrierId.IsNullOrWhiteSpace() || orderPackInfo.status == (int)OrderPackStatusEnum.Outbound || orderPackInfo.freightId.IsNullOrWhiteSpace())
            {
                throw new RequestValidationException($"失败。失败原因：请检查包裹是否已有单号/是否已出库/是否没有选择物流。orderPackNo：{orderPackInfo.orderPackNo}");
            }

            List<AutoSplitOrderFreightConfig> listAutoSplitOrderFreightConfig = splitOrderDomainService.GetAutoSplitOrderFreightConfig();

            if (listAutoSplitOrderFreightConfig.IsNullOrEmpty())
            {
                throw new RequestValidationException($"失败。失败原因：未找到AutoSplitOrderFreight配置。请重试");
            }

            var freightSetting = listAutoSplitOrderFreightConfig.FirstOrDefault(x => x.ExportFlag == orderPackInfo.exportFlag);

            var resultAutoFill = splitOrderDomainService.AutoFillCarrierId(freightSetting, orderPackInfo, ref promptMessage);

            //add log
            OrderLogEntity orderLog = new OrderLogEntity
            {
                flag = 0,
                OPID = orderPackInfo.opID,
                OID = orderPackInfo.oID,
                OptDate = DateTime.UtcNow
            };

            if (!resultAutoFill)
            {
                string log = string.Format("自动填充物流单号失败，请稍候重试，原因：{0}", promptMessage);
                orderLog.Comments = log;
            }
            else
            {
                var carriderID = orderPackRepository.FirstOrDefault(x => x.opID == orderPackInfo.opID).carrierId;
                orderLog.Comments = string.Format("使用自动填充物流修改快递单号为：{0}", carriderID);
            }

            orderLogRepository.Insert(orderLog);
        }

        /// <summary>
        /// 创建新单
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public OrderPackDTO AddNewPack(OrderPackDTO dto)
        {
            if (dto.orderPackNo == null)
            {
                throw new RequestValidationException("input cannot be null");
            }

            var orderPackInfo = orderPackRepository.FirstOrDefault(x => x.orderPackNo == dto.orderPackNo);
            if (orderPackInfo == null)
            {
                throw new RequestValidationException($"失败。失败原因：找不到包裹号。orderPackNo：{dto.orderPackNo}");
            }

            try
            {
                //get max packno for orderpacks with same oid
                var maxPackNo = orderPackRepository.GetAll().Where(x => x.oID == orderPackInfo.oID).Max(x => x.packNo);
                var newPackNo = maxPackNo == null ? 1 : maxPackNo + 1;
                var oldOrderPackNo = orderPackInfo.orderPackNo;
                var oldOIdpackNo = orderPackInfo.oIdPackNo;

                var newOrderPack = new OrderPackEntity
                {
                    oID = orderPackInfo.oID,
                    orderID = orderPackInfo.orderID,
                    packNo = newPackNo,
                    carrierId = "",
                    rec_name = orderPackInfo.rec_name,
                    rec_addr = orderPackInfo.rec_addr,
                    rec_phone = orderPackInfo.rec_phone,
                    idNo = orderPackInfo.idNo,
                    status = 0,
                    taxVal = 0,
                    weightVal = 0,
                    pseq = orderPackInfo.pseq,
                    exportFlag = orderPackInfo.exportFlag,
                    orderPackNo = $"{orderPackInfo.orderID}-{newPackNo}-{newPackNo}",
                    oIdPackNo = $"{orderPackInfo.oID}-{newPackNo}-{newPackNo}",
                    opt_time = DateTime.UtcNow,
                    freezeFlag = 0
                };

                orderPackRepository.InsertAsync(newOrderPack);



                var listOrderPack = orderPackRepository.GetAll().Where(x => x.oID == orderPackInfo.oID).ToList();

                for (int i = 0; i < listOrderPack.Count(); i++)
                {
                    listOrderPack[i].orderPackNo = $"{orderPackInfo.orderID}-{newPackNo}-{listOrderPack[i].packNo}";
                    listOrderPack[i].oIdPackNo = $"{orderPackInfo.oID}-{newPackNo}-{listOrderPack[i].packNo}";
                }

                OrderLogEntity orderLog = new OrderLogEntity
                {
                    Comments = $"创建新单，原单{oldOrderPackNo}({oldOIdpackNo})",
                    flag = (int)OrderLogFlagEnum.订单处理环节,
                    OPID = orderPackInfo.opID,
                    OID = orderPackInfo.oID,
                    OptDate = DateTime.UtcNow,
                };

                orderLogRepository.InsertAsync(orderLog);

                var result = new OrderPackDTO
                {
                    orderPackNo = listOrderPack.FirstOrDefault(x => x.opID == orderPackInfo.opID).orderPackNo
                };

                return result;
            }
            catch (Exception ex)
            {
                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = ClogTagType;
                tags["method"] = "AddNewPack";
                tags["orderPackNo"] = dto.orderPackNo;
                CLSLogger.Error("创建新单发生异常", ex.Message, tags);
                throw new RequestValidationException($"创建新单发生异常，失败原因：{ex.Message}");
            }
        }

        /// <summary>
        /// 重新分配选中明细
        /// </summary>
        /// <param name="dto"></param>
        public void ReallocateOrderDetail(ReallocateOrderDetailDTO dto)
        {
            if (dto.currentOrderPackNo == null || dto.targetOrderPackNo == null)
            {
                throw new RequestValidationException("input cannot be null");
            }
            if (dto.listOpdid == null)
            {
                throw new RequestValidationException("请先选中包裹明细。");
            }

            var currentOrderPackInfo = orderPackRepository.FirstOrDefault(x => x.orderPackNo == dto.currentOrderPackNo);
            if (currentOrderPackInfo == null)
            {
                throw new RequestValidationException($"失败。失败原因：找不到现包裹号。currentOrderPackNo：{dto.currentOrderPackNo}");
            }

            var targetOrderPackInfo = orderPackRepository.FirstOrDefault(x => x.orderPackNo == dto.targetOrderPackNo);
            if (targetOrderPackInfo == null)
            {
                throw new RequestValidationException($"失败。失败原因：找不到目标包裹号。targetOrderPackNo：{dto.targetOrderPackNo}");
            }

            if (currentOrderPackInfo.status != (int)OrderPackStatusEnum.Unassigned && targetOrderPackInfo.status != (int)OrderPackStatusEnum.Unassigned)
            {
                throw new RequestValidationException($"失败。失败原因：包裹状态不为未配，无法重新分配。currentOrderPackInfo：{dto.currentOrderPackNo}，targetOrderPackNo：{dto.targetOrderPackNo}");
            }
            try
            {
                string logComment = $"从{currentOrderPackInfo.orderPackNo}({currentOrderPackInfo.oIdPackNo})拆单到{targetOrderPackInfo.orderPackNo}({targetOrderPackInfo.oIdPackNo}),";

                var listCurrentOrderPackDetail = orderPackDetailRepository.GetAll().Where(x => x.opid == currentOrderPackInfo.opID).ToList();

                for (int i = 0; i < dto.listOpdid.Count; i++)
                {
                    var detailInfo = listCurrentOrderPackDetail.FirstOrDefault(x => x.opdid == dto.listOpdid[i]);

                    logComment += $"{detailInfo.sku} {detailInfo.product_name}x{detailInfo.qty}；";

                    var newDetailInfo = new OrderPackDetailEntity
                    {
                        price = detailInfo.price,
                        sku = detailInfo.sku,
                        product_name = detailInfo.product_name,
                        status = 0,
                        inputqty = 0,
                        taxrate = detailInfo.taxrate,
                        weight = detailInfo.weight,
                        flag = 1,
                        barcode = detailInfo.barcode,
                        enname = detailInfo.enname,

                        opid = targetOrderPackInfo.opID,
                        oid = targetOrderPackInfo.oID,
                        qty = detailInfo.qty,
                        total_w = detailInfo.total_w,
                        total_t = detailInfo.total_t
                    };

                    orderPackDetailRepository.DeleteAsync(detailInfo);
                    orderPackDetailRepository.InsertAsync(newDetailInfo);
                }

                OrderLogEntity orderLog = new OrderLogEntity
                {
                    Comments = logComment,
                    flag = (int)OrderLogFlagEnum.订单处理环节,
                    OPID = currentOrderPackInfo.opID,
                    OID = currentOrderPackInfo.oID,
                    OptDate = DateTime.UtcNow,
                    //记录订单状态
                    relFlag1Title = "status",
                    relFlag1Val = currentOrderPackInfo.status,
                    relFlagTitle = "exportflag",
                    relFlagVal = currentOrderPackInfo.exportFlag,
                    relFlag2Title = "missflag",
                    relFlag2Val = currentOrderPackInfo.missFlag,
                    notes1 = $"{targetOrderPackInfo.orderPackNo}({targetOrderPackInfo.oIdPackNo})"
                };

                orderLogRepository.InsertAsync(orderLog);
            }
            catch (Exception ex)
            {
                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = ClogTagType;
                tags["method"] = "ReallocateOrderDetail";
                tags["currentOrderPackNo"] = dto.currentOrderPackNo;
                tags["targetOrderPackNo"] = dto.targetOrderPackNo;
                CLSLogger.Error("重新分配选中明细发生异常", ex.Message, tags);
                throw new RequestValidationException($"重新分配选中明细发生异常，失败原因：{ex.Message}");
            }

        }

        [HttpGet]
        public List<string> GetRelatedOrderPackNo(OrderPackDTO dto)
        {
            if (dto.orderId == null)
            {
                throw new RequestValidationException("input cannot be null");
            }

            var listOrderPack = orderPackRepository.GetAll().Where(x => x.orderID == dto.orderId).ToList();
            if (listOrderPack == null)
            {
                throw new RequestValidationException($"失败。失败原因：找不到相关包裹号。orderId：{dto.orderId}");
            }
            else
            {
                List<string> result = new List<string>();
                foreach (var item in listOrderPack)
                {
                    result.Add(item.orderPackNo);
                }
                return result;
            }
        }
    }
}
