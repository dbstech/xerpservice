using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
namespace MagicLamp.PMS
{
    public class FreightsSetsAppService : PMSCrudAppServiceBase<FreightsSetsEntity, FreightsSetsDTO, FreightsSetsDTO, int>
    {

        private IRepository<FreightsSetsEntity, int> _freightsSetsRepository;
        public FreightsSetsAppService(IRepository<FreightsSetsEntity, int> freightsSetsRepository) : base(freightsSetsRepository)
        {
            _freightsSetsRepository = freightsSetsRepository;
        }
    }
}

