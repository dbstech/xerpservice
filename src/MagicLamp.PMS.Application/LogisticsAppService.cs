﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.UI;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Proxy;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Abp.Extensions;
using MagicLamp.PMS.Domain;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.Enums;
using MagicLamp.PMS.DTOs.Enums;
using Abp.Json;
using Microsoft.AspNetCore.Authorization;

namespace MagicLamp.PMS
{
    public class LogisticsAppService : PMSCrudAppServiceBase<LogisticsFeeEntity, LogisticsFeeDTO, LogisticsFeeDTO, long>
    {
        private IAccountManager _accountManager;
        private const string ClogTagType = "logisticsfee";
        private IRepository<LogisticsFeeEntity, long> _logisticsfeeRepository;

        public LogisticsAppService(
         IRepository<LogisticsFeeEntity, long> logisticsfeeRepository,
         IAccountManager accountManager)
         : base(logisticsfeeRepository)
        {
            _logisticsfeeRepository = logisticsfeeRepository;
            _accountManager = accountManager;
        }

        [Authorize]
        public override ExcelImportOutputDTO ImportExcel(IFormFile file)
        {

            ExcelImportOutputDTO outputDTO = new ExcelImportOutputDTO();

            try
            {
                if (file.Length <= 0)
                {
                    throw new UserFriendlyException("文件大小异常，请选择正确的文件");
                }

                string filePath = Ultities.CreateDirectory("App_Data/ExcelImport");
                string fileName = $"{Guid.NewGuid()}{file.FileName}";
                string fullPath = Path.Combine(filePath, fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                var mapper = new Npoi.Mapper.Mapper(fullPath);
                var dtos = mapper.Take<LogisticsFeeDTO>().Where(x => x.Value != null && !string.IsNullOrEmpty(x.Value.CourierNO)).Select(x => x.Value).ToList();

                File.Delete(fullPath);

                if (dtos.IsNullOrEmpty())
                {
                    throw new UserFriendlyException("excel中未找到有效数据，请确保excel模板正确且有数据");
                }

                var duplicateCourierNO = dtos.GroupBy(x => x.CourierNO).Where(x => x.Count() > 1).ToList();
                StringBuilder errorMsg = new StringBuilder();

                foreach (var item in duplicateCourierNO)
                {
                    errorMsg.Append($"{item.Key}，");
                }

                if (errorMsg.Length > 0)
                {
                    errorMsg.Append($" 物流单号发现重复，请删除重复项");
                    throw new UserFriendlyException(errorMsg.ToString());
                }

                //  TODO: use validation attribute, validate customername, couriername is legit
                dtos.ForEach(x =>
                {
                    if (x.CustomerName.IsNullOrEmpty())
                    {
                        throw new UserFriendlyException("货主不能为空");
                    }
                    if (x.CourierNO.IsNullOrEmpty())
                    {
                        throw new UserFriendlyException("运单号不能为空");
                    }
                    if (x.CourierName.IsNullOrEmpty())
                    {
                        throw new UserFriendlyException("承运方不能为空");
                    }
                    if (x.Fee.IsNullOrEmpty())
                    {
                        throw new UserFriendlyException("结算金额不能为空");
                    }
                    if (x.Weight.IsNullOrEmpty())
                    {
                        throw new UserFriendlyException("计费重量（KG）不能为空");
                    }
                    if (x.SettlementAccount.IsNullOrEmpty())
                    {
                        throw new UserFriendlyException("结算账单编号不能为空");
                    }
                    if (x.SettlementDate.IsNullOrEmpty())
                    {
                        throw new UserFriendlyException("结算日期不能为空");
                    }
                    if (x.CurrencyType.IsNullOrEmpty())
                    {
                        throw new UserFriendlyException("结算货币不能为空");
                    }
                    if (x.InvoiceNumber.IsNullOrEmpty())
                    {
                        throw new UserFriendlyException("发票号码不能为空");
                    }
                    if (!x.InvoiceDate.HasValue)
                    {
                        throw new UserFriendlyException("发票日期不能为空");
                    }
                });

                var courierno = dtos.Select(x => x.CourierNO).Distinct().ToList();
                var courierOrders = _logisticsfeeRepository.GetAll().Where(x => courierno.Contains(x.CourierNO)).ToList();
                var entities = dtos.MapTo<List<LogisticsFeeEntity>>();
                int existsedroww = 0, newrows = 0;

                List<LogUserActivityVO> userActivities = new List<LogUserActivityVO> { };

                entities.ForEach(x =>
                {
                    var existedEntry = courierOrders.FirstOrDefault(y => y.CourierNO == x.CourierNO);
                    if (existedEntry != null)
                    {
                        userActivities.Add(new LogUserActivityVO
                        {
                            ObjectType = UserActivityObjectType.LogisticsFee,
                            ObjectId = existedEntry.CourierNO.ToString(),
                            Action = UserActionEnum.Update.ToString(),
                            CreatedAt = DateTime.UtcNow,
                            Content = $"修改了物流实付运费, 物流单号'{x.CourierNO}'",
                            Before = existedEntry.ToJsonString(),
                            After = x.ToJsonString()
                        });

                        existedEntry.CourierName = x.CourierName;
                        existedEntry.CustomerName = x.CustomerName;
                        existedEntry.CourierNO = x.CourierNO;
                        existedEntry.Weight = x.Weight;
                        existedEntry.Fee = x.Fee;
                        existedEntry.CurrencyType = x.CurrencyType;
                        existedEntry.SettlementDate = x.SettlementDate;
                        existedEntry.SettlementAccount = x.SettlementAccount;
                        existedEntry.Updated_at = DateTime.UtcNow;
                        existedEntry.InvoiceNumber = x.InvoiceNumber;
                        var invoiceDate = TimeZoneInfo.ConvertTime(x.InvoiceDate ?? DateTime.MinValue, TimeZoneInfo.FindSystemTimeZoneById("Pacific/Auckland"));
                        existedEntry.InvoiceDate = TimeZoneInfo.ConvertTimeToUtc(invoiceDate);
                        existsedroww = existsedroww + 1;
                    }
                    else
                    {
                        userActivities.Add(new LogUserActivityVO
                        {
                            ObjectType = UserActivityObjectType.LogisticsFee,
                            ObjectId = x.CourierNO.ToString(),
                            Action = UserActionEnum.Update.ToString(),
                            CreatedAt = DateTime.UtcNow,
                            Content = $"创建了物流实付运费, 物流单号'{x.CourierNO}'",
                            After = x.ToJsonString()
                        });

                        _logisticsfeeRepository.Insert(x);
                        newrows = newrows + 1;
                    }
                });

                CurrentUnitOfWork.SaveChanges();

                userActivities.ForEach(x =>
                {
                    _accountManager.LogUserActivity(x);
                });

                outputDTO.IsSuccess = true;
                outputDTO.Message = $"成功导入{entities.Count}条数据, 其中更新{existsedroww}, 新增加{newrows};";
                return outputDTO;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("导入excel发生异常", ex);
                if (ex is UserFriendlyException)
                {
                    throw ex;
                }
                throw new UserFriendlyException("导入excel发生异常", ex.ToString());

            }


        }


    }
}
