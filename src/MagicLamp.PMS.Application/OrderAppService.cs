﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class OrderAppService : PMSCrudAppServiceBase<OrderEntity, OrderDTO, OrderDTO, int>
    {
        private IRepository<OrderEntity, int> orderRepository;
        public OrderAppService(IRepository<OrderEntity, int> _orderRepository) : base(_orderRepository)
        {
            orderRepository = _orderRepository;
        }

        public string PushOrders()
        {
            return "";
        }
    }
}
