﻿using Abp.Application.Services;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Runtime.Caching;
using MagicLamp.PMS.Domain;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.Infrastructure.Common;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using MagicLamp.PMS.EntityFrameworkCore;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class StockAppService : PMSCrudAppServiceBase<APIStockEntity, APIStockDTO, APIStockDTO, string>
    {
        private const string ClogTagType = "Stock";

        private const string MFD = "MFD";
        private const string HH = "HH";

        private IRepository<APIStockEntity, string> apiStockRepository;
        private IRepository<ProductEntity, string> productRepository;
        private IRepository<APICompanyEntity, string> apiCompanyRepository;
        private IBackgroundJobManager backgroundJobManager;
        private ICacheManager cacheManager;
        private IRepository<APIConfigEntity, string> apiConfigRepository;
        private IHttpContextAccessor httpContextAccessor;
        private IHostingEnvironment _env;

        public StockAppService(IRepository<APIStockEntity, string> _apiStockRepository,
            IRepository<ProductEntity, string> _productRepository,
            IRepository<APICompanyEntity, string> _apiCompanyRepository,
            IBackgroundJobManager _backgroundJobManager,
            ICacheManager _cacheManager,
            IRepository<APIConfigEntity, string> _apiConfigRepository,
            IHttpContextAccessor _httpContextAccessor,
            IHostingEnvironment env) : base(_apiStockRepository)
        {
            apiStockRepository = _apiStockRepository;
            productRepository = _productRepository;
            apiCompanyRepository = _apiCompanyRepository;
            backgroundJobManager = _backgroundJobManager;
            cacheManager = _cacheManager;
            apiConfigRepository = _apiConfigRepository;
            httpContextAccessor = _httpContextAccessor;
            _env = env;
        }

        private bool ValidateAPICompanyKey(string key)
        {
            bool result = false;

            var cacheInstance = cacheManager.GetCache(CacheKeys.APPSERVICECACHEKEYNAME);
            var cacheItem = cacheInstance.GetOrDefault(CacheKeys.APICOMPANYTOKENKEY);

            if (cacheItem == null)
            {
                var apiCompanyInfo = apiCompanyRepository.FirstOrDefault(x => x.apikey == key);

                if (apiCompanyInfo != null)
                {
                    cacheInstance.Set(CacheKeys.APICOMPANYTOKENKEY, apiCompanyInfo, TimeSpan.FromDays(1));
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                var apiCompanyInfo = (APICompanyEntity)cacheItem;
                if (apiCompanyInfo.company_id.StartsWith("alt"))
                {
                    result = true;
                }
            }

            return result;
        }


        //if existed, check sku 
        private bool IsOrderIDCached(string orderID)
        {
            bool result = false;
            var cacheInstance = cacheManager.GetCache(CacheKeys.APPSERVICECACHEKEYNAME);
            var cacheItem = cacheInstance.GetOrDefault(string.Format(CacheKeys.RESERVESTOCKCACHEDORDERID, orderID));

            if (cacheItem != null)
            {
                result = true;
            }

            return result;
        }

        private bool SetCachedOrderID(ReserveStockInputDTO input, StringBuilder logBuilder)
        {
            bool result = false;

            var cacheInstance = cacheManager.GetCache(CacheKeys.APPSERVICECACHEKEYNAME);

            ProductDomainService productDomain = new ProductDomainService();

            int seconds = productDomain.FormalizeReserveSeconds(input.SecondsReserved);

            if (!input.SecondsReserved.HasValue)
            {
                logBuilder.Append($"预定时间为null，设为{seconds}秒。");
            }
            else
            {
                logBuilder.Append($"预定时间为{seconds}秒。");
            }

            cacheInstance.Set(string.Format(CacheKeys.RESERVESTOCKCACHEDORDERID, input.OrderID), input.OrderID, TimeSpan.FromSeconds(seconds));
            result = true;

            return result;
        }


        /// <summary>
        /// Sync HH stock vlaue and HH expiry date of related SKU.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public SyncHHStockOutput SyncStockFromHH(List<SyncHHStockDTO> dtos)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = ClogTagType,
                ["method"] = "SyncStockFromHH"
            };

            List<string> listIPWhite = new List<string> { "8.219.98.74" }; //47.74.85.66

            SyncHHStockOutput result = new SyncHHStockOutput();

            CLSLogger.Info("HH发起同步库存请求", $"请求IP: {httpContextAccessor.GetIPAddress()}, 同步数量为: {dtos.Count()}, 同步明细:{dtos.ToJsonString()} ", clogTags);
        

            bool validIp =  _env.IsProduction() ? listIPWhite.Contains(httpContextAccessor.GetIPAddress()) : true;

            //delay 1 second to avoid stock sync happens at the same time of ProcessCreateOrderAsyncResultFromHH() in PushOrderToFluxJob

            if (validIp)
            {
                backgroundJobManager.EnqueueAsync<SyncStockFromHHJob, List<SyncHHStockDTO>>(dtos, BackgroundJobPriority.BelowNormal, TimeSpan.FromSeconds(3));
                backgroundJobManager.EnqueueAsync<SyncHHPriceTo188, List<SyncHHStockDTO>>(dtos);
                result.Success = true;
                return result;
            }
            else
            {
                CLSLogger.Info("拒绝HH发起同步库存请求", $"IP不在白名单中，请求IP: {httpContextAccessor.GetIPAddress()}，白名单IP为：{listIPWhite.ToJsonString()}", clogTags);
            }

            return result;

        }


        /// <summary>
        /// 用户付款之前，预定库存
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public ReserveStockOutputDTO ReserveStock(ReserveStockInputDTO input)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = ClogTagType,
                ["method"] = "ReserveStock",
                ["orderid"] = input.OrderID
            };

            StringBuilder logBuilder = new StringBuilder();
            var inputListSKU = input.ListSKU.Select(x => x.SKU).ToList();
            ReserveStockOutputDTO result = new ReserveStockOutputDTO
            {
                ListStock = new List<SimpleSKUWithFlagDTO>(),
                Success = true
            };

            if (IsOrderIDCached(input.OrderID))
            {
                result.Message = "无需重复预定库存。";
                return result;
            }

            if (!ValidateAPICompanyKey(input.Key))
            {
                logBuilder.Append("验证Key失败。");

                CLSLogger.Info("预定库存失败", logBuilder.ToString(), clogTags);
                result.Success = false;
                result.Message = "预定库存失败。失败原因：验证Key失败，请检查Key是否正确。";
                return result;
            }

            LockSKUWhenReservingJobInputDTO lockSKU = new LockSKUWhenReservingJobInputDTO
            {
                OrderID = input.OrderID,
                SecondsReserved = input.SecondsReserved,
                ListSKU = new List<SimpleSKUWithFlagDTO>(),
                ListOrderSKU = inputListSKU
            };

            List<OrderSKULockForHHEntity> listOrderSKUStockHHLock = new List<OrderSKULockForHHEntity>();
            List<ProductStockVO> listProductStockVOForUpdate = new List<ProductStockVO>();
            if (input == null || input.OrderID.IsNullOrEmpty() || input.ListSKU.IsNullOrEmpty())
            {
                throw new RequestValidationException("失败。失败原因：input不能为空，请检查OrderID，ListSKU。");
            }

            try
            {
                StockShareDomainService stockShareDomainService = new StockShareDomainService();
                ProductDomainService productDomainService = new ProductDomainService();
                string originalRequest = input.ToJsonString();
                CLSLogger.Info("ReserveStock请求报文", originalRequest, clogTags);
                //为分单和判断库存阶段预留处理时间，因此增加预定库存的时间
                IncreaseReservationTime(input, logBuilder);
                List<ProductStockVO> listProductStockVO = productDomainService.GetStocks(input.Partner, inputListSKU);
                List<ProductEntity> listProductInfo = productDomainService.GetProductsFromCache(inputListSKU);
                if (!IsProductInfoValid(input, listProductInfo, result))
                {
                    return result;
                }

                ExecuteReserveStock(input, listProductInfo, listProductStockVO, lockSKU, listProductStockVOForUpdate, listOrderSKUStockHHLock, result, logBuilder);

                //update cahced stock
                if (result.Success)
                {
                    logBuilder.Append("预定库存成功。");
                    productDomainService.UpdateProductStockCache(listProductStockVOForUpdate.ToArray());
                    if (!lockSKU.ListSKU.IsNullOrEmpty())
                    {
                        LockSKUWhenReservingJob lockSKUWhenReservingJob = new LockSKUWhenReservingJob();
                        lockSKUWhenReservingJob.Process(lockSKU);
                        //async refresh lock stock
                        backgroundJobManager.Enqueue<RefreshSKULockQtyJob, List<string>>(lockSKU.ListOrderSKU, BackgroundJobPriority.Normal);
                    }
                    if (SetCachedOrderID(input, logBuilder))
                    {
                        logBuilder.Append("OrderID存入缓存。");
                    }
                    //lock sku stock for hh
                    productDomainService.AddHHStockLockByOrderID(listOrderSKUStockHHLock);
                }
                else
                {
                    logBuilder.Append("预定库存失败。");
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("ReserveStock失败", ex, clogTags);
            }
            finally
            {
                CLSLogger.Info("ReserveStock响应报文", result.ToJsonString(), clogTags);
                CLSLogger.Info("ReserveStock完成", logBuilder.ToString(), clogTags);
            }

            return result;
        }

        /// <summary>
        /// 为分单和判断库存阶段预留处理时间，因此增加预定库存的时间
        /// </summary>
        /// <param name="input"></param>
        /// <param name="logBuilder"></param>
        private void IncreaseReservationTime(ReserveStockInputDTO input, StringBuilder logBuilder)
        {
            logBuilder.Append($"预定时长增加2小时，原时长为{input.SecondsReserved}，为分单和判断库存保留时间。");
            input.SecondsReserved += 7200;
        }

        public void TriggerSyncFluxStockJob(List<SyncFluxStockDTO> listOfSKU)
        {
            var jobInput = listOfSKU.Select(x => x.Sku).Distinct().ToList();

            backgroundJobManager.Enqueue<SyncFluxStockJob, List<string>>(jobInput, BackgroundJobPriority.High);
        }

        private bool IsProductInfoValid(ReserveStockInputDTO input, List<ProductEntity> listProductInfo, ReserveStockOutputDTO result)
        {
            var noProductInfoSkus = input.ListSKU.Where(x => listProductInfo.Any(item => item.SKU.ToLower().Trim() == x.SKU.ToLower().Trim()) == false)
                    .Select(x => x.SKU.Trim()).ToList();
            //check if sku exists
            if (!noProductInfoSkus.IsNullOrEmpty())
            {
                StringBuilder notExistsSkusLog = new StringBuilder();
                foreach (var item in noProductInfoSkus)
                {
                    notExistsSkusLog.Append($"SKU未找到，SKU：{item}。订单预定库存失败。");
                    result.Success = false;
                }
                result.Message = notExistsSkusLog.ToString();
                return false;
            }

            return true;
        }

        private void ExecuteReserveStock(ReserveStockInputDTO input, List<ProductEntity> listProductInfo,
            List<ProductStockVO> listProductStockVO, LockSKUWhenReservingJobInputDTO lockSKU,
            List<ProductStockVO> listProductStockVOForUpdate, List<OrderSKULockForHHEntity> listOrderSKUStockHHLock,
            ReserveStockOutputDTO result, StringBuilder logBuilder)
        {
            foreach (SimpleSKUWithFlagDTO currentSKU in input.ListSKU)
            {
                ProductEntity productInfo = listProductInfo.FirstOrDefault(x => x.SKU.StandardizeSKU() == currentSKU.SKU.StandardizeSKU());
                bool allowOverselling = currentSKU.AllowOverselling;
                ProductStockVO productStockVOInfo = listProductStockVO.FirstOrDefault(x => x.Sku.ToLower().Trim() == currentSKU.SKU.ToLower().Trim());
                if (productStockVOInfo == null)
                {
                    logBuilder.Append($"未找到sku对应ProductStockVO，sku：{currentSKU.SKU}，qty：{currentSKU.Qty}，overselling：{currentSKU.AllowOverselling}");
                    continue;
                }
                else
                {
                    DetermineReservationResult(currentSKU, productStockVOInfo, input, productInfo, lockSKU, listProductStockVOForUpdate, listOrderSKUStockHHLock, result);
                }
            }
        }

        private void DetermineReservationResult(SimpleSKUWithFlagDTO currentSKU, ProductStockVO productStockVOInfo, ReserveStockInputDTO input, ProductEntity productInfo, LockSKUWhenReservingJobInputDTO lockSKU, List<ProductStockVO> listProductStockVOForUpdate, List<OrderSKULockForHHEntity> listOrderSKUStockHHLock, ReserveStockOutputDTO result)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = ClogTagType,
                ["method"] = "ReserveStock_usingHHStock"
            };

            PMSDbContext pmsContext = new PMSDbContextFactory().CreateDbContext();

            bool usingHHStock = false;
            //using hh
            if (
                (productStockVOInfo.OriginalHHStock != null
                && productStockVOInfo.OriginalStock < currentSKU.Qty
                && productInfo != null
                && productInfo.IsHHPurchase == true)
                    )
            {
                usingHHStock = true;
            }

            //if (
            //     pmsContext.Products.Where(x => x.SyncToEcinx == true && !string.IsNullOrWhiteSpace(x.ECinxSKU) && x.ActiveFlag == true
            //     && x.PurchasePeriodCode == "3rdParty" && x.SKU == productStockVOInfo.Sku).ToList().Count > 0
            //    )
            //{
            //    usingHHStock = true;
            //}


            CLSLogger.Info("ReserveStock_usingHHStock", usingHHStock.ToJsonString() + " 1-" + productStockVOInfo.ToJsonString() + " 2-" +
            currentSKU.ToJsonString() + " 3-"+ productInfo.ToJsonString()
            , clogTags);
            var stock = AssembleStock(currentSKU.Qty, productStockVOInfo, productInfo, usingHHStock);

            if (stock.LackQty > 0)
            {
                result.Success = false;
            }
            else
            {
                if (usingHHStock)
                {
                    ProductDomainService productDomain = new ProductDomainService();
                    OrderSKULockForHHEntity stockLockHH = new OrderSKULockForHHEntity
                    {
                        OrderID = input.OrderID,
                        SKU = currentSKU.SKU,
                        QTY = currentSKU.Qty,
                        //get secondsreserved
                        LockDuration = productDomain.FormalizeReserveSeconds(input.SecondsReserved),
                        CreationTime = DateTime.UtcNow
                    };

                    listOrderSKUStockHHLock.Add(stockLockHH);
                }
                else
                {
                    lockSKU.ListSKU.Add(currentSKU);
                    productStockVOInfo.Qty_available -= currentSKU.Qty;
                    productStockVOInfo.LockQty += currentSKU.Qty;
                    productStockVOInfo.OriginalStock -= currentSKU.Qty;
                    listProductStockVOForUpdate.Add(productStockVOInfo);
                }
            }

            result.ListStock.Add(stock);
        }

        private SimpleSKUWithFlagDTO AssembleStock(int qtyWanted, ProductStockVO productStockVOInfo, ProductEntity productInfo, bool usingHHStock)
        {
            int OriginalHHStock = productStockVOInfo.OriginalHHStock ?? 0;
            int stockToUse = usingHHStock ? OriginalHHStock : productStockVOInfo.Qty_available;
            int remainingQty = stockToUse - qtyWanted;
            var QtyLacking = remainingQty >= 0 ? 0 : Math.Abs(remainingQty);
          
            SimpleSKUWithFlagDTO stock = new SimpleSKUWithFlagDTO
            {
                SKU = productStockVOInfo.Sku,
                //spark 2022.08.02
                Stock = (productInfo != null && productInfo.IsHHPurchase == true && productInfo.PurchasePeriodCode != "virtualStock") ?
                                (productStockVOInfo.Qty_available + OriginalHHStock) : productStockVOInfo.Qty_available,
                //Stock = usingHHStock ? (productStockVOInfo.Qty_available + OriginalHHStock) : productStockVOInfo.Qty_available,

                LackQty = QtyLacking,
                OriginalStock = productStockVOInfo.OriginalStock,
                OriginalHHStock = OriginalHHStock,
                Qty = qtyWanted
            };

            if (QtyLacking > 0)
            {
                return stock;
            }
            else
            {
                stock.Stock -= qtyWanted;
                if (usingHHStock)
                {
                    stock.OriginalHHStock = OriginalHHStock - qtyWanted;
                }
                else
                {
                    stock.OriginalStock = productStockVOInfo.OriginalStock - qtyWanted;
                }
                return stock;
            }

        
         
        }
    }
}
