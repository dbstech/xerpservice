
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;

namespace MagicLamp.PMS
{
    public class BWCountryAppService : PMSCrudAppServiceBase<BWCountryEntity, BWCountryDTO, BWCountryDTO, int>
    {
        private IRepository<BWCountryEntity, int> _BWCountryRepository;
        public BWCountryAppService(IRepository<BWCountryEntity, int> BWCountryRepository) : base(BWCountryRepository)
        {
            _BWCountryRepository = BWCountryRepository;
        }
    }
}

