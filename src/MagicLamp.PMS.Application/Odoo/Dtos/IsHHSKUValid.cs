namespace MagicLamp.PMS.Odoo.Products.Dto {
    public class IsHHSKUValid {
        public bool Valid { get; set; }
        public string Message { get; set; }
    }   
}
