using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.UI;
using MagicLamp.PMS.Proxy;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using OdooRpc.CoreCLR.Client.Models;
using OdooRpc.CoreCLR.Client.Models.Parameters;
using Abp.Domain.Repositories;
using MagicLamp.PMS.Entities;
using System.Linq;
using MagicLamp.PMS.Odoo.Products.Dto;

namespace MagicLamp.PMS
{
    public class OdooAppService : ApplicationService
    {
        IRepository<ProductEntity, string> _productRepository;
        OdooProxy _odooProxy;

        public OdooAppService(IRepository<ProductEntity, string> productRepository, OdooProxy odooProxy)
        {
            _productRepository = productRepository;
            _odooProxy = odooProxy;
        }

        [HttpGet]
        public async Task<JObject[]> ProductCategories()
        {
            JObject[] categories;
            try
            {
                categories = await _odooProxy.GetAllCategories();
                return categories;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("获取odoo产品分级发生异常", ex.Message);
                throw new UserFriendlyException("获取odoo产品分级发生异常，请稍后再尝试");
            }
        }
        [HttpGet]
        public async Task<JObject[]> Users()
        {
            JObject[] users;
            try
            {
                users = await _odooProxy.GetAllUsers();
                return users;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("获取odoo用户发生异常", ex.Message);
                throw new UserFriendlyException("获取odoo用户发生异常，请稍后再尝试");
            }
        }
        [HttpGet]
        public async Task<JObject[]> AccountTaxes()
        {
            JObject[] accountTaxes;
            try
            {
                accountTaxes = await _odooProxy.GetAllAccountTaxes();
                return accountTaxes;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("获取odoo税种发生异常", ex.Message);
                throw new UserFriendlyException("获取odoo税种发生异常，请稍后再尝试");
            }
        }
        [HttpGet]
        public async Task<JObject[]> SearchProducts(string fieldName, string searchOperator, string value = "")
        {
            JObject[] products;
            try
            {
                products = await _odooProxy.GetProducts(
                new OdooDomainFilter().Filter(fieldName, searchOperator, value.Trim()),
                new OdooFieldParameters()
                    {
                        "id",
                        "default_code",
                        "barcode",
                        "name",
                        "cn_name",
                        "responsible_id"
                    }
                );
                return products;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("获取odoo产品发生异常", ex.Message);
                throw new UserFriendlyException("获取odoo产品发生异常，请稍后再尝试");
            }
        }

        [HttpGet]
        public async Task<IsHHSKUValid> IsHHSKUValid(string sku, string hhsku)
        {
            var isHHSKUValid = new IsHHSKUValid { };
            isHHSKUValid.Valid = false;

            ProductEntity product = _productRepository.FirstOrDefault(x => x.HHSKU == hhsku);

            // Invalid HHSKU if another product currently uses it
            if (product != null && product.SKU != sku)
            {
                isHHSKUValid.Message = $"该HHSKU已被以下产品使用：{product.SKU}";
                return isHHSKUValid;
            }

            JObject[] odooProducts;
            var odooDomainFilter = new OdooDomainFilter().Filter("default_code", "=", hhsku);
            var odooFieldParameters = new OdooFieldParameters()
            {
                "id",
                "default_code",
                "barcode",
                "name",
                "cn_name",
                "responsible_id"
            };

            try
            {
                odooProducts = await _odooProxy.GetProducts(odooDomainFilter, odooFieldParameters);
            }
            catch (Exception ex)
            {
                CLSLogger.Error("检验HHSKU发生异常", ex.Message);
                throw new UserFriendlyException("检验HHSKU发生异常，请稍后再尝试");
            }

            // Valid HHSKU if no odoo products uses it
            if (odooProducts.Length == 0)
            {
                isHHSKUValid.Valid = true;
                isHHSKUValid.Message = "并未在odoo上找到該HHSKU, 使用该HHSKU的话将会在odoo上创建新产品";
                return isHHSKUValid;
            }
            else if (odooProducts.Length == 1)
            {
                isHHSKUValid.Valid = true;
                return isHHSKUValid;
            }
            else if (odooProducts.Length > 1)
            {
                isHHSKUValid.Message = $"在odoo上找到{odooProducts.Length}个产品拥有该HHSKU";
                return isHHSKUValid;
            }

            return isHHSKUValid;
        }
    }
}