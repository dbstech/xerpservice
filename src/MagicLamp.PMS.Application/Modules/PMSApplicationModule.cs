﻿using System;
using Abp.AspNetCore;
using Abp.AspNetCore.Configuration;
using Abp.AutoMapper;
using Abp.FluentValidation;
using Abp.Hangfire.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;

using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Entities.DBQueryTypes;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Proxy.Odoo;
using MagicLamp.PMS.Brands.Dto;
using MagicLamp.PMS.ProductCategories;
using MagicLamp.PMS.CQBondedProduct;

namespace MagicLamp.PMS
{
    [DependsOn(
        typeof(PMSCoreModule),
        typeof(AbpAutoMapperModule),
        typeof(AbpAspNetCoreModule),
        typeof(AbpFluentValidationModule),
        typeof(PMSDomainModule))]
    public class PMSApplicationModule : AbpModule
    {

        public override void PreInitialize()
        {
            Configuration.EntityHistory.IsEnabled = true;

            Configuration.Modules.AbpAspNetCore().CreateControllersForAppServices(typeof(PMSApplicationModule).Assembly, moduleName: "app", useConventionalHttpVerbs: true);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(config =>
            {
                config.CreateMissingTypeMaps = true;

                config.CreateMap<IDCardEntity, IDCardDTO>()
                      .ForMember(x => x.Id, options => options.MapFrom(input => input.Number))
                      .ForMember(x => x.URLBackImage, options => options.MapFrom(input => input.URLBack))
                      .ForMember(x => x.URLFontImage, options => options.MapFrom(input => input.URLFont));

                config.CreateMap<OrderIDCardEntity, IDCardDTO>()
                   .ForMember(x => x.Id, options => options.MapFrom(input => input.OrderPackNo))
                   .ForMember(x => x.Number, options => options.MapFrom(input => input.IDCardNo))
                   .ForMember(x => x.Name, options => options.MapFrom(input => input.RecevierName))
                   .ForMember(x => x.UpdateTime, options => options.MapFrom(input => input.CreationTime))
                   .ForMember(x => x.CourierNo, options => options.MapFrom(input => input.CarrierID));

                config.CreateMap<ExportFlagEntity, ExportFlagDTO>()
                 .ForMember(x => x.Id, options => options.MapFrom(input => input.Flag));

                config.CreateMap<ProductEntity, ProductDTO>()
                    .ForMember(x => x.Materials, options => options.Ignore())
                    .ForMember(x => x.Warehouse, options => options.Ignore())
                    .ForMember(x => x.MainImages, options => options.Ignore())
                    .ForMember(x => x.DetailImages, options => options.Ignore())
                    .ForMember(x => x.SplitOrderCategoryName, options => options.Ignore())
                    .ForMember(x => x.CategoryName, options => options.Ignore())
                    .ForMember(x => x.VirtualStockQuantity, options => options.Ignore())
                    .ForMember(x => x.BrandId, options => options.MapFrom(input => input.BrandId))
                    .ForMember(x => x.PackMaterialID, options => options.MapFrom(input => input.MaterialsID))
                    .ForMember(x => x.CurrencyID, options => options.MapFrom(input => input.Currency))
                    .ForMember(x => x.CostTypeID, options => options.MapFrom(input => input.CostType))
                    .ForMember(x => x.OdooCategory, options => options.Ignore())
                    .ForMember(x => x.CountryName, options => options.Ignore())
                    .ForAllOtherMembers(options => options.Condition((src, dest, srcMember) =>
                    {
                        if (srcMember is string)
                        {
                            return !String.IsNullOrWhiteSpace(srcMember.ToString());
                        }
                        return srcMember != null;
                    }));

                config.CreateMap<ProductDTO, ProductEntity>()
                    .ForMember(x => x.Brand, options => options.Ignore())
                    .ForMember(x => x.OdooSyncStatus, options => options.Ignore())
                    .ForMember(x => x.SyncToOdoo, options => options.Condition((src, dest, srcMember) => srcMember != null))
                    .ForMember(x => x.AlertTime, options => options.Condition((src, dest, srcMember) => srcMember != null))
                    .ForMember(x => x.LifeTime, options => options.Condition((src, dest, srcMember) => srcMember != null))
                    .ForMember(x => x.BoxQty, options => options.Condition((src, dest, srcMember) => srcMember != null))
                    .ForMember(x => x.IsHHPurchase, options => options.Condition((src, dest, srcMember) => srcMember != null))
                    .ForMember(x => x.OdooProductID, options => options.Condition((src, dest, srcMember) => srcMember != null))
                    .ForMember(x => x.CategoryID, options => options.Condition((src, dest, srcMember) => src.CategoryID != null))
                    .ForMember(x => x.SKU, options => options.Condition((src, dest, srcMember) => srcMember != null && dest.SKU == null))
                    .ForMember(x => x.HHSKU, opt =>
                    {
                        opt.PreCondition(src => !String.IsNullOrWhiteSpace(src.HHSKU));
                        opt.MapFrom(src => (
                            (src.SyncToOdoo != true && src.IsHHPurchase != true && src.HHSKU.ToUpper() == "EMPTY") ? null : src.HHSKU
                        ));
                    })
                    .ForAllOtherMembers(options => options.Condition((src, dest, srcMember) =>
                    {
                        if (srcMember is string)
                        {
                            return !String.IsNullOrWhiteSpace(srcMember.ToString());
                        }
                        return srcMember != null;
                    }));

                config.CreateMap<BrandEntity, BrandDTO>()
                .ForMember(x => x.Logos, options => options.Ignore())
                .ForMember(x => x.AuthorizationFiles, options => options.Ignore())
                .ForMember(x => x.Banners, options => options.Ignore())
                .ForMember(x => x.CnBrand, options => options.MapFrom(x => x.ChineseName))
                .ForMember(x => x.EnBrand, options => options.MapFrom(x => x.EnglishName))
                .ForMember(x => x.BrandCategoryName, options => options.MapFrom(x => x.BrandCategory != null ? x.BrandCategory.Name : null));

                config.CreateMap<BrandDTO, BrandEntity>()
                .ForAllMembers(options => options.Condition((src, dest, srcMember) =>
                {
                    if (srcMember is string)
                    {
                        return !string.IsNullOrWhiteSpace(srcMember.ToString());
                    }
                    return srcMember != null;
                }));

                config.CreateMap<BrandFilterDTO, BrandEntity>()
                .ForMember(x => x.OdooSyncStatus, options => options.Ignore());

                config.CreateMap<BatchCreateBrandDTO, BrandEntity>()
                .ForMember(x => x.OdooSyncStatus, options => options.Ignore())
                .ForMember(x => x.CreateTime, options => options.Ignore())
                .ForMember(x => x.Id, options => options.Ignore())
                .ForMember(x => x.CreateUser, options => options.Ignore());

                config.CreateMap<BrandDTO, BrandEntity>()
                .ForMember(x => x.OdooSyncStatus, options => options.Ignore());

                config.CreateMap<UpdateBrandDto, BrandEntity>()
                .ForMember(x => x.OdooSyncStatus, options => options.Ignore())
                .ForMember(x => x.CreateTime, options => options.Ignore())
                .ForMember(x => x.Id, options => options.Ignore())
                .ForMember(x => x.BrandCategory, options => options.Ignore())
                .ForMember(x => x.BrandCategoryId, options => options.Ignore())
                .ForAllMembers(options => options.Condition((src, dest, srcMember) =>
                {
                    if (srcMember is string)
                    {
                        return !string.IsNullOrWhiteSpace(srcMember.ToString());
                    }
                    return srcMember != null;
                }));

                config.CreateMap<CreateBrandDto, BrandEntity>()
                .ForMember(x => x.OdooSyncStatus, options => options.Ignore())
                .ForMember(x => x.CreateTime, options => options.Ignore())
                .ForMember(x => x.Id, options => options.Ignore())
                .ForMember(x => x.BrandCategory, options => options.Ignore())
                .ForMember(x => x.BrandCategoryId, options => options.Ignore());

                config.CreateMap<OrderPackEntity, OrderPackDTO>()
               .ForMember(x => x.exportFlagName, options => options.Ignore())
               .ForMember(x => x.freezeFlagName, options => options.Ignore())
               .ForMember(x => x.warehouseStatus, options => options.Ignore())
               .ForMember(x => x.bizId, option => option.Ignore())
               .ForMember(x => x.ordercreate_time_begin, option => option.Ignore())
               .ForMember(x => x.ordercreate_time_end, option => option.Ignore())
               .ForMember(x => x.out_time_begin, option => option.Ignore())
               .ForMember(x => x.out_time_end, option => option.Ignore())
               .ForMember(x => x.trackingNo, option => option.Ignore())
               .ForMember(x => x.comment, option => option.Ignore())
               .ForMember(x => x.shippingMethod, option => option.Ignore())
               .ForMember(x => x.store, option => option.Ignore())
               .ForMember(x => x.freightName, option => option.Ignore())
               .ForMember(x => x.orderCreateTime, option => option.Ignore())
               .ForMember(x => x.sku, option => option.Ignore());

                config.CreateMap<FluxOrderInfoDTO, GFCDataMqDto>()
                .ForMember(x => x.Action, options => options.Ignore());

                config.CreateMap<OrderPackDetailEntity, OrderPackDetailDTO>()
                .ForMember(x => x.QtyAllocated, options => options.Ignore())
                .ForMember(x => x.Stock, options => options.Ignore())
                .ForMember(x => x.WarehouseStatus, options => options.Ignore())
                .ForMember(x => x.orderPackNo, options => options.Ignore())
                .ForMember(x => x.statusName, options => options.Ignore());

                config.CreateMap<OrderLogEntity, OrderLogDTO>()
                .ForMember(x => x.status, option => option.Ignore());

                config.CreateMap<AgentProductOrderCostEntity, AgentProductOrderCostDTO>()
                .ForMember(x => x.CreationTimeBegin, option => option.Ignore())
                .ForMember(x => x.CreationTimeEnd, option => option.Ignore())
                .ForMember(x => x.OrderTimeBegin, option => option.Ignore())
                .ForMember(x => x.OrderTimeEnd, option => option.Ignore());

                config.CreateMap<ChangJiangCreateOrderDTO, FreightCreateOrderInputDTO>()
                 .ForMember(x => x.ShippingMethod, option => option.Ignore())
                 .ForMember(x => x.ShippingLabelRemark, option => option.Ignore())
                 .ForMember(x => x.OrderRemark, option => option.Ignore());

                config.CreateMap<PackRuleEntity, PackRuleDTO>()
                .ForMember(x => x.exportFlagName, options => options.Ignore())
                .ForMember(x => x.cKindName, options => options.Ignore());

                config.CreateMap<APIStockEntity, APIStockDTO>()
                .ForMember(x => x.IsHHPurchase, options => options.Ignore())
                .ForMember(x => x.CreateTime, options => options.Ignore())
                .ForMember(x => x.IsVirtualStock, options => options.Ignore());

                config.CreateMap<PurchaseOrderLineEntity, PurchaseOrderLineDTO>()
                .ForMember(x => x.ItemId, options => options.MapFrom(input => input.Sku))
                .ForMember(x => x.UnitPrice, options => options.MapFrom(input => input.Cost))
                .ForMember(x => x.UnitPriceTax, options => options.MapFrom(input => input.TaxedCost));

                config.CreateMap<OrderPackQueryType, SkuLockQtyDto>();

                config.CreateMap<SyncProductCostPriceFilterDTO, SyncProductCostPriceEntity>()
                .ForMember(x => x.BeforePrice, options => options.Ignore())
                .ForMember(x => x.AfterPrice, options => options.Ignore())
                .ForMember(x => x.Id, options => options.Ignore())
                .ForMember(x => x.CreationTime, options => options.Ignore());

                config.CreateMap<ProductDTO, Product>()
                // .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.OdooProductID))
                .ForMember(dest => dest.default_code, opt => opt.MapFrom(src => src.HHSKU))
                .ForMember(dest => dest.cn_name, opt => opt.MapFrom(src => src.ChineseName))
                .ForMember(dest => dest.name, opt => opt.MapFrom(src => src.EnglishName))
                .ForMember(dest => dest.specs, opt => opt.MapFrom(src => src.Specs))
                .ForMember(dest => dest.barcode, opt => opt.MapFrom(src => src.Barcode1))
                .ForMember(dest => dest.brand_id, opt => opt.MapFrom(src => src.OdooBrandID))
                .ForMember(dest => dest.netweight, opt => opt.MapFrom(src => src.NetWeight / 1000))
                .ForMember(dest => dest.weight, opt => opt.MapFrom(src => src.GrossWeight / 1000))
                .ForMember(dest => dest.length, opt => opt.MapFrom(src => src.Length / 10))
                .ForMember(dest => dest.wide, opt => opt.MapFrom(src => src.Wide / 10))
                .ForMember(dest => dest.high, opt => opt.MapFrom(src => src.High / 10))
                .ForMember(dest => dest.volume, opt => opt.MapFrom(src => src.Length * src.Wide * src.High))
                .ForMember(dest => dest.supplier_barcode, opt => opt.MapFrom(src => src.SupplierBarcode))
                .ForMember(dest => dest.tracking, opt => opt.MapFrom(src => src.TrackingType))
                .ForMember(dest => dest.hs_code, opt => opt.MapFrom(src => src.HSCode))
                .ForMember(dest => dest.origin_country, opt => opt.MapFrom(src => src.CountryName))
                .ForMember(dest => dest.producer, opt => opt.MapFrom(src => src.Producer))
                .ForMember(dest => dest.description, opt => opt.MapFrom(src => src.Note))
                .ForMember(dest => dest.boxqty, opt => opt.MapFrom(src => src.BoxQty))
                .ForMember(dest => dest.categ_id, opt => opt.MapFrom(src => src.OdooCategoryID))
                .ForMember(dest => dest.type, opt => opt.MapFrom(src =>
                        src.PurchasePeriodCode == "3rdParty" ? "product" :
                        src.PurchasePeriodCode == "consignment" ? "product" :
                        src.PurchasePeriodCode == "selfPicking" ? "product" :
                        src.PurchasePeriodCode == "virtualStock" ? "service" :
                        "product"
                ))
                .ForMember(dest => dest.life_time, opt => opt.MapFrom(src => src.LifeTime))
                .ForMember(dest => dest.alert_time, opt => opt.MapFrom(src => src.AlertTime))
                .ForMember(dest => dest.taxes_id, opt => opt.Ignore())
                .ForMember(dest => dest.supplier_taxes_id, opt => opt.Ignore())
                .ForMember(dest => dest.company_id, opt => opt.Ignore())
                .ForMember(dest => dest.responsible_id, opt => opt.Ignore());

                config.CreateMap<ProductEntity, Product>()
                .ForMember(dest => dest.default_code, opt => opt.MapFrom(src => src.HHSKU))
                .ForMember(dest => dest.cn_name, opt => opt.MapFrom(src => src.ChineseName))
                .ForMember(dest => dest.name, opt => opt.MapFrom(src => src.EnglishName))
                .ForMember(dest => dest.specs, opt => opt.MapFrom(src => src.Specs))
                .ForMember(dest => dest.barcode, opt => opt.MapFrom(src => src.Barcode1))
                .ForMember(dest => dest.brand_id, opt => opt.MapFrom(src => src.Brand == null ? null : src.Brand.OdooBrandId))
                .ForMember(dest => dest.netweight, opt => opt.MapFrom(src => src.NetWeight / 1000))
                .ForMember(dest => dest.weight, opt => opt.MapFrom(src => src.GrossWeight / 1000))
                .ForMember(dest => dest.length, opt => opt.MapFrom(src => src.Length / 10))
                .ForMember(dest => dest.wide, opt => opt.MapFrom(src => src.Wide / 10))
                .ForMember(dest => dest.high, opt => opt.MapFrom(src => src.High / 10))
                .ForMember(dest => dest.volume, opt => opt.MapFrom(src => src.Length * src.Wide * src.High))
                .ForMember(dest => dest.supplier_barcode, opt => opt.MapFrom(src => src.SupplierBarcode))
                .ForMember(dest => dest.tracking, opt => opt.MapFrom(src => src.TrackingType))
                .ForMember(dest => dest.hs_code, opt => opt.MapFrom(src => src.HSCode))
                .ForMember(dest => dest.origin_country, opt => opt.MapFrom(src => src.Country == null ? null : src.Country.CountryName))
                .ForMember(dest => dest.producer, opt => opt.MapFrom(src => src.Producer))
                .ForMember(dest => dest.description, opt => opt.MapFrom(src => src.Note))
                .ForMember(dest => dest.boxqty, opt => opt.MapFrom(src => src.BoxQty))
                .ForMember(dest => dest.categ_id, opt => opt.MapFrom(src => src.OdooCategoryID))
                .ForMember(dest => dest.type, opt => opt.MapFrom(src =>
                        src.PurchasePeriodCode == "3rdParty" ? "product" :
                        src.PurchasePeriodCode == "consignment" ? "product" :
                        src.PurchasePeriodCode == "selfPicking" ? "product" :
                        src.PurchasePeriodCode == "virtualStock" ? "service" :
                        "product"
                ))
                .ForMember(dest => dest.life_time, opt => opt.MapFrom(src => src.LifeTime))
                .ForMember(dest => dest.alert_time, opt => opt.MapFrom(src => src.AlertTime))
                .ForMember(dest => dest.taxes_id, opt => opt.Ignore())
                .ForMember(dest => dest.supplier_taxes_id, opt => opt.Ignore())
                .ForMember(dest => dest.company_id, opt => opt.Ignore())
                .ForMember(dest => dest.responsible_id, opt => opt.Ignore());


                config.CreateMap<BrandCategoryFilterDTO, BrandCategoryEntity>()
               .ForMember(x => x.Id, options => options.Ignore())
               .ForMember(x => x.CreatedAt, options => options.Ignore());

                config.CreateMap<BrandCategoryDTO, BrandCategoryEntity>()
                .ForMember(x => x.CreatedAt, options => options.Ignore());

                config.CreateMap<FreightsDTO, FreightsEntity>()
                .ForMember(dest => dest.FreightsSets, options => options.Ignore())
                .ForAllOtherMembers(options => options.Condition((src, dest, srcMember) =>
                {
                    if (srcMember is string)
                    {
                        return !String.IsNullOrWhiteSpace(srcMember.ToString());
                    }
                    return srcMember != null;
                }));

                config.CreateMap<FreightsEntity, FreightsDTO>()
                .ForMember(dest => dest.Currency, options => options.Ignore());

                config.CreateMap<FreightsFilterDTO, FreightsEntity>()
                .ForMember(x => x.PricePerKg, options => options.Ignore())
                .ForMember(x => x.CostPerKg, options => options.Ignore())
                .ForMember(x => x.UpdateDate, options => options.Ignore());

                config.CreateMap<UnionPayPaymentTransactionEntity, UnionPayPaymentTransactionEntity>()
                .ForMember(x => x.CreatedDate, options => options.Ignore())
                .ForMember(x => x.UpdatedDate, options => options.Ignore());

                config.CreateMap<LatiPayPaymentTransactionEntity, LatiPayPaymentTransactionEntity>()
                .ForMember(x => x.CreatedDate, options => options.Ignore())
                .ForMember(x => x.UpdatedDate, options => options.Ignore());

                config.CreateMap<AllInPayPaymentTransactionEntity, AllInPayPaymentTransactionEntity>()
                .ForMember(x => x.CreatedDate, options => options.Ignore())
                .ForMember(x => x.UpdatedDate, options => options.Ignore());

                config.CreateMap<FreightOrderSplitCategoryRule, FreightOrderSplitCategoryRuleDto>()
                .ForMember(x => x.SameCategories, options => options.Ignore());

                config.CreateMap<CQBondedProductDto, CQBondedProductEntity>()
                .ForMember(x => x.CreateTime, options => options.Ignore())
                .ForMember(x => x.UpdateTime, options => options.Ignore());

                config.CreateMap<CreateCQBondedProductDto, CQBondedProductEntity>()
                .ForMember(x => x.CreateTime, options => options.Ignore())
                .ForMember(x => x.UpdateTime, options => options.Ignore());
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PMSApplicationModule).GetAssembly());
            IocManager.RegisterAssemblyByConvention(typeof(PMSDomainModule).GetAssembly());
            IocManager.RegisterAssemblyByConvention(typeof(PMSCoreModule).GetAssembly());
        }
    }
}
