﻿using Abp.Domain.Repositories;
using Abp.UI;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Proxy;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using Abp.Collections.Extensions;
using Abp.AutoMapper;

namespace MagicLamp.PMS
{
    public class BanktransferAppService : PMSCrudAppServiceBase<BanktransferEntity, BanktransferDTO, BanktransferDTO, long>
    {

        private const string ClogTagType = "Banktransfer";
        private IRepository<BanktransferEntity, long> banktransferRepository;

        public BanktransferAppService(
         IRepository<BanktransferEntity, long> _banktransferRepository)
         : base(_banktransferRepository)
        {
            banktransferRepository = _banktransferRepository;
        }

        public override ExcelImportOutputDTO ImportExcel(IFormFile file)
        {

            ExcelImportOutputDTO outputDTO = new ExcelImportOutputDTO();

            try
            {
                if (file.Length <= 0)
                {
                    throw new UserFriendlyException("文件大小异常，请选择正确的文件");
                }

                string filePath = Ultities.CreateDirectory("App_Data/ExcelImport");
                string fileName = $"{Guid.NewGuid()}{file.FileName}";
                string fullPath = Path.Combine(filePath, fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                var mapper = new Npoi.Mapper.Mapper(fullPath);
                var dtos = mapper.Take<BanktransferDTO>().Where(x => x.Value != null && !string.IsNullOrEmpty(x.Value.OrderNO)).Select(x => x.Value).ToList();

                File.Delete(fullPath);

                if (dtos.IsNullOrEmpty())
                {
                    throw new UserFriendlyException("excel中未找到有效数据，请确保excel模板正确且有数据");
                }

                var duplicateOrderNO = dtos.GroupBy(x => x.OrderNO).Where(x => x.Count() > 1).ToList();
                StringBuilder errorMsg = new StringBuilder();
                foreach (var item in duplicateOrderNO)
                {
                    errorMsg.Append($"{item.Key}，");
                }

                if (errorMsg.Length > 0)
                {
                    errorMsg.Append($" 订单号发现重复，请删除重复项");
                    throw new UserFriendlyException(errorMsg.ToString());
                }



                var orderno = dtos.Select(x => x.OrderNO).Distinct().ToList();
                var banktransferOrders = banktransferRepository.GetAll().Where(x => orderno.Contains(x.OrderNO)).ToList();
                var entities = dtos.MapTo<List<BanktransferEntity>>();
                int existsedroww = 0, newrows = 0;

                entities.ForEach(x =>
                {
                    var existedEntry = banktransferOrders.FirstOrDefault(y => y.OrderNO == x.OrderNO);
                    if (existedEntry != null)
                    {
                        existedEntry.OrderNO = x.OrderNO;
                        existedEntry.TargetCurrency = x.TargetCurrency;
                        existedEntry.PayMethod = x.PayMethod;
                        existedEntry.ExchangeDate = x.ExchangeDate;
                        existedEntry.CurrencyRate = x.CurrencyRate;
                        existedEntry.Total = x.Total;
                        existedEntry.Updated_at = System.DateTime.UtcNow;
                        existsedroww = existsedroww + 1;
                    }
                    else
                    {
                        banktransferRepository.Insert(x);
                        newrows = newrows + 1;
                    }
                });

                CurrentUnitOfWork.SaveChanges();

                outputDTO.IsSuccess = true;
                outputDTO.Message = $"成功导入{entities.Count}条数据, 其中更新{existsedroww}, 新增加{newrows};";
                return outputDTO;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("导入excel发生异常", ex);
                if (ex is UserFriendlyException)
                {
                    throw ex;
                }
                throw new UserFriendlyException("导入excel发生异常", ex.ToString());

            }


        }


    }
}
