using MagicLamp.PMS.Entities;
using MagicLamp.PMS.DTOs;
using Abp.Domain.Repositories;

namespace MagicLamp.PMS
{
    public class CustomKindAppService : PMSCrudAppServiceBase<CustomKindEntity, CustomKindDTO, CustomKindDTO, string>
    {
        private IRepository<CustomKindEntity, string> customKindRepositary;
        public CustomKindAppService(
            IRepository<CustomKindEntity, string> _customKindRepositary
        ) : base(_customKindRepositary)
        {
            customKindRepositary = _customKindRepositary;
        }
    }
}