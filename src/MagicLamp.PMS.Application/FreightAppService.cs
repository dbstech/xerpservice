﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class FreightAppService : PMSCrudAppServiceBase<FreightEntity, FreightDTO, FreightDTO, string>
    {

        private IRepository<FreightEntity, string> freightRepository;
        public FreightAppService(IRepository<FreightEntity, string> _freightRepository) : base(_freightRepository)
        {
            freightRepository = _freightRepository;
        }


    }


}

