﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using System.Reflection;

using Abp.AutoMapper;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using Abp.Dependency;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Infrastructure;
using System.Linq;
using MagicLamp.PMS.Infrastructure.Extensions;
using Abp.UI;
using MagicLamp.PMS.Proxy;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Npoi.Mapper;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.DTOs.Attributes;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace MagicLamp.PMS
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class PMSCrudAppServiceBase<TEntity, TDto, TSearchFilterDTO, TPrimaryKeyType>
        : ApplicationService
         where TEntity : class, IEntity<TPrimaryKeyType>
        where TDto : class, IEntityDto<TPrimaryKeyType>
        where TSearchFilterDTO : class
    {

        IRepository<TEntity, TPrimaryKeyType> BaseRepository;
        IRepository<ImageEntity, long> ImageRepository;
        IRepository<FileEntity, long> FileRepository;
        private readonly object DbUpdateException;

        protected PMSCrudAppServiceBase(IRepository<TEntity, TPrimaryKeyType> _repository)
        {
            LocalizationSourceName = PMSConsts.LocalizationSourceName;
            BaseRepository = _repository;
            ImageRepository = IocManager.Instance.Resolve<IRepository<ImageEntity, long>>();
            FileRepository = IocManager.Instance.Resolve<IRepository<FileEntity, long>>();
        }

        public virtual TDto Create(TDto input)
        {
            try
            {
                TEntity entity = input.MapTo<TEntity>();

                var properties = entity.GetProperties();
                foreach (var item in properties)
                {
                    if (!item.HasAttribute<DatabaseGeneratedAttribute>())
                    {
                        continue;
                    }

                    var val = item.GetValue(entity);

                    if ((typeof(DateTime).IsAssignableFrom(item.PropertyType) ||
                        typeof(DateTime?).IsAssignableFrom(item.PropertyType)) &&
                        (val == null || (DateTime)val == DateTime.MinValue))
                    {
                        item.SetValue(entity, DateTime.UtcNow);
                    }
                }

                TPrimaryKeyType result = BaseRepository.InsertAndGetId(entity);
                CurrentUnitOfWork.SaveChanges();
                input.Id = result;
                return input;
            }
            catch (DbUpdateException ex) when (ex.InnerException is SqlException)
            {
                throw ex.InnerException;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("create发生未知异常", $"{ex.Message}; 实体类信息：{input.ToJsonString()}");
                throw new UserFriendlyException("create发生未知异常", ex);
            }
        }

        public virtual bool Delete(string[] ids)
        {
            try
            {
                List<TPrimaryKeyType> splittedIds = ids.Select(
                    x => (TPrimaryKeyType)Convert.ChangeType(x, typeof(TPrimaryKeyType))
                ).ToList();
                BaseRepository.Delete(x => splittedIds.Contains(x.Id));
                return true;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("Delete发生未知异常", $"{ex.Message};Ids：{ids}");
                throw new UserFriendlyException("删除发生未知异常", ex);
            }
        }

        public virtual TDto Update(TDto input)
        {
            try
            {
                var propertyInfo = typeof(TEntity).GetPrimaryKeyPropertyInfo();
                var predicate = $" {propertyInfo.Name} = @0 ";
                TPrimaryKeyType keyVal = input.Id;
                if (keyVal == null)
                {
                    throw new UserFriendlyException("无效的Id请求参数");
                }

                var entity = BaseRepository.FirstOrDefault(input.Id);

                if (entity == null)
                {
                    throw new UserFriendlyException($"未找到对应主键：{input.Id} 记录，无法更新");
                }
                input.MapTo(entity);
                entity.Id = input.Id;
                return input;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("Update发生未知异常", $"{ex.Message};实体类：{input.ToJsonString()}");
                throw new UserFriendlyException("更新发生未知异常", ex);
            }
        }

        public virtual TDto Get(TDto input)
        {
            try
            {
                var type = typeof(TEntity);

                TPrimaryKeyType primaryKeyVal = default(TPrimaryKeyType);
                PropertyInfo primaryProperty = type.GetPrimaryKeyPropertyInfo();

                if (primaryProperty == null)
                {
                    throw new UserFriendlyException($"{typeof(TEntity).Name} has no Key defenition");
                }

                primaryKeyVal = input.Id;
                if (primaryKeyVal == null)
                {
                    return default(TDto);
                }

                string predicate = string.Format("{0} = @0 ", primaryProperty.Name);
                LambdaExpression lambdaExpression = DynamicExpressionParser.ParseLambda(
                    typeof(TEntity), typeof(bool),
                    predicate, primaryKeyVal);
                Expression<Func<TEntity, bool>> expression = (Expression<Func<TEntity, bool>>)lambdaExpression;
                TEntity resultEntity = BaseRepository.FirstOrDefault(expression);

                if (resultEntity == null)
                {
                    return default(TDto);
                }

                var resultDto = resultEntity.MapTo<TDto>();

                GetImagesByID(resultDto);
                GetFilesByID(resultDto);

                return resultDto;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("get请求发生意外错误，请稍后重试", ex);
                throw new UserFriendlyException("请求发生意外错误，请稍后重试", ex);
            }

        }

        public virtual List<TDto> GetAll()
        {
            try
            {
                var list = BaseRepository.GetAllList();
                return list.MapTo<List<TDto>>();

            }
            catch (Exception ex)
            {
                CLSLogger.Error("getAll请求发生意外错误，请稍后重试", ex);
                throw new UserFriendlyException("请求发生意外错误，请稍后重试", ex);
            }
        }

        protected IQueryable<TEntity> BuildQuery(PagedQueryInputBaseDTO<TSearchFilterDTO> inputDto)
        {
            try
            {
                string predicate = string.Empty;
                object[] parameters = null;

                if (inputDto.Filter != null)
                {
                    TEntity entity = inputDto.Filter.MapTo<TEntity>();
                    predicate = ReflectionUtilities.GenerateSQLPredicate<TEntity, TSearchFilterDTO>(entity, inputDto.Filter, out parameters);
                }

                var query = BaseRepository.GetAll();

                if (!string.IsNullOrEmpty(predicate) && !parameters.IsNullOrEmpty())
                {
                    query = query.Where(predicate, parameters);
                }

                return query;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("分页查询发生未知异常", ex);
                throw new UserFriendlyException("分页查询发生未知异常", ex);
            }
        }

        protected PagedQueryOutputBaseDTO<TDto> PaginateQuery(IQueryable<TEntity> query, PagedQueryInputBaseDTO<TSearchFilterDTO> inputDto)
        {
            try
            {
                PropertyInfo primaryKey = typeof(TEntity).GetPrimaryKeyPropertyInfo();
                if (primaryKey == null)
                {
                    throw new Exception($"{typeof(TEntity).Name} 实体未定义主键，请使用KeyAttribute定义");
                }

                string keyName = string.Empty;

                if (primaryKey != null)
                {
                    keyName = primaryKey.Name;
                    inputDto.Sort = inputDto.Sort.ChangeIfEqual("id", keyName);
                }

                PagedQueryOutputBaseDTO<TDto> output = new PagedQueryOutputBaseDTO<TDto>();
                output.Meta.PageIndex = inputDto.PageIndex;
                output.Meta.PageSize = inputDto.PageSize;

                //get upload template
                var uploadTemplateAttr = typeof(TDto).GetCustomAttribute<UploadTemplateAttribute>();
                if (uploadTemplateAttr != null && !string.IsNullOrEmpty(uploadTemplateAttr.TemplateName))
                {
                    output.Meta.UploadTemplate = new UploadTemplate
                    {
                        Label = uploadTemplateAttr.Label,
                        TemplateName = uploadTemplateAttr.TemplateName
                    };
                }

                int totalCount = query.Count();
                output.Meta.TotalCount = totalCount;
                if (!inputDto.Sort.IsNullOrEmpty() && !string.IsNullOrEmpty(inputDto.SortType))
                {
                    query = query.OrderBy(string.Format("{0} {1}", inputDto.Sort, inputDto.SortType));
                }
                else
                {
                    query = query.OrderBy($"{keyName} DESC");
                }

                var entities = query.PagedQuery(inputDto.PageIndex, inputDto.PageSize).ToList();
                if (!entities.IsNullOrEmpty())
                {
                    output.Data = entities.MapTo<List<TDto>>();
                    GetImagesForList(output.Data);
                }
                else
                {
                    output.Data = new List<TDto>();
                }
                return output;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("分页查询发生未知异常", ex);
                throw new UserFriendlyException("分页查询发生未知异常", ex);
            }
        }

        public virtual PagedQueryOutputBaseDTO<TDto> GetList(PagedQueryInputBaseDTO<TSearchFilterDTO> inputDto)
        {
            var query = BuildQuery(inputDto);
            var output = PaginateQuery(query, inputDto);
            return output;
        }

        public virtual ExcelImportOutputDTO ImportExcel(IFormFile file)
        {
            ExcelImportOutputDTO outputDTO = new ExcelImportOutputDTO();
            try
            {
                if (file.Length <= 0)
                {
                    throw new UserFriendlyException("文件大小异常，请选择正确的文件");
                }

                string filePath = Ultities.CreateDirectory("App_Data/ExcelImport");
                string fileName = $"{Guid.NewGuid()}{file.FileName}";
                string fullPath = Path.Combine(filePath, fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                var mapper = new Mapper(fullPath);
                var dtos = mapper.Take<TDto>().Select(x => x.Value).ToList();
                var entities = dtos.MapTo<List<TEntity>>();

                foreach (var item in entities)
                {
                    BaseRepository.Insert(item);
                }

                outputDTO.Message = $"成功导入{entities.Count}条数据";
                outputDTO.IsSuccess = true;
                return outputDTO;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("ImportExcel发生异常", ex);
                throw new UserFriendlyException("导入excel发生异常", ex);
            }
        }

        protected List<TDto> ConvertDTOFromExcel(IFormFile file)
        {
            try
            {
                if (file.Length <= 0)
                {
                    throw new UserFriendlyException("文件大小异常，请选择正确的文件");
                }

                string filePath = Ultities.CreateDirectory("App_Data/ExcelImport");
                string fileName = $"{Guid.NewGuid()}{file.FileName}";
                string fullPath = Path.Combine(filePath, fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                var mapper = new Mapper(fullPath);
                var dtos = mapper.Take<TDto>().Select(x => x.Value).ToList();
                File.Delete(fullPath);
                return dtos;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("ConvertDTOFromExcel发生异常", ex);
                return new List<TDto>();
            }
        }

        private string GetDefaultAttachmentType(Type type)
        {
            string stringType = type.Name.Replace("DTO", string.Empty);

            return stringType;
        }

        private string GetReferenceType(string propertyName, string referenceType)
        {
            if (string.IsNullOrEmpty(referenceType))
            {
                return propertyName.Replace("s", string.Empty);
            }
            return referenceType;
        }

        private void GetImagesByID(TDto input)
        {
            var key = input.Id;
            var properties = input.GetProperties();
            var inputType = typeof(TDto);

            List<ImageDTO> imagesDtos = null;

            foreach (var item in properties)
            {
                HasImageAttribute hasImageAttribute = null;
                if (!item.HasAttribute<HasImageAttribute>(out hasImageAttribute))
                {
                    continue;
                }

                string imageType = string.IsNullOrEmpty(hasImageAttribute.ImageType) ? GetDefaultAttachmentType(inputType) : hasImageAttribute.ImageType;
                string referenceType = GetReferenceType(item.Name, hasImageAttribute.ReferenceType);

                if (imagesDtos == null)
                {
                    imagesDtos = ImageRepository.GetAll()
                    .Where(x => x.ImageType == imageType && x.ReferenceId == key.ToString())
                    .ToList().MapTo<List<ImageDTO>>();
                }

                var currentImages = imagesDtos.Where(x => x.ReferenceType == referenceType).ToList();
                item.SetValue(input, currentImages);
            }

        }

        private void GetFilesByID(TDto input)
        {
            var key = input.Id;
            var properties = input.GetProperties();
            var inputType = typeof(TDto);

            List<FileDTO> fileDtos = null;

            foreach (var item in properties)
            {
                HasFileAttribute hasFileAttribute = null;
                if (!item.HasAttribute<HasFileAttribute>(out hasFileAttribute))
                {
                    continue;
                }

                string fileType = string.IsNullOrEmpty(hasFileAttribute.FileType) ? GetDefaultAttachmentType(inputType) : hasFileAttribute.FileType;
                string referenceType = GetReferenceType(item.Name, hasFileAttribute.ReferenceType);

                if (fileDtos == null)
                {
                    fileDtos = FileRepository.GetAll()
                    .Where(x => x.FileType == fileType && x.ReferenceId == key.ToString())
                    .ToList().MapTo<List<FileDTO>>();
                }

                var currentFiles = fileDtos.Where(x => x.ReferenceType == referenceType).ToList();
                item.SetValue(input, currentFiles);

            }

        }

        private void GetImagesForList(List<TDto> dtos)
        {
            if (dtos.IsNullOrEmpty())
            {
                return;
            }


            List<string> referenceTypes = new List<string>();
            var type = typeof(TDto);
            string imageType = GetDefaultAttachmentType(type);
            var properties = type.GetProperties();

            var ids = dtos.Select(x => x.Id.ToString()).ToList();
            var images = ImageRepository.GetAll().Where(x => ids.Contains(x.ReferenceId) && x.ImageType == imageType).ToList().MapTo<List<ImageDTO>>();

            foreach (var item in properties)
            {
                HasImageAttribute hasImageAttribute = null;
                if (!item.HasAttribute<HasImageAttribute>(out hasImageAttribute))
                {
                    continue;
                }

                string referenceType = GetReferenceType(item.Name, hasImageAttribute.ReferenceType);
                foreach (var dto in dtos)
                {
                    var currentImages = images.Where(x => x.ReferenceType == referenceType && x.ReferenceId == dto.Id.ToString()).ToList();
                    item.SetValue(dto, currentImages);
                }
            }

        }

        //private List<ImageDTO> UpdateImages(TDto input)
        //{
        //    var key = input.Id.ToString();
        //    var type = typeof(TDto);
        //    var properties = input.GetProperties();
        //    var imageType = type.Name.Replace("DTO", string.Empty);

        //    var oldImages = ImageRepository.GetAll().Where(x => x.ReferenceId == key &&
        //    x.ImageType == imageType).ToList();

        //    foreach (var item in properties)
        //    {
        //        HasImageAttribute hasImageAttribute = null;
        //        if (!item.HasAttribute<HasImageAttribute>(out hasImageAttribute))
        //        {
        //            continue;
        //        }





        //    }

        //    if (images.IsNullOrEmpty() && oldImages.IsNullOrEmpty())
        //    {
        //        return null;
        //    }

        //    //删除or更新旧的
        //    foreach (var item in oldImages)
        //    {
        //        var currentImg = images.FirstOrDefault(x => x.ImageId == item.ImageId);
        //        if (currentImg == null)
        //        {
        //            imageRepository.Delete(item.ImageId);
        //        }
        //        else
        //        {
        //            item.Sort = currentImg.Sort;
        //            item.OriginalPath = currentImg.OriginalPath;
        //        }
        //    }

        //    //新增的image(更新image的ReferenceId)
        //    List<long> imageIds = images.Where(x => x.ReferenceId.IsNullOrEmpty()).Select(x => x.ImageId).ToList();
        //    imageRepository.GetAll().Where(x => imageIds.Contains(x.ImageId)).ToList().ForEach(x =>
        //    {
        //        var currentImg = images.FirstOrDefault(y => y.ImageId == x.ImageId);
        //        x.ReferenceType = currentImg.ReferenceType;
        //        x.ReferenceId = shortCode;
        //        currentImg.ReferenceId = shortCode;

        //        x.ImageType = ImageTypeEnum.Brand.ToString();
        //        currentImg.ImageType = x.ImageType;

        //        x.CreationTime = DateTime.UtcNow;
        //        currentImg.CreationTime = x.CreationTime;
        //    });
        //    return images;
        //}


        ///// <summary>
        ///// Update Brand Files 
        ///// </summary>
        ///// <param name="Files"></param>
        ///// <param name="sku"></param>
        //private List<FileDTO> UpdateBrandFiles(List<FileDTO> Files, string shortCode)
        //{
        //    var oldFiles = fileRepository.GetAll().Where(x => x.ReferenceId == shortCode &&
        //    x.FileType == FileTypeEnum.Brand.ToString()).ToList();

        //    if (Files.IsNullOrEmpty() && oldFiles.IsNullOrEmpty())
        //    {
        //        return null;
        //    }



        //    //删除or更新旧的
        //    foreach (var item in oldFiles)
        //    {
        //        var currentImg = Files.FirstOrDefault(x => x.FileId == item.FileId);
        //        if (currentImg == null)
        //        {
        //            fileRepository.Delete(item.FileId);
        //        }
        //        else
        //        {
        //            item.Sort = currentImg.Sort;
        //            item.OriginalPath = currentImg.OriginalPath;
        //        }
        //    }

        //    //新增的File(更新File的ReferenceId)
        //    List<long> FileIds = Files.Where(x => x.ReferenceId.IsNullOrEmpty()).Select(x => x.FileId).ToList();
        //    fileRepository.GetAll().Where(x => FileIds.Contains(x.FileId)).ToList().ForEach(x =>
        //    {
        //        var currentImg = Files.FirstOrDefault(y => y.FileId == x.FileId);
        //        x.ReferenceType = currentImg.ReferenceType;
        //        x.ReferenceId = shortCode;
        //        currentImg.ReferenceId = shortCode;

        //        x.FileType = FileTypeEnum.Brand.ToString();
        //        currentImg.FileType = x.FileType;

        //        x.CreationTime = DateTime.UtcNow;
        //        currentImg.CreationTime = x.CreationTime;
        //    });
        //    return Files;
        //}


    }

}
