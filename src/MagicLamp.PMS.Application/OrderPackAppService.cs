﻿
using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.DTOs.Enums;
using Abp.UI;
using Newtonsoft.Json.Linq;
using MagicLamp.PMS.Infrastructure.Extensions;
using Cmq_SDK;
using Abp.BackgroundJobs;
using MagicLamp.XERP.BackgroundJob;
using MagicLamp.PMS.DTOs.ThirdPart;
using System.Linq.Dynamic.Core;
using System.Text.RegularExpressions;
using Abp.Extensions;
using MagicLamp.PMS.EntityFrameworkCore;
using Abp.Web.Models;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class OrderPackAppService : PMSCrudAppServiceBase<OrderPackEntity, OrderPackDTO, OrderPackDTO, int>
    {
        //货主ID: au, aladdin, he
        private List<string> customerAladdin = new List<string> { "au", "aladdin", "he" };
        private const string ClogTagType = "OrderPack";

        private IRepository<OrderPackEntity, int> orderPackRepository;
        private IRepository<ProductEntity, string> productRepository;
        private IRepository<OrderPackWMSQueueEntity, string> orderpackwmsqueueRepository;
        private PMSDbContext _pmsContext;
        private HEERPDbContext _heerpContext;


        private IRepository<OrderEntity, int> orderRepository;
        private IRepository<OrderPackDetailEntity, int> orderPackDetailRepository;
        private IBackgroundJobManager backgroundJobManager;
        private IRepository<ExportFlagEntity, int> exportFlagRepository;
        private IRepository<FluxDocOrderDetailEntity, string> fluxDocOrderDetailRepository;
        private IRepository<FluxDocOrderHeaderEntity, string> fluxDocOrderHeaderRepository;
        private IRepository<FluxInvLotEntity, string> fluxInvLotRepository;
        private IRepository<SeqEntity, int> seqRepository;
        private IRepository<APIStockEntity, string> apiStockRepository;
        private IRepository<OrderPackReservedHHSKUEntity, long> orderPackHHReservedRepository;
        private IRepository<PSeqStatusEntity, int> pSeqStatusRepository;
        private IRepository<FreightsEntity, string> freightsRepository;
        private IRepository<CacheOrderEntity, int> cacheOrderRepository;
        private IRepository<CQBondedOrderSKUInNumEntity, long> cqBondedOrderSKUInNumRepository;

        private SendNotificationToDingDingJob _dingdingNotificationJob;

        public OrderPackAppService(
            IRepository<OrderPackEntity, int> _orderPackRepository,
            IRepository<ProductEntity, string> _productRepository,
            IRepository<OrderPackWMSQueueEntity, string> _orderpackwmsqueueRepository,
            IRepository<OrderEntity, int> _orderRepository,
            IRepository<OrderDetailEntity, int> _orderDetailRepository,
            IBackgroundJobManager _backgroundJobManager,
            IRepository<OrderPackDetailEntity, int> _orderPackDetailRepository,
            IRepository<ExportFlagEntity, int> _exportFlagRepository,
            IRepository<FluxDocOrderDetailEntity, string> _fluxDocOrderDetailRepository,
            IRepository<FluxDocOrderHeaderEntity, string> _fluxDocOrderHeaderRepository,
            IRepository<FluxInvLotEntity, string> _fluxInvLotRepository,
            IRepository<SeqEntity, int> _seqRepository,
            IRepository<APIStockEntity, string> _apiStockRepository,
            IRepository<OrderPackReservedHHSKUEntity, long> _orderPackHHReservedRepository,
            IRepository<PSeqStatusEntity, int> _pSeqStatusRepository,
            IRepository<FreightsEntity, string> _freightsRepository,
            IRepository<CacheOrderEntity, int> _cacheOrderRepository,
            IRepository<CQBondedOrderSKUInNumEntity, long> _cqBondedOrderSKUInNumRepository
        ) : base(_orderPackRepository)
        {
            orderPackRepository = _orderPackRepository;
            orderRepository = _orderRepository;
            backgroundJobManager = _backgroundJobManager;
            orderPackDetailRepository = _orderPackDetailRepository;
            exportFlagRepository = _exportFlagRepository;
            fluxDocOrderDetailRepository = _fluxDocOrderDetailRepository;
            fluxDocOrderHeaderRepository = _fluxDocOrderHeaderRepository;
            fluxInvLotRepository = _fluxInvLotRepository;
            seqRepository = _seqRepository;
            apiStockRepository = _apiStockRepository;
            orderPackHHReservedRepository = _orderPackHHReservedRepository;
            pSeqStatusRepository = _pSeqStatusRepository;
            freightsRepository = _freightsRepository;
            productRepository = _productRepository;

            orderpackwmsqueueRepository = _orderpackwmsqueueRepository;

            cacheOrderRepository = _cacheOrderRepository;
            cqBondedOrderSKUInNumRepository = _cqBondedOrderSKUInNumRepository;
            _pmsContext = new PMSDbContextFactory().CreateDbContext();
            _heerpContext = new HEERPDbContextFactory().CreateDbContext();

            _dingdingNotificationJob = new SendNotificationToDingDingJob();
        }


        /// <summary>
        /// Get the status of the pack.（原：状态）
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        protected string GetOrderPackStatusName(OrderPackDTO dto)
        {
            switch (dto.status)
            {
                case 0:
                    return "未配";
                case 1:
                    return "已配";
                case 2:
                    return "缺货";
                case 3:
                    return "已包";
                case 4:
                    return "已出库";
                case 40:
                    return "分配";
                default:
                    return "未知状态";
            }
        }

        /// <summary>
        /// Get the freeze flag of the OrderPackDTO.（原：冻结状态）
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        protected string GetOrderPackFreezeFlagName(OrderPackDTO dto)
        {
            switch (dto.freezeFlag)
            {
                case 0:
                    return "未冻结";
                case 1:
                    return "作废";
                case 2:
                    return "整单退款";
                case 3:
                    return "缺货退款";
                case 4:
                    return "换货退款";
                case 5:
                    return "换货补款";
                case 6:
                    return "无差价换货";
                case 7:
                    return "信息待确认";
                case 8:
                    return "改标志";
                case 9:
                    return "其他";
                default:
                    return "未知状态";
            }
        }

        protected string GetOrderPackStatusName(OrderPackDetailDTO dto)
        {
            switch (dto.status)
            {
                case 0:
                    return "未配";
                case 1:
                    return "已配";
                case 2:
                    return "缺货";
                case 3:
                    return "已包";
                case 4:
                    return "已出库";
                default:
                    return "未知状态";
            }
        }

        protected string GetOrderPackDetailStatusName(OrderPackDetailDTO dto)
        {
            switch (dto.status)
            {
                case 0:
                    return "未配";
                case 1:
                    return "已配";
                case 2:
                    return "缺货";
                case 3:
                    return "部分";
                default:
                    return "未知状态";
            }
        }
        protected string GetFluxWarehouseStatus(OrderPackDetailDTO dto)
        {
            switch (dto.WarehouseStatus)
            {
                case "00":
                    return "创建订单";
                case "10":
                    return "部分预配";
                case "20":
                    return "预配完成";
                case "30":
                    return "部分分配";
                case "40":
                    return "分配完成";
                case "50":
                    return "部分拣货";
                case "60":
                    return "拣货完成";
                case "61":
                    return "播种完成";
                case "62":
                    return "部分装箱";
                case "63":
                    return "完全装箱";
                case "65":
                    return "部分装车";
                case "66":
                    return "装车完成";
                case "70":
                    return "部分发运";
                case "80":
                    return "完全发运";
                case "90":
                    return "订单取消";
                case "98":
                    return "等待释放";
                case "99":
                    return "订单完成";
                default:
                    return "未知状态";
            }
        }

        protected string GetOrderStatus(OrderDTO dto)
        {
            switch (dto.status)
            {
                case 0:
                    return "待分单";
                case 1:
                    return "已分单";
                case 2:
                    return "待配货";
                case 3:
                    return "缺货";
                case 4:
                    return "已配货";
                default:
                    return "未知状态";
            }
        }
        /// <summary>
        /// Get export flag of the pack.（原column：标志）
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        protected string GetOrderPackExportFlagName(OrderPackDTO dto)
        {
            var exportFlags = exportFlagRepository.GetAllList();
            var entity = exportFlags.FirstOrDefault(x => x.Flag == dto.exportFlag);
            if (entity == null)
            {
                return string.Empty;
            }
            return entity.Name;
        }

        // TODO: optimize so these are not queried once each iteration in the loop
        protected void SetOrderPackPropertyName(OrderPackDTO item)
        {
            item.warehouseStatus = GetOrderPackStatusName(item);
            item.freezeFlagName = GetOrderPackFreezeFlagName(item);
            item.exportFlagName = GetOrderPackExportFlagName(item);
        }

        private IQueryable<OrderInfoDTO> GetQueryable(PagedQueryInputBaseDTO<OrderPackDTO> inputDto)
        {
            IQueryable<OrderInfoDTO> query = null;

            if (inputDto.Filter == null)
            {
                inputDto.Filter = new OrderPackDTO();
            }

            if (!inputDto.Filter.sku.IsNullOrWhiteSpace())
            {
                query = from orderPack in orderPackRepository.GetAll()

                        join order in orderRepository.GetAll()
                        on orderPack.orderID equals order.orderid

                        join orderPackDetail in orderPackDetailRepository.GetAll()
                        on orderPack.opID equals orderPackDetail.opid

                        where orderPackDetail.sku == inputDto.Filter.sku

                        join pSeqStatus in pSeqStatusRepository.GetAll()
                        on orderPack.pseq equals pSeqStatus.pSeq into ps
                        from pResult in ps.DefaultIfEmpty()

                        select new OrderInfoDTO
                        {
                            orderPack = orderPack,
                            isExport = pResult == null ? false : pResult.isExport,
                            orderCreateTime = order.orderCreateTime
                        };

            }
            else
            {
                query = from orderPack in orderPackRepository.GetAll()

                        join order in orderRepository.GetAll()
                        on orderPack.orderID equals order.orderid

                        join pSeqStatus in pSeqStatusRepository.GetAll()
                        on orderPack.pseq equals pSeqStatus.pSeq into ps
                        from pResult in ps.DefaultIfEmpty()

                        select new OrderInfoDTO
                        {
                            orderPack = orderPack,
                            isExport = pResult == null ? false : pResult.isExport,
                            orderCreateTime = order.orderCreateTime
                        };
            }

            if (!string.IsNullOrWhiteSpace(inputDto.Filter.trackingNo))
            {
                //订单号or包裹号or物流号
                var trackingNo = inputDto.Filter.trackingNo.SplitByCommonSymbol();
                query = query.Where(x => trackingNo.Contains(x.orderPack.orderID) || trackingNo.Contains(x.orderPack.orderPackNo) || trackingNo.Contains(x.orderPack.carrierId));
            }
            if (!inputDto.Filter.exportFlags.IsNullOrEmpty())
            {
                query = query.Where(x => inputDto.Filter.exportFlags.Contains(x.orderPack.exportFlag.Value));
            }
            if (inputDto.Filter.ordercreate_time_begin.HasValue)
            {
                query = query.Where(x => x.orderCreateTime >= inputDto.Filter.ordercreate_time_begin);
            }
            if (inputDto.Filter.ordercreate_time_end.HasValue)
            {
                query = query.Where(x => x.orderCreateTime <= inputDto.Filter.ordercreate_time_end);
            }
            if (inputDto.Filter.out_time_begin.HasValue)
            {
                query = query.Where(x => x.orderPack.out_time >= inputDto.Filter.out_time_begin);
            }
            if (inputDto.Filter.out_time_end.HasValue)
            {
                query = query.Where(x => x.orderPack.out_time <= inputDto.Filter.out_time_end);
            }
            if (!inputDto.Filter.listStatus.IsNullOrEmpty())
            {
                query = query.Where(x => inputDto.Filter.listStatus.Contains(x.orderPack.status.Value));
            }
            if (!inputDto.Filter.freezeFlags.IsNullOrEmpty())
            {
                query = query.Where(x => inputDto.Filter.freezeFlags.Contains(x.orderPack.freezeFlag.Value));
            }
            if (inputDto.Filter.isExport.HasValue)
            {
                query = query.Where(x => x.isExport == inputDto.Filter.isExport);
            }
            if (!inputDto.Sort.IsNullOrEmpty() && !inputDto.SortType.IsNullOrEmpty())
            {
                switch (inputDto.Sort)
                {
                    case "id":
                        query = query.OrderBy(string.Format("{0} {1}", "orderPack.opID", inputDto.SortType));
                        break;
                    case "orderCreateTime":
                        query = query.OrderBy(string.Format("{0} {1}", "orderCreateTime", inputDto.SortType));
                        break;
                    case "out_time":
                        query = query.OrderBy(string.Format("{0} {1}", "orderPack.out_time", inputDto.SortType));
                        break;
                }
            }
            else
            {
                query = query.OrderByDescending(x => x.orderPack.opID);
            }
            return query;
        }
        private PagedQueryOutputBaseDTO<OrderPackDTO> GetListForExportExcel(PagedQueryInputBaseDTO<OrderPackDTO> inputDto, int limit)
        {
            PagedQueryOutputBaseDTO<OrderPackDTO> result = new PagedQueryOutputBaseDTO<OrderPackDTO>();

            var query = GetQueryable(inputDto);

            result.Meta.TotalCount = query.Count();
            if (result.Meta.TotalCount > limit)
            {
                throw new RequestValidationException($"结果条数大于{limit}，请缩小查询范围。");
            }

            result.Meta.PageIndex = inputDto.PageIndex;
            result.Meta.PageSize = inputDto.PageSize;

            var entities = query.PagedQuery(inputDto.PageIndex, inputDto.PageSize).ToList();

            result.Data = entities.Select(x =>
            {
                OrderPackDTO orderPack = x.orderPack.MapTo<OrderPackDTO>();
                orderPack.orderCreateTime = x.orderCreateTime;
                orderPack.isExport = x.isExport;
                SetOrderPackPropertyName(orderPack);
                return orderPack;
            }).ToList();

            return result;
        }


        private string MappingFreightID(string originalFreightID)
        {
            if (string.IsNullOrEmpty(originalFreightID))
            {
                return originalFreightID;
            }

            foreach (var item in PMSConsts.FREIGHT_MAPPING)
            {
                if (item.Value.Contains(originalFreightID))
                {
                    return item.Key;
                }
            }

            return originalFreightID;
        }

        private string MappingFreightName(string originalFreightName)
        {
            if (string.IsNullOrEmpty(originalFreightName))
            {
                return originalFreightName;
            }

            foreach (var item in PMSConsts.FREIGHTName_MAPPING)
            {
                if (item.Value.Contains(originalFreightName))
                {
                    return item.Key;
                }
            }

            return originalFreightName;
        }

        //get a list of order pack according to the parameter
        public override PagedQueryOutputBaseDTO<OrderPackDTO> GetList(PagedQueryInputBaseDTO<OrderPackDTO> inputDto)
        {
            PagedQueryOutputBaseDTO<OrderPackDTO> result = new PagedQueryOutputBaseDTO<OrderPackDTO>();

            var query = GetQueryable(inputDto);

            result.Meta.TotalCount = query.Count();
            result.Meta.PageIndex = inputDto.PageIndex;
            result.Meta.PageSize = inputDto.PageSize;

            var entities = query.PagedQuery(inputDto.PageIndex, inputDto.PageSize).ToList();

            result.Data = entities.Select(x =>
            {
                OrderPackDTO orderPack = x.orderPack.MapTo<OrderPackDTO>();
                orderPack.orderCreateTime = x.orderCreateTime;
                orderPack.isExport = x.isExport;
                SetOrderPackPropertyName(orderPack);
                return orderPack;
            }).ToList();

            return result;
        }

        /// <summary>
        /// Return a list of OrderPackDetail.
        /// </summary>
        /// <param name="inputDto"></param>
        /// <returns></returns>
        public List<OrderPackDetailDTO> GetDetails(OrderPackDTO inputDto)
        {
            var fluxOrderHeader = fluxDocOrderHeaderRepository.GetAll().Where(x => x.OrderPackNo == inputDto.orderPackNo);
            var orderPackDetails = orderPackDetailRepository.GetAll().Where(x => x.opid == inputDto.opId).ToList();

            return null;

        }


        /// <summary>
        /// Batch get order pack info
        /// </summary>
        /// <param name="inputDto"></param>
        /// <returns></returns>
        [HttpGet]
        public List<BatchGetOrderPackInfoOutDTO> BatchGetOrderPackInfo(BatchGetOrderPackInfoInputDTO inputDto)
        {
            if (inputDto.OrderIDs.IsNullOrEmpty() && inputDto.OrderPackNos.IsNullOrEmpty())
            {
                throw new RequestValidationException("Should passing OrderPackNos or OrderIDs");
            }

            List<BatchGetOrderPackInfoOutDTO> orderPackInfos = new List<BatchGetOrderPackInfoOutDTO>();

            //get opid from opid in inputDto
            var orderPacks = orderPackRepository.GetAll().Where(x => inputDto.OrderPackNos.Contains(x.orderPackNo) || inputDto.OrderIDs.Contains(x.orderID)).ToList();

            if (orderPacks.IsNullOrEmpty())
            {
                throw new RequestValidationException($"失败。失败原因：找不到包裹号。orderPackNo：{inputDto.OrderPackNos}");
            }

            List<string> orderPackNos = orderPacks.Select(x => x.orderPackNo).ToList();
            var opIDs = orderPacks.Select(y => y.opID).ToList();

            //query order pack info
            var orderPackDetailEntities = orderPackDetailRepository.GetAll().Where(x => opIDs.Contains(x.opid.Value)).ToList();

            if (orderPackDetailEntities.IsNullOrEmpty())
            {
                var listOrderPacks = orderPacks.GroupBy(x => x.opID).Select(x => new BatchGetOrderPackInfoOutDTO
                {
                    OrderPackNo = orderPacks.FirstOrDefault(y => y.opID == x.Key).orderPackNo,
                    OrderPackDetails = null
                });

                var returnOrderPacks = listOrderPacks.ToList();

                orderPackInfos = returnOrderPacks;
                return orderPackInfos;
            }

            var skus = orderPackDetailEntities.Select(x => x.sku).ToList();

            //match the sku detail for erp detail and flux detail
            var erpOrderPacks = orderPackDetailEntities.GroupBy(x => x.opid).Select(x => new BatchGetOrderPackInfoOutDTO
            {
                //OrderPackNo = x.Key,
                OrderPackNo = orderPacks.FirstOrDefault(y => y.opID == x.Key).orderPackNo,
                OrderPackDetails = x.ToList().MapTo<List<OrderPackDetailDTO>>()
            });


            //get erp api stock
            var apiStocks = apiStockRepository.GetAll().Where(x => skus.Contains(x.SKU)).ToList();

            if (apiStocks.IsNullOrEmpty())
            {
                throw new RequestValidationException($"失败。失败原因：找不到apistocks。skus：{skus}");
            }

            var listErpOrderPacks = erpOrderPacks.ToList();

            for (int i = 0; i < listErpOrderPacks.Count(); i++)
            {
                for (int j = 0; j < listErpOrderPacks[i].OrderPackDetails.Count(); j++)
                {
                    var orderPackNo = listErpOrderPacks[i].OrderPackNo;
                    if (orderPackNo.IsNullOrEmpty())
                    {
                        throw new RequestValidationException($"失败。失败原因：找不到包裹号。orderPackNo：{listErpOrderPacks[i].OrderPackNo}");
                    }

                    listErpOrderPacks[i].OrderPackDetails[j].QtyAllocated = listErpOrderPacks[i].OrderPackDetails[j].qty;

                    var sku = listErpOrderPacks[i].OrderPackDetails[j].sku;
                    int stock = 0;


                    var apiStockValue = apiStocks.FirstOrDefault(x => x.SKU == sku).Qty_available;
                    stock = apiStockValue;
                    //货主，在flux 对应 LotAtt06
                    listErpOrderPacks[i].OrderPackDetails[j].Stock = stock;

                    //仓库状态
                    listErpOrderPacks[i].OrderPackDetails[j].statusName = GetOrderPackDetailStatusName(listErpOrderPacks[i].OrderPackDetails[j]);
                }
            }

            orderPackInfos = listErpOrderPacks;
            return orderPackInfos;
        }

        //TODO:出库确认接口，pending: handle GFC case and create gfc background job
        /// <summary>
        /// 
        /// 
        /// 出库确认
        /// </summary>
        /// <param name="method">Flux接口方法的名称</param>
        /// <param name="data">Flux参数</param>
        /// <returns></returns>
        public string OrderPackOutboundConfirm([FromForm] string method, [FromForm] string data)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>();
            clogTags["type"] = "orderpack_OrderPackOutboundConfirm";
            //Flux处理结果说明
            FluxResponseDTO fluxResponse = new FluxResponseDTO();
            //保存状态
            //出库并通知各个平台已出库
            //修改包裹锁定状态

            //handle parameter:method
            if (!method.Equals("confirmSOData"))
            {
                throw new UserFriendlyException("Wrong method name."); //--http status code 500
            }

            JObject fluxData = JObject.Parse(data);
            var orderInfo = fluxData["xmldata"]["data"]["orderinfo"].Children();
            var orderInfos = orderInfo.ToJsonString().ConvertFromJsonString<List<FluxOrderInfoDTO>>();

            foreach (var fluxOrderInfo in orderInfos)
            {
                FluxOrderInfoMqDto mqDto = new FluxOrderInfoMqDto
                {
                    OrderNo = fluxOrderInfo.OrderNo,
                    Weight = fluxOrderInfo.Weight,
                    Udf1 = fluxOrderInfo.Udf1
                };

                GFCDataMqDto gfcData = fluxOrderInfo.MapTo<GFCDataMqDto>();

                clogTags["orderpackno"] = mqDto.OrderNo;

                //aladdin
                if (customerAladdin.Contains(fluxOrderInfo.CustomerID.ToLower()))
                {
                    backgroundJobManager.Enqueue<FluxOrderPackOutboundGenerateJob, FluxOrderInfoMqDto>(mqDto);
                    fluxResponse.returnFlag = "1";
                    fluxResponse.returnDesc = "Background Job: FluxOrderPackOutboundGenerateJob Finished";
                    return fluxResponse.ToJsonString();

                }
                //GFC菜鸟
                else
                {
                    gfcData.Action = GFCBackgroundJobActionEnum.Post;
                    backgroundJobManager.Enqueue<GFCBackgroundJob, GFCDataMqDto>(gfcData);
                    fluxResponse.returnFlag = "1";
                    //TODO: refer to post.Post(data.data.ToString(), "api/out/confirm")ERPFIlter in FLUXAPI
                    fluxResponse.resultInfo = "";
                }
            }
            fluxResponse.returnFlag = "1";
            return fluxResponse.ToJsonString();
        }

        //导出
        //5000, 大于5000请缩小查询范围
        [HttpGet]
        public FileContentResult ExportToExcel(PagedQueryInputBaseDTO<OrderPackDTO> dto)
        {
            try
            {
                //maximun 5000 result
                dto.PageSize = 5000;
                var result = GetListForExportExcel(dto, dto.PageSize);

                var listOrderPackDTO = result.Data;

                //get store from order table
                var listOrderId = listOrderPackDTO.Select(x => x.orderId).ToList();


                //get list of order
                var listOrder = orderRepository.GetAll().Where(x => listOrderId.Contains(x.orderid)).ToList();

                if (listOrder.IsNullOrEmpty())
                {
                    throw new RequestValidationException($"失败。失败原因：找不到相关订单。");
                }
                var seqs = listOrder.Select(x => x.seq).ToList();
                //get 上传时间 from seq
                var listSeq = seqRepository.GetAll().Where(x => seqs.Contains(x.seq));


                //get orderpackdetail from opid
                var listOpid = listOrderPackDTO.Select(x => x.opId).ToList();
                var listOrderPackDetail = orderPackDetailRepository.GetAll().Where(x => listOpid.Contains(x.opid)).ToList();

                List<ExcelOrderPackDetailDTO> listDto = new List<ExcelOrderPackDetailDTO>();

                foreach (var item in listOrderPackDetail)
                {
                    //get orderPackInfo
                    var orderPackInfo = listOrderPackDTO.FirstOrDefault(x => x.opId == item.opid);

                    //get orderInfo
                    var orderInfo = listOrder.FirstOrDefault(x => x.orderid == orderPackInfo.orderId);

                    //get seqInfo
                    var seqInfo = listSeq.FirstOrDefault(x => x.seq == orderInfo.seq);

                    ExcelOrderPackDetailDTO entry = new ExcelOrderPackDetailDTO
                    {
                        store = orderInfo == null ? "" : orderInfo.store,
                        orderPackNo = orderPackInfo == null ? "" : orderPackInfo.orderPackNo,
                        oidPackNo = orderPackInfo == null ? "" : orderPackInfo.oIdPackNo,
                        sku = item.sku,
                        productName = item.product_name,
                        qty = item.qty,
                        name = orderPackInfo == null ? "" : orderPackInfo.rec_name,
                        address = orderPackInfo == null ? "" : orderPackInfo.rec_addr,
                        phone = orderPackInfo == null ? "" : orderPackInfo.rec_phone,
                        idNo = orderPackInfo == null ? "" : orderPackInfo.idNo,
                        carrierId = orderPackInfo == null ? "" : orderPackInfo.carrierId,
                        weightValue = orderPackInfo == null ? 0 : orderPackInfo.weightVal,
                        packWeight = orderPackInfo == null ? 0 : orderPackInfo.packWeight,
                        uploadTime = seqInfo == null ? "" : seqInfo.up_time?.SpecifyKindInUTC().ToString(),
                        platform = seqInfo == null ? "" : ((BusinessEnum)seqInfo.bizID).ToString(),
                        pseq = orderPackInfo == null ? 0 : orderPackInfo.pseq
                    };
                    listDto.Add(entry);
                }

                var mapper = new Npoi.Mapper.Mapper();
                mapper.Put(listDto, "newSheet");

                MemoryStream stream = new MemoryStream();
                mapper.Workbook.Write(stream);
                var bytes = stream.ToArray();
                FileContentResult output = new FileContentResult(bytes, "application/vnd.ms-excel");
                output.FileDownloadName = $"{DateTime.UtcNow.ToString()}.xls";
                return output;
            }
            catch (Exception ex)
            {
                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = ClogTagType;
                tags["method"] = "ExportToExcel";
                CLSLogger.Error("导出Excel发生异常", ex.Message, tags);
                throw new RequestValidationException($"导出Excel发生异常，失败原因：{ex.Message}");
            }

        }

        [HttpPost]
        public OutboundNotifyHHOutDTO OutBoundNotifyHH(List<OutboundNotifyHHInputDTO> inputDto)
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "OutBoundNotifyHH";
            tags["trackid"] = Guid.NewGuid().ToString();

            string reqJson = inputDto.ToJsonString();
            CLSLogger.Info("OutBoundNotifyHH请求报文", reqJson, tags);

            OutboundNotifyHHOutDTO outPutDto = new OutboundNotifyHHOutDTO
            {
                Success = false
            };

            if (inputDto.IsNullOrEmpty())
            {
                outPutDto.Error = "Invalid Request";
                return outPutDto;
            }

            inputDto.ForEach(x =>
            {
                x.OutBoundTime = DateTime.UtcNow;
            });

            backgroundJobManager.Enqueue<OrderPackOutboundNotifyHHJob, List<OutboundNotifyHHInputDTO>>(inputDto);

            outPutDto.Success = true;
            return outPutDto;

        }

        public bool CancelHHOrder(HHPushOrderInputDTO inputDto)
        {
            backgroundJobManager.Enqueue<CancelHHOrderJob, HHPushOrderInputDTO>(inputDto);
            return true;
        }

        /// <summary>
        /// 通过上传Excel更新相关包裹的物流号
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public ExcelImportOutputDTO ImportExcelToUploadCarrierInfo(IFormFile file, string freightId)
        {
            ExcelImportOutputDTO result = new ExcelImportOutputDTO();

            if (freightId.IsNullOrEmpty())
            {
                throw new RequestValidationException("请选择物流");
            }

            var dtos = ConvertDTOFromExcel(file);
            dtos = dtos.Where(x => !x.orderPackNo.IsNullOrEmpty()).ToList();

            if (dtos.IsNullOrEmpty())
            {
                throw new RequestValidationException("上传excel中未找到有效数据，请检查");
            }

            StringBuilder error = new StringBuilder();

            int index = 2;
            foreach (var item in dtos)
            {
                if (item.carrierId.IsNullOrEmpty())
                {
                    error.Append($"第{index}行信息有误，请检查运单号是否填写正确；\n\r");
                }
                index++;
            }

            if (error.Length > 0)
            {
                throw new RequestValidationException(error.ToString());
            }

            var listOrderPackNo = dtos.Select(x => x.orderPackNo).Distinct().ToList();
            var listOrderPackInfo = orderPackRepository.GetAll().Where(x => listOrderPackNo.Contains(x.orderPackNo)).ToList();

            var listOrderId = dtos.Select(x => x.orderId).ToList();
            var listOrderInfo = orderRepository.GetAll().Where(x => listOrderId.Contains(x.orderid)).ToList();

            if (listOrderInfo.IsNullOrEmpty())
            {
                throw new RequestValidationException($"失败。失败原因：找不到相关订单。");
            }

            var seqs = listOrderInfo.Select(x => x.seq).ToList();
            var listSeq = seqRepository.GetAll().Where(x => seqs.Contains(x.seq));

            var listFreights = freightsRepository.GetAll();

            int count = 0;
            foreach (var item in dtos)
            {
                var orderPackInfo = listOrderPackInfo.FirstOrDefault(x => x.orderPackNo == item.orderPackNo);
                if (orderPackInfo != null)
                {
                    orderPackInfo.carrierId = item.carrierId;
                    orderPackInfo.freightId = freightId;
                    count++;
                }

                //send to CMQ
                var orderInfo = listOrderInfo.FirstOrDefault(x => x.orderid == orderPackInfo.orderID);
                //get seqInfo
                var seqInfo = listSeq.FirstOrDefault(x => x.seq == orderInfo.seq);

                OrderPackStatusMqDto orderPackStatusMqDto = new OrderPackStatusMqDto
                {
                    OrderID = orderPackInfo.orderID,
                    OrderPackNo = orderPackInfo.orderPackNo,
                    OrderCreateDate = orderInfo.orderCreateTime.Value.SpecifyKindInUTC(),//The date orginally from clients in UTC format
                    OrderDeliveryDate = orderInfo.sendtime.HasValue ? DateTime.MinValue : orderInfo.sendtime.Value.SpecifyKindInLocal(),
                    ParcelPickDate = orderPackInfo.pick_time.HasValue ? DateTime.MinValue : orderPackInfo.pick_time.Value.SpecifyKindInLocal(),
                    ParcelPackDate = orderPackInfo.pack_time.HasValue ? DateTime.MinValue : orderPackInfo.pack_time.Value.SpecifyKindInLocal()
                };

                var orderDto = orderInfo.MapTo<OrderDTO>();
                var orderPackDto = orderPackInfo.MapTo<OrderPackDTO>();

                if (orderDto.status != null)
                {
                    var statusValue = orderInfo.status.Value;
                    orderPackStatusMqDto.OrderStatus = statusValue.ToString();
                    orderPackStatusMqDto.OrderStatusDesc = GetOrderStatus(orderDto);
                }

                if (orderPackDto.status != null)
                {
                    var statusValue = orderPackDto.status.Value;
                    orderPackStatusMqDto.ParcelStatus = statusValue.ToString();
                    orderPackStatusMqDto.ParcelStatusDesc = GetOrderPackStatusName(orderPackDto);
                }

                var freightsInfo = listFreights.FirstOrDefault(x => x.id == orderPackInfo.freightId);

                if (freightsInfo != null)
                {
                    var placeHolder = TCConstants.CMQTopics.TOPIC_ORDERNOTIFICATION;
                    orderPackStatusMqDto.CourierCode = MappingFreightID(freightsInfo.id);
                    orderPackStatusMqDto.CourierName = MappingFreightName(freightsInfo.name);
                }

                List<string> msgTags = new List<string>();
                msgTags.Add("shipment");
                orderPackStatusMqDto.ToJsonString();

            }

            result.IsSuccess = true;
            result.Message = $"成功导入{count}条数据";

            return result;

        }

        /// <summary>
        /// 丝路根据订单号回传出库信息(快递单号，出库时间)
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public SiLuOutboundOutputDTO SiLuPushOutboundTime(SiLuOutboundInputDTO dto)
        {
            SiLuOutboundOutputDTO result = new SiLuOutboundOutputDTO();

            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["method"] = "SiLuPushOutboundTime",
                ["orderid"] = dto.OrderId,
                ["type"] = ClogTagType
            };

            StringBuilder log = new StringBuilder();

            if (dto == null)
            {
                throw new RequestValidationException("失败。失败原因：input不能为空");
            }

            CLSLogger.Info("丝路回传出库信息请求报文", dto.ToJsonString(), clogTags);

            try
            {
                if (dto.OrderId.IsNullOrEmpty() || dto.ExpressNo.IsNullOrEmpty() || dto.OutTime == null)
                {
                    string message = "失败。失败原因：OrderId，ExpressNo，OutTime皆不能为空。";
                    CLSLogger.Error("丝路同步包裹出库失败", message, clogTags);
                    result.Message = message;
                    result.Success = false;
                    return result;
                }

                bool isOrderPackNo = Regex.Match(dto.OrderId, "^[0-9]+-[0-9]+-[0-9]+", RegexOptions.IgnoreCase).Success;
                var listOrderPackInfo = orderPackRepository.GetAll().Where(x => isOrderPackNo ? x.orderPackNo == dto.OrderId : x.orderID == dto.OrderId).ToList();

                if (listOrderPackInfo.IsNullOrEmpty())
                {
                    var cacheOrder = cacheOrderRepository.FirstOrDefault(x => x.orderid == dto.OrderId);
                    var order = orderRepository.FirstOrDefault(x => x.orderid == dto.OrderId);

                    if (cacheOrder == null && order == null)
                    {
                        string message = "订单已取消，无需回传出库。";
                        CLSLogger.Info("丝路同步包裹出库终止", message, clogTags);
                        result.Message = message;
                        result.Success = true;
                        return result;
                    }
                    else
                    {
                        string message = "失败。失败原因：OrderId无匹配包裹。";
                        CLSLogger.Error("丝路同步包裹出库失败", message, clogTags);
                        result.Message = message;
                        result.Success = false;
                        return result;
                    }
                }

                foreach (var item in listOrderPackInfo)
                {
                    item.bonded_warehouse_carrier_id = dto.ExpressNo;
                    item.bonded_warehouse_carrier_company = dto.ExpressComp;
                    //丝路出库时间为中国时间
                    item.out_time = dto.OutTime.ConvertCNTimeToNZTime();
                    item.status = (int)OrderPackStatusEnum.Outbound;

                    if (item.freightId.IsNullOrEmpty())
                    {
                        item.carrierId = dto.ExpressNo;
                        switch (dto.ExpressComp)
                        {
                            case "yuantong":
                                item.freightId = "yto";
                                log.Append("原freightid为空，更新为yto。");
                                break;
                            case "shentong":
                                item.freightId = "ShengTong";
                                log.Append("原freightid为空，更新为ShengTong。");
                                break;
                        }
                    }

                    string message = $"更新包裹号{item.orderPackNo}状态为已出库，出库时间：{dto.OutTime}，快递单号：{dto.ExpressNo}。";
                    log.Append(message);

                    // 保税仓出库，通知Exinc发货单发货; SILU
                    //EcinxDeliverySendOut(item);
                }

                string resultMessage = $"更新订单号：{dto.OrderId} 下所有包裹为已出库。";
                log.Insert(0, resultMessage);
                CLSLogger.Info("丝路同步包裹出库成功", log.ToString(), clogTags);

                RecordGMSKUDetail(dto);

                result.Message = log.ToString();
                result.Success = true;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("丝路同步包裹出库发生异常", ex, clogTags);

                log.Append(ex);
                result.Message = log.ToString();
                result.Success = false;
                return result;
            }

            return result;
        }







        private void RecordGMSKUDetail(SiLuOutboundInputDTO dto)
        {
            foreach (var item in dto.Items)
            {
                CQBondedOrderSKUInNumEntity entryToBeAdded = new CQBondedOrderSKUInNumEntity
                {
                    OrderID = dto.OrderId,
                    GMSKU = item.SkuNum,
                    Qty = item.Qty,
                    InNum = item.InNum,
                    CreateTime = DateTime.UtcNow
                };
                cqBondedOrderSKUInNumRepository.Insert(entryToBeAdded);
            }
        }

        public void SyncOrderPackInQueueFromHH(HHCreateOrderResultDTO input)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["method"] = "SyncOrderPackInQueueFromHH",
                ["orderpackno"] = input.OrderPackNo,
                ["type"] = ClogTagType
            };

            CLSLogger.Info("HH回传订单状态请求报文", input.ToJsonString(), clogTags);

            if (input == null || input.OrderPackNo.IsNullOrEmpty())
            {
                throw new RequestValidationException("失败。失败原因：input及input.OrderPackNo不能为空");
            }

            // 如果缺少对应关系，T+1补推送;
            // 1) 解析 没有找到ZNZATGK476540与之对应的SKU
            // 2) update ta set status='Created',ReturnMessageFromFlux=null,ReturnMessageFromHH=null from OrderPacks_WMSQueue ta  where   orderpackno in ('3915200000016-2-1') 更新
            // 一直发通知To @Kimi；
            if (input.message.Contains("没有找到")) //解析 没有找到ZNZATGK476540与之对应的SKU
            {
                _dingdingNotificationJob.SendProductMappingAlert("魔法灯SKU与HH SKU 无对应关系", $"锁定HH库存失败。订单为:{input.OrderPackNo}。失败原因：{input.message}");

                var orderpackExists = orderpackwmsqueueRepository.GetAll().Where(x => x.OrderPackNo == input.OrderPackNo).ToList();
                if (orderpackExists.IsNullOrEmpty())
                {
                    throw new RequestValidationException($"失败。失败原因：Queue未找到该包裹,订单号:{input.OrderPackNo}");
                }
                else
                {
                    // Try Push order again;
                    OrderPackWMSQueueEntity orderpackwmsqueue;
                    orderpackwmsqueue = orderpackExists.FirstOrDefault();
                    orderpackwmsqueue.Status = "Created";
                    orderpackwmsqueue.ReturnMessageFromFlux = "";
                    orderpackwmsqueue.ReturnMessageFromHH = "";
                    orderpackwmsqueueRepository.Update(orderpackwmsqueue);
                }
                return;
            }
            else //正常情况处理Flux的更新；
            {
                backgroundJobManager.Enqueue<SyncCreateOrderResultFromHH, HHCreateOrderResultDTO>(input, BackgroundJobPriority.High);
            }

        }
    }
}