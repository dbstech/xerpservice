﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class ExportFlagAppService : PMSCrudAppServiceBase<ExportFlagEntity, ExportFlagDTO, ExportFlagDTO, int>
    {

        private IRepository<ExportFlagEntity, int> exportFlagRepository;
        public ExportFlagAppService(IRepository<ExportFlagEntity, int> _exportFlagRepository) : base(_exportFlagRepository)
        {
            exportFlagRepository = _exportFlagRepository;
        }


    }


}

