﻿
using Abp.Application.Services;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Json;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob;
using System.Collections.Generic;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class HHDataSyncAppService : ApplicationService
    {
        private const string ClogTagType = "HHDataSync";

        private IRepository<SyncProductCostPriceEntity, long> syncProductCostPriceRepository;
        private IBackgroundJobManager backgroundJobManager;

        public HHDataSyncAppService(IRepository<SyncProductCostPriceEntity, long> _syncProductCostPriceRepository,
            IBackgroundJobManager _backgroundJobManager)
        {
            syncProductCostPriceRepository = _syncProductCostPriceRepository;
            backgroundJobManager = _backgroundJobManager;
        }

        /// <summary>
        /// 记录共享库存SKU价格变化
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public bool ProductCostPriceSync(List<SyncProductCostPriceDTO> dtos)
        {
            // TODO: validate beforeprice and afterprice is not null
            backgroundJobManager.Enqueue<SyncProductCostPriceFromHHJob, List<SyncProductCostPriceDTO>>(dtos, BackgroundJobPriority.Normal);
            return true;
        }

    }
}

