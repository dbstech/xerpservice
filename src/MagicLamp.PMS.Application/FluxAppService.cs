﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using MagicLamp.Flux.API.Models;
using MagicLamp.PMS.Proxy;
using Abp.UI;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.DTOs.Enums;
using Abp.BackgroundJobs;
using MagicLamp.XERP.BackgroundJob;
using MagicLamp.PMS.Infrastructure;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using MagicLamp.PMS.EntityFrameworkCore;
using Abp.Web.Models;

namespace MagicLamafterDto.PMS
{
    public class FluxAppService : ApplicationService
    {

        private IRepository<LotStockEntity, long> lockEntityRepository;
        private IBackgroundJobManager backgroundJobManager;

        public FluxAppService(IRepository<LotStockEntity, long> _lockEntityRepository,
            IBackgroundJobManager _backgroundJobManager)
        {
            lockEntityRepository = _lockEntityRepository;
            backgroundJobManager = _backgroundJobManager;
        }




    }
}
