﻿using System.ComponentModel;
using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.XERP.BackgroundJob;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MagicLamp.PMS.Infrastructure.Extensions;
using Abp.Dependency;
using Microsoft.Extensions.Options;
using MagicLamp.PMS.DTOs.Configs;
using Npoi.Mapper;
using Microsoft.AspNetCore.Http;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.DTOs.MqDTO;
using MagicLamp.PMS.DTOs.ThirdPart;
using MagicLamp.PMS.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using TimeZoneConverter;
using Cmq_SDK;
using Abp.Runtime.Caching;
using MagicLamp.PMS.Infrastructure.Common;
using MagicLamp.PMS.Domain;
using Microsoft.Extensions.Configuration;
using MagicLamp.PMS.Proxy.YiTongBaoOMSAPI;
using MagicLamp.PMS.DTOs.BondedWarehouseQingDao;
using Cmq_SDK.Cmq;
using MagicLamp.PMS.BaXi;
using MagicLamp.PMS.Proxy.Odoo;
using MagicLamp.PMS.Proxy.BaXi.Models;
using MagicLamp.PMS.YiWuBondedWarehouse;
using MagicLamp.PMS.Proxy.YiWuBondedWarehouse.Models;
using RestSharp;
using MagicLamp.PMS.DTOs.BondedWarehouse;
using MagicLamp.PMS.DTO;
using MagicLamp.PMS.Proxy.ECinx.Models;
using MagicLamp.PMS.Proxy.ECinx;
using Newtonsoft.Json;
using OdooRpc.CoreCLR.Client.Models;
using OdooRpc.CoreCLR.Client;
using OdooRpc.CoreCLR.Client.Interfaces;
using JsonRpc.CoreCLR.Client.Interfaces;
using JsonRpc.CoreCLR.Client;
using static MagicLamp.XERP.BackgroundJob.SYNCEcinxDeliveryQuery;
using Abp.Domain.Entities;
using Org.BouncyCastle.Asn1.Ocsp;
using static MagicLamp.PMS.EcinxAppService;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using static Org.BouncyCastle.Math.EC.ECCurve;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class TestAppService : ApplicationService
    {
        private IBackgroundJobManager _backgroundJobManager;
        private IRepository<ProductEntity, string> _productRepository;
        private IRepository<WareHouseEntity, int> _warehouseRepository;
        private IRepository<InventoryProductCategoryEntity, int> _productCateogryRepository;
        private IRepository<BrandEntity, int> _brandRepository;
        private IRepository<ProductBrandEntity, int> _productBrandRepository;
        private IRepository<LotStockEntity, long> _lotStockRepository;
        private IRepository<SeqEntity, int> _seqRepository;
        private IRepository<APIStockEntity, string> _apiStockRepository;
        private IRepository<ExchangeRate, string> _exchangeRateRepository;
        private BaXiClient baxiClient;
        private SendNotificationToDingDingJob ddJob;
        private IHostingEnvironment _env;
        private PMSDbContext _pmsContext;
        private HEERPDbContext _heerpContext;
        private List<EcinxConfig> _config;

        private ECinxClient _EcinxClient; //连接Ecinx同步产品
        private ECinxClient _WHYC_EcinxClient; //连接WHYC Ecinx同步产品


        private OdooConnectionInfo _odooConnection;
        private IOdooRpcClient _odooRpcClient;
        private IJsonRpcClient _jsonRpcClient;
        private long _odooUserId;
        private TimeZoneInfo timeZoneNZ = TZConvert.GetTimeZoneInfo("Pacific/Auckland");
        private TimeZoneInfo timeZoneCN = TZConvert.GetTimeZoneInfo("Asia/Shanghai");

        public List<string> whycwarehousecode = new List<string> { "YCYWBS", "YCYWBJZX", "YCYWMX", "YCYWKX" };

        Dictionary<string, string> clogTags = new Dictionary<string, string>
        {
            ["type"] = "EcinxAllInventorytesting"
        };

        public TestAppService(IBackgroundJobManager backgroundJobManager,
            IRepository<ProductEntity, string> productRepository,
            IRepository<WareHouseEntity, int> warehouseRepository,
            IRepository<InventoryProductCategoryEntity, int> productCateogryRepository,
            IRepository<BrandEntity, int> brandRepository,
            IRepository<ProductBrandEntity, int> productBrandRepository,
            IRepository<SeqEntity, int> seqRepository,
            IRepository<LotStockEntity, long> lotStockRepository,
            IRepository<APIStockEntity, string> apiStockRepository,
            IRepository<ExchangeRate, string> exchangeRateRepository,
            IHostingEnvironment env)
        {
            _backgroundJobManager = backgroundJobManager;
            _productRepository = productRepository;
            _warehouseRepository = warehouseRepository;
            _productCateogryRepository = productCateogryRepository;
            _brandRepository = brandRepository;
            _productBrandRepository = productBrandRepository;
            _lotStockRepository = lotStockRepository;
            _seqRepository = seqRepository;
            _apiStockRepository = apiStockRepository;
            _exchangeRateRepository = exchangeRateRepository;
            _env = env;
            _pmsContext = new PMSDbContextFactory().CreateDbContext();
            _heerpContext = new HEERPDbContextFactory().CreateDbContext();
            baxiClient = new BaXiClient();
            ddJob = new SendNotificationToDingDingJob();
            _config = Ultities.GetConfig<List<EcinxConfig>>();
            ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
            _EcinxClient = new ECinxClient(cacheManager, ECinxClient.EcinxAccountEnum.CQBSC); //连接Ecinx同步产品 任选一个数据源即可）
            _WHYC_EcinxClient = new ECinxClient(cacheManager, ECinxClient.EcinxAccountEnum.YIWUYINGCHI);//连接WHYC Ecinx同步产品 任选一个数据源即可）
        }

        public string TestEnviro()
        {
            return _env.EnvironmentName;
        }

        public bool TestFunc(ProductDTO inputDto)
        {
            try
            {
                //var bb = _seqRepository.FirstOrDefault(10);
                //var cc = _warehouseRepository.FirstOrDefault(9);
                //var aa = _lotStockRepository.GetAllList();
                _backgroundJobManager.Enqueue<FluxStockAllocationAutoConfigJob, string>("Y", BackgroundJobPriority.High);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("exception", ex);
                return false;
            }

        }

        public int test()
        {
            string a = "{\"Orders\":null}";
            return (a.ToJsonString().ToString().Length == 19 &&  a.ToJsonString().ToString().IndexOf("Orders") >= 0) ? 100 : 200  ;  //= "{\"Orders\":null}" ? 100 : 200;

        }

        public void InitProductCategory2(IFormFile file)
        {
            string filePath = Ultities.CreateDirectory("App_Data/ExcelImport");
            string fileName = $"{Guid.NewGuid()}{file.FileName}";
            string fullPath = Path.Combine(filePath, fileName);
            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }

            var mapper = new Mapper(fullPath);
            var firstLevelCategories = mapper.Take<dynamic>(0).Select(x => x.Value).ToList();
            var secondLevelCategories = mapper.Take<dynamic>(1).Select(x => x.Value).ToList();
            var thirdLevelCategories = mapper.Take<dynamic>(2).Select(x => x.Value).ToList();

            Dictionary<string, int> firstLevelDictionary = new Dictionary<string, int>();
            Dictionary<string, int> secondLevelDictionary = new Dictionary<string, int>();
            firstLevelCategories.ForEach(
                x =>
                {
                    InventoryProductCategoryEntity category = new InventoryProductCategoryEntity()
                    {
                        Name = x.cat.Trim(),
                        ParentID = 0
                    };
                    var id = _productCateogryRepository.InsertAndGetId(category);
                    firstLevelDictionary.Add(category.Name, id);
                }
            );

            secondLevelCategories.ForEach(
                x =>
                {
                    InventoryProductCategoryEntity category = new InventoryProductCategoryEntity()
                    {
                        Name = x.cat.Trim(),
                        ParentID = firstLevelDictionary[x.parent.Trim()]
                    };
                    var id = _productCateogryRepository.InsertAndGetId(category);
                    secondLevelDictionary.Add(category.Name, id);
                }
            );

            thirdLevelCategories.ForEach(
                x =>
                {
                    InventoryProductCategoryEntity category = new InventoryProductCategoryEntity()
                    {
                        Name = x.cat.Trim(),
                        ParentID = secondLevelDictionary[x.parent.Trim()]
                    };
                    var id = _productCateogryRepository.InsertAndGetId(category);
                    // categoryDictionary.Add(category.Name, id);
                }
            );
        }

        public Dictionary<string, int> InitProductCategory(IFormFile file)
        {
            string filePath = Ultities.CreateDirectory("App_Data/ExcelImport");
            string fileName = $"{Guid.NewGuid()}{file.FileName}";
            string fullPath = Path.Combine(filePath, fileName);

            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }

            var mapper = new Mapper(fullPath);
            var dtos = mapper.Take<InventoryProductCategoryDTO>().Select(x => x.Value).ToList();
            var entities = dtos.MapTo<List<InventoryProductCategoryEntity>>();
            Dictionary<string, int> codeID = new Dictionary<string, int>();

            entities.ForEach(x =>
            {
                x.Code = x.Code.Trim();
                x.Name = x.Name.Trim();
            });

            var fristLevel = entities.Where(x => x.Code.Length == 2).ToList();
            var secondLevel = entities.Where(x => x.Code.Length == 4).ToList();
            var thirdLevel = entities.Where(x => x.Code.Length == 6).ToList();

            foreach (var item in fristLevel)
            {
                var id = _productCateogryRepository.InsertAndGetId(item);
                codeID.Add(item.Code, id);
            }

            foreach (var item in secondLevel)
            {
                var parentID = codeID[item.Code.Substring(0, 2)];
                item.ParentID = parentID;
                var id = _productCateogryRepository.InsertAndGetId(item);
                codeID.Add(item.Code, id);
            }

            foreach (var item in thirdLevel)
            {
                var parentID = codeID[item.Code.Substring(0, 4)];
                item.ParentID = parentID;
                var id = _productCateogryRepository.InsertAndGetId(item);
                codeID.Add(item.Code, id);
            }
            return codeID;
        }


        public void InitBrandData()
        {

            var oldBrands = _productBrandRepository.GetAllList();
            List<string> codes = new List<string>();

            foreach (var item in oldBrands)
            {
                if (codes.Contains(item.BrandCode))
                {
                    continue;
                }
                BrandEntity brandEntity = new BrandEntity
                {
                    ActiveFlag = true,
                    CreateUser = "System",
                    ChineseName = item.CnBrand,
                    EnglishName = item.EnBrand,
                    ShortCode = item.BrandCode,
                    CreateTime = DateTime.Now
                };

                _brandRepository.Insert(brandEntity);
                codes.Add(item.BrandCode);
            }

        }

        public string TestNSFLogin(NSFAccountInfoDTO accountInfo)
        {
            return NSFProxy.Login(accountInfo);
        }

        public bool SentryTest()
        {
            int a = Convert.ToInt32("aaaaa");
            return true;
        }

        public FreightCreateOrderOutputDTO TestChangjiang(FreightCreateOrderInputDTO input)
        {
            var result = ChangJiangExpressProxy.CreateNewOrder(input);
            return result;
        }


        public string UGGGetToken()
        {
            return UGGProxy.GetToken();
        }

        public List<UGGStockResultDTO> UGGAuStock()
        {
            return UGGProxy.GetAllAuStock();
        }

        public List<UGGStockResultDTO> UGGCnStock()
        {
            return UGGProxy.GetAllCnStock();
        }

        public void UGGBackgroundJob()
        {
            UpdateStockForUGGJob testJob = new UpdateStockForUGGJob();
            testJob.UpdateStock();
        }

        public List<BondedWarehouseProductDTO> BondedWarehouseGetProducts()
        {
            var proxy = new BondedWarehouseProxy();
            return proxy.GetAllProducts();
        }

        public void RepushOrderPackToNSF()
        {
            RepushOrderPackToNSFJob test = new RepushOrderPackToNSFJob();
            test.RepushOrderPack();
        }

        public void TestNSFBackgroundJob()
        {
            NSFOrderDTO NSFOrder = new NSFOrderDTO
            {
                opid = 3736933,
                orderid = "423700002986",
                orderpackno = "423700002986-2-1",
                carrierid = "91502908981",
                freightid = "nsf",
                exportflag = 1,
                status = 4,
                packweight = (decimal)0.66
            };

            NSFOrder.Action = NSFBackgroundJobActionEnum.CreateOrder;

            IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
            backgroundJobManager.Enqueue<NSFBackgroundJob, NSFOrderDTO>(NSFOrder);
        }

        //Don't,TestAPI has been deploied on React Projecty
        //By Spark 08/01/2025
        public void TestUploaFTDExpress(string carrcarrierid)
        {
            var IdcardObj = from ops in _pmsContext.OrderIDCards
                            where ops.CarrierID == carrcarrierid 
                            && !string.IsNullOrWhiteSpace(ops.IDCardNo)
                            select ops;

            FreightCodeEnum freightCode = new FreightCodeEnum();

            if (IdcardObj.FirstOrDefault().FreightID == "FTDEXPRESSML")
            {
                freightCode = FreightCodeEnum.FTDEXPRESSML;
            }

            if (IdcardObj.FirstOrDefault().FreightID == "FTD")
            {
                freightCode = FreightCodeEnum.FTD;
            }

            if (IdcardObj.FirstOrDefault().FreightID == "FTDSF")
            {
                freightCode = FreightCodeEnum.FTDSF;
            }

            if (IdcardObj.FirstOrDefault().FreightID == "HYFTDMILK")
            {
                freightCode = FreightCodeEnum.HYFTDMILK;
            }

            var dto = new IDCardUploadMqDto
            {
                //TrackNos = new List<string>() { carrcarrierid },
                ReceiverName = IdcardObj.FirstOrDefault().RecevierName,
                IDCardNo = IdcardObj.FirstOrDefault().IDCardNo,
                //OrderPackNo = IdcardObj.FirstOrDefault().OrderPackNo,
                PhotoFront = IdcardObj.FirstOrDefault().URLFont,
                PhotoRear = IdcardObj.FirstOrDefault().URLBack,
                FreightCode = freightCode,
                //ReceiverPhone = IdcardObj.FirstOrDefault().ReceiverPhone
            };
            IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
            backgroundJobManager.Enqueue<IDCardUploadJob, IDCardUploadMqDto>(dto);
        }


        //Get FTD Last 1 month;'','',''
        public void TestFTDIDCardUploadJob()
        {

            List<string> FreightID = new List<string> { "FTDEXPRESSML", "FTD", "FTDSF", "HYFTDMILK" };

            List<string> corrierid = new List<string> { "175121302", "175121309", "175121310" };

            DateTime dateTime = DateTime.Now.AddDays(-30);



            var IdcardObj = from ops in _pmsContext.OrderIDCards
                            where corrierid.Contains(ops.CarrierID)
                            && !string.IsNullOrWhiteSpace(ops.IDCardNo)
                            select ops;


            //var IdcardObj = from ops in _pmsContext.OrderIDCards
            //             where ops.CreationTime > dateTime && FreightID.Contains(ops.FreightID)
            //             && !string.IsNullOrWhiteSpace(ops.IDCardNo)
            //             select ops;


            //List<string> Idcard = _pmsContext.OrderIDCards.Where(x => IdcardObj.ToList().Contains(x.Number)).ToList();

            //var ftdrepushidcard = _pmsContext.IDCards.Where(x => Idcard.Contains(x.Number)).ToList();

            foreach (var item in IdcardObj)
            {
                FreightCodeEnum freightCode = new FreightCodeEnum();


                if (item.FreightID =="FTDEXPRESSML")
                {
                    freightCode = FreightCodeEnum.FTDEXPRESSML;
                }

                if (item.FreightID == "FTD")
                {
                    freightCode = FreightCodeEnum.FTD;
                }

                if (item.FreightID == "FTDSF")
                {
                    freightCode = FreightCodeEnum.FTDSF;
                }

                if (item.FreightID == "HYFTDMILK")
                {
                    freightCode = FreightCodeEnum.HYFTDMILK;
                }


                var dto = new IDCardUploadMqDto
                {
                    
                    ReceiverName = item.RecevierName,
                    IDCardNo = item.IDCardNo,
                    PhotoFront = item.URLFont,
                    PhotoRear = item.URLBack,
                    FreightCode = freightCode,
                };
                IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
                backgroundJobManager.Enqueue<IDCardUploadJob, IDCardUploadMqDto>(dto);
            }
           
        }


        public void TestPushOrderToHH()
        {
            HHOrderLineDTO line = new HHOrderLineDTO
            {
                sku = "ZNZAT01010049",
                qty = 2
            };

            //HHOrderLineDTO line1 = new HHOrderLineDTO
            //{
            //    sku = "ZNZAT01113214",
            //    qty = 1
            //};

            HHPushOrderInputDTO input = new HHPushOrderInputDTO
            {
                order_no = "2040800211322-4-1",
                origin = "HH",
                order_lines = new List<HHOrderLineDTO>
                {
                    line
                }
            };

            var result = HHProxy.PushOrderToHH(input);
        }

        public void TestCancelOrderInHH()
        {
            HHPushOrderInputDTO input = new HHPushOrderInputDTO
            {
                order_no = "test0000002-3-2"
            };

            var result = HHProxy.CancelOrderInHH(input);
        }

        public void TestBindWaveAndOrder()
        {
            HHBindWaveAndOrderInputDTO input = new HHBindWaveAndOrderInputDTO
            {
                wave_picking_no = "W1807090007",
                create_time = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"),
                orders = new List<string> { "test0000002-3-1", "test0000002-3-2" }
            };

            var result = HHProxy.BindWaveAndOrder(input);
        }

        //{"order_no":"2040800287429-1-1","delivery_date":"2023-03-14 17:38:48","package_weight":2.70000000,"express_no":"FENGTIAN","express_company":"顺速达物流"}
        public void TestSyncOutboundDateToHH()
        {
            HHSyncOutboundDateInputDTO input = new HHSyncOutboundDateInputDTO
            {
                order_no = "2040800287429-1-1",
                delivery_date = "2023-03-14 17:38:48",
                package_weight = decimal.Parse("2.700"),
                express_no = "FENGTIAN",
                express_company = "顺速达物流"
            };
            var result = HHProxy.SyncOutboundDateToHH(input);
        }

        public void TestScanQueue()
        {
            PushOrderToFluxJob testCase = new PushOrderToFluxJob();
            testCase.ScanQueue(100);
        }

        public void TestSyncAladdinStock()
        {
            IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
            var skus = new List<string> { };
            backgroundJobManager.Enqueue<SyncAladdinStockJob, List<string>>(skus, BackgroundJobPriority.High);
        }

        /*public HHTokenInfoDTO TestHHGetAccessToken()
        {
            return HHProxy.GetAccessToken();
        }*/

        public void TestPushOrderToFluxJob()
        {
            PushOrderToFluxJob testJob = new PushOrderToFluxJob();
            testJob.ScanQueue(100);
        }

        public void TestPushOrderBindedToWaveToHHJob()
        {
            PushOrderBindedToWaveToHHJob testJob = new PushOrderBindedToWaveToHHJob();
            testJob.ScanFluxWave();
        }

        public void TestAutoAddPurchasingInfoForHHJob()
        {
            //** 存在未生成到货通知波次 处理流程
            /***
            // 1. 删除采购单该波次的所有采购单头和行信息: 如波次：W2404020002
               delete ta from po.PoOrderHeader ta where reference1 in ('W2404020002')
               delete ta  from po.PoOrderline ta where poheaderid in ('W2404020002')
               2. 更新推送波次状态
                update ta
                set Status='BindFailed',Message=''
                from HHBindedWaveNo ta where WaveNo in ('W2404020002')
               3. 运行方法：TestAutoAddPurchasingInfoForHHJob
            ***/
            List<string> wavenolist = new List<string>() {
               "W2404020002"
            };
            
            foreach (var item in wavenolist)
            {

                List<string> ordersForHH = new List<string>();

                var HHBindedwaveNoString = _pmsContext.HHBindedWaveNoEntities.Where(x => x.WaveNo == item).Select(x => x.OrdersIncluded).FirstOrDefault();

                if (HHBindedwaveNoString != null)
                {
                    HHBindedwaveNoString = HHBindedwaveNoString.Trim('[', ']').Replace("\"", "");
                    // Split the string by commas to get individual order numbers
                    string[] orderNumbers = HHBindedwaveNoString.Split(',');
                    ordersForHH.AddRange(orderNumbers);
                }
                
                HHBindWaveAndOrderInputDTO input = new HHBindWaveAndOrderInputDTO
                {
                    wave_picking_no = item,
                    create_time = DateTime.UtcNow.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss"),
                    orders = ordersForHH
                };

                IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
                backgroundJobManager.Enqueue<AutoAddPurchasingInfoForHHJob, HHBindWaveAndOrderInputDTO>(input, BackgroundJobPriority.High);
            }

        }


        public void TestFluxWave()
        {
            FluxWMSDbContext fluxWMSContext = new FluxWMSDbContextFactory().CreateDbContext();
            HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
            var config = heERPContext.APIConfigs.FirstOrDefault(x => x.ConfigKey == "StockShareConfig");
            var jObject = JObject.Parse(config.ConfigValue);
            var enabled = jObject["EnableStockShare"].Value<bool>();
            List<String> listTestSKU = ((JArray)jObject["TestSKU"]).Select(x => x.ToString()).ToList();
            List<String> listWaveRule = ((JArray)jObject["WaveRule"]).Select(x => x.ToString()).ToList();
            List<string> qualifiedWaveStatus = new List<string> { "40" };
            var listFluxWaveHeader = fluxWMSContext.FluxDocWaveHeaders.Where(x =>
            qualifiedWaveStatus.Contains(x.WaveStatus) && listWaveRule.Contains(x.WaveRule)).ToList();

        }

        public void TestProductRelatedInfoValidationJob()
        {
            _backgroundJobManager.EnqueueAsync<ProductRelatedInfoValidationJob, List<string>>(new List<string> { "BSCHAH5795", "LEEtest123113" });
        }

        public void TestAPIStock()
        {
            HEERPDbContext heERPContext = new HEERPDbContextFactory().CreateDbContext();
            var skus = new List<string> { "DEAMAE8419OX" };
            //get stock info
            var currentListApiStock = heERPContext.APIStocks.Where(x => skus.Contains(x.SKU))
                .Select(x => new
                {
                    x.SKU,
                    x.Qty_available,
                    x.Qty_hh,
                    x.Expired_date,
                    x.Expiry_date_hh
                }).ToList();
        }

        public void TestFluxInvLotLocId()
        {
            FluxWMSDbContext fluxWMSContext = new FluxWMSDbContextFactory().CreateDbContext();

            var sku = "ZNZAF050667";
            var testLocation = "A7-08-2";
            var excluded = PMSConsts.excludedLocation;

            var listFluxInvLotLocId = fluxWMSContext.FluxInvLotLocIds.Where(x => x.SKU == sku);
            var listUnqualifiedLocation = PMSConsts.UnqualifiedLocationId;

            var stockQtyFlux = listFluxInvLotLocId.Where(x => x.SKU.ToLower().Trim() == sku.ToLower().Trim()
                        && !listUnqualifiedLocation.Contains(x.LocationID)
                        && !x.LocationID.Contains(testLocation))
                            .Sum(x => Convert.ToInt32(x.Qty - x.QtyAllocated - x.QtyOnHold));

            /* var stockQtyFlux = listFluxInvLotLocId.Where(x => x.SKU.ToLower().Trim() == sku.ToLower().Trim())
                 .Sum(x => Convert.ToInt32(!listUnqualifiedLocation.Contains(x.LocationID) ? (x.Qty - x.QtyAllocated - x.QtyOnHold) : (0 - x.QtyAllocated - x.QtyOnHold)));*/

        }

        public void TestSyncOrderStatusFromFlux()
        {
            _backgroundJobManager.Enqueue<SyncFluxOrderStatusJob, string>("");
        }

        public void TestGetGroupInvLot()
        {
            var listUnqualifiedLocation = PMSConsts.UnqualifiedLocationId;
            FluxWMSDbContext fluxWMSContext = new FluxWMSDbContextFactory().CreateDbContext();
            var excluded = PMSConsts.excludedLocation;
            var time = DateTime.Now.AddMinutes(-5);

            var listSkuInfo = fluxWMSContext.FluxInvLotLocIds.Where(x => x.EditTime > DateTime.Now.AddMinutes(-5)).GroupBy(x => x.SKU).ToList();
            //var editedSku = listSkuInfo.Select(x => x.Key).ToList();
            var editedSku = new List<string> { "ZNZAF080462", "ZNZAF080459", "ZNZAT004119", "ZNZAT06666020" };

            var returnValue =
                from lotLoc in fluxWMSContext.FluxInvLotLocIds
                join lotAtt in fluxWMSContext.FluxInvLotAtts on new { lotLoc.LotNum, lotLoc.SKU } equals new { lotAtt.LotNum, lotAtt.SKU }
                where !listUnqualifiedLocation.Contains(lotLoc.LocationID)
                      && !lotLoc.LocationID.Contains(excluded)
                      && editedSku.Contains(lotLoc.SKU)
                group new { lotLoc, lotAtt } by new { lotLoc.SKU, lotLoc.LotNum, lotAtt.LotAtt02, lotAtt.LotAtt04 }
                into grp
                select new
                {
                    Sku = grp.Key.SKU,
                    LotNum = grp.Key.LotNum,
                    Qty = grp.Sum(x => x.lotLoc.Qty - x.lotLoc.QtyAllocated - x.lotLoc.QtyOnHold),
                    ExpiryDateSpecific = grp.Key.LotAtt02,
                    ExpiryDateAfterOpen = grp.Key.LotAtt04
                };

            var listSkuStock = returnValue.ToList();

            var listToBeProcessed = listSkuStock.GroupBy(x => x.Sku)
                .Select(x => new
                {
                    Sku = x.Key,
                    Qty = x.Sum(y => y.Qty),
                    ExpiryDateSpecific = x.Min(y =>
                    {
                        DateTime.TryParse(y.ExpiryDateSpecific, out DateTime resultDate);
                        return resultDate;
                    }),
                    ExpiryDateAfterOpen = x.Select(y => y.ExpiryDateAfterOpen).FirstOrDefault(z => !z.IsNullOrEmpty())
                })
                .ToList();

            foreach (var item in listToBeProcessed)
            {
                if (item.ExpiryDateSpecific == DateTime.MinValue)
                {
                    //use afteropen
                }
            }

            var num = returnValue.Count();
        }

        public void TestSyncFluxStockJob()
        {
            var editedSku = new List<string> { "XCNAF080462", "xcnAF080459", "ZNZPW005167 ", "ZNZAT004119", "AUEXT06666020" };

            SyncFluxStockJob testJob = new SyncFluxStockJob();
            testJob.Process(editedSku);
        }


        public void TestFullSyncFluxStock()
        {
            _backgroundJobManager.Enqueue<FullSyncFluxStockJob, string>(null);
        }

        public void TestIncrementallySyncFluxStockJob()
        {
            List<string> jobInput = new List<string>();
            _backgroundJobManager.Enqueue<SyncFluxStockJob, List<string>>(jobInput, BackgroundJobPriority.High);
        }


        public void TestRefreshLockQty()
        {
            //List<string> skus = new List<string>() { "ZNZAT02181804", "ZNZAT06666019", "ZNZAT111458", "ZNZAT780166", "ZNZATGRGWP005", "ZNZATGV145011", "ZNZATHN137315", "ZNZATJL203097", "ZNZATLE261001", "znzatNW115001", "znzatNW115011", "ZNZATSL007907", "ZNZATSW113041", "ZNZATSW113045", "ZNZATSW113109", "ZNZATSW113126", "ZNZCY017959", "ZNZDQ81213BL100", "ZNZEN162425", "ZNZGY561405", "ZNZJE162050", "ZNZLE004699", "ZNZNE045888", "ZNZSR515897", "ZNZSR515958" };
            //new RefreshSKULockQtyJob().Process(skus);

            _backgroundJobManager.EnqueueAsync<ProductRelatedInfoValidationJob, List<string>>(new List<string> {"ZNZATAT19221"});
            //_backgroundJobManager.Enqueue<RefreshSKULockQtyJob, List<string>>(skus);
        }

        public void TestASNTime()
        {
            FluxWMSDbContext fluxWmsContext = new FluxWMSDbContextFactory().CreateDbContext();


            var listPendingASNHeader =
                    fluxWmsContext.FluxDocASNHeaders.FirstOrDefault(x => x.ASNReference1 == "tt552");

            var timeZoneNZ = TZConvert.GetTimeZoneInfo("Pacific/Auckland");

            var testTime = listPendingASNHeader.ASNCreationTime.SpecifyKindInUTC();
            var restult = TimeZoneInfo.ConvertTimeFromUtc(listPendingASNHeader.ASNCreationTime.SpecifyKindInUTC(), timeZoneNZ).ToString("yyyy-MM-dd HH:mm:ss");

        }
        //MFD
        public List<string> warehousecode = new List<string> { "CQZGC", "YOCHYDF", "CQBSC", "YWOYBSCNFMX", "YWOYBSCYWBS", "YWOYBSCNFKX", "YWOYBSCBJPZX", "NSFYWZYDFC", "QDXHZYC", "NBZ005", "YWOYDMC", "ZX-YWZ007", "QDXHCNC", "YWOYBSCBLP", "YWOYBSCBSNM", "CQZGCFTF2", "CQZGCSTF", "OTHER", "HKZ005", "FZZ005", "GZZ003", "YWZ806" };
        //WHYC
        public List<string> whyc_warehousecode = new List<string> { "YCYWBS", "YCYWBJZX", "YCYWMX", "YCYWKX" };

        public void SyncYWYC()
        {
            EcinxConfigerData config = EcinxConfigerData.Configuration;
            //MFD
            int[] WHYCWarehouseID = config.PushWHYCMFDWarehouseID;
          

        }

    public void TestCMQ()
        {

            //string payload = System.IO.File.ReadAllText(Ultities.GetPhysicalPath("App_Data/TestPayload.txt"));
            //var skus = new List<string> { "ADHXN0001","AUBXD0004","AUBXD00077","AUBXD00084","AUBXD0012","AUBXD00134"};
            //get stock 


            //var productlist = _pmsContext.Products.ToList(); && skus.Contains(x.SKU)
            var productlist = _pmsContext.Products.Where(x => x.ActiveFlag == true ).ToList();
            
            foreach (var product in productlist)
            {
                ProductInfoMqDto productInfoMqDto = new ProductInfoMqDto();
                productInfoMqDto.Action = MqActionEnum.Update;
                productInfoMqDto.Before = product.MapTo<ProductDTO>();
                productInfoMqDto.After  = product.MapTo<ProductDTO>();

                Dictionary<string, string> clogTags = new Dictionary<string, string>();
                clogTags["trackid"] = productInfoMqDto.After.SKU;

                BaseMqDTO<ProductInfoMqDto> skuInfoMqDto = new BaseMqDTO<ProductInfoMqDto>();
                skuInfoMqDto.MessageBody = productInfoMqDto;

                TencentCMQProxy.PublishTopicMessage(TCConstants.CMQTopics.TOPIC_SKUINFO, skuInfoMqDto, null, clogTags);
            }

           
        }

        public string TestGetCancelledOrderFromRedis()
        {
            ICacheManager Cache = IocManager.Instance.Resolve<ICacheManager>();

            var orderID = "123";

            var valueObject = new
            {
                Payload = true,
                Type = "System.Boolean"
            };
            var stringValue = valueObject.ToJsonString();


            var manager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);
            var val = manager.GetOrDefault(string.Format(CacheKeys.CANCELLEDORDERID, orderID));

            if (val == null)
            {
                throw new RequestValidationException("失败");
            }
            else
            {
                return "成功";
            }

        }


        public void TestEFQueryWithWhiteSpace()
        {
            List<string> skus = new List<string> { "AUFD21012709" };
            List<string> skus2 = new List<string> { "AUFD21012709 " };
            string sku = "AUFD21012709";
            string sku2 = "AUFD21012709 ";
            var entity1 = _apiStockRepository.GetAll().Where(x => skus.Contains(x.SKU)).ToList();
            var entity2 = _apiStockRepository.GetAll().FirstOrDefault(x => sku == x.SKU);
            var entity3 = _apiStockRepository.GetAll().Where(x => skus2.Contains(x.SKU)).ToList();
            var entity4 = _apiStockRepository.GetAll().FirstOrDefault(x => sku2 == x.SKU);


            List<string> skus3 = new List<string> { "ZNZAF050352" };
            var productEntity = _productRepository.GetAll().Where(x => skus3.Contains(x.SKU)).ToList();

            foreach (var item in skus3)
            {
                var product = productEntity.FirstOrDefault(x => x.SKU == item);
            }


            var entity5 = entity3.Where(x => skus.Contains(x.SKU)).ToList();
            var entity6 = entity4.SKU == sku;
        }

        public void TestTencentCloudRedis(string key1, string value1, string key2, string value2)
        {
            ICacheManager Cache = IocManager.Instance.Resolve<ICacheManager>();
            var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);
            KeyValuePair<string, object>[] keyValuePairs = new KeyValuePair<string, object>[1];
            keyValuePairs[0] = new KeyValuePair<string, object>(key2, value2);

            cacheManager.Set(key1, value1, TimeSpan.FromHours(24), TimeSpan.FromHours(24));
            cacheManager.Set(keyValuePairs, TimeSpan.FromHours(24), TimeSpan.FromHours(24));
        }

        public void TestGetOrderInHH()
        {
            HHPushOrderInputDTO input = new HHPushOrderInputDTO
            {
                order_no = "test0000002-3-2"
            };

            var result = HHProxy.GetOrder(input);
        }

        public string TestGenerateAPIKey()
        {
            APIAuthenticationDomainService service = new APIAuthenticationDomainService();

            return service.GenerateAPIKey();
        }

        public void TestCLSLog()
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["orderid"] = "123123";
            tags["sku"] = "asdasd;123123aaa;sadas11";
            CLSLogger.Info("testlog", "test-123-321-456", tags);
        }

        [HttpGet]
        public void RemoveCache(string cacheManager, string key)
        {
            ICacheManager Cache = IocManager.Instance.Resolve<ICacheManager>();
            var manager = Cache.GetCache(cacheManager);
            manager.Remove(key);
        }

        [HttpGet]
        public string TestAppSettingConfiguration(string sectionName, string key)
        {
            var section = Configuration.AppConfigurations.Get(Environment.CurrentDirectory).GetSection(sectionName);

            if (section == null)
            {
                return "can not found specified sectionName";
            }

            return section.GetValue<string>(key);

        }

        public void TestSaveOrderPackDetailProductCostJob()
        {
            List<OrderPackDetailProductCostDTO> listCost = new List<OrderPackDetailProductCostDTO>();
            OrderPackDetailProductCostDTO cost = new OrderPackDetailProductCostDTO
            {
                OrderID = "testId",
                SKU = "testSKU",
                Qty = 1,
                Cost = 9.99M,
                CreationTime = DateTime.UtcNow
            };
            listCost.Add(cost);

            _backgroundJobManager.EnqueueAsync<SaveOrderPackDetailProductCostJob, List<OrderPackDetailProductCostDTO>>(listCost);
        }

        public void TestAddSKUStockLockForHH()
        {
            ProductDomainService pService = new ProductDomainService();

            OrderSKULockForHHEntity entry = new OrderSKULockForHHEntity
            {
                OrderID = "testorder",
                SKU = "testsku",
                QTY = 1,
                LockDuration = 3600,
                CreationTime = DateTime.UtcNow
            };


            List<OrderSKULockForHHEntity> input = new List<OrderSKULockForHHEntity> { entry };

            pService.AddHHStockLockByOrderID(input);
        }

        public void FreeHHStockLock()
        {
            ProductDomainService pService = new ProductDomainService();
            pService.FreeHHStockLockByOrderID("10000001487");
        }

        public List<InventoryQueryResult> InventoryQuery(string itemCode)
        {
            var proxy = new YiTongBaoOMSProxy();

            var criteriaList = new List<InventoryQueryCriteria> {
                new InventoryQueryCriteria {
                    itemCode = itemCode
                }
            };
            var result = proxy.InventoryQuery(criteriaList);
            return result.ToList();
        }

        public List<Cmq_SDK.Cmq.Message> BatchReceiveMessage(int? numOfMsg, int? pollingWaitSeconds)
        {
            return TencentCMQProxy.BatchReceiveAndDeleteMessages("yishan_currency_rate");
        }

        public void InsertOrUpdateExchangeRate(string from, string to, decimal rate)
        {
            _exchangeRateRepository.InsertOrUpdate(new ExchangeRate
            {
                From = from,
                To = to,
                Rate = rate
            });
        }

        public async void ProductsQuery()
        {
            StringBuilder priceUpdateMsg = new StringBuilder();
            int PageSize = 50;
            List<SyncAladdinStockDTO> listStockForSync = new List<SyncAladdinStockDTO>();

            int warehouseId = _pmsContext.WareHouses.FirstOrDefault(x => x.Location == "八禧仓").Id;
            List<ProductEntity> products = _pmsContext.Products.Where(x => x.WarehouseID == warehouseId && x.ActiveFlag == true).ToList();
            

            List<string> skus = products.Select(x => x.SKU).ToList();
            List<string> upcs = products.Select(x => x.Barcode1).Distinct().ToList();

            BaXiClient client = new BaXiClient();


            for (int i = 0; i < upcs.Count; i += PageSize)
            {
                var batchUpcs = upcs.Skip(i).Take(PageSize).ToList();

                // data process 
                //<BaXiProduct> baXiProducts = await client.ProductsQuery(batchUpcs);
                //下线的八喜产品
                //List<BaXiProduct> baXiOffProducts = await client.ProductsOffQuery(batchUpcs);

                var result = await client.BAXIProductsQuery(batchUpcs);
                // Access the off and true products
                List<BaXiProduct> baXiOffProducts = result.offProducts;
                List<BaXiProduct> baXiProducts = result.trueProducts;

                List<BaXiProductInfo> baXiProductInfos = _pmsContext.BaXiProductInfos.ToList();

                _heerpContext.Database.SetCommandTimeout(120);
                List<APIStockEntity> apiStocks = _heerpContext.APIStocks.Where(x => skus.Contains(x.SKU) && x.Status == 1).ToList();


                //CLSLogger.Info("baXiProducts", "baXiProducts:" + baXiProducts.ToJsonString(), LogTags);
                //CLSLogger.Info("baXiOffProducts", "baXiOffProducts:" + baXiOffProducts.ToJsonString(), LogTags);


                foreach (var _baxiproducts in baXiProducts)
                {
                    string Upc = _baxiproducts.Upc;
                    int stock = (int)_baxiproducts.Stock;
                    string expired_date = _baxiproducts.Deadline?.ToString("yyyy/MM/dd");
                    decimal newPrice = Convert.ToDecimal(_baxiproducts.Price);

                    List<ProductEntity> productsToUpdate = products.Where(x => x.Barcode1 == Upc).ToList();

                    foreach (var product in productsToUpdate)
                    {
                        APIStockEntity apiStock = apiStocks.FirstOrDefault(x => x.SKU == product.SKU);

                        if (apiStock.Qty != stock)
                        {
                            SyncAladdinStockDTO syncdata = new SyncAladdinStockDTO();
                            syncdata.best_before = _baxiproducts.Deadline?.ToString("yyyy/MM/dd");
                            syncdata.sku = product.SKU;
                            syncdata.stock = stock;
                            listStockForSync.Add(syncdata);

                            // 更新库存
                            //APIStockEntity apistock = apiStocks.FirstOrDefault(x => x.SKU == product.SKU);
                            apiStock.Qty = stock;
                            apiStock.Qty_available = stock;
                            apiStock.Qty_aladdin = stock;
                            apiStock.Expired_date = expired_date;
                            apiStock.Updated_time = DateTime.Now;
                            _heerpContext.APIStocks.Update(apiStock);
                            _heerpContext.SaveChanges();
                        }


                        ProductEntity productinfo = product;
                        productinfo.Cost = newPrice;
                        _pmsContext.Products.Update(productinfo);
                        _pmsContext.SaveChanges();
                    }


                    //更新成本价格
                    BaXiProductInfo baXiProductInfo = baXiProductInfos.FirstOrDefault(x => x.Upc == Upc);
                    if (baXiProductInfo == null)
                    {
                        BaXiProductInfo baxiinfo = new BaXiProductInfo();
                        baxiinfo.Upc = Upc;
                        baxiinfo.Price = newPrice;
                        baxiinfo.Stock = stock;
                        _pmsContext.BaXiProductInfos.Add(baxiinfo);
                        _pmsContext.SaveChanges();

                    }
                    else
                    {
                        if (newPrice != baXiProductInfo.Price)
                        {
                            productsToUpdate.ForEach(product =>
                            {
                                priceUpdateMsg.Append($"{product.SKU} {Upc}\n\n{product.ChineseName}\n\n${baXiProductInfo.Price}->${newPrice}");
                            });
                            baXiProductInfo.PreviousPrice = baXiProductInfo.Price;
                            baXiProductInfo.Price = newPrice;
                            baXiProductInfo.LastModified = DateTime.UtcNow;
                            _pmsContext.BaXiProductInfos.Update(baXiProductInfo);
                            _pmsContext.SaveChanges();
                        }
                    }





                }

                foreach (var _baxiproducts in baXiOffProducts)
                {
                    string Upc = _baxiproducts.Upc;
                    int stock = 0;
                    List<ProductEntity> productsToUpdate = products.Where(x => x.Barcode1 == Upc).ToList();

                    foreach (var product in productsToUpdate)
                    {
                        APIStockEntity apiStock = apiStocks.FirstOrDefault(x => x.SKU == product.SKU);

                        if (apiStock.Qty != stock)
                        {
                            SyncAladdinStockDTO syncdata = new SyncAladdinStockDTO();
                            syncdata.sku = product.SKU;
                            syncdata.stock = stock;
                            listStockForSync.Add(syncdata);

                            // 更新库存0 已下线；
                            //APIStockEntity apistock = apiStocks.FirstOrDefault(x => x.SKU == product.SKU);
                            apiStock.Qty = stock;
                            apiStock.Qty_available = stock;
                            apiStock.Qty_aladdin = stock;
                            apiStock.Updated_time = DateTime.Now;
                            apiStock.ProductNameCN = apiStock.ProductNameCN + " Baxi Flag =Off(产品已下线)";
                            _heerpContext.APIStocks.Update(apiStock);
                            _heerpContext.SaveChanges();

                            ProductEntity productinfo = product;
                            productinfo.ActiveFlag = false;
                            productinfo.ChineseName = product.ChineseName + "[八喜仓已下线]";
                            _pmsContext.Products.Update(productinfo);
                            _pmsContext.SaveChanges();

                        }
                    }
                }

                if (priceUpdateMsg.Length > 0)
                {
                    priceUpdateMsg.Insert(0, "##### 以下八禧仓产品价格发生更改： \n\n");

                    List<string> staffToNotify = new List<string> { "+86-18766212252" };

                    string ddTokenBaXi = "34adddda7c173e972431bbd7a0f0f3ffbebb8746dbfdc66c4191f2e168e098ef";
                    string signature = "SECe480bad4be94d0ce5b516f826141d46a6130a54be731558507d68a583f675fb0";
                    DDNotificationProxy.MarkdownNotification($"八禧仓监控报警", priceUpdateMsg.ToString(), ddTokenBaXi, signature, staffToNotify, 100000);
                }

                if (listStockForSync.Count > 0)
                {
                    //LogTags["method"] = "IncrementBAXISKU_Stock";

                    //同步给188
                    EShopProxy eShop = new EShopProxy();
                    eShop.SyncStockTo188(listStockForSync);

                    //CLSLogger.Info("增量库存_同步188完成", "同步记录数:" + listStockForSync.ToJsonString(), LogTags);
                }


            }
        }
        public async Task<List<BaXiProduct>> ProductsQueryAll()
        {
            BaXiClient client = new BaXiClient();
            var response = await client.ProductsQueryAll();
            return response;
        }

        public async Task<LoginResponse> Login()
        {
            ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
            YiWuBondedWarehouseClient client = new YiWuBondedWarehouseClient(cacheManager, YiWuAccountEnum.Default);
            var response = await client.Login();
            return response;
        }

        public async Task<TradeOrderQueryResponse> TradeOrderQuery(string orderid)
        {
            ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
            YiWuBondedWarehouseClient client = new YiWuBondedWarehouseClient(cacheManager, YiWuAccountEnum.Default);
            var response = await client.TradeOrderQuery(new TradeOrderQueryData{
                orderType = "10",
                orderCode = orderid
            });
            return response;
        }

     

       
        public void TestYiTongBaoCreateOrder()
        {
            YiTongBaoOMSProxy yProxy = new YiTongBaoOMSProxy();

            YiTongBaoDeliveryOrderDTO inputDTO = new YiTongBaoDeliveryOrderDTO
            {
                modifyMark = "ADD",
                billNo = "YT2019",
                ebcOmsCode = "TFXHNJC",
                ebpOmsCode = "33019492AB",
                logisticsOmsCode = "ZTO",
                payOmsCode = "TENPAY",
                orderNo = "testOrder",
                payNo = "testPay",
                //
                assureCode = "MFD",
                buyerIdNumber = "430124199201140024",
                buyerName = "张三三",
                buyerRegNo = "001",
                buyerTelephone = "13888888888",
                consigneeAddress = "某省 某市 某区 某路 某楼 某间",
                consigneePhone = "13666666666",
                consigneeName = "李四四",
                province = "某省",
                city = "某市",
                area = "某区",
                freight = 0,
                discount = 0,
                goodsValue = 10,
                taxTotal = 0,
                acturalPaid = 10,
                grossWeight = 20,
                netWeight = 10,
                senderName = "淘宝",
                senderAddress = "郑州新郑综合保税区,",
                senderPhone = "66666666",
                orderTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                orderItems = new List<orderItems>
                {
                    new orderItems
                    {
                        itemCode = "testSKU",
                        //itemCode = "158987709600001",
                        qty = 1,
                        price = 10
                    }
                }
            };


            var orderResult = yProxy.CreateOrder(inputDTO);

            var content = orderResult.Content;

            var result = content.ConvertFromJsonString<YiTongBaoDeliveryOrderOutputDTO>();

            if (result.code != "0")
            {
                SendNotificationToDingDingJob ddJob = new SendNotificationToDingDingJob();
                ddJob.SendBondedWarehouseQingDaoAlert($"易通宝返回失败，请检查。订单为：{inputDTO.orderNo}。易通宝返回失败原因：{result.msg}");
            }
        }

        public async Task TestBaXiCreateOrderAsync()
        {

            BaXiCreateOrderInput orderInput = new BaXiCreateOrderInput
            {
                orderNo = "802700000465-1-1",
                //发件人
                sender = "ALA",
                senderPhone = "0296000000",
                consignee = "韩英",
                phone = "15848128858",

                province = "内蒙古自治区",
                city = "呼和浩特市 ",

                powderList = new List<string> { "1", "4", "3", "10" },
                groceriesList = new List<string> { "1", "4", "3", "19" },

                address = "内蒙古自治区 呼和浩特市 新城区 胜利路东口路南兴野印章",
                goods = new List<BaXiCreateOrderInput.Good>()
            };

            orderInput.goods.Add(new BaXiCreateOrderInput.Good
            {
                id = "9345850006996",
                number = "3"
            });

            BaXiClient client = new BaXiClient();

            var response = await client.CreateOrder(orderInput, orderInput.orderNo);

            if (response.retCode != ((int)BaXiCreateOrderRetCodeEnum.成功).ToString())
            {
                SendNotificationToDingDingJob ddJob = new SendNotificationToDingDingJob();
                //dingding notification
                ddJob.SendBaXiAlert($"八禧仓创建订单失败，订单号为：{orderInput.orderNo}。失败详情：{response.ToJsonString()}", orderInput.orderNo);
                //set status to pending
            }



        }

        public async Task TestBaXiOrderDetail()
        {
            string orderNo = "test0011";

            BaXiClient client = new BaXiClient();

            var response = await client.GetOrderDetail(orderNo);
        }


        public async Task TestBaxiSyncOrderStatus()
        {
            int pageSize = 10;

            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = "BaXiOrderProcessJob",
                ["method"] = "SyncOrderDetail"
            };

            try
            {
                var listBaXiOrder = _pmsContext.BaXiOrders.Where(x => x.Status == BaXiOrderStatusEnum.ordered.ToString()).ToList();

                var totalCount = listBaXiOrder.Count();
                int totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;


                //订单下所有包裹都出库的话，更新orderpack为出库，并记录freightid，以；分开
                for (int i = 0; i < totalPage; i++)
                {
                    var baxiOrderToProcess = listBaXiOrder.OrderBy(x => x.CreateTime).Skip(i * pageSize).Take(pageSize).ToList();

                    var listOpID = baxiOrderToProcess.Select(x => x.Opid).ToList();

                    CLSLogger.Info("八禧订单将同步状态一批", $"listopid为：{listOpID.ToJsonString()}", clogTags);

                    List<OrderPackEntity> listOrderPackNo = _heerpContext.OrderPacks.Where(x => listOpID.Contains(x.opID)).ToList();

                    foreach (var order in baxiOrderToProcess)
                    {
                        await UpdateBaXiOrderStatusAsync(order, listOrderPackNo);
                    }
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("八禧订单同步状态出错", ex, clogTags);
            }

        }


        private async Task UpdateBaXiOrderStatusAsync(BaXiOrderEntity order, List<OrderPackEntity> listOrderPackNo)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = "UpdateBaXiOrderStatusAsync",
                ["method"] = "UpdateBaXiOrderStatusAsync"
            };

            var orderDetail = await baxiClient.GetOrderDetail(order.OrderPackNo);

            CLSLogger.Info("八禧仓订单同步状态结果", $" 包裹号：{order.OrderPackNo} API返回明细：{orderDetail}", clogTags);

            StringBuilder listTrackingNo = new StringBuilder();

            bool isOutbound = true;

            foreach (var package in orderDetail.order.packages)
            {
                if (package.status != "已出库" && package.status != "已取消")
                {
                    isOutbound = false;
                }
                else
                {
                    if (orderDetail.order.packages.Count() == 1)
                    {
                        listTrackingNo.Append($"{package.packageNo}");
                    }
                    else
                    {
                        listTrackingNo.Append($"{package.packageNo};");
                    }
                }
            }

            clogTags["orderpackno"] = order.OrderPackNo;

            CLSLogger.Info("八禧仓订单同步状态结果", $"是否出库：{isOutbound}", clogTags);

            try
            {
                if (isOutbound)
                {
                    order.Status = BaXiOrderStatusEnum.done.ToString();
                    order.UpdateTime = DateTime.UtcNow;

                    if (order.ResponseData.IsNullOrEmpty() || order.ResponseData.Contains("orderNo已存在"))
                    {
                        order.ResponseData = orderDetail.ToJsonString();
                    }

                    var orderPack = listOrderPackNo.FirstOrDefault(x => x.opID == order.Opid);

                    CLSLogger.Info("八禧仓订单同步状态erp包裹", $"{orderPack.ToJsonString()}", clogTags);

                    if (orderPack == null)
                    {
                        //dingding notification
                        ddJob.SendBaXiAlert($"八禧仓更新订单失败，订单号为：{order.OrderPackNo}。失败详情：未找到erp对应包裹。", order.OrderPackNo);
                        return;
                    }

                    orderPack.freightId = "AUOD";

                    if (listTrackingNo.Length > 500)
                    {
                        ddJob.SendBaXiAlert($"八禧仓所分配物流号过长，无法记录。订单号为：{order.OrderPackNo}。物流号：{listTrackingNo}", order.OrderPackNo);
                    }
                    else
                    {
                        orderPack.carrierId = listTrackingNo.ToString();
                    }

                    orderPack.status = (int)OrderPackStatusEnum.Outbound;
                    orderPack.out_time = orderPack.out_time ?? DateTime.UtcNow.ConvertUTCTimeToNZTime();

                    _heerpContext.SaveChanges();
                    _pmsContext.SaveChanges();

                    CLSLogger.Info("八禧订单同步状态完成，订单已出库", $"包裹号：{order.OrderPackNo}，明细：{orderDetail.ToJsonString()}", clogTags);
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Info("八禧订单同步状态失败", $"{ex.ToJsonString()}", clogTags);
                throw;
            }
           
        }

        public void LogBaXiOrderProductPrice()
        {
            _backgroundJobManager.Enqueue<LogBaXiOrderProductPrices, List<int>>(new List<int> { 1904247 });
        }

        public async Task<ResultList> TestYiWuCreateSupplierOrder()
        {
            CreateSupplierOrderInput testInput = new CreateSupplierOrderInput
            {
                Orders = new List<SupplierOrder>()
            };

            testInput.Orders.Add(new SupplierOrder
            {
                orderSource = "73",
                orderCode = "E202104231410100666042411210HZ",
                receiverName = "熊大",
                mobile = "13416648623",
                province = "江苏省",
                city = "宿迁市",
                district = "泗阳县",
                receiverAddress = "雨润广场",
                receiverZip = "223700",
                //payCompanyType = ((int)PayCompanyTypeEnum.银盈通).ToString(),
                payCompanyType = "12",
                payNumber = "210412141011000100",
                payDate = "2021-04-12 14:10:28",
                payment = "1066.00",
                couponAmount = "0.0",
                orderTotalAmount = "1066.00",
                orderGoodsAmount = "977.08",
                orderTaxAmount = "88.92",
                feeAmount = "0.00",
                insureAmount = "0.0",
                paperType = "01",
                name = "张**",

                paperNumber = "310123148505132118",
                companyCode = "",
                receptUrl = "",
                orderDate = "2021-04-12 14:10:10",
                holdCode = "",
                clientId = "20",
                outOrderCode = "E20210412141010066604241",
                OrderDtls = new List<OrderDetail> { new OrderDetail
                {
                    commodityCode = "N6117870004",
                    commodityName = "500g 灌装 荷兰进口奶粉",
                    qty = "1",
                    price = "977.08",
                    amount = "977.08"
                }}
            });



            ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
            YiWuBondedWarehouseClient testYiWu = new YiWuBondedWarehouseClient(cacheManager, YiWuAccountEnum.Default);
            return await testYiWu.CreateSupplierOrder(testInput);

        }


        public async Task<string> odooConnectionTest(List<string> skus)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = "odooConnectionTestTyoe",
                ["method"] = "odooConnectionTestmethod"
            };
            OdooProxy odooProxy = new OdooProxy();
            try
            {
                await odooProxy.LoginToOdoo();
            }
            catch (Exception ex)
            {
                //CLSLogger.Info("同步odoo品牌,方法:SyncOdooProductBrandJob 登录发生异常", ex);
                CLSLogger.Info("odooConnectionTest", $" 测试：{ex.ToJsonString()}", clogTags);
                throw ex;
            }
            var odooBrands = await odooProxy.GetAllProductBrands();
            CLSLogger.Info("odooConnectionTestResult", $" Data：{odooBrands.ToJsonString()}", clogTags);
            return odooBrands.ToJsonString();

        }


     

   

        //Test Ecinx Log
        public void  TestEcinxLog(EcinxOrderInput inputDto)
        {
          
            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();
            ecinpushorderLog.OrderPackNo = "2008-10-10";
            ecinpushorderLog.LackStockDays = 20;
            ecinpushorderLog.MessageType = "定时或手动触发推送";
            ecinpushorderLog.FaildReason = "_ 区名称不可为空 _";
            ecinpushorderLog.MessageDetail = "ECinx下单时发现订单已包";
            _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
            _pmsContext.SaveChanges();

        }


        public string WHYC_TestSyncStockChange(EcinxStockChange ResponseDTO)
        {
            string _return = "success";
            EcinxInventoryQueryResponse result = new EcinxInventoryQueryResponse();

            if (ResponseDTO == null)
            {
                CLSLogger.Info("WHYC_EcinxStockChange", "ResponseDTO is null", clogTags);
                return "failed";
            }

            var Response = ResponseDTO.ToJsonString();
            CLSLogger.Info("WHYC_EcinxStockChange", Response, clogTags);

            try
            {
                if (ResponseDTO.goodsCode == null)
                {
                    CLSLogger.Info("WHYC_EcinxStockChange", "goodsCode is null", clogTags);
                    return "failed";
                }

                EcinxInventoryQueryInput input = new EcinxInventoryQueryInput
                {
                    goodsCode = ResponseDTO.goodsCode
                };

                result = _WHYC_EcinxClient.QueryInventory(input).Result;

                CLSLogger.Info("WHYC_result", result.ToJsonString(), clogTags);

                if (result == null)
                {
                    string MessageContent = $"WHYC_Ecinx查询库存失败, 返回值：{result.ToJsonString()}";
                    //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx查询库存失败_SyncStockChange", MessageContent);
                    CLSLogger.Info("WHYC_QueryEcinxinventory", $"goodcode:{ResponseDTO.goodsCode} Message：{MessageContent} ", clogTags);

                    return _return;
                }
                else
                {
                    if (result.result.total == 0)
                    {
                        string MessageContent = $"_WHYC_EcinxClientEcinx库存查询失败_产品原因,SKU:{ResponseDTO.goodsCode} 在Ecinx系统未查到产品库存信息!";
                        //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx库存查询失败", MessageContent);
                        return _return;
                    }


                    int totalinventory = 0;
                    foreach (var item in result.result.rows)
                    {
                        //Filter WarehouseCode
                        if (whycwarehousecode.Contains(item.warehouseCode))
                        {
                            //All canSalesNum
                            totalinventory = totalinventory + item.canSalesNum;
                        }
                    }


                    if (totalinventory <= 0)
                    {
                        return _return;
                    }
                    else
                    {

                        //DateTime start = Convert.ToDateTime("2022-07-08 10:00:00"); //UTC时间 -> 北京时间: 2022-07-08 18:00:00
                        //DateTime end = Convert.ToDateTime("2022-07-11 10:00:00"); //UTC时间 -> 北京时间: 2022-02-07 00:00:00

                        //string MessageContent = "**消息类型**: 库存变更 \n\n";
                        //if (DateTime.UtcNow >= start && DateTime.UtcNow <= end)
                        //{
                        //    MessageContent = $"<font color=#FF0000  face=黑体>停止推送Ecinx系统</font> \n\n";
                        //    MessageContent = MessageContent + $"北京时间 7月8日 18:00 ~  7月11日 18:00 停推！";
                        //    _dingdingNotificationJob.SendEcinxERPAlert("魔法灯-停止推送Ecinx系统", MessageContent);
                        //    //CLSLogger.Info("义乌保税仓(暂停服务 9月3日 周五中午12点-9月6日 周五中午12),返回值为空:", $"包裹号：{orderPack.orderPackNo}  inputDto: {inputDto.ToJsonString()} listProductsFromOrder: {listProductsFromOrder.ToJsonString()} listProductInfo: {listProductInfo.ToJsonString()}", LogTags);
                        //    return _return;
                        //}


                        string orderpackSQL = $@"  with cte as (
                             select row_number() over(order by PayTime) as ID,OrderPackNo,Opid,customsPaymentData,CreateTime,OrderID,Status,DeliveryCode,ExportFlag,
                             EcinxResponse,OriginalInfo,paytime,EcinxOrderCode
                             from EcinxOrdersPackTrade where Status = 0 and ExportFlag in (104,105,106,107)
                            ),
                            cteresult as( 
                             select *,NTILE(24) over(order by id )-1 as rowid
                             from cte
                            )
                            select OrderPackNo,Opid,CustomsPaymentData,CreateTime,OrderID,Status,DeliveryCode,ExportFlag,EcinxResponse,OriginalInfo,PayTime,EcinxOrderCode
                            from cteresult where OriginalInfo like '%{ResponseDTO.goodsCode}%'
                            order by PayTime asc
                            ";
                        //var PMSDbContext = new PMSDbContextFactory().CreateDbContext();
                        using (var PMSDbContext = new PMSDbContextFactory().CreateDbContext())
                        {
                            //List<Ecinxorderpack> orderpacklist = PMSDbContext.EcinxOrdersPackTrades.FromSql(orderpackSQL)
                            //                .MapTo<List<Ecinxorderpack>>()
                            //                .ToList();
                            List<Ecinxorderpack> orderpacklist = PMSDbContext.EcinxOrdersPackTrades
                                                                   .FromSql(orderpackSQL)
                                                                   .Select(x => new Ecinxorderpack
                                                                   {
                                                                       OrderPackNo = x.OrderPackNo,
                                                                       Opid = x.Opid,
                                                                       CustomsPaymentData = x.CustomsPaymentData,
                                                                       CreateTime = x.CreateTime,
                                                                       OrderID = x.OrderID,
                                                                       Status = x.Status,
                                                                       DeliveryCode = x.DeliveryCode,
                                                                       ExportFlag = x.ExportFlag,
                                                                       EcinxResponse = x.EcinxResponse,
                                                                       OriginalInfo = x.OriginalInfo,
                                                                       PayTime = x.PayTime,
                                                                       EcinxOrderCode = x.EcinxOrderCode
                                                                   })
                                                                   .ToList();
                            //var stringorderpakno = string.Join("|", orderpacklist.Select(x => x.OrderPackNo).ToArray());
                            if (orderpacklist.Count > 0)
                            {
                                string MessageContent = $"<font color=#FF0000  face=黑体>新库存分配: 先支付先分配原则 </font> \n\n";
                                MessageContent = MessageContent + $" ##### **库存分配结论**: SKU:{ResponseDTO.goodsCode} 可销售数量: {totalinventory} 本次同步参与库存分配，根据分配原则, 参与分配此SKU的缺货订单数量：{orderpacklist.Count}    \n\n";
                                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx库存分配", MessageContent);
                                BondWarehousePackaged(orderpacklist, ResponseDTO);
                            }
                        }

                    }

                }

                List<SyncEcinxStockDTO> dtos = new List<SyncEcinxStockDTO>();
                EcinxInventoryQueryResponse inventoryresult = new EcinxInventoryQueryResponse();

                EcinxInventoryQueryInput inventoryinput = new EcinxInventoryQueryInput();
                inventoryinput.goodsCode = ResponseDTO.goodsCode;
                //inventoryinput.warehouseCode = ResponseDTO.warehouseCode;
                inventoryresult = _WHYC_EcinxClient.QueryInventory(inventoryinput).Result;
                CLSLogger.Info("EcinxAppService1", $"goodcode: {ResponseDTO.goodsCode} QueryInventory:{inventoryresult.ToJsonString()} ", clogTags);

                if (inventoryresult == null)
                {
                    string MessageContent = $"_WHYC_EcinxClient, 返回值：{inventoryresult.ToJsonString()}";
                    //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx查询库存失败_SyncStockChange", MessageContent);
                    CLSLogger.Info("_WHYC_EcinxClient", $"goodcode:{ResponseDTO.goodsCode} Message：{MessageContent} ", clogTags);
                    return _return;
                }
                else
                {

                    if (inventoryresult.result.total == 0)
                    {
                        string MessageContent = $"_WHYC_EcinxClient,SKU:{ResponseDTO.goodsCode} 在Ecinx系统未查到产品库存信息!";
                        //_dingdingNotificationJob.SendEcinxERPAlert("_WHYC_EcinxClient", MessageContent);

                        return _return;
                    }

                    int totalinventory = 0;
                    string expiredate = "";
                    double CostPrice = 0;

                    List<double> CostPriceList = new List<double>();
                    List<string> expiredateList = new List<string>();


                    foreach (var item in inventoryresult.result.rows)
                    {
                        //Filter WarehouseCode
                        if (whycwarehousecode.Contains(item.warehouseCode))
                        {
                            //All canSalesNum
                            totalinventory = totalinventory + item.canSalesNum;
                            if (item.costPrice > 0)
                            {
                                CostPriceList.Add(item.costPrice);
                            }

                            if (item.batches != null)
                            {
                                if (item.batches.Where(t => t.canPickingNum > 0).Select(t => t.expiryDate) != null)
                                {
                                    expiredateList.Add(item.batches.Where(t => t.canPickingNum > 0).Min(t => t.expiryDate));
                                }
                            }
                        }
                    }

                    expiredate = expiredateList.Count > 0 ? expiredateList.Min() : expiredate; //Muti Warehouse get Min
                    CostPrice = CostPriceList.Count > 0 ? CostPriceList.Max() : CostPrice; //Muti Warehosue get Max

                    if (totalinventory > 0 || expiredateList.Count > 0 || CostPriceList.Count > 0)
                    {
                        SyncEcinxStockDTO _ecinxsku = new SyncEcinxStockDTO();
                        _ecinxsku.SKU = ResponseDTO.goodsCode;
                        _ecinxsku.Barcode = ResponseDTO.barCode;
                        _ecinxsku.ExpiryDate = expiredate;
                        //运营还有一个需求 就是  由于188 并发导致的 超卖 需要ecinx同步库存的时候  跟odoo一样 mfderp 需要把 ecinx 可销售库存总数 * 0.9 （向下取整）  
                        //如果可销售总数 = 1 则同步 1
                        //这种方式来避免并发超卖
                        _ecinxsku.StockQty = totalinventory == 1 ? 1 : (int)Math.Floor(totalinventory * 0.9);

                        _ecinxsku.CostPrice = CostPrice;
                        dtos.Add(_ecinxsku);
                    }
                }

                if (dtos.Count > 0)
                {
                    _backgroundJobManager.EnqueueAsync<SyncStockFromEcinxJob, List<SyncEcinxStockDTO>>(dtos, BackgroundJobPriority.BelowNormal, TimeSpan.FromSeconds(3));
                }

            }
            catch (Exception ex)
            {
                string MessageContent = $"Ecinx库存查询失败,返回值:{Response.ToJsonString()}";
                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx库存查询失败", MessageContent);
                CLSLogger.Info("whyc_queryex", $"goodcode: {ResponseDTO.goodsCode} ex: {ex.ToJsonString()} canSalesNum:{result.ToJsonString()} ", clogTags);
                _return = "failed";

            }

            return _return;
        }


        public void createorderfromhhTest(HHCreateOrderResultDTO input)
        {
            _backgroundJobManager.Enqueue<SyncCreateOrderResultFromHH, HHCreateOrderResultDTO>(input, BackgroundJobPriority.High);
        }

        private void BondWarehousePackaged(List<Ecinxorderpack> orderpacnolist, EcinxStockChange ecinxstock)
        {
            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();

            Dictionary<string, string> LogTags = new Dictionary<string, string>
            {
                ["jobid"] = Guid.NewGuid().ToString(),
                ["method"] = "SyncCreateEcinxOder"
            };


            foreach (var orderpacknoObject in orderpacnolist)
            {

                try
                {
                    //EcinxOrdersPackTradeEntity orderPackTradeEntity;
                    //_pmsContext.Database.SetCommandTimeout(6000);
                    //orderPackTradeEntity = _pmsContext.EcinxOrdersPackTrades.FirstOrDefault(x => x.OrderPackNo == orderpacknoObject.OrderPackNo || x.OrderID == orderpacknoObject.OrderPackNo);
                    //if (orderPackTradeEntity == null)
                    //{
                    //    break;
                    //}
                    //var input = orderPackTradeEntity.OriginalInfo.ConvertFromJsonString<EcinxOrderInput>();
                    _pmsContext.Database.SetCommandTimeout(120);

                    var orderPackTradeEntity = _pmsContext.EcinxOrdersPackTrades
                        .FirstOrDefault(x => new[] { x.OrderPackNo, x.OrderID }
                                             .Contains(orderpacknoObject.OrderPackNo));

                    if (orderPackTradeEntity == null)
                        return;

                    var input = orderPackTradeEntity.OriginalInfo.ConvertFromJsonString<EcinxOrderInput>();


                    string warehousecode = "", warehouseName = "";

                    EcinxConfigerData config = EcinxConfigerData.Configuration;
                    ExportResponse _ecinxconfig = config.GetExportByFlag(orderPackTradeEntity.ExportFlag ?? 0);
                    warehousecode = _ecinxconfig.Export.WarehouseCode;
                    warehouseName = _ecinxconfig.Export.WarehouseName;
                    //是否使用供销功能 19/02/2025
                    bool supplySales = config.supplySalesExportFlag.Contains(orderPackTradeEntity.ExportFlag ?? 0);

                    int differenceInDays = 0;
                    DateTime oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                    if (!string.IsNullOrWhiteSpace(orderpacknoObject.PayTime.ToString()))
                    {
                        oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(orderpacknoObject.PayTime.ToString()), timeZoneNZ);
                        DateTime newDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                        TimeSpan ts = newDate - oldDate;
                        differenceInDays = ts.Days;
                    }


                    string MessageContent = $"<font color=#FF0000  face=黑体>新库存分配: 先支付先分配原则(分配失败) </font> \n\n";
                    MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {orderpacknoObject.OrderPackNo}  \n\n";
                    MessageContent += $" ##### **付款时间**: {(orderpacknoObject.PayTime.HasValue ? orderpacknoObject.PayTime.Value.ToString("yyyy-MM-dd HH:mm:ss") : "未知")}  \n\n";
                    MessageContent = MessageContent + $" ##### **归属仓**:  {warehouseName}  \n\n";
                    MessageContent = MessageContent + $"**缺货天数**: **_<font face=华云彩绘 color=#FF0000> {differenceInDays} </font>_**  \n\n";



                    EcinxOrder eorder = new EcinxOrder();

                    eorder.deduction = input.deduction;
                    eorder.discount = input.discount;
                    eorder.freight = input.freight;
                    eorder.orderTime = input.orderTime;
                    eorder.platformOrderCode = input.platformOrderCode;
                    eorder.declareOrderCode = input.declareOrderCode;

                    //// 阿狸： CQ奶粉普通线 82,106 渠道，指定Ecinx仓库，其它不指定Ecinx 仓库  2025.11.22
                    List<int> mfdyc_export = new List<int> { 82, 106 };
                    eorder.warehouseCode = mfdyc_export.Contains(orderPackTradeEntity.ExportFlag ?? 0) ? warehousecode : "";

                    eorder.receiver = input.receiver;
                    eorder.receiverTel = input.receiverTel;
                    eorder.receiverProvince = input.receiverProvince;
                    eorder.receiverCity = input.receiverCity;
                    eorder.receiverArea = input.receiverArea;
                    eorder.receiverAddress = input.receiverAddress;
                    eorder.tax = input.tax;
                    eorder.type = input.type;
                    // 提交供销代码
                    if (supplySales)
                    {
                        eorder.providerCode = _ecinxconfig.Export.providerCode;
                        eorder.type = 2;
                    }
                    eorder.details = input.details;
                    eorder.customsPlatformCode = input.customsPlatformCode;
                    eorder.customsPlatformName = input.customsPlatformName;

                    eorder.purchaser = input.purchaser;
                    eorder.purchaserCardNo = input.purchaserCardNo;
                    eorder.purchaserCardType = input.purchaserCardType;
                    eorder.purchaserTel = input.purchaserTel;

                    eorder.payments = input.payments;
                    eorder.expressSheetInfo = input.expressSheetInfo;
                    eorder.presell = input.presell;
                    eorder.planDeliveryTime = input.planDeliveryTime;

                    eorder.purchaserCardFrontPic = input.GetType().GetProperty("purchaserCardFrontPic") != null ? input.purchaserCardFrontPic : "";
                    eorder.purchaserCardBackPic = input.GetType().GetProperty("purchaserCardBackPic") != null ? input.purchaserCardBackPic : "";

                    eorder.ExportFlag = orderpacknoObject.ExportFlag;


                    EcinxResponse result = null;

                    EcinxClientV2 ecinxv2 = new EcinxClientV2();
                    result = ecinxv2.CreateOrderV2(orderPackTradeEntity.ExportFlag ?? 0, eorder).Result;


                    if (!result.success)
                    {
                        MessageContent = MessageContent + $" ##### **创建订单失败原因**:  **_<font face=华云彩绘 color=#FF0000> {result.errorMsg} </font>_** \n\n";
                        //禁用 24.10.2024
                        //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx 创建订单失败", MessageContent);
                        CLSLogger.Info("Ecinx创建订单失败", $"包裹号：{orderpacknoObject.OrderPackNo}  失败详情:{result.ToJsonString()} ", LogTags);

                        ecinpushorderLog.OrderPackNo = "N/A";
                        ecinpushorderLog.WarningLevel = 0;  //WarningLevel =0: 普通告警; WarningLevel =10: 需要客服处理;
                        ecinpushorderLog.LackStockDays = -1;
                        ecinpushorderLog.MessageType = "库存变化回调";
                        ecinpushorderLog.FaildReason = MessageContent;
                        ecinpushorderLog.MessageDetail = "";
                        _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                        _pmsContext.SaveChanges();


                        continue;
                    }
                    else
                    {
                        MessageContent = $"<font color=#228b22  face=黑体>新库存分配: 先支付先分配原则(分配成功) </font> \n\n";
                        MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {orderpacknoObject.OrderPackNo}  \n\n";
                        MessageContent += $" ##### **付款时间**: {(orderpacknoObject.PayTime.HasValue ? orderpacknoObject.PayTime.Value.ToString("yyyy-MM-dd HH:mm:ss") : "未知")}  \n\n";
                        MessageContent = MessageContent + $" ##### **归属仓**:  {warehouseName}  \n\n";
                        MessageContent = MessageContent + $" ##### **分配的SKU**:  {ecinxstock.goodsCode}  \n\n";
                        MessageContent = MessageContent + $"**未处理天数**: **_<font face=华云彩绘 color=#FF0000> {differenceInDays} </font>_**  \n\n";
                        //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx 分配缺货订单成功", MessageContent);
                    }

                }
                catch (Exception ex)
                {
                    CLSLogger.Error("Ecinx下单发生异常 Exception ", "异常：" + ex.Message, LogTags);
                    throw new Exception($"Ecinx查询接口失败，订单号：{ex.Message}");
                }

            }
        }

        public void TestEcinxInventory(EcinxOrderInput inputDto)
        {

            List<SyncEcinxStockDTO> dtos = new List<SyncEcinxStockDTO>();

            List<string> warehousecode = new List<string> { "CQZGC", "YOCHYDF", "CQBSC", "YWOYBSCNFMX", "YWOYBSCYWBS", "YWOYBSCNFKX", "YWOYBSCBJPZX", "NSFYWZYDFC", "QDXHZYC", "NBZ005", "YWOYDMC", "ZX-YWZ007", "QDXHCNC", "YWOYBSCBLP", "YWOYBSCBSNM" }; //, 


            EcinxInventoryQueryResponse inventoryresult = new EcinxInventoryQueryResponse();

            ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
            ECinxClient _ecinxClient = new ECinxClient(cacheManager, ECinxClient.EcinxAccountEnum.CQBSC);

            EcinxInventoryQueryInput inventoryinput = new EcinxInventoryQueryInput();
            inventoryinput.goodsCode = "MKYWFTFAS1A";

            inventoryresult = _ecinxClient.QueryInventory(inventoryinput).Result;

            if (inventoryresult == null)
            {
                string MessageContent = $"Ecinx查询库存失败, 返回值：{inventoryresult.ToJsonString()}";
               

            }
            else
            {

                if (inventoryresult.result.total == 0)
                {
                    string MessageContent = $"Ecinx库存查询失败_产品原因,SKU:{inventoryinput.goodsCode} 在Ecinx系统未查到产品库存信息!";

                    return;
                }

                int totalinventory = 0;
                string expiredate = "";
                double CostPrice = 0;

                List<double> CostPriceList = new List<double>();
                List<string> expiredateList = new List<string>();


                foreach (var item in inventoryresult.result.rows)
                {
                    //Filter WarehouseCode
                    if (warehousecode.Contains(item.warehouseCode))
                    {
                        //All canSalesNum
                        totalinventory = totalinventory + item.canSalesNum;
                        if (item.costPrice > 0)
                        {
                            CostPriceList.Add(item.costPrice);
                        }

                        if (item.batches != null && item.canSalesNum > 0)
                        {
                            expiredateList.Add(item.batches.Where(t => t.inventory > 0).Min(t => t.expiryDate));
                        }
                    }
                }

                expiredate = expiredateList.Count > 0 ? expiredateList.Min() : expiredate; //Muti Warehouse get Min
                CostPrice = CostPriceList.Count > 0 ? CostPriceList.Max() : CostPrice; //Muti Warehosue get Max

                if (totalinventory > 0 || expiredateList.Count > 0 || CostPriceList.Count > 0)
                {
                    SyncEcinxStockDTO _ecinxsku = new SyncEcinxStockDTO();
                    _ecinxsku.SKU = inventoryinput.goodsCode;
                    //_ecinxsku.Barcode = inventoryinput.barCode;
                    _ecinxsku.ExpiryDate = expiredate;
                    _ecinxsku.StockQty = totalinventory;
                    _ecinxsku.CostPrice = CostPrice;
                    dtos.Add(_ecinxsku);
                }
            }

            if (dtos.Count > 0)
            {
                _backgroundJobManager.EnqueueAsync<SyncStockFromEcinxJob, List<SyncEcinxStockDTO>>(dtos, BackgroundJobPriority.High, TimeSpan.FromSeconds(3));
            }

        }


        //Ecinx JOB Test
        public void TestCreateEcinxJObOrder(EcinxOrderInput inputDto)
        {
            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();

            OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x =>
                    x.orderPackNo == inputDto.platformOrderCode || x.orderID == inputDto.platformOrderCode);
            if (orderPack == null)
            {
                //string message = $"Ecinx仓创建订单失败, 未找到此包裹：{inputDto.platformOrderCode} ";
                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx 创建订单失败", message);
                //CLSLogger.Info("Ecinx仓创建订单失败", $"未找到此包裹：{inputDto.platformOrderCode} ", LogTags);
                //hangfire retry
                throw new Exception($"义乌保税仓下单时无orderpack,将重试 订单号：{inputDto.platformOrderCode} ");
            }
            int differenceInDays = 0;
            DateTime oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
            if (!string.IsNullOrWhiteSpace(orderPack.opt_time.ToString()))
            {
                oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(orderPack.opt_time.ToString()), timeZoneNZ);
                DateTime newDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                TimeSpan ts = newDate - oldDate;
                differenceInDays = ts.Days;
            }


        }

   
        
        public void TestChongQingOrderFirstExpress()
        {
            var dto = new GMOrderFirstExpressDTO
            {
                ORDERS = new List<GMOrderExpressDTO> { new GMOrderExpressDTO { ORDERNO = "4494700000123", EXPRESSNO = "express001" } }
            };

            //form里面每一个name都要加进来
            string postString = $"me=orderFirstExpress&data={dto.ToJsonString()}";
            byte[] postData = Encoding.UTF8.GetBytes(postString);
            string url = "http://61.128.133.130:8292/T3/ownererp.jsp";
            using (System.Net.WebClient webClient = new System.Net.WebClient())
            {
                webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                byte[] responseData = webClient.UploadData(url, "POST", postData);
                string srcString = Encoding.UTF8.GetString(responseData);
                var returnObject = JObject.Parse(srcString);
                var returnOutput = returnObject.ToObject<GMOrderFirstExpressOutputDTO>();
            }


        }

        public CLSSearchLogOutputDto CLSSearchLog(string queryString)
        {
            var output = CLSLogger.SearchLog(new CLSSearchLogInputDto
            {
                startTime = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss"),
                endTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                queryString = queryString
            });
            return output;
        }


        // public void  cmqpushteambitiontes()
        // {
        //     string originalOrderCode = "2040800190330-2-2",
        //            resultMsg = "测试:相关MFD平台SKU: MKYWSTFAPN31A 请尽快备货, 系统会在:2021-08-12 11:26:21【北京时间】再次尝试向【义乌保税仓】推送此订单!";

        //     Dictionary<string, string> LogTags = new Dictionary<string, string>();
        //     LogTags["method"] = "CreateOrder";
        //     LogTags["orderpackno"] = originalOrderCode;

        //     List<string> messageTags = new List<string> { "YiWuCreateOrderJob" };

        //     TeambitionMessage teambitionmesage = new TeambitionMessage
        //     {
        //         OrderNo = originalOrderCode, //订单号
        //         Warehouse = "义乌保税仓",
        //         Title = "系统中商品编号GJI21060300001对应库存不足.",
        //         TriggerTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"),
        //         MessageContent = resultMsg
        //     };

        //     TencentCMQProxy.PublishTopicMessage(TCConstants.CMQTopics.TOPIC_TEAMBITIONNOTIFICATION, teambitionmesage, messageTags, LogTags);

        // }

        public void testsyncsku()
        {
            
        }




        public async Task<EcinxResponse> TestCreateEcinxAPIOrder(EcinxOrderInput inputDto)
        {

            string originalInputString = inputDto.ToJsonString();

            OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x =>
                    x.orderPackNo == inputDto.platformOrderCode || x.orderID == inputDto.platformOrderCode //orderpacno
                );

            bool EcinxorderPackTradeExists = _pmsContext.EcinxOrdersPackTrades.Any(x => x.Opid == orderPack.opID);

            EcinxOrdersPackTradeEntity EcinxorderPackTrade = new EcinxOrdersPackTradeEntity();
            //if (EcinxorderPackTradeExists)
            //{
            //    EcinxorderPackTrade.Opid = orderPack.opID;
            //    EcinxorderPackTrade.OrderID = orderPack.orderID;
            //    EcinxorderPackTrade.OrderPackNo = !string.IsNullOrWhiteSpace(orderPack.orderPackNo) ? orderPack.orderPackNo : orderPack.orderID; // orderpackno is nul then use orderid
            //    EcinxorderPackTrade.CustomsPaymentData = !string.IsNullOrWhiteSpace(inputDto.customsPaymentData.ToString()) ? inputDto.customsPaymentData.ToJsonString() : "";
            //    EcinxorderPackTrade.CreateTime = DateTime.UtcNow;
            //    EcinxorderPackTrade.Status = 0;
            //    // 0: Created Ecinx Success 
            //    // 1: Ordered Ecinx Success
            //    // 2: Ordered Ecinx Failed
            //    // 3: order out
            //    // 4: 已配货
            //    // -1: 已删除
            //    EcinxorderPackTrade.ExportFlag = orderPack.exportFlag;
            //    EcinxorderPackTrade.OriginalInfo = originalInputString;
            //    EcinxorderPackTrade.PayTime = DateTime.Parse(inputDto.orderTime);// TimeZoneInfo.ConvertTimeFromUtc(, timeZoneCN);
            //                                                                     //!string.IsNullOrWhiteSpace(inputDto.payments[0].payTime) ? DateTime.Parse(inputDto.payments[0].payTime) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneCN); 
            //    _pmsContext.EcinxOrdersPackTrades.Update(EcinxorderPackTrade);
            //    _pmsContext.SaveChanges();
            //}
            //else
            //{
            //    EcinxorderPackTrade = new EcinxOrdersPackTradeEntity
            //    {
            //        Opid = orderPack.opID,
            //        OrderID = orderPack.orderID,
            //        OrderPackNo = !string.IsNullOrWhiteSpace(orderPack.orderPackNo) ? orderPack.orderPackNo : orderPack.orderID, // orderpackno is nul then use orderid
            //        CustomsPaymentData = !string.IsNullOrWhiteSpace(inputDto.customsPaymentData.ToString()) ? inputDto.customsPaymentData.ToJsonString() : "",
            //        CreateTime = DateTime.UtcNow,
            //        OriginalInfo = originalInputString,
            //        Status = 0,  // 0: Created Ecinx Success 1: Ordered  Ecinx Success 2: Ordered  Ecinx Failed 3: order out ; 4: packaged; -1: deleted
            //        ExportFlag = orderPack.exportFlag,
            //        PayTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneCN)
            //    };
            //    _pmsContext.EcinxOrdersPackTrades.Add(EcinxorderPackTrade);
            //    _pmsContext.SaveChanges();
            //}

            string warehousecode = "";

            EcinxConfigerData config = EcinxConfigerData.Configuration;
            ExportResponse _ecinxconfig = config.GetExportByFlag(orderPack.exportFlag?? 0);
            warehousecode = _ecinxconfig.Export.WarehouseCode;
            // 生成成功通过回调函数获取真实状态
            EcinxResponse result = null;

            EcinxOrder eorder = new EcinxOrder();
            eorder.deduction = inputDto.deduction;
            eorder.discount = inputDto.discount;
            eorder.freight = inputDto.freight;
            eorder.orderTime = inputDto.orderTime;
            eorder.platformOrderCode = inputDto.platformOrderCode;
            eorder.declareOrderCode = inputDto.declareOrderCode;
            //// 阿狸： CQ奶粉普通线 82,106 渠道，指定Ecinx仓库，其它不指定Ecinx 仓库  2024.11.22
            List<int> mfdyc_export = new List<int> { 82, 106 };//82 MFD //106 YC
            eorder.warehouseCode = mfdyc_export.Contains(orderPack.exportFlag ?? 0) ? warehousecode : "";

            eorder.receiver = inputDto.receiver;
            eorder.receiverTel = inputDto.receiverTel;
            eorder.receiverProvince = inputDto.receiverProvince;
            eorder.receiverCity = inputDto.receiverCity;
            eorder.receiverArea = inputDto.receiverArea;
            eorder.receiverAddress = inputDto.receiverAddress;
            eorder.tax = inputDto.tax;
            eorder.type = inputDto.type;

            eorder.details = inputDto.details;
            eorder.customsPlatformCode = inputDto.customsPlatformCode;
            eorder.customsPlatformName = inputDto.customsPlatformName;

            eorder.purchaser = inputDto.purchaser;
            eorder.purchaserCardNo = inputDto.purchaserCardNo;
            eorder.purchaserCardType = inputDto.purchaserCardType;
            eorder.purchaserTel = inputDto.purchaserTel;

            eorder.payments = inputDto.payments;
            eorder.expressSheetInfo = inputDto.expressSheetInfo;
            eorder.presell = inputDto.presell;
            eorder.planDeliveryTime = inputDto.planDeliveryTime;

            eorder.purchaserCardFrontPic = inputDto.GetType().GetProperty("purchaserCardFrontPic") != null ? inputDto.purchaserCardFrontPic : "";
            eorder.purchaserCardBackPic = inputDto.GetType().GetProperty("purchaserCardBackPic") != null ? inputDto.purchaserCardBackPic : "";

            //orderInput.appkey = _config.Appkey;
            //orderInput.method = method;
            //orderInput.shopCode = _config.ShopCode;  // 店铺编码
            //orderInput.warehouseCode = _config.WarehouseCode; // 重庆保税仓-迈集客BS
            //orderInput.notifyUrl = _config.NotifyUrl; // 异步通知

            EcinxClientV2 ecinxv2 = new EcinxClientV2();
            MagicLamp.PMS.Proxy.ECinx.Models.EcinxResponse response = await ecinxv2.CreateOrderV2(106, eorder);

            //switch (orderPack.exportFlag)
            //{
            //    case 93: //渝欧
            //        result = _YOBSClient.CreateOrder(eorder).Result;
            //        break;
            //    case 81:  //重庆仓直邮
            //    case 82:
            //    case 83:
            //        result = _CQZGCClient.CreateOrder(eorder).Result;
            //        break;
            //    case 66:
            //        result = _CQBSCClient.CreateOrder(eorder).Result;
            //        break;

            //    //义乌仓
            //    case 79:
            //        result = _YWCBSClient.CreateOrder(eorder).Result;
            //        break;
            //    case 84:
            //        result = _YWCNFKXClient.CreateOrder(eorder).Result;
            //        break;
            //    case 85:
            //        result = _YWBSCClient.CreateOrder(eorder).Result;
            //        break;
            //    case 88:
            //        result = _YWCJPZXClient.CreateOrder(eorder).Result;
            //        break;
            //    case 95:
            //        result = _NSFYWCClient.CreateOrder(eorder).Result;
            //        break;
            //    case 94:
            //        result = _QDXHZYCClient.CreateOrder(eorder).Result;
            //        break;
            //}



            //ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
            //ECinxClient testEclient = new ECinxClient(cacheManager, ECinxClient.EcinxAccountEnum.CQBSC);
            return response;

        }




        public void SyncEcinxOrderStatus()
        {
            string ClogTagType = "SYNCEcinxDeliveryQuerySparkTest";
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = ClogTagType
            };

           EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();

            //93 : 渝欧保税; 81,82,83: 重庆奶粉极速仓； 66: 重庆仓直邮  //79，84，85, 88 义乌保税 ,94; 共享菜园:96: 义乌大货:97
            var ExportFlags = new List<int> { 93, 81, 82, 83, 66, 79, 84, 85, 88, 95, 94, 96, 97 };
            var vaildstatus = new List<int> { 1, 4 };


            var orderPacks = _heerpContext.OrderPacks.Where(x => x.status == 3 && ExportFlags.Contains(x.exportFlag.GetValueOrDefault())).Select(x => x.orderPackNo).ToList();
            //var _orderpacks = orderPacks.Select(x => x.orderPackNo).ToList();
            var EcinxorderPacks = _pmsContext.EcinxOrdersPackTrades.Where(x => x.Status == 1).Select(x => x.OrderPackNo).ToList();
            List<string> UnionorderPacksList = orderPacks.Union(EcinxorderPacks).ToList();


            var ecinxorderpack = _pmsContext.EcinxOrdersPackTrades.Where(x => UnionorderPacksList.Contains(x.OrderPackNo) && vaildstatus.Contains(x.Status) ).Select(x => new
            {
                x.ExportFlag,
                x.OrderPackNo
            }).ToList();

            CLSLogger.Info("Ecinx出库查询包裹", ecinxorderpack.ToJsonString(), clogTags);

        }



        public void TestSyncProductToFlux()
        {

            var sendProducts = new List<MagicLamp.Flux.API.Models.Product> ();

            var pro = new MagicLamp.Flux.API.Models.Product
            {
                CustomerID = "AU",
                Active_Flag = "1",
                SKU = "ANZ1001",
                ReservedField02 = "ANZ1001",
                Descr_C = "ABC",
                Descr_E = "ABC",
                GrossWeight =  0,
                NetWeight =  0,
                SKULength =  0,
                SKUHigh =  0,
                SKUWidth =  0,
                Alternate_SKU1 = "ANZ1001",
                Alternate_SKU2 = "ANZ1001",
                //Alternate_SKU3保留
                Alternate_SKU4 = "ANZ1001",
                ReservedField01 = "ANZ1001",
                ReservedField09 = "ANZ1001",
                ReservedField08 = "ANZ1001",
                SKU_Group9 = "ANZ1001",
            };

            sendProducts.Add(pro);


            _backgroundJobManager.EnqueueAsync<SyncProductToFluxJob, List<MagicLamp.Flux.API.Models.Product>>(sendProducts, BackgroundJobPriority.High, TimeSpan.FromSeconds(3));
        }








        public void SwapSKUForItemCode() {

            CreateSupplierOrderInput testInput = new CreateSupplierOrderInput
            {
                Orders = new List<SupplierOrder>()
            };

            testInput.Orders.Add(new SupplierOrder
            {
                orderSource = "73",
                orderCode = "E202104231410100666042411211HZ",
                receiverName = "熊大",
                mobile = "13416648623",
                province = "江苏省",
                city = "宿迁市",
                district = "泗阳县",
                receiverAddress = "雨润广场",
                receiverZip = "223700",
                //payCompanyType = ((int)PayCompanyTypeEnum.银盈通).ToString(),
                payCompanyType = "12",
                payNumber = "210412141011000100",
                payDate = "2021-04-12 14:10:28",
                payment = "1066.00",
                couponAmount = "0.0",
                orderTotalAmount = "1066.00",
                orderGoodsAmount = "977.08",
                orderTaxAmount = "88.92",
                feeAmount = "0.00",
                insureAmount = "0.0",
                paperType = "01",
                name = "张**",

                paperNumber = "310123148505132118",
                companyCode = "",
                receptUrl = "",
                orderDate = "2021-04-12 14:10:10",
                holdCode = "",
                clientId = "20",
                outOrderCode = "E20210412141010066604241",
                OrderDtls = new List<OrderDetail> { new OrderDetail
                {
                    commodityCode = "MKYWSTFA41A",
                    commodityName = "【普通线】A2 白金系列 1段 900g*1罐(单罐)",
                    qty = "1",
                    price = "977.08",
                    amount = "977.08"
                }}
            });

            var inputOrder = testInput.Orders.FirstOrDefault();

            var listProductsFromOrder = inputOrder.OrderDtls.Select(x => x.commodityCode).ToList();

            List<ProductEntity> listProductInfo = _pmsContext.Products
                   .Where(x => listProductsFromOrder.Contains(x.BondedWarehouseItemCode) || listProductsFromOrder.Contains(x.SKU))
                   .ToList();


            foreach (var item in testInput.Orders.FirstOrDefault().OrderDtls)
            {
                var productInfo = listProductInfo.FirstOrDefault(x => x.BondedWarehouseItemCode.StandardizeSKU() == item.commodityCode.StandardizeSKU() || x.SKU.StandardizeSKU() == item.commodityCode.StandardizeSKU());

                if (productInfo == null)
                {
                    var orderno = testInput.Orders.FirstOrDefault().orderPackNo; //订单号
                }
                else
                {
                    item.commodityCode = productInfo.BondedWarehouseItemCode;
                    item.commodityName = productInfo.ChineseName;
                }
            }



        }

    }
}

