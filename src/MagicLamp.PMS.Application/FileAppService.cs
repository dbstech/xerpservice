﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.DTOs.Enums;
using Abp.UI;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class FileAppService : PMSCrudAppServiceBase<FileEntity, FileDTO, FileDTO, long>
    {

        private IRepository<FileEntity, long> fileRepository;
        public FileAppService(IRepository<FileEntity, long> _fileRepository) : base(_fileRepository)
        {
            fileRepository = _fileRepository;
        }


        [DisableRequestSizeLimit]
        public virtual FileDTO Upload(string type, IFormFile file)
        {
            string filePath = Ultities.CreateDirectory("App_Data/FileUploads/");
            string fileName = Guid.NewGuid().ToString();
            string fullPath = Path.Combine(filePath, fileName);
            string extension = Path.GetExtension(file.FileName);

            FileTypeEnum fileType = FileTypeEnum.Unknow;
            if (!Enum.TryParse<FileTypeEnum>(type, out fileType))
            {
                throw new UserFriendlyException("Invalid type parameter, pls check.");
            }

            string key = $"{PMSConsts.ProductImgKeyPrefix}{fileType}-{fileName}{extension}";

            //save File
            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }

            //upload to aliyun oss
            bool uploadResult = AliyunOSSProxy.Instance.UploadFile(fullPath, key);

            if (!uploadResult)
            {
                throw new UserFriendlyException("上传失败，请稍后重试");
            }

            //delete temp file
            System.IO.File.Delete(fullPath);

            //save to db
            FileEntity File = new FileEntity
            {
                CreationTime = DateTime.UtcNow,
                FileType = fileType.ToString(),
                OriginalPath = key
            };
            long FileId = fileRepository.InsertAndGetId(File);
            var FileDTO = File.MapTo<FileDTO>();
            FileDTO.FileId = FileId;

            return FileDTO;
        }


        /// <summary>
        ///下载身份证压缩包
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DownloadTemplateFile(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new UserFriendlyException("Invalid name parameter");
            }

            try
            {

                string path = Path.Combine("App_Data", "Templates", name);
                string physicalPath = Ultities.GetPhysicalPath(path);
                CLSLogger.Info("DownloadTemplateFile请求", $"路径：{physicalPath}，是否存在：{File.Exists(physicalPath)}");
                var result = new PhysicalFileResult(physicalPath, Ultities.GetContentTypeByFilePath(name));
                result.FileDownloadName = name;
                return result;

            }
            catch (Exception ex)
            {
                CLSLogger.Error("DownloadTemplateFile下载模板发生异常", ex);
                throw new UserFriendlyException("下载模板发生异常");
            }

        }

    }


}

