﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class CarrierAppService : PMSCrudAppServiceBase<FreightsEntity, FreightsDTO, FreightsDTO, string>
    {
        private IRepository<FreightsEntity, string> freightsRepository;

        public CarrierAppService(IRepository<FreightsEntity, string> _freightsRepository) : base(_freightsRepository)
        {
            freightsRepository = _freightsRepository;
        }
    }
}
