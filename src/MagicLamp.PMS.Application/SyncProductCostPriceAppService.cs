using MagicLamp.PMS.Entities;
using MagicLamp.PMS.DTOs;
using Abp.Domain.Repositories;
using MagicLamp.PMS.Infrastructure.Extensions;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Abp.BackgroundJobs;
using MagicLamp.XERP.BackgroundJob;
using System.Collections.Generic;
using Abp.Extensions;

namespace MagicLamp.PMS
{
    public class SyncProductCostPriceAppService : PMSCrudAppServiceBase<SyncProductCostPriceEntity, SyncProductCostPriceDTO, SyncProductCostPriceFilterDTO, long>
    {
        private IRepository<SyncProductCostPriceEntity, long> _syncProductCostRepository;
        private IRepository<ProductEntity, string> _productRepository;
        private IBackgroundJobManager _backgroundJobManager;

        public SyncProductCostPriceAppService(
            IRepository<SyncProductCostPriceEntity, long> syncProductCostRepository,
            IRepository<ProductEntity, string> productRepository,
            IBackgroundJobManager backgroundJobManager) : base(syncProductCostRepository)
        {
            _syncProductCostRepository = syncProductCostRepository;
            _productRepository = productRepository;
            _backgroundJobManager = backgroundJobManager;
        }

        public override PagedQueryOutputBaseDTO<SyncProductCostPriceDTO> GetList(PagedQueryInputBaseDTO<SyncProductCostPriceFilterDTO> input)
        {
            IQueryable<SyncProductCostPriceEntity> query = base.BuildQuery(input)
                            .Include(x => x.Product)
                            .ThenInclude(x => x.Brand);

            if (input.Filter != null)
            {
                if (input.Filter.Display == "latest")
                {
                    query = query.GroupBy(x => x.SKU, (key, g) => g.OrderByDescending(x => x.CreationTime).FirstOrDefault());
                }
                if (input.Filter.CreationTimeBegin.HasValue)
                {
                    query = query.Where(x => x.CreationTime >= input.Filter.CreationTimeBegin);
                }
                if (input.Filter.CreationTimeEnd.HasValue)
                {
                    query = query.Where(x => x.CreationTime <= input.Filter.CreationTimeEnd);
                }
                if (!input.Filter.Barcode.IsNullOrWhiteSpace())
                {
                    query = query.Where(x => x.Product.Barcode1 == input.Filter.Barcode);
                }
                if (!input.Filter.ProductId.IsNullOrWhiteSpace())
                {
                    query = query.Where(x => x.SKU == input.Filter.ProductId);
                }
                if (input.Filter.BrandId.HasValue)
                {
                    query = query.Where(x => x.Product.Brand.Id == input.Filter.BrandId);
                }
            }

            var output = base.PaginateQuery(query, input);

            return output;
        }

        //called by pushorder in Naky
        public void SaveOrderProductCost(List<OrderPackDetailProductCostDTO> listCost)
        {
            _backgroundJobManager.EnqueueAsync<SaveOrderPackDetailProductCostJob, List<OrderPackDetailProductCostDTO>>(listCost, BackgroundJobPriority.Low);
        }
    }
}