﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Configs;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Proxy;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class OrderLogAppService : PMSCrudAppServiceBase<OrderLogEntity, OrderLogDTO, OrderLogDTO, int>
    {
        private const string ClogTagType = "OrderLog";

        private IRepository<OrderLogEntity, int> orderLogRepository;
        private IRepository<OrderPackEntity, int> orderPackRepository;
        private IRepository<OrderEntity, int> orderRepository;

        public OrderLogAppService(IRepository<OrderLogEntity, int> _orderLogRepository,
            IRepository<OrderPackEntity, int> _orderPackRepository,
            IRepository<OrderEntity, int> _orderRepository) : base (_orderLogRepository)
        {
            orderLogRepository = _orderLogRepository;
            orderPackRepository = _orderPackRepository;
            orderRepository = _orderRepository;
        }

        private string GetFlagStatus(int flag)
        {
            switch (flag)
            {
                case 0:
                    return "订单处理环节";
                case 1:
                    return "客服跟踪环节";
                case 2:
                    return "缺货跟踪环节";
                case 3:
                    return "破损丢失环节";
                case 4:
                    return "退补款环节";
                case 5:
                    return "重发环节";
                default:
                    return "未知状态";
            }
        }
        public List<OrderLogDTO> GetOrderLog(OrderPackDTO dto)
        {
            try
            {
                var orderPackInfo = orderPackRepository.FirstOrDefault(x => x.orderPackNo == dto.orderPackNo);

                var listOrderLog = orderLogRepository.GetAll().Where(x => x.OPID == orderPackInfo.opID).ToList();
                List<OrderLogDTO> result = listOrderLog.MapTo<List<OrderLogDTO>>();

                //PictureAddressConfig config = Ultities.GetConfig<PictureAddressConfig>();
                //TODO: config for PictureAddressConfig production
                PictureAddressConfig config = new PictureAddressConfig
                {
                    Url = "https://xerp-next.aladdin.nz/"
                };
                var url = config.Url;
                for (int i = 0; i < result.Count(); i++)
                {
                    result[i].status = GetFlagStatus(result[i].flag);
                    if (result[i].picPath != null)
                    {
                        result[i].picPath = url + result[i].picPath;
                    }
                }
                result = result.OrderByDescending(x => x.OptDate).ToList();

                return result;
            }
            catch (Exception ex)
            {
                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = ClogTagType;
                tags["trackId"] = "GetOrderLog";
                tags["orderpackno"] = dto.orderPackNo;
                CLSLogger.Error("获取操作记录发生异常", ex.Message, tags);
                throw new RequestValidationException($"获取操作记录发生异常，失败原因：{ex.Message}");
            }
        }

        public void AddLog(OrderPackDTO dto)
        {
            try
            {
                var orderPackInfo = orderPackRepository.FirstOrDefault(x => x.orderPackNo == dto.orderPackNo);
                if (orderPackInfo == null)
                {
                    throw new RequestValidationException("Cannot find any order pack");
                }

                var orderInfo = orderRepository.FirstOrDefault(x => x.orderid == orderPackInfo.orderID);
                if (orderInfo == null)
                {
                    throw new RequestValidationException("Cannot find any order");
                }

                var orderLogInfo = new OrderLogEntity
                {
                    OID = orderInfo.oid,
                    OPID = orderPackInfo.opID,
                    OptDate = DateTime.UtcNow,
                    Comments = dto.comment,
                    //1: 客服跟踪环节
                    flag = (int)OrderLogFlagEnum.客服跟踪环节,
                    relFlagTitle = "status",
                    relFlagVal = orderPackInfo.status,
                    relFlag1Title = "missflag",
                    relFlag1Val = orderPackInfo.missFlag,
                    relFlag2Title = "refundflag",
                    relFlag2Val = orderPackInfo.refundFlag
                };

                orderLogRepository.InsertAsync(orderLogInfo);

                //缺货未通知
                if (dto.missFlag == 1)
                {
                    orderPackInfo.missFlag = 1;
                    orderPackInfo.missFlagTime = DateTime.UtcNow;
                }
                //缺货已通知
                if (dto.missFlag == 2)
                {
                    orderPackInfo.missFlag = 2;
                    orderPackInfo.missFlagTime = DateTime.UtcNow;
                }
            }
            catch (Exception ex)
            {
                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = ClogTagType;
                tags["trackId"] = "AddLog";
                tags["orderpackno"] = dto.orderPackNo;
                CLSLogger.Error("新增操作记录发生异常", ex.Message, tags);
                throw new RequestValidationException($"新增操作记录发生异常，失败原因：{ex.Message}");
            }
        }
    }
}
