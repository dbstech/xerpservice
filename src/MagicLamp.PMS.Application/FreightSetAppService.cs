using Abp.Application.Services;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class FreightSetAppService : PMSCrudAppServiceBase<FreightSetEntity, FreightSetDTO, FreightSetDTO, string>
    {
        private IRepository<FreightSetEntity, string> _freightSetRepository;
        public FreightSetAppService(IRepository<FreightSetEntity, string> freightSetRepository) : base(freightSetRepository)
        {
            _freightSetRepository = freightSetRepository;
        }
    }
}

