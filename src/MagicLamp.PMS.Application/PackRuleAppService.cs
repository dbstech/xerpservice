using MagicLamp.PMS.Entities;
using MagicLamp.PMS.DTOs;
using Abp.Domain.Repositories;
using System.Linq;

namespace MagicLamp.PMS
{
    public class PackRuleAppService : PMSCrudAppServiceBase<PackRuleEntity, PackRuleDTO, PackRuleDTO, int>
    {
        private IRepository<PackRuleEntity, int> packRuleRepository;
        private IRepository<ExportFlagEntity, int> exportFlagRepository;
        private IRepository<CustomKindEntity, string> customKindRepository;
        public PackRuleAppService(
            IRepository<PackRuleEntity, int> _packRuleRepository,
            IRepository<ExportFlagEntity, int> _exportFlagRepository,
            IRepository<CustomKindEntity, string> _customKindRepository
        ) : base(_packRuleRepository)
        {
            packRuleRepository = _packRuleRepository;
            exportFlagRepository = _exportFlagRepository;
            customKindRepository = _customKindRepository;
        }

        public override PackRuleDTO Create(PackRuleDTO input)
        {
            //TODO: validate exportflag and ckind exists
            return base.Create(input);
        }
        public override PagedQueryOutputBaseDTO<PackRuleDTO> GetList(PagedQueryInputBaseDTO<PackRuleDTO> input)
        {
            var exportFlags = exportFlagRepository.GetAllList();
            var customKinds = customKindRepository.GetAllList();

            var output = base.GetList(input);
            output.Data = output.Data.Select(packRule =>
            {
                var exportFlag = exportFlags.FirstOrDefault(x => x.Flag == packRule.exportFlag);
                var customKind = customKinds.FirstOrDefault(x => x.kind == packRule.cKind);

                packRule.exportFlagName = exportFlag != null ? exportFlag.Name : null;
                packRule.cKindName = customKind != null ? customKind.name : null;

                return packRule;
            }).ToList();
            return output;
        }

        public override PackRuleDTO Update(PackRuleDTO input)
        {
            var output = base.Update(input);

            // TODO: DRY, refactor 
            var exportFlags = exportFlagRepository.GetAllList();
            var customKinds = customKindRepository.GetAllList();
            var exportFlag = exportFlags.FirstOrDefault(x => x.Flag == output.exportFlag);
            var customKind = customKinds.FirstOrDefault(x => x.kind == output.cKind);

            output.exportFlagName = exportFlag != null ? exportFlag.Name : null;
            output.cKindName = customKind != null ? customKind.name : null;
            return output;
        }

        public override bool Delete(string[] ids)
        {
            return base.Delete(ids);
        }
    }
}