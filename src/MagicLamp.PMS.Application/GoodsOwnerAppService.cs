﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class GoodsOwnerAppService : PMSCrudAppServiceBase<GoodsOwnerEntity, GoodsOwnerDTO, GoodsOwnerDTO, string>
    {

        private IRepository<GoodsOwnerEntity, string> goodsOwnerRepository;
        public GoodsOwnerAppService(IRepository<GoodsOwnerEntity, string> _goodsOwnerRepository) : base(_goodsOwnerRepository)
        {
            goodsOwnerRepository = _goodsOwnerRepository;
        }


    }


}

