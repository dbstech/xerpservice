﻿using Abp.Application.Services;
using Abp.Web.Models;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy.ECinx;
using MagicLamp.XERP.BackgroundJob;
using System;
using System.Collections.Generic;
using System.Text;
using static MagicLamp.PMS.EcinxAppService;
using MagicLamp.PMS.Infrastructure.Extensions;


namespace MagicLamp.PMS
{
    public class DingDingNotification : ApplicationService
    {
        private SendNotificationToDingDingJob ddJob;

        private const string ClogTagType = "DDNotification";
        Dictionary<string, string> clogTags = new Dictionary<string, string>
        {
            ["type"] = ClogTagType
        };

        public DingDingNotification()
        {
            ddJob = new SendNotificationToDingDingJob();
        }

        public class DDMsg
        { 
            public string Message { get; set; }
        }


        [DontWrapResult]
        public APIresult SendDDMessageForEcinxBondWarehouseStock(DDMsg Message)
        {
            APIresult result = new APIresult();

            try
            {
                SendNotificationToDingDingJob ddJob = new SendNotificationToDingDingJob();

                ddJob.SendEcinxYWCQStockAlert(Message.Message);

                result.Code = 200;
                result.Msg = $"Success";
                return result;

            }
            catch (Exception ex)
            {
                result.Code = 203;
                result.Msg = ex.Message.ToJsonString();
                return result;
            }

        }
    }
}
