﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MagicLamp.PMS.DTOs.ThirdPart
{
    [ProtoContract]
    public class Content
    {
        [ProtoMember(1)]
        public string key { get; set; } // 每组字段的 key
        [ProtoMember(2)]
        public string value { get; set; } // 每组字段的 value
    }

    [ProtoContract]
    public class Log
    {
        [ProtoMember(1)]
        public long time { get; set; }  // 时间戳，UNIX时间格式
        [ProtoMember(2)]
        public List<Content> contents { get; set; } // 一条日志里的多个kv组合
    }

    [ProtoContract]
    public class LogGroup
    {
        [ProtoMember(1)]
        public List<Log> logs { get; set; } // 多条日志合成的日志数组
        [ProtoMember(2)]
        public string contextFlow { get; set; }  // 目前暂无效用
        [ProtoMember(3)]
        public string filename { get; set; } // 日志文件名
        [ProtoMember(4)]
        public string source { get; set; } // 日志来源，一般使用机器IP

    }

    [ProtoContract]
    public class LogGroupList
    {
        [ProtoMember(1)]
        public List<LogGroup> logGroupList { get; set; }// 日志组列表
    }

}