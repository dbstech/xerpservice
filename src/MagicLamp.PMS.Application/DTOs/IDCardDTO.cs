﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(IDCardEntity), typeof(OrderIDCardEntity))]
    public class IDCardDTO : EntityDto<string>
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public DateTime UpdateTime { get; set; }
        public int Verification { get; set; }
        public string URLFont { get; set; }
        public string URLBack { get; set; }
        public string URLFontImage { get; set; }
        public string URLBackImage { get; set; }
        public string OrderID { get; set; }
        public string CourierNo { get; set; }
    }
}
