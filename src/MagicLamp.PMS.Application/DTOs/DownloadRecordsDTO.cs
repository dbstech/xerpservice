﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(DownloadRecordsEntity))]
    public class DownloadRecordsDTO : EntityDto<Guid>
    {

        public Guid TaskID { get; set; }
        public string Type { get; set; }
        public string FilePath { get; set; }
        public string UserID { get; set; }
        public DateTime CreationTime { get; set; }
        public string Status { get; set; }

    }
}
