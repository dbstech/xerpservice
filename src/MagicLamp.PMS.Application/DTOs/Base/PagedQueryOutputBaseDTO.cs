﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{

    public class PagedQueryMeta
    {
        public long TotalCount { get; set; }
        private long? _pageCount;
        public long PageCount
        {
            get
            {
                _pageCount = this.TotalCount % this.PageSize == 0 ? this.TotalCount / this.PageSize :
                     this.TotalCount / this.PageSize + 1;
                return _pageCount.Value;
            }
            set { _pageCount = value; }
        }

        private int? _pageSize;
        public int PageSize
        {
            get
            {
                if (!_pageSize.HasValue || _pageSize.Value <= 0)
                {
                    _pageSize = PMSConsts.PageSize;
                }
                return _pageSize.Value;
            }
            set { _pageSize = value; }
        }
        public int PageIndex { get; set; }
        public string SortType { get; set; }
        public string Sort { get; set; }
    }

    public class PagedQueryOutputBaseDTO<T>
    {
        private List<T> _data = new List<T>();
        public List<T> Data
        {
            get { return _data; }
            set
            {
                _data = value;
            }
        }

        private PagedQueryMeta _meta = new PagedQueryMeta();
        public PagedQueryMeta Meta { get { return _meta; } set { _meta = value; } }
    }


}
