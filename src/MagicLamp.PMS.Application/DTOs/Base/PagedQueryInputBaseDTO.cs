﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class PagedQueryInputBaseDTO<T>
    {
        private int _pagesize;
        public int PageSize
        {
            get
            {
                if (_pagesize <= 0)
                {
                    _pagesize = PMSConsts.PageSize;
                }
                return _pagesize;
            }
            set { _pagesize = value; }
        }
        private int _pageindex;
        public int PageIndex
        {
            get
            {
                if (_pageindex <= 0)
                {
                    _pageindex = 1;
                }
                return _pageindex;
            }
            set { _pageindex = value; }
        }
        public string SortType { get; set; }
        public string Sort { get; set; }

    }
}
