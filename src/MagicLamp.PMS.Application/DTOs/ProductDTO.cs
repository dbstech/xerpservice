﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    [AutoMap(typeof(ProductEntity))]
    public class ProductDTO : EntityDto<string>
    {
        public int ID { get; set; }
        public string CustomerID { get; set; }
        public string SKU { get; set; }
        public string ChineseName { get; set; }
        public decimal Price { get; set; }

    }
}
