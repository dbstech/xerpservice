﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MagicLamp.PMS.Entities
{
    [AutoMap(typeof(ExportFlagEntity))]
    public class ExportFlagDTO : EntityDto<int>
    {

        public int Flag { get; set; }
        public string Name { get; set; }
    }
}
