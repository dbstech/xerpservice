﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.DTOs
{
    public class GenerateIDCardZipInputDTO
    {

        public DateTime UpdateBeginTime { get; set; }
        public DateTime EndBeginTime { get; set; }
        public int ExportFlag { get; set; }
        public string Name { get; set; }
        public string CourierID { get; set; }
        public string OrderID { get; set; }
        public string IDCard { get; set; }
    
        

    }


}
