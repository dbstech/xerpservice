using Abp.Application.Services;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Infrastructure;
using Abp.AutoMapper;

namespace MagicLamp.PMS
{

    public class ChangJiangExpressAppService : ApplicationService
    {
        private IRepository<OrderPackEntity, int> orderPackRepository;
        private IRepository<OrderEntity, int> orderRepository;
        private IRepository<OrderPackDetailEntity, int> orderPackDetailRepository;
        private IRepository<ProductEntity, string> productRepository;
        private const string ClogTagType = "ChangJiangExpress";

        public ChangJiangExpressAppService(
            IRepository<OrderPackEntity, int> _orderPackRepository,
            IRepository<OrderEntity, int> _orderRepository,
            IRepository<OrderPackDetailEntity, int> _orderPackDetailRepository,
            IRepository<ProductEntity, string> _productRepository
        )
        {
            orderPackRepository = _orderPackRepository;
            orderRepository = _orderRepository;
            orderPackDetailRepository = _orderPackDetailRepository;
            productRepository = _productRepository;
        }

        private List<ChangJiangCreateOrderDTO> GetOrderByCarrierIds(
            List<string> carrierIds,
            ref string error,
            ref int FailedOrdersCount,
            ref List<string> carrierIdsFound
        )
        {
            var query = from orderPack in orderPackRepository.GetAll()
                        join orderPackDetail in orderPackDetailRepository.GetAll()
                        on orderPack.opID equals orderPackDetail.opid
                        join order in orderRepository.GetAll()
                        on orderPack.oID equals order.oid
                        where
                            (orderPackDetail.flag == 1) &&
                            (orderPack.freezeFlag == 0) &&
                            (carrierIds.Contains(orderPack.carrierId))
                        select new ChangJiangCreateOrderDTO
                        {
                            Seq = order.seq,
                            OPId = orderPack.opID,
                            ParcelNo = orderPack.orderPackNo,
                            FreightTrackNo = orderPack.carrierId,
                            ParcelWeight = orderPack.packWeight,
                            OrderID = order.orderid,
                            SKU = orderPackDetail.sku,
                            Qty = orderPackDetail.qty,
                            ProductName = orderPackDetail.product_name,
                            ProductPrice = orderPackDetail.price,
                            ProductWeight = orderPackDetail.weight,
                            ReceiverName = orderPack.rec_name,
                            ReceiverAddress = orderPack.rec_addr,
                            ReceiverPhone = orderPack.rec_phone,
                            ReceiverTown = order.town,
                            ReceiverProvince = order.province,
                            ReceiverCity = order.city,
                            ReceiverDistrict = order.district,
                            ReceiverIdNo = order.idno
                        };
            try
            {
                IEnumerable<ChangJiangCreateOrderDTO> result = query.AsEnumerable();
                var groupOrders = result.GroupBy(x => x.OPId);
                var finalOrders = new List<ChangJiangCreateOrderDTO>();

                foreach (var group in groupOrders)
                {
                    var skus = group.ToList();
                    var item = skus[0];
                    carrierIdsFound.Add(item.FreightTrackNo);

                    item.SenderName = "Australia";
                    //item.SenderAddress = "Unit R, 63 Hugo Johnston Dr, Penrose, Auckland";
                    item.SenderAddress = "";
                    item.SenderPhone = "0064 9 838 8681";
                    item.SenderPostCode = "1061";
                    item.GoodItems = new List<GoodsItemDTO>();
                    item.ParcelWeight = item.ParcelWeight == null ? 0 : item.ParcelWeight;

                    if (item.ReceiverProvince.IsNullOrEmpty() || item.ReceiverCity.IsNullOrEmpty())
                    {
                        string addressError = string.Format("物流号: {0}, 订单号：{1}，省/市/区 信息为空，请尝试重新保存修改包裹收件地址；", item.FreightTrackNo, item.OrderID);

                        error += addressError;
                        FailedOrdersCount += 1;
                        CLSLogger.Error("创建第三方物流订单失败", string.Format("物流号: {0}, 订单号：{1}，省 / 市 / 区 信息为空，请尝试重新修改保存包裹收件地址", item.FreightTrackNo, item.OrderID), null);
                        continue;
                    }

                    if (!string.IsNullOrWhiteSpace(item.ReceiverTown))
                    {
                        //如有精确地址则使用不含省市的地址
                        item.ReceiverAddress = string.Format("{0}{1}", item.ReceiverDistrict, item.ReceiverTown);
                    }

                    //对city做特殊处理
                    if (item.ReceiverCity.Contains("省直辖县") && !string.IsNullOrEmpty(item.ReceiverDistrict))
                    {
                        item.ReceiverCity = item.ReceiverDistrict;
                    }
                    if (!string.IsNullOrWhiteSpace(item.ReceiverCity))
                    {
                        item.ReceiverCity = item.ReceiverCity.Trim();
                    }

                    //添加包裹商品
                    skus.ForEach(x =>
                    {
                        item.GoodItems.Add(new GoodsItemDTO
                        {
                            Name = x.ProductName,
                            Quantity = x.Qty.ToString(),
                            Price = x.ProductPrice.HasValue ? x.ProductPrice.Value : 0,
                            Weight = x.ProductWeight.HasValue ? x.ProductWeight.Value : 0,
                            SKU = x.SKU,
                            Unit = "件"
                        });
                    });

                    finalOrders.Add(item);
                }
                return finalOrders;
            }
            catch (Exception exception)
            {
                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = ClogTagType;
                tags["trackid"] = string.Join(";", carrierIds);
                CLSLogger.Error("数据库搜索失败", exception.Message, tags);
                throw new RequestValidationException($"数据库搜索失败");
            }
        }

        public PushOrderToChangJiangOutputDTO PushOrders(CarrierIdStringDTO carrierIdsDTO)
        {
            List<string> carrierIds = carrierIdsDTO.IdString.SplitByCommonSymbol();

            if (carrierIds.Count > 200)
            {
                throw new RequestValidationException($"一次补推的包裹号不能超过200条");
            }

            string error = "";
            int PushedOrdersCount = 0;
            int FailedOrdersCount = 0;
            List<string> carrierIdsFound = new List<string>();

            List<ChangJiangCreateOrderDTO> orderPacks = GetOrderByCarrierIds(carrierIds, ref error, ref FailedOrdersCount, ref carrierIdsFound);
            List<string> skus = new List<string>();
            foreach (var item in orderPacks)
            {
                skus.AddRange(item.GoodItems.Select(x => x.SKU));
            }

            skus = skus.Distinct().ToList();
            var products = productRepository.GetAll().Where(x => skus.Contains(x.SKU)).Select(x => new
            {
                x.SKU,
                x.Barcode1
            });


            foreach (var orderPack in orderPacks)
            {
                carrierIdsFound.Add(orderPack.FreightTrackNo);

                FreightCreateOrderInputDTO orderInput = orderPack.MapTo<FreightCreateOrderInputDTO>();

                bool hasBarcodeError = false;
                foreach (var orderItem in orderInput.GoodItems)
                {
                    var barcode = products.FirstOrDefault(x => x.SKU == orderItem.SKU);
                    if (barcode == null)
                    {
                        string barCodeError = string.Format("物流号: {0}, 订单号：{1}，SKU：{2} 未找到对应barcode；", orderInput.FreightTrackNo, orderInput.OrderID, orderItem.SKU);
                        error += barCodeError;
                        hasBarcodeError = true;
                        continue;
                    }
                    orderItem.Barcode = barcode.Barcode1;
                }

                if (hasBarcodeError)
                {
                    continue;
                }

                FreightCreateOrderOutputDTO orderOutput = ChangJiangExpressProxy.CreateNewOrder(orderInput, "neworderext");
                if (orderOutput == null)
                {
                    string pushError = string.Format("物流号: {0}, 订单号：{1}，{2}；", orderInput.FreightTrackNo, orderInput.OrderID, "订单推送失败");
                    error += pushError;
                    FailedOrdersCount += 1;
                }
                else if (orderOutput.Success)
                {
                    PushedOrdersCount += 1;
                }
                else if (orderOutput.Success == false)
                {
                    string pushError = string.Format("物流号: {0}, 订单号：{1}，{2}；", orderInput.FreightTrackNo, orderInput.OrderID, orderOutput.Message);
                    error += pushError;
                    FailedOrdersCount += 1;
                }
            }

            var carrierIdsNotFound = carrierIds.Except(carrierIdsFound).ToList();

            PushOrderToChangJiangOutputDTO result = new PushOrderToChangJiangOutputDTO
            {
                PushedOrdersCount = PushedOrdersCount,
                FailedOrdersCount = FailedOrdersCount,
                FailedOrders = error,
                CarrierIdsNotFound = carrierIdsNotFound,
            };

            return result;
        }
    }
}