﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class PackMaterialAppService : PMSCrudAppServiceBase<PackMaterialEntity, PackMaterialDTO, PackMaterialDTO, int>
    {

        private IRepository<PackMaterialEntity, int> packmaterialRepository;
        public PackMaterialAppService(IRepository<PackMaterialEntity, int> _packmaterialRepository) : base(_packmaterialRepository)
        {
            packmaterialRepository = _packmaterialRepository;
        }


    }


}

