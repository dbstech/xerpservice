using System.Data.SqlClient;
using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.UI;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EventHandlers;
using System;
using Abp.Web.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Abp.AutoMapper;
using MagicLamp.PMS.Proxy;
using Abp.Json;

namespace MagicLamp.PMS
{
    public class FreightsAppService : PMSCrudAppServiceBase<FreightsEntity, FreightsDTO, FreightsFilterDTO, string>
    {

        private IRepository<FreightsEntity, string> _freightsRepository;
        private IRepository<FreightsSetsEntity, int> _freightsSetsRepository;
        private IRepository<CurrencyEntity, string> _currencyRepository;
        public FreightsAppService(
            IRepository<FreightsEntity, string> freightsRepository,
            IRepository<FreightsSetsEntity, int> freightsSetsRepository,
            IRepository<CurrencyEntity, string> currencyRepository
        ) : base(freightsRepository)
        {
            _freightsRepository = freightsRepository;
            _freightsSetsRepository = freightsSetsRepository;
            _currencyRepository = currencyRepository;
        }
        public override PagedQueryOutputBaseDTO<FreightsDTO> GetList(PagedQueryInputBaseDTO<FreightsFilterDTO> input)
        {
            var query = base.BuildQuery(input).Include(x => x.FreightsSets);
            var result = base.PaginateQuery(query, input);
            var currencies = _currencyRepository.GetAll().ToList();
            result.Data.ForEach(x => x.Currency = currencies.FirstOrDefault(y => y.Code == x.CurrencyCode).Name);
            return result;
        }
        public override FreightsDTO Create(FreightsDTO input)
        {
            try
            {
                input.UpdateDate = DateTime.UtcNow;
                var result = base.Create(input);
                int maxFreightSetId = _freightsSetsRepository.GetAll().Max(x => x.Id);
                _freightsSetsRepository.Insert(new FreightsSetsEntity
                {
                    FreightId = result.Id,
                    Id = maxFreightSetId + 1,
                    Valflag = "1",
                    Keyword = input.name
                });
                return result;
            }
            catch (SqlException ex) when (ex.Number == 2627)
            {
                throw new UserFriendlyException($"物流公司代码:{input.name}已被使用");
            }
        }
        public override FreightsDTO Update(FreightsDTO input)
        {
            try
            {
                var keyVal = input.id;
                if (keyVal == null)
                {
                    throw new UserFriendlyException("无效的Id请求参数");
                }

                FreightsEntity freightsEntity = _freightsRepository.FirstOrDefault(x => x.id.Equals(keyVal));

                if (freightsEntity == null)
                {
                    throw new UserFriendlyException($"未找到对应主键：{input.Id} 记录，无法更新");
                }
                input.UpdateDate = DateTime.UtcNow;
                input.MapTo(freightsEntity);
                var freightsSets = _freightsSetsRepository.FirstOrDefault(x => x.FreightId == input.id);
                if (freightsSets == null)
                {
                    int maxFreightSetId = _freightsSetsRepository.GetAll().Max(x => x.Id);
                    _freightsSetsRepository.Insert(new FreightsSetsEntity
                    {
                        FreightId = input.id,
                        Id = maxFreightSetId + 1,
                        Valflag = "1",
                        Keyword = freightsEntity.name
                    });
                }
                else
                {
                    freightsSets.Keyword = freightsEntity.name;
                }
                return input;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("Update发生未知异常", $"{ex.Message};实体类：{input.ToJsonString()}");
                throw new UserFriendlyException("更新发生未知异常", ex);
            }
        }

        public override bool Delete(string[] ids)
        {
            foreach (var id in ids)
            {
                var freightsSets = _freightsSetsRepository.FirstOrDefault(x => x.FreightId == id);
                _freightsSetsRepository.Delete(freightsSets);
            }
            return base.Delete(ids);
        }
    }
}

