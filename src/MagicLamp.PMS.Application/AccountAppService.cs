using Abp.Application.Services;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using Abp.Domain.Uow;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy;
using System;
using System.Text;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.DTOs.Configs;
using System.Linq;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Domain.ValueObjects;

namespace MagicLamp.PMS
{
    public class AccountAppService : ApplicationService
    {
        private readonly IUnitOfWorkManager unitOfWorkManager;
        private IRepository<AccountEntity, string> accountRepository;
        private IRepository<LoginSessionEntity, Guid> loginSessionRepository;
        private IRepository<UserActivityLogEntity, long> userActivityLogRepository;
        private readonly IHttpContextAccessor httpContextAccessor;
        protected HttpContext HttpContext => httpContextAccessor.HttpContext;
        private const string SessionIdAlias = "_a";

        public AccountAppService(
            IRepository<AccountEntity, string> _accountRepository,
            IRepository<LoginSessionEntity, Guid> _loginSessionRepository,
            IRepository<UserActivityLogEntity, long> _userActivityLogRepository,
            IHttpContextAccessor _httpContextAccessor,
            IUnitOfWorkManager _unitOfWorkManager
        )
        {
            accountRepository = _accountRepository;
            loginSessionRepository = _loginSessionRepository;
            userActivityLogRepository = _userActivityLogRepository;
            httpContextAccessor = _httpContextAccessor;
            unitOfWorkManager = _unitOfWorkManager;
        }

        private string Hash(string str)
        {
            string salt = "jRzugyGfYITm6Fog";
            byte[] data = Encoding.UTF8.GetBytes(str + salt);
            MD5 md5 = new MD5CryptoServiceProvider();
            data = md5.ComputeHash(data);
            string result = Encoding.UTF8.GetString(data);
            return result;
        }

        private UserInfoDTO ConvertJObjectToUserInfo(JObject userInfoJsonObj)
        {
            UserInfoDTO output = new UserInfoDTO
            {
                Name = userInfoJsonObj["name"].Value<string>(),
                UnionID = userInfoJsonObj["unionid"].Value<string>(),
                OpenID = userInfoJsonObj["openId"].Value<string>(),
                UserID = userInfoJsonObj["userid"].Value<string>(),
                WorkPlace = userInfoJsonObj["workPlace"] != null ? userInfoJsonObj["workPlace"].Value<string>() : string.Empty,
                Mobile = userInfoJsonObj["mobile"].Value<string>(),
                StateCode = userInfoJsonObj["stateCode"].Value<string>(),
                Avatar = userInfoJsonObj["avatar"].Value<string>(),
                Active = userInfoJsonObj["active"].Value<bool>(),
                Position = userInfoJsonObj["position"] != null ? userInfoJsonObj["position"].Value<string>() : string.Empty,
                OrgEmail = userInfoJsonObj["orgEmail"] != null ? userInfoJsonObj["orgEmail"].Value<string>() : string.Empty,
                Department = userInfoJsonObj["department"].Values<int>().ToList(),
            };

            return output;
        }

        private ClaimsIdentity GenerateClainmsIdentityByAccountInfo(AccountEntity accountEntity, string sessionId = null)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, accountEntity.UserID),
                    new Claim(ClaimTypes.Name, accountEntity.UserName),
                    new Claim("SessionId", sessionId),
                    new Claim(ClaimTypes.Role, accountEntity.RoleID),
                };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            return claimsIdentity;
        }

        private async Task SignInAsync(AccountEntity accountEntity, string sessionId = null)
        {
            ClaimsIdentity claimsIdentity = GenerateClainmsIdentityByAccountInfo(accountEntity, sessionId);
            ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                ExpiresUtc = DateTimeOffset.UtcNow.AddDays(7),
                IsPersistent = true,
                IssuedUtc = DateTimeOffset.UtcNow
            };

            await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimsIdentity),
                    authProperties);
        }


        [HttpGet]
        public UserInfoDTO UserInfo()
        {
            LoginSessionEntity loginSessionEntity;
            if (HttpContext.User.HasClaim(claim => claim.Type == "SessionId"))
            {
                Guid sessionId = Guid.Parse(HttpContext.User.Claims.First(x => x.Type == "SessionId").Value);
                loginSessionEntity = loginSessionRepository.FirstOrDefault(loginSession => loginSession.Id == sessionId);
            }
            else
            {
                throw new AbpAuthorizationException($"你的登录授权已失效，请重新登录");
            };

            try
            {
                // TODO: remove when Authorization Attribute/Handler is implemented
                if (loginSessionEntity == null)
                {
                    throw new AbpAuthorizationException($"你的登录授权已失效，请重新登录");
                }

                if ((DateTime.UtcNow >= loginSessionEntity.ExpiryTime) || !loginSessionEntity.Active)
                {
                    throw new AbpAuthorizationException($"你的登录授权已失效，请重新登录");
                }

                loginSessionEntity.LastActivity = DateTime.UtcNow;

                UserInfoDTO userInfo = null;
                LoginMethodEnum loginMethodEnum;
                if (!Enum.TryParse(loginSessionEntity.LoginMethod, out loginMethodEnum))
                {
                    throw new AbpAuthorizationException("Invalid Login method");
                }

                switch (loginMethodEnum)
                {
                    case LoginMethodEnum.DingTalk:
                        JObject userInfoJsonObj = JObject.Parse(loginSessionEntity.UserInfo);
                        userInfo = ConvertJObjectToUserInfo(userInfoJsonObj);
                        userInfo.Authority = "admin";
                        break;
                    case LoginMethodEnum.Account:
                        AccountEntity accountEntity = accountRepository.FirstOrDefault(account => account.UserID == loginSessionEntity.UserID);
                        userInfo = new UserInfoDTO
                        {
                            Name = accountEntity.UserName,
                            UserID = accountEntity.UserID,
                            Authority = "admin"
                        };
                        break;
                }

                return userInfo;
            }
            catch (AbpAuthorizationException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("GetUserInfo发生异常", ex);
                throw new UserFriendlyException("GetUserInfo发生异常");
            }
        }

        public async Task<UserInfoDTO> Login(AccountLoginInputDTO input)
        {
            
            throw new AbpAuthorizationException("账户密码登录方式已关闭，请使用DingDing进行身份验证!");

            //try
            //{
            //    if (string.IsNullOrEmpty(input.UserName) || string.IsNullOrEmpty(input.Password))
            //    {
            //        throw new AbpAuthorizationException("账号或密码不能为空");
            //    }

            //    AccountEntity accountEntity = accountRepository.FirstOrDefault(account => account.UserID == input.UserName);

            //    if ((accountEntity == null) || (accountEntity.ActiveFlag == '0'))
            //    {
            //        throw new AbpAuthorizationException("用户名或密码错误");
            //    }

            //    if (Hash(input.Password) != accountEntity.Password)
            //    {
            //        using (var unitOfWork = unitOfWorkManager.Begin())
            //        {
            //            accountEntity.ErroPasswordTimes += 1;
            //            if (accountEntity.ErroPasswordTimes >= 5)
            //            {
            //                accountEntity.ActiveFlag = '0';
            //            }
            //            accountRepository.Update(accountEntity);
            //            unitOfWork.Complete();
            //        }


            //        throw new AbpAuthorizationException("用户名或密码错误");
            //    }
            //    LoginSessionEntity loginSessionEntity = new LoginSessionEntity
            //    {
            //        LoginMethod = LoginMethodEnum.Account.ToString(),
            //        ExpiryTime = DateTime.UtcNow.AddDays(7),
            //        LastActivity = DateTime.UtcNow,
            //        UserID = accountEntity.UserID,
            //    };

            //    Guid sessionId = loginSessionRepository.InsertAndGetId(loginSessionEntity);

            //    UserInfoDTO userInfoDTO = new UserInfoDTO
            //    {
            //        Name = accountEntity.UserName,
            //        Authority = "admin"
            //    };

            //    using (var unitOfWork = unitOfWorkManager.Begin())
            //    {
            //        accountEntity.ErroPasswordTimes = 0;
            //        accountEntity.LastLoginDate = DateTime.UtcNow;
            //        accountRepository.Update(accountEntity);
            //        unitOfWork.Complete();
            //    }

            //    await SignInAsync(accountEntity, sessionId.ToString());

            //    return userInfoDTO;
            //}
            //catch (Exception ex)
            //{
            //    if (ex is AbpAuthorizationException)
            //    {
            //        throw ex;
            //    }
            //    CLSLogger.Error("AccountLogin发生异常", ex);
            //    throw new UserFriendlyException("AccountLogin发生异常");
            //}
        }


        public async Task<UserInfoDTO> DingTalkLogin(DDGetUserByCodeInputDTO input)
        {
            try
            {
                var businessConfig = Ultities.GetConfig<BusinessConfig>();

                string appid = string.IsNullOrEmpty(input.AppID) ? businessConfig.DDAppID : input.AppID;
                string appSecret = string.IsNullOrEmpty(input.AppSecret) ? businessConfig.DDAppSecret : input.AppSecret;
                JObject userInfoJsonObj = DDOpenAPIProxy.GetDingTalkUserInfoByCode(appid, appSecret, input.Code);

                UserInfoDTO userInfo = this.ConvertJObjectToUserInfo(userInfoJsonObj);

                var accountEntity = accountRepository.FirstOrDefault(x => x.DingTalkUnionId == userInfo.UnionID);
                if (accountEntity == null)
                {
                    accountEntity = new AccountEntity
                    {
                        UserID = userInfo.UserID,
                        UserName = userInfo.Name,
                        Password = Hash(userInfo.UnionID),
                        CreateDate = DateTime.UtcNow,
                        ActiveFlag = '1',
                        RoleID = "admin",
                        CreateBy = "System",
                        LastLoginDate = DateTime.UtcNow,
                        Email = userInfo.OrgEmail,
                        DingTalkNick = userInfo.Name,
                        DingTalkUnionId = userInfo.UnionID
                    };
                    var accountId = accountRepository.InsertAndGetId(accountEntity);
                }

                LoginSessionEntity loginSessionEntity = new LoginSessionEntity
                {
                    UserInfo = userInfoJsonObj.ToString(),
                    LoginMethod = LoginMethodEnum.DingTalk.ToString(),
                    ExpiryTime = DateTime.UtcNow.AddDays(7),
                    LastActivity = DateTime.UtcNow,
                };

                Guid sessionId = loginSessionRepository.InsertAndGetId(loginSessionEntity);
                userInfo.Authority = "admin";

                accountEntity.LastLoginDate = DateTime.UtcNow;

                await SignInAsync(accountEntity, sessionId.ToString());

                return userInfo;
            }
            catch (UserFriendlyException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("钉钉登录发生异常", ex);
                throw new UserFriendlyException("钉钉登录发生异常");
            }

        }

        public async Task Logout()
        {
            if (HttpContext.User.HasClaim(claim => claim.Type == "SessionId"))
            {
                var claims = HttpContext.User.Claims;
                Guid sessionId = Guid.Parse(claims.FirstOrDefault(x => x.Type == "SessionId").Value);
                var loginSessionEntity = loginSessionRepository.FirstOrDefault(loginSession => loginSession.Id == sessionId);
                loginSessionEntity.Active = false;
            };
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        [HttpGet]
        public List<AccountDTO> Accounts()
        {
            List<AccountEntity> users = accountRepository.GetAll().Where(x => x.ActiveFlag == '1').ToList();
            return ObjectMapper.Map<List<AccountDTO>>(users);
        }
    }

}