using FluentValidation;
namespace MagicLamp.PMS.CQBondedProduct
{
    public class CreateCQBondedProductDtoValidator : AbstractValidator<CreateCQBondedProductDto>
    {
        public CreateCQBondedProductDtoValidator()
        {
            RuleFor(x => x.ERPSKU).NotEmpty().WithMessage("\"{PropertyName}\"不能为空");
            RuleFor(x => x.GMSKU).NotEmpty().WithMessage("\"{PropertyName}\"不能为空");
            RuleFor(x => x.GOODS_SPEC).NotEmpty().WithMessage("\"{PropertyName}\"不能为空");
            RuleFor(x => x.CURRENCY_CODE).NotEmpty().WithMessage("\"{PropertyName}\"不能为空");
            RuleFor(x => x.COUNTRY).NotEmpty().WithMessage("\"{PropertyName}\"不能为空");
            RuleFor(x => x.BAR_CODE).NotEmpty().WithMessage("\"{PropertyName}\"不能为空");
            RuleFor(x => x.UNIT_CODE).NotEmpty().WithMessage("\"{PropertyName}\"不能为空");
            RuleFor(x => x.GOODS_NAME).NotEmpty().WithMessage("\"{PropertyName}\"不能为空");
            RuleFor(x => x.HS_CODE).NotEmpty().WithMessage("\"{PropertyName}\"不能为空");
        }
    }
}

