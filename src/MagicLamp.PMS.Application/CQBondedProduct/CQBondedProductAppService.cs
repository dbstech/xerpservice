using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using MagicLamp.PMS.Entities;
using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Collections.Generic;
using MagicLamp.PMS.Infrastructure.Extensions;
using NPOI.HSSF.Util;
using System.IO;
using Abp.UI;
using Abp.EntityFrameworkCore;
using MagicLamp.PMS.EntityFrameworkCore;
using EFCore.BulkExtensions;
using MagicLamp.PMS.Proxy;
using FluentValidation.Results;
using System.Text;

namespace MagicLamp.PMS.CQBondedProduct
{
    public class CQBondedProductAppService : AsyncCrudAppService<CQBondedProductEntity, CQBondedProductDto, string, PagedRequestDto>
    {
        private readonly IRepository<CQBondedProductEntity, string> _cQBondedProductRepository;
        private readonly IRepository<ProductEntity, string> _productRepository;
        private IDbContextProvider<PMSDbContext> _pmsDbContextProvider;
        public CQBondedProductAppService(
            IRepository<CQBondedProductEntity, string> cQProductRepository,
            IRepository<ProductEntity, string> productRepository,
            IDbContextProvider<PMSDbContext> pmsDbContextProvider
        ) : base(cQProductRepository)
        {
            _cQBondedProductRepository = cQProductRepository;
            _productRepository = productRepository;
            _pmsDbContextProvider = pmsDbContextProvider;
        }
        protected override CQBondedProductEntity MapToEntity(CQBondedProductDto createInput)
        {
            var entity = base.MapToEntity(createInput);
            entity.CreateTime = DateTime.UtcNow;
            return entity;
        }

        protected override void MapToEntity(CQBondedProductDto updateInput, CQBondedProductEntity entity)
        {
            base.MapToEntity(updateInput, entity);
            entity.UpdateTime = DateTime.UtcNow;
        }
        protected override IQueryable<CQBondedProductEntity> CreateFilteredQuery(PagedRequestDto input)
        {
            return Repository.GetAll()
                            .WhereIf(!input.erpSku.IsNullOrWhiteSpace(), m => m.ERPSKU == input.erpSku.Trim())
                            .WhereIf(!input.gmSku.IsNullOrWhiteSpace(), m => m.GMSKU == input.gmSku.Trim())
                            .WhereIf(!input.hsCode.IsNullOrWhiteSpace(), m => m.HS_CODE == input.hsCode.Trim())
                            .WhereIf(!input.goodsName.IsNullOrWhiteSpace(), m => m.GOODS_NAME.Contains(input.goodsName.Trim()));
        }

        [HttpPost]
        public async Task<bool> BulkInsert(IFormFile file)
        {
            if (file == null || file.Length <= 0)
            {
                throw new UserFriendlyException("文件大小异常，请选择正确的文件");
            }

            try
            {
                var workbook = new HSSFWorkbook(file.OpenReadStream());

                var sheet = workbook.GetSheet("Sheet1");

                var mapper = new Npoi.Mapper.Mapper(workbook);

                var uploadedProducts = mapper.Take<CreateCQBondedProductDto>("Sheet1").ToList();

                CreateCQBondedProductDtoValidator validator = new CreateCQBondedProductDtoValidator();

                List<CreateCQBondedProductDto> productDtos = new List<CreateCQBondedProductDto> { };
                StringBuilder errorMsg = new StringBuilder();
                List<string> productSkus = uploadedProducts.Select(x => x.Value.ERPSKU).ToList();
                List<string> cqProductSkus = _cQBondedProductRepository.GetAll().Where(x => productSkus.Contains(x.ERPSKU)).Select(x => x.ERPSKU).ToList();
                List<string> skusFound = _productRepository.GetAll().Where(x => productSkus.Contains(x.SKU)).Select(x => x.SKU).ToList();

                foreach (var rowInfo in uploadedProducts)
                {
                    CreateCQBondedProductDto productDto = rowInfo.Value;
                    ValidationResult validationResult = validator.Validate(productDto);

                    if (!skusFound.Contains(productDto.ERPSKU))
                    {
                        errorMsg.Append($"第{rowInfo.RowNumber}行无法找到SKU为:{productDto.ERPSKU}的产品\r\n");
                    }

                    if (cqProductSkus.Contains(productDto.ERPSKU))
                    {
                        errorMsg.Append($"第{rowInfo.RowNumber}SKU已被使用，不能重复:{productDto.ERPSKU}\r\n");
                    }

                    foreach (var error in validationResult.Errors)
                    {
                        errorMsg.Append($"第{rowInfo.RowNumber}行{error.ErrorMessage}\r\n");
                    }
                    productDtos.Add(productDto);
                }

                if (errorMsg.Length > 0)
                {
                    throw new UserFriendlyException(errorMsg.ToString());
                }

                var pmsDbContext = _pmsDbContextProvider.GetDbContext();

                List<CQBondedProductEntity> productEntities = ObjectMapper.Map<List<CQBondedProductEntity>>(productDtos);
                List<string> erpSkus = productEntities.Select(x => x.ERPSKU).ToList();

                var existingProducts = _cQBondedProductRepository.GetAll().Where(x => erpSkus.Contains(x.ERPSKU)).Select(x => x.ERPSKU).ToList();

                pmsDbContext.BulkInsert(productEntities, new BulkConfig
                {
                    PropertiesToExclude = new List<string> { nameof(CQBondedProductEntity.CreateTime), nameof(CQBondedProductEntity.UpdateTime) },
                });
            }
            catch (UserFriendlyException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                CLSLogger.ErrorWithSentry("CQBondedWarehouse BulkInsert Exception:", ex);
                throw new UserFriendlyException("文件上传失败");
            }
            return true;
        }

        public async Task<FileContentResult> GetTemplate()
        {
            CreateCQBondedProductDto product = new CreateCQBondedProductDto
            {
                GMSKU = "9400575001289",
                ERPSKU = "BSCH01060604",
                GOODS_SPEC = "150片/瓶",
                CURRENCY_CODE = "142",
                COUNTRY = "609",
                BAR_CODE = "9400575001289",
                UNIT_CODE = "142",
                GOODS_NAME = "汤普森维生素C咀嚼片",
                HS_CODE = "2106909090",
                VATRate = 0.00M,
                UNIT2 = "011",
                QTY2 = "1"
            };
            var productType = typeof(CreateCQBondedProductDto);

            HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
            ISheet sheet1 = hssfWorkbook.CreateSheet("Sheet1");
            IRow headerRow = sheet1.CreateRow(0);
            ICellStyle requiredColumnsStyle = hssfWorkbook.CreateCellStyle();
            IFont requiredColumnsFont = hssfWorkbook.CreateFont();
            requiredColumnsFont.Color = HSSFColor.Red.Index;
            requiredColumnsStyle.SetFont(requiredColumnsFont);

            var npoiMapper = new Npoi.Mapper.Mapper(hssfWorkbook);

            // Add headers to sheet
            List<string> headers = typeof(CreateCQBondedProductDto).GetDisplayAttributeValues();

            for (var i = 0; i < headers.Count(); i++)
            {
                ICell cell = headerRow.CreateCell(i);
                cell.SetCellValue(headers[i]);
            }

            npoiMapper.Put(new List<CreateCQBondedProductDto> { product }, "Sheet1");

            headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(product.GMSKU)))).CellStyle = requiredColumnsStyle;
            headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(product.ERPSKU)))).CellStyle = requiredColumnsStyle;
            headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(product.GOODS_SPEC)))).CellStyle = requiredColumnsStyle;
            headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(product.CURRENCY_CODE)))).CellStyle = requiredColumnsStyle;
            headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(product.COUNTRY)))).CellStyle = requiredColumnsStyle;
            headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(product.BAR_CODE)))).CellStyle = requiredColumnsStyle;
            headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(product.UNIT_CODE)))).CellStyle = requiredColumnsStyle;
            headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(product.GOODS_NAME)))).CellStyle = requiredColumnsStyle;
            headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(product.HS_CODE)))).CellStyle = requiredColumnsStyle;

            MemoryStream memoryStream = new MemoryStream();
            hssfWorkbook.Write(memoryStream);
            var bytes = memoryStream.ToArray();
            FileContentResult result = new FileContentResult(bytes, "application/vnd.ms-excel");
            result.FileDownloadName = $"cq_product_template.xls";

            return result;
        }
    }
}