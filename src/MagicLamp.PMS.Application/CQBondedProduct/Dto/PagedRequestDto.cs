using Abp.Application.Services.Dto;

namespace MagicLamp.PMS.CQBondedProduct
{
    public class PagedRequestDto : PagedAndSortedResultRequestDto
    {
        public string erpSku { get; set; }
        public string gmSku { get; set; }
        public string hsCode { get; set; }
        public string goodsName { get; set; }
    }
}