using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Application.Services.Dto;
using System.Runtime.Serialization;
using MagicLamp.PMS.Entities;
using Abp.AutoMapper;
using Newtonsoft.Json;

namespace MagicLamp.PMS.CQBondedProduct
{
    // [DataContract]
    [AutoMapFrom(typeof(CQBondedProductEntity))]
    public class CQBondedProductDto : EntityDto<string>
    {
        /// <summary>
        /// 关贸SKU（海关备案SKU）
        /// </summary>
        [JsonProperty("gmSku")]
        public string GMSKU { get; set; }

        /// <summary>
        /// ERP--SKU
        /// </summary>
        [JsonProperty("erpSku")]
        public string ERPSKU { get; set; }
        [JsonProperty("goodsSpec")]
        public string GOODS_SPEC { get; set; }
        [JsonProperty("currencyCode")]
        public string CURRENCY_CODE { get; set; }
        [JsonProperty("country")]
        public string COUNTRY { get; set; }
        [JsonProperty("barcode")]
        public string BAR_CODE { get; set; }
        [JsonProperty("unitCode")]
        public string UNIT_CODE { get; set; }
        [JsonProperty("goodsName")]
        public string GOODS_NAME { get; set; }
        [JsonProperty("hsCode")]
        public string HS_CODE { get; set; }

        /// <summary>
        /// 增值税率
        /// </summary>
        [JsonProperty("vatRate")]
        public decimal VATRate { get; set; }

        [JsonProperty("createTime")]
        public DateTime? CreateTime { get; set; }
        [JsonProperty("updateTime")]
        public DateTime? UpdateTime { get; set; }
        [JsonProperty("unit2")]
        public string UNIT2 { get; set; }
        [JsonProperty("qty2")]
        public string QTY2 { get; set; }

    }


}
