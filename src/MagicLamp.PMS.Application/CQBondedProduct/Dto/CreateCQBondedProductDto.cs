using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MagicLamp.PMS.Entities;
using System.ComponentModel.DataAnnotations;

namespace MagicLamp.PMS.CQBondedProduct
{
    [AutoMapFrom(typeof(CQBondedProductEntity))]

    public class CreateCQBondedProductDto : EntityDto<string>
    {
        /// <summary>
        /// 关贸SKU（海关备案SKU）
        /// </summary>
        [Display(Name = "料号")]
        public string GMSKU { get; set; }

        /// <summary>
        /// ERP--SKU
        /// </summary>
        [Display(Name = "SKU")]
        public string ERPSKU { get; set; }
        [Display(Name = "商品规格")]
        public string GOODS_SPEC { get; set; }
        [Display(Name = "币制代码")]
        public string CURRENCY_CODE { get; set; }
        [Display(Name = "原产国")]
        public string COUNTRY { get; set; }
        [Display(Name = "商品条形码")]
        public string BAR_CODE { get; set; }
        [Display(Name = "申报单位")]
        public string UNIT_CODE { get; set; }
        [Display(Name = "商品名称")]
        public string GOODS_NAME { get; set; }
        [Display(Name = "商品编码")]
        public string HS_CODE { get; set; }

        /// <summary>
        /// 增值税率
        /// </summary>
        [Display(Name = "vatRate")]
        public decimal VATRate { get; set; }
        [Display(Name = "第二单位")]
        public string UNIT2 { get; set; }
        [Display(Name = "第二数量")]
        public string QTY2 { get; set; }
    }
}