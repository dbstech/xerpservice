﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class CostTypeAppService : PMSCrudAppServiceBase<CostTypeEntity, CostTypeDTO, CostTypeDTO, string>
    {

        private IRepository<CostTypeEntity, string> costtypeRepository;
        public CostTypeAppService(IRepository<CostTypeEntity, string> _costtypeRepository) : base(_costtypeRepository)
        {
            costtypeRepository = _costtypeRepository;
        }


    }


}

