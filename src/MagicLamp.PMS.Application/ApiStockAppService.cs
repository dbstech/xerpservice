using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.DTOs;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using System;
using System.Linq;
using MagicLamp.PMS.Proxy;
using Abp.UI;
using System.Collections.Generic;
using Abp.Dependency;
using Abp.BackgroundJobs;
using MagicLamp.XERP.BackgroundJob;
using MagicLamp.PMS.DTOs.Enums;
using Microsoft.AspNetCore.Mvc;
using MagicLamp.PMS.Domain;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.Enums;
using System.Text;

namespace MagicLamp.PMS
{
    public class ApiStockAppService : PMSCrudAppServiceBase<APIStockEntity, APIStockDTO, APIStockDTO, string>
    {
        private IRepository<APIStockEntity, string> apiStockRepository;
        private IRepository<LoginSessionEntity, Guid> loginSessionRepository;
        private IRepository<ProductEntity, string> productRepository;
        private IRepository<UserActivityLogEntity, long> userActivityLogRepository;
        private IBackgroundJobManager backgroundJobManager;
        private IAccountManager accountManager;
        public ApiStockAppService(
            IRepository<APIStockEntity, string> _apiStockRepository,
            IRepository<ProductEntity, string> _productRepository,
            IRepository<LoginSessionEntity, Guid> _loginSessionRepository,
            IRepository<UserActivityLogEntity, long> _userActivityLogRepository,
            IBackgroundJobManager _backgroundJobManager,
            IAccountManager _accountManager
        ) : base(_apiStockRepository)
        {
            apiStockRepository = _apiStockRepository;
            loginSessionRepository = _loginSessionRepository;
            productRepository = _productRepository;
            userActivityLogRepository = _userActivityLogRepository;
            backgroundJobManager = _backgroundJobManager;
            accountManager = _accountManager;
        }

        public void SetProductProperties(APIStockDTO dto, bool? isHHPurchase, DateTime createTime)
        {
            dto.IsHHPurchase = isHHPurchase;
            dto.CreateTime = createTime;
        }
        public override PagedQueryOutputBaseDTO<APIStockDTO> GetList(PagedQueryInputBaseDTO<APIStockDTO> input)
        {
            PagedQueryOutputBaseDTO<APIStockDTO> output = base.GetList(input);

            var skus = output.Data.Select(x => x.SKU.Trim());

            var products = productRepository.GetAll()
                            .Where(x => skus.Contains(x.SKU))
                            .Select(x => new
                            {
                                SKU = x.SKU,
                                IsHHPurchase = x.IsHHPurchase,
                                CreateTime = x.CreateTime,
                                IsVirtualStock = x.PurchasePeriodCode == "virtualStock"
                            })
                            .ToDictionary(x => x.SKU, x => x);

            ProductDomainService productDomainService = new ProductDomainService();
            var virtualStockSkuPrefixList = productDomainService.GetVirtualStockSkuPrefix();

            output.Data.ForEach(x =>
            {
                string sku = x.SKU.Trim();
                if (products.ContainsKey(sku))
                {
                    var product = products[sku];
                    x.IsHHPurchase = product.IsHHPurchase;
                    x.CreateTime = product.CreateTime;
                    x.IsVirtualStock = product.IsVirtualStock;
                }
                else
                {
                    x.NoProduct = true;
                }
            });
            return output;
        }

        public override APIStockDTO Update(APIStockDTO input)
        {
            ProductEntity product = productRepository.FirstOrDefault(x => x.SKU == input.Id);

            if (product != null)
            {
                List<ProductStockChangedDTO> changedskustock = new List<ProductStockChangedDTO>();

                ProductDomainService productDomainService = new ProductDomainService();
                var isVirtualStock = product.PurchasePeriodCode == "virtualStock";

                APIStockEntity apiStockEntity = apiStockRepository.FirstOrDefault(x => x.SKU == input.Id);
                int Qty_availablebefore = apiStockEntity.Qty_available;
                int qty_locked = apiStockEntity.Qty_locked ;

                if (input.Qty_available != null && isVirtualStock == true && apiStockEntity.Qty_available != input.Qty_available)
                {
                    var before = apiStockEntity.ToJsonString();
                    apiStockEntity.Qty_available = input.Qty_available.Value - apiStockEntity.Qty_locked;
                    apiStockEntity.Qty_aladdin = apiStockEntity.Qty_available;
                    apiStockEntity.Qty = apiStockEntity.Qty_available;

                    LogUserActivityVO qtyAvailableActivityVO = new LogUserActivityVO
                    {
                        ObjectType = UserActivityObjectType.ApiStock_QtyAvailable,
                        ObjectId = apiStockEntity.Id,
                        Action = UserActionEnum.Update.ToString(),
                        Content = $"修改魔法库存购为'{apiStockEntity.Qty_available}'",
                        Before = before,
                        After = apiStockEntity.ToJsonString()
                    };
                    accountManager.LogUserActivity(qtyAvailableActivityVO);
                }

                if (input.IsHHPurchase != null && product.IsHHPurchase != input.IsHHPurchase)
                {
                    var before = product.ToJsonString();
                    product.IsHHPurchase = input.IsHHPurchase;

                    LogUserActivityVO logUserActivityVO = new LogUserActivityVO
                    {
                        ObjectType = UserActivityObjectType.Product_IsHHPurchase,
                        ObjectId = input.Id,
                        Action = UserActionEnum.Update.ToString(),
                        Content = $"修改魔法灯采购为'{(input.IsHHPurchase == true ? "否" : "是")}'",
                        Before = before,
                        After = product.ToJsonString()
                    };
                    accountManager.LogUserActivity(logUserActivityVO);
                }

                APIStockDTO apiStock = apiStockEntity.MapTo<APIStockDTO>();
                apiStock.IsVirtualStock = isVirtualStock;

                SetProductProperties(apiStock, product.IsHHPurchase, product.CreateTime);

                CurrentUnitOfWork.SaveChanges();

                IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();

                backgroundJobManager.Enqueue<SyncAladdinStockJob, List<string>>(
                    new List<string> { input.Id },
                    BackgroundJobPriority.High
                );

                SyncAladdinStockOutputDTO result = new SyncAladdinStockOutputDTO();

                backgroundJobManager.Enqueue<RefreshProductInfoCacheJob, List<string>>(
                    new List<string> { input.Id },
                    BackgroundJobPriority.High
                );

                //DingdingMesage
                ProductStockChangedDTO _changedskustock = new ProductStockChangedDTO();
                _changedskustock.Sku =product.SKU.Trim().ToUpper();
                _changedskustock.Qty_availableBefore = Qty_availablebefore;
                _changedskustock.Qty_upload = input.Qty_available.Value; // RealUpdateQTY;
                _changedskustock.Qty_availableAfter = input.Qty_available.Value - qty_locked; // RealUpdateQTY;

                PushDingDingUpdateDataNotification(accountManager.GetUserName(), changedskustock, true);
                return apiStock;
            }
            CLSLogger.Error("ApiStockUpdate", $"Unable to find product with sku: {input.Id}");
            throw new UserFriendlyException("更新失败，未找到该产品");
        }

        public SyncAladdinStockOutputDTO SyncAladdinStock(List<SyncAladdinStockInputDTO> input)
        {
            List<string> skus = input.Select(x => x.Sku).ToList();
            backgroundJobManager.Enqueue<SyncAladdinStockJob, List<string>>(skus, BackgroundJobPriority.High);
            SyncAladdinStockOutputDTO result = new SyncAladdinStockOutputDTO();
            result.Success = true;
            result.Message = "同步完成，请稍后前往前台查看库存";
            return result;
        }

        [HttpGet]
        public List<UserActivityLogDTO> UserActivityLog(string sku)
        {
            List<UserActivityLogDTO> result = userActivityLogRepository.GetAll()
            .Where(x => (x.ObjectType == UserActivityObjectType.Product_IsHHPurchase || x.ObjectType == UserActivityObjectType.ApiStock_QtyAvailable) && x.ObjectId == sku)
            .Select(x => new UserActivityLogDTO
            {
                Activity = x.Content,
                Time = x.CreatedAt,
                User = x.UserName,
            })
            .OrderByDescending(x => x.Time)
            .ToList();
            return result;
        }

        [HttpPost]
        public RefreshPorductStockOutDTO RefreshProductStock(RefreshPorductStockInputDTO inputDTO)
        {
            if (inputDTO == null || inputDTO.Skus.IsNullOrEmpty())
            {
                throw new RequestValidationException("should pass skus parameter");
            }

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["sku"] = string.Join(";", inputDTO.Skus);
            tags["type"] = "RefreshProductStock";
            tags["trackid"] = Guid.NewGuid().ToString();

            CLSLogger.Info("RefreshProductStock.Request", inputDTO.ToJsonString(), tags);

            ProductDomainService productDomainService = new ProductDomainService();
            List<ProductStockVO> stockVOs = productDomainService.RecalculateSkuLockQty(inputDTO.Skus);

            if (stockVOs.IsNullOrEmpty())
            {
                throw new Exception("RecaculateSkuLockQtyException");
            }

            RefreshPorductStockOutDTO outputDTO = new RefreshPorductStockOutDTO();


            outputDTO.ProductStocks = stockVOs.Select(x => new ProductStockDTO
            {
                Sku = x.Sku,
                Qty_available = x.Qty_available,
                ExpiredDate = x.ExpiredDate,
                OriginalBestBefore = x.OriginalBestBefore,
                OriginalHHBestBefore = x.OriginalHHBestBefore,
                OriginalStock = x.Qty_available,
                OriginalHHStock = x.OriginalHHStock,
                LockQty = x.LockQty,
                //spark add stock 19/06/2022  syncecinx ? (stockMFD + int.Parse(_original_hh_stock.ToString())) : stockMFD,
                Stock = x.Sku.StartsWith("AUBXD") ? x.Qty_available - 2 : 
                            productRepository.GetAll().Where(y => y.SKU == x.Sku).FirstOrDefault().ECinxSKU  !=null ? 
                        (x.Qty_available + int.Parse(x.OriginalHHStock.ToString())) : x.Qty_available,

            }).ToList();

            //notify 188, motan
            backgroundJobManager.Enqueue<SyncAladdinStockJob, List<string>>(inputDTO.Skus, BackgroundJobPriority.High);

            CLSLogger.Info("RefreshProductStock.Response", outputDTO.ToJsonString(), tags);

            return outputDTO;
        }


        private void PushDingDingUpdateDataNotification(string username, List<ProductStockChangedDTO> changedskustock, bool isUpdate = false)
        {

            Dictionary<string, string> LogTags = new Dictionary<string, string>
            {
                ["jobid"] = Guid.NewGuid().ToString(),
                ["method"] = "PushDingDingUpdateDataNotification"
            };

            StringBuilder ddMessage = new StringBuilder();
            string title = string.Empty;
            string optinfo = "# (" + username + " ) " + DateTime.UtcNow.AddHours(8).ToString("yyyy-MM-dd HH:mm:ss") + "【北京时间】";
            if (isUpdate)
            {
                title = optinfo + " 将以下SKU批量更新了库存信息(仅显示变化库存) \n\n";
            }

            string ddtoken = "9578cfebb98e3e2132a9a02eaadad3013dceaf3749656d1b6a5825b50f8d05bc";
            ddMessage.Append(title);

            int pageSize = 15;
            int totalPage = changedskustock.Count % pageSize == 0 ? changedskustock.Count / pageSize : changedskustock.Count / pageSize + 1;

            for (int i = 0; i < totalPage; i++)
            {
                var Items = changedskustock.OrderBy(x => x.Sku).Skip(i * pageSize).Take(pageSize).ToList();
                CLSLogger.Info("将以下SKU批量更新了库存信息(从库存管理修改)", $"SKU：{Items.ToJsonString()} ", LogTags);
                foreach (var item in Items)
                {
                    ddMessage.Append($"{item.Sku} 当前有效库存(Before):{item.Qty_availableBefore} 修改库存数量为:{item.Qty_upload} 新有效库存(After): **_<font face=华云彩绘 color=#FF0000> {item.Qty_availableAfter} </font>_** \n\n");
                }
                DDNotificationProxy.MarkdownNotification(title, ddMessage.ToString(), ddtoken);
            }


        }


    }
}