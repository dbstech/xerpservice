﻿using Abp.Application.Services;

namespace MagicLamp.PMS
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class PMSAppServiceBase : ApplicationService
    {
        protected PMSAppServiceBase()
        {
            LocalizationSourceName = PMSConsts.LocalizationSourceName;
        }
    }
}