﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using MagicLamp.Flux.API.Models;
using MagicLamp.PMS.Proxy;
using Abp.UI;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.DTOs.Enums;
using Abp.BackgroundJobs;
using MagicLamp.XERP.BackgroundJob;
using MagicLamp.PMS.Infrastructure;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using MagicLamp.PMS.EntityFrameworkCore;


namespace MagicLamafterDto.PMS
{
    public class ProductCMQAppService : ApplicationService
    {

        private IRepository<WareHouseEntity, int> warehouseRepository;
        private IRepository<PackMaterialEntity, int> packMaterialRepository;
        private IRepository<ProductEntity, string> productRepository;
        private IBackgroundJobManager backgroundJobManager;

        public ProductCMQAppService(IRepository<WareHouseEntity, int> _warehouseRepository,
            IRepository<PackMaterialEntity, int> _packMaterialRepository,
            IRepository<ProductEntity, string> _productRepository,
            IBackgroundJobManager _backgroundJobManager)
        {
            warehouseRepository = _warehouseRepository;
            packMaterialRepository = _packMaterialRepository;
            backgroundJobManager = _backgroundJobManager;
            productRepository = _productRepository;
        }


        /// <summary>
        /// Sync product info for CMQ request
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public bool SyncProductInfo([FromBody]string json)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>();
            logTags["type"] = "syncproductinfo";
            logTags["trackid"] = Guid.NewGuid().ToString();
            try
            {
                CLSLogger.Info("同步产品信息请求报文", json, logTags);

                CMQPushMsgDTO cmqPushMsgDTO = json.ConvertFromJsonString<CMQPushMsgDTO>();

                if (cmqPushMsgDTO == null)
                {
                    CLSLogger.Error("同步产品信息发生异常", "消息体信息异常", logTags);
                    return false;
                }

                BaseMqDTO<ProductInfoMqDto> mqDto = cmqPushMsgDTO.MsgBody.ConvertFromJsonString<BaseMqDTO<ProductInfoMqDto>>();

                if (mqDto == null || mqDto.MessageBody == null || mqDto.MessageBody.After == null)
                {
                    CLSLogger.Error("同步产品信息发生异常", "消息体信息异常", logTags);
                    return false;
                }

                logTags["sku"] = mqDto.MessageBody.After.SKU;

                var afterDto = mqDto.MessageBody.After;
                var warehouse = warehouseRepository.FirstOrDefault(x => x.WareHouseId == afterDto.WarehouseID);

                if (warehouse == null)
                {
                    CLSLogger.Error("同步产品信息发生异常", "未找到对应warehouse", logTags);
                    return false;
                }

                if (!warehouse.IsWMS)
                {
                    CLSLogger.Info("同步产品信息", "非富勒管理产品不同步", logTags);
                    //非富勒管理产品不同步
                    return false;
                }

                int materialID = 0;
                PackMaterialEntity packMaterial = null;

                if (afterDto.PackMaterialID > 0)
                {
                    materialID = afterDto.PackMaterialID.Value;
                }
                else if (!afterDto.Materials.IsNullOrEmpty())
                {
                    packMaterial = packMaterialRepository.FirstOrDefault(x => x.Name == afterDto.Materials);
                }

                if (packMaterial == null)
                {
                    packMaterial = packMaterialRepository.FirstOrDefault(x => x.MaterialsID == materialID);
                }

                if (packMaterial == null)
                {
                    CLSLogger.Error("同步产品信息发生异常", "未找到对应packmaterial", logTags);
                    return false;
                }


                var sendProducts = new List<Product>();

                string activeFlag = mqDto.MessageBody.Action == MqActionEnum.Delete ? "N" : "Y";

                var pro = new Product
                {
                    CustomerID = afterDto.CustomerID,
                    Active_Flag = activeFlag,
                    SKU = afterDto.SKU.ToUpper(),
                    ReservedField02 = afterDto.SKU,
                    Descr_C = afterDto.ChineseName,
                    Descr_E = afterDto.EnglishName,
                    GrossWeight = afterDto.GrossWeight != null ? Convert.ToDecimal(afterDto.GrossWeight) : 0,
                    NetWeight = afterDto.NetWeight != null ? Convert.ToDecimal(afterDto.NetWeight) : 0,
                    SKULength = afterDto.Length != null ? Convert.ToDecimal(afterDto.Length) : 0,
                    SKUHigh = afterDto.High != null ? Convert.ToDecimal(afterDto.High) : 0,
                    SKUWidth = afterDto.Wide != null ? Convert.ToDecimal(afterDto.Wide) : 0,
                    Alternate_SKU1 = afterDto.Barcode1,
                    Alternate_SKU2 = afterDto.Barcode2,
                    //Alternate_SKU3保留
                    Alternate_SKU4 = afterDto.Barcode4,
                    ReservedField01 = packMaterial.Name,
                    ReservedField09 = packMaterial.Score.ToString(),
                    ReservedField08 = packMaterial.Cost.ToString(),
                    SKU_Group9 = afterDto.PurchasePeriodCode,
                };

                sendProducts.Add(pro);

                // Add Async Task Spark 2022.11.30
                backgroundJobManager.EnqueueAsync<SyncProductToFluxJob, List<Product>>(sendProducts, BackgroundJobPriority.High, TimeSpan.FromSeconds(3));
                //FluxResponse fluxResponse = FluxProxy.PushSKUData(sendProducts.ToArray());
                //if (fluxResponse == null || fluxResponse.Response == null || fluxResponse.Response.@return == null
                //    || fluxResponse.Response.@return.returnFlag != "1")
                //{
                //    CLSLogger.Error("同步产品信息发生异常", $"富勒返回异常，response：{fluxResponse.ToJsonString()}", logTags);
                //    throw new UserFriendlyException("同步产品信息发生异常", "富勒返回异常");
                //}
                //CLSLogger.Info("同步产品信息成功", $"{afterDto.SKU}   {afterDto.ChineseName}", logTags);
                return true;
            }
            catch (Exception ex)
            {
                if (ex is UserFriendlyException)
                {
                    throw ex;
                }
                CLSLogger.Error("同步产品信息发生异常", ex, logTags);
                throw new UserFriendlyException("同步产品信息发生异常", ex);
            }

        }


        public bool SyncProductPrice(List<ProductPriceMqDto> productPrices)
        {
            // CLSLogger.LogRequest("SyncProductPrice", productPrices);

            // if (productPrices.IsNullOrEmpty())
            // {
            //     throw new UserFriendlyException("invalid request");
            // }

            // int invalidCount = productPrices.Where(x => string.IsNullOrEmpty(x.SKU) || string.IsNullOrEmpty(x.BusinessCode)).Count();

            // if (invalidCount > 0)
            // {
            //     throw new UserFriendlyException("invalid sku or business in the request");
            // }

            // int pageSize = 1000;
            // int pageCount = Ultities.CaculatePageCount(productPrices.Count, pageSize);

            // for (int i = 0; i < pageCount; i++)
            // {
            //     var tempList = productPrices.Skip(i * pageSize).Take(pageSize).ToList();
            //     var skus = tempList.Select(x => x.SKU).ToList();
            //     var skuInfos = productRepository.GetAll().Where(x => skus.Contains(x.SKU)).ToList();

            //     tempList.ForEach(x =>
            //     {
            //         var temp = skuInfos.FirstOrDefault(item => item.SKU == x.SKU);
            //         if (temp != null)
            //         {
            //             x.Currency = temp.Currency;
            //         }
            //     });

            //     backgroundJobManager.EnqueueAsync<ProductPriceUpdateNotifyJob, List<ProductPriceMqDto>>(tempList);
            // }

            return true;
        }


        public string SyncSpecificSkuToFlux(List<string> skus)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>();
            logTags["type"] = "syncspecificskutoflux";
            var sendProducts = new List<Product>();
            StringBuilder log = new StringBuilder();
            try
            {
                foreach (var sku in skus)
                {
                    log.Append($"当前sku：{sku} \n\n");
                    var skuInfo = productRepository.FirstOrDefault(sku);
                    var warehouse = warehouseRepository.FirstOrDefault(x => x.WareHouseId == skuInfo.WarehouseID);

                    if (warehouse == null)
                    {
                        log.Append($"未找到对应warehouse \n\n");
                        CLSLogger.Error("同步产品信息发生异常", "未找到对应warehouse", logTags);
                        throw new UserFriendlyException("同步产品信息发生异常，未找到对应warehouse");
                    }

                    var packMaterial = packMaterialRepository.FirstOrDefault(x => x.MaterialsID == skuInfo.MaterialsID);

                    if (packMaterial == null)
                    {
                        log.Append($"未找到对应packmaterial \n\n");
                        CLSLogger.Error("同步产品信息发生异常", "未找到对应packmaterial", logTags);
                        throw new UserFriendlyException("同步产品信息发生异常，未找到对应packmaterial");
                    }


                    if (!warehouse.IsWMS)
                    {
                        log.Append($"非富勒管理产品不同步 \n\n");
                        CLSLogger.Info("同步产品信息", "非富勒管理产品不同步", logTags);
                        //非富勒管理产品不同步
                        continue;
                    }

                    string activeFlag = "Y";

                    var pro = new Product
                    {
                        CustomerID = skuInfo.CustomerID,
                        Active_Flag = activeFlag,
                        SKU = skuInfo.SKU.ToUpper(),
                        ReservedField02 = skuInfo.SKU,
                        Descr_C = skuInfo.ChineseName,
                        Descr_E = skuInfo.EnglishName,
                        GrossWeight = skuInfo.GrossWeight != null ? Convert.ToDecimal(skuInfo.GrossWeight) : 0,
                        NetWeight = skuInfo.NetWeight != null ? Convert.ToDecimal(skuInfo.NetWeight) : 0,
                        SKULength = skuInfo.Length != null ? Convert.ToDecimal(skuInfo.Length) : 0,
                        SKUHigh = skuInfo.High != null ? Convert.ToDecimal(skuInfo.High) : 0,
                        SKUWidth = skuInfo.Wide != null ? Convert.ToDecimal(skuInfo.Wide) : 0,
                        Alternate_SKU1 = skuInfo.Barcode1,
                        Alternate_SKU2 = skuInfo.Barcode2,
                        //Alternate_SKU3保留
                        Alternate_SKU4 = skuInfo.Barcode4,
                        ReservedField01 = packMaterial.Name,
                        ReservedField09 = packMaterial.Score.ToString(),
                        ReservedField08 = packMaterial.Cost.ToString(),
                        SKU_Group9 = skuInfo.PurchasePeriodCode,
                    };

                    sendProducts.Add(pro);
                    log.Append($"添加完成； \n\n");
                }


                // Add Async Task Spark 2022.11.30
                backgroundJobManager.EnqueueAsync<SyncProductToFluxJob, List<Product>>(sendProducts, BackgroundJobPriority.High, TimeSpan.FromSeconds(3));
                //FluxResponse fluxResponse = FluxProxy.PushSKUData(sendProducts.ToArray());

                //if (fluxResponse == null || fluxResponse.Response == null || fluxResponse.Response.@return == null
                //    || fluxResponse.Response.@return.returnFlag != "1")
                //{
                //    log.Append($"富勒返回异常，response：{fluxResponse.ToJsonString()} \n\n");
                //    CLSLogger.Error("同步产品信息发生异常", $"富勒返回异常，response：{fluxResponse.ToJsonString()}", logTags);
                //    throw new UserFriendlyException("同步产品信息发生异常", "富勒返回异常");
                //}
            }
            catch (UserFriendlyException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("同步产品信息发生异常", ex.ToString());
            }

            return log.ToString();
        }


        /// <summary>
        /// 刷新产品库存 todo
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public bool RefreshProductStock([FromBody]string json)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>();
            logTags["type"] = "refreshproductstock";
            logTags["trackid"] = Guid.NewGuid().ToString();
            try
            {
                CLSLogger.Info("刷新产品库存请求报文", json, logTags);

                CMQPushMsgDTO cmqPushMsgDTO = json.ConvertFromJsonString<CMQPushMsgDTO>();

                if (cmqPushMsgDTO == null)
                {
                    CLSLogger.Error("刷新产品库存发生异常", "消息体信息异常", logTags);
                    return false;
                }

                BaseMqDTO<List<string>> mqDto = cmqPushMsgDTO.MsgBody.ConvertFromJsonString<BaseMqDTO<List<string>>>();

                if (mqDto == null || mqDto.MessageBody == null || mqDto.MessageBody.Count <= 0)
                {
                    CLSLogger.Error("刷新产品库存发生异常", "消息体信息异常", logTags);
                    return false;
                }

                HEERPDbContext hEERPDbContext = new HEERPDbContextFactory().CreateDbContext(null);
          
                //hEERPDbContext.Database
            }
            catch (Exception ex)
            {
                CLSLogger.Error("刷新产品库存发生异常", ex, logTags);
                if (ex is UserFriendlyException)
                {
                    throw ex;
                }
                throw new UserFriendlyException("刷新产品库存发生异常", ex);
            }

            return true;
        }


    }
}
