﻿using Abp.Dependency;
using Abp.Events.Bus.Exceptions;
using Abp.Events.Bus.Handlers;
using MagicLamp.PMS.Proxy;
using Sentry;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS.EventHandlers
{
    class AbpExceptionHandler : IEventHandler<AbpHandledExceptionData>, ITransientDependency
    {
        public void HandleEvent(AbpHandledExceptionData eventData)
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "abpexception";
            CLSLogger.Error("XERPService发生未捕获的异常", eventData.Exception, tags);
            SentrySdk.CaptureException(eventData.Exception);
        }


    }
}