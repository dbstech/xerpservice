﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class BusinessAppService : PMSCrudAppServiceBase<BusinessEntity, BusinessDTO, BusinessDTO, int>
    {
        private IRepository<BusinessEntity, int> businessRepository;

        public BusinessAppService(IRepository<BusinessEntity, int> _businessRepository) : base(_businessRepository)
        {
            businessRepository = _businessRepository;
        }
    }
}
