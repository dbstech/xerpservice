using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.Extensions;
using Abp.UI;
using CsvHelper;
using EFCore.BulkExtensions;
using MagicLamp.PMS.Domain;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Enums;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Proxy;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Npoi.Mapper;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace MagicLamp.PMS
{
    public class PaymentTransactionAppService : ApplicationService
    {
        private IAccountManager _accountManager;
        private IDbContextProvider<PMSDbContext> _pmsDbContextProvider;
        private IRepository<OrderEntity, int> _orderRepository;
        public PaymentTransactionAppService(
            IAccountManager accountManager,
            IDbContextProvider<PMSDbContext> pmsDbContextProvider,
            IRepository<OrderEntity, int> orderRepository
        )
        {
            _accountManager = accountManager;
            _pmsDbContextProvider = pmsDbContextProvider;
            _orderRepository = orderRepository;
        }

        private decimal GetExchangeRate(ISheet sheet)
        {
            for (int rowIndex = sheet.LastRowNum; rowIndex >= 0; rowIndex--)
            {
                IRow row = sheet.GetRow(rowIndex);
                if (row == null) continue;

                ICell cell = row.GetCell(4);
                DataFormatter formatter = new DataFormatter();
                string cellValue = formatter.FormatCellValue(cell);

                if (cell != null && cellValue == "FX Rate(NZD)")
                {
                    ICell exchangeRateCell = sheet.GetRow(rowIndex + 1).GetCell(4);
                    string exchangeRateCellValue = formatter.FormatCellValue(exchangeRateCell);
                    return Convert.ToDecimal(exchangeRateCellValue);
                }
            }
            throw new Exception("Unable to find exchange rate");
        }

        private void RemoveRedundantRows(ISheet sheet)
        {
            for (int rowIndex = sheet.LastRowNum; rowIndex >= 0; rowIndex--)
            {
                IRow row = sheet.GetRow(rowIndex);
                string cellValue = row.GetCell(0)?.StringCellValue;
                sheet.RemoveRow(row);

                if (cellValue == "Total：")
                {
                    break;
                }
            }
        }

        [HttpPost]
        [Authorize]
        public bool UploadUnionPayTransactions(IFormFile file)
        {
            // TODO: validate file extension/format
            if (file == null || file.Length <= 0)
            {
                throw new UserFriendlyException("文件大小异常，请选择正确的文件");
            }

            try
            {
                var workbook = new HSSFWorkbook(file.OpenReadStream());

                var sheet = workbook.GetSheet("Merchant");

                // remove first 2 rows making the table header the first row
                sheet.ShiftRows(2, sheet.LastRowNum, -2);

                decimal exchangeRate = GetExchangeRate(sheet);

                // clean up excel sheet so only the header and row data remains
                RemoveRedundantRows(sheet);

                var mapper = new Npoi.Mapper.Mapper(workbook);

                // npoi mapper creates an extra object with all properties set to default value for some reason
                List<UnionPayPaymentTransactionEntity> transactions = mapper.Take<UnionPayPaymentTransactionEntity>("Merchant")
                                                                        .Where(x => x.Value != null && !x.Value.TraceNo.IsNullOrWhiteSpace())
                                                                        .Select(x => x.Value)
                                                                        .ToList();
                var pmsDbContext = _pmsDbContextProvider.GetDbContext();

                transactions.ForEach(x =>
                {
                    x.UpdatedDate = DateTime.UtcNow;
                    x.ExchangeRate = exchangeRate;
                });
                pmsDbContext.BulkInsertOrUpdate(transactions, new BulkConfig
                {
                    PropertiesToExclude = new List<string> { nameof(UnionPayPaymentTransactionEntity.CreatedDate) },
                });

                _accountManager.LogUserActivity(new LogUserActivityVO
                {
                    ObjectType = UserActivityObjectType.FileName,
                    ObjectId = file.FileName,
                    Action = UserActionEnum.UploadFile.ToString(),
                    CreatedAt = DateTime.UtcNow,
                });
            }
            catch (Exception ex)
            {
                CLSLogger.ErrorWithSentry("UnionPay upload error", ex);
                throw new UserFriendlyException("文件上传失败");
            }

            return true;
        }

        [HttpPost]
        [Authorize]
        public bool UploadLatiPayTransactions(IFormFile file)
        {

            // V1 version
            //if (file == null || file.Length <= 0)
            //{
            //    throw new UserFriendlyException("文件大小异常，请选择正确的文件");
            //}

            //try
            //{
            //    var workbook = new XSSFWorkbook(file.OpenReadStream());

            //    var sheet = workbook.GetSheet("TRANSACTION");
            //    var mapper = new Npoi.Mapper.Mapper(workbook);
            //    List<LatiPayPaymentTransactionEntity> transactions = mapper.Take<LatiPayPaymentTransactionEntity>("TRANSACTION")
            //                                                                .Where(x => x.Value != null && !x.Value.LatiPayTransactionId.IsNullOrWhiteSpace())
            //                                                                .Select(x => x.Value)
            //                                                                .ToList();
            //    var pmsDbContext = _pmsDbContextProvider.GetDbContext();

            //    transactions.ForEach(x => x.UpdatedDate = DateTime.UtcNow);
            //    pmsDbContext.BulkInsertOrUpdate(transactions, new BulkConfig
            //    {
            //        PropertiesToExclude = new List<string> { nameof(LatiPayPaymentTransactionEntity.CreatedDate) },
            //    });

            //    _accountManager.LogUserActivity(new LogUserActivityVO
            //    {
            //        ObjectType = UserActivityObjectType.FileName,
            //        ObjectId = file.FileName,
            //        Action = UserActionEnum.UploadFile.ToString(),
            //        CreatedAt = DateTime.UtcNow,
            //    });
            //}
            //catch (Exception ex)
            //{
            //    CLSLogger.ErrorWithSentry("LatiPay upload error", ex);
            //    throw new UserFriendlyException("文件上传失败");
            //}

            //return true;

            //V2 Version
            if (file == null || file.Length <= 0)
            {
                throw new UserFriendlyException("文件大小异常，请选择正确的文件");
            }

            try
            {
                var workbook = new XSSFWorkbook(file.OpenReadStream());

                var sheet = workbook.GetSheet("TRANSACTION");
                var mapper = new Npoi.Mapper.Mapper(workbook);
                List<LatiPayPaymentTransactionV2Entity> transactions = mapper.Take<LatiPayPaymentTransactionV2Entity>("TRANSACTION")
                                                                            .Where(x => x.Value != null && !x.Value.LatiPayTransactionId.IsNullOrWhiteSpace())
                                                                            .Select(x => x.Value)
                                                                            .ToList();
                var pmsDbContext = _pmsDbContextProvider.GetDbContext();

                transactions.ForEach(x => x.UpdatedDate = DateTime.UtcNow);
                pmsDbContext.BulkInsertOrUpdate(transactions, new BulkConfig
                {
                    PropertiesToExclude = new List<string> { nameof(LatiPayPaymentTransactionV2Entity.CreatedDate) },
                });

                _accountManager.LogUserActivity(new LogUserActivityVO
                {
                    ObjectType = UserActivityObjectType.FileName,
                    ObjectId = file.FileName,
                    Action = UserActionEnum.UploadFile.ToString(),
                    CreatedAt = DateTime.UtcNow,
                });
            }
            catch (Exception ex)
            {
                CLSLogger.ErrorWithSentry("LatiPayV2 upload error", ex);
                throw new UserFriendlyException("文件上传失败");
            }

            return true;
        }

        [HttpPost]
        [Authorize]
        public bool UploadAllInPayTransactions(IFormFile file)
        {
            if (file == null || file.Length <= 0)
            {
                throw new UserFriendlyException("文件大小异常，请选择正确的文件");
            }

            try
            {
                var workbook = new XSSFWorkbook(file.OpenReadStream());

                var sheet = workbook.GetSheetAt(0);
                sheet.ShiftRows(1, sheet.LastRowNum, -1);

                var mapper = new Npoi.Mapper.Mapper(workbook);

                mapper.Map<AllInPayPaymentTransactionEntity>(
                    0,
                    o => o.TransactionId,
                    (column, target) =>
                    {
                        if (column.HeaderValue == null || column.CurrentValue == null) return false;
                        ((AllInPayPaymentTransactionEntity)target).TransactionId = Convert.ToInt64(column.CurrentValue);
                        return true;
                    },
                    null
                );

                mapper.Map<AllInPayPaymentTransactionEntity>(
                    20,
                    o => o.OriginalTransactionId,
                    (column, target) =>
                    {
                        if (column.HeaderValue == null || column.CurrentValue == null) return false;

                        if (column.CurrentValue.ToString() == "-")
                        {
                            ((AllInPayPaymentTransactionEntity)target).OriginalTransactionId = null;
                        }
                        else
                        {
                            ((AllInPayPaymentTransactionEntity)target).OriginalTransactionId = Convert.ToInt64(column.CurrentValue);
                        }
                        return true;
                    },
                    null
                );

                List<AllInPayPaymentTransactionEntity> transactions = mapper.Take<AllInPayPaymentTransactionEntity>()
                                                                            .Where(x => x.Value != null && x.Value.TransactionId != 0)
                                                                            .Select(x => x.Value)
                                                                            .ToList();

                List<long> transactionIds = transactions.Select(x => x.TransactionId).ToList();
                var pmsDbContext = _pmsDbContextProvider.GetDbContext();

                transactions.ForEach(x => x.UpdatedDate = DateTime.UtcNow);
                pmsDbContext.BulkInsertOrUpdate(transactions, new BulkConfig
                {
                    PropertiesToExclude = new List<string> { nameof(AllInPayPaymentTransactionEntity.CreatedDate) },
                });

                _accountManager.LogUserActivity(new LogUserActivityVO
                {
                    ObjectType = UserActivityObjectType.FileName,
                    ObjectId = file.FileName,
                    Action = UserActionEnum.UploadFile.ToString(),
                    CreatedAt = DateTime.UtcNow,
                });
            }
            catch (Exception ex)
            {
                CLSLogger.ErrorWithSentry("AllInPay upload error", ex);
                throw new UserFriendlyException("文件上传失败");
            }
            return true;
        }

        [HttpPost]
        [Authorize]
        public bool UploadPoliPayTransactions(IFormFile file)
        {
            if (file == null || file.Length <= 0)
            {
                throw new UserFriendlyException("文件大小异常，请选择正确的文件");
            }

            try
            {
                List<PoliPayPaymentTransactionEntity> transactions = new List<PoliPayPaymentTransactionEntity> { };
                using (var reader = new StreamReader(file.OpenReadStream()))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    transactions = csv.GetRecords<PoliPayPaymentTransactionEntity>().ToList();
                }
                var pmsDbContext = _pmsDbContextProvider.GetDbContext();

                transactions.ForEach(x => x.UpdatedDate = DateTime.UtcNow);
                pmsDbContext.BulkInsertOrUpdate(transactions, new BulkConfig
                {
                    PropertiesToExclude = new List<string> { nameof(PoliPayPaymentTransactionEntity.CreatedDate) },
                });

                _accountManager.LogUserActivity(new LogUserActivityVO
                {
                    ObjectType = UserActivityObjectType.FileName,
                    ObjectId = file.FileName,
                    Action = UserActionEnum.UploadFile.ToString(),
                    CreatedAt = DateTime.UtcNow,
                });
            }
            catch (Exception ex)
            {
                CLSLogger.ErrorWithSentry("PoliPay upload error", ex);
                throw new UserFriendlyException("文件上传失败");
            }
            return true;
        }
    }
}