﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Web.Models;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TimeZoneConverter;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class CurrencyAppService : PMSCrudAppServiceBase<CurrencyEntity, CurrencyDTO, CurrencyDTO, string>
    {

        private const string ClogTagType = "Exchangeratefrom188";
        private TimeZoneInfo timeZoneNZ = TZConvert.GetTimeZoneInfo("Pacific/Auckland");

        Dictionary<string, string> clogTags = new Dictionary<string, string>
        {
            ["type"] = ClogTagType
        };

        private IRepository<CurrencyEntity, string> currencyRepository;
        private IRepository<ExchangeRate, string> exchangeRateRepository;
        public CurrencyAppService(IRepository<CurrencyEntity, string> _currencyRepository, IRepository<ExchangeRate, string> exchangeRateRepository) : base(_currencyRepository)
        {
            currencyRepository = _currencyRepository;
            this.exchangeRateRepository = exchangeRateRepository;
        }

        [DontWrapResult]
        public ResultResponseInfo SaveExchangeRate(RatesDto ExchangeRatesDTO)
        {
            ResultResponseInfo result = new ResultResponseInfo();

            var exchangeratefrom188 = ExchangeRatesDTO.ToJsonString();
            CLSLogger.Info("SyncCreateOrderStatus", exchangeratefrom188, clogTags);


            try
            {
                var exchangeRates = exchangeRateRepository.GetAll().ToList();

                InsertOrUpdateCurrency(exchangeRates, ExchangeRatesDTO.from, ExchangeRatesDTO.to, ExchangeRatesDTO.rate);
                InsertOrUpdateCurrency(exchangeRates, ExchangeRatesDTO.to, ExchangeRatesDTO.from, 1 / ExchangeRatesDTO.rate);
            }
            catch (Exception ex)
            {
                result.Code = "101";
                result.Message = ex.Message.ToString();
                return result;
            }

            result.Code = "100";
            result.Message = "success";
            return result;


        }

        private void InsertOrUpdateCurrency(List<ExchangeRate> exchangeRates, string from, string to, decimal rate)
        {

            DateTime CurrentTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);

            var exchangeRate = exchangeRates.FirstOrDefault(x => x.From == from && x.To == to);
            if (exchangeRate == null)
            {
                exchangeRateRepository.Insert(new ExchangeRate
                {
                    From = from,
                    To = to,
                    Rate = rate,
                    CreatedDate = CurrentTime,
                    UpdatedDate = CurrentTime
                });
            }
            else
            {
                exchangeRate.Rate = rate;
                exchangeRate.UpdatedDate = CurrentTime;
                exchangeRateRepository.Update(exchangeRate);
            }
        }


        public class ResultResponseInfo
        {
            public string Code { get; set; }
            public string Message { get; set; }
        }

        public class RatesDto
        {
            public string from { get; set; }
            public string to { get; set; }
            public decimal rate { get; set; }
            public DateTime? CreatedDate { get; set; }
            public DateTime? UpdatedDate { get; set; }
        }

    }
}

