﻿using Abp.Application.Services;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Json;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.BondedWarehouseQingDao;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.PMS
{
    /// <summary>
    /// 青岛保税仓
    /// </summary>
    public class BondedWarehouseQingDaoAppService : ApplicationService
    {
        private const string ClogTagType = "BondedWarehouseQingDao";

        private IBackgroundJobManager backgroundJobManager;
        private IRepository<OrderPackTradeEntity, int> orderPackTradeRepository;

        public BondedWarehouseQingDaoAppService(IBackgroundJobManager _backgroundJobManager,
            IRepository<OrderPackTradeEntity, int> _orderPackTradeRepository)
        {
            backgroundJobManager = _backgroundJobManager;
            orderPackTradeRepository = _orderPackTradeRepository;
        }

        public void CreateOrder(QingDaoBondedOrderInputDTO inputDTO)
        {
            backgroundJobManager.Enqueue<YiTongBaoCreateOrderJob, QingDaoBondedOrderInputDTO>(inputDTO, BackgroundJobPriority.Normal);
        }

        public PushOrdersOutputDTO RepushListOfOrder(List<string> listOrderID)
        {
            List<OrderPackTradeEntity> listOrderPackTrade = orderPackTradeRepository.GetAll().Where(x => listOrderID.Contains(x.OrderID)).ToList();

            List<string> listOrderIDFound = listOrderPackTrade.Select(x => x.OrderID).ToList();
            List<string> listOrderIDNotFound = listOrderID.Except(listOrderIDFound).ToList();

            foreach (var item in listOrderPackTrade)
            {
                try
                {
                    QingDaoBondedOrderInputDTO input = item.OriginalInfo.ConvertFromJsonString<QingDaoBondedOrderInputDTO>();
                    backgroundJobManager.Enqueue<YiTongBaoCreateOrderJob, QingDaoBondedOrderInputDTO>(input, BackgroundJobPriority.Normal);
                }
                catch (Exception ex)
                {
                    Dictionary<string, string> clogTags = new Dictionary<string, string>
                    {
                        ["type"] = ClogTagType,
                        ["orderid"] = item.OrderID
                    };

                    CLSLogger.Error("重推青岛保税仓失败", ex, clogTags);
                }
            }

            PushOrdersOutputDTO output = new PushOrdersOutputDTO
            {
                Success = true,
                PushedOrdersCount = listOrderPackTrade.Count(),
                IdsNotFound = listOrderIDNotFound
            };

            return output;
        }
    }
}