﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace MagicLamp.PMS
{

    //TODO: App service need to be tested
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class PurchasingAppService : PMSCrudAppServiceBase<PurchaseOrderHeaderEntity, PurchaseOrderHeaderDTO, PurchaseOrderHeaderDTO, string>
    {
        private IRepository<PurchaseOrderHeaderEntity, string> purchaseOrderHeaderRepository;
        private IRepository<EditLogEntity, int> editLogRepository;
        private IRepository<ProductEntity, string> productRepository;
        private IRepository<PurchaseOrderLineEntity, int> purchaseOrderLineRepository;
        private IRepository<HHBindedWaveNoEntity, string> hhBindedWaveNoRepository;
        private IRepository<OrderPackReservedHHSKUEntity, long> orderPackReservedHHSKURepository;

        public PurchasingAppService(IRepository<PurchaseOrderHeaderEntity, string> _purchaseOrderHeaderRepository,
            IRepository<EditLogEntity, int> _editLogRepository,
            IRepository<ProductEntity, string> _productRepository,
            IRepository<PurchaseOrderLineEntity, int> _purchaseOrderLineRepository,
            IRepository<HHBindedWaveNoEntity, string> _hhBindedWaveNoRepository,
            IRepository<OrderPackReservedHHSKUEntity, long> _orderPackReservedHHSKURepository) : base(_purchaseOrderHeaderRepository)
        {
            purchaseOrderHeaderRepository = _purchaseOrderHeaderRepository;
            editLogRepository = _editLogRepository;
            productRepository = _productRepository;
            purchaseOrderLineRepository = _purchaseOrderLineRepository;
            hhBindedWaveNoRepository = _hhBindedWaveNoRepository;
            orderPackReservedHHSKURepository = _orderPackReservedHHSKURepository;
        }

        private void InsertPurchasingOrderHeader(PurchaseOrderHeaderDTO input, string user)
        {
            input.CreateBy = user;
            input.LastUpdateBy = user;
            input.CreateDate = DateTime.UtcNow;
            input.LastUpdateDate = DateTime.UtcNow;

            EditLogEntity log = new EditLogEntity
            {
                System = "pms",
                Type = "Add_Order",
                Uesr = user,
                DateTime = DateTime.UtcNow
            };

            var entity = input.MapTo<PurchaseOrderHeaderEntity>();
            purchaseOrderHeaderRepository.Insert(entity);

            log.Content = $"新增订单成功; Content: {input.ToJsonString()}";
            //Add log
            editLogRepository.Insert(log);
        }

        private void InsertPurchasingOrderLine(PurchaseOrderLineDTO input, string user)
        {
            input.CreateBy = user;
            input.CreateDate = DateTime.UtcNow;
            input.LastUpdateBy = user;
            input.LastUpdateDate = DateTime.UtcNow;

            EditLogEntity log = new EditLogEntity
            {
                System = "pms",
                Type = "Add_OrderLine",
                Uesr = user,
                DateTime = DateTime.UtcNow
            };

            var entity = input.MapTo<PurchaseOrderLineEntity>();
            purchaseOrderLineRepository.Insert(entity);

            log.Content = $"新增订单行成功; Content: {input.ToJsonString()}";
            //Add log
            editLogRepository.Insert(log);
        }

        public PurchasingOutputDTO AddNewPurchasingOrder(PurchaseOrderHeaderDTO input)
        {

            PurchasingOutputDTO result = new PurchasingOutputDTO();

            if (input == null || input.PurchasingOrderNo.IsNullOrEmpty())
            {
                throw new RequestValidationException("采购单表头不能为空，采购单号不能为空。");
            }

            var existed = purchaseOrderHeaderRepository.FirstOrDefault(x => x.PurchasingOrderNo == input.PurchasingOrderNo);

            if (existed != null)
            {
                result.Success = false;
                result.Message = "单号已存在";
                return result;
            }
            else
            {
                try
                {
                    //插入订单头
                    InsertPurchasingOrderHeader(input, "System");
                    result.Success = true;
                    result.Data = input;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Message = $"保存失败，原因：{ex.Message}";
                }
            }

            return result;
        }

        public PurchasingOutputDTO AddNewPurchasingOrderLine(PurchaseOrderLineDTO input)
        {
            PurchasingOutputDTO result = new PurchasingOutputDTO();

            if (input == null || input.ItemId.IsNullOrEmpty())
            {
                throw new RequestValidationException("采购单行不能为空，采购单行SKU不能为空。");
            }

            var productInfo = productRepository.FirstOrDefault(x => x.SKU == input.ItemId);

            if (productInfo != null)
            {
                input.ItemDescription = productInfo.ChineseName;
            }

            try
            {
                //插入订单行
                InsertPurchasingOrderLine(input, "System");
                result.Success = true;
                result.Data = input;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = $"保存失败，原因：{ex.Message}";
            }

            return result;
        }

        /// <summary>
        /// get purchase order line that are related to MFD order
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public List<PurchaseOrderLineDTO> GetOrderLineRelatedToMFD(PurchaseOrderHeaderDTO input)
        {
            List<PurchaseOrderLineDTO> result = new List<PurchaseOrderLineDTO>();

            var waveNo = input.PurchasingOrderNo;

            var listPoOrderLine = purchaseOrderLineRepository.GetAll().Where(x => x.PoHeaderId == waveNo).ToList();

            var bindedWaveNo = hhBindedWaveNoRepository.FirstOrDefault(x => x.WaveNo == waveNo);

            if (bindedWaveNo == null)
            {
                return null;
            }

            var listOrderPackNoBondedToWaveNo = bindedWaveNo.OrdersIncluded.ConvertFromJsonString<List<string>>();

            if (listOrderPackNoBondedToWaveNo.IsNullOrEmpty())
            {
                return null;
            }

            var listMFDOrderReservedHHSKU = orderPackReservedHHSKURepository.GetAll().Where(x => listOrderPackNoBondedToWaveNo.Contains(x.OrderPackNo) && x.Partner.IsNullOrEmpty()).ToList();

            //reserved sku realted to mfd order
            var listMFDOrderGroupedReservedSKU = listMFDOrderReservedHHSKU.GroupBy(x => new
            {
                x.SKU,
                x.Price
            });

            var listMFDOrderReservedSKU = listMFDOrderGroupedReservedSKU.Select(x => x.Key.SKU).ToList();

            var listMFDOrderSKUDetail = listMFDOrderGroupedReservedSKU.Select(x => new OrderPackDetailDTO
            {
                sku = x.Key.SKU,
                qty = x.Sum(item => item.Qty.Value),
                price = x.Key.Price
            }).ToList();

            foreach (var item in listPoOrderLine)
            {
                if (listMFDOrderReservedSKU.Contains(item.Sku))
                {
                    var elementToBeAdded = item.MapTo<PurchaseOrderLineDTO>();
                    //get accurate qty ordered for MFD order
                    var skuDetail = listMFDOrderSKUDetail.FirstOrDefault(x => x.sku == item.Sku && x.price == item.TaxedCost);

                    elementToBeAdded.QTY = skuDetail.qty;

                    result.Add(elementToBeAdded);
                }
            }

            return result;
        }

    }
}
