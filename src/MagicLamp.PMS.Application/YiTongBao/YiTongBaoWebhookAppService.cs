using Abp.Application.Services;
using MagicLamp.PMS.Proxy.YiTongBaoOMSAPI;
using Newtonsoft.Json.Linq;
using Abp.AutoMapper;
using MagicLamp.PMS.Proxy.YiTongBaoOMSAPI.DeliveryOrderConfirm;
using Abp.Domain.Repositories;
using MagicLamp.PMS.Entities;
using System;
using MagicLamp.PMS.Proxy;
using Abp.Web.Models;
// using Abp.Extensions;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Linq;
using MagicLamp.PMS.YiTongBao;
using MagicLamp.XERP.BackgroundJob;
using MagicLamp.PMS.Proxy.YiTongBaoOMSAPI.OrderProcessReport;
using MagicLamp.PMS.Infrastructure.Extensions;
using Abp.Extensions;

namespace MagicLamafterDto.PMS
{
    /// <summary>
    /// 青岛保税仓
    /// </summary>
    [DontWrapResult]
    public class YiTongBaoWebhookAppService : ApplicationService
    {
        private IRepository<OrderEntity> _orderRepository;
        private IRepository<OrderPackEntity> _orderPackRepository;
        private IRepository<DeliveryOrderConfirmEntity, long> _deliverOrderConfirmRepository;
        private IRepository<OrderLogEntity> _orderLogRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        protected HttpContext HttpContext => _httpContextAccessor.HttpContext;

        public YiTongBaoWebhookAppService(
            IRepository<OrderEntity> orderRepository,
            IRepository<OrderPackEntity> orderPackRepository,
            IRepository<OrderLogEntity> orderLogRepository,
            IHttpContextAccessor httpContextAccessor,
            IRepository<DeliveryOrderConfirmEntity, long> deliverOrderConfirmRepository)
        {
            _orderRepository = orderRepository;
            _orderPackRepository = orderPackRepository;
            _orderLogRepository = orderLogRepository;
            _httpContextAccessor = httpContextAccessor;
            _deliverOrderConfirmRepository = deliverOrderConfirmRepository;
        }
        public WebhookResponse DeliveryOrderConfirm()
        {
            string requestString = string.Empty;

            using (StreamReader reader = new StreamReader(HttpContext.Request.Body, Encoding.UTF8))
            {
                requestString = reader.ReadToEnd();
            }

            CLSLogger.LogRequest("青岛保税仓发货单确认请求", requestString);

            var response = new WebhookResponse
            {
                code = "1",
                success = false,
            };

            string deliveryOrderCode = null;

            try
            {
                JObject requestJson = JObject.Parse(requestString);
                WebhookRequest request = requestJson.MapTo<WebhookRequest>();

                YiTongBaoOMSProxy ytbProxy = new YiTongBaoOMSProxy();
                if (ytbProxy.ValidateWebhookRequest(request) == false)
                {
                    response.msg = "签名认证错误";
                    throw new Exception("签名认证错误");
                }

                // decode url encoded data
                string dataString = System.Web.HttpUtility.UrlDecode(request.data);

                // parse string to json
                JObject jObject = JObject.Parse(dataString);

                // map json to object
                DeliveryOrderConfirm data = jObject.ToObject<DeliveryOrderConfirm>();

                // find orderpack, return fail if not found
                deliveryOrderCode = data.DeliveryOrder.DeliveryOrderCode;
                var orderPackToMatch = _orderPackRepository.FirstOrDefault(x => x.carrierId == deliveryOrderCode);

                if (orderPackToMatch == null)
                {
                    response.msg = $"未在任何包裹里找到对应的出库单号：{deliveryOrderCode}";
                    throw new Exception($"未在任何包裹里找到对应的出库单号：{deliveryOrderCode}");
                }

                var orderPackNoToMatch = orderPackToMatch.orderPackNo;

                var order = _orderRepository.FirstOrDefault(x => x.notes.Trim() == orderPackNoToMatch);

                if (order == null)
                {
                    response.msg = $"未在任何订单备注里找到和该包裹关联的订单：{orderPackNoToMatch}";
                    throw new Exception($"未在任何订单备注里找到和该包裹关联的订单：{orderPackNoToMatch}");
                }

                var orderPack = _orderPackRepository.FirstOrDefault(x => x.orderID == order.orderid);

                // set status to dispatched/已配货
                orderPack.status = 4;
                // TODO: validate expressCode is not null or whitespace
                // TODO: refactor code for change in request data structure 

                // api should always have only 1 package in packages list
                var package = data.Packages.First();
                // orderPack.carrierId = package.ExpressCode;

                // TimeZoneInfo chinaStandardTime = TimeZoneInfo.FindSystemTimeZoneById("Asia/Shanghai");
                // DateTime orderConfirmTime = TimeZoneInfo.ConvertTime(data.DeliveryOrder.OrderConfirmTime, chinaStandardTime).ToUniversalTime();

                // use current time as outtime
                orderPack.out_time = DateTime.UtcNow.ConvertUTCTimeToNZTime();

                // log batch information
                _deliverOrderConfirmRepository.Insert(new DeliveryOrderConfirmEntity
                {
                    OrderId = orderPack.orderID,
                    ExpressCode = package.ExpressCode
                });
            }
            catch (Exception ex)
            {
                // TODO: add deliveryOrderCode to message?
                response.msg = response.msg.IsNullOrWhiteSpace() ? "发货单确认请求失败" : response.msg;
                CLSLogger.Error("青岛保税仓发货单确认失败", ex);
                SendNotificationToDingDingJob ddJob = new SendNotificationToDingDingJob();
                ddJob.SendBondedWarehouseQingDaoAlert($"青岛保税仓发货单确认失败，请检查, 出库单号: {deliveryOrderCode}, 失败原因：" + response.msg);
            }

            response.code = "0";
            response.success = true;
            CLSLogger.LogResponse("青岛保税仓发货单确认响应", response);

            return response;
        }

        public WebhookResponse OrderProcessReport()
        {
            string requestString = string.Empty;

            using (StreamReader reader = new StreamReader(HttpContext.Request.Body, Encoding.UTF8))
            {
                requestString = reader.ReadToEnd();
            }

            CLSLogger.LogRequest("青岛保税仓订单流水通知请求", requestString);

            var response = new WebhookResponse
            {
                code = "1",
                success = false,
            };

            string orderId = null;

            try
            {
                JObject requestJson = JObject.Parse(requestString);
                WebhookRequest request = ObjectMapper.Map<WebhookRequest>(requestJson);

                YiTongBaoOMSProxy ytbProxy = new YiTongBaoOMSProxy();
                if (ytbProxy.ValidateWebhookRequest(request) == false)
                {
                    response.msg = "签名认证错误";
                    throw new Exception(response.msg);
                }

                string dataString = System.Web.HttpUtility.UrlDecode(request.data);
                JObject jObject = JObject.Parse(dataString);
                OrderProcessReport data = jObject.ToObject<OrderProcessReport>();

                // find orderpack
                orderId = data.OrderCode;
                var orderPack = _orderPackRepository.FirstOrDefault(x => x.orderID == orderId);
                if (orderPack == null)
                {
                    response.msg = $"未找到订单：{data.OrderCode}";
                    throw new Exception(response.msg);
                }

                ProcessStatus processStatus;

                // validate processStatus
                if (!Enum.TryParse(data.ProcessStatus, out processStatus))
                {
                    response.msg = $"不存在单据状态：{data.ProcessStatus}";
                    throw new Exception(response.msg);
                }

                // log processStatus change
                OrderLogEntity orderLog = new OrderLogEntity
                {
                    OID = orderPack.oID,
                    OPID = orderPack.opID,
                    OptDate = DateTime.UtcNow,
                    Comments = $"订单流水通知回执，订单号：{orderId}，回执状态：{processStatus.GetDescription()}",
                    OptName = "System"
                };
                _orderLogRepository.Insert(orderLog);

                // update carrierId and freightId
                var carrierId = data.LogisticNo;

                // TODO: use switch instead of if else
                if ((processStatus == ProcessStatus.STARTDECLARE) && !carrierId.IsNullOrWhiteSpace())
                {
                    orderPack.carrierId = carrierId;
                    // TODO: validate against freights table
                    if (data.LogisticsCode.ToLower() != "yto")
                    {
                        response.msg = $"未找到该快递公司: {data.LogisticsCode}";
                        throw new Exception(response.msg);
                    }
                    orderPack.freightId = "yto";
                }
                if (processStatus == ProcessStatus.DELIVERED)
                {
                    orderPack.status = 4; //出库
                    orderPack.out_time = DateTime.UtcNow.ConvertUTCTimeToNZTime();
                }
            }
            catch (Exception ex)
            {
                response.msg = response.msg.IsNullOrWhiteSpace() ? "订单流水通请求失败" : response.msg;
                CLSLogger.Error("青岛保税仓订单流水通失败", ex);
                SendNotificationToDingDingJob ddJob = new SendNotificationToDingDingJob();
                ddJob.SendBondedWarehouseQingDaoAlert($"青岛保税仓订单流水通失败，请检查, 订单号: {orderId}, 失败原因：" + response.msg);

            }

            response.code = "0";
            response.success = true;
            CLSLogger.LogResponse("青岛保税仓订单流水通知响应", response);

            return response;
        }
    }
}