﻿using Abp.Dependency;
using Abp.Runtime.Caching;

using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using Cmq_SDK;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy;
using System;
using System.Collections.Generic;
using MagicLamp.PMS.Infrastructure.Extensions;
using System.Linq;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using MagicLamp.XERP.BackgroundJob;
using Microsoft.AspNetCore.Http;
using MagicLamp.PMS.Infrastructure;
using System.IO;
using System.Text;
using MagicLamp.Flux.API.Models;
using Abp.Runtime.Validation;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Linq;
using Abp.Domain.Uow;
using MagicLamp.PMS.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MagicLamp.PMS.Enums;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Entities.DBQueryTypes;
using OdooRpc.CoreCLR.Client.Models;
using OdooRpc.CoreCLR.Client.Models.Parameters;
using System.Data.SqlClient;
using MagicLamp.PMS.EntityFrameworkCore;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.Proxy.Odoo;
using MagicLamp.PMS.ExchangeRates;
using Abp.Linq.Extensions;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;
using Abp.Extensions;
using MagicLamp.PMS.Proxy.ECinx;
using Abp.Runtime.Caching;
using Abp.Dependency;
using MagicLamp.PMS.Infrastructure.Common;
using NPOI.SS.Formula.Functions;
using CsvHelper;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class ProductAppService : PMSCrudAppServiceBase<ProductEntity, ProductDTO, ProductDTO, string>
    {
        private IRepository<ProductEntity, string> productRepository;
        private IRepository<ImageEntity, long> imageRepository;
        private IRepository<WareHouseEntity, int> warehouseRepository;
        private IRepository<BrandEntity, int> brandRepository;
        private IRepository<GoodsOwnerEntity, string> goodsOwnerRepository;
        private IRepository<PackMaterialEntity, int> materialRepository;
        private IRepository<SplitOrderProductCategoryEntity, int> splitOrderProductCategoryRepository;
        private IRepository<CurrencyEntity, string> currencyRepository;
        private IRepository<ProductSalesBusinessEntity, int> productSalesBusinessRepository;
        private IRepository<XERPBusinessEntity, string> xerpBusinessRepository;
        private IRepository<SkuSplitOrderCategoryEntity, string> skuSplitOrderCategoryRepository;
        private IRepository<FreightEntity, string> freightRepository;
        private IRepository<InventoryProductCategoryEntity, int> inventoryProCategoryRepository;
        private IRepository<APIStockEntity, string> apiStockRepository;
        private IRepository<LotStockEntity, long> lotStockRepository;
        private IRepository<APIConfigEntity, string> apiConfigRepository;
        private IBackgroundJobManager backgroundJobManager;
        private IRepository<UserActivityLogEntity, long> userActivityLogRepository;
        private IRepository<PurchasePeriodEntity, string> purchasePeriodRepository;
        private IRepository<CacheOrderDetail, int> cacheOrderDetailRepository;
        private IRepository<AccountEntity, string> accountRepository;
        private IRepository<BWCountryEntity, int> countryRepository;
        private readonly IAccountManager accountManager;
        private IRepository<EditLogEntity, int> editLogRepository;
        private OdooProxy _odooProxy;
        private IExchangeRateManager _exchangeRateManager;
        private ECinxClient _EcinxClient; //连接Ecinx同步产品
        private ECinxClient _WHYCEcinxClient; //连接Ecinx同步产品
        public ICacheManager Cache { get; set; }

        public ProductAppService(IRepository<ProductEntity, string> _productRepository,
            IRepository<EditLogEntity, int> _editLogRepository,
            IRepository<ImageEntity, long> _imageRepository,
            IBackgroundJobManager _backgroundJobManager,
            IRepository<WareHouseEntity, int> _warehouseRepository,
            IRepository<BrandEntity, int> _brandRepository,
            IRepository<GoodsOwnerEntity, string> _goodsOwnerRepository,
            IRepository<PackMaterialEntity, int> _materialRepository,
            IRepository<SplitOrderProductCategoryEntity, int> _splitOrderProductCategoryRepository,
            IRepository<CurrencyEntity, string> _currencyRepository,
            IRepository<ProductSalesBusinessEntity, int> _productSalesBusinessRepository,
            IRepository<XERPBusinessEntity, string> _xerpBusinessRepository,
            IRepository<SkuSplitOrderCategoryEntity, string> _skuSplitOrderCategoryRepository,
            IRepository<FreightEntity, string> _freightRepository,
            IRepository<InventoryProductCategoryEntity, int> _inventoryProCategoryRepository,
            IRepository<APIStockEntity, string> _apiStockRepository,
            IRepository<LotStockEntity, long> _lotStockRepository,
            IRepository<PurchasePeriodEntity, string> _purchasePeriodRepository,
            IRepository<APIConfigEntity, string> _apiConfigRepository,
            IRepository<UserActivityLogEntity, long> _userActivityLogRepository,
            IRepository<CacheOrderDetail, int> _cacheOrderDetailRepository,
            IRepository<AccountEntity, string> _accountRepository,
            IRepository<BWCountryEntity, int> _countryRepository,
            OdooProxy odooProxy,
            ExchangeRateManager exchangeRateManager,
            IAccountManager _accountManager) : base(_productRepository)

        {
            productRepository = _productRepository;
            imageRepository = _imageRepository;
            backgroundJobManager = _backgroundJobManager;
            warehouseRepository = _warehouseRepository;
            brandRepository = _brandRepository;
            goodsOwnerRepository = _goodsOwnerRepository;
            materialRepository = _materialRepository;
            splitOrderProductCategoryRepository = _splitOrderProductCategoryRepository;
            currencyRepository = _currencyRepository;
            productSalesBusinessRepository = _productSalesBusinessRepository;
            xerpBusinessRepository = _xerpBusinessRepository;
            skuSplitOrderCategoryRepository = _skuSplitOrderCategoryRepository;
            freightRepository = _freightRepository;
            inventoryProCategoryRepository = _inventoryProCategoryRepository;
            apiStockRepository = _apiStockRepository;
            lotStockRepository = _lotStockRepository;
            apiConfigRepository = _apiConfigRepository;
            userActivityLogRepository = _userActivityLogRepository;
            purchasePeriodRepository = _purchasePeriodRepository;
            cacheOrderDetailRepository = _cacheOrderDetailRepository;
            accountRepository = _accountRepository;
            accountManager = _accountManager;
            countryRepository = _countryRepository;
            editLogRepository = _editLogRepository;
            _odooProxy = odooProxy;
            _exchangeRateManager = exchangeRateManager;

            ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
            _EcinxClient = new ECinxClient(cacheManager, ECinxClient.EcinxAccountEnum.CQBSC); //连接Ecinx同步产品 任选一个数据源即可）
            _WHYCEcinxClient = new ECinxClient(cacheManager, ECinxClient.EcinxAccountEnum.YIWUYINGCHI); //连接WYYC Ecinx同步产品 任选一个数据源即可）

            Cache = IocManager.Instance.Resolve<ICacheManager>();

        }

        int BarcodeMiniumLength = 3;

        /// <summary>
        /// 校验产品信息
        /// </summary>
        /// <param name="proInfo"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        private bool ValidateProductInfoAndSetReference(ProductDTO proInfo, out string msg)
        {
            msg = string.Empty;
            System.Text.StringBuilder error = new System.Text.StringBuilder();
            if (proInfo == null)
            {
                error.Append("产品信息为空；");
                msg = error.ToString();
                return false;
            }

            if (string.IsNullOrEmpty(proInfo.SKU))
            {
                error.Append("SKU编号不能为空；");
                msg = error.ToString();
                return false;
            }

            if (!proInfo.GrossWeight.HasValue)
            {
                error.Append("权重不能为空；");
                msg = error.ToString();
                return false;
            }

            if (!proInfo.NetWeight.HasValue)
            {
                error.Append("净重不能为空；");
                msg = error.ToString();
                return false;
            }

            if (!proInfo.Cost.HasValue)
            {
                error.Append("自定义成本不能为空；");
                msg = error.ToString();
                return false;
            }

            bool isSkuExists = productRepository.GetAll().Any(x => x.SKU == proInfo.SKU);
            if (isSkuExists)
            {
                error.Append($"SKU：{proInfo.SKU} 已经存在不能重复录入；");
                msg = error.ToString();
                return false;
            }

            //BrandName is actually ChineseName while uploading excel
            if (!proInfo.BrandName.IsNullOrEmpty())
            {
                var brand = brandRepository.FirstOrDefault(x => x.ChineseName == proInfo.BrandName || x.ShortCode == proInfo.BrandName || x.EnglishName == proInfo.BrandName);
                if (brand == null)
                {
                    error.Append($"未找到对应品牌编号：{proInfo.BrandName}；");
                }
                else if (brand.ActiveFlag == false)
                {
                    error.Append($"该品牌：{proInfo.BrandName} 已被禁用;");
                }
                else
                {
                    proInfo.BrandId = brand.Id;
                }
            }

            var warehouse = warehouseRepository.FirstOrDefault(x => x.Location == proInfo.Warehouse || x.Id == proInfo.WarehouseID);
            if (warehouse == null)
            {
                error.Append($"未找到对应仓库：{proInfo.Warehouse}；");
            }
            else
            {
                proInfo.WarehouseID = warehouse.Id;
                if ((warehouse.Location == "青岛保税仓" || warehouse.Location.Contains("义乌")) && proInfo.BondedWarehouseItemCode.IsNullOrWhiteSpace())
                {
                    error.Append($"保税仓产品必须填上商品编码");
                }
            }


            if (proInfo.BondedWarehouseItemCode != null)
            {
                var product = productRepository.FirstOrDefault(x => x.WarehouseID == proInfo.WarehouseID && x.BondedWarehouseItemCode == proInfo.BondedWarehouseItemCode);
                if (product != null)
                {
                    error.Append($"保税仓商品编码: {proInfo.BondedWarehouseItemCode}已被使用");
                }
            }

            List<string> barcodes = new List<string> { proInfo.Barcode2, proInfo.Barcode3, proInfo.Barcode4 };
            barcodes = barcodes.Where(x => !string.IsNullOrWhiteSpace(x) && x.Length > BarcodeMiniumLength).ToList();

            bool isBarcodeExists = productRepository.GetAll().Any(x => barcodes.Contains(x.Barcode1) && x.WarehouseID == proInfo.WarehouseID && x.CustomerID == proInfo.CustomerID);

            if (isBarcodeExists)
            {
                error.Append($"Barcode：{string.Join(",", barcodes)} 已经存在不能重复录入；");
                msg = error.ToString();
                return false;
            }

            if (!proInfo.CustomerID.IsNullOrEmpty())
            {
                var goodsOwner = goodsOwnerRepository.FirstOrDefault(x => x.Code == proInfo.CustomerID);
                if (goodsOwner == null)
                {
                    error.Append($"未找到对应货主：{proInfo.CustomerID}；");
                }
            }
            else
            {
                error.Append("货主不能为空；");
                msg = error.ToString();
                return false;
            }

            if (!proInfo.PurchasePeriodCode.IsNullOrEmpty())
            {
                var purchasePeriodCode = purchasePeriodRepository.FirstOrDefault(x => x.Name == proInfo.PurchasePeriodCode || x.Id == proInfo.PurchasePeriodCode);
                if (purchasePeriodCode == null)
                {
                    error.Append($"未找到对应库存类型：{proInfo.PurchasePeriodCode}；");
                }
                else
                {
                    proInfo.PurchasePeriodCode = purchasePeriodCode.Id;
                }
            }
            else
            {
                error.Append("库存类型不能为空；");
                msg = error.ToString();
                return false;
            }

            if (!proInfo.Materials.IsNullOrEmpty())
            {
                var packMaterial = materialRepository.FirstOrDefault(x => x.Name == proInfo.Materials);
                if (packMaterial == null)
                {
                    error.Append($"未找到对应包装材料：{proInfo.Materials}；");
                }
                else
                {
                    proInfo.PackMaterialID = packMaterial.Id;
                }
            }

            var splitOrderCategory = splitOrderProductCategoryRepository.FirstOrDefault(x => x.CNName == proInfo.SplitOrderCategoryName || x.Id == proInfo.SplitOrderCategoryID);
            if (splitOrderCategory == null)
            {
                error.Append($"未找到对应分单分类：{proInfo.SplitOrderCategoryName}；");
            }
            else
            {
                proInfo.SplitOrderCategoryID = splitOrderCategory.Id;
            }

            var currency = currencyRepository.FirstOrDefault(x => x.Code == proInfo.CurrencyID);
            if (currency == null)
            {
                error.Append($"未找到对应币种：{proInfo.CurrencyID}；");
            }

            if (!string.IsNullOrEmpty(proInfo.DefaultFreightID))
            {
                var freight = freightRepository.FirstOrDefault(x => x.Name == proInfo.DefaultFreightID || x.FreightID == proInfo.DefaultFreightID || x.FreightSet == proInfo.DefaultFreightID);
                if (freight == null)
                {
                    error.Append($"未找到对应默认发货渠道：{proInfo.DefaultFreightID}；");
                }
                else
                {
                    proInfo.DefaultFreightID = freight.Id;
                }
            }

            if (!string.IsNullOrEmpty(proInfo.CategoryName))
            {
                var category = inventoryProCategoryRepository.FirstOrDefault(x => x.Name == proInfo.CategoryName);
                if (category == null)
                {
                    error.Append($"未找到对应产品供应链分类：{proInfo.CategoryName}；");
                }
                else
                {
                    proInfo.CategoryID = category.CategoryID;
                }
            }

            if (!string.IsNullOrEmpty(proInfo.CategoryName) && !proInfo.CategoryID.HasValue)
            {
                error.Append($"产品供应链分类不能为空; ");
            }

            if (!string.IsNullOrEmpty(proInfo.PersonInChargeID))
            {
                var account = accountRepository.FirstOrDefault(x => x.UserID == proInfo.PersonInChargeID || x.UserName == proInfo.PersonInChargeID);
                if (account == null)
                {
                    error.Append($"未找到对应负责人：{proInfo.PersonInChargeID}；");
                }
                else
                {
                    proInfo.PersonInChargeID = account.UserID;
                }
            }
            else
            {
                error.Append("负责人不能为空；");
            }

            if (!string.IsNullOrEmpty(proInfo.CountryName))
            {
                var country = countryRepository.FirstOrDefault(x => x.CountryName == proInfo.CountryName.Trim());
                if (country == null)
                {
                    error.Append($"未找到对应原产国: {proInfo.CountryName}; ");
                }
                else
                {
                    proInfo.CountryId = country.Code;
                }
            }

            if (proInfo.CountryId.HasValue)
            {
                var country = countryRepository.FirstOrDefault(x => x.Code == proInfo.CountryId);
                if (country == null)
                {
                    error.Append($"未找到对应原产国id: {proInfo.CountryId}; ");
                }
                else
                {
                    proInfo.CountryName = country.CountryName;
                }
            }

            if (!proInfo.HHSKU.IsNullOrWhiteSpace())
            {
                var product = productRepository.FirstOrDefault(x => x.HHSKU == proInfo.HHSKU);
                if (product != null && product.SKU != proInfo.SKU)
                {
                    error.Append($"HHSKU已被一下產品使用：{product.SKU}");
                }
            }

            if (proInfo.ActiveFlag == null)
            {
                proInfo.ActiveFlag = true;
            }

            if (error.Length > 0)
            {
                msg = error.ToString();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 批量更新时校验产品信息
        /// </summary>
        /// <param name="proInfo"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        private bool ValidateForBatchUpdate(ProductDTO proInfo, ProductEntity entity, out string msg)
        {
            msg = string.Empty;
            System.Text.StringBuilder error = new System.Text.StringBuilder();
            if (proInfo == null)
            {
                error.Append("产品信息为空；");
                msg = error.ToString();
                return false;
            }

            if (proInfo.Cost.HasValue)
            {
                if (proInfo.Cost != entity.Cost && proInfo.Cost >= 0)
                {
                    EditLogEntity log = new EditLogEntity
                    {
                        System = "xerp-next",
                        Type = "Cost",
                        Uesr = accountManager.GetUserName(),
                        DateTime = DateTime.UtcNow,
                        Content = $"{proInfo.SKU}:{entity.Cost}=>{proInfo.Cost}"
                    };
                    editLogRepository.Insert(log);
                }
            }
            

            if (!string.IsNullOrEmpty(proInfo.Warehouse))
            {
                var warehouse = warehouseRepository.FirstOrDefault(x => x.Location == proInfo.Warehouse || x.Id == proInfo.WarehouseID);
                if (warehouse == null)
                {
                    error.Append($"未找到对应仓库：{proInfo.Warehouse}；");
                }
                else
                {
                    proInfo.WarehouseID = warehouse.Id;
                    if ((warehouse.Location == "青岛保税仓" || warehouse.Location.Contains("义乌")) && (proInfo.BondedWarehouseItemCode.IsNullOrWhiteSpace() && entity.BondedWarehouseItemCode.IsNullOrWhiteSpace()))
                    {
                        error.Append($"保税仓产品必须填上商品编码");
                    }
                }
            }

            if (proInfo.BondedWarehouseItemCode != null && proInfo.BondedWarehouseItemCode != entity.BondedWarehouseItemCode)
            {
                var warehouseId = proInfo.WarehouseID ?? entity.WarehouseID;
                var product = productRepository.FirstOrDefault(x => x.WarehouseID == warehouseId && x.BondedWarehouseItemCode == proInfo.BondedWarehouseItemCode);
                if (product != null)
                {
                    error.Append($"保税仓商品编码: {proInfo.BondedWarehouseItemCode}已被使用");
                }
            }

            if (!proInfo.Materials.IsNullOrEmpty())
            {
                var packMaterial = materialRepository.FirstOrDefault(x => x.Name == proInfo.Materials);
                if (packMaterial == null)
                {
                    error.Append($"未找到对应包装材料：{proInfo.Materials}；");
                }
                else
                {
                    proInfo.PackMaterialID = packMaterial.Id;
                }
            }

            if (!string.IsNullOrEmpty(proInfo.SplitOrderCategoryName))
            {
                var splitOrderCategory = splitOrderProductCategoryRepository.FirstOrDefault(x => x.CNName == proInfo.SplitOrderCategoryName);
                if (splitOrderCategory == null)
                {
                    error.Append($"未找到对应分单分类：{proInfo.SplitOrderCategoryName}；");
                }
                else
                {
                    proInfo.SplitOrderCategoryID = splitOrderCategory.Id;
                }
            }

            if (!string.IsNullOrEmpty(proInfo.CurrencyID))
            {
                var currency = currencyRepository.FirstOrDefault(x => x.Code == proInfo.CurrencyID);
                if (currency == null)
                {
                    error.Append($"未找到对应币种：{proInfo.CurrencyID}；");
                }
            }

            if (!string.IsNullOrEmpty(proInfo.CategoryName))
            {
                var category = inventoryProCategoryRepository.FirstOrDefault(x => x.Name == proInfo.CategoryName);
                if (category == null)
                {
                    error.Append($"未找到对应产品供应链分类：{proInfo.CategoryName}；");
                }
                else
                {
                    proInfo.CategoryID = category.CategoryID;
                }
            }

            if (!string.IsNullOrEmpty(proInfo.BrandName))
            {
                var brand = brandRepository.FirstOrDefault(x => x.ChineseName == proInfo.BrandName || x.ShortCode == proInfo.BrandName || x.EnglishName == proInfo.BrandName);
                if (brand == null)
                {
                    error.Append($"未找到对应品牌编号：{proInfo.BrandName}；");
                }
                else if (brand.ActiveFlag == false)
                {
                    error.Append($"该品牌：{proInfo.BrandName} 已被禁用;");
                }
                else
                {
                    proInfo.BrandId = brand.Id;
                }
            }

            if (!proInfo.CustomerID.IsNullOrEmpty())
            {
                var goodsOwner = goodsOwnerRepository.FirstOrDefault(x => x.Code == proInfo.CustomerID);
                if (goodsOwner == null)
                {
                    error.Append($"未找到对应货主：{proInfo.CustomerID}；");
                }
            }

            if (!proInfo.PurchasePeriodCode.IsNullOrEmpty())
            {
                var purchasePeriodCode = purchasePeriodRepository.FirstOrDefault(x => x.Name == proInfo.PurchasePeriodCode || x.Id == proInfo.PurchasePeriodCode);
                if (purchasePeriodCode == null)
                {
                    error.Append($"未找到对应库存类型：{proInfo.PurchasePeriodCode}；");
                }
                else
                {
                    proInfo.PurchasePeriodCode = purchasePeriodCode.Id;
                }
            }

            if (!string.IsNullOrEmpty(proInfo.PersonInChargeID))
            {
                var account = accountRepository.FirstOrDefault(x => x.UserID == proInfo.PersonInChargeID || x.UserName == proInfo.PersonInChargeID);
                if (account == null)
                {
                    error.Append($"未找到对应负责人：{proInfo.PersonInChargeID}；");
                }
                else
                {
                    proInfo.PersonInChargeID = account.UserID;
                }
            }

            if (!string.IsNullOrEmpty(proInfo.CountryName))
            {
                var country = countryRepository.FirstOrDefault(x => x.CountryName == proInfo.CountryName.Trim());
                if (country == null)
                {
                    error.Append($"未找到对应原产国: {proInfo.CountryName}; ");
                }
                else
                {
                    proInfo.CountryId = country.Code;
                }
            }

            if (proInfo.CountryId.HasValue)
            {
                var country = countryRepository.FirstOrDefault(x => x.Code == proInfo.CountryId);
                if (country == null)
                {
                    error.Append($"未找到对应原产国id: {proInfo.CountryId}; ");
                }
                else
                {
                    proInfo.CountryName = country.CountryName;
                }
            }

            if (!proInfo.HHSKU.IsNullOrWhiteSpace())
            {
                var product = productRepository.FirstOrDefault(x => x.HHSKU == proInfo.HHSKU);
                if (product != null && product.SKU != proInfo.SKU)
                {
                    error.Append($"HHSKU已被以下產品使用：{product.SKU}");
                }
            }

            if (error.Length > 0)
            {
                msg = error.ToString();
                return false;
            }

            return true;
        }

        [Authorize]
        public override ProductDTO Create(ProductDTO input)
        {

            if (accountManager.GetUserId().IsNullOrWhiteSpace())
            {
                CLSLogger.Error("ProductAPPService_odoo产品更新失败,当前用户信息为空", input.ToJsonString());
                throw new UserFriendlyException("产品更新失败，同步odoo发生异常，用户信息丢失");
            }

            string error = string.Empty;
            ValidateProductInfoAndSetReference(input, out error);

            if (!string.IsNullOrEmpty(error))
            {
                throw new RequestValidationException(error);
            }

            input.CreateTime = DateTime.Now;
            input.UpdateTime = DateTime.Now;
            input.SKU = input.SKU.Trim();

            StringBuilder odooValidationErrors;
            GetOdooProductParameterIds(new List<ProductDTO> { input }, out odooValidationErrors);
            if (odooValidationErrors.Length > 0)
            {
                throw new UserFriendlyException(odooValidationErrors.ToString());
            }
            ValidateOdooParameters(new List<ProductDTO> { input }, out odooValidationErrors);
            if (odooValidationErrors.Length > 0)
            {
                throw new UserFriendlyException(odooValidationErrors.ToString());
            }

            if (input.SyncToOdoo == true)
            {
                try
                {
                    var odooProducts = _odooProxy.GetProducts(
                        new OdooDomainFilter().Filter("default_code", "=", input.HHSKU.Trim()),
                        new OdooFieldParameters()
                        {
                        "id",
                        "default_code",
                        "name",
                        "cn_name"
                        }
                    ).Result;

                    var odooProduct = odooProducts.FirstOrDefault();
                    var product = ObjectMapper.Map<Proxy.Odoo.Product>(input);

                    var odooCategories = _odooProxy.GetAllCategories().Result;

                    bool isVirtualStock = input.PurchasePeriodCode == "virtualStock";
                    var odooCategory = odooCategories.FirstOrDefault(x => x["id"].ToObject<long>() == input.OdooCategoryID);
                    string categoryCompleteName = odooCategory["complete_name"].ToString();
                    bool syncCostToOdoo = isVirtualStock && (categoryCompleteName.Contains("DF") || categoryCompleteName.Contains("FO"));
                    decimal? standardPrice = null;
                    if (syncCostToOdoo)
                    {
                        standardPrice = _exchangeRateManager.GetRate(input.CurrencyID, "NZD") * input.Cost;
                        product.standard_price = standardPrice;
                        product.taxes_id = new object[1] { new object[3] { 6, 0, new int[] { 3, 12 } } };
                        product.supplier_taxes_id = new object[1] { new object[3] { 6, 0, new int[] { 6, 15 } } };
                    }

                    long odooProductId;

                    if (odooProduct == null)
                    {
                        odooProductId = _odooProxy.CreateProduct(product).Result;
                        input.OdooProductID = odooProductId;
                    }
                    else
                    {
                        odooProductId = odooProduct["id"].ToObject<long>();
                        input.OdooProductID = odooProductId;
                        product.responsible_id = odooProduct["responsible_id"][0].ToObject<long>();
                        _odooProxy.UpdateProduct((long)odooProductId, product).Wait();
                        input.OdooProductID = odooProductId;
                    }

                    if (syncCostToOdoo)
                    {
                        _odooProxy.UpdateProduct(
                            odooProductId,
                            new Proxy.Odoo.Product
                            {
                                default_code = input.HHSKU,
                                standard_price = standardPrice,
                                taxes_id = null,
                                supplier_taxes_id = null
                            },
                            new OptionalParameters
                            {
                                force_company = 1
                            }
                        ).Wait();
                    }
                }
                catch (UserFriendlyException ex)
                {
                    throw ex;
                }
                catch (Exception ex)
                {
                    CLSLogger.Error("odoo产品更新失败", ex);
                    throw new UserFriendlyException("产品更新失败，同步odoo发生异常，请稍后再尝试");
                }
            }

            EcinxConfigerData config = EcinxConfigerData.Configuration;
            // 同步Ecinx;
            if (input.SyncToEcinx == true)
            {
                //WHYC
                if (config.PushWHYCMFDWarehouseID.Contains((int)input.WarehouseID))
                {
                    var queryResponse = _WHYCEcinxClient.GoodsQuery(new GoodsQueryInput
                    {
                        code = input.SKU
                    });

                    if (queryResponse.Result.result.total > 0)
                    {
                        // 存在 更新
                        var result = _WHYCEcinxClient.GoodsUpdate(new GoodsAddInput
                        {
                            code = input.SKU,
                            name = input.ChineseName,
                            barCode = input.Barcode1,
                            catalogCode = "KJED"
                            //height = input.High,
                            //length = input.Length
                        });
                    }
                    else
                    {
                        //不存在写入
                        var result = _WHYCEcinxClient.GoodsAdd(new GoodsAddInput
                        {
                            code = input.SKU,
                            name = input.ChineseName,
                            barCode = input.Barcode1,
                            catalogCode = "KJED"
                            //height = input.High,
                            //length = input.Length
                        });
                    }
                }
                //MFD
                else
                {
                    var queryResponse = _EcinxClient.GoodsQuery(new GoodsQueryInput
                    {
                        code = input.SKU
                    });

                    if (queryResponse.Result.result.total > 0)
                    {
                        // 存在 更新
                        var result = _EcinxClient.GoodsUpdate(new GoodsAddInput
                        {
                            code = input.SKU,
                            name = input.ChineseName,
                            barCode = input.Barcode1,
                            catalogCode = "MFD"
                            //height = input.High,
                            //length = input.Length
                        });
                    }
                    else
                    {
                        //不存在写入
                        var result = _EcinxClient.GoodsAdd(new GoodsAddInput
                        {
                            code = input.SKU,
                            name = input.ChineseName,
                            barCode = input.Barcode1,
                            catalogCode = "MFD"
                            //height = input.High,
                            //length = input.Length
                        });
                    }
                }
            }

            var productDto = base.Create(input);

            var images = new List<ImageDTO>();
            if (!input.MainImages.IsNullOrEmpty())
            {
                input.MainImages.ForEach(x => { x.ReferenceType = ProductImageTypeEnum.Main.ToString(); });
                images.AddRange(input.MainImages);
            }
            if (!input.DetailImages.IsNullOrEmpty())
            {
                input.DetailImages.ForEach(x => { x.ReferenceType = ProductImageTypeEnum.Detail.ToString(); });
                images.AddRange(input.DetailImages);
            }

            UpdateProductImages(images, input.SKU);
            UpdateProductBusiness(input.Business, input.SKU);
            UpdateProductSplitOrderCategory(input);

            CurrentUnitOfWork.SaveChanges();

            ProductInfoMqDto productInfoMqDto = new ProductInfoMqDto();
            productInfoMqDto.After = productDto;
            productInfoMqDto.Action = MqActionEnum.Create;

            backgroundJobManager.Enqueue<ProductUpdateNotifyJob, ProductUpdateNotifyJobArgs>(new ProductUpdateNotifyJobArgs
            {
                Products = new List<ProductInfoMqDto> { productInfoMqDto },
                UserId = accountManager.GetUserId(),
                UserName = accountManager.GetUserName(),
                CreatedAt = DateTime.UtcNow
            });
            backgroundJobManager.EnqueueAsync<ProductRelatedInfoValidationJob, List<string>>(new List<string> { input.SKU });

            PushDingDingNotification(accountManager.GetUserName(),new List<ProductDTO> { input });


            // Spark 2023.08.03 set cachmager sku;
            List<string> skus = new List<string> { input.SKU };
            var cacheKeys = skus.Select(x => string.Format(CacheKeys.PRODUCTKEYFORMAT, x.Trim().ToUpper())).ToList();
            var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);

            List<ProductEntity> cacheProducts = cacheManager.GetOrDefault<string, ProductEntity>(cacheKeys.ToArray()).ToList();
            cacheProducts.RemoveAll(x => x == null);

            var dbProducts = productRepository.GetAll().Where(x => input.SKU.Contains(x.SKU)).ToList();

            dbProducts.ForEach(x =>
            {
                cacheManager.Set(string.Format(CacheKeys.PRODUCTKEYFORMAT, x.SKU.Trim().ToUpper()), x, TimeSpan.FromHours(24));
            });



            return productDto;
        }

        /// <summary>
        /// Update product split order category
        /// </summary>
        /// <param name="inputDto"></param> 
        private void UpdateProductSplitOrderCategory(ProductDTO inputDto)
        {
            if (!inputDto.SplitOrderCategoryID.HasValue)
            {
                throw new UserFriendlyException("产品分单分类不能为空");
            }
            var oldCategory = skuSplitOrderCategoryRepository.FirstOrDefault(x => x.SKU == inputDto.SKU);
            if (oldCategory != null)
            {
                if (oldCategory.ProductCategoryId != inputDto.SplitOrderCategoryID.Value)
                {
                    oldCategory.ProductCategoryId = inputDto.SplitOrderCategoryID.Value;
                }
            }
            else
            {
                SkuSplitOrderCategoryEntity skuSplitOrderCategoryEntity = new SkuSplitOrderCategoryEntity
                {
                    ProductCategoryId = inputDto.SplitOrderCategoryID.Value,
                    SKU = inputDto.SKU
                };
                skuSplitOrderCategoryRepository.Insert(skuSplitOrderCategoryEntity);
            }
        }

        /// <summary>
        /// Update product images 
        /// </summary>
        /// <param name="images"></param>
        /// <param name="sku"></param>
        private List<ImageDTO> UpdateProductImages(List<ImageDTO> images, string sku)
        {
            var oldImages = imageRepository.GetAll().Where(x => x.ReferenceId == sku &&
            x.ImageType == ImageTypeEnum.Product.ToString()).ToList();

            if (images.IsNullOrEmpty() && oldImages.IsNullOrEmpty())
            {
                return null;
            }

            //删除or更新旧的
            foreach (var item in oldImages)
            {
                var currentImg = images.FirstOrDefault(x => x.ImageId == item.ImageId);
                if (currentImg == null)
                {
                    imageRepository.Delete(item.ImageId);
                }
                else
                {
                    item.Sort = currentImg.Sort;
                    item.OriginalPath = currentImg.OriginalPath;
                }
            }

            //新增的image(更新image的ReferenceId)
            List<long> imageIds = images.Where(x => x.ReferenceId.IsNullOrEmpty()).Select(x => x.ImageId).ToList();
            imageRepository.GetAll().Where(x => imageIds.Contains(x.ImageId)).ToList().ForEach(x =>
            {
                var currentImg = images.FirstOrDefault(y => y.ImageId == x.ImageId);
                x.ReferenceType = currentImg.ReferenceType;
                x.ReferenceId = sku;
                x.Sort = currentImg.Sort;
                currentImg.ReferenceId = sku;

                x.ImageType = ImageTypeEnum.Product.ToString();
                currentImg.ImageType = x.ImageType;

                x.CreationTime = DateTime.UtcNow;
                currentImg.CreationTime = x.CreationTime;
            });
            return images;
        }

        private List<ImageDTO> GetImagesBySku(string sku)
        {
            var entities = imageRepository.GetAll().Where(x => x.ReferenceId == sku && x.ImageType == ImageTypeEnum.Product.ToString()).ToList();
            return entities.MapTo<List<ImageDTO>>();
        }

        // TODO: validate input, reuse validateforbatchupdate logic or use attributes Spark

        [Authorize]
        public override ProductDTO Update(ProductDTO input)
        {
            if (accountManager.GetUserId().IsNullOrWhiteSpace())
            {
                CLSLogger.Error("ProductAPPService_Update,当前用户信息为空", input.ToJsonString());
                throw new UserFriendlyException("更新产品信息发生异常,用户信息为空，重新登录后重试！");
            }

            string validationError = string.Empty;
            var oldEntity = productRepository.FirstOrDefault(x => x.SKU == input.SKU);

            if (oldEntity == null)
            {
                CLSLogger.Error("更新产品发生错误", $"未找到：{input.SKU}，无法更新");
                throw new RequestValidationException($"未找到：{input.SKU}，无法更新");
            }

            input.OdooProductID = oldEntity.OdooProductID;
            input.OdooBrandID = oldEntity.OdooBrandID;

            ValidateForBatchUpdate(input, oldEntity, out validationError);

            if (!string.IsNullOrEmpty(validationError))
            {
                throw new UserFriendlyException($"更新产品发生错误：{validationError}");
            }

            try
            {
                ProductInfoMqDto productInfoMqDto = new ProductInfoMqDto();

                StringBuilder odooValidationErrors;
                GetOdooProductParameterIds(new List<ProductDTO> { input }, out odooValidationErrors);
                if (odooValidationErrors.Length > 0)
                {
                    throw new UserFriendlyException(odooValidationErrors.ToString());
                }
                ValidateUpdateOdooParameters(new List<ProductDTO> { input }, new List<ProductEntity> { oldEntity }, out odooValidationErrors);
                if (odooValidationErrors.Length > 0)
                {
                    throw new UserFriendlyException(odooValidationErrors.ToString());
                }

                if (input.SyncToOdoo == true)
                {
                    try
                    {
                        var odooProducts = _odooProxy.GetProducts(
                            new OdooDomainFilter().Filter("default_code", "=", input.HHSKU.Trim()),
                            new OdooFieldParameters()
                            {
                                "id",
                                "default_code",
                                "name",
                                "cn_name",
                                "responsible_id"
                            }
                        ).Result;

                        var odooProduct = odooProducts.FirstOrDefault();
                        var product = ObjectMapper.Map<Proxy.Odoo.Product>(input);

                        var odooCategories = _odooProxy.GetAllCategories().Result;

                        bool isVirtualStock = input.PurchasePeriodCode == "virtualStock";
                        var odooCategory = odooCategories.FirstOrDefault(x => x["id"].ToObject<long>() == input.OdooCategoryID);
                        string categoryCompleteName = odooCategory["complete_name"].ToString();
                        bool syncCostToOdoo = isVirtualStock && (categoryCompleteName.Contains("DF") || categoryCompleteName.Contains("FO"));
                        long odooProductId;
                        decimal? standardPrice = null;

                        //CLSLogger.Error("Spark_Parameters", $" syncCostToOdoo：{syncCostToOdoo}，categoryCompleteName: {categoryCompleteName} ");

                        if (syncCostToOdoo)
                        {
                           
                            standardPrice = _exchangeRateManager.GetRate(input.CurrencyID, "NZD") * input.Cost;
                            product.standard_price = standardPrice;
                            product.taxes_id = new object[1] { new object[3] { 6, 0, new int[] { 3, 12 } } };
                            product.supplier_taxes_id = new object[1] { new object[3] { 6, 0, new int[] { 6, 15 } } };

                            //CLSLogger.Error("Spark_syncCostToOdoo", $"standardPrice：{standardPrice}");
                        }
                        if (odooProduct == null)
                        {
                            odooProductId = _odooProxy.CreateProduct(product).Result;
                            oldEntity.OdooProductID = odooProductId;
                            input.OdooProductID = odooProductId;
                        }
                        else
                        {
                            odooProductId = odooProduct["id"].ToObject<long>();
                            input.OdooProductID = odooProductId;
                            product.responsible_id = odooProduct["responsible_id"][0].ToObject<long>();
                            _odooProxy.UpdateProduct(odooProductId, product).Wait();
                            oldEntity.OdooProductID = odooProductId;
                        }

                        if (syncCostToOdoo)
                        {
                          

                            _odooProxy.UpdateProduct(
                                odooProductId,
                                new Proxy.Odoo.Product
                                {
                                    default_code = input.HHSKU,
                                    standard_price = standardPrice,
                                    taxes_id = null,
                                    supplier_taxes_id = null
                                },
                                new OptionalParameters
                                {
                                    force_company = 1
                                }
                            ).Wait();

                            //CLSLogger.Error("Spark_syncCostToOdoo", $"standardPrice：{standardPrice}");
                        }

                        //CLSLogger.Error("Spark_last", $"Finished!");
                    }
                    catch (UserFriendlyException ex)
                    {
                        throw ex;
                    }
                    catch (AggregateException ex)
                    {
                        ex.Handle(inner =>
                        {
                            CLSLogger.Error("同步odoo发生异常", inner.ToJsonString());
                            return true;
                        });
                        throw new UserFriendlyException("产品更新失败，同步odoo发生异常，请稍后再尝试");
                    }
                    catch (Exception ex)
                    {
                        CLSLogger.Error("odoo产品更新失败", ex);
                        throw new UserFriendlyException("产品更新失败，同步odoo发生异常，请稍后再尝试");
                    }
                }

                //var  brandnameentity = brandRepository.FirstOrDefault(x => x.Id == input.BrandId);
                EcinxConfigerData config = EcinxConfigerData.Configuration;
                if (input.SyncToEcinx == true)
                {
                    //WHYC
                    if (config.PushWHYCMFDWarehouseID.Contains((int)input.WarehouseID))
                    {

                        var queryResponse = _WHYCEcinxClient.GoodsQuery(new GoodsQueryInput { code = input.SKU });

                        if (queryResponse.Result.result.total > 0)
                        {
                            // 存在 更新
                            var result = _WHYCEcinxClient.GoodsUpdate(new GoodsAddInput
                            {
                                code = input.SKU,
                                name = input.ChineseName,
                                barCode = input.Barcode1,
                                catalogCode = "KJED"
                                //brandCode = brandnameentity.ChineseName, 品牌信息需要提前维护
                                //height = input.High,
                                //length = input.Length
                            });
                        }
                        else
                        {
                            //不存在写入
                            var result = _WHYCEcinxClient.GoodsAdd(new GoodsAddInput
                            {
                                code = input.SKU,
                                name = input.ChineseName,
                                barCode = input.Barcode1,
                                catalogCode = "KJED"
                                //brandCode = brandnameentity.ChineseName,
                                //height = input.High,
                                //length = input.Length
                            });
                        }
                    }
                    else
                    {
                        var queryResponse = _EcinxClient.GoodsQuery(new GoodsQueryInput { code = input.SKU });

                        if (queryResponse.Result.result.total > 0)
                        {
                            // 存在 更新
                            var result = _EcinxClient.GoodsUpdate(new GoodsAddInput
                            {
                                code = input.SKU,
                                name = input.ChineseName,
                                barCode = input.Barcode1,
                                catalogCode = "MFD"
                                //brandCode = brandnameentity.ChineseName, 品牌信息需要提前维护
                                //height = input.High,
                                //length = input.Length
                            });
                        }
                        else
                        {
                            //不存在写入
                            var result = _EcinxClient.GoodsAdd(new GoodsAddInput
                            {
                                code = input.SKU,
                                name = input.ChineseName,
                                barCode = input.Barcode1,
                                catalogCode = "MFD"
                                //brandCode = brandnameentity.ChineseName,
                                //height = input.High,
                                //length = input.Length
                            });
                        }
                    }


                }


                string error = string.Empty;

                List<string> barcodes = new List<string> { input.Barcode2, input.Barcode3, input.Barcode4 };
                barcodes = barcodes.Where(x => !string.IsNullOrWhiteSpace(x) && x.Length > BarcodeMiniumLength).ToList();

                bool isBarcodeExists = productRepository.GetAll().Any(x => barcodes.Contains(x.Barcode1) && x.SKU != input.SKU && x.WarehouseID == input.WarehouseID && x.CustomerID == input.CustomerID);

                if (isBarcodeExists)
                {
                    throw new RequestValidationException($"Barcode：{string.Join(",", barcodes)} 已经存在不能重复录入；");
                }

                if (!string.IsNullOrEmpty(error))
                {
                    throw new RequestValidationException(error);
                }

                productInfoMqDto.Before = oldEntity.MapTo<ProductDTO>();
                var oldImages = GetImagesBySku(input.SKU);
                if (!oldImages.IsNullOrEmpty())
                {
                    productInfoMqDto.Before.MainImages = oldImages.Where(x => x.ReferenceType == ProductImageTypeEnum.Main.ToString()).ToList();
                    productInfoMqDto.Before.DetailImages = oldImages.Where(x => x.ReferenceType == ProductImageTypeEnum.Detail.ToString()).ToList();
                }

                if (!input.MainImages.IsNullOrEmpty())
                {
                    input.MainImages.ForEach(x => { x.ReferenceType = ProductImageTypeEnum.Main.ToString(); });
                }
                if (!input.DetailImages.IsNullOrEmpty())
                {
                    input.DetailImages.ForEach(x => { x.ReferenceType = ProductImageTypeEnum.Detail.ToString(); });
                }

                input.UpdateTime = DateTime.Now;
                input.MapTo(oldEntity);

                var newImages = input.MainImages.Concat(input.DetailImages).ToList();
                UpdateProductImages(newImages, input.SKU);
                UpdateProductBusiness(input.Business, input.SKU);
                UpdateProductSplitOrderCategory(input);

                productInfoMqDto.After = input;
                productInfoMqDto.Action = MqActionEnum.Update;

                CurrentUnitOfWork.SaveChanges();

                backgroundJobManager.Enqueue<ProductUpdateNotifyJob, ProductUpdateNotifyJobArgs>(new ProductUpdateNotifyJobArgs
                {
                    Products = new List<ProductInfoMqDto> { productInfoMqDto },
                    UserId = accountManager.GetUserId(),
                    UserName = accountManager.GetUserName(),
                    CreatedAt = DateTime.UtcNow
                });
                backgroundJobManager.EnqueueAsync<ProductRelatedInfoValidationJob, List<string>>(new List<string> { input.SKU });


                // spark 08032023 maintance sku cache data
                List<string> skus = new List<string> { input.SKU };
                var cacheKeys = skus.Select(x => string.Format(CacheKeys.PRODUCTKEYFORMAT, x.Trim().ToUpper())).ToList();
                var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);

                List<ProductEntity> cacheProducts = cacheManager.GetOrDefault<string, ProductEntity>(cacheKeys.ToArray()).ToList();
                cacheProducts.RemoveAll(x => x == null);

                var dbProducts = productRepository.GetAll().Where(x => input.SKU.Contains(x.SKU)).ToList();

                dbProducts.ForEach(x =>
                {
                    cacheManager.Set(string.Format(CacheKeys.PRODUCTKEYFORMAT, x.SKU.Trim().ToUpper()), x, TimeSpan.FromHours(24));
                });

                return input;
            }
            catch (RequestValidationException ex)
            {
                throw ex;
            }
            catch (UserFriendlyException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("更新产品发生错误", ex);
                throw new UserFriendlyException("更新产品信息发生异常", ex);
            }
        }

        public override ProductDTO Get(ProductDTO input)
        {
            var outputDto = base.Get(input);

            var images = GetImagesBySku(input.Id);

            if (!images.IsNullOrEmpty())
            {
                outputDto.MainImages = images.Where(x => x.ReferenceType == ProductImageTypeEnum.Main.ToString()).ToList();
                outputDto.DetailImages = images.Where(x => x.ReferenceType == ProductImageTypeEnum.Detail.ToString()).ToList();
            }

            var business = productSalesBusinessRepository.GetAll().Where(x => x.SKU == input.Id).ToList();
            if (!business.IsNullOrEmpty())
            {
                outputDto.Business = business.Select(x => x.BusinessName).ToList();
            }

            return outputDto;
        }

        /// <summary>
        /// EXCEL 导入
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [Authorize]
        public override ExcelImportOutputDTO ImportExcel(IFormFile file)
        {
            if (accountManager.GetUserId().IsNullOrWhiteSpace())
            {
                CLSLogger.Error("ProductAPPService_ImportExcel,当前用户信息为空", "");
                throw new UserFriendlyException("上传的Excel时, 用户信息为空，重新登录后重试！");
            }

            ExcelImportOutputDTO excelImportOutputDTO = new ExcelImportOutputDTO();
            try
            {
                if (file.Length <= 0)
                {
                    throw new UserFriendlyException("文件大小异常，请选择正确的文件");
                }

                string filePath = Ultities.CreateDirectory("App_Data/ExcelImport");
                string fileName = $"{Guid.NewGuid()}{file.FileName}";
                string fullPath = Path.Combine(filePath, fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                var mapper = new Npoi.Mapper.Mapper(fullPath);
                var dtos = mapper.Take<ProductDTO>().Where(x => x.Value != null && !string.IsNullOrEmpty(x.Value.SKU)).Select(x => x.Value).ToList();

                File.Delete(fullPath);

                if (dtos.IsNullOrEmpty())
                {
                    throw new UserFriendlyException("excel中未找到有效数据，请确保excel模板正确且有数据");
                }

                var duplicateSkus = dtos.GroupBy(x => x.SKU).Where(x => x.Count() > 1).ToList();
                StringBuilder errorMsg = new StringBuilder();
                foreach (var item in duplicateSkus)
                {
                    errorMsg.Append($"{item.Key}，");
                }

                if (errorMsg.Length > 0)
                {
                    errorMsg.Append($"上传的Excel中SKU发现重复，请删除重复项");
                    throw new UserFriendlyException(errorMsg.ToString());
                }

                StringBuilder odooValidationErrors;
                GetOdooProductParameterIds(dtos, out odooValidationErrors);
                if (odooValidationErrors.Length > 0)
                {
                    throw new UserFriendlyException(odooValidationErrors.ToString());
                }
                ValidateOdooParameters(dtos, out odooValidationErrors);
                if (odooValidationErrors.Length > 0)
                {
                    throw new UserFriendlyException(odooValidationErrors.ToString());
                }

                var proBusiness = xerpBusinessRepository.GetAllList().Select(x => x.Name).ToList();
                List<ProductEntity> products = new List<ProductEntity>();

                StringBuilder error = new StringBuilder();
                for (int i = 0; i < dtos.Count; i++)
                {
                    string tempError = string.Empty;
                    ValidateProductInfoAndSetReference(dtos[i], out tempError);
                    if (!string.IsNullOrEmpty(tempError))
                    {
                        error.Append($"导入出错请检查修正；第{i + 2}行：{tempError} \n\r");
                    }
                }

                if (error.Length > 0)
                {
                    throw new RequestValidationException(error.ToString());
                }


                List<string> AllSyncEcinxSku = new List<string>(); // 所有需要同步到Ecinx的SKU


                for (int i = 0; i < dtos.Count; i++)
                {
                    var entity = dtos[i].MapTo<ProductEntity>();
                    entity.SKU = entity.SKU.Trim();
                    entity.ActiveFlag = true;
                    entity.UpdateTime = DateTime.UtcNow;
                    if (dtos[i].SyncToOdoo == true)
                    {
                        entity.OdooSyncStatus = OdooSyncStatusEnum.Pending.ToString();
                    }

                    //Add sales business
                    UpdateProductBusiness(proBusiness, entity.SKU);

                    //update split order category
                    if (dtos[i].SplitOrderCategoryID.HasValue && dtos[i].SplitOrderCategoryID > 0)
                    {
                        UpdateProductSplitOrderCategory(dtos[i]);
                    }

                    productRepository.Insert(entity);
                    products.Add(entity);


                    // 同步Ecinx;
                    if (entity.SyncToEcinx == true)
                    {
                        AllSyncEcinxSku.Add(entity.SKU);
                    }


                }

                CurrentUnitOfWork.SaveChanges();

                // 同步Ecinx;
                if (AllSyncEcinxSku.Count > 0)
                {
                    backgroundJobManager.EnqueueAsync<CreateOrUpdateEcinxProduct, List<string>>(AllSyncEcinxSku);
                }

                List<ProductInfoMqDto> mqDtos = new List<ProductInfoMqDto>();

                foreach (var item in dtos)
                {
                    ProductInfoMqDto productInfoMqDto = new ProductInfoMqDto();
                    productInfoMqDto.After = item;
                    productInfoMqDto.Action = MqActionEnum.Create;
                    mqDtos.Add(productInfoMqDto);
                }

                backgroundJobManager.EnqueueAsync<ProductRelatedInfoValidationJob, List<string>>(dtos.Select(x => x.SKU).ToList());
                backgroundJobManager.Enqueue<ProductUpdateNotifyJob, ProductUpdateNotifyJobArgs>(new ProductUpdateNotifyJobArgs
                {
                    Products = mqDtos,
                    UserId = accountManager.GetUserId(),
                    UserName = accountManager.GetUserName(),
                    CreatedAt = DateTime.UtcNow
                });

                PushDingDingNotification(accountManager.GetUserName(),dtos);

                excelImportOutputDTO.IsSuccess = true;


                // Spark 2023.08.03 set cachemanager;
                List<string> skus = dtos.Select(x => x.SKU).ToList();
                var cacheKeys = skus.Select(x => string.Format(CacheKeys.PRODUCTKEYFORMAT, x.Trim().ToUpper())).ToList();
                var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);

                List<ProductEntity> cacheProducts = cacheManager.GetOrDefault<string, ProductEntity>(cacheKeys.ToArray()).ToList();
                cacheProducts.RemoveAll(x => x == null);

                var dbProducts = productRepository.GetAll().Where(x => skus.Contains(x.SKU)).ToList();

                dbProducts.ForEach(x =>
                {
                    cacheManager.Set(string.Format(CacheKeys.PRODUCTKEYFORMAT, x.SKU.Trim().ToUpper()), x, TimeSpan.FromHours(24));
                });


                return excelImportOutputDTO;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("导入excel发生异常", ex);
                if (ex is UserFriendlyException)
                {
                    throw ex;
                }
                throw new UserFriendlyException("导入excel发生异常", ex.ToString());
            }
        }

        private void UpdateProductBusiness(List<string> businessNames, string sku)
        {
            if (businessNames.IsNullOrEmpty() || string.IsNullOrEmpty(sku))
            {
                return;
            }

            var oldSalesBusiness = productSalesBusinessRepository.GetAll().Where(x => x.SKU == sku).ToList();
            var businesses = xerpBusinessRepository.GetAll().Select(x => x.Name).ToList();

            //Delete
            foreach (var item in oldSalesBusiness)
            {
                if (!businessNames.Contains(item.BusinessName))
                {
                    productSalesBusinessRepository.Delete(item);
                }
            }

            //Insert
            foreach (var item in businessNames)
            {
                if (oldSalesBusiness.Any(x => x.BusinessName == item)
                    || !businesses.Contains(item))
                {
                    continue;
                }

                ProductSalesBusinessEntity productSalesBusinessEntity = new ProductSalesBusinessEntity
                {
                    Active = true,
                    BusinessName = item,
                    SKU = sku,
                    CreationTime = DateTime.Now
                };
                productSalesBusinessRepository.Insert(productSalesBusinessEntity);
            }
        }

        [Authorize]
        public override PagedQueryOutputBaseDTO<ProductDTO> GetList(PagedQueryInputBaseDTO<ProductDTO> input)
        {
            input.Sort = "CreateTime";//ELLIOT
            string barcode = input.Filter?.Barcode1;
            if (barcode != null)
            {
                input.Filter.Barcode1 = null;
            }
            IQueryable<ProductEntity> query = base.BuildQuery(input)
                            .Include(x => x.Brand)
                            .WhereIf(!barcode.IsNullOrWhiteSpace(), x => x.Barcode1.Contains(barcode) || x.Barcode2.Contains(barcode) || x.Barcode3.Contains(barcode) || x.Barcode4.Contains(barcode));
            return base.PaginateQuery(query, input);
        }


        private void PushDingDingUpdateDataNotification(string username, List<ProductStockChangedDTO> changedskustock, bool isUpdate = false)
        {

            Dictionary<string, string> LogTags = new Dictionary<string, string>
            {
                ["jobid"] = Guid.NewGuid().ToString(),
                ["method"] = "PushDingDingUpdateDataNotification"
            };

            //StringBuilder ddMessage = new StringBuilder();
            string title = string.Empty;
            string optinfo = "# (" + username + " ) " + DateTime.UtcNow.AddHours(8).ToString("yyyy-MM-dd HH:mm:ss") + "【北京时间】";
            if (isUpdate)
            {
                title = optinfo + " 将以下SKU批量更新了库存信息(从Excel批量修改) \n\n";
            }

            //string ddtoken = "9578cfebb98e3e2132a9a02eaadad3013dceaf3749656d1b6a5825b50f8d05bc";
            //ddMessage.Append(title);

            CLSLogger.Info("将以下SKU批量更新了库存信息(从Excel批量修改)", $"title:{ title }  SKU：{changedskustock.ToJsonString()} ", LogTags);
            //int pageSize = 10;
            //int totalPage = changedskustock.Count % pageSize == 0 ? changedskustock.Count / pageSize : changedskustock.Count / pageSize + 1;

            //for (int i = 0; i < totalPage; i++)
            //{
            //    var Items = changedskustock.OrderBy(x => x.Sku).Skip(i * pageSize).Take(pageSize).ToList();

            //    CLSLogger.Info("将以下SKU批量更新了库存信息", $"SKU：{Items.ToJsonString()} ", LogTags);
            //    foreach (var item in Items)
            //    {
            //        if (item.Qty_availableBefore != item.Qty_availableAfter)
            //        {
            //            ddMessage.Append($"{item.Sku} 当前有效库存(Before):{item.Qty_availableBefore} 上传库存数量:{item.Qty_upload} 新有效库存(After):**_<font face=华云彩绘 color=#FF0000> {item.Qty_availableAfter} </font>_** \n\n");
            //        }
            //        DDNotificationProxy.MarkdownNotification(title, ddMessage.ToString(), ddtoken);
            //        CLSLogger.Info("将以下SKU批量更新了库存信息(从Excel批量修改)", $"SKU：{Items.ToJsonString()} ", LogTags);
            //    }
            //}
        }
     

        private void PushDingDingNotification(string username, List<ProductDTO> dtos, bool isUpdate = false)
        {
            StringBuilder ddMessage = new StringBuilder();
            string title = string.Empty;
            string optinfo = "# (" + username + " ) " + DateTime.UtcNow.AddHours(8).ToString("yyyy-MM-dd HH:mm:ss") + "【北京时间】";
            if (isUpdate)
            {
                title = optinfo +  " 将以下SKU批量更新了信息 \n\n";
            }
            else
            {
                title = optinfo + " 将以下SKU为上新产品，请相关人员及时维护信息 \n\n";
            }

            string ddtoken = "9578cfebb98e3e2132a9a02eaadad3013dceaf3749656d1b6a5825b50f8d05bc";
            ddMessage.Append(title);

            foreach (var item in dtos)
            {
                ddMessage.Append($"{item.SKU}&nbsp;&nbsp;&nbsp;&nbsp;{item.ChineseName} \n\n");
            }
            DDNotificationProxy.MarkdownNotification(title, ddMessage.ToString(), ddtoken);
        }

        public bool SyncProductInfo(BaseMqDTO<ProductInfoMqDto> mqDto)
        {
            Dictionary<string, string> logTags = new Dictionary<string, string>();
            logTags["type"] = "syncproductinfo";
            try
            {
                CLSLogger.Info("同步产品信息", mqDto.ToJsonString(), logTags);
                if (mqDto == null || mqDto.MessageBody == null || mqDto.MessageBody.After == null)
                {
                    CLSLogger.Error("同步产品信息发生异常", "消息体信息异常", logTags);
                    return false;
                }

                logTags["sku"] = mqDto.MessageBody.After.SKU;

                var afterDto = mqDto.MessageBody.After;
                var warehouse = warehouseRepository.FirstOrDefault(x => x.WareHouseId == afterDto.WarehouseID);

                if (warehouse == null)
                {
                    CLSLogger.Error("同步产品信息发生异常", "未找到对应warehouse", logTags);
                    throw new UserFriendlyException("同步产品信息发生异常，未找到对应warehouse");
                }

                var packMaterial = materialRepository.FirstOrDefault(x => x.MaterialsID == 0);

                if (packMaterial == null)
                {
                    CLSLogger.Error("同步产品信息发生异常", "未找到对应packmaterial", logTags);
                    throw new UserFriendlyException("同步产品信息发生异常，未找到对应packmaterial");
                }


                if (!warehouse.IsWMS)
                {
                    CLSLogger.Info("同步产品信息", "非富勒管理产品不同步", logTags);
                    //非富勒管理产品不同步
                    return false;
                }

                var sendProducts = new List<Flux.API.Models.Product>();

                string activeFlag = mqDto.MessageBody.Action == MqActionEnum.Delete ? "N" : "Y";

                var pro = new Flux.API.Models.Product
                {
                    CustomerID = afterDto.CustomerID,
                    Active_Flag = activeFlag,
                    SKU = afterDto.SKU.ToUpper(),
                    ReservedField02 = afterDto.SKU,
                    Descr_C = afterDto.ChineseName,
                    Descr_E = afterDto.EnglishName,
                    GrossWeight = afterDto.GrossWeight != null ? Convert.ToDecimal(afterDto.GrossWeight) : 0,
                    NetWeight = afterDto.NetWeight != null ? Convert.ToDecimal(afterDto.NetWeight) : 0,
                    SKULength = afterDto.Length != null ? Convert.ToDecimal(afterDto.Length) : 0,
                    SKUHigh = afterDto.High != null ? Convert.ToDecimal(afterDto.High) : 0,
                    SKUWidth = afterDto.Wide != null ? Convert.ToDecimal(afterDto.Wide) : 0,
                    Alternate_SKU1 = afterDto.Barcode1,
                    Alternate_SKU2 = afterDto.Barcode2,
                    //Alternate_SKU3保留
                    Alternate_SKU4 = afterDto.Barcode4,
                    ReservedField01 = packMaterial.Name,
                    ReservedField09 = packMaterial.Score.ToString(),
                    ReservedField08 = packMaterial.Cost.ToString(),
                    SKU_Group9 = afterDto.PurchasePeriodCode,
                };

                sendProducts.Add(pro);


                // Add Async Task Spark 2022.11.30
                backgroundJobManager.EnqueueAsync<SyncProductToFluxJob, List<Flux.API.Models.Product>>(sendProducts, BackgroundJobPriority.High, TimeSpan.FromSeconds(3));

                //FluxResponse fluxResponse = FluxProxy.PushSKUData(sendProducts.ToArray());

                //if (fluxResponse == null || fluxResponse.Response == null || fluxResponse.Response.@return == null
                //    || fluxResponse.Response.@return.returnFlag != "1")
                //{
                //    CLSLogger.Error("同步产品信息发生异常", $"富勒返回异常，response：{fluxResponse.ToJsonString()}", logTags);
                //    throw new UserFriendlyException("同步产品信息发生异常", "富勒返回异常");
                //}

                return true;
            }
            catch (Exception ex)
            {
                if (ex is UserFriendlyException)
                {
                    throw ex;
                }
                CLSLogger.Error("同步产品信息发生异常", ex, logTags);
                throw new UserFriendlyException("同步产品信息发生异常", ex);
            }

        }

        private void GetOdooProductParameterIds(IEnumerable<ProductDTO> products, out StringBuilder msg)
        {
            msg = new StringBuilder();
            var odooProxy = new OdooProxy();
            var odooCategories = odooProxy.GetAllCategories().Result;

            foreach (var product in products)
            {
                if (product.OdooCategory != null)
                {
                    var odooCategory = odooCategories.FirstOrDefault(x =>
                        (x["complete_name"].ToString().Replace(" ", String.Empty).ToUpper() == product.OdooCategory?.Replace(" ", String.Empty)?.ToUpper())
                        || (x["id"].ToObject<long>() == product.OdooCategoryID)
                    );

                    if (odooCategory == null)
                    {
                        msg.AppendLine($"产品sku:{product.SKU}；未找到产品分级{product.OdooCategory}");
                    }
                    else
                    {
                        product.OdooCategoryID = odooCategory["id"].ToObject<long>();
                    }
                }
            }
        }

        private void ValidateOdooParameters(List<ProductDTO> products, out StringBuilder msg)
        {
            msg = new StringBuilder();

            foreach (var product in products)
            {
                if (string.IsNullOrEmpty(product.Specs))
                {
                    msg.Append("规格不能为空;");
                }
                if (string.IsNullOrEmpty(product.TrackingType))
                {
                    msg.Append("追踪属性不能为空; ");
                }
                else
                {
                    string[] trackingTypes = new string[] { "lot", "none" };
                    if (!trackingTypes.Contains(product.TrackingType))
                    {
                        msg.Append($"不存在追踪属性{product.TrackingType}, 追踪属性只能为lot(批次保质期跟踪)或none(无批次追踪)");
                    }
                }

                if (product.OdooCategory == null && product.OdooCategoryID == null)
                {
                    msg.Append("产品分级不能为空; ");
                }

                if (!product.BrandName.IsNullOrEmpty())
                {
                    var brand = brandRepository.FirstOrDefault(x => x.ChineseName == product.BrandName || x.ShortCode == product.BrandName || x.EnglishName == product.BrandName);
                    if (brand?.OdooBrandId == null)
                    {
                        msg.Append($"品牌并未关联odoo：{product.BrandName}；");
                    }
                    else
                    {
                        product.OdooBrandID = brand.OdooBrandId;
                    }
                }
            }
        }
        private void ValidateUpdateOdooParameters(List<ProductDTO> products, List<ProductEntity> entities, out StringBuilder msg)
        {
            msg = new StringBuilder();

            var hhSkus = products.Select(x =>
            {
                var hhSku = !x.HHSKU.IsNullOrWhiteSpace() ? x.HHSKU : entities.FirstOrDefault(y => y.SKU == x.SKU)?.HHSKU;
                return hhSku;
            });

            var odooProducts = _odooProxy.GetProducts(
                new OdooDomainFilter().Filter("default_code", "in", hhSkus),
                new OdooFieldParameters()
                {
                    "id",
                    "default_code",
                    "barcode"
                }
            ).Result;

            foreach (var product in products)
            {
                var entity = entities.FirstOrDefault(x => x.SKU.ToUpper().Trim() == product.SKU.ToUpper().Trim());
                bool syncToOdoo = product.SyncToOdoo == true || (entity?.SyncToOdoo == true && product.SyncToOdoo != false);
                // bool isHHPurchase = product.IsHHPurchase == true || (entity?.IsHHPurchase == true && product.IsHHPurchase != false);
                bool hhSkuChanged = !product.HHSKU.IsNullOrWhiteSpace() && product.HHSKU != entity?.HHSKU;
                string hhSku = !product.HHSKU.IsNullOrWhiteSpace() ? product.HHSKU : entity?.HHSKU;
                if (syncToOdoo)
                {
                    if (hhSku.IsNullOrWhiteSpace())
                    {
                        if (entity.OdooProductID.HasValue == false)
                        {
                            msg.Append($"产品: {product.SKU}并未关联odoo，请先在xerp上关联或填上HHSKU");
                        }
                    }
                    else
                    {
                        if (hhSkuChanged && !product.HHSKU.IsNullOrWhiteSpace())
                        {
                            var odooProduct = odooProducts.FirstOrDefault(x => x["default_code"].ToString() == entity.HHSKU);
                            if (odooProduct != null)
                            {
                                msg.Append($"HHSKU: {hhSku}已被使用");
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(product.TrackingType))
                {
                    string[] trackingTypes = new string[] { "lot", "none" };
                    if (!trackingTypes.Contains(product.TrackingType))
                    {
                        msg.Append($"不存在追踪属性{product.TrackingType}, 追踪属性只能为lot或none");
                    }
                }

                if (!product.BrandName.IsNullOrEmpty())
                {
                    var brand = brandRepository.FirstOrDefault(x => x.ChineseName == product.BrandName || x.ShortCode == product.BrandName || x.EnglishName == product.BrandName);
                    if (brand?.OdooBrandId == null)
                    {
                        msg.Append($"品牌并未关联odoo：{product.BrandName}；");
                    }
                    else
                    {
                        product.OdooBrandID = brand.OdooBrandId;
                    }
                }
                else if (product.BrandId.HasValue)
                {
                    var brand = brandRepository.FirstOrDefault(x => x.Id == product.BrandId);
                    if (brand?.OdooBrandId == null)
                    {
                        msg.Append($"品牌并未关联odoo, 品牌id: {product.BrandId}；");
                    }
                    else
                    {
                        product.OdooBrandID = brand.OdooBrandId;
                    }
                }
            }
        }

        /// <summary>
        /// EXCEL 批量修改
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [Authorize]
        public bool BatchUpdate(IFormFile file)
        {
            if (accountManager.GetUserId().IsNullOrWhiteSpace())
            {
                CLSLogger.Error("ProductAPPService_BatchUpdate,当前用户信息为空", "");
                throw new UserFriendlyException("开始批量更新SKU信息时, 用户信息为空，重新登录后重试！");
            }
            try
            {
                //
                List<ProductStockChangedDTO> changedskustock = new List<ProductStockChangedDTO>();

                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = "productbatchupdate";
                tags["trackid"] = file.FileName;

                CLSLogger.Info("开始批量更新SKU信息", file.FileName, tags);

                if (file.Length <= 0)
                {
                    throw new UserFriendlyException("文件大小异常，请选择正确的文件");
                }

                string filePath = Ultities.CreateDirectory("App_Data/ExcelImport");
                string fileName = $"{Guid.NewGuid()}{file.FileName}";
                string fullPath = Path.Combine(filePath, fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                var mapper = new Npoi.Mapper.Mapper(fullPath);
                var dtos = mapper.Take<ProductDTO>().Where(x => x.Value != null && !string.IsNullOrEmpty(x.Value.SKU)).Select(x => x.Value).ToList();

                File.Delete(fullPath);

                if (dtos.IsNullOrEmpty())
                {
                    throw new UserFriendlyException("excel中未找到有效数据，请确保excel模板正确且有数据");
                }

                if (dtos.Count() > 2000)
                {
                    throw new UserFriendlyException("单次批量上传不能超过2000个产品");
                }

                string error = string.Empty;
                List<string> skus = dtos.Select(x => x.SKU).ToList();
                var entities = productRepository.GetAll().Where(x => skus.Contains(x.SKU)).ToList();
                var stocks = apiStockRepository.GetAll().Where(x => skus.Contains(x.SKU)).ToList();
                var imageType = ImageTypeEnum.Product.ToString();
                var images = imageRepository.GetAll().Where(x => skus.Contains(x.ReferenceId) && x.ImageType == imageType).AsEnumerable().MapTo<List<ImageDTO>>();

                StringBuilder odooValidationErrors;
                GetOdooProductParameterIds(dtos.Where(x => x.OdooCategory != null), out odooValidationErrors);
                if (odooValidationErrors.Length > 0)
                {
                    throw new UserFriendlyException(odooValidationErrors.ToString());
                }
                ValidateUpdateOdooParameters(dtos, entities, out odooValidationErrors);
                if (odooValidationErrors.Length > 0)
                {
                    throw new UserFriendlyException(odooValidationErrors.ToString());
                }

                List<ProductInfoMqDto> mqDtos = new List<ProductInfoMqDto>();

                int rowIndex = 2;
                var detailImage = ProductImageTypeEnum.Detail.ToString();
                var mainImage = ProductImageTypeEnum.Main.ToString();

                List<string> AllSyncEcinxSku = new List<string>(); // 所有需要同步到Ecinx的SKU
                foreach (var item in dtos)
                {

                    //log sku stock changed;
                    ProductStockChangedDTO _changedskustock = new ProductStockChangedDTO();

                    var entity = entities.FirstOrDefault(x => x.SKU.Trim().ToUpper() == item.SKU.Trim().ToUpper());
                    if (entity == null)
                    {
                        throw new UserFriendlyException($"SKU：{item.SKU}不存在无法更新；");
                    }

                    // current product images
                    var mainImages = images.Where(x => x.ReferenceId == item.SKU && x.ReferenceType == mainImage).ToList();
                    var detailImages = images.Where(x => x.ReferenceId == item.SKU && x.ReferenceType == detailImage).ToList();

                    ProductInfoMqDto productInfoMqDto = new ProductInfoMqDto();
                    productInfoMqDto.Action = MqActionEnum.Update;
                    productInfoMqDto.Before = entity.MapTo<ProductDTO>();
                    productInfoMqDto.Before.MainImages = mainImages;
                    productInfoMqDto.Before.DetailImages = detailImages;

                    //item.OdooProductID = entity.OdooProductID;

                    ValidateForBatchUpdate(item, entity, out error);

                    if (!string.IsNullOrEmpty(error))
                    {
                        throw new UserFriendlyException($"导入出错请检查修正；第{rowIndex}行：{error}");
                    }
                    entity.UpdateTime = DateTime.Now;
                    //entity.ECinxSKU = item.ECinxSKU;

                    ObjectMapper.Map<ProductDTO, ProductEntity>(item, entity);

                    if (item.SyncToOdoo == true || (entity?.SyncToOdoo == true && item.SyncToOdoo != false))
                    {
                        entity.OdooSyncStatus = OdooSyncStatusEnum.Pending.ToString();
                    }

                    productInfoMqDto.After = ObjectMapper.Map<ProductDTO>(entity);
                    productInfoMqDto.After.MainImages = mainImages;
                    productInfoMqDto.After.DetailImages = detailImages;
                    //productInfoMqDto.After.ECinxSKU = item.ECinxSKU;

                    mqDtos.Add(productInfoMqDto);
                    
                    //
                    _changedskustock.Sku = item.SKU.Trim().ToUpper();

                    //更新库存/保质期
                    if (item.VirtualStockQuantity.HasValue || !item.Bestbefore.IsNullOrWhiteSpace())
                    {
                        
                        var stock = stocks.FirstOrDefault(x => x.SKU.Trim().ToUpper() == item.SKU.Trim().ToUpper());
                        if (stock == null)
                        {
                            stock = new APIStockEntity
                            {
                                SKU = entity.SKU,
                                Flag = 1,
                                ProductNameCN = entity.ChineseName,
                                ProductNameEN = entity.EnglishName,
                                Qty_baseline = 0,
                                Qty_cache = 0,
                                Qty_heweb = 0,
                                Qty_locked = 0,
                            };
                            apiStockRepository.Insert(stock);
                            //
                            _changedskustock.Qty_availableBefore = 0;
                            _changedskustock.Qty_upload = item.VirtualStockQuantity.Value; // RealUpdateQTY;
                            _changedskustock.Qty_availableAfter = item.VirtualStockQuantity.Value; // RealUpdateQTY;
                        }
                        if (item.VirtualStockQuantity.HasValue)
                        {
                            int _Qty_available = stock.Qty_available;
                            stock.Qty_aladdin = item.VirtualStockQuantity.Value;
                            stock.Qty = item.VirtualStockQuantity.Value;
                            stock.Qty_available = item.VirtualStockQuantity.Value - stock.Qty_locked;

                            _changedskustock.Qty_availableBefore = _Qty_available;
                            _changedskustock.Qty_upload = item.VirtualStockQuantity.Value; // RealUpdateQTY;
                            _changedskustock.Qty_availableAfter = item.VirtualStockQuantity.Value - stock.Qty_locked; ; // RealUpdateQTY;
                        }
                        if (!item.Bestbefore.IsNullOrWhiteSpace())
                        {
                            DateTime tempDate;
                            stock.Expired_date = DateTime.TryParse(item.Bestbefore, out tempDate) ? tempDate.ToString("yyyy/MM/dd") : item.Bestbefore;
                        }

                        stock.Syc_flag = false;
                        stock.Updated_time = DateTime.Now;
                    }

                    //更新分单分类
                    if (item.SplitOrderCategoryID.HasValue && item.SplitOrderCategoryID > 0)
                    {
                        UpdateProductSplitOrderCategory(item);
                    }

                    changedskustock.Add(_changedskustock);

                   
                    if (item.SyncToEcinx == true)
                    {
                        AllSyncEcinxSku.Add(item.SKU);
                    }

                    rowIndex++;
                }

                // 同步Ecinx;
                if (AllSyncEcinxSku.Count>0)
                {
                    backgroundJobManager.EnqueueAsync<CreateOrUpdateEcinxProduct, List<string>>(AllSyncEcinxSku);
                }



                CurrentUnitOfWork.SaveChanges();

                var odooProductSkus = mqDtos.Where(x => x?.After.SyncToOdoo == true).Select(x => x.After.SKU).ToList();

                if (odooProductSkus.Count > 0)
                {
                    backgroundJobManager.EnqueueAsync<CreateOrUpdateOdooProductJob, List<string>>(odooProductSkus);
                }

                backgroundJobManager.Enqueue<ProductUpdateNotifyJob, ProductUpdateNotifyJobArgs>(new ProductUpdateNotifyJobArgs
                {
                    Products = mqDtos,
                    UserId = accountManager.GetUserId(),
                    UserName = accountManager.GetUserName(),
                    CreatedAt = DateTime.UtcNow
                });

                backgroundJobManager.EnqueueAsync<ProductRelatedInfoValidationJob, List<string>>(dtos.Select(x => x.SKU).ToList());

                //同步虚拟库存给销售渠道（188，海淘）
                var hasVirtualStockProducts = dtos.Where(x => x.VirtualStockQuantity.HasValue || !x.Bestbefore.IsNullOrWhiteSpace()).Select(x => x.SKU).ToList();
                if (!hasVirtualStockProducts.IsNullOrEmpty())
                {
                    backgroundJobManager.Enqueue<RefreshSKULockQtyJob, List<string>>(hasVirtualStockProducts);
                    backgroundJobManager.Enqueue<SyncAladdinStockJob, List<string>>(hasVirtualStockProducts, BackgroundJobPriority.High);
                }

                //mqDtos.FirstOrDefault().Before.Qty,
                //mqDtos.FirstOrDefault().After.Qty,
                
                //spark
                PushDingDingUpdateDataNotification(accountManager.GetUserName(), changedskustock, true);


                // Spark 2023.08.03 set cachemanager;
                var cacheKeys = skus.Select(x => string.Format(CacheKeys.PRODUCTKEYFORMAT, x.Trim().ToUpper())).ToList();
                var cacheManager = Cache.GetCache(CacheKeys.GENERALCACHEMANAGERKEY);

                List<ProductEntity> cacheProducts = cacheManager.GetOrDefault<string, ProductEntity>(cacheKeys.ToArray()).ToList();
                cacheProducts.RemoveAll(x => x == null);

                var dbProducts = productRepository.GetAll().Where(x => skus.Contains(x.SKU)).ToList();

                dbProducts.ForEach(x =>
                {
                    cacheManager.Set(string.Format(CacheKeys.PRODUCTKEYFORMAT, x.SKU.Trim().ToUpper()), x, TimeSpan.FromHours(24));
                });


                CLSLogger.Info("批量更新SKU信息结束", $"已更新{rowIndex - 1}条产品信息", tags);

                return true;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("导入excel发生异常", ex);
                if (ex is UserFriendlyException)
                {
                    throw ex;
                }
                throw new UserFriendlyException("导入excel发生异常", ex.ToString());
            }
        }



        /// <summary>
        /// 更新保税仓的基本信息 同步到188； CMQ；  WarehouseIDs = new List<int> { 7, 15, 28, 33, 39 };
        /// </summary>
        /// <returns></returns>
        public bool BASProductUpdate()
        {
          
            try
            {
             
                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = "productbatchupdate";
                tags["trackid"] = "AllProductUpdate";

                var WarehouseIDs = new List<int> { 7, 15, 28, 33, 39 };
                var dtos = productRepository.GetAll().Where(x => WarehouseIDs.Contains(x.WarehouseID.GetValueOrDefault()) ).ToList();

                List<string> skus = dtos.Select(x =>x.SKU).ToList();

                var entities = productRepository.GetAll().Where(x => skus.Contains(x.SKU)).ToList();
                var imageType = ImageTypeEnum.Product.ToString();
                var images = imageRepository.GetAll().Where(x => skus.Contains(x.ReferenceId) && x.ImageType == imageType).AsEnumerable().MapTo<List<ImageDTO>>();


                List<ProductInfoMqDto> mqDtos = new List<ProductInfoMqDto>();

                int rowIndex = 2;
                var detailImage = ProductImageTypeEnum.Detail.ToString();
                var mainImage = ProductImageTypeEnum.Main.ToString();
                foreach (var item in dtos)
                {

                    var entity = entities.FirstOrDefault(x => x.SKU.Trim().ToUpper() == item.SKU.Trim().ToUpper());
                    if (entity == null)
                    {
                        throw new UserFriendlyException($"SKU：{item.SKU}不存在无法更新；");
                    }

                    // current product images
                    var mainImages = images.Where(x => x.ReferenceId == item.SKU && x.ReferenceType == mainImage).ToList();
                    var detailImages = images.Where(x => x.ReferenceId == item.SKU && x.ReferenceType == detailImage).ToList();

                    ProductInfoMqDto productInfoMqDto = new ProductInfoMqDto();
                    productInfoMqDto.Action = MqActionEnum.Update;
                    productInfoMqDto.Before = entity.MapTo<ProductDTO>();
                    productInfoMqDto.Before.MainImages = mainImages;
                    productInfoMqDto.Before.DetailImages = detailImages;

                    // item.OdooProductID = entity.OdooProductID;
                    productInfoMqDto.After = ObjectMapper.Map<ProductDTO>(entity);
                    productInfoMqDto.After.MainImages = mainImages;
                    productInfoMqDto.After.DetailImages = detailImages;
                    mqDtos.Add(productInfoMqDto);
                    rowIndex++;
                }
                CurrentUnitOfWork.SaveChanges();

                backgroundJobManager.Enqueue<ProductUpdateNotifyJob, ProductUpdateNotifyJobArgs>(new ProductUpdateNotifyJobArgs
                {
                    Products = mqDtos,
                    UserId = "spark", //accountManager.GetUserId(),
                    UserName = "spark", //accountManager.GetUserName(),
                    CreatedAt = DateTime.UtcNow
                });


                CLSLogger.Info("批量更新SKU信息结束", $"已更新{rowIndex - 1}条产品信息", tags);

                return true;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("批量更新SKU信息结束 失败", ex);
                return false;
            }
        }


        /// <summary>
        /// Batch get stock of skus
        /// </summary>
        /// <param name="inputDto"></param>
        /// <returns></returns>
        public List<BatchGetProductStockOutDTO> BatchGetStocks(BatchGetProductStockInputDTO inputDto)
        {

            if (inputDto == null || inputDto.SKUs.IsNullOrEmpty())
            {
                throw new RequestValidationException("Invalid request, check skus parameter");
            }

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "batchgetstocks";
            tags["trackid"] = Guid.NewGuid().ToString();
            tags["sku"] = string.Join(";", inputDto.SKUs);

            CLSLogger.Info("BatchGetStocks请求报文", inputDto.ToJsonString(), tags);

            ProductDomainService productDomainService = new ProductDomainService();
            var products = productDomainService.GetProductsFromCache(inputDto.SKUs);

            if (products.IsNullOrEmpty())
            {
                throw new RequestValidationException("Can not find any sku");
            }

            var listStockVO = productDomainService.GetStocks(inputDto.Partner, inputDto.SKUs, products);

            //get sku stock lock for hh
            List<OrderSKULockForHHEntity> listAllSKULockHH = productDomainService.GetCurrentSKUStockLockForHH(inputDto.SKUs);

            StockShareDomainService stockShareDomainService = new StockShareDomainService();
            StockShareConfigVO configStockShare = stockShareDomainService.GetStockShareConfig();

            var productStocks = new List<BatchGetProductStockOutDTO>();

            SyncAladdinStockJob syncAladdinStockJob = new SyncAladdinStockJob();

            foreach (var item in listStockVO)
            {
                APIStockDTO apiStockDTO = new APIStockDTO
                {
                    SKU = item.Sku,
                    Qty_available = item.Qty_available,
                    Qty_hh = item.OriginalHHStock,
                    Expired_date = item.ExpiredDate,
                    Expiry_date_hh = item.OriginalHHBestBefore,
                    Qty_locked = item.LockQty
                };

                var currentStock = syncAladdinStockJob.GetCalculatedSKUStock(apiStockDTO, products, listAllSKULockHH, productDomainService, configStockShare);

                BatchGetProductStockOutDTO stockOutput = new BatchGetProductStockOutDTO
                {
                    Sku = currentStock.sku.Trim(),
                    Qty = currentStock.stock,
                    ExpiredDate = currentStock.best_before,
                    OriginalBestBefore = currentStock.original_best_before,
                    OriginalHHBestBefore = currentStock.original_hh_best_before,
                    OriginalStock = currentStock.original_stock,
                    OriginalHHStock = currentStock.original_hh_stock
                };

                productStocks.Add(stockOutput);
            }

            inputDto.SKUs.ForEach(x =>
            {
                if (!productStocks.Any(item => item.Sku == x.Trim()) && products.Any(item => item.SKU.Trim() == x.Trim()))
                {
                    productStocks.Add(new BatchGetProductStockOutDTO
                    {
                        Sku = x,
                        Qty = 0
                    });
                }
            });

            CLSLogger.Info("BatchGetStocks响应报文", productStocks.ToJsonString(), tags);

            return productStocks;
        }


        /// <summary>
        /// Batch get stock of skus
        /// </summary>
        /// <param name="inputDto"></param>
        /// <returns></returns>
        public async Task<List<BatchGetProductStockOutDTO>> BatchGetStocksAsync(BatchGetProductStockInputDTO inputDto)
        {

            if (inputDto == null || inputDto.SKUs.IsNullOrEmpty())
            {
                throw new RequestValidationException("Invalid request, check skus parameter");
            }

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "batchgetstocks";
            tags["trackid"] = Guid.NewGuid().ToString();
            tags["sku"] = string.Join(";", inputDto.SKUs);

            CLSLogger.Info("BatchGetStocks请求报文", inputDto.ToJsonString(), tags);

            ProductDomainService productDomainService = new ProductDomainService();
            var products = await productDomainService.GetProductsFromCacheAsync(inputDto.SKUs);

            if (products.IsNullOrEmpty())
            {
                throw new RequestValidationException("Can not find any sku");
            }

            var productStocks = (await productDomainService.GetStocksAsync(inputDto.Partner, inputDto.SKUs, products))
                .Select(x => new BatchGetProductStockOutDTO
                {
                    Sku = x.Sku.Trim(),
                    Qty = x.Qty_available,
                    ExpiredDate = x.ExpiredDate,
                    OriginalBestBefore = x.OriginalBestBefore,
                    OriginalHHBestBefore = x.OriginalHHBestBefore,
                    OriginalStock = x.OriginalStock,
                    OriginalHHStock = x.OriginalHHStock
                }).ToList();

            inputDto.SKUs.ForEach(x =>
            {
                if (!productStocks.Any(item => item.Sku == x.Trim()) && products.Any(item => item.SKU.Trim() == x.Trim()))
                {
                    productStocks.Add(new BatchGetProductStockOutDTO
                    {
                        Sku = x,
                        Qty = 0
                    });
                }
            });

            CLSLogger.Info("BatchGetStocks响应报文", productStocks.ToJsonString(), tags);

            return productStocks;
        }


        // TODO: create helper methods for each type of locks?
        [HttpGet]
        public List<SkuLockQtyDto> SkuLockQtys(List<string> skus)
        {
            var products = productRepository.GetAll()
                .Where(x => skus.Contains(x.SKU))
                .Select(x => new
                {
                    sku = x.SKU,
                    productName = x.ChineseName
                })
                .ToDictionary(x => x.sku.Trim(), x => x.productName);

            // Cache Orders
            // TODO: create enum for lock types
            var cacheOrderLocks = cacheOrderDetailRepository.GetAll()
                .Where(x => skus.Contains(x.SKU))
                .Select(x =>
                    new SkuLockQtyDto
                    {
                        Sku = x.SKU,
                        OrderId = x.OrderID,
                        LockedQty = x.Qty,
                        CreatedAt = x.CTime,
                        Oversale = x.Oversale
                    }
                )
                .ToList();

            cacheOrderLocks.ForEach(x =>
            {
                x.LockType = "cacheOrder";
            });

            // Order Packs
            var freezeFlags = new List<int> { 0, 7, 8 };
            StringBuilder freezeFlagsSql = new StringBuilder();
            StringBuilder skusSql = new StringBuilder();
            List<SqlParameter> parameters = new List<SqlParameter>();

            for (int i = 0; i < freezeFlags.Count; i++)
            {
                freezeFlagsSql.Append($"@freezeFLags{i},");
                parameters.Add(new SqlParameter("@freezeFLags" + i, freezeFlags[i]));
            }

            for (int i = 0; i < skus.Count; i++)
            {
                skusSql.Append($"@skus{i},");
                parameters.Add(new SqlParameter("@skus" + i, skus[i]));
            }
            freezeFlagsSql.Length--;
            skusSql.Length--;

            // TODO: avoid allowing sql injection
            string orderpackSQL =
            $@"SELECT detail.sku, pack.orderid, pack.orderpackno, detail.product_name, detail.oversale, detail.qty, pack.opt_time 
            FROM dbo.orders_pack AS pack WITH (NOLOCK)
            INNER JOIN dbo.orders_pack_detail AS detail WITH(NOLOCK)
            ON pack.opid = detail.opid
            WHERE
            detail.flag = 1
            AND detail.sku IN ({skusSql})
            AND pack.[freezeflag]  in ({freezeFlagsSql})
            AND pack.assign_time IS NULL 
            AND pack.status= 0 
            AND pack.out_time IS NULL
            AND pack.opt_time>=dateadd(day,-60, getdate())";

            var hEERPDbContext = new HEERPDbContextFactory().CreateDbContext();
            var orderPackLocks = hEERPDbContext.OrderPackDBQuery.FromSql(orderpackSQL, parameters.ToArray())
                .MapTo<List<SkuLockQtyDto>>()
                .ToList();

            var fluxWMSDbContext = new FluxWMSDbContextFactory().CreateDbContext();
            var orderPackNos = orderPackLocks.Select(x => x.OrderPackNo).ToList();

            var fluxOrderDic = fluxWMSDbContext.FluxDocOrderHeaders.Where(x => orderPackNos.Contains(x.OrderPackNo))
                                                                    .Select(x => new
                                                                    {
                                                                        fluxOrderNo = x.FluxOrderNo,
                                                                        orderPackNo = x.OrderPackNo
                                                                    })
                                                                    .ToDictionary(x => x.orderPackNo, x => x.fluxOrderNo);

            var fluxOrderNos = fluxOrderDic.Values.ToList();
            var fluxOrderDetails = fluxWMSDbContext.FluxDocOrderDetails
                                     .Where(x => fluxOrderNos.Contains(x.OrderNo))
                                     .Select(x => new
                                     {
                                         x.SKU,
                                         x.OrderNo,
                                         x.LotAtt07,
                                         x.QtyOrdered,
                                         x.QtyAllocated
                                     })
                                     .AsEnumerable()
                                     .Select(x => new
                                     {
                                         x.SKU,
                                         x.OrderNo,
                                         QtyAllocated = string.IsNullOrWhiteSpace(x.LotAtt07) ? x.QtyAllocated : x.QtyOrdered
                                         //if LotAtt07 is not empty that means the SKU will be allocated from HH inventory, so should not be considered as part of lock quantity
                                     })
                                     .GroupBy(x => new { x.OrderNo, x.SKU })
                                     .Select(x => new
                                     {
                                         x.Key.OrderNo,
                                         x.Key.SKU,
                                         Qty = (int)x.Sum(y => y.QtyAllocated)
                                     }).ToList();

            orderPackLocks.ForEach(x =>
            {
                x.LockedQty = x.LockedQty - (fluxOrderDetails.FirstOrDefault(y => fluxOrderDic.ContainsKey(x.OrderPackNo) && y.OrderNo == fluxOrderDic[x.OrderPackNo] && y.SKU == x.Sku)?.Qty ?? 0);
                x.LockType = "orderPack";
            });

            orderPackLocks = orderPackLocks.Where(x => x.LockedQty > 0).ToList();

            // Unpaid Orders
            // TODO: join with products table to obtain product name
            var unpaidOrderLock = hEERPDbContext.OrderSkuLocks.Where(x => skus.Contains(x.SKU))
                .Select(x => new SkuLockQtyDto
                {
                    OrderId = x.Orderid,
                    Sku = x.SKU,
                    LockedQty = x.Qty,
                    CreatedAt = x.Ctime
                })
                .ToList();

            unpaidOrderLock.ForEach(x =>
            {
                x.LockType = "unpaidOrder";
            });

            // Unsplit Orders
            var orderStartDate = DateTime.Now.AddDays(-3);
            var unsplitOrderLock = (from order in hEERPDbContext.Orders
                                    join orderDetail in hEERPDbContext.OrderDetails
                                    on order.oid equals orderDetail.oid
                                    where order.status == 0 && order.valflag == 1 && order.orderCreateTime > orderStartDate
                                    && skus.Contains(orderDetail.sku)
                                    select new SkuLockQtyDto
                                    {
                                        LockType = "unsplitOrder",
                                        OrderId = order.orderid,
                                        Sku = orderDetail.sku,
                                        LockedQty = orderDetail.qty,
                                        CreatedAt = order.orderCreateTime
                                    }).ToList();

            var result = cacheOrderLocks.Concat(orderPackLocks).Concat(unpaidOrderLock).Concat(unsplitOrderLock).ToList();
            result.ForEach(x =>
            {
                if (products.ContainsKey(x.Sku))
                {
                    x.ProductName = products[x.Sku];
                }
            });
            return result;
        }

        [HttpGet]
        public List<UserActivityLogDTO> UserActivityLog(string sku)
        {
            List<UserActivityLogDTO> result = userActivityLogRepository.GetAll()
            .Where(x => (x.ObjectId == sku) && (x.ObjectType == UserActivityObjectType.Product_IsHHPurchase || x.ObjectType == UserActivityObjectType.Product))
            .Select(x => new UserActivityLogDTO
            {
                Activity = x.Content,
                Time = x.CreatedAt,
                User = x.UserName,
            })
            .OrderByDescending(x => x.Time)
            .ToList();
            return result;
        }

        public async Task<FileContentResult> GetTemplate()
        {

            try
            {
                var productDto = new ProductDTO
                {
                    SKU = "SKU1234567",
                    CustomerID = "ALADDIN",
                    ActiveFlag = true,
                    Cost = (decimal)6.50,
                    CurrencyID = "NZD",
                    ChineseName = "麦旋风",
                    EnglishName = "McFlurry",
                    BrandName = "Banana Boat",
                    Barcode1 = "BARCODE123",
                    TaxRate = (decimal)0.15,
                    GrossWeight = 226,
                    NetWeight = 226,
                    Warehouse = "新西兰仓",
                    TakeTime = 90,
                    TrackingType = "lot",
                    SplitOrderCategoryName = "化妆品",
                    PurchasePeriodCode = "consignment",
                    CategoryName = "母婴用品",
                    DefaultFreightID = "澳邮",
                    OdooCategory = "All",
                    Specs = "8 ounce",
                    Materials = "不需要包装",
                    LifeTime = 90,
                    BoxQty = 6,
                    Producer = "McDonalds",
                    CountryName = "New Zealand",
                    AlertTime = 30,
                    PersonInChargeID = "创博",
                    Bestbefore = DateTime.UtcNow.ToString(),
                };
                var productType = typeof(ProductDTO);
                HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
                ISheet sheet1 = hssfWorkbook.CreateSheet("Sheet1");
                IRow headerRow = sheet1.CreateRow(0);
                ICellStyle requiredColumnsStyle = hssfWorkbook.CreateCellStyle();
                IFont requiredColumnsFont = hssfWorkbook.CreateFont();
                requiredColumnsFont.Color = HSSFColor.Red.Index;
                requiredColumnsStyle.SetFont(requiredColumnsFont);

                var npoiMapper = new Npoi.Mapper.Mapper(hssfWorkbook);

                // add headers to sheet
                List<string> headers = productType.GetDisplayAttributeValues();
                for (var i = 0; i < headers.Count(); i++)
                {
                    ICell cell = headerRow.CreateCell(i);
                    cell.SetCellValue(headers[i]);
                }

                sheet1.CopyTo(hssfWorkbook, "Sheet2", true, true);
                ISheet sheet2 = hssfWorkbook.GetSheet("Sheet2");

                npoiMapper.Put(new List<ProductDTO> { productDto }, "Sheet1");

                // TODO: determine required cells using attributes instead of hard coding
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.SKU)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.EnglishName)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.Cost)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.Specs)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.BrandName)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.Barcode1)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.NetWeight)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.GrossWeight)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.Warehouse)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.PersonInChargeID)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.CurrencyID)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.DefaultFreightID)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.SplitOrderCategoryName)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.TaxRate)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.OdooCategory)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.PurchasePeriodCode)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.TrackingType)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.CustomerID)))).CellStyle = requiredColumnsStyle;
                headerRow.GetCell(headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.CategoryName)))).CellStyle = requiredColumnsStyle;

                // TODO: store odooCategories in database
                Task<JObject[]> odooCategoriesTask = _odooProxy.GetAllCategories();

                int columnIndex;
                string columnString;
                string[] validationValues;

                // add data validations

                // 激活
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.ActiveFlag)));
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateExplicitListConstraint(new string[] { "TRUE", "FALSE" })
                    )
                );

                // 同步odoo
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.SyncToOdoo)));
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateExplicitListConstraint(new string[] { "TRUE", "FALSE" })
                    )
                );

                // 同步HH库存
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.IsHHPurchase)));
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateExplicitListConstraint(new string[] { "TRUE", "FALSE" })
                    )
                );

                // 品牌
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.BrandName)));
                columnString = CellReference.ConvertNumToColString(columnIndex);
                validationValues = brandRepository.GetAll().Select(x => x.ChineseName).ToArray();
                sheet2.InsertColumn(1, columnIndex, validationValues);
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateFormulaListConstraint($"Sheet2!${columnString}1:${columnString}{validationValues.Length + 1}")
                    )
                );

                // 追踪属性
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.TrackingType)));
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateExplicitListConstraint(Enum.GetNames(typeof(OdooTrackingType)))
                    )
                );

                // 库存类型
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.PurchasePeriodCode)));
                validationValues = purchasePeriodRepository.GetAll().Select(x => x.Name).ToArray();
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateExplicitListConstraint(validationValues)
                    )
                );

                // 货主
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.CustomerID)));
                validationValues = goodsOwnerRepository.GetAll().Select(x => x.Code).ToArray();
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateExplicitListConstraint(validationValues)
                    )
                );

                // 分单分类
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.SplitOrderCategoryName)));
                columnString = CellReference.ConvertNumToColString(columnIndex);
                validationValues = splitOrderProductCategoryRepository.GetAll().Select(x => x.CNName).ToArray();
                sheet2.InsertColumn(1, columnIndex, validationValues);
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateFormulaListConstraint($"Sheet2!${columnString}1:${columnString}{validationValues.Length + 1}")
                    )
                );

                // 默认发货渠道
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.DefaultFreightID)));
                columnString = CellReference.ConvertNumToColString(columnIndex);
                validationValues = freightRepository.GetAll().Select(x => x.Name).ToArray();
                sheet2.InsertColumn(1, columnIndex, validationValues);
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateFormulaListConstraint($"Sheet2!${columnString}1:${columnString}{validationValues.Length + 1}")
                    )
                );

                // 税率
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.TaxRate)));
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateExplicitListConstraint(new string[] { "0.15", "0.1", "0" })
                    )
                );

                // 产品分类
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.CategoryName)));
                columnString = CellReference.ConvertNumToColString(columnIndex);
                validationValues = inventoryProCategoryRepository.GetAll().Select(x => x.Name).ToArray();
                sheet2.InsertColumn(1, columnIndex, validationValues);
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateFormulaListConstraint($"Sheet2!${columnString}1:${columnString}{validationValues.Length + 1}")
                    )
                );

                // 仓库
                //columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.Warehouse)));
                //validationValues = warehouseRepository.GetAll().Select(x => x.Location).ToArray();
                //((HSSFSheet)sheet1).AddValidationData(
                //    new HSSFDataValidation(
                //        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                //        DVConstraint.CreateExplicitListConstraint(validationValues)
                //    )
                //);

                // 仓库 V2
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.Warehouse)));
                columnString = CellReference.ConvertNumToColString(columnIndex);
                validationValues = warehouseRepository.GetAll().Select(x => x.Location).ToArray();
                sheet2.InsertColumn(1, columnIndex, validationValues);
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateFormulaListConstraint($"Sheet2!${columnString}1:${columnString}{validationValues.Length + 1}")
                    )
                );


                // 负责人
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.PersonInChargeID)));
                columnString = CellReference.ConvertNumToColString(columnIndex);
                validationValues = accountRepository.GetAll().Where(x => x.ActiveFlag == '1').Select(x => x.UserName).ToArray();
                sheet2.InsertColumn(1, columnIndex, validationValues);
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateFormulaListConstraint($"Sheet2!${columnString}1:${columnString}{validationValues.Length + 1}")
                    )
                );

                // 币种
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.CurrencyID)));
                validationValues = currencyRepository.GetAll().Select(x => x.Code).ToArray();
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateExplicitListConstraint(validationValues)
                    )
                );

                // 产品分级
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.OdooCategory)));
                columnString = CellReference.ConvertNumToColString(columnIndex);
                var odooCategories = await odooCategoriesTask;
                validationValues = odooCategories.Select(x => x["complete_name"].ToString()).ToArray();
                sheet2.InsertColumn(1, columnIndex, validationValues);
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateFormulaListConstraint($"Sheet2!${columnString}1:${columnString}{validationValues.Length + 1}")
                    )
                );

                // 包装材料
                columnIndex = headers.IndexOf(productType.GetDisplayAttributeValue(nameof(productDto.Materials)));
                columnString = CellReference.ConvertNumToColString(columnIndex);
                validationValues = materialRepository.GetAll().Select(x => x.Name).ToArray();
                sheet2.InsertColumn(1, columnIndex, validationValues);
                ((HSSFSheet)sheet1).AddValidationData(
                    new HSSFDataValidation(
                        new CellRangeAddressList(1, int.MaxValue, columnIndex, columnIndex),
                        DVConstraint.CreateFormulaListConstraint($"Sheet2!${columnString}1:${columnString}{validationValues.Length + 1}")
                    )
                );

                MemoryStream memoryStream = new MemoryStream();
                hssfWorkbook.Write(memoryStream);
                var bytes = memoryStream.ToArray();
                FileContentResult result = new FileContentResult(bytes, "application/vnd.ms-excel");
                result.FileDownloadName = $"product_template.xls";

                return result;
            }
            catch (Exception ex)
            {
                Dictionary<string, string> clogTags = new Dictionary<string, string>
                {
                    ["type"] = "下载Excel模板"
                };
                CLSLogger.Error("商品_下载模板失败", ex, clogTags);

                return null;
            }
           
        }
    }
}
