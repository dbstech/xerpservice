using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;

namespace MagicLamp.PMS
{
    public class BrandCategoryAppService : PMSCrudAppServiceBase<BrandCategoryEntity, BrandCategoryDTO, BrandCategoryFilterDTO, int>
    {
        private IRepository<BrandCategoryEntity, int> _brandCategoryRepository;
        public BrandCategoryAppService(
           IRepository<BrandCategoryEntity, int> brandCategoryRepository
        ) : base(brandCategoryRepository)
        {
            _brandCategoryRepository = brandCategoryRepository;
        }
    }
}
