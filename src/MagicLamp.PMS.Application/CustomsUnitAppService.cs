﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;



namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class CustomsUnitAppService : PMSCrudAppServiceBase<CustomsUnitEntity, CustomsUnitDTO, CustomsUnitDTO, int>
    {
        private IRepository<CustomsUnitEntity, int> CustomsUnitRepository;
        public CustomsUnitAppService(IRepository<CustomsUnitEntity, int> _customsunitRepository) : base(_customsunitRepository)
        {
            CustomsUnitRepository = _customsunitRepository;
        }

    }
}
