using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
namespace MagicLamp.PMS.ProductCategories
{
    public class FreightOrderSplitCategoryRuleAppService : AsyncCrudAppService<FreightOrderSplitCategoryRule, FreightOrderSplitCategoryRuleDto, int, PagedRequestDto>
    {
        private readonly IRepository<FreightOrderSplitCategoryRule, int> _freightOrderSplitCategoryRuleRepository;
        public FreightOrderSplitCategoryRuleAppService(
            IRepository<FreightOrderSplitCategoryRule, int> repository
        ) : base(repository)
        {
            _freightOrderSplitCategoryRuleRepository = repository;
        }

        protected override IQueryable<FreightOrderSplitCategoryRule> CreateFilteredQuery(PagedRequestDto input)
        {
            return Repository.GetAllIncluding(x => x.ProductCategory)
                            .WhereIf(input.ForMixed.HasValue, m => m.ForMixed == input.ForMixed)
                            .WhereIf(input.NoMixed.HasValue, m => m.NoMixed == input.NoMixed)
                            .WhereIf(input.NoWeightLimited.HasValue, m => m.NoWeightLimited == input.NoWeightLimited)
                            .WhereIf(!input.CNName.IsNullOrWhiteSpace(), m => m.ProductCategory.CNName.Contains(input.CNName));
        }

        public override async Task<PagedResultDto<FreightOrderSplitCategoryRuleDto>> GetAll(PagedRequestDto input)
        {
            var result = await base.GetAll(input);
            var freightOrderSplitCategoryRules = _freightOrderSplitCategoryRuleRepository.GetAllIncluding(x => x.ProductCategory).ToList();
            foreach (var item in result.Items)
            {
                item.SameCategories = freightOrderSplitCategoryRules.Where(x => item.SameCategoryIds.Contains(x.Id)).Select(x => x.GetCNName()).ToList();
            }
            return result;
        }
    }
}