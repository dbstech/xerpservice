using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace MagicLamp.PMS.ProductCategories
{
    public class FreightOrderSplitCategoryRuleDto : EntityDto<int>
    {
        public int CategoryRuleID { get; set; }
        public int RuleID { get; set; }
        public int ProductCategoryID { get; set; }
        public int MaxQty { get; set; }
        public bool ForMixed { get; set; }
        public DateTime? CreateTime { get; set; }
        public bool NoWeightLimited { get; set; }
        public bool NoMixed { get; set; }
        public decimal MaxPrice { get; set; }
        public List<int> SameCategoryIds { get; set; }
        public List<string> SameCategories { get; set; }
        public string CNName { get; set; }
    }
}