using Abp.Application.Services.Dto;

namespace MagicLamp.PMS.ProductCategories
{
    public class PagedRequestDto : PagedAndSortedResultRequestDto
    {
        public string CNName { get; set; }
        public bool? ForMixed { get; set; }
        public bool? NoMixed { get; set; }
        public bool? NoWeightLimited { get; set; }
    }
}