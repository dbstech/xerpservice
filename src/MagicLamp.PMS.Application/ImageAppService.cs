﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.DTOs.Enums;
using Abp.UI;
using MagicLamp.PMS.Infrastructure.Extensions;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class ImageAppService : PMSCrudAppServiceBase<ImageEntity, ImageDTO, ImageDTO, long>
    {

        private IRepository<ImageEntity, long> imageRepository;
        public ImageAppService(IRepository<ImageEntity, long> _imageRepository) : base(_imageRepository)
        {
            imageRepository = _imageRepository;
        }


        [DisableRequestSizeLimit]
        public virtual ImageDTO Upload(string type, IFormFile file)
        {
            string filePath = Ultities.CreateDirectory("App_Data/TempImages");
            string fileName = Guid.NewGuid().ToString("N");
            string fullPath = Path.Combine(filePath, fileName);
            string extension = Path.GetExtension(file.FileName);
            string originalFileName = Path.GetFileNameWithoutExtension(file.FileName);

            ImageTypeEnum imageType = ImageTypeEnum.Unknow;
            if (!Enum.TryParse<ImageTypeEnum>(type, out imageType))
            {
                throw new UserFriendlyException("Invalid type parameter, pls check.");
            }

            string key = string.Empty;

            switch (imageType)
            {
                case ImageTypeEnum.Product:
                case ImageTypeEnum.Brand:
                    key = $"{PMSConsts.ProductImgKeyPrefix}{originalFileName.Trim().UrlEncode()}-{fileName}{extension}";
                    break;
                default:
                    key = $"{PMSConsts.ProductImgKeyPrefix}{imageType}-{fileName}{extension}";
                    break;
            }

            //save image
            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }

            //upload to aliyun oss
            bool uploadResult = AliyunOSSProxy.Instance.UploadImage(fullPath, key);

            if (!uploadResult)
            {
                throw new UserFriendlyException("上传失败，请稍后重试");
            }

            //delete temp file
            System.IO.File.Delete(fullPath);

            //save to db
            ImageEntity image = new ImageEntity
            {
                CreationTime = DateTime.UtcNow,
                ImageType = imageType.ToString(),
                OriginalPath = key
            };
            long imageId = imageRepository.InsertAndGetId(image);
            var imageDTO = image.MapTo<ImageDTO>();
            imageDTO.ImageId = imageId;

            return imageDTO;
        }


    }


}

