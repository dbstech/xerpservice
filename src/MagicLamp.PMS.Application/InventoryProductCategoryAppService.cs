﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class InventoryProductCategoryAppService : PMSCrudAppServiceBase<InventoryProductCategoryEntity, InventoryProductCategoryDTO, InventoryProductCategoryDTO, int>
    {

        private IRepository<InventoryProductCategoryEntity, int> inventoryProductCategoryRepository;
        public InventoryProductCategoryAppService(IRepository<InventoryProductCategoryEntity, int> _inventoryProductCategoryRepository) : base(_inventoryProductCategoryRepository)
        {
            inventoryProductCategoryRepository = _inventoryProductCategoryRepository;
        }

        // TODO: restrict duplicate names for categories during create and update
        // throw exception if update parentID to itself or children
        // recursive function to get all descendants
    }


}

