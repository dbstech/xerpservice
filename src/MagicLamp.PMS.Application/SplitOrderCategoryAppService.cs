﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class SplitOrderCategoryAppService : PMSCrudAppServiceBase<SplitOrderProductCategoryEntity, SplitOrderProductCategoryDTO, SplitOrderProductCategoryDTO, int>
    {

        private IRepository<SplitOrderProductCategoryEntity, int> splitOrderCategoryRepository;
        public SplitOrderCategoryAppService(IRepository<SplitOrderProductCategoryEntity, int> _splitOrderCategoryRepository) : base(_splitOrderCategoryRepository)
        {
            splitOrderCategoryRepository = _splitOrderCategoryRepository;
        }

    }


}

