﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.DTO.Enums;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using MagicLamp.XERP.BackgroundJob;
using Abp.UI;
using System.Linq.Dynamic.Core;
using MagicLamp.PMS.Proxy.BaXi.Models;
using System.Net;
using MagicLamp.PMS.BaXi;
using MagicLamp.PMS.DTOs.Configs;
using System.Security.Cryptography;
using Abp.Web.Models;
using MagicLamp.PMS.DTOs.MqDTO;
using MagicLamp.PMS.DTOs.ThirdPart;
using Microsoft.AspNetCore.Authorization;
using MagicLamp.PMS.Domain;
using Microsoft.AspNetCore.Http;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;
using System.Web;
using Newtonsoft.Json;
using TimeZoneConverter;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.EntityFrameworkCore;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class IDCardAppService : PMSCrudAppServiceBase<IDCardEntity, IDCardDTO, SearchIDCardsInputDTO, string>
    {
        private BaXiConfig _config = Ultities.GetConfig<BaXiConfig>();
        private IRepository<IDCardEntity, string> idcardRepository;
        private IRepository<OrderPackEntity, int> orderpackRepository;
        private IRepository<DownloadRecordsEntity, Guid> downloadRecordsRepository;
        private IRepository<OrderIDCardEntity, string> orderIDCardRepository;
        private IRepository<UserLoginInfoEntity,int> userloginfoRepository;


        private IBackgroundJobManager backgroundJobManager;
        private readonly IAccountManager accountManager;

        private readonly IHttpContextAccessor _httpContextAccessor;

        private TimeZoneInfo timeZoneNZ = TZConvert.GetTimeZoneInfo("Pacific/Auckland");
        private TimeZoneInfo timeZoneCN = TZConvert.GetTimeZoneInfo("Asia/Shanghai");

        public IDCardAppService(IRepository<IDCardEntity, string> _idcardRepository,
            IRepository<OrderPackEntity, int> _orderpackRepository,
            IRepository<DownloadRecordsEntity, Guid> _downloadRecordsRepository,
            IRepository<OrderIDCardEntity, string> _orderIDCardRepository,
            IRepository<UserLoginInfoEntity, int> _userloginfoRepository,

            IBackgroundJobManager _backgroundJobManager,
             IAccountManager _accountManager,
             IHttpContextAccessor httpContextAccessor) : base(_idcardRepository)
        {
            idcardRepository = _idcardRepository;
            orderpackRepository = _orderpackRepository;
            downloadRecordsRepository = _downloadRecordsRepository;
            orderIDCardRepository = _orderIDCardRepository;

            userloginfoRepository = _userloginfoRepository;

            accountManager = _accountManager;
            backgroundJobManager = _backgroundJobManager;
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 查询身份证信息
        /// </summary>
        /// <param name="inputDtoFilter"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public override  PagedQueryOutputBaseDTO<IDCardDTO> GetList(PagedQueryInputBaseDTO<SearchIDCardsInputDTO> inputDto)
        {

            AddUserLogIntoUserLoginInfo();

            PagedQueryOutputBaseDTO<IDCardDTO> output = new PagedQueryOutputBaseDTO<IDCardDTO>();
            try
            {

                

                var query = orderIDCardRepository.GetAll().Where(x => !string.IsNullOrEmpty(x.IDCardNo));
                if (inputDto.Filter == null)
                {
                    inputDto.Filter = new SearchIDCardsInputDTO();
                }
                if (!string.IsNullOrWhiteSpace(inputDto.Filter.Name))
                {
                    var names = inputDto.Filter.Name.SplitByCommonSymbol();
                    query = query.Where(x => names.Contains(x.RecevierName));
                }
                if (!string.IsNullOrWhiteSpace(inputDto.Filter.CourierID))
                {
                    var courierIds = inputDto.Filter.CourierID.SplitByCommonSymbol();
                    query = query.Where(x => courierIds.Contains(x.CarrierID));
                }
                if (inputDto.Filter.ExportFlagID > 0)
                {
                    query = query.Where(x => x.ExportFlag == inputDto.Filter.ExportFlagID);
                }
                if (!string.IsNullOrWhiteSpace(inputDto.Filter.OrderID))
                {
                    var orderIds = inputDto.Filter.OrderID.SplitByCommonSymbol();
                    query = query.Where(x => orderIds.Contains(x.OrderID));
                }
                if (!string.IsNullOrWhiteSpace(inputDto.Filter.IDCard))
                {
                    query = query.Where(x => x.IDCardNo == inputDto.Filter.IDCard);
                }
                if (inputDto.Filter.Verification.HasValue)
                {
                    query = query.Where(x => x.Verification == inputDto.Filter.Verification);
                }

                if (inputDto.Filter.UpdateBeginTime > DateTime.MinValue)
                {
                    query = query.Where(x => x.IDCardUpdateTime >= inputDto.Filter.UpdateBeginTime);
                }
                if (inputDto.Filter.EndBeginTime > DateTime.MinValue)
                {
                    query = query.Where(x => x.IDCardUpdateTime <= inputDto.Filter.EndBeginTime);
                }
                if (!string.IsNullOrEmpty(inputDto.Filter.FreightID))
                {
                    query = query.Where(x => x.FreightID == inputDto.Filter.FreightID);
                }
                if (inputDto.Filter.OrderBeginTime > DateTime.MinValue)
                {
                    query = query.Where(x => x.CreationTime >= inputDto.Filter.OrderBeginTime);
                }
                if (inputDto.Filter.OrderEndBeginTime > DateTime.MinValue)
                {
                    query = query.Where(x => x.CreationTime <= inputDto.Filter.OrderEndBeginTime);
                }

                if (!inputDto.Sort.IsNullOrEmpty() && !inputDto.SortType.IsNullOrEmpty())
                {

                    query = query.OrderBy(string.Format("{0} {1}", inputDto.Sort.ChangeIfEqual("id", "CreationTime"), inputDto.SortType));
                }
                else
                {
                    query = query.OrderByDescending(x => x.CreationTime);
                }

                output.Meta.TotalCount = query.Count();
                output.Meta.PageIndex = inputDto.PageIndex;
                var entities = query.PagedQuery(inputDto.PageIndex, inputDto.PageSize).ToList();
                output.Data = entities.MapTo<List<IDCardDTO>>();

                output.Data.ForEach(x =>
                {
                    x.Verification = x.Verification.HasValue ? x.Verification : 2;
                    if (!x.URLBack.IsNullOrEmpty() && !x.URLFont.IsNullOrEmpty())
                    {
                        x.URLBackImage = AliyunOSSProxy.Instance.GenerateIDCardPresignedUrl(x.URLBack, 100, 100);
                        x.URLFontImage = AliyunOSSProxy.Instance.GenerateIDCardPresignedUrl(x.URLFont, 100, 100);
                        x.URLBack = x.URLBackImage;
                        x.URLFont = x.URLFontImage;
                    }
                });


                return output;

            }
            catch (Exception ex)
            {
                Logger.Error("发生异常", ex);
                return output;
            }

        }


        public void AddUserLogIntoUserLoginInfo()
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>
            {
                ["type"] = "GetCountryFromIP"
            };

            string userIpAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
            if (_httpContextAccessor.HttpContext.Request.Headers.TryGetValue("X-Forwarded-For", out var values))
            {
                userIpAddress = values.FirstOrDefault();
                string[] ipList = userIpAddress.Split(',');
                userIpAddress = ipList[0].Trim();
            }

            var context = _httpContextAccessor.HttpContext;
        
            string userAgent = context.Request.Headers["User-Agent"];
            //string currentTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            DateTime CurrentTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ); //NZTimeZone

            string city = "", region = "", country = "", org = "", loc = "", timezone = "";
            //IPAddress Exists 

            var IpaddressExists = userloginfoRepository.GetAll().Where(x => x.IpAddress == userIpAddress).FirstOrDefault();
            if (IpaddressExists == null)
            {
                var IPInfo = MagicLamp.PMS.Proxy.UserLoginLogProxy.GetCountryFromIP(userIpAddress, "b062936e9f3063");
                if (IPInfo != null)
                {
                    city = IPInfo.city;
                    region = IPInfo.region;
                    country = IPInfo.country;
                    org = IPInfo.org;
                    loc = IPInfo.loc;
                    timezone = IPInfo.timezone;
                }
            }
            else
            {
                city = IpaddressExists.city;
                region = IpaddressExists.region;
                country = IpaddressExists.country;
                org = IpaddressExists.org;
                loc = IpaddressExists.loc;
                timezone = IpaddressExists.timezone;
            }


            //CLSLogger.Info("GetCountryFromIPSuccess2", City + " " + Region + " " + Country + " " + Org + " " + Loc + " " + Timezone, clogTags);
            // 添加日志
            MagicLamp.PMS.Proxy.UserLoginLogProxy.UserLoginInfo userlog = new UserLoginLogProxy.UserLoginInfo();
            userlog.UserName = accountManager.GetUserName();
            userlog.IpAddress = userIpAddress;
            userlog.UserAgent = userAgent;
            userlog.LoginResult = string.Format("{0} viewed XERP IDCard at {1} ", accountManager.GetUserName(), CurrentTime.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            userlog.City = city;
            userlog.Region = region;
            userlog.Country = country;
            userlog.Org = org;
            userlog.Loc = loc;
            userlog.Timezone = timezone;
            MagicLamp.PMS.Proxy.UserLoginLogProxy.AddUserLoginInfo(userlog);

        }
        /// <summary>
        /// 创建身份证下载zip包任务
        /// </summary>
        /// <param name="inputDto"></param>
        /// <returns></returns>
        public GenerateIDCardZipOutputDTO GenerateIDCardZip(GenerateIDCardZipInputDTO inputDto)
        {
            GenerateIDCardZipOutputDTO response = new GenerateIDCardZipOutputDTO();

            //step1: Create download record
            Guid taskid = Guid.NewGuid();
            DownloadRecordsEntity downloadRecordsEntity = new DownloadRecordsEntity
            {
                CreationTime = DateTime.Now,
                TaskID = taskid,
                Type = PMSConsts.IDCardDownloadRecordType,
                Status = DownloadTaskStatusEnum.PROCESSING.ToString()
            };
            downloadRecordsRepository.Insert(downloadRecordsEntity);

            response.TaskID = taskid.ToString();

            //step2: Create async idcard generated task
            inputDto.TaskID = taskid;
            backgroundJobManager.Enqueue<IDCardZipGenerateJob, GenerateIDCardZipInputDTO>(inputDto);


            return response;
        }


        /// <summary>
        ///下载身份证压缩包
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DownloadIDCardZip(DownloadIDCardZipInputDTO inputDto)
        {
            if (inputDto.TaskID == Guid.Empty)
            {
                throw new UserFriendlyException("未找到对应下载任务");
            }

            try
            {

                var record = downloadRecordsRepository.FirstOrDefault(x => x.TaskID == inputDto.TaskID);
                if (record == null || string.IsNullOrEmpty(record.FilePath))
                {
                    throw new UserFriendlyException("未找到对应下载任务");
                }

                var result = new PhysicalFileResult(record.FilePath, Ultities.GetContentTypeByFilePath(record.FilePath));

                result.FileDownloadName = Path.GetFileName(record.FilePath);
                return result;

            }
            catch (Exception ex)
            {
                CLSLogger.Error("DownloadIDCardZip发生异常", ex);
                throw new UserFriendlyException("DownloadIDCardZip发生异常");
            }

        }

        [HttpPost]
        public async Task<FTDAPIOutputDTO> PushIDCard2FTD(IDCardUploadMqDto dto)
        {
            AliyunOSSProxy aliyunOSSProxy = AliyunOSSProxy.Instance;

            var frontImage = ImageToByteArray(aliyunOSSProxy.DownloadImage(dto.PhotoFront));
            var rearImage = ImageToByteArray(aliyunOSSProxy.DownloadImage(dto.PhotoRear));

            FTDIDCardInfoDTO FTDFrontDto = new FTDIDCardInfoDTO
            {
                name = dto.ReceiverName,
                id = dto.IDCardNo,
                card_full = null,
                card_front = frontImage,
                card_back = rearImage,
                card_frontname = dto.PhotoFront,
                card_backtname = dto.PhotoRear
            };

            FTDAPIOutputDTO result = await FTDExpressProxy.UploadIDCard(FTDFrontDto);

            return result;
        }

        [HttpPost]
        public void UploaFTDExpress(string carrcarrierid)
        {
            var query = orderIDCardRepository.GetAll().Where(x => !string.IsNullOrEmpty(x.IDCardNo) && x.CarrierID == carrcarrierid).FirstOrDefault();

            FreightCodeEnum freightCode = new FreightCodeEnum();

            if (query.FreightID == "FTDEXPRESSML")
            {
                freightCode = FreightCodeEnum.FTDEXPRESSML;
            }

            if (query.FreightID == "FTD")
            {
                freightCode = FreightCodeEnum.FTD;
            }

            if (query.FreightID == "FTDSF")
            {
                freightCode = FreightCodeEnum.FTDSF;
            }

            if (query.FreightID == "HYFTDMILK")
            {
                freightCode = FreightCodeEnum.HYFTDMILK;
            }

            var dto = new IDCardUploadMqDto
            {
                //TrackNos = new List<string>() { carrcarrierid },
                ReceiverName = query.RecevierName,
                IDCardNo = query.IDCardNo,
                //OrderPackNo = IdcardObj.FirstOrDefault().OrderPackNo,
                PhotoFront = query.URLFont,
                PhotoRear = query.URLBack,
                FreightCode = freightCode,
                //ReceiverPhone = IdcardObj.FirstOrDefault().ReceiverPhone
            };
            IBackgroundJobManager backgroundJobManager = IocManager.Instance.Resolve<IBackgroundJobManager>();
            backgroundJobManager.Enqueue<IDCardUploadJob, IDCardUploadMqDto>(dto);
        }

        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// 根据运单号返回用户的身份证信息
        /// </summary>
        /// <param BaxiIDCardInput="input"></param>
        /// <returns></returns>

        [HttpPost]
        [DontWrapResult]
        public BaxiIDCardResponse GetBaxiIDCard(BaxiIDCardInput input)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>();
            clogTags["type"] = "GetBaxiIDCard";
            clogTags["wayBillCode"] = input.wayBillCode;

            string[] FreightIDList = { "AUOD", "EWE" };

            BaxiIDCardResponse output = new BaxiIDCardResponse();

            List<BaxiIDCardResponse.Data> BaxiIDCardData = new List<BaxiIDCardResponse.Data>();
            BaxiIDCardData.Clear();

            if (string.IsNullOrWhiteSpace(input.wayBillCode))
            {
                output.status = "0";
                output.msg = "物流单号不能为空";
                output.data = BaxiIDCardData;

                CLSLogger.Info("wayBillCode不能为空", $"wayBillCode: {input.wayBillCode}", clogTags);
                return output;
            }
          
            try
            {
                var BaxiIDCard = orderIDCardRepository.GetAll().Where(x =>
                             !string.IsNullOrWhiteSpace(x.CarrierID)  &&  (input.wayBillCode.Contains(x.CarrierID) || x.CarrierID.Contains(input.wayBillCode) )
                            && FreightIDList.Contains(x.FreightID) ).FirstOrDefault();

                if (BaxiIDCard == null)
                {
                    output.status = "0";
                    output.msg = string.Format("八喜仓,此物流单号:{0}不存在,调用失败!",input.wayBillCode);
                    output.data = BaxiIDCardData;

                    CLSLogger.Info("无此物流单号", $"wayBillCode: {input.wayBillCode}", clogTags);
                    return output;
                }

                string _sign = Md5(input.wayBillCode + _config.SecretKey);

                if (_sign != input.Sign.ToUpper()) {
                    output.status = "0";
                    output.msg = "SecretKey 不正确 或者 wayBillCode 不存在 , 调用失败!";
                    output.data = BaxiIDCardData;

                    CLSLogger.Info("SecretKey 不正确", $"Sign: {input.Sign},wayBillCode:{input.wayBillCode} ", clogTags);
                    return output;
                }

                BaxiIDCardResponse.Data entity = new BaxiIDCardResponse.Data();
                entity.Image1 = "data:image/png;base64,";
                entity.Image2 = "data:image/png;base64,";
                if (!BaxiIDCard.URLBack.IsNullOrEmpty() && !BaxiIDCard.URLFont.IsNullOrEmpty())
                {
                    entity.Image1 = entity.Image1 + ConvertImageURLToBase64(AliyunOSSProxy.Instance.GenerateIDCardPresignedUrl2(BaxiIDCard.URLFont));
                    entity.Image2 = entity.Image2 + ConvertImageURLToBase64(AliyunOSSProxy.Instance.GenerateIDCardPresignedUrl2(BaxiIDCard.URLBack));
                }
                entity.name = BaxiIDCard.RecevierName;
                entity.mobile = BaxiIDCard.ReceiverPhone;
                entity.idCardNo = BaxiIDCard.IDCardNo;
                entity.validityPeriod = string.IsNullOrWhiteSpace(BaxiIDCard.ExpiredDate.ToString()) ? "" : DateTime.Parse(BaxiIDCard.ExpiredDate.ToString()).ToString("yyyy-MM-dd");
                entity.wayBillCode = input.wayBillCode;
                BaxiIDCardData.Add(entity);

                output.status = "1";
                output.msg = "查询成功!";
                output.data = BaxiIDCardData;

            }
            catch (Exception ex)
            {
                output.status = "0";
                output.msg = string.Format("异常:{0}", ex);
                output.data = BaxiIDCardData;

                CLSLogger.Error("异常信息:", ex, clogTags);
            }

            return output;
        }


        public String ConvertImageURLToBase64(String url)
        {
            StringBuilder _sb = new StringBuilder();

            Byte[] _byte = this.GetImage(url);

            _sb.Append(Convert.ToBase64String(_byte, 0, _byte.Length));

            return _sb.ToString();
        }

        private byte[] GetImage(string url)
        {
            Stream stream = null;
            byte[] buf;

            try
            {
                WebProxy myProxy = new WebProxy();
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                stream = response.GetResponseStream();

                using (BinaryReader br = new BinaryReader(stream))
                {
                    int len = (int)(response.ContentLength);
                    buf = br.ReadBytes(len);
                    br.Close();
                }

                stream.Close();
                response.Close();
            }
            catch (Exception exp)
            {
                buf = null;
            }

            return (buf);
        }


        private string Md5(string message)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] input = Encoding.UTF8.GetBytes(message);
            byte[] buff = md5.ComputeHash(input);
            return ByteToHex(buff);
        }

        private string ByteToHex(byte[] bytes)
        {
            StringBuilder sign = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                String hex = (bytes[i] & 0xFF).ToString("X");
                if (hex.Length == 1)
                {
                    sign.Append("0");
                }
                sign.Append(hex.ToUpper());
            }
            return sign.ToString();
        }
    }


}

