﻿using Abp.AspNetCore.Configuration;
using Abp.AspNetCore.Mvc.ExceptionHandling;
using Abp.Authorization;
using Abp.Dependency;
using Abp.Domain.Entities;
using Abp.Events.Bus.Exceptions;
using Abp.Events.Bus.Handlers;
using Abp.Runtime.Validation;
using Abp.Web.Models;
using MagicLamp.PMS.Proxy;
using Microsoft.AspNetCore.Mvc.Filters;
using Sentry;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using MagicLamp.PMS.Infrastructure;

namespace MagicLamp.PMS.EventHandlers
{
    public class CustomizeExceptionFilter : AbpExceptionFilter
    {

        public CustomizeExceptionFilter(IErrorInfoBuilder errorInfoBuilder, IAbpAspNetCoreConfiguration configuration) : base(errorInfoBuilder, configuration) { }

        protected override int GetStatusCode(ExceptionContext context, bool wrapOnError)
        {
            if (context.Exception is RequestValidationException)
            {
                return (int)HttpStatusCode.BadRequest;
            }

            return base.GetStatusCode(context, wrapOnError);
        }

    }
}