using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Npoi.Mapper.Attributes;

namespace MagicLamp.PMS.Brands.Dto
{
    public class ExportBrandDto : EntityDto<int>
    {
        [Column("短码")]
        public string ShortCode { get; set; }
        [Column("激活")]
        public bool? ActiveFlag { get; set; }
        [Column("中文名称")]
        public string ChineseName { get; set; }
        [Column("英文名称")]
        public string EnglishName { get; set; }
        [Column("分级")]
        public string BrandCategoryName { get; set; }
        [Column("创建日期")]
        public DateTime CreateTime { get; set; }
        public long OdooBrandId { get; set; }
    }
}