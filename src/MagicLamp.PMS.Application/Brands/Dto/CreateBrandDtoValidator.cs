using FluentValidation;
namespace MagicLamp.PMS.Brands.Dto
{
    public class CreateBrandDtoValidator : AbstractValidator<CreateBrandDto>
    {
        public CreateBrandDtoValidator()
        {
            RuleFor(brand => brand.ShortCode).NotEmpty().WithMessage("\"{PropertyName}\"不能为空");
            RuleFor(brand => brand.EnglishName).NotEmpty().WithMessage("\"{PropertyName}\"不能为空");
            RuleFor(brand => brand.ChineseName).NotEmpty().WithMessage("\"{PropertyName}\"不能为空");
        }
    }
}

