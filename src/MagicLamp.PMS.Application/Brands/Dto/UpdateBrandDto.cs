using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace MagicLamp.PMS.Brands.Dto
{
    public class UpdateBrandDto: EntityDto<int>
    {
        [Display(Name = "短码")]
        public string ShortCode { get; set; }
        [Display(Name = "激活")]
        public bool? ActiveFlag { get; set; }
        [Display(Name = "中文名称")]
        public string ChineseName { get; set; }
        [Display(Name = "英文名称")]
        public string EnglishName { get; set; }
        [Display(Name = "分级")]
        public string BrandCategory { get; set; }
    }
}