﻿using Abp.Application.Services;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.YiWuBondedWarehouse;
using MagicLamp.XERP.BackgroundJob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicLamp.PMS
{
    /// <summary>
    /// 义乌保税仓
    /// </summary>
    public class BondedWarehouseYiWuAppService : ApplicationService
    {
        private const string ClogTagType = "BondedWarehouseYiWu";

        private IBackgroundJobManager _backgroundJobManager;
        private IRepository<OrderPackTradeEntity, int> _orderPackTradeRepository;

        public BondedWarehouseYiWuAppService(IBackgroundJobManager backgroundJobManager,
            IRepository<OrderPackTradeEntity, int> orderPackTradeRepository)
        {
            _backgroundJobManager = backgroundJobManager;
            _orderPackTradeRepository = orderPackTradeRepository;
        }

        public void CreateOrder(CreateSupplierOrderInput inputDTO)
        {
            _backgroundJobManager.Enqueue<YiWuCreateOrderJob, CreateSupplierOrderInput>(inputDTO, BackgroundJobPriority.Normal);
        }

        public PushOrdersOutputDTO RepushListOfOrder(List<string> listOrderID)
        {
            List<OrderPackTradeEntity> listOrderPackTrade = _orderPackTradeRepository.GetAll().Where(x => listOrderID.Contains(x.OrderID)).ToList();

            List<string> listOrderIDFound = listOrderPackTrade.Select(x => x.OrderID).ToList();
            List<string> listOrderIDNotFound = listOrderID.Except(listOrderIDFound).ToList();

            foreach (var item in listOrderPackTrade)
            {
                try
                {
                    var input = item.OriginalInfo.ConvertFromJsonString<CreateSupplierOrderInput>();
                    _backgroundJobManager.Enqueue<YiWuCreateOrderJob, CreateSupplierOrderInput>(input, BackgroundJobPriority.Normal);
                }
                catch (Exception ex)
                {
                    Dictionary<string, string> clogTags = new Dictionary<string, string>
                    {
                        ["type"] = ClogTagType,
                        ["orderid"] = item.OrderID
                    };

                    CLSLogger.Error("重推义乌保税仓失败", ex, clogTags);
                }
            }

            PushOrdersOutputDTO output = new PushOrdersOutputDTO
            {
                Success = true,
                PushedOrdersCount = listOrderPackTrade.Count(),
                IdsNotFound = listOrderIDNotFound
            };

            return output;
        }
    }
}
