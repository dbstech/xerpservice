﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using MagicLamp.Flux.API.Models;
using MagicLamp.PMS.Proxy;
using Abp.UI;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.DTOs.Enums;
using Abp.BackgroundJobs;
using MagicLamp.XERP.BackgroundJob;
using MagicLamp.PMS.Infrastructure;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.DTOs.BondedWarehouse;
using Abp.Runtime.Validation;
using Microsoft.AspNetCore.Http;
using System.IO;
using Abp.AutoMapper;
using System.Text.RegularExpressions;
using MagicLamp.PMS.DTO;
using Newtonsoft.Json.Linq;
using MagicLamp.PMS.DTO.BondedWarehouse;

namespace MagicLamafterDto.PMS
{
    /// <summary>
    /// 重庆保税仓
    /// </summary>
    public class BondedWarehouseAppService : ApplicationService
    {

        const string ClogTagType = "bondedwarehouse";

        private IRepository<CQBondedProductEntity, string> gmBondedProductRepository;
        private IRepository<OrderPackEntity, int> orderPackRepository;
        private IRepository<ProductEntity, string> productRepository;
        private IRepository<OrderLogEntity, int> orderLogRepository;
        private IRepository<OrderPackTradeEntity, int> orderPackTradeRepository;
        private IRepository<EcinxOrdersPackTradeEntity, int> EcinxorderPackTradeRepository;

        private IBackgroundJobManager backgroundJobManager;

        public BondedWarehouseAppService(IRepository<CQBondedProductEntity, string> _gmBondedProductRepository,
            IRepository<OrderPackEntity, int> _orderPackRepository,
             IRepository<ProductEntity, string> _productRepository,
             IRepository<OrderLogEntity, int> _orderLogRepository,
             IRepository<OrderPackTradeEntity, int> _orderPackTradeRepository,
             IRepository<EcinxOrdersPackTradeEntity, int> _exincorderPackTradeRepository,

            IBackgroundJobManager _backgroundJobManager)
        {
            gmBondedProductRepository = _gmBondedProductRepository;
            orderPackRepository = _orderPackRepository;
            productRepository = _productRepository;
            orderLogRepository = _orderLogRepository;
            orderPackTradeRepository = _orderPackTradeRepository;
            EcinxorderPackTradeRepository = _exincorderPackTradeRepository;
            backgroundJobManager = _backgroundJobManager;
        }




        public GMCreateOrderOutputDTO CreateOrder(GMCreateOrderInputDTO input)
        {
            backgroundJobManager.Enqueue<GMCreateOrderJob, GMCreateOrderInputDTO>(input, BackgroundJobPriority.Normal);
            GMCreateOrderOutputDTO output = new GMCreateOrderOutputDTO();
            output.Success = true;
            return output;
        }
        public PushOrdersOutputDTO PushOrders(List<GMGetOrderInfoDTO> input)
        {
            List<string> orderIDs = input.Select(x => x.OrderID).ToList();

            List<OrderPackTradeEntity> orderPackTrades = orderPackTradeRepository.GetAll().Where(x => orderIDs.Contains(x.OrderID)).ToList();
            List<string> orderIDsFound = orderPackTrades.Select(x => x.OrderID).ToList();
            List<string> orderIDSNotFound = orderIDs.Except(orderIDsFound).ToList();

            foreach (OrderPackTradeEntity orderPackTrade in orderPackTrades)
            {
                try
                {
                    GMCreateOrderInputDTO GMInput = orderPackTrade.OriginalInfo.ConvertFromJsonString<GMCreateOrderInputDTO>();
                    backgroundJobManager.Enqueue<GMCreateOrderJob, GMCreateOrderInputDTO>(GMInput, BackgroundJobPriority.Normal);
                }
                catch (Exception ex)
                {
                    Dictionary<string, string> clogTags = new Dictionary<string, string>();
                    clogTags["type"] = ClogTagType;
                    clogTags["orderid"] = orderPackTrade.OrderID;
                    CLSLogger.Error("重推关贸失败", ex, clogTags);
                }
            }

            PushOrdersOutputDTO output = new PushOrdersOutputDTO
            {
                Success = true,
                PushedOrdersCount = orderPackTrades.Count(),
                IdsNotFound = orderIDSNotFound,
            };

            return output;
        }

        public string SyncStatus([FromForm] string data)
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = ClogTagType;

            if (data.IsNullOrEmpty())
            {
                CLSLogger.Info("关贸回执请求报文", "invalid request", tags);
                return "invalid request";
            }

            try
            {
                data = data.ConvertBase64ToString();
            }
            catch (Exception ex)
            {
                CLSLogger.Info("关贸回执请求报文", $"解析base64异常； base64：{data}   异常：{ex.ToString()}", tags);
            }

            CLSLogger.Info("关贸回执请求报文", data, tags);

            GMOrderConfirmDTO gmOrderConfirmDTO = null;
            ORDER_XT2WMS gmOrderDeliveryConfirmDTO = null;
            CEB622Message gmOrderInventoryConfirmDTO = null;

            if (data.TryConvertFromXMLString<GMOrderConfirmDTO>(out gmOrderConfirmDTO))
            {
                OrderConfirm(gmOrderConfirmDTO);
            }
            else if (data.TryConvertFromXMLString<ORDER_XT2WMS>(out gmOrderDeliveryConfirmDTO))
            {
                OrderDeliveryConfirm(gmOrderDeliveryConfirmDTO);
            }
            else if (data.TryConvertFromXMLString<CEB622Message>(out gmOrderInventoryConfirmDTO))
            {
                OrderInventoryConfirm(gmOrderInventoryConfirmDTO);
            }
            else
            {
                return "invalid request, convert failed";
            }

            return "同步成功";
        }


        private void OrderConfirm(GMOrderConfirmDTO gmOrderConfirmDTO)
        {
            //BIZNO is orderPackNo or orderId
            string bizNo = gmOrderConfirmDTO.BIZNO;
            bool isOrderPackNo = Regex.Match(bizNo, "^[0-9]+-[0-9]+-[0-9]+", RegexOptions.IgnoreCase).Success;
            string orderStatus = "状态未获取到";

            //get the status from RETN using switch statement
            switch (gmOrderConfirmDTO.RETN)
            {
                case "1":
                    orderStatus = "入库失败";
                    //send notification to dingding
                    var notificationMessage = $"请注意！{(isOrderPackNo ? "包裹号" : "订单号")}：{bizNo}，入库状态为：{orderStatus}，关贸返回信息：{gmOrderConfirmDTO.MSG}";
                    SendNotificationToDingDingJob ddJob = new SendNotificationToDingDingJob();
                    ddJob.SendBondedWarehouseChongQingAlert(notificationMessage);
                    break;
                case "0":
                    orderStatus = "入库成功";
                    break;
            }
            var orderPacks = orderPackRepository.GetAll().Where(x => isOrderPackNo ? x.orderPackNo == bizNo : x.orderID == bizNo).ToList();
            if (orderPacks == null)
            {
                return;
            }

            foreach (var item in orderPacks)
            {
                //add log to database
                OrderLogEntity orderLog = new OrderLogEntity
                {
                    //get orderid and query orders table to get the oid and opid 
                    OID = item.oID,
                    OPID = item.opID,
                    OptDate = DateTime.UtcNow,
                    Comments = $"{(isOrderPackNo ? "包裹号" : "订单号")}：{bizNo}，入库状态为：{orderStatus}，具体信息：{gmOrderConfirmDTO.MSG}",
                    //OptID
                    OptName = "System"
                };
                orderLogRepository.InsertAsync(orderLog);
            }

            //TODO:待打单


        }

        private void OrderDeliveryConfirm(ORDER_XT2WMS gmOrderDeliveryConfirmDTO)
        {
            //订单号/包裹号
            string originalOrderNo = gmOrderDeliveryConfirmDTO.ORDER_HEAD.ORIGINAL_ORDER_NO;
            bool isOrderPackNo = Regex.Match(originalOrderNo, "^[0-9]+-[0-9]+-[0-9]+", RegexOptions.IgnoreCase).Success;
            //运单号
            string logisticsNo = gmOrderDeliveryConfirmDTO.ORDER_HEAD.LOGISTICS_NO;
            //物流企业代码
            string logisticsCode = gmOrderDeliveryConfirmDTO.ORDER_HEAD.LOGISTICS_CODE;

            StringBuilder log = new StringBuilder();
            log.Append($"订单反抛开始处理, {(isOrderPackNo ? "包裹号" : "订单号")}: {originalOrderNo}, 运单号: {logisticsNo}, 物流代码: {logisticsCode};");

            Dictionary<string, string> clogTags = new Dictionary<string, string>();
            clogTags["type"] = ClogTagType;
            clogTags["orderid"] = originalOrderNo;

            var orderPack = orderPackRepository.GetAll().FirstOrDefault(x => isOrderPackNo ? x.orderPackNo == originalOrderNo : x.orderID == originalOrderNo);

            if (orderPack == null)
            {
                log.Append("订单反抛未找到匹配包裹号");
                return;
            }

            backgroundJobManager.Enqueue<PushOrderToSiLu, ORDER_XT2WMS>(gmOrderDeliveryConfirmDTO);

            //add log
            OrderLogEntity orderLog = new OrderLogEntity
            {
                OID = orderPack.oID,
                OPID = orderPack.opID,
                OptDate = DateTime.UtcNow,
                Comments = $"订单反抛，分单号：{orderPack.opID}，所属{(isOrderPackNo ? "包裹号" : "订单号")}：{originalOrderNo}，运单号：{logisticsNo}，物流企业代码：{logisticsCode}，原状态代码：{orderPack.status}",
                OptName = "System"
            };
            orderLogRepository.InsertAsync(orderLog);

            orderPack.bonded_warehouse_carrier_id = logisticsNo;
            string freightId = string.Empty;
            switch (logisticsCode)
            {
                case "500696082C":
                    freightId = "ShengTong";
                    break;
                case "50122604QT":
                    freightId = "yto";
                    break;
            }
            orderPack.bonded_warehouse_carrier_company = freightId;

            if (orderPack.freightId.IsNullOrEmpty())
            {
                orderPack.carrierId = logisticsNo;
                orderPack.freightId = freightId;
            }

            log.Append($"更新一个包裹: {orderPack.orderPackNo};");
            //item.status = (int)OrderPackStatusEnum.Outbound;
            //item.out_time = DateTime.Now;


            CLSLogger.Info("订单反抛结束", log.ToString(), clogTags);
        }

        private void OrderInventoryConfirm(CEB622Message gmOrderInventoryConfirmDTO)
        {
            //订单号/包裹号为copNo
            string copNo = gmOrderInventoryConfirmDTO.InventoryReturn.CopNo;
            bool isOrderPackNo = Regex.Match(copNo, "^[0-9]+-[0-9]+-[0-9]+", RegexOptions.IgnoreCase).Success;

            string returnStatus = "状态未获取到";

            switch (gmOrderInventoryConfirmDTO.InventoryReturn.ReturnStatus)
            {
                case "0":
                    returnStatus = "跨境入库失败";
                    break;
                case "1":
                    returnStatus = "跨境入库成功";
                    break;
                case "2":
                    returnStatus = "电子口岸申报中";
                    break;
                case "120":
                    returnStatus = "海关入库（逻辑校验通过）";
                    break;
                case "-621001":
                    returnStatus = "总署入库失败";
                    break;
                case "100":
                    returnStatus = "海关退单";
                    var notificationMessage = $"清单同步回执状态为：海关退单，请检查。订单号：{gmOrderInventoryConfirmDTO.InventoryReturn.CopNo}，原因：{gmOrderInventoryConfirmDTO.InventoryReturn.ReturnInfo}";
                    SendNotificationToDingDingJob ddJob = new SendNotificationToDingDingJob();
                    ddJob.SendBondedWarehouseChongQingAlert(notificationMessage);
                    break;
                case "300":
                    returnStatus = "人工审核";
                    break;
                case "800":
                    returnStatus = "放行";
                    break;
                case "399":
                    returnStatus = "海关审结（撤单通过）";
                    break;
                case "K05":
                    returnStatus = "已过卡口";
                    break;

            }

            var orderPacks = orderPackRepository.GetAll().Where(x => isOrderPackNo ? x.orderPackNo == copNo : x.orderID == copNo).ToList();

            if (orderPacks == null)
            {
                return;
            }

            foreach (var item in orderPacks)
            {
                //add log
                OrderLogEntity orderLog = new OrderLogEntity
                {
                    OID = item.oID,
                    OPID = item.opID,
                    OptDate = DateTime.UtcNow,
                    Comments = $"清单同步回执，{(isOrderPackNo ? "包裹号" : "订单号")}：{copNo}，回执状态：{returnStatus}",
                    //OptID = System.Guid.NewGuid(),
                    OptName = "System"
                };
                orderLogRepository.InsertAsync(orderLog);
            }
            //海关状态


        }

        /// <summary>
        /// Get three WeChat messages.
        /// </summary>
        /// <param name="orderid"></param>
        /// <returns></returns>
        [HttpPost]
        public GMOrderInfoDTO GetOrderInfo(GMGetOrderInfoDTO input)
        {
            Dictionary<string, string> clogTags = new Dictionary<string, string>();
            clogTags["type"] = ClogTagType;
            clogTags["orderid"] = input.OrderID;

            var orderPack = orderPackTradeRepository.FirstOrDefault(x => (x.OrderID == input.OrderID || x.OrderPackNo == input.OrderID));

            var EcinxorderPack = EcinxorderPackTradeRepository.FirstOrDefault(x => (x.OrderPackNo == input.OrderID || x.OrderPackNo == input.OrderID) );


            GMOrderInfoDTO result = new GMOrderInfoDTO();

            if (orderPack == null && EcinxorderPack  == null)
            {
                CLSLogger.Info("orderid不存在，返回业务端信息为空", $"orderid: {input.OrderID}", clogTags);

                throw new UserFriendlyException("Can not find order info");
            }
            try
            {

                if (orderPack == null)
                {
                    result.customsPaymentData = EcinxorderPack.CustomsPaymentData.ConvertFromJsonString<object>();
                    
                }else if(EcinxorderPack == null)
                {
                    result.customsPaymentData = orderPack.CustomsPaymentData.ConvertFromJsonString<object>();
                }
                else
                {
                    result.customsPaymentData = "";
                }
            }

            catch (Exception ex)
            {
                CLSLogger.Error("返回业务端信息为空", ex, clogTags);
            }

            return result;
        }

        public bool ImportExcel(IFormFile file)
        {
            try
            {
                if (file.Length <= 0)
                {
                    throw new UserFriendlyException("文件大小异常，请选择正确的文件");
                }

                string filePath = Ultities.CreateDirectory("App_Data/ExcelImport");
                string fileName = $"{Guid.NewGuid()}{file.FileName}";
                string fullPath = Path.Combine(filePath, fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                var mapper = new Npoi.Mapper.Mapper(fullPath);
                var dtos = mapper.Take<CQBondedSKUDTO>().Where(x => x.Value != null && !string.IsNullOrEmpty(x.Value.ERPSKU)).Select(x => x.Value).ToList();

                File.Delete(fullPath);

                if (dtos.IsNullOrEmpty())
                {
                    throw new UserFriendlyException("excel中未找到有效数据，请确保excel模板正确且有数据");
                }

                var duplicateSkus = dtos.GroupBy(x => x.ERPSKU).Where(x => x.Count() > 1).ToList();
                StringBuilder errorMsg = new StringBuilder();
                foreach (var item in duplicateSkus)
                {
                    errorMsg.Append($"{item.Key}，");
                }

                if (errorMsg.Length > 0)
                {
                    errorMsg.Append($" SKU发现重复，请删除重复项");
                    throw new UserFriendlyException(errorMsg.ToString());
                }


                foreach (var item in dtos)
                {
                    var entity = item.MapTo<CQBondedProductEntity>();
                    entity.ERPSKU = entity.ERPSKU.Trim();
                    entity.GMSKU = entity.GMSKU.Trim();
                    entity.CreateTime = DateTime.UtcNow;

                    gmBondedProductRepository.Insert(entity);
                }

                CurrentUnitOfWork.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("导入excel发生异常", ex);
                if (ex is UserFriendlyException)
                {
                    throw ex;
                }
                throw new UserFriendlyException("导入excel发生异常", ex.ToString());
            }
        }

        public RepushOrdersToSiLuOutput RepushOrdersToSiLu(string orderIds)
        {
            char[] delimiters = new[] { ',', ';', ' ' };
            var ids = orderIds.Split(delimiters, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList();
            var errorLog = new StringBuilder();
            RepushOrdersToSiLuOutput output = new RepushOrdersToSiLuOutput
            {
                errors = new List<Error> { }
            };

            foreach (var id in ids)
            {
                var searchResult = CLSLogger.SearchLog(new CLSSearchLogInputDto
                {
                    startTime = DateTime.Now.AddDays(-90).ToString("yyyy-MM-dd HH:mm:ss"),
                    endTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                    queryString = $"WL_RETINFO AND {id}"
                });

                if (searchResult.Results.Count() > 0)
                {
                    var contentString = searchResult.Results.First().Content;
                    var content =  JObject.Parse(contentString);
                    ORDER_XT2WMS gmOrderDeliveryConfirmDTO;
                    if (content["content"].ToString().TryConvertFromXMLString<ORDER_XT2WMS>(out gmOrderDeliveryConfirmDTO))
                    {
                        backgroundJobManager.Enqueue<PushOrderToSiLu, ORDER_XT2WMS>(gmOrderDeliveryConfirmDTO);
                    }
                    else
                    {
                        output.errors.Add(new Error
                        {
                            orderId = id,
                            errorMessage = "Unable to convert from xml string to object"
                        });
                    }
                }
                else
                {
                    output.errors.Add(new Error
                    {
                        orderId = id,
                        errorMessage = "Unable to find logs for order"
                    });
                }
            }

            return output;
        }
    }
}
