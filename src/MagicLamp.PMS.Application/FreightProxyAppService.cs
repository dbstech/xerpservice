﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using MagicLamp.Flux.API.Models;
using MagicLamp.PMS.Proxy;
using Abp.UI;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.DTOs.Enums;
using Abp.BackgroundJobs;
using MagicLamp.XERP.BackgroundJob;
using MagicLamp.PMS.Infrastructure;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using MagicLamp.PMS.EntityFrameworkCore;
using MagicLamp.PMS.Proxy.FreightsAPI;
using Abp.Web.Models;
using Abp.Extensions;

namespace MagicLamafterDto.PMS
{
    public class FreightProxyAppService : ApplicationService
    {

        IRepository<ParcelTrackNoEntity, string> parcelTrackNoRepository;
        IRepository<ProductEntity, string> productRepository;

        public FreightProxyAppService(IRepository<ParcelTrackNoEntity, string> _parcelTrackNoRepository,
            IRepository<ProductEntity, string> _productRepository)
        {
            parcelTrackNoRepository = _parcelTrackNoRepository;
            productRepository = _productRepository;
        }


        [DontWrapResult(WrapOnError = false, LogError = true, WrapOnSuccess = false)]
        public FreightCreateOrderOutputDTO CreateOrder(FreightCreateOrderInputDTO inputDTO)
        {
            var tags = new Dictionary<string, string>();
            tags["type"] = "FreightProxyCreateOrder";            
            FreightCreateOrderOutputDTO result = new FreightCreateOrderOutputDTO();

            if (inputDTO != null)
            {
                tags["orderid"] = inputDTO.OrderID;
                tags["orderpackno"] = inputDTO.ParcelNo;
                string reqJson = inputDTO.ToJsonString();
                CLSLogger.Info("FreightProxyCreateOrder请求报文", reqJson, tags);
            }
            else
            {
                result.Message = "request is null";
                CLSLogger.Info("FreightProxyCreateOrder请求报文", "request is null", tags);  
                return result;
            }


            if (!inputDTO.ReceiverAddress.IsNullOrEmpty())
            {
                inputDTO.ReceiverAddress = inputDTO.ReceiverAddress.Trim().Replace("?", string.Empty).Replace("？", string.Empty)
                    .Replace("。", string.Empty);
            }

            try
            {
                switch (inputDTO.FreightKey)
                {
                    case "ydtsf":
                        ParcelTrackNoEntity parcelTrackNoEntity = parcelTrackNoRepository.GetAll().FirstOrDefault(x => x.IsUsed == 0 && x.FreightId == inputDTO.FreightKey);
                        if (parcelTrackNoEntity == null)
                        {
                            result.Message = "创建订单失败，原因：易达通顺丰已无可用运单号，请及时录入";
                            return result;
                        }
                        inputDTO.FreightTrackNo = parcelTrackNoEntity.TrackNo;
                        YdtFreightProxy ydtFreightProxy = new YdtFreightProxy();
                        result = ydtFreightProxy.CreateOrder(inputDTO);
                        if (result.Success && !result.CourierReciept.IsNullOrEmpty())
                        {
                            parcelTrackNoEntity.IsUsed = 1;
                        }
                        break;
                    //长江大客户
                    case "ChangJiangExpressCustomizeTrackNo":
                    case "ChangJiangExpress":
                        //get barcode
                        List<string> listProductBySKU = new List<string>();
                        foreach (var product in inputDTO.GoodItems)
                        {
                            listProductBySKU.Add(product.SKU);
                        }

                        var products = productRepository.GetAll().Where(x => listProductBySKU.Contains(x.SKU)).ToList();
                        if (products.IsNullOrEmpty())
                        {
                            result.Message = "创建订单失败，原因：产品无对应Barcode，请检查SKU及对应Barcode";
                            return result;
                        }
                        else
                        {
                            for (int i = 0; i < inputDTO.GoodItems.Count; i++)
                            {
                                var barcode = products.FirstOrDefault(x => x.SKU == inputDTO.GoodItems[i].SKU);
                                if (barcode == null || barcode.Barcode1.IsNullOrWhiteSpace())
                                {
                                    result.Message = $"未找到：{inputDTO.GoodItems[i].SKU} 对应的barcode";
                                    return result;
                                }
                                inputDTO.GoodItems[i].Barcode = barcode.Barcode1;
                            }
                        }

                        if (inputDTO.FreightKey == "ChangJiangExpressCustomizeTrackNo")
                        {
                            result = ChangJiangExpressProxy.CreateNewOrder(inputDTO, "neworderext");
                        }
                        else
                        {
                            result = ChangJiangExpressProxy.CreateNewOrder(inputDTO);
                        }

                        break;
                    default:
                        result.Message = "invalid freight key";
                        return result;
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("FreightProxyCreateOrder发生异常", ex, tags);

                result.Message = "FreightProxyCreateOrder发生异常：" + ex.Message;
                return result;
            }
            finally
            {
                CLSLogger.Info("FreightProxyCreateOrder响应报文", result.ToJsonString(), tags);
            }

            return result;

        }


    }
}
