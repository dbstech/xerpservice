﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using RestSharp;

namespace MagicLamp.PMS.Application.Proxy
{
    /// <summary>
    /// 钉钉消息推送助手
    /// </summary>
    public class DDNotification
    {

        /// <summary>
        /// 推送markdown内容格式的提醒
        /// </summary>
        /// <param name="title"></param>
        /// <param name="text"></param>
        /// <param name="hookurl"></param>
        public static void MarkdownNotification(string title, string text, string token = null)
        {
            if (string.IsNullOrEmpty(token))
            {
                token = "8ff65b0fa81c034e263ccb8df38a5a769ca03ee50da6efe4a9e3dfd89ed22284";//erp dev 
            }

            string hookurl = string.Format("/robot/send?access_token={0}", token);

            var msg = new
            {
                msgtype = "markdown",
                isAtAll = false,
                markdown = new
                {
                    title = title,
                    text = text
                }
            };

            RestClient client = new RestClient("https://oapi.dingtalk.com");
            client.Encoding = System.Text.Encoding.UTF8;
            RestRequest request = new RestRequest(hookurl);
            request.AddJsonBody(msg);
            var response = client.Post(request);
            string resStr = response.Content;

        }

    }
}