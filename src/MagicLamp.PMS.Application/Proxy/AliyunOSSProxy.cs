﻿using Aliyun.OSS;
using Aliyun.OSS.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MagicLamp.PMS.Application.Proxy
{
    public class AliyunOSSProxy
    {

        public const string ACCESSKEYID = "LTAIdMDOcyey5I6P";
        public const string ACCESSSECRETID = "pG5mLDCwnSHDNA9pcU9uxB5DiyJEds";
        public const string ENDPOINT = "oss-cn-hongkong.aliyuncs.com";
        public const string BUCKETNAME = "motan-private-dev";
        private static OssClient AliyunOssClient = new OssClient(ENDPOINT, ACCESSKEYID, ACCESSSECRETID);



        public static Image DownloadImage(string key)
        {
            return DownloadImage(BUCKETNAME, key);
        }


        public static Image DownloadImage(string bucketName, string key)
        {
            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags["type"] = "downloadImageFromOSS";
            try
            {

                var result = AliyunOssClient.GetObject(bucketName, key);

                using (var requestStream = result.Content)
                {
                    var image = System.Drawing.Image.FromStream(result.Content);
                    return image;
                }
            }
            catch (Exception ex)
            {
                CLSLogger.Error("DownloadImage发生异常", ex, tags);
                return null;
            }

        }


    }
}