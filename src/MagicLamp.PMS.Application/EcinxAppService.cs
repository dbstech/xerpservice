﻿using Abp.Application.Services;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Proxy.ECinx.Models;
using MagicLamp.XERP.BackgroundJob;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MagicLamp.PMS.Proxy;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.EntityFrameworkCore;
using TimeZoneConverter;
using Abp.Web.Models;
using MagicLamp.PMS.Proxy.ECinx;
using MagicLamp.PMS.Infrastructure;
using Abp.Runtime.Caching;
using Abp.Dependency;
using Microsoft.EntityFrameworkCore;
using Abp.AutoMapper;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Data.SqlClient;
using Newtonsoft.Json;
using MagicLamp.PMS.DTO;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Asn1.Ocsp;
using static Org.BouncyCastle.Math.EC.ECCurve;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace MagicLamp.PMS
{
    /// <summary>
    /// Ecinx
    /// </summary>
   public class EcinxAppService : ApplicationService
    {
        private const string ClogTagType = "EcinxOrderNotify";
        private List<EcinxConfig> _config;

        EcinxConfigerData config = EcinxConfigerData.Configuration;


        Dictionary<string, string> clogTags = new Dictionary<string, string>
        {
            ["type"] = ClogTagType
        };
        private TimeZoneInfo timeZoneNZ = TZConvert.GetTimeZoneInfo("Pacific/Auckland");
        private PMSDbContext _pmsContext;
        private HEERPDbContext _heerpContext;
        private IBackgroundJobManager _backgroundJobManager;
        private IRepository<EcinxOrdersPackTradeEntity, int> _EcinxOrdersPackTradeRepository;
        private SendNotificationToDingDingJob _dingdingNotificationJob;
        private ECinxClient _EcinxClient; //连接Ecinx同步产品
        private ECinxClient _WHYC_EcinxClient; //连接WHYC Ecinx同步产品

   
        public EcinxAppService(IBackgroundJobManager backgroundJobManager,
            IRepository<EcinxOrdersPackTradeEntity, int> EcinxOrdersPackTradeEntityRepository)
        {
            _pmsContext = new PMSDbContextFactory().CreateDbContext();
            _heerpContext = new HEERPDbContextFactory().CreateDbContext();
            _dingdingNotificationJob = new SendNotificationToDingDingJob();
            _backgroundJobManager = backgroundJobManager;
            _EcinxOrdersPackTradeRepository = EcinxOrdersPackTradeEntityRepository;
            _config = Ultities.GetConfig<List<EcinxConfig>>();
            ICacheManager cacheManager = IocManager.Instance.Resolve<ICacheManager>();
            _EcinxClient = new ECinxClient(cacheManager, ECinxClient.EcinxAccountEnum.CQBSC); //连接Ecinx同步产品 任选一个数据源即可）
            _WHYC_EcinxClient = new ECinxClient(cacheManager, ECinxClient.EcinxAccountEnum.YIWUYINGCHI);//连接WHYC Ecinx同步产品 任选一个数据源即可）
        }


        public void CreateOrder(EcinxOrderInput inputDTO)
        {
            _backgroundJobManager.Enqueue<ECinxCreateOrderJob, EcinxOrderInput>(inputDTO, BackgroundJobPriority.Normal);
        }


        /// <summary>
        /// 删除订单
        /// </summary>
        /// <param name="orderpakno"></param>
        /// <returns></returns>
        [DontWrapResult]
        public EcinxResponseInfo DeleteOrder(string orderpackno)
        {
            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();

            EcinxResponseInfo msg = new EcinxResponseInfo();
            //string shopcode = "";
            string MessageContent = "**消息类型**: 删除订单 \n\n";

            OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x => x.orderPackNo == orderpackno || x.orderID == orderpackno);

            int differenceInDays = 0;
            DateTime oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
            if (!string.IsNullOrWhiteSpace(orderPack.opt_time.ToString()))
            {
                oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(orderPack.opt_time.ToString()), timeZoneNZ);
                DateTime newDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                TimeSpan ts = newDate - oldDate;
                differenceInDays = ts.Days;
            }

            EcinxOrdersPackTradeEntity EcinxOrder = _pmsContext.EcinxOrdersPackTrades.FirstOrDefaultAsync(x => x.OrderPackNo == orderpackno || x.OrderID == orderpackno).Result;

            if (EcinxOrder == null)
            {
                MessageContent = $"包裹号在Ecinx不存在,包裹号：{orderpackno}";
                _dingdingNotificationJob.SendEcinxERPAlert("Ecinx未查找到此包裹", MessageContent);
                msg.code = 201;
                msg.message = "This package does not exist";

                ecinpushorderLog.OrderPackNo = orderpackno;
                ecinpushorderLog.WarningLevel = 0;  //WarningLevel =0: 普通告警;
                ecinpushorderLog.LackStockDays = differenceInDays;
                ecinpushorderLog.MessageType = "删除订单";
                ecinpushorderLog.FaildReason = "包裹号在Ecinx不存在";
                ecinpushorderLog.MessageDetail = "";
                _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                _pmsContext.SaveChanges();

                return msg;
            }

            

            EcinxResponse result = null;

            OrderDeleteInput input = new OrderDeleteInput();
            //input.declareOrderCode = orderpackno;
            //input.shopCode = shopcode;
            input.orderCode = EcinxOrder.EcinxOrderCode ?? "mfdorder";
            result = _EcinxClient.orderdelete(input).Result;

            if (!result.success)
            {
                MessageContent = $" **_<font face=华云彩绘 color=#FF0000> Ecinx删除订单失败 </font>_** \n\n";
                MessageContent = MessageContent + $" ##### **包裹号**:  **{orderPack.orderPackNo}** \n\n";
                MessageContent = MessageContent + $" 失败原因: ：{result.errorMsg} ";
                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx删除订单失败", MessageContent);

                msg.code = 203;
                msg.message = MessageContent;

                ecinpushorderLog.OrderPackNo = orderpackno;
                ecinpushorderLog.WarningLevel = 0;  //WarningLevel =0: 普通告警;
                ecinpushorderLog.LackStockDays = differenceInDays;
                ecinpushorderLog.MessageType = "删除订单";
                ecinpushorderLog.FaildReason = "Ecinx接口方错误";
                ecinpushorderLog.MessageDetail = result.errorMsg;
                _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                _pmsContext.SaveChanges();

                return msg;
            }

            msg.code = 200;
            msg.message = "success";


            //删除Ecinx订单
            var _ecinxorder = _pmsContext.EcinxOrdersPackTrades.Where(x => x.Opid == orderPack.opID).Select(x => new
            {
                x.CustomsPaymentData,
                x.CreateTime,
                x.OriginalInfo,
                x.Status,
                x.EcinxResponse,
                x.DeliveryCode,
                x.PayTime
            }).ToList();

            EcinxOrdersPackTradeEntity EcinxorderPackTrade = new EcinxOrdersPackTradeEntity();
            EcinxorderPackTrade.Opid = orderPack.opID;
            EcinxorderPackTrade.OrderID = orderPack.orderID;
            EcinxorderPackTrade.OrderPackNo = orderPack.orderPackNo;
            EcinxorderPackTrade.CustomsPaymentData = _ecinxorder.Count == 0 ? "" : _ecinxorder.FirstOrDefault().CustomsPaymentData;
            EcinxorderPackTrade.CreateTime = _ecinxorder.Count == 0 ? DateTime.UtcNow.ConvertUTCTimeToNZTime() : _ecinxorder.FirstOrDefault().CreateTime;
            EcinxorderPackTrade.ExportFlag = orderPack.exportFlag;
            EcinxorderPackTrade.OriginalInfo = _ecinxorder.Count == 0 ? "" : _ecinxorder.FirstOrDefault().OriginalInfo;
            // 0: Created Ecinx Success 1: Ordered  Ecinx Success 2: Ordered  Ecinx Failed; 3 order out; 4: 已包； -1：已删除 -2: 已取消
            EcinxorderPackTrade.Status = -1;
            EcinxorderPackTrade.EcinxResponse ="Order Deleted";
            EcinxorderPackTrade.DeliveryCode = _ecinxorder.FirstOrDefault().DeliveryCode;
            EcinxorderPackTrade.PayTime = _ecinxorder.FirstOrDefault().PayTime;
            _pmsContext.EcinxOrdersPackTrades.Update(EcinxorderPackTrade);
            _pmsContext.SaveChanges();

            return msg;

        }


        /// <summary>
        /// 取消订单  删除的订单才可以重新新增 取消订单不能再次新增
        /// </summary>
        /// <param name="orderpakno"></param>
        /// <returns></returns>
        [DontWrapResult]
        public EcinxResponseInfo CancelOrder(string orderpackno)
        {
            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();

            EcinxResponseInfo msg = new EcinxResponseInfo();
            //string shopcode = "";
            string MessageContent = "";

            OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x => x.orderPackNo == orderpackno || x.orderID == orderpackno);
            int differenceInDays = 0;
            DateTime oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
            if (!string.IsNullOrWhiteSpace(orderPack.opt_time.ToString()))
            {
                oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(orderPack.opt_time.ToString()), timeZoneNZ);
                DateTime newDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                TimeSpan ts = newDate - oldDate;
                differenceInDays = ts.Days;
            }

          
            var ecinxOrder = _pmsContext.EcinxOrdersPackTrades.FirstOrDefault(x => x.OrderPackNo == orderpackno || x.OrderID == orderpackno);

            if (ecinxOrder == null)
            {
                MessageContent = $"冻结的包裹号在Ecinx不存在，取消Ecinx订单失败,包裹号：{orderpackno}";
                _dingdingNotificationJob.SendEcinxERPAlert("Ecinx未查找到此订单", MessageContent);
                msg.code = 201;
                msg.message = "This package does not exist";

                ecinpushorderLog.OrderPackNo = orderpackno;
                ecinpushorderLog.WarningLevel = 0;  //WarningLevel =0: 普通告警;
                ecinpushorderLog.LackStockDays = differenceInDays;
                ecinpushorderLog.MessageType = "取消订单";
                ecinpushorderLog.FaildReason = MessageContent;
                ecinpushorderLog.MessageDetail = "";
                _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                _pmsContext.SaveChanges();

                return msg;
            }


            EcinxResponse result = null;

            OrderDeleteInput input = new OrderDeleteInput();
            //input.declareOrderCode = orderpackno;
            //input.shopCode = shopcode;
            input.orderCode = ecinxOrder.EcinxOrderCode;

            //104，105，106，107 ->WHYC
            //if (new[] { 104, 105, 106, 107 }.Contains((int)orderPack.exportFlag))
            //{
            //    result = _WHYC_EcinxClient.ordercancel(input).Result;
            //}
            //// MFD
            //else
            //{
            //    result = _EcinxClient.ordercancel(input).Result;
            //}

            EcinxClientV2 ecinxv2 = new EcinxClientV2();
            result = ecinxv2.ordercancelV2(orderPack.exportFlag ?? 0, input).Result;


            if (!result.success)
            {
                MessageContent = $"Ecinx取消订单失败, 包裹号：{orderpackno}。失败详情：{result.errorMsg.ToJsonString()} ";
                _dingdingNotificationJob.SendEcinxERPAlert("Ecinx取消订单失败", MessageContent);
                CLSLogger.Info("Ecinx取消订单失败", $"包裹号：{orderPack.orderPackNo}  Message: {result.ToJsonString()} ", clogTags);

                msg.code = 203;
                msg.message = MessageContent;

                ecinpushorderLog.OrderPackNo = orderpackno;
                ecinpushorderLog.WarningLevel = 0;  //WarningLevel =0: 普通告警;
                ecinpushorderLog.LackStockDays = differenceInDays;
                ecinpushorderLog.MessageType = "取消订单";
                ecinpushorderLog.FaildReason = MessageContent;
                ecinpushorderLog.MessageDetail = "";
                _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                _pmsContext.SaveChanges();


                return msg;
            }

            msg.code = 200;
            msg.message = "success";


            //取消Ecinx订单
            var _ecinxorder = _pmsContext.EcinxOrdersPackTrades.Where(x => x.Opid == orderPack.opID).Select(x => new
            {
                x.CustomsPaymentData,
                x.CreateTime,
                x.OriginalInfo,
                x.Status,
                x.EcinxResponse,
                x.DeliveryCode
            }).ToList();


            EcinxOrdersPackTradeEntity EcinxorderPackTrade = new EcinxOrdersPackTradeEntity();
            EcinxorderPackTrade.Opid = orderPack.opID;
            EcinxorderPackTrade.OrderID = orderPack.orderID;
            EcinxorderPackTrade.OrderPackNo = orderPack.orderPackNo;
            EcinxorderPackTrade.CustomsPaymentData = _ecinxorder.Count == 0 ? "" : _ecinxorder.FirstOrDefault().CustomsPaymentData;
            EcinxorderPackTrade.CreateTime = _ecinxorder.Count == 0 ? DateTime.UtcNow.ConvertUTCTimeToNZTime() : _ecinxorder.FirstOrDefault().CreateTime;
            EcinxorderPackTrade.ExportFlag = orderPack.exportFlag;
            EcinxorderPackTrade.OriginalInfo = _ecinxorder.Count == 0 ? "" : _ecinxorder.FirstOrDefault().OriginalInfo;
            // 0: Created Ecinx Success 1: Ordered  Ecinx Success 2: Ordered  Ecinx Failed; 3 order out; 4: 已包； -1：已删除; -2: 取消订单
            EcinxorderPackTrade.Status = -2;
            EcinxorderPackTrade.EcinxResponse = "Order Canceled";
            EcinxorderPackTrade.DeliveryCode = _ecinxorder.FirstOrDefault().DeliveryCode;
            _pmsContext.EcinxOrdersPackTrades.Update(EcinxorderPackTrade);
            _pmsContext.SaveChanges();


            return msg;

        }

        /// <summary>
        /// 根据包裹号解冻Ecinx订单；
        /// </summary>
        /// <param name="orderpakno"></param>
        /// <returns></returns>
        [DontWrapResult]
        public EcinxResponseInfo RestoreOrder(string orderpackno)
        {
            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();

            EcinxResponseInfo msg = new EcinxResponseInfo();
            string MessageContent = "";
            //已经删除的订单恢复
            List<EcinxOrdersPackTradeEntity> listOrderPackTrade = _EcinxOrdersPackTradeRepository.GetAll().Where(x => (orderpackno.Contains(x.OrderID) || orderpackno.Contains(x.OrderPackNo))).ToList();


            OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x => x.orderPackNo == orderpackno || x.orderID == orderpackno);
            int differenceInDays = 0;
            DateTime oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
            if (!string.IsNullOrWhiteSpace(orderPack.opt_time.ToString()))
            {
                oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(orderPack.opt_time.ToString()), timeZoneNZ);
                DateTime newDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                TimeSpan ts = newDate - oldDate;
                differenceInDays = ts.Days;
            }

            if (listOrderPackTrade.Count == 0)
            {
                MessageContent = $"Ecinx 未找到冻结的包裹，创建订单失败：包裹号：{orderpackno}";
                _dingdingNotificationJob.SendEcinxERPAlert("Ecinx未查找到此订单", MessageContent);

                msg.code = 201;
                msg.message = MessageContent;

                ecinpushorderLog.OrderPackNo = orderpackno;
                ecinpushorderLog.WarningLevel = 0;  //WarningLevel =0: 普通告警;
                ecinpushorderLog.LackStockDays = differenceInDays;
                ecinpushorderLog.MessageType = "解冻包裹";
                ecinpushorderLog.FaildReason = MessageContent;
                ecinpushorderLog.MessageDetail = "";
                _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                _pmsContext.SaveChanges();

                return msg;
            }

            try
            {
                var input = listOrderPackTrade.FirstOrDefault().OriginalInfo.ConvertFromJsonString<EcinxOrderInput>();
                _backgroundJobManager.Enqueue<ECinxCreateOrderJob, EcinxOrderInput>(input, BackgroundJobPriority.Normal);
            }
            catch (Exception ex)
            {
                Dictionary<string, string> clogTags = new Dictionary<string, string>
                {
                    ["type"] = ClogTagType,
                    ["orderid"] = listOrderPackTrade.FirstOrDefault().OrderPackNo
                };
                CLSLogger.Error("解冻Ecinx包裹失败", ex, clogTags);

                MessageContent = $"Ecinx 冻结的包裹失败，创建订单失败：包裹号：{orderpackno}, 明细:{ex.ToJsonString()} ";
                _dingdingNotificationJob.SendEcinxERPAlert("Ecinx冻结的包裹失败", MessageContent);
                msg.code = 201;
                msg.message = "failed";

                ecinpushorderLog.OrderPackNo = orderpackno;
                ecinpushorderLog.WarningLevel = 0;  //WarningLevel =0: 普通告警;
                ecinpushorderLog.LackStockDays = differenceInDays;
                ecinpushorderLog.MessageType = "解冻包裹";
                ecinpushorderLog.FaildReason = MessageContent;
                ecinpushorderLog.MessageDetail = "";
                _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                _pmsContext.SaveChanges();

                return msg;
            }


            msg.code = 200;
            msg.message = "success";
            return msg;
        }



        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="orderpakno"></param>
        /// <returns></returns>
        [DontWrapResult]
        public EcinxResponseInfo QueryOrder(string orderpackno)
        {
            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();

            EcinxResponseInfo msg = new EcinxResponseInfo();
            string MessageContent = "";

            OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x => x.orderPackNo == orderpackno || x.orderID == orderpackno);
            int differenceInDays = 0;
            DateTime oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
            if (!string.IsNullOrWhiteSpace(orderPack.opt_time.ToString()))
            {
                oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(orderPack.opt_time.ToString()), timeZoneNZ);
                DateTime newDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                TimeSpan ts = newDate - oldDate;
                differenceInDays = ts.Days;
            }

            bool EcinxorderPackTradeExists = _pmsContext.EcinxOrdersPackTrades.Any(x => x.OrderPackNo == orderpackno || x.OrderID == orderpackno);

            if (!EcinxorderPackTradeExists)
            {
                MessageContent = $"单号在Ecinx不存在，单号号：{orderpackno}";
                _dingdingNotificationJob.SendEcinxERPAlert("Ecinx未查找到此订单", MessageContent);
                msg.code = 201;
                msg.message = "This package does not exist";
                msg.data = "0"; // ERP会根据这个值，来删除Ecinx订单；

                ecinpushorderLog.OrderPackNo = orderpackno;
                ecinpushorderLog.WarningLevel = 0;  //WarningLevel =0: 普通告警;
                ecinpushorderLog.LackStockDays = differenceInDays;
                ecinpushorderLog.MessageType = "查询订单";
                ecinpushorderLog.FaildReason = MessageContent;
                ecinpushorderLog.MessageDetail = "";
                _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                _pmsContext.SaveChanges();

                return msg;
            }

            //EcinxQueryOrderResult result = null;
            //EcinxOrderQueryInput input = new EcinxOrderQueryInput();
            //input.declareOrderCode = orderpackno;
            //input.includeCancel = true;
            ////104，105，106，107 ->WHYC
            //if (new[] { 104, 105, 106, 107 }.Contains((int)orderPack.exportFlag))
            //{
            //    result = _WHYC_EcinxClient.QueryOrder(input).Result;
            //}
            //// MFD
            //else
            //{
            //    result = _EcinxClient.QueryOrder(input).Result;
            //}

            EcinxQueryOrderResult Orderresult = null;
            EcinxOrderQueryInput Orderinput = new EcinxOrderQueryInput();
            Orderinput.declareOrderCode = orderpackno; //orderpackno.IndexOf("-") > 0 ? orderpackno.Substring(0, orderpackno.IndexOf("-")) : orderpackno;
            Orderinput.includeCancel = true;

            EcinxClientV2 ecinxv2 = new EcinxClientV2();
            Orderresult = ecinxv2.QueryOrderV2(orderPack.exportFlag ?? 0, Orderinput).Result;


            ////104，105，106，107 ->WHYC
            //if (new[] { 104, 105, 106, 107 }.Contains((int)orderPack.exportFlag))
            //{
            //    Orderresult = _WHYC_EcinxClient.QueryOrder(Orderinput).Result;
            //}
            //// MFD
            //else
            //{
            //    Orderresult = _EcinxClient.QueryOrder(Orderinput).Result;
            //}


            if (!Orderresult.success)
            {
                MessageContent = $"Ecinx订单查询失败, 包裹号：{orderpackno}。失败详情：{Orderresult.ToJsonString()} ";
                _dingdingNotificationJob.SendEcinxERPAlert("Ecinx删除订单失败", MessageContent);
                CLSLogger.Info("Ecinx删除订单失败", $" Message: {Orderresult.ToJsonString()} ", clogTags);

                msg.code = 203;
                msg.message = MessageContent;
                msg.data = "1"; // 接口返回失败，需要重试

                ecinpushorderLog.OrderPackNo = orderpackno;
                ecinpushorderLog.WarningLevel = 0;  //WarningLevel =0: 普通告警;
                ecinpushorderLog.LackStockDays = differenceInDays;
                ecinpushorderLog.MessageType = "查询订单";
                ecinpushorderLog.FaildReason = MessageContent;
                ecinpushorderLog.MessageDetail = "";
                _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                _pmsContext.SaveChanges();

                return msg;
            }

            msg.code = 200;
            msg.message = "success";
            // cancel int 是否取消 (0:是 1:否);
            // 0:已取消；1：未取消;
            msg.data = Orderresult.result.total == 1 ? Orderresult.result.rows[0].cancel.ToString() : Orderresult.ToJsonString();

            return msg;
        }


        [DontWrapResult]
        public EcinxResponseInfo SearchOrderpackNO(string orderpakno)
        {
            EcinxResponseInfo msg = new EcinxResponseInfo();
            msg.code = 200;
            msg.message = "This Orderpack already exists";

            bool EcinxorderPackTradeExists = _pmsContext.EcinxOrdersPackTrades.Any(x => x.OrderPackNo == orderpakno || x.OrderID == orderpakno);
            bool orderPackTradeExists = _pmsContext.OrderPackTrades.Any(x => x.OrderPackNo == orderpakno || x.OrderID == orderpakno);

            if (EcinxorderPackTradeExists == false && orderPackTradeExists == false)
            {
                msg.code = 201;
                msg.message = "This package does not exist"; 
            }

            return msg;

        }

        // 步骤一：回调方式，订单创建成功；
        // 回调URL在web.config配置; 不需要Ecinx配置
        [DontWrapResult]
        public string SyncCreateOrderStatus(OrderAddOrOutnotifyResponse ResponseDTO)
        {
            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();
            string result = "", declareOrderCode = "", orderCode = "";
            string _return= "success";
            var Response = ResponseDTO.ToJsonString();

            CLSLogger.Info("SyncCreateOrderStatus", Response, clogTags);

            result = ResponseDTO.result;
            declareOrderCode = ResponseDTO.declareOrderCode;
            orderCode = ResponseDTO.orderCode;

            OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x => x.orderPackNo == declareOrderCode || x.orderID == declareOrderCode);
            int differenceInDays = 0;
            DateTime oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
            if (!string.IsNullOrWhiteSpace(orderPack.opt_time.ToString()))
            {
                oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(orderPack.opt_time.ToString()), timeZoneNZ);
                DateTime newDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                TimeSpan ts = newDate - oldDate;
                differenceInDays = ts.Days;
            }

            try
            {

                bool EcinxorderPackTradeExists = _pmsContext.EcinxOrdersPackTrades.Any(x => x.OrderPackNo == declareOrderCode || x.OrderID == declareOrderCode);
                if (!EcinxorderPackTradeExists)
                {
                    string MessageContent = $"未找到订单号：{declareOrderCode}。详情：{Response}";
                    _dingdingNotificationJob.SendEcinxERPAlert("Ecinx创建订单失败", MessageContent);
                    CLSLogger.Info("ECinx订单回调失败", MessageContent, clogTags);


                    ecinpushorderLog.OrderPackNo = declareOrderCode;
                    ecinpushorderLog.WarningLevel = 10;  //WarningLevel =0: 普通告警; WarningLevel =10: 需要客服处理;
                    ecinpushorderLog.LackStockDays = differenceInDays;
                    ecinpushorderLog.MessageType = "创建订单回调";
                    ecinpushorderLog.FaildReason = MessageContent;
                    ecinpushorderLog.MessageDetail = "";
                    _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                    _pmsContext.SaveChanges();

                    return _return;
                }
                else
                {
                    if (orderPack == null)
                    {
                        return _return;
                    }



                    // 订单创建结果：如果是创建订单成功
                    if (result == "success" || result == "fail")
                    {
                        var _ecinxorder = _pmsContext.EcinxOrdersPackTrades.Where(x => x.Opid == orderPack.opID).Select(x => new
                        {
                            x.CustomsPaymentData,
                            x.CreateTime,
                            x.OriginalInfo,
                            x.PayTime
                        }).ToList();

                        EcinxOrdersPackTradeEntity EcinxorderPackTrade = new EcinxOrdersPackTradeEntity();
                        EcinxorderPackTrade.Opid = orderPack.opID;
                        EcinxorderPackTrade.OrderID = orderPack.orderID;
                        EcinxorderPackTrade.OrderPackNo = orderPack.orderPackNo;
                        EcinxorderPackTrade.CustomsPaymentData = _ecinxorder.FirstOrDefault().CustomsPaymentData;
                        EcinxorderPackTrade.CreateTime = _ecinxorder.FirstOrDefault().CreateTime;
                        EcinxorderPackTrade.ExportFlag = orderPack.exportFlag;
                        EcinxorderPackTrade.OriginalInfo = _ecinxorder.FirstOrDefault().OriginalInfo;
                        // 0: Created Ecinx Success 1: Ordered  Ecinx Success 2: Ordered  Ecinx Failed
                        EcinxorderPackTrade.Status = result == "success" ? 1 : 2;
                        EcinxorderPackTrade.EcinxResponse = Response;
                        EcinxorderPackTrade.PayTime = _ecinxorder.FirstOrDefault().PayTime;
                        EcinxorderPackTrade.EcinxOrderCode = orderCode;
                        _pmsContext.EcinxOrdersPackTrades.Update(EcinxorderPackTrade);
                        _pmsContext.SaveChanges();

                        //因为订单在Ecinx创建成功，允许修改订单的收件人信息，所以不改变包裹状态 orderPack.status = 0；
                        // Ecinx下单成功的时间，orderPack.assignTime；
                        if ((orderPack.status == 0) && (result == "success"))
                        {
                            //orderPack.status = 3;
                            orderPack.assignTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                            _heerpContext.OrderPacks.Update(orderPack);
                            _heerpContext.SaveChanges();
                        }

                        string[] errorMessagesToExclude = { "此订单已存在" };

                        if (result == "fail")
                        {
                            string MessageContent = $"**消息类型**: <font color=#FF0000  face=黑体>推送Ecinx订单,回调返回结果 </font> \n\n";
                            MessageContent = MessageContent + $"**包裹号**: **_<font face=华云彩绘 color=#FF0000> {ResponseDTO.declareOrderCode} </font>_**  \n\n";
                            MessageContent = MessageContent + $"**失败原因**: **_<font face=华云彩绘 color=#FF0000> {ResponseDTO.errorMsg} </font>_**  \n\n";
                            MessageContent = MessageContent + $"**详情**:  {Response.ToJsonString()}   \n\n";

                            if (!errorMessagesToExclude.Contains(ResponseDTO.errorMsg))
                            {
                                _dingdingNotificationJob.SendEcinxERPAlert("订单创建失败", MessageContent);
                            }
                           
                            ecinpushorderLog.OrderPackNo = declareOrderCode;
                            ecinpushorderLog.WarningLevel = 10;  //WarningLevel =0: 普通告警; WarningLevel =10: 需要客服处理;
                            ecinpushorderLog.LackStockDays = differenceInDays;
                            ecinpushorderLog.MessageType = "创建订单回调";
                            ecinpushorderLog.FaildReason = MessageContent;
                            ecinpushorderLog.MessageDetail = "";
                            _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                            _pmsContext.SaveChanges();

                            return _return;
                        }
                      

                    }
                    else {

                        //订单出库

                        // 出库成功
                        var logisticCode = ResponseDTO.logisticsCode;
                        var expressCode = ResponseDTO.expressCode;
                        //物流信息不能为空
                        if (expressCode != null || logisticCode != null)
                        {

                            var _ecinxorder = _pmsContext.EcinxOrdersPackTrades.Where(x => x.Opid == orderPack.opID).Select(x => new
                            {
                                x.CustomsPaymentData,
                                x.CreateTime,
                                x.OriginalInfo,
                                x.Status,
                                x.EcinxResponse,
                                x.PayTime
                            }).ToList();


                            EcinxOrdersPackTradeEntity EcinxorderPackTrade = new EcinxOrdersPackTradeEntity();
                            EcinxorderPackTrade.Opid = orderPack.opID;
                            EcinxorderPackTrade.OrderID = orderPack.orderID;
                            EcinxorderPackTrade.OrderPackNo = orderPack.orderPackNo;
                            EcinxorderPackTrade.CustomsPaymentData = _ecinxorder.Count == 0 ? "" : _ecinxorder.FirstOrDefault().CustomsPaymentData;
                            EcinxorderPackTrade.CreateTime = _ecinxorder.Count == 0 ? DateTime.UtcNow.ConvertUTCTimeToNZTime() : _ecinxorder.FirstOrDefault().CreateTime;
                            EcinxorderPackTrade.ExportFlag = orderPack.exportFlag;
                            EcinxorderPackTrade.OriginalInfo = _ecinxorder.Count == 0 ? "" : _ecinxorder.FirstOrDefault().OriginalInfo;
                            // 0: Created Ecinx Success 1: Ordered  Ecinx Success 2: Ordered  Ecinx Failed; 3 order out; 4: 已包； -1：已删除
                            EcinxorderPackTrade.Status = expressCode != "" ? 3 : _ecinxorder.FirstOrDefault().Status;
                            EcinxorderPackTrade.EcinxResponse = _ecinxorder.Count == 0 ? "" : Response;
                            EcinxorderPackTrade.DeliveryCode = logisticCode; // 查询发货单号，保存发货单号;
                            EcinxorderPackTrade.PayTime = _ecinxorder.FirstOrDefault().PayTime;
                            EcinxorderPackTrade.EcinxOrderCode = orderCode;
                            _pmsContext.EcinxOrdersPackTrades.Update(EcinxorderPackTrade);
                            _pmsContext.SaveChanges();

                            // 已包包裹变-出库状态
                            if (orderPack.status == 3)
                            {
                                orderPack.status = 4;
                                //orderPack.carrierId = logisticCode;
                                //orderPack.freightId = expressCode;
                                orderPack.bonded_warehouse_carrier_id = logisticCode;
                                orderPack.bonded_warehouse_carrier_company = expressCode;

                                //如果是真直邮，没有一程面单，用第三方物流覆盖为空的一程面单
                                //如果是假直邮，保留一程面单，不要覆盖原面单
                                if (orderPack.carrierId.IsNullOrEmpty())
                                {
                                    orderPack.carrierId = logisticCode;
                                    orderPack.freightId = expressCode;
                                }

                                orderPack.out_time = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                                _heerpContext.OrderPacks.Update(orderPack);
                                _heerpContext.SaveChanges();
                            }
                          
                            
                        }


                    }
                   
                }

            }
            catch (Exception ex)
            {
                string MessageContent = $"Ecinx消息通知ERP接收处理失败_同步状态[下单或者出库] Ecinx后期会重试通知, Ecinx消息通知内容：{Response.ToJsonString()}";
                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx消息通知处理失败", MessageContent);
                //CLSLogger.Info("Ecinx消息通知处理失败", ex + " 参数: "+ Response.ToJsonString(), clogTags);

                //ecinpushorderLog.OrderPackNo = declareOrderCode;
                //ecinpushorderLog.WarningLevel = 10;  //WarningLevel =0: 普通告警; WarningLevel =10: 需要客服处理;
                //ecinpushorderLog.LackStockDays = differenceInDays;
                //ecinpushorderLog.MessageType = "创建订单回调";
                //ecinpushorderLog.FaildReason = MessageContent;
                //ecinpushorderLog.MessageDetail = "";
                //_pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                //_pmsContext.SaveChanges();

                _return = "failed";
            }

            return _return;
        }
        // 步骤二：回调方式，发货单状态;  changeType：1新增 一次配货一个发货单
        // 通知请在 ecinx_cloud 订单管理 -> 发货单通知配置” 中进行维护
        [DontWrapResult]
        public string SendOrderStatus(SendOrdernotifyResponse ResponseDTO)
        {
            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();
            string declareOrderCode = "", orderCode = "";
            string _return = "success";

            var Response = ResponseDTO.ToJsonString();
            declareOrderCode = ResponseDTO.platformOrderCode;
            orderCode = ResponseDTO.orderCode;

            OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x => x.orderPackNo == declareOrderCode || x.orderID == declareOrderCode);
            int differenceInDays = 0;
            DateTime oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);


            if (orderPack?.opt_time != null)
            {
                oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(orderPack.opt_time.ToString()), timeZoneNZ);
                DateTime newDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                TimeSpan ts = newDate - oldDate;
                differenceInDays = ts.Days;
            }

            string MessageContent = "**消息类型**: Ecinx回调通知 \n\n";

            try
            {
               

                bool EcinxorderPackTradeExists = _pmsContext.EcinxOrdersPackTrades.Any(x => x.OrderPackNo == declareOrderCode || x.OrderID == declareOrderCode);
                if (!EcinxorderPackTradeExists)
                {
                    MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {declareOrderCode}  \n\n";
                    MessageContent = MessageContent + $" ##### **失败原因**:  EcinxOrdersPackTrades 未找到该订单号 \n\n";
                    //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx 创建订单失败", MessageContent);

                }
                else
                {

                    var _ecinxorder = _pmsContext.EcinxOrdersPackTrades.Where(x => x.Opid == orderPack.opID).Select(x => new
                    {
                        x.CustomsPaymentData,
                        x.CreateTime,
                        x.OriginalInfo,
                        x.Status,
                        x.PayTime
                    }).ToList();

                    EcinxOrdersPackTradeEntity EcinxorderPackTrade = new EcinxOrdersPackTradeEntity();
                    EcinxorderPackTrade.Opid = orderPack.opID;
                    EcinxorderPackTrade.OrderID = orderPack.orderID;
                    EcinxorderPackTrade.OrderPackNo = orderPack.orderPackNo;
                    EcinxorderPackTrade.CustomsPaymentData = _ecinxorder.FirstOrDefault().CustomsPaymentData;
                    EcinxorderPackTrade.CreateTime = _ecinxorder.FirstOrDefault().CreateTime;
                    EcinxorderPackTrade.ExportFlag = orderPack.exportFlag;
                    EcinxorderPackTrade.OriginalInfo = _ecinxorder.FirstOrDefault().OriginalInfo;
                    // 0: Created Ecinx Success 1: Ordered  Ecinx Success 2: Ordered  Ecinx Failed; 3 order out; 4: 已包； -1：已删除; -2: 取消订单
                    EcinxorderPackTrade.Status = ResponseDTO.changeType == "1" ? 4 : _ecinxorder.FirstOrDefault().Status;
                    EcinxorderPackTrade.EcinxResponse = Response;
                    EcinxorderPackTrade.PayTime = _ecinxorder.FirstOrDefault().PayTime; ;
                    EcinxorderPackTrade.EcinxOrderCode = orderCode;
                    _pmsContext.EcinxOrdersPackTrades.Update(EcinxorderPackTrade);
                    _pmsContext.SaveChanges();
                    CLSLogger.Info("ECinx订单回调成功", Response, clogTags);

                    if (orderPack.status == 0 && ResponseDTO.changeType == "1" ) //Ecinx已经配
                    {
                        orderPack.status = 3; //Ecinx已配货
                        orderPack.pick_time = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                        orderPack.pickIdReal = "Ecinx已配货";

                        orderPack.pack_time = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                        orderPack.packIdReal = "Ecinx已包(包裹不能取消)";
                        _heerpContext.OrderPacks.Update(orderPack);
                        _heerpContext.SaveChanges();
                    }
                  
                }


            }
            catch (Exception ex)
            {
                MessageContent = MessageContent + $" ##### **失败原因**: {Response.ToJsonString()} \n\n";
                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx 发货单状态同步失败", MessageContent);
                CLSLogger.Info("Ecinx 发货单状态同步失败", MessageContent + ex.ToJsonString(), clogTags);

                //ecinpushorderLog.OrderPackNo = declareOrderCode;
                //ecinpushorderLog.WarningLevel = 0;  //WarningLevel =0: 普通告警; WarningLevel =10: 需要客服处理;
                //ecinpushorderLog.LackStockDays = differenceInDays;
                //ecinpushorderLog.MessageType = "发货订单回调";
                //ecinpushorderLog.FaildReason = MessageContent;
                //ecinpushorderLog.MessageDetail = "";
                //_pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                //_pmsContext.SaveChanges();

                _return = "failed";
            }

            return _return;
        }


        // 步骤二：回调方式，订单变动通知;   changeType为 3:审核、4:反审核、5:拦截、6:取消拦截、7:取消、8:删除 时的json数据 : 暂定9 配货失败
        // 通知请在 ecinx_cloud 订单管理 -> 订单通知配置” 中进行维护
        [DontWrapResult]
        public string ChangeOrderNotification(ChangeOrderNotificationResponse ResponseDTO)
        {
            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();
            string declareOrderCode = "";
            string _return = "success";

            var Response = ResponseDTO.ToJsonString();
            declareOrderCode = ResponseDTO.platformOrderCode;

            OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x => x.orderPackNo == declareOrderCode || x.orderID == declareOrderCode);
            int differenceInDays = 0;
            DateTime oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
            if (orderPack?.opt_time != null)
            {
                oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(orderPack.opt_time.ToString()), timeZoneNZ);
                DateTime newDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                TimeSpan ts = newDate - oldDate;
                differenceInDays = ts.Days;
            }

            string MessageContent = "**消息类型**: 订单变动通知 \n\n";
            try
            {

                bool EcinxorderPackTradeExists = _pmsContext.EcinxOrdersPackTrades.Any(x => x.OrderPackNo == declareOrderCode || x.OrderID == declareOrderCode);
                if (!EcinxorderPackTradeExists)
                {
                    MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {declareOrderCode}  \n\n";
                    MessageContent = MessageContent + $" ##### **失败原因**:  EcinxOrdersPackTrades 未找到该订单号 \n\n";

                    _dingdingNotificationJob.SendEcinxERPAlert("Ecinx 创建订单失败", MessageContent);
                }
                else
                {
                    if (ResponseDTO.changeType == 9)
                    {
                        MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {declareOrderCode}  \n\n";
                        MessageContent = MessageContent + $" ##### **失败原因**:  配货失败 \n\n";
                       // _dingdingNotificationJob.SendEcinxERPAlert("Ecinx 订单变动通知", MessageContent);

                        ecinpushorderLog.OrderPackNo = declareOrderCode;
                        ecinpushorderLog.WarningLevel = 10;  //WarningLevel =0: 普通告警; WarningLevel =10: 需要客服处理;
                        ecinpushorderLog.LackStockDays = differenceInDays;
                        ecinpushorderLog.MessageType = "订单变动通知";
                        ecinpushorderLog.FaildReason = "配货失败";
                        ecinpushorderLog.MessageDetail = "";
                        _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                        _pmsContext.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageContent = MessageContent + $" ##### **失败原因**: {Response.ToJsonString()} \n\n";
                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx 订单变动通知", MessageContent);
                //ecinpushorderLog.OrderPackNo = declareOrderCode;
                //ecinpushorderLog.WarningLevel = 0;  //WarningLevel =0: 普通告警; WarningLevel =10: 需要客服处理;
                //ecinpushorderLog.LackStockDays = differenceInDays;
                //ecinpushorderLog.MessageType = "发货订单回调";
                //ecinpushorderLog.FaildReason = MessageContent;
                //ecinpushorderLog.MessageDetail = "";
                //_pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                //_pmsContext.SaveChanges();
                _return = "failed";
            }

            return _return;
        }



        // 库存变动接收SKU
        // 通知配置请在 ecinx_cloud 仓库进行维护 基本信息->仓库列表->编辑某一个仓库-》库存变动通知设置
        [DontWrapResult]
        public string SyncStockChange(EcinxStockChange ResponseDTO)
        {

            string _return = "success";
            EcinxInventoryQueryResponse result = new EcinxInventoryQueryResponse();

            var Response = ResponseDTO.ToJsonString();
            CLSLogger.Info("EcinxStockChange", Response, clogTags);

            try
            {

                EcinxInventoryQueryInput input = new EcinxInventoryQueryInput();
                input.goodsCode = ResponseDTO.goodsCode;
                //input.warehouseCode = ResponseDTO.warehouseCode;

                result = _EcinxClient.QueryInventory(input).Result;
                if (result == null)
                {
                    string MessageContent = $"Ecinx查询库存失败, 返回值：{result.ToJsonString()}";
                    //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx查询库存失败_SyncStockChange", MessageContent);
                    CLSLogger.Info("QueryEcinxinventory", $"goodcode:{ResponseDTO.goodsCode} Message：{ MessageContent } ", clogTags);

                    return _return;
                }
                else
                {
                    if (result.result.total == 0)
                    {
                        string MessageContent = $"Ecinx库存查询失败_产品原因,SKU:{ResponseDTO.goodsCode} 在Ecinx系统未查到产品库存信息!";
                        _dingdingNotificationJob.SendEcinxERPAlert("Ecinx库存查询失败", MessageContent);
                        return _return;
                    }


                    int  totalinventory = 0;
                    foreach (var item in result.result.rows)
                    {
                        List<string>  MFDwarehousecode = config.MFD.ExportMapping.Values.Select(e => e.WarehouseCode).ToList();
                        //Filter WarehouseCode
                        if (MFDwarehousecode.Contains(item.warehouseCode))
                        {
                            //All canSalesNum
                            totalinventory = totalinventory + item.canSalesNum;
                        }
                    }


                    if (totalinventory <= 0)
                    {
                        //Sync Stock to ERP
                        // 2024.12.18 14:52 labi 确认需要同步负库存到我们ERP
                        SyncEcinxStockToERP("MFD", ResponseDTO.goodsCode, ResponseDTO.barCode);
                        return _return;
                    }
                    else
                    {

                        //DateTime start = Convert.ToDateTime("2022-07-08 10:00:00"); //UTC时间 -> 北京时间: 2022-07-08 18:00:00
                        //DateTime end = Convert.ToDateTime("2022-07-11 10:00:00"); //UTC时间 -> 北京时间: 2022-02-07 00:00:00

                        //string MessageContent = "**消息类型**: 库存变更 \n\n";
                        //if (DateTime.UtcNow >= start && DateTime.UtcNow <= end)
                        //{
                        //    MessageContent = $"<font color=#FF0000  face=黑体>停止推送Ecinx系统</font> \n\n";
                        //    MessageContent = MessageContent + $"北京时间 7月8日 18:00 ~  7月11日 18:00 停推！";
                        //    _dingdingNotificationJob.SendEcinxERPAlert("魔法灯-停止推送Ecinx系统", MessageContent);
                        //    //CLSLogger.Info("义乌保税仓(暂停服务 9月3日 周五中午12点-9月6日 周五中午12),返回值为空:", $"包裹号：{orderPack.orderPackNo}  inputDto: {inputDto.ToJsonString()} listProductsFromOrder: {listProductsFromOrder.ToJsonString()} listProductInfo: {listProductInfo.ToJsonString()}", LogTags);
                        //    return _return;
                        //}


                        string orderpackSQL = $@"  with cte as (
                             select row_number() over(order by PayTime) as ID,OrderPackNo,Opid,customsPaymentData,CreateTime,OrderID,Status,DeliveryCode,ExportFlag,
                             EcinxResponse,OriginalInfo,paytime,EcinxOrderCode
                             from EcinxOrdersPackTrade where Status = 0 and ExportFlag not in (104,105,106,107)
                            ),
                            cteresult as( 
                             select *,NTILE(24) over(order by id )-1 as rowid
                             from cte
                            )
                            select OrderPackNo,Opid,CustomsPaymentData,CreateTime,OrderID,Status,DeliveryCode,ExportFlag,EcinxResponse,OriginalInfo,PayTime,EcinxOrderCode
                            from cteresult where OriginalInfo like '%{ResponseDTO.goodsCode}%'
                            order by PayTime asc
                            ";
                        //var PMSDbContext = new PMSDbContextFactory().CreateDbContext();
                        using (var PMSDbContext = new PMSDbContextFactory().CreateDbContext())
                        {
                            //List<Ecinxorderpack> orderpacklist = PMSDbContext.EcinxOrdersPackTrades.FromSql(orderpackSQL)
                            //                .MapTo<List<Ecinxorderpack>>()
                            //                .ToList();
                            List<Ecinxorderpack> orderpacklist = PMSDbContext.EcinxOrdersPackTrades
                                                                   .FromSql(orderpackSQL)
                                                                   .Select(x => new Ecinxorderpack
                                                                   {
                                                                       OrderPackNo = x.OrderPackNo,
                                                                       Opid = x.Opid,
                                                                       CustomsPaymentData = x.CustomsPaymentData,
                                                                       CreateTime = x.CreateTime,
                                                                       OrderID = x.OrderID,
                                                                       Status = x.Status,
                                                                       DeliveryCode = x.DeliveryCode,
                                                                       ExportFlag = x.ExportFlag,
                                                                       EcinxResponse = x.EcinxResponse,
                                                                       OriginalInfo = x.OriginalInfo,
                                                                       PayTime = x.PayTime,
                                                                       EcinxOrderCode = x.EcinxOrderCode
                                                                   })
                                                                   .ToList();
                            //var stringorderpakno = string.Join("|", orderpacklist.Select(x => x.OrderPackNo).ToArray());
                            if (orderpacklist.Count > 0)
                            {
                                string MessageContent = $"<font color=#FF0000  face=黑体>新库存分配: 先支付先分配原则 </font> \n\n";
                                MessageContent = MessageContent + $" ##### **库存分配结论**: SKU:{ResponseDTO.goodsCode} 可销售数量: {totalinventory} 本次同步参与库存分配，根据分配原则, 参与分配此SKU的缺货订单数量：{orderpacklist.Count}    \n\n";
                                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx库存分配", MessageContent);
                                BondWarehousePackaged(orderpacklist, ResponseDTO);
                            }
                        }


                        // 2024.12.18 14:52 labi 确认需要同步负库存到我们ERP
                        SyncEcinxStockToERP("MFD", ResponseDTO.goodsCode, ResponseDTO.barCode);
                        return _return;
                    }

                }

            }
            catch (Exception ex)
            {
                string MessageContent = $"Ecinx库存查询失败,返回值:{Response.ToJsonString()}";
                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx库存查询失败", MessageContent);
                CLSLogger.Info("EcinxInventoryQueryResultFailed",$"goodcode: {ResponseDTO.goodsCode} ex: {ex.ToJsonString()} canSalesNum:{result.ToJsonString()} ", clogTags);
                _return = "failed";

            }

            return _return;

        }

        //New Method
        public void SyncEcinxStockToERP(string ecinxsystem, string goodscode,string barcode)
        {
            List<SyncEcinxStockDTO> dtos = new List<SyncEcinxStockDTO>();
            EcinxInventoryQueryResponse inventoryresult = new EcinxInventoryQueryResponse();
            List<string> WarehouseCode = new List<string> { };

            EcinxInventoryQueryInput inventoryinput = new EcinxInventoryQueryInput();
            inventoryinput.goodsCode = goodscode;
            //inventoryinput.warehouseCode = ResponseDTO.warehouseCode;
            if (ecinxsystem == "MFD")
            {
                List<string> MFDwarehousecode = config.MFD.ExportMapping.Values.Select(e => e.WarehouseCode).ToList();
                inventoryresult = _EcinxClient.QueryInventory(inventoryinput).Result;
                WarehouseCode = MFDwarehousecode;
            }

            if (ecinxsystem == "WHYC")
            {
                List<string> WHYCwarehousecode = config.WHYC.ExportMapping.Values.Select(e => e.WarehouseCode).ToList();
                inventoryresult = _WHYC_EcinxClient.QueryInventory(inventoryinput).Result;
                WarehouseCode = WHYCwarehousecode;
            }

            CLSLogger.Info("EcinxAppService1", $"goodcode: {goodscode} QueryInventory:{inventoryresult.ToJsonString()} ", clogTags);

            if (inventoryresult == null)
            {
                string MessageContent = $"Ecinx查询库存失败, 返回值：{inventoryresult.ToJsonString()}";
                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx查询库存失败_SyncStockChange", MessageContent);
                CLSLogger.Info("QueryEcinxinventory", $"goodcode:{goodscode} Message：{MessageContent} ", clogTags);
                return ;
            }
            else
            {

                if (inventoryresult.result.total == 0)
                {
                    string MessageContent = $"Ecinx库存查询失败_产品原因,SKU:{goodscode} 在Ecinx系统未查到产品库存信息!";
                    _dingdingNotificationJob.SendEcinxERPAlert("Ecinx库存查询失败", MessageContent);

                    return ;
                }

                int totalinventory = 0;
                string expiredate = "";
                double CostPrice = 0;

                List<double> CostPriceList = new List<double>();
                List<string> expiredateList = new List<string>();


                foreach (var item in inventoryresult.result.rows)
                {
                    //Filter WarehouseCode
                    if (WarehouseCode.Contains(item.warehouseCode))
                    {
                        //All canSalesNum
                        totalinventory = totalinventory + item.canSalesNum;
                        if (item.costPrice > 0)
                        {
                            CostPriceList.Add(item.costPrice);
                        }

                        if (item.batches != null)
                        {
                            if (item.batches.Where(t => t.canPickingNum > 0).Select(t => t.expiryDate) != null)
                            {
                                expiredateList.Add(item.batches.Where(t => t.canPickingNum > 0).Min(t => t.expiryDate));
                            }
                        }
                    }
                }

                expiredate = expiredateList.Count > 0 ? expiredateList.Min() : expiredate; //Muti Warehouse get Min
                CostPrice = CostPriceList.Count > 0 ? CostPriceList.Max() : CostPrice; //Muti Warehosue get Max

                if (totalinventory > 0 || expiredateList.Count > 0 || CostPriceList.Count > 0)
                {
                    SyncEcinxStockDTO _ecinxsku = new SyncEcinxStockDTO();
                    _ecinxsku.SKU = goodscode;
                    _ecinxsku.Barcode = barcode;
                    _ecinxsku.ExpiryDate = expiredate;
                    //运营还有一个需求 就是  由于188 并发导致的 超卖 需要ecinx同步库存的时候  跟odoo一样 mfderp 需要把 ecinx 可销售库存总数 * 0.9 （向下取整）  
                    //如果可销售总数 = 1 则同步 1
                    //这种方式来避免并发超卖
                    _ecinxsku.StockQty = totalinventory == 1 ? 1 : (int)Math.Floor(totalinventory * 0.9);

                    _ecinxsku.CostPrice = CostPrice;
                    dtos.Add(_ecinxsku);
                }
            }

            if (dtos.Count > 0)
            {
                _backgroundJobManager.EnqueueAsync<SyncStockFromEcinxJob, List<SyncEcinxStockDTO>>(dtos, BackgroundJobPriority.BelowNormal, TimeSpan.FromSeconds(3));
            }
        }


        // 库存变动接收SKU
        // 通知配置请在 ecinx_cloud 仓库进行维护 基本信息->仓库列表->编辑某一个仓库-》库存变动通知设置
        [DontWrapResult]
        public string WHYC_SyncStockChange(EcinxStockChange ResponseDTO)
        {
            string _return = "success";
            EcinxInventoryQueryResponse result = new EcinxInventoryQueryResponse();

            if (ResponseDTO == null)
            {
                CLSLogger.Info("WHYC_EcinxStockChange", "ResponseDTO is null", clogTags);
                return "failed";
            }

            var Response = ResponseDTO.ToJsonString();
            CLSLogger.Info("WHYC_EcinxStockChange", Response, clogTags);

            try
            {
                if (ResponseDTO.goodsCode == null)
                {
                    CLSLogger.Info("WHYC_EcinxStockChange", "goodsCode is null", clogTags);
                    return "failed";
                }

                EcinxInventoryQueryInput input = new EcinxInventoryQueryInput
                {
                    goodsCode = ResponseDTO.goodsCode
                };

                result = _WHYC_EcinxClient.QueryInventory(input).Result;

                CLSLogger.Info("WHYC_result", result.ToJsonString(), clogTags);

                if (result == null)
                {
                    string MessageContent = $"WHYC_Ecinx查询库存失败, 返回值：{result.ToJsonString()}";
                    //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx查询库存失败_SyncStockChange", MessageContent);
                    CLSLogger.Info("WHYC_QueryEcinxinventory", $"goodcode:{ResponseDTO.goodsCode} Message：{MessageContent} ", clogTags);

                    return _return;
                }
                else
                {
                    if (result.result.total == 0)
                    {
                        string MessageContent = $"_WHYC_EcinxClientEcinx库存查询失败_产品原因,SKU:{ResponseDTO.goodsCode} 在Ecinx系统未查到产品库存信息!";
                        _dingdingNotificationJob.SendEcinxERPAlert("Ecinx库存查询失败", MessageContent);
                        return _return;
                    }


                    int totalinventory = 0;
                    foreach (var item in result.result.rows)
                    {
                        List<string> WHYCwarehousecode = config.WHYC.ExportMapping.Values.Select(e => e.WarehouseCode).ToList();
                        //Filter WarehouseCode
                        if (WHYCwarehousecode.Contains(item.warehouseCode))
                        {
                            //All canSalesNum
                            totalinventory = totalinventory + item.canSalesNum;
                        }
                    }


                    if (totalinventory <= 0)
                    {
                        //return _return;
                        // 2024.12.18 14:52 labi 确认需要同步负库存到我们ERP
                        SyncEcinxStockToERP("WHYC", ResponseDTO.goodsCode, ResponseDTO.barCode);
                        return _return;
                    }
                    else
                    {

                        //DateTime start = Convert.ToDateTime("2022-07-08 10:00:00"); //UTC时间 -> 北京时间: 2022-07-08 18:00:00
                        //DateTime end = Convert.ToDateTime("2022-07-11 10:00:00"); //UTC时间 -> 北京时间: 2022-02-07 00:00:00

                        //string MessageContent = "**消息类型**: 库存变更 \n\n";
                        //if (DateTime.UtcNow >= start && DateTime.UtcNow <= end)
                        //{
                        //    MessageContent = $"<font color=#FF0000  face=黑体>停止推送Ecinx系统</font> \n\n";
                        //    MessageContent = MessageContent + $"北京时间 7月8日 18:00 ~  7月11日 18:00 停推！";
                        //    _dingdingNotificationJob.SendEcinxERPAlert("魔法灯-停止推送Ecinx系统", MessageContent);
                        //    //CLSLogger.Info("义乌保税仓(暂停服务 9月3日 周五中午12点-9月6日 周五中午12),返回值为空:", $"包裹号：{orderPack.orderPackNo}  inputDto: {inputDto.ToJsonString()} listProductsFromOrder: {listProductsFromOrder.ToJsonString()} listProductInfo: {listProductInfo.ToJsonString()}", LogTags);
                        //    return _return;
                        //}


                        string orderpackSQL = $@"  with cte as (
                             select row_number() over(order by PayTime) as ID,OrderPackNo,Opid,customsPaymentData,CreateTime,OrderID,Status,DeliveryCode,ExportFlag,
                             EcinxResponse,OriginalInfo,paytime,EcinxOrderCode
                             from EcinxOrdersPackTrade where Status = 0 and ExportFlag in (104,105,106,107)
                            ),
                            cteresult as( 
                             select *,NTILE(24) over(order by id )-1 as rowid
                             from cte
                            )
                            select OrderPackNo,Opid,CustomsPaymentData,CreateTime,OrderID,Status,DeliveryCode,ExportFlag,EcinxResponse,OriginalInfo,PayTime,EcinxOrderCode
                            from cteresult where OriginalInfo like '%{ResponseDTO.goodsCode}%'
                            order by PayTime asc
                            ";
                        //var PMSDbContext = new PMSDbContextFactory().CreateDbContext();
                        using (var PMSDbContext = new PMSDbContextFactory().CreateDbContext())
                        {
                            //List<Ecinxorderpack> orderpacklist = PMSDbContext.EcinxOrdersPackTrades.FromSql(orderpackSQL)
                            //                .MapTo<List<Ecinxorderpack>>()
                            //                .ToList();
                            List<Ecinxorderpack> orderpacklist = PMSDbContext.EcinxOrdersPackTrades
                                                                   .FromSql(orderpackSQL)
                                                                   .Select(x => new Ecinxorderpack
                                                                   {
                                                                       OrderPackNo = x.OrderPackNo,
                                                                       Opid = x.Opid,
                                                                       CustomsPaymentData = x.CustomsPaymentData,
                                                                       CreateTime = x.CreateTime,
                                                                       OrderID = x.OrderID,
                                                                       Status = x.Status,
                                                                       DeliveryCode = x.DeliveryCode,
                                                                       ExportFlag = x.ExportFlag,
                                                                       EcinxResponse = x.EcinxResponse,
                                                                       OriginalInfo = x.OriginalInfo,
                                                                       PayTime = x.PayTime,
                                                                       EcinxOrderCode = x.EcinxOrderCode
                                                                   })
                                                                   .ToList();
                            //var stringorderpakno = string.Join("|", orderpacklist.Select(x => x.OrderPackNo).ToArray());
                            if (orderpacklist.Count > 0)
                            {
                                string MessageContent = $"<font color=#FF0000  face=黑体>新库存分配: 先支付先分配原则 </font> \n\n";
                                MessageContent = MessageContent + $" ##### **库存分配结论**: SKU:{ResponseDTO.goodsCode} 可销售数量: {totalinventory} 本次同步参与库存分配，根据分配原则, 参与分配此SKU的缺货订单数量：{orderpacklist.Count}    \n\n";
                                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx库存分配", MessageContent);
                                BondWarehousePackaged(orderpacklist, ResponseDTO);
                            }
                        }


                        SyncEcinxStockToERP("WHYC", ResponseDTO.goodsCode, ResponseDTO.barCode);
                        return _return;
                    }

                }


            }
            catch (Exception ex)
            {
                string MessageContent = $"Ecinx库存查询失败,返回值:{Response.ToJsonString()}";
                //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx库存查询失败", MessageContent);
                CLSLogger.Info("whyc_queryex", $"goodcode: {ResponseDTO.goodsCode} ex: {ex.ToJsonString()} canSalesNum:{result.ToJsonString()} ", clogTags);
                _return = "failed";

            }

            return _return;
        }



        private void BondWarehousePackaged(List<Ecinxorderpack> orderpacnolist, EcinxStockChange ecinxstock)
        {
            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();

            Dictionary<string, string> LogTags = new Dictionary<string, string>
            {
                ["jobid"] = Guid.NewGuid().ToString(),
                ["method"] = "SyncCreateEcinxOder"
            };

           
            foreach (var orderpacknoObject in orderpacnolist)
            {

                try
                {
 
                    _pmsContext.Database.SetCommandTimeout(120);

                    var orderPackTradeEntity = _pmsContext.EcinxOrdersPackTrades
                        .FirstOrDefault(x => new[] { x.OrderPackNo, x.OrderID }
                                             .Contains(orderpacknoObject.OrderPackNo));

                    if (orderPackTradeEntity == null)
                        return;

                    var input = orderPackTradeEntity.OriginalInfo.ConvertFromJsonString<EcinxOrderInput>();


                    string warehousecode = "", warehouseName = "";

                    EcinxConfigerData config = EcinxConfigerData.Configuration;
                    ExportResponse _ecinxconfig = config.GetExportByFlag(orderPackTradeEntity.ExportFlag ?? 0);
                    warehousecode = _ecinxconfig.Export.WarehouseCode;
                    warehouseName = _ecinxconfig.Export.WarehouseName;
                    //判断是否使用供销功能 2025.02.19
                    bool supplySales = config.supplySalesExportFlag.Contains(orderPackTradeEntity.ExportFlag ?? 0);

                    int differenceInDays = 0;
                    DateTime oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                    if (!string.IsNullOrWhiteSpace(orderpacknoObject.PayTime.ToString()))
                    {
                        oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(orderpacknoObject.PayTime.ToString()), timeZoneNZ);
                        DateTime newDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                        TimeSpan ts = newDate - oldDate;
                        differenceInDays = ts.Days;
                    }


                   string MessageContent = $"<font color=#FF0000  face=黑体>新库存分配: 先支付先分配原则(分配失败) </font> \n\n";
                    MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {orderpacknoObject.OrderPackNo}  \n\n";
                    MessageContent += $" ##### **付款时间**: {(orderpacknoObject.PayTime.HasValue ? orderpacknoObject.PayTime.Value.ToString("yyyy-MM-dd HH:mm:ss") : "未知")}  \n\n";
                    MessageContent = MessageContent + $" ##### **归属仓**:  {warehouseName}  \n\n";
                    MessageContent = MessageContent + $"**缺货天数**: **_<font face=华云彩绘 color=#FF0000> {differenceInDays} </font>_**  \n\n";


                   

                    EcinxOrder eorder = new EcinxOrder();

                    eorder.deduction = input.deduction;
                    eorder.discount = input.discount;
                    eorder.freight = input.freight;
                    eorder.orderTime = input.orderTime;
                    eorder.platformOrderCode = input.platformOrderCode;
                    eorder.declareOrderCode = input.declareOrderCode;

                    //// 阿狸： CQ奶粉普通线 82,106 渠道，指定Ecinx仓库，其它不指定Ecinx 仓库  2024.11.22
                    List<int> mfdyc_export = new List<int> { 82, 106 };//82 MFD //106 YC
                    eorder.warehouseCode = mfdyc_export.Contains(orderPackTradeEntity.ExportFlag ?? 0) ? warehousecode : ""; 

                    eorder.receiver = input.receiver;
                    eorder.receiverTel = input.receiverTel;
                    eorder.receiverProvince = input.receiverProvince;
                    eorder.receiverCity = input.receiverCity;
                    eorder.receiverArea = input.receiverArea;
                    eorder.receiverAddress = input.receiverAddress;
                    eorder.tax = input.tax;
                    eorder.type = input.type;


                    eorder.details = input.details;
                    eorder.customsPlatformCode = input.customsPlatformCode;
                    eorder.customsPlatformName = input.customsPlatformName;

                    eorder.purchaser = input.purchaser;
                    eorder.purchaserCardNo = input.purchaserCardNo;
                    eorder.purchaserCardType = input.purchaserCardType;
                    eorder.purchaserTel = input.purchaserTel;
                    //提交供销代码
                    if (supplySales)
                    {
                        eorder.providerCode = _ecinxconfig.Export.providerCode;
                        eorder.type = 2;
                    }
                    eorder.payments = input.payments;
                    eorder.expressSheetInfo = input.expressSheetInfo;
                    eorder.presell = input.presell;
                    eorder.planDeliveryTime = input.planDeliveryTime;

                    eorder.purchaserCardFrontPic = input.GetType().GetProperty("purchaserCardFrontPic") != null ? input.purchaserCardFrontPic : "";
                    eorder.purchaserCardBackPic = input.GetType().GetProperty("purchaserCardBackPic") != null ? input.purchaserCardBackPic : "";

                    eorder.ExportFlag = orderpacknoObject.ExportFlag;


                    EcinxResponse result = null;

                    EcinxClientV2 ecinxv2 = new EcinxClientV2();
                    result = ecinxv2.CreateOrderV2(orderPackTradeEntity.ExportFlag ?? 0, eorder).Result;


                    if (!result.success)
                    {
                        MessageContent = MessageContent + $" ##### **创建订单失败原因**:  **_<font face=华云彩绘 color=#FF0000> {result.errorMsg} </font>_** \n\n";
                        //禁用 24.10.2024
                        _dingdingNotificationJob.SendEcinxERPAlert("Ecinx 创建订单失败", MessageContent);
                        CLSLogger.Info("Ecinx创建订单失败", $"包裹号：{orderpacknoObject.OrderPackNo}  失败详情:{result.ToJsonString()} ", LogTags);

                        ecinpushorderLog.OrderPackNo = "N/A";
                        ecinpushorderLog.WarningLevel = 0;  //WarningLevel =0: 普通告警; WarningLevel =10: 需要客服处理;
                        ecinpushorderLog.LackStockDays = -1;
                        ecinpushorderLog.MessageType = "库存变化回调";
                        ecinpushorderLog.FaildReason = MessageContent;
                        ecinpushorderLog.MessageDetail = "";
                        _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                        _pmsContext.SaveChanges();


                        continue;
                    }
                    else
                    {
                        MessageContent = $"<font color=#228b22  face=黑体>新库存分配: 先支付先分配原则(分配成功) </font> \n\n";
                        MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {orderpacknoObject.OrderPackNo}  \n\n";
                        MessageContent += $" ##### **付款时间**: {(orderpacknoObject.PayTime.HasValue ? orderpacknoObject.PayTime.Value.ToString("yyyy-MM-dd HH:mm:ss") : "未知")}  \n\n";
                        MessageContent = MessageContent + $" ##### **归属仓**:  {warehouseName}  \n\n";
                        MessageContent = MessageContent + $" ##### **分配的SKU**:  {ecinxstock.goodsCode}  \n\n";
                        MessageContent = MessageContent + $"**未处理天数**: **_<font face=华云彩绘 color=#FF0000> {differenceInDays} </font>_**  \n\n";
                        //_dingdingNotificationJob.SendEcinxERPAlert("Ecinx 分配缺货订单成功", MessageContent);
                    }

                }
                catch (Exception ex)
                {
                    CLSLogger.Error("Ecinx下单发生异常 Exception ",  "异常：" + ex.Message, LogTags);
                    throw new Exception($"Ecinx查询接口失败，订单号：{ex.Message}");
                }

            }
        }


        public PushOrdersOutputDTO RepushListOfOrderPackNo(List<string> orderpacknolist)
        {

            PushOrdersOutputDTO output = new PushOrdersOutputDTO();
           

            EcinxPushOrderLogEntity ecinpushorderLog = new EcinxPushOrderLogEntity();

            Dictionary<string, string> LogTags = new Dictionary<string, string>
            {
                ["jobid"] = Guid.NewGuid().ToString(),
                ["method"] = "EnforceCreateEcinxOder"
            };

            List<EcinxOrdersPackTradeEntity> listOrderPacknoTrade = _EcinxOrdersPackTradeRepository.GetAll().Where(x => orderpacknolist.Contains(x.OrderPackNo)).ToList();

            List<string> listOrderpackNoFound = listOrderPacknoTrade.Select(x => x.OrderPackNo).ToList();
            List<string> listOrderpacknoNotFound = orderpacknolist.Except(listOrderpackNoFound).ToList();

            foreach (var item in listOrderPacknoTrade)
            {
                try
                {
                    var input = item.OriginalInfo.ConvertFromJsonString<EcinxOrderInput>();

                    OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x => x.orderPackNo == item.OrderPackNo || x.orderID == item.OrderPackNo);
                    
                    if (orderPack == null)
                    {
                        continue;
                    }

                    if (orderPack.freezeFlag > 0)
                    {
                        string message = $"手工推送, 包裹已冻结：{orderPack.orderPackNo} freezeflag: {orderPack.freezeFlag} {orderPack.freezeComment} ";
                        _dingdingNotificationJob.SendEcinxERPAlert("Ecinx 创建订单失败", message);
                        CLSLogger.Info("手工推送", $"包裹已冻结：{orderPack.orderPackNo}  freezeflag: {orderPack.freezeFlag}  {orderPack.freezeComment}", LogTags);

                        continue;
                    }

                    int differenceInDays = 0;
                    DateTime oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                    if (!string.IsNullOrWhiteSpace(orderPack.opt_time.ToString()))
                    {
                        oldDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Parse(orderPack.opt_time.ToString()), timeZoneNZ);
                        DateTime newDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZoneNZ);
                        TimeSpan ts = newDate - oldDate;
                        differenceInDays = ts.Days;
                    }


                    string warehousecode = "", warehouseName = "";

                    EcinxConfigerData config = EcinxConfigerData.Configuration;
                    ExportResponse _ecinxconfig = config.GetExportByFlag(item.ExportFlag ?? 0);
                    warehousecode = _ecinxconfig.Export.WarehouseCode;
                    warehouseName = _ecinxconfig.Export.WarehouseName;
                    //是否使用供销功能 19/02/2025
                    bool supplySales = config.supplySalesExportFlag.Contains(item.ExportFlag ?? 0);

                    string MessageContent = $"<font color=#FF0000  face=黑体>强制推送Ecinx系统</font> \n\n";
                    MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {item.OrderPackNo}  \n\n";
                    MessageContent = MessageContent + $" ##### **付款时间**:  {item.PayTime.ToString("yyyy-MM-dd HH:mm:ss")}  \n\n";
                    MessageContent = MessageContent + $" ##### **归属仓**:  {warehouseName}  \n\n";
                    MessageContent = MessageContent + $"**缺货天数**: **_<font face=华云彩绘 color=#FF0000> {differenceInDays} </font>_**  \n\n";

                    EcinxOrder eorder = new EcinxOrder();

                    eorder.deduction = input.deduction;
                    eorder.discount = input.discount;
                    eorder.freight = input.freight;
                    eorder.orderTime = input.orderTime;
                    eorder.platformOrderCode = input.platformOrderCode;
                    eorder.declareOrderCode = input.declareOrderCode;

                    //// 阿狸： CQ奶粉普通线 82,106 渠道，指定Ecinx仓库，其它不指定Ecinx 仓库  2024.11.22
                    List<int> mfdyc_export = new List<int> { 82, 106 };//82 MFD //106 YC
                    eorder.warehouseCode = mfdyc_export.Contains(item.ExportFlag ?? 0) ? warehousecode : "";


                    eorder.receiver = input.receiver;
                    eorder.receiverTel = input.receiverTel;
                    eorder.receiverProvince = input.receiverProvince;
                    eorder.receiverCity = input.receiverCity;
                    eorder.receiverArea = input.receiverArea;
                    eorder.receiverAddress = input.receiverAddress;
                    eorder.tax = input.tax;
                    eorder.type = input.type;
                    //提交供销代码
                    if (supplySales)
                    {
                        eorder.providerCode = _ecinxconfig.Export.providerCode;
                        eorder.type = 2;
                    }
                    eorder.details = input.details;
                    eorder.customsPlatformCode = input.customsPlatformCode;
                    eorder.customsPlatformName = input.customsPlatformName;

                    eorder.purchaser = input.purchaser;
                    eorder.purchaserCardNo = input.purchaserCardNo;
                    eorder.purchaserCardType = input.purchaserCardType;
                    eorder.purchaserTel = input.purchaserTel;

                    eorder.payments = input.payments;
                    eorder.expressSheetInfo = input.expressSheetInfo;
                    eorder.presell = input.presell;
                    eorder.planDeliveryTime = input.planDeliveryTime;

                    eorder.purchaserCardFrontPic = input.GetType().GetProperty("purchaserCardFrontPic") != null ? input.purchaserCardFrontPic : "";
                    eorder.purchaserCardBackPic = input.GetType().GetProperty("purchaserCardBackPic") != null ? input.purchaserCardBackPic : "";

                    eorder.ExportFlag = item.ExportFlag;


                    EcinxResponse result = null;

                    EcinxClientV2 ecinxv2 = new EcinxClientV2();
                    result = ecinxv2.CreateOrderV2(item.ExportFlag ?? 0, eorder).Result;


                    if (!result.success)
                    {
                        MessageContent = MessageContent + $" ##### **创建订单失败原因**:  **_<font face=华云彩绘 color=#FF0000> {result.errorMsg} </font>_** \n\n";
                        _dingdingNotificationJob.SendEcinxERPAlert("强制推送Ecinx系统", MessageContent);
                        CLSLogger.Info("强制推送Ecinx系统", $"包裹号：{item.OrderPackNo}  失败详情:{result.ToJsonString()} ", LogTags);

                        ecinpushorderLog.OrderPackNo = item.OrderPackNo;
                        ecinpushorderLog.WarningLevel = 0;  //WarningLevel = 0: 普通告警; WarningLevel =10: 需要客服处理;
                        ecinpushorderLog.LackStockDays = differenceInDays;
                        ecinpushorderLog.MessageType = "强制推送Ecinx系统";
                        ecinpushorderLog.FaildReason = MessageContent;
                        ecinpushorderLog.MessageDetail = "";
                        _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                        _pmsContext.SaveChanges();

                        continue;
                    }
                    else
                    {
                        MessageContent = $"<font color=#228b22  face=黑体>强制推送Ecinx系统</font> \n\n";
                        MessageContent = MessageContent + $" ##### **订单号或包裹号**:  {item.OrderPackNo}  \n\n";
                        MessageContent = MessageContent + $" ##### **付款时间**:  {item.PayTime.ToString("yyyy-MM-dd HH:mm:ss")}  \n\n";
                        MessageContent = MessageContent + $" ##### **归属仓**:  {warehouseName}  \n\n";
                        MessageContent = MessageContent + $"**缺货天数**: **_<font face=华云彩绘 color=#FF0000> {differenceInDays} </font>_**  \n\n";
                        _dingdingNotificationJob.SendEcinxERPAlert("强制推送Ecinx系统", MessageContent);

                        ecinpushorderLog.OrderPackNo = item.OrderPackNo;
                        ecinpushorderLog.WarningLevel = 0;  //WarningLevel = 0: 普通告警; WarningLevel =10: 需要客服处理;
                        ecinpushorderLog.LackStockDays = differenceInDays;
                        ecinpushorderLog.MessageType = "强制推送Ecinx系统";
                        ecinpushorderLog.FaildReason = MessageContent;
                        ecinpushorderLog.MessageDetail = "";
                        _pmsContext.EcinxPushOrderLog.Add(ecinpushorderLog);
                        _pmsContext.SaveChanges();
                    }
                    //_backgroundJobManager.Enqueue<ECinxCreateOrderJob, EcinxOrderInput>(input, BackgroundJobPriority.Normal);
                }
                catch (Exception ex)
                {
                    Dictionary<string, string> clogTags = new Dictionary<string, string>
                    {
                        ["type"] = ClogTagType,
                        ["orderid"] = item.OrderID
                    };

                    CLSLogger.Error("重推Ecinx失败", ex, clogTags);

                    output.Success = false;
                    output.PushedOrdersCount = listOrderPacknoTrade.Count();
                    output.IdsNotFound = new List<string> { ex.Message };
                    return output;
                }
            }
            output.Success = true;
            output.PushedOrdersCount = listOrderPacknoTrade.Count();
            output.IdsNotFound = listOrderpacknoNotFound;
            return output;

        }


        // Update Orderpack Status = 0
        [DontWrapResult]
        public APIresult ChangeOrderPackStatus(string OrderPackNO)
        {
            APIresult result = new APIresult();

            try
            {

                OrderPackEntity orderPack = _heerpContext.OrderPacks.FirstOrDefault(x => x.orderPackNo == OrderPackNO);
                if (orderPack == null )
                {
                    result.Code = 201;
                    result.Msg = $"Error, OrderPackNo is not exist, orderPackNo：{OrderPackNO}";
                    return result;
                }

                else
                {
                    orderPack.status = 0; //Ecinx已配货
                    _heerpContext.OrderPacks.Update(orderPack);
                    _heerpContext.SaveChanges();

                    result.Code = 200;
                    result.Msg = $"Success";
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.Code = 203;
                result.Msg = ex.Message.ToJsonString();
                return result;
            }

        }


        public class APIresult
        {
            public int Code { set; get; }
            public string Msg { set; get; }

        }
        public class Ecinxorderpack
        {
            public int? Opid { get; set; }
            public string OrderPackNo { get; set; }
            public string CustomsPaymentData { get; set; }
            public DateTime CreateTime { get; set; }
            public string OrderID { get; set; }
            public int Status { get; set; }
            public string DeliveryCode { get; set; }
            public int? ExportFlag { get; set; }
            public string EcinxResponse { get; set; }
            public string OriginalInfo { get; set; }
            public DateTime? PayTime { get; set; }
            public string EcinxOrderCode { get; set; }
        }


        /// <summary>
        /// 订单创建成功 + 订单出库返回参数，返回消息 
        /// </summary>
        public class OrderAddOrOutnotifyResponse
        {
            public string result { get; set; }
            public string errorMsg { get; set; }
            public string platformOrderCode { get; set; }
            public string appKey { get; set; }
            public string declareOrderCode { get; set; }
            public string notifyUrl { get; set; }
            public string orderCode { get; set; }
            public string sign { get; set; }

            public string logisticsCode { get; set; }
            public string deliveryCode { get; set; }
            public string distributionBillCode { get; set; }
            public string expressCode { get; set; }
        }
        public class DeliverySendOutnotifyResponse
        {
            public string appkey { get; set; }
            public string logisticsCode { get; set; }
            public string deliveryCode { get; set; }
            public string declareOrderCode { get; set; }
            public string distributionBillCode { get; set; }
            public string expressCode { get; set; }
            public string sign { get; set; }
        }
        public class ReturnDeliverySendOut
        {
            public string code { get; set; }
            public string msg { get; set; }
        }
        public class SendOrdernotifyResponse {
            public string appkey { get; set; }
            public string changeType { get; set; }
            public string orderCode { get; set; }
            public string platformOrderCode { get; set; }

        }


        public class ChangeOrderNotificationResponse
        {
            public string appkey { get; set; }
            public int changeType { get; set; }
            public string orderCode { get; set; }
            public string platformOrderCode { get; set; }
            public string sign { get; set; }
        }


        public class EcinxResponseInfo
        {
            public int code { set; get; }
            public string message { set; get; }
            public string data { set; get; }

        }

        public class EcinxStockChange
        {
            public string appkey { get; set; }
            public string warehouseCode { get; set; }
            public string warehouseName { get; set; }
            public string goodsCode { get; set; }
            public string barCode { get; set; }
            public string goodsName { get; set; }
            public int changeType { get; set; }
            public int beforeNum { get; set; }
            public int changeNum { get; set; }
            public int afterNum { get; set; }
            public string changeTime { get; set; }
            public string sign { get; set; }
        }


    }
}
