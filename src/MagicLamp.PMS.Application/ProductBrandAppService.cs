﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.UI;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.DTOs.Enums;
using MagicLamp.PMS.DTOs.MqDTO;
using MagicLamp.PMS.Entities;
using MagicLamp.PMS.Infrastructure;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Proxy;
using MagicLamp.XERP.BackgroundJob;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.AspNetCore.Http;
using MagicLamp.PMS.Domain.ValueObjects;
using MagicLamp.PMS.Enums;
using MagicLamp.PMS.Domain;
using System.Threading.Tasks;
using OdooRpc.CoreCLR.Client.Models;
using OdooRpc.CoreCLR.Client.Models.Parameters;
using Newtonsoft.Json.Linq;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using MagicLamp.PMS.Brands.Dto;
using FluentValidation.Results;
using EFCore.BulkExtensions;
using Abp.EntityFrameworkCore;
using MagicLamp.PMS.EntityFrameworkCore;
using Abp.Extensions;

namespace MagicLamp.PMS.Brands
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class ProductBrandAppService : PMSCrudAppServiceBase<BrandEntity, BrandDTO, BrandFilterDTO, int>
    {

        private IRepository<BrandEntity, int> _brandRepository;
        private IRepository<BrandCategoryEntity, int> _brandCategoryRepository;
        private IRepository<ImageEntity, long> imageRepository;
        private IRepository<FileEntity, long> fileRepository;
        private IBackgroundJobManager backgroundJobManager;
        private IAccountManager accountManager;
        private OdooProxy _odooProxy;
        private IDbContextProvider<PMSDbContext> _pmsDbContextProvider;

        public ProductBrandAppService(IRepository<BrandEntity, int> brandRepository,
            IRepository<BrandCategoryEntity, int> brandCategoryRepository,
            IRepository<ImageEntity, long> _imageRepository,
            IRepository<FileEntity, long> _fileRepository,
            IBackgroundJobManager _backgroundJobManager,
            IAccountManager _accountManager,
            IDbContextProvider<PMSDbContext> pmsDbContextProvider,
            OdooProxy odooProxy
            ) : base(brandRepository)
        {
            _brandRepository = brandRepository;
            _brandCategoryRepository = brandCategoryRepository;
            imageRepository = _imageRepository;
            fileRepository = _fileRepository;
            backgroundJobManager = _backgroundJobManager;
            accountManager = _accountManager;
            _odooProxy = odooProxy;
            _pmsDbContextProvider = pmsDbContextProvider;
        }
        public override PagedQueryOutputBaseDTO<BrandDTO> GetList(PagedQueryInputBaseDTO<BrandFilterDTO> input)
        {
            IQueryable<BrandEntity> query = base.BuildQuery(input)
                            .Include(x => x.BrandCategory);
            return base.PaginateQuery(query, input);
        }

        public override BrandDTO Create(BrandDTO input)
        {
            var existEntity = _brandRepository.FirstOrDefault(x => x.ShortCode == input.ShortCode || x.EnglishName == input.EnBrand || x.ChineseName == input.CnBrand);

            if (existEntity != null)
            {
                StringBuilder errorMsg = new StringBuilder();

                if (existEntity.ShortCode == input.ShortCode)
                {
                    errorMsg.Append($"短码 {input.ShortCode} 已经存在，无法重复创建");
                }
                if (existEntity.EnglishName == input.EnBrand)
                {
                    errorMsg.Append($"英文名称 {input.EnBrand} 已经存在，无法重复创建");
                }
                if (existEntity.ChineseName == input.CnBrand)
                {
                    errorMsg.Append($"中文名称 {input.CnBrand} 已经存在，无法重复创建");
                }
                if (errorMsg.Length > 0)
                {
                    throw new UserFriendlyException(errorMsg.ToString());
                }
            }

            input.CreateTime = DateTime.UtcNow;

            if (input.BrandCategoryId.HasValue)
            {
                input.BrandCategoryName = _brandCategoryRepository.FirstOrDefault(x => x.Id == input.BrandCategoryId)?.Name;
            }

            if (input.ActiveFlag == true)
            {
                try
                {
                    long odooProductBrandId = _odooProxy.CreateProductBrand(input.EnBrand, input.CnBrand, input.BrandCategoryName).GetAwaiter().GetResult();
                    input.OdooBrandId = odooProductBrandId;
                }
                catch (RpcCallException ex)
                {
                    CLSLogger.Error("创建odoo品牌失败", ex.RpcErrorData.ToString());
                    throw new UserFriendlyException("创建品牌失败，同步odoo发生异常，请稍后再尝试");
                }
            }

            var brandDto = base.Create(input);

            SetReferenceType(input);

            var images = new List<ImageDTO>();
            images.AddRange(input.Logos);
            images.AddRange(input.Banners);

            UpdateBrandImages(images, brandDto.Id);
            UpdateBrandFiles(input.AuthorizationFiles, brandDto.Id);

            BaseUpdateMqDTO<BrandDTO> mqDto = new BaseUpdateMqDTO<BrandDTO>();
            mqDto.After = input;
            mqDto.Action = MqActionEnum.Create;

            // backgroundJobManager.Enqueue<ProductBrandUpdateNotifyJob, BaseUpdateMqDTO<BrandDTO>>(mqDto);

            return brandDto;
        }

        public override BrandDTO Update(BrandDTO input)
        {
            StringBuilder errorMsg = new StringBuilder();

            try
            {
                BaseUpdateMqDTO<BrandDTO> mqDto = new BaseUpdateMqDTO<BrandDTO>();
                mqDto.Action = MqActionEnum.Update;

                var oldEntity = _brandRepository.FirstOrDefault(x => x.ShortCode == input.ShortCode);

                if (oldEntity == null)
                {
                    CLSLogger.Error("更新品牌发生错误", $"未找到：{input.ShortCode}，无法更新");
                    throw new UserFriendlyException($"未找到：{input.ShortCode}，无法更新");
                }
                else
                {

                    var existingBrands = _brandRepository.GetAll().ToList();

                    if (
                        input.ShortCode != oldEntity.ShortCode
                        && existingBrands.Any(x => x.ShortCode == input.ShortCode))
                    {
                        errorMsg.Append($"短码{input.ShortCode}已被使用，请确认没有重复上传或修改");
                    }
                    if (
                        input.EnBrand != oldEntity.EnglishName
                        && existingBrands.Any(x => x.EnglishName == input.EnBrand))
                    {
                        errorMsg.Append($"英文名称{input.EnBrand}已被使用，请确认没有重复上传或修改");
                    }
                    if (input.CnBrand != oldEntity.ChineseName
                        && existingBrands.Any(x => x.ChineseName == input.CnBrand))
                    {
                        errorMsg.Append($"中文名称{input.CnBrand}已被使用，请确认没有重复上传或修改");
                    }
                    if (input.BrandCategoryId.HasValue)
                    {
                        input.BrandCategoryName = _brandCategoryRepository.FirstOrDefault(x => x.Id == input.BrandCategoryId)?.Name;
                    }
                    if (errorMsg.Length > 0)
                    {
                        throw new UserFriendlyException(errorMsg.ToString());
                    }
                }

                if (input.ActiveFlag == true || (input.ActiveFlag != false && oldEntity.ActiveFlag == true))
                {
                    try
                    {
                        if (input.OdooBrandId == null)
                        {
                            long odooProductBrandId = _odooProxy.CreateProductBrand(input.EnBrand, input.CnBrand, input.BrandCategoryName).GetAwaiter().GetResult();
                            input.OdooBrandId = odooProductBrandId;
                        }
                        else
                        {
                            long odooBrandId = oldEntity.OdooBrandId ?? throw new Exception("并未关联odoo品牌，请关联后再更新");
                            _odooProxy.UpdateProductBrand(odooBrandId, input.EnBrand, input.CnBrand, input.BrandCategoryName).GetAwaiter().GetResult();
                        }
                    }
                    catch (RpcCallException ex)
                    {
                        CLSLogger.Error("odoo品牌更新失败", ex.RpcErrorData.ToString());
                        throw new UserFriendlyException($"品牌更新失败，同步odoo发生异常: {ex.Message}");
                    }
                }

                var images = GetImagesByID(input);
                var files = GetFilesByID(input);

                mqDto.Before = oldEntity.MapTo<BrandDTO>();
                mqDto.Before.Banners = images.Where(x => x.ReferenceType == BrandImageTypeEnum.Banner.ToString()).ToList();
                mqDto.Before.Logos = images.Where(x => x.ReferenceType == BrandImageTypeEnum.Logo.ToString()).ToList();
                mqDto.Before.AuthorizationFiles = files;

                input.MapTo(oldEntity);

                SetReferenceType(input);
                var newImages = input.Logos.Concat(input.Banners).ToList();
                UpdateBrandImages(newImages, input.Id);
                UpdateBrandFiles(input.AuthorizationFiles, input.Id);

                mqDto.After = input;

                // backgroundJobManager.Enqueue<ProductBrandUpdateNotifyJob, BaseUpdateMqDTO<BrandDTO>>(mqDto);

                return input;
            }
            catch (UserFriendlyException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("更新品牌发生错误", ex);
                throw new UserFriendlyException("更新品牌信息发生异常" + errorMsg, ex);
            }
        }

        [HttpPost]
        public bool BatchCreate(IFormFile file)
        {
            if (file.Length <= 0)
            {
                throw new UserFriendlyException("文件大小异常，请选择正确的文件");
            }

            var workbook = new HSSFWorkbook(file.OpenReadStream());
            StringBuilder errorMsg = new StringBuilder();
            var mapper = new Npoi.Mapper.Mapper(workbook);

            var uploadedBrands = mapper.Take<CreateBrandDto>("sheet1").ToList();

            if (uploadedBrands.IsNullOrEmpty())
            {
                throw new UserFriendlyException("excel中未找到有效数据，请确保excel模板正确且有数据");
            }

            var brands = _brandRepository.GetAll().ToList();
            var brandCategories = _brandCategoryRepository.GetAll().ToList();
            var type = typeof(CreateBrandDto);

            List<BrandEntity> brandEntities = new List<BrandEntity> { };
            CreateBrandDtoValidator validator = new CreateBrandDtoValidator();
            foreach (var rowInfo in uploadedBrands)
            {
                CreateBrandDto brand = rowInfo.Value;
                BrandEntity brandEntity = ObjectMapper.Map<BrandEntity>(brand);
                brandEntities.Add(brandEntity);
                ValidationResult validationResult = validator.Validate(brand);
                var rowNumber = rowInfo.RowNumber;
                foreach (var error in validationResult.Errors)
                {
                    errorMsg.Append($"第{rowNumber}行{error.ErrorMessage}\r\n");
                }
                if (brands.FirstOrDefault(x => x.ShortCode == brand.ShortCode) != null)
                {
                    var displayValue = type.GetDisplayAttributeValue(nameof(brand.ShortCode));
                    errorMsg.Append($"第{rowNumber}行{displayValue}已被使用，不能重复\r\n");
                }
                if (brands.FirstOrDefault(x => x.ChineseName == brand.ChineseName) != null)
                {
                    var displayValue = type.GetDisplayAttributeValue(nameof(brand.ChineseName));
                    errorMsg.Append($"第{rowNumber}行{displayValue}已被使用，不能重复\r\n");
                }
                if (brands.FirstOrDefault(x => x.EnglishName == brand.EnglishName) != null)
                {
                    var displayValue = type.GetDisplayAttributeValue(nameof(brand.EnglishName));
                    errorMsg.Append($"第{rowNumber}行{displayValue}已被使用，不能重复\r\n");
                }
                if (brand.BrandCategory != null)
                {
                    var brandCategory = brandCategories.FirstOrDefault(x => x.Name == brand.BrandCategory);
                    if (brandCategory != null)
                    {
                        brandEntity.BrandCategoryId = brandCategory.Id;
                    }
                    else
                    {
                        var displayValue = type.GetDisplayAttributeValue(nameof(brand.BrandCategory));
                        errorMsg.Append($"第{rowNumber}行{displayValue}并未被找到，请修改\r\n");
                    }
                }
            }

            if (errorMsg.Length > 0)
            {
                throw new UserFriendlyException(errorMsg.ToString());
            }

            // List<BrandEntity> brandEntities = ObjectMapper.Map<List<BrandEntity>>(uploadedBrands.Select(x => x.Value).ToList());
            brandEntities.ForEach(x =>
            {
                if (x.ActiveFlag == true)
                {
                    x.OdooSyncStatus = OdooSyncStatusEnum.Pending.ToString();
                }
            });

            try
            {
                var pmsDbContext = _pmsDbContextProvider.GetDbContext();
                pmsDbContext.BulkInsert(brandEntities, new BulkConfig
                {
                    PropertiesToExclude = new List<string> { nameof(BrandEntity.Id) },
                });
            }
            catch (Exception ex)
            {
                CLSLogger.ErrorWithSentry("导入excel发生异常", ex);
                throw new UserFriendlyException("导入excel发生异常");
            }

            backgroundJobManager.Enqueue<CreateOrUpdateOdooBrand, List<int>>(new List<int> { });

            return true;
        }

        [HttpPost]
        public bool BatchUpdate(IFormFile file)
        {
            if (file.Length <= 0)
            {
                throw new UserFriendlyException("文件大小异常，请选择正确的文件");
            }

            try
            {
                var workbook = new HSSFWorkbook(file.OpenReadStream());

                StringBuilder errorMsg = new StringBuilder();

                var mapper = new Npoi.Mapper.Mapper(workbook);

                var uploadedBrands = mapper.Take<UpdateBrandDto>("sheet1").ToList();

                if (uploadedBrands.IsNullOrEmpty())
                {
                    throw new UserFriendlyException("excel中未找到有效数据，请确保excel模板正确且有数据");
                }

                var existingBrands = _brandRepository.GetAll().ToList();
                var brandCategories = _brandCategoryRepository.GetAll().ToList();

                List<int> brandIds = new List<int> { };

                foreach (var rowInfo in uploadedBrands)
                {
                    var brand = rowInfo.Value;
                    if (brand.ShortCode == null)
                    {
                        errorMsg.Append($"第{rowInfo.RowNumber}行品牌的短码并未被填上，请补充参数后再上传\r\n");
                    }

                    var brandEntity = existingBrands.FirstOrDefault(x => x.ShortCode == brand.ShortCode);

                    if (!brand.ShortCode.IsNullOrWhiteSpace() && existingBrands.Any(x => x.ShortCode.ToLower() == brand.ShortCode?.ToLower() && x.ShortCode != brand.ShortCode))
                    {
                        errorMsg.Append($"第{rowInfo.RowNumber}行品牌{brand.ShortCode}已被使用，请确认没有重复上传或修改\r\n");
                    }
                    if (brandEntity == null)
                    {
                        errorMsg.Append($"第{rowInfo.RowNumber}行品牌{brand.ShortCode}并未被找到，请确认该品牌短码输入正确\r\n");
                    }
                    if (!brand.EnglishName.IsNullOrWhiteSpace() && existingBrands.Any(x => x.EnglishName.ToLower() == brand.EnglishName?.ToLower() && x.ShortCode != brand.ShortCode))
                    {
                        errorMsg.Append($"第{rowInfo.RowNumber}行品牌的英文名称{brand.EnglishName}已被使用，请确认没有重复上传或修改\r\n");
                    }
                    if (!brand.ChineseName.IsNullOrWhiteSpace() && existingBrands.Any(x => x.ChineseName.ToLower() == brand.ChineseName?.ToLower() && x.ShortCode != brand.ShortCode))
                    {
                        errorMsg.Append($"第{rowInfo.RowNumber}行品牌的中文名称{brand.ChineseName}已被使用，请确认没有重复上传或修改\r\n");
                    }
                    if (!brand.BrandCategory.IsNullOrWhiteSpace() && !brandCategories.Any(x => x.Name == brand.BrandCategory))
                    {
                        errorMsg.Append($"第{rowInfo.RowNumber}行品牌的分级{brand.BrandCategory}未被找到，请确认输入正确\r\n");
                    }
                    else
                    {
                        brandEntity.BrandCategoryId = brandCategories.First(x => x.Name == brand.BrandCategory).Id;
                    }

                    ObjectMapper.Map<UpdateBrandDto, BrandEntity>(brand, brandEntity);

                    if (brandEntity.ActiveFlag == true)
                    {
                        brandEntity.OdooSyncStatus = OdooSyncStatusEnum.Pending.ToString();
                        brandIds.Add(brandEntity.Id);
                    }
                }

                if (errorMsg.Length > 0)
                {
                    errorMsg.Append($"上传的Excel中发现错误的参数，请修改或删除该品牌");
                    throw new Exception(errorMsg.ToString());
                }

                CurrentUnitOfWork.SaveChanges();

                backgroundJobManager.Enqueue<CreateOrUpdateOdooBrand, List<int>>(brandIds);
            }
            catch (Exception ex)
            {
                CLSLogger.Error("导入excel发生异常", ex);
                throw new UserFriendlyException("导入excel发生异常" + ex.Message);
            }
            return true;
        }

        /// <summary>
        /// Update Brand images 
        /// </summary>
        /// <param name="images"></param>
        /// <param name="sku"></param>
        private List<ImageDTO> UpdateBrandImages(List<ImageDTO> images, int id)
        {
            var oldImages = imageRepository.GetAll().Where(x => x.ReferenceId == id.ToString() &&
            x.ImageType == ImageTypeEnum.Brand.ToString()).ToList();

            if (images.IsNullOrEmpty() && oldImages.IsNullOrEmpty())
            {
                return null;
            }

            //删除or更新旧的
            foreach (var item in oldImages)
            {
                var currentImg = images.FirstOrDefault(x => x.ImageId == item.ImageId);
                if (currentImg == null)
                {
                    imageRepository.Delete(item.ImageId);
                }
                else
                {
                    item.Sort = currentImg.Sort;
                    item.OriginalPath = currentImg.OriginalPath;
                }
            }

            //新增的image(更新image的ReferenceId)
            List<long> imageIds = images.Where(x => x.ReferenceId.IsNullOrEmpty()).Select(x => x.ImageId).ToList();
            imageRepository.GetAll().Where(x => imageIds.Contains(x.ImageId)).ToList().ForEach(x =>
            {
                var currentImg = images.FirstOrDefault(y => y.ImageId == x.ImageId);
                x.Sort = currentImg.Sort;
                x.ReferenceType = currentImg.ReferenceType;
                x.ReferenceId = id.ToString();
                currentImg.ReferenceId = id.ToString();

                x.ImageType = ImageTypeEnum.Brand.ToString();
                currentImg.ImageType = x.ImageType;

                x.CreationTime = DateTime.UtcNow;
                currentImg.CreationTime = x.CreationTime;
            });
            return images;
        }


        /// <summary>
        /// Update Brand Files 
        /// </summary>
        /// <param name="files"></param>
        /// <param name="sku"></param>
        private List<FileDTO> UpdateBrandFiles(List<FileDTO> files, int id)
        {
            var oldFiles = fileRepository.GetAll().Where(x => x.ReferenceId == id.ToString() &&
            x.FileType == FileTypeEnum.Brand.ToString()).ToList();

            if (files.IsNullOrEmpty() && oldFiles.IsNullOrEmpty())
            {
                return null;
            }



            //删除or更新旧的
            foreach (var item in oldFiles)
            {
                var currentImg = files.FirstOrDefault(x => x.FileId == item.FileId);
                if (currentImg == null)
                {
                    fileRepository.Delete(item.FileId);
                }
                else
                {
                    item.Sort = currentImg.Sort;
                    item.OriginalPath = currentImg.OriginalPath;
                }
            }

            //新增的File(更新File的ReferenceId)
            List<long> FileIds = files.Where(x => x.ReferenceId.IsNullOrEmpty()).Select(x => x.FileId).ToList();
            fileRepository.GetAll().Where(x => FileIds.Contains(x.FileId)).ToList().ForEach(x =>
            {
                var currentFile = files.FirstOrDefault(y => y.FileId == x.FileId);
                x.Sort = currentFile.Sort;
                x.ReferenceType = currentFile.ReferenceType;
                x.ReferenceId = id.ToString();
                currentFile.ReferenceId = id.ToString();

                x.FileType = FileTypeEnum.Brand.ToString();
                currentFile.FileType = x.FileType;

                x.CreationTime = DateTime.UtcNow;
                currentFile.CreationTime = x.CreationTime;
            });
            return files;
        }

        private void SetReferenceType(BrandDTO input)
        {
            if (!input.Logos.IsNullOrEmpty())
            {
                input.Logos.ForEach(x => { x.ReferenceType = BrandImageTypeEnum.Logo.ToString(); });
            }
            if (!input.Banners.IsNullOrEmpty())
            {
                input.Banners.ForEach(x => { x.ReferenceType = BrandImageTypeEnum.Banner.ToString(); });
            }
            if (!input.AuthorizationFiles.IsNullOrEmpty())
            {
                input.AuthorizationFiles.ForEach(x => { x.ReferenceType = "AuthorizationFile"; });
            }
        }

        private List<ImageDTO> GetImagesByID(BrandDTO input)
        {

            var images = imageRepository.GetAll().Where(x => x.ReferenceId == input.Id.ToString() && x.ImageType == ImageTypeEnum.Brand.ToString()).ToList().MapTo<List<ImageDTO>>();
            return images;
        }

        private List<FileDTO> GetFilesByID(BrandDTO input)
        {
            var files = fileRepository.GetAll().Where(x => x.ReferenceId == input.Id.ToString() && x.FileType == FileTypeEnum.Brand.ToString()).ToList().MapTo<List<FileDTO>>();
            return files;
        }

        public FileContentResult GetTemplate()
        {
            HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
            ISheet sheet1 = hssfWorkbook.CreateSheet("Sheet1");
            IRow headerRow = sheet1.CreateRow(0);

            // Add headers to sheet
            List<string> headers = typeof(UpdateBrandDto).GetDisplayAttributeValues();

            for (var i = 0; i < headers.Count(); i++)
            {
                ICell cell = headerRow.CreateCell(i);
                cell.SetCellValue(headers[i]);
            }

            // Add data validation constraints to columns
            // TODO: write extension to get display name for specific property instead of hard coding string
            int indexOfBrandCategoryColumn = headers.IndexOf("分级");
            CellRangeAddressList brandCategoryRangeList = new CellRangeAddressList(1, int.MaxValue, indexOfBrandCategoryColumn, indexOfBrandCategoryColumn);
            string[] brandCategories = _brandCategoryRepository.GetAll().Select(x => x.Name).ToArray();
            DVConstraint brandCategoryDvConstraint = DVConstraint.CreateExplicitListConstraint(brandCategories);
            ((HSSFSheet)sheet1).AddValidationData(new HSSFDataValidation(brandCategoryRangeList, brandCategoryDvConstraint));

            int indexOfIsActiveFlagColumn = headers.IndexOf("激活");
            CellRangeAddressList isActiveFlagRangeList = new CellRangeAddressList(1, int.MaxValue, indexOfIsActiveFlagColumn, indexOfIsActiveFlagColumn);
            DVConstraint isActiveFlagDvConstraint = DVConstraint.CreateExplicitListConstraint(new string[] { "TRUE", "FALSE" });
            ((HSSFSheet)sheet1).AddValidationData(new HSSFDataValidation(isActiveFlagRangeList, isActiveFlagDvConstraint));

            MemoryStream memoryStream = new MemoryStream();
            hssfWorkbook.Write(memoryStream);
            var bytes = memoryStream.ToArray();
            FileContentResult result = new FileContentResult(bytes, "application/vnd.ms-excel");
            result.FileDownloadName = $"brand_template.xls";

            return result;
        }
        [HttpGet]
        public FileContentResult DownloadAll()
        {
            List<BrandEntity> brandEntities = _brandRepository.GetAll().Include(x => x.BrandCategory).ToList();
            List<ExportBrandDto> brandDtos = ObjectMapper.Map<List<ExportBrandDto>>(brandEntities);
            HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
            var npoiMapper = new Npoi.Mapper.Mapper(hssfWorkbook);
            npoiMapper.Put(brandDtos, "Sheet1");

            MemoryStream memoryStream = new MemoryStream();
            npoiMapper.Workbook.Write(memoryStream);
            var bytes = memoryStream.ToArray();
            FileContentResult result = new FileContentResult(bytes, "application/vnd.ms-excel");
            result.FileDownloadName = $"brands.xls";

            return result;
        }
    }
}

