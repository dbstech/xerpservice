﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using MagicLamp.PMS.DTOs;
using MagicLamp.PMS.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using MagicLamp.PMS.Infrastructure.Extensions;
using MagicLamp.PMS.Infrastructure;
using System.Linq;
using Abp.AutoMapper;
using Abp.Domain.Uow;
using MagicLamp.PMS.Proxy;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using MagicLamp.PMS.Domain;

namespace MagicLamp.PMS
{
    [RemoteService(IsEnabled = true, IsMetadataEnabled = true)]
    public class AgentProductOrderCostAppService : PMSCrudAppServiceBase<AgentProductOrderCostEntity, AgentProductOrderCostDTO, AgentProductOrderCostDTO, long>
    {
        private const string ClogTagType = "AgentProductOrderCost";
        private IRepository<AgentProductOrderCostEntity, long> agentProductOrderCostRepository;
        private IRepository<ProductEntity, string> productRepository;
        private IRepository<OrderPackEntity, int> orderPackRepository;
        private IRepository<OrderEntity, int> orderRepository;
        private IRepository<OrderPackDetailEntity, int> orderPackDetailRepository;
        private readonly IAccountManager accountManager;
        private IRepository<EditLogEntity, int> editLogRepository;
        private IRepository<CurrencyEntity, string> currencyRepository;

        public AgentProductOrderCostAppService(
            IRepository<AgentProductOrderCostEntity, long> _agentProductOrderCostRepository,
            IRepository<ProductEntity, string> _productRepository,
            IRepository<OrderPackEntity, int> _orderPackRepository,
            IRepository<OrderEntity, int> _ordeRepository,
            IRepository<OrderPackDetailEntity, int> _orderPackDetailRepository,
            IRepository<CurrencyEntity, string> _currencyRepository,
            IAccountManager _accountManager,
            IRepository<EditLogEntity, int> _editLogRepository) : base(_agentProductOrderCostRepository)
        {
            agentProductOrderCostRepository = _agentProductOrderCostRepository;
            productRepository = _productRepository;
            orderPackRepository = _orderPackRepository;
            orderRepository = _ordeRepository;
            orderPackDetailRepository = _orderPackDetailRepository;
            accountManager = _accountManager;
            editLogRepository = _editLogRepository;
            currencyRepository = _currencyRepository;
        }

        public override AgentProductOrderCostDTO Update(AgentProductOrderCostDTO input)
        {
            input.CreationTime = DateTime.UtcNow;   
            return base.Update(input);
        }

        private PagedQueryOutputBaseDTO<AgentProductOrderCostDTO> GetListForExportExcel(PagedQueryInputBaseDTO<AgentProductOrderCostDTO> inputDto, int limit)
        {
            try
            {
                PagedQueryOutputBaseDTO<AgentProductOrderCostDTO> result = new PagedQueryOutputBaseDTO<AgentProductOrderCostDTO>();

                var query = from item in agentProductOrderCostRepository.GetAll()
                            select item;

                if (inputDto.Filter == null)
                {
                    inputDto.Filter = new AgentProductOrderCostDTO();
                }

                if (!string.IsNullOrEmpty(inputDto.Filter.SKU))
                {
                    query = query.Where(x => x.SKU == inputDto.Filter.SKU);
                }
                if (!string.IsNullOrEmpty(inputDto.Filter.OrderPackNo))
                {
                    query = query.Where(x => x.OrderPackNo == inputDto.Filter.OrderPackNo);
                }
                //创建时间
                if (inputDto.Filter.CreationTimeBegin.HasValue)
                {
                    query = query.Where(x => x.CreationTime >= inputDto.Filter.CreationTimeBegin);
                }
                if (inputDto.Filter.CreationTimeEnd.HasValue)
                {
                    query = query.Where(x => x.CreationTime <= inputDto.Filter.CreationTimeEnd);
                }
                //下单时间
                if (inputDto.Filter.OrderTimeBegin.HasValue)
                {
                    query = query.Where(x => x.OrderTime >= inputDto.Filter.OrderTimeBegin);
                }
                if (inputDto.Filter.OrderTimeEnd.HasValue)
                {
                    query = query.Where(x => x.OrderTime <= inputDto.Filter.OrderTimeEnd);
                }

                result.Meta.TotalCount = query.Count();
                if (result.Meta.TotalCount > limit)
                {
                    throw new RequestValidationException($"结果条数大于{limit}，请缩小查询范围。");
                }
                result.Meta.PageIndex = inputDto.PageIndex;
                result.Meta.PageSize = inputDto.PageSize;
                var entites = query.PagedQuery(inputDto.PageIndex, inputDto.PageSize).ToList();

                result.Data = entites.MapTo<List<AgentProductOrderCostDTO>>();

                foreach (var item in result.Data)
                {
                    item.CreationTime = item.CreationTime.SpecifyKindInUTC();
                    item.OrderTime = item.OrderTime.SpecifyKindInUTC();
                    item.OutboundTime = item.OutboundTime?.SpecifyKindInUTC();
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new RequestValidationException(ex.Message);
            }
        }

        public override PagedQueryOutputBaseDTO<AgentProductOrderCostDTO> GetList(PagedQueryInputBaseDTO<AgentProductOrderCostDTO> inputDto)
        {
            try
            {
                PagedQueryOutputBaseDTO<AgentProductOrderCostDTO> result = new PagedQueryOutputBaseDTO<AgentProductOrderCostDTO>();

                var query = from item in agentProductOrderCostRepository.GetAll()
                            select item;

                if (inputDto.Filter == null)
                {
                    inputDto.Filter = new AgentProductOrderCostDTO();
                }

                if (!string.IsNullOrEmpty(inputDto.Filter.SKU))
                {
                    query = query.Where(x => x.SKU == inputDto.Filter.SKU);
                }
                if (!string.IsNullOrEmpty(inputDto.Filter.OrderPackNo))
                {
                    query = query.Where(x => x.OrderPackNo == inputDto.Filter.OrderPackNo);
                }
                //创建时间
                if (inputDto.Filter.CreationTimeBegin.HasValue)
                {
                    query = query.Where(x => x.CreationTime >= inputDto.Filter.CreationTimeBegin);
                }
                if (inputDto.Filter.CreationTimeEnd.HasValue)
                {
                    query = query.Where(x => x.CreationTime <= inputDto.Filter.CreationTimeEnd);
                }
                //下单时间
                if (inputDto.Filter.OrderTimeBegin.HasValue)
                {
                    query = query.Where(x => x.OrderTime >= inputDto.Filter.OrderTimeBegin);
                }
                if (inputDto.Filter.OrderTimeEnd.HasValue)
                {
                    query = query.Where(x => x.OrderTime <= inputDto.Filter.OrderTimeEnd);
                }

                result.Meta.TotalCount = query.Count();
                result.Meta.PageIndex = inputDto.PageIndex;
                result.Meta.PageSize = inputDto.PageSize;
                var entites = query.PagedQuery(inputDto.PageIndex, inputDto.PageSize).ToList();

                result.Data = entites.MapTo<List<AgentProductOrderCostDTO>>();

                foreach (var item in result.Data)
                {
                    item.CreationTime = item.CreationTime.SpecifyKindInUTC();
                    item.OrderTime = item.OrderTime.SpecifyKindInUTC();
                    item.OutboundTime = item.OutboundTime?.SpecifyKindInUTC();
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new RequestValidationException(ex.Message);
            }
        }

        [Authorize]
        public override ExcelImportOutputDTO ImportExcel(IFormFile file)
        {
            try
            {
                ExcelImportOutputDTO outputDTO = new ExcelImportOutputDTO();

                var dtos = ConvertDTOFromExcel(file);
                dtos = dtos.Where(x => !x.OrderPackNo.IsNullOrEmpty()).ToList();

                if (dtos.IsNullOrEmpty())
                {
                    throw new RequestValidationException("上传excel中未找到有效数据，请检查");
                }

                StringBuilder error = new StringBuilder();

                var groupedDto = dtos.GroupBy(x => new { x.Agency, x.OrderPackNo, x.SKU });
                foreach (var item in groupedDto)
                {
                    if (item.Count() > 1)
                    {
                        var element = item.FirstOrDefault();
                        error.Append($"excel中存在重复数据，请检查是否存在另一条记录有相同的供应商，包裹号，SKU。供应商：{element.Agency}，包裹号：{element.OrderPackNo}，SKU：{element.SKU}");
                    }
                }

                var currencies = currencyRepository.GetAll().Select(x => x.Code).ToList();

                int index = 2;
                foreach (var item in dtos)
                {
                    if (item.SKU.IsNullOrEmpty() || item.Qty == 0 || item.PurchasingCurrency.IsNullOrEmpty())
                    {
                        error.Append($"第{index}行信息有误，请检查SKU，数量，采购货币是否正确；\n\r");
                    }

                    if (item.OrderPackNo.IndexOf('-')<=0)
                    {
                        error.Append($"第{index}行信息有误，请检查OrderPackNo否正确；\n\r");
                    }

                    if (!item.PurchasingCurrency.IsNullOrEmpty() && !currencies.Contains(item.PurchasingCurrency))
                    {
                        error.Append($"第{index}行信息有误，采购货币'{item.PurchasingCurrency}'并不存在，请检查采购货币是否正确；\n\r");
                    }
                    index++;
                }

                if (error.Length > 0)
                {
                    throw new RequestValidationException(error.ToString());
                }

                var orderPackNos = dtos.Select(x => x.OrderPackNo).Distinct().ToList();
                var agentOrdes = agentProductOrderCostRepository.GetAll().Where(x => orderPackNos.Contains(x.OrderPackNo)).ToList();
                var orderPackInfos = orderPackRepository.GetAll().Where(x => orderPackNos.Contains(x.orderPackNo)).ToList();
                var listOPID = orderPackInfos.Select(x => x.opID).ToList();
                var listOrderPackDetail = orderPackDetailRepository.GetAll().Where(x => listOPID.Contains(x.opid.Value)).ToList();

                /*if (!agentOrdes.IsNullOrEmpty())
                {
                    throw new RequestValidationException($"包裹号：{string.Join("，", agentOrdes.Select(x => x.OrderPackNo).ToList())} 已存在，不能重复导入");
                }*/

                var skus = dtos.Select(x => x.SKU).Distinct().ToList();
                var listProductInfo = productRepository.GetAll().Where(x => skus.Contains(x.SKU)).ToList();


                var orderIDs = dtos.Select(x => x.OrderPackNo.Substring(0, x.OrderPackNo.IndexOf('-'))).Distinct().ToList();

                
                var orderInfos = orderRepository.GetAll().Where(x => orderIDs.Contains(x.orderid)).ToList();


                foreach (var item in dtos)
                {
                    item.CreationTime = DateTime.UtcNow;
                    item.SKU = item.SKU.Trim();

                    var skuInfo = listProductInfo.FirstOrDefault(x => x.SKU.Trim() == item.SKU);
                    if (skuInfo != null)
                    {
                        item.CustomerID = skuInfo.CustomerID;
                    }
                    else
                    {
                        throw new RequestValidationException($"未找到SKU：{item.SKU}");
                    }

                    string orderID = item.OrderPackNo.Substring(0, item.OrderPackNo.IndexOf('-'));
                    var orderInfo = orderInfos.FirstOrDefault(x => x.orderid == orderID);
                    if (orderInfo != null)
                    {
                        item.OrderID = orderInfo.orderid;
                        item.OrderTime = orderInfo.orderCreateTime ?? DateTime.MinValue;
                    }
                    else
                    {
                        throw new RequestValidationException($"未找到订单：{orderID}");
                    }

                    var orderPackInfo = orderPackInfos.FirstOrDefault(x => x.orderPackNo == item.OrderPackNo);
                    if (orderPackInfo != null)
                    {
                        item.OutboundTime = orderPackInfo.out_time ?? null;
                    }

                    //check qty
                    var orderPackDetailInfo = listOrderPackDetail.Where(x => x.opid == orderPackInfo.opID && x.sku.StandardizeSKU() == item.SKU.StandardizeSKU() && x.qty == item.Qty);
                    if (orderPackDetailInfo == null)
                    {
                        throw new RequestValidationException($"请检查此包裹是否存在对应SKU及数量。包裹号：{orderPackInfo.orderPackNo}，SKU：{item.SKU}，数量：{item.Qty}");
                    }
                }

                var entities = dtos.MapTo<List<AgentProductOrderCostEntity>>();

                entities.ForEach(x =>
                {
                    //var existedEntry = agentOrdes.FirstOrDefault(y => y.OrderPackNo == x.OrderPackNo && y.SKU == x.SKU && y.Agency == x.Agency);
                    var existedEntry = agentOrdes.FirstOrDefault(y => y.OrderPackNo == x.OrderPackNo && y.SKU == x.SKU);


                    //update existing entry
                    if (existedEntry != null)
                    {
                        //throw new RequestValidationException($"条目已存在，不能重复导入。请修正。包裹号：{item.OrderPackNo}，SKU：{item.SKU}，供应商：{item.Agency}。");
                        existedEntry.ProductName = x.ProductName;
                        existedEntry.Qty = x.Qty;
                        existedEntry.Cost = x.Cost;
                        existedEntry.TotalCost = x.TotalCost;
                        existedEntry.OrderTime = x.OrderTime;
                        existedEntry.CreationTime = DateTime.UtcNow;
                        existedEntry.PurchasingCurrency = x.PurchasingCurrency;
                        existedEntry.PurchasingPrice = x.PurchasingPrice;
                        existedEntry.BenchmarkPrice = x.BenchmarkPrice;
                        existedEntry.BenchmarkPriceExchangeRate = x.BenchmarkPriceExchangeRate;
                        existedEntry.Agency = x.Agency;

                        var orderPack = orderPackInfos.FirstOrDefault(y => y.orderPackNo == x.OrderPackNo);
                        if (orderPack != null)
                        {
                            existedEntry.OutboundTime = orderPack.out_time ?? null;
                        }
                    }
                    else
                    {
                        agentProductOrderCostRepository.Insert(x);
                    }

                    //update cost and currency in product
                    var productInfo = listProductInfo.FirstOrDefault(y => y.SKU == x.SKU);
                    if (productInfo != null)
                    {
                        decimal? previousCost = productInfo.Cost;
                        
                        productInfo.Cost = x.Cost;
                        productInfo.Currency = x.PurchasingCurrency;

                        //log previous cost
                        EditLogEntity log = new EditLogEntity
                        {
                            System = "PMS",
                            Type = "Cost",
                            Content = $"{productInfo.SKU}:{previousCost}=>{productInfo.Cost}",
                            Uesr = accountManager.GetUserName(),
                            DateTime = DateTime.UtcNow,
                        };

                        editLogRepository.Insert(log);
                    }
                });

                CurrentUnitOfWork.SaveChanges();

                outputDTO.IsSuccess = true;
                outputDTO.Message = $"成功导入{entities.Count}条数据";
                return outputDTO;
            }
            catch (Exception ex)
            {
                CLSLogger.Error("导入excel发生异常", ex);
                throw new RequestValidationException($"{ex}");
            }

        }

        [HttpGet]
        public FileContentResult ExportToExcel(PagedQueryInputBaseDTO<AgentProductOrderCostDTO> inputDto)
        {
            try
            {
                //maximun 5000
                inputDto.PageSize = 5000;
                var result = GetListForExportExcel(inputDto, inputDto.PageSize);
                var listAgentProductOrderCostDTO = result.Data;

                var mapper = new Npoi.Mapper.Mapper();
                mapper.Put(listAgentProductOrderCostDTO, "newSheet");

                MemoryStream stream = new MemoryStream();
                mapper.Workbook.Write(stream);
                var bytes = stream.ToArray();
                FileContentResult output = new FileContentResult(bytes, "application/vnd.ms-excel");
                output.FileDownloadName = $"{DateTime.UtcNow.ToString("dd/MM/yyy")}.xls";
                return output;
            }
            catch (Exception ex)
            {
                Dictionary<string, string> tags = new Dictionary<string, string>();
                tags["type"] = ClogTagType;
                tags["trackId"] = "ExportToExcel";
                CLSLogger.Error("导出Excel发生异常", ex.Message, tags);
                throw new RequestValidationException($"导出发生异常，失败原因：{ex.Message}");
            }
        }
    }
}
